def folder='accounting/ibor-transaction-api'
def testFolder="$folder/tests"

//This is the main container build

job("$folder/build_ibor_transaction_api") {
  label('autoscale')
  multiscm {
    git {
      remote {
        name('origin')
        url('ssh://git@stash.ezesoft.net:7999/imsacnt/ibor-transaction-api.git')
        credentials('stash_global')
      }
      branch('master')
      extensions {
        submoduleOptions {
          //get latest submodules
          tracking(true)
        }
       }
      }
  }
	triggers {
		scm('H/5* * * * *')
	}
  wrappers {
    buildInDocker {
      dockerfile('./slave/')
      volume('/var/run/docker.sock', '/var/run/docker.sock')
      userGroup('2000')
      verbose()
    }
    environmentVariables {
    	propertiesFile('properties')
    }
    credentialsBinding {
      usernamePassword('dtr_user', 'dtr_password', 'dtr-builder')
      usernamePassword('vaultguard_username', 'vaultguard_password', 'vaultguard-user')
    }
    preBuildCleanup()
    maskPasswords()
    ansiColorBuildWrapper {
      colorMapName('XTerm')
      defaultFg(15)
      defaultBg(1)
    }
    buildName('${ENV,var="container_name"} ${ENV,var="stck_version"}')
  }
  steps {
    // Pull the latest jenkins-scripts
    shell('if [ -d $scripts ]; then rm -rf $scripts; fi && git clone $scripts_repo')

    // Build the container
    shell('bash jenkins-scripts/build-container-step.sh')

    // Run unit tests & Lint
    shell('bash scripts/runner.sh')
    
    // Run gometalinter for sonarqube analysis
    // shell('bash scripts/lint.sh')

    // Run sonarqube scanner
    sonarRunnerBuilder({
     project 'sonar-project.properties'
     properties 'sonar.projectVersion=$version_prefix.$BUILD_NUMBER'
     jdk '(Inherit From Job)'
     task 'scan'
    })

    // run end to end tests
    //shell('bash scripts/run-e2e.sh')

    // Upload the container
    shell('bash jenkins-scripts/upload-container-step.sh')

    // Register container with Vault
    shell('bash jenkins-scripts/register-container.sh Dev')
    shell('bash jenkins-scripts/register-container.sh AWSProd')

    // Assemble some variables to pass on the stack build
    shell('bash jenkins-scripts/vars.template')

  }
  publishers {

    archiveArtifacts{
      pattern("*.vars,reports/*.xml")
      onlyIfSuccessful()
    }
	
	whiteSourcePublisher {
		jobCheckPolicies('global')
		jobForceUpdate('global')
		jobApiToken('bc083042-88fb-4776-9288-afdf9044ae01')
		jobUserKey(null)
		product('b8072ee844704d9ca5cdecce033784522464af0749b04c26953779d48e55acad')
		productVersion(null)
		projectToken(null)
		libIncludes('**/*.c **/*.cc **/*.cp **/*.cpp **/*.cxx **/*.c++ **/*.h **/*.hpp **/*.hxx **/*.dll **/*.cs **/*.nupkg **/*.m **/*.mm **/*.js **/*.php **/*.gem **/*.rb **/*.tgz **/*.deb **/*.gzip **/*.rpm **/*.tar.bz2 **/*.zip **/*.tar.gz **/*.egg **/*.whl **/*.py')
		libExcludes('**/*.jar')
		requesterEmail(null)
		ignorePomModules(false)
		mavenProjectToken(null)
		modulesToInclude(null)
		modulesToExclude(null)
		moduleTokens(null)
	}
    stashNotifier()

    //Yes we need all these settings :(
    slackNotifier {
      teamDomain(null)
      authToken(null)
      room('team-invictus')
      startNotification(true)
      notifyNotBuilt(true)
      notifyAborted(true)
      notifyFailure(true)
      notifySuccess(true)
      notifyUnstable(true)
      notifyBackToNormal(true)
      notifyRepeatedFailure(true)
      includeTestSummary(true)
      includeCustomMessage(false)
      customMessage(null)
      buildServerUrl(null)
      sendAs(null)
      commitInfoChoice('NONE')
    }
    

    triggers {
      scm('H/5 * * * *')
    }

		downstream("$folder/build_ibor_transaction_api_stack", 'SUCCESS')
  }
}

job("$folder/build_ibor_transaction_api_stack") {
  label('autoscale')
  multiscm {
    git {
      remote {
        name('origin')
        url('ssh://git@stash.ezesoft.net:7999/imsacnt/ibor-transaction-api-stack.git')
        credentials('stash_global')      
      }
      branch('master')
    }
  }
  triggers {
    scm('H/5 * * * *')
  }

  wrappers {
    buildInDocker {
      dockerfile('./slave/')      
      volume('/var/run/docker.sock', '/var/run/docker.sock')
      userGroup('2000')
      verbose()
    }
    environmentVariables {
    	propertiesFile('properties')
    }
    maskPasswords()
    ansiColorBuildWrapper {
      colorMapName('XTerm')
      defaultFg(15)
      defaultBg(1) 
    }
    buildName('${ENV,var="stack_name"} ${ENV,var="stack_version"}')
    sonarBuildWrapper {
      installationName 'SonarQube'
    }
  }
  steps {

      copyArtifacts("$folder/build_ibor_transaction_api") {
        includePatterns('*.vars')
        targetDirectory('vars')
        flatten()
        optional(false)
        buildSelector {
            latestSuccessful(true)
        }
    }

    // Pull the latest jenkins-scripts
    shell('if [ -d $scripts ]; then rm -rf $scripts; fi && git clone $scripts_repo')
    shell('bash jenkins-scripts/rancher-run-template.sh')
    shell('bash jenkins-scripts/rancher-update-catalog.sh')
  }
  
  publishers {
    archiveArtifacts{
      pattern("docker-compose.yml")
      pattern("rancher-compose.yml")
      onlyIfSuccessful()
    }
  }
}
