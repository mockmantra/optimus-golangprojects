DROP DATABASE IF EXISTS test;
create database test;
use test;
DROP TABLE IF EXISTS Ibor_Transactions;
DROP TABLE IF EXISTS Ibor_ValidationErrors;
DROP TABLE IF EXISTS Ibor_Valuations_Equity;
-- ALTER USER 'root'@'localhost' IDENTIFIED WITH 'mysql_native_password' BY '123456';
-- FLUSH PRIVILEGES;

  CREATE TABLE IF NOT EXISTS Ibor_Transactions(
    RecordID            int NOT NULL AUTO_INCREMENT,
    MessageBody         text NOT NULL,
    BusinessDateTimeUTC datetime(6) NOT NULL,
    IsDeleted           boolean NOT NULL default FALSE,
    ActivityID          varchar(100) NOT NULL,
    ValidFromUTC		datetime(6) NOT NULL,
    ValidToUTC			datetime(6) default null ,
    UserID              int NOT NULL,
    UserName            varchar(250) NOT NULL,
    UserSessionToken    varchar(250) NOT NULL,
    EventTypeID         int NOT NULL,
    OrderQuantity       int NOT NULL,
    SecurityID          int NOT NULL,
    SourceSystemName    varchar(100) NOT NULL,
    SourceSystemReference    varchar(200) NOT NULL,
    PRIMARY KEY (RecordID)
);

CREATE TABLE IF NOT EXISTS Ibor_ValidationErrors(
ID int not null AUTO_INCREMENT,
TransactionRecordID  int not null,
TransactionType varchar(100) NOT NULL,
ErrorMessage varchar(250) Not Null,
PRIMARY KEY (ID),
FOREIGN KEY(TransactionRecordID) REFERENCES Ibor_Transactions(RecordID)
);



show databases;
show tables;