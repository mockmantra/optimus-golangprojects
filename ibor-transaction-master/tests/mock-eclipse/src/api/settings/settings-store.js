const _ = require("lodash");

const settingStore = {
    "TBZQK" : {
        firm: {
            "system/foo": "firm-bar",
            "system/faaz": "firm-baaz"
        },
        user: {
            "HANSOLO" : {
                "system/foo": "hansolo-bar"
            }, 
            "HANSOLO2" : {
                "system/foo": "hansolo2-bar"
            }
        }
    }
}

function splitFqSettingName(fqSettingName) {
    let parts = fqSettingName.split('/');
    return {
        productName: parts[0],
        settingName: parts[1]
    }
}

function getResource(productName,settingName, value, scope, userId, firmId) {
    if(!value) {
        return null;
    }

    let href = null;
    let id = null;
    if(scope === "user") {
        href = `/api/ipa/v1/settings/${productName}/${settingName}?userId=${userId}`;
        id = `${productName}/${settingName}?userId=${userId}`
    }else {
        href = `/api/ipa/v1/settings/${productName}/${settingName}?firmId=${firmId}`;
        id = `${productName}/${settingName}?firmId=${firmId}`
    }

    return {
        href,
        id,
        product: productName,
        settingName: settingName,
        value: value,
        scope
    }
}

var settingStoreMethods = {
    addFirmLevelSetting: (firmToken, productName, settingName, value) => {
        let fqSettingName = `${productName}/${settingName}`;
        firmToken = firmToken.toUpperCase();
        settingStore[firmToken] = settingStore[firmToken] || { firm: {}, user: {}};
        settingStore[firmToken].firm[fqSettingName] = value;
    },
    addUserLevelSetting: (firmToken, userName, productName, settingName, value) => {
        firmToken = firmToken.toUpperCase();
        userName = userName.toUpperCase();
        let fqSettingName = `${productName}/${settingName}`;
        settingStore[firmToken] = settingStore[firmToken] || { firm: {}, user: {}};
        settingStore[firmToken].user[userName][fqSettingName] = value;
    },
    getFirmLevelSettingsByProduct: (firmToken, productName) => {
        firmToken = firmToken.toUpperCase();
        if(!settingStore[firmToken]) {
            return null;
        }
        let firmLevelSettings = settingStore[firmToken].firm;
        var returnArray = [];
        Object.keys(firmLevelSettings).forEach(fqSettingName => {
            let name = splitFqSettingName(fqSettingName);
            if(name.productName !== productName) {
                return;
            }
            returnArray.push(getResource(productName, name.settingName, firmLevelSettings[fqSettingName], "firm", null, firmToken))
        });
        return returnArray;
    },
    getFirmLevelSetting: (firmToken, productName, settingName) => {
        firmToken = firmToken.toUpperCase();
        if(!settingStore[firmToken]) {
            return null;
        }
        let fqSettingName = `${productName}/${settingName}`;
        let firmLevelSettingValue = settingStore[firmToken].firm[fqSettingName];
        return getResource(productName, settingName, firmLevelSettingValue, 'firm', null, firmToken);
    },
    deleteFirmLevelSetting: (firmToken, productName, settingName) => {
        firmToken = firmToken.toUpperCase();

        if(!settingStore[firmToken]) {
            return false;
        }
        let fqSettingName = `${productName}/${settingName}`;

        if(fqSettingName in settingStore[firmToken].firm){
            delete settingStore[firmToken].firm[fqSettingName];
            return true;
        };
        return false;
    },

    getUserLevelSettingsByProduct: (firmToken, userName, productName) => {
        firmToken = firmToken.toUpperCase();
        userName = userName.toUpperCase();
        if(!settingStore[firmToken]) {
            return null;
        }
        let userLevelSettings = {};
        let firmLevelSettings = {};
        if(settingStore[firmToken].user[userName]) {
            userLevelSettings = _.clone(settingStore[firmToken].user[userName]);
        }
        if(settingStore[firmToken].firm) {
            firmLevelSettings = _.clone(settingStore[firmToken].firm);
        }

        let mergedSettings = Object.assign(firmLevelSettings, userLevelSettings); // overwrite firmlevel is one exists at user level
        let returnArray = [];
        Object.keys(mergedSettings).forEach(fqSettingName => {
            let name = splitFqSettingName(fqSettingName);
            if(name.productName !== productName) {
                return;
            }
            returnArray.push(getResource(productName, name.settingName, mergedSettings[fqSettingName], "user", userName))
        });
        return returnArray;
    },

    getUserLevelSetting: (firmToken, userName, productName, settingName) => {
        firmToken = firmToken.toUpperCase();
        userName = userName.toUpperCase();
        let fqSettingName = `${productName}/${settingName}`;
        if(!settingStore[firmToken]) {
            return null;
        }
        if(settingStore[firmToken].user[userName] && settingStore[firmToken].user[userName][fqSettingName]) {
            let userLevelSettingValue = settingStore[firmToken].user[userName][fqSettingName];
            return getResource(productName, settingName, userLevelSettingValue, 'user', userName);
        }
        else {
            return settingStoreMethods.getFirmLevelSetting(firmToken, productName, settingName);
        }
    },

    deleteUserLevelSetting: (firmToken, userName, productName, settingName) => {
        firmToken = firmToken.toUpperCase();
        userName = userName.toUpperCase();
        let fqSettingName = `${productName}/${settingName}`;

        if(!settingStore[firmToken]) {
            return false;
        }

        if(settingStore[firmToken].user[userName] && fqSettingName in settingStore[firmToken].user[userName]){
            delete settingStore[firmToken].user[userName][fqSettingName];
            return true;
        };
        return false;
    }
}

module.exports = settingStoreMethods;