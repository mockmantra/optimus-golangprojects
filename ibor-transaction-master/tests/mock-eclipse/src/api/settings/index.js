const systemSettings = require("./system-settings");

let mergedApiDefs = {};
Object.assign(mergedApiDefs, systemSettings);

module.exports = mergedApiDefs;