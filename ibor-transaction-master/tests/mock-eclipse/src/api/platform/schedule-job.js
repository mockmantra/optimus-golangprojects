const scheduler = {
    route: "/api/platform/v1/schedulerjob",
    get: (req, res, next) => {
        res.status(200).json(
            [
                {
                    "ResourceId": "15927bbd-f8c5-4c2e-8af0-1b55cc02191b",
                    "DisplayName": "Mocked Scheduler Job 1",
                    "ScheduleType": "DailySchedule",
                    "Status": 0,
                    "LastFiredTimeUtc": "2018-01-02T05:00:00Z",
                    "NextFireTimeUtc": "2018-01-03T05:00:00Z",
                    "Name": "15927bbd-f8c5-4c2e-8af0-1b55cc02191b",
                    "ProductId": "reporting",
                    "JobViewerId": "sb_schedulerJob",
                    "JobMetadata": {
                      "Username": "rsalhotra",
                      "Operation": 2,
                      "ScheduleData": {
                        "Schedule": "{\"recurrence\":{\"repetition\":{\"repeatEvery\":1,\"daily\":{\"excludeWeekends\":true},\"weekly\":null,\"monthly\":null,\"yearly\":null},\"end\":{\"after\":null,\"by\":null,\"byDate\":null}},\"start\":\"2017-08-03T00:00:00\",\"timeZone\":\"Eastern Standard Time\"}",
                        "ScheduleId": "15927bbd-f8c5-4c2e-8af0-1b55cc02191b",
                        "ReportList": {},
                        "FileNameList": {},
                        "ParamterList": {},
                        "PreRenderParameterList": {},
                        "Recipient": {
                          "test": "test@no-email.com"
                        },
                        "Enabled": true,
                        "Id": "15927bbd-f8c5-4c2e-8af0-1b55cc02191b"
                      },
                      "ScheduleStartTime": "2017-08-03T00:00:00",
                      "ScheduleEndTime": "1753-01-01T00:00:00"
                    },
                    "OccurrenceJson": "{\"start\":\"2017-08-03T00:00:00\",\"timeZone\":\"Eastern Standard Time\",\"recurrence\":{\"repetition\":{\"repeatEvery\":1,\"daily\":{\"excludeWeekends\":true}}}}"
                  },
                  {
                    "ResourceId": "2574005e-e57c-4ec3-9660-d1ef3f6fc967",
                    "DisplayName": "Mocked Scheduler Job 2",
                    "ScheduleType": "DailySchedule",
                    "Status": 0,
                    "LastFiredTimeUtc": "2018-01-02T05:00:00Z",
                    "NextFireTimeUtc": "2018-01-03T05:00:00Z",
                    "Name": "2574005e-e57c-4ec3-9660-d1ef3f6fc967",
                    "ProductId": "reporting",
                    "JobViewerId": "sb_schedulerJob2",
                    "JobMetadata": {
                      "Username": "rsalhotra",
                      "Operation": 2,
                      "ScheduleData": {
                        "Schedule": "{\"recurrence\":{\"repetition\":{\"repeatEvery\":1,\"daily\":{\"excludeWeekends\":true},\"weekly\":null,\"monthly\":null,\"yearly\":null},\"end\":{\"after\":null,\"by\":null,\"byDate\":null}},\"start\":\"2017-08-03T00:00:00\",\"timeZone\":\"Eastern Standard Time\"}",
                        "ScheduleId": "2574005e-e57c-4ec3-9660-d1ef3f6fc967",
                        "ReportList": {},
                        "FileNameList": {},
                        "ParamterList": {},
                        "PreRenderParameterList": {},
                        "Recipient": {
                          "test": "test@no-email.com"
                        },
                        "Enabled": true,
                        "Id": "15927bbd-f8c5-4c2e-8af0-1b55cc02191b"
                      },
                      "ScheduleStartTime": "2017-08-03T00:00:00",
                      "ScheduleEndTime": "1753-01-01T00:00:00"
                    },
                    "OccurrenceJson": "{\"start\":\"2017-08-03T00:00:00\",\"timeZone\":\"Eastern Standard Time\",\"recurrence\":{\"repetition\":{\"repeatEvery\":1,\"daily\":{\"excludeWeekends\":true}}}}"
                  },
                  {
                    "ResourceId": "180bbdaf-9754-4ef4-b95e-4e08fb2881ee",
                    "DisplayName": "Mocked SchedulerJob 3",
                    "ScheduleType": "DailySchedule",
                    "Status": 0,
                    "LastFiredTimeUtc": "2018-01-02T05:00:00Z",
                    "NextFireTimeUtc": "2018-01-03T05:00:00Z",
                    "Name": "180bbdaf-9754-4ef4-b95e-4e08fb2881ee",
                    "ProductId": "reporting",
                    "JobViewerId": "sb_schedulerJob2",
                    "JobMetadata": {
                      "Username": "rsalhotra",
                      "Operation": 2,
                      "ScheduleData": {
                        "Schedule": "{\"recurrence\":{\"repetition\":{\"repeatEvery\":1,\"daily\":{\"excludeWeekends\":true},\"weekly\":null,\"monthly\":null,\"yearly\":null},\"end\":{\"after\":null,\"by\":null,\"byDate\":null}},\"start\":\"2017-08-03T00:00:00\",\"timeZone\":\"Eastern Standard Time\"}",
                        "ScheduleId": "180bbdaf-9754-4ef4-b95e-4e08fb2881ee",
                        "ReportList": {},
                        "FileNameList": {},
                        "ParamterList": {},
                        "PreRenderParameterList": {},
                        "Recipient": {
                          "test": "test@no-email.com"
                        },
                        "ScheduleSummary": "protractor delete test",
                        "Enabled": true,
                        "Id": "180bbdaf-9754-4ef4-b95e-4e08fb2881ee"
                      },
                      "ScheduleSummary": "protractor delete test",
                      "ScheduleStartTime": "2018-01-02T00:00:00",
                      "ScheduleEndTime": "1753-01-01T00:00:00"
                    },
                    "OccurrenceJson": "{\"start\":\"2017-08-03T00:00:00\",\"timeZone\":\"Eastern Standard Time\",\"recurrence\":{\"repetition\":{\"repeatEvery\":1,\"daily\":{\"excludeWeekends\":true}}}}"
                  }
            ]            
        );
    }
}

module.exports = {
    scheduler
}