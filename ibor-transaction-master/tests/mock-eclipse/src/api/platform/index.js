const common = require("./common");
const session = require("./session");
const scheduler = require("./schedule-job");
const dashboardConfig = require("./dashboardconfig");

let mergedApiDefs = {};
Object.assign(mergedApiDefs, common, session, 
    dashboardConfig, scheduler

);

module.exports = mergedApiDefs;