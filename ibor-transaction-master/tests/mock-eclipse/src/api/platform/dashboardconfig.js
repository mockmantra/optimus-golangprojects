const dashboardConfigByNameRoute = "/api/platform/v1/dashboardconfiguration/:configName";
const dashboardConfigRoute = "/api/platform/v1/dashboardconfiguration";
const userStore = require("../../common/sessions");
const basicAuth = require("basic-auth");

const dashboardConfigStore = {
    "TBZQK-HANSOLO": {

    }
}


function findConfigSettingByName(firmAuthToken, userName, configName) {
    userName = userName.toUpperCase();
    firmAuthToken = firmAuthToken.toUpperCase();
    let fqUserName = `${firmAuthToken}-${userName}`;
    if (!dashboardConfigStore[fqUserName]) {
        dashboardConfigStore[fqUserName] = {};
    }
    return dashboardConfigStore[fqUserName][configName];
}

function dashboardConfigAddSettingByName(firmAuthToken, userName, configName, value) {
    userName = userName.toUpperCase();
    firmAuthToken = firmAuthToken.toUpperCase();
    let fqUserName = `${firmAuthToken}-${userName}`;
    if (!dashboardConfigStore[fqUserName]) {
        dashboardConfigStore[fqUserName] = {};
    }
    dashboardConfigStore[fqUserName][configName] = value;
    return dashboardConfigStore[fqUserName][configName];
}



const dashboardConfigByNameApi = {
    route: dashboardConfigByNameRoute,
    get: function (req, res, next) {
        if (!req.headers.authorization) {
            res.status(401).send();
            return false;
        }

        let credentials = basicAuth(req);
        let glx2Token = credentials.name;

        let userSession = userStore.getSessionByGlxToken(glx2Token);

        if (!userSession) {
            res.status(401).send();
            return false;
        }

        let userInfo = userSession.UserSession;

        let configSetting = findConfigSettingByName(userInfo.FirmAuthToken, userInfo.UserName, req.params.configName);
        res.status(200).json({
            Configuration: configSetting,
            Name: req.params.configName
        });
    }
}

const dashboardConfigPutApi = {
    route: dashboardConfigRoute,

    put: function (req, res, next) {
        if (!req.headers.authorization) {
            res.status(401).send();
            return false;
        }

        let credentials = basicAuth(req);
        let glx2Token = credentials.name;

        let userSession = userStore.getSessionByGlxToken(glx2Token);

        if (!userSession) {
            res.status(401).send();
            return false;
        }

        let userInfo = userSession.UserSession;

        if (!req.body || !req.body.Name || !req.body.Configuration) {
            res.status(400).json("Invalid request body");
            return false;
        }

        let addedSetting = dashboardConfigAddSettingByName(userInfo.FirmAuthToken, userInfo.UserName, req.body.Name, req.body.Configuration);

        res.status(200).json({
            Configuration: addedSetting,
            Name: req.body.Name
        });
    }
}


module.exports = {
    dashboardConfigByNameApi,
    dashboardConfigPutApi
}