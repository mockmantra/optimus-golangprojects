const workers_api_Route = "/api/work-distribution/v1/workers";

const workersResponse = {
    "Id": "df6aa56d-df3a-216d-dee6-0ae44d15ed54",
    "Metadata": {
        "Container": "ibor-transaction-master-stack-ibor-transaction-master-1",
        "Host": "aecastle01acc01.awsdev.ezesoftcloud.com",
        "Instance": "9809b5767a7d"
    },
    "TTL": "15s",
    "WorkType": "ibor_transaction_master_leader_election",
    "WorkerGroup": "ibor_transaction_master"
};


const workResponse = {
    ...workersResponse,
    "WorkItems": [
        {
            "Extra": {
                "AssignedOn": "2019-01-08T16:43:34.944Z"
            },
            "Metadata": {
                "SequenceNumber": ""
            },
            "WorkId": "ibor_transaction_master_leader_election"
        }
    ]
};



const wdApi = {
    route: workers_api_Route,
    post: function (req, res, next) {
        res.set('x-workdistribution-index', 0);
        res.status(201).json(workersResponse)
    },
    get: function (req, res, next) {
        res.set('x-workdistribution-index', 1);
        res.set('x-remaining-ttl', '15s');
        res.status(200).json(workResponse)
    }
}

module.exports = {
    wdApi
};