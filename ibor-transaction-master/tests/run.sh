#!/bin/bash

export APP_ENV="local"
export host=sqldb

export IMS_BASE_URL="http://nginx:8001"
export AWS_REGION="us-east-1"

export AWS_PROXY_URL="http://localhost:4567"
export AWS_UP_STREAM_NAME="CastleAccountingTransaction01"
export AWS_DOWN_STREAM_NAME="CastleAccountingTransactionIntraday01"
export PORT=6989

export CONSUL_URL="http://aecastle01ran01.awsdev.ezesoftcloud.com:8500"
export CL_DPERMSRESTURI="aedevauth01dperm.awsdev.ezesoftcloud.com"
export IMS_INTI_SVC_USER="IborTMSvcUser"
export AWS_REGION_NAME="us-east-1"

node /go/src/stash.ezesoft.net/imsacnt/ibor-transaction-master/tests/service/tx-api/kinesis_server.js &
sleep 2
node /go/src/stash.ezesoft.net/imsacnt/ibor-transaction-master/tests/service/tx-api/Create_Stream.js 
node /go/src/stash.ezesoft.net/imsacnt/ibor-transaction-master/tests/service/tx-api/server.js &
sleep 2
go run /go/src/stash.ezesoft.net/imsacnt/ibor-transaction-master/src/cmd/ibor-transaction-master/main.go &

sleep 8

npm test