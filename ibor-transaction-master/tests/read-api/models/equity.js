var db = require ('../dbconnection');

var Task = {
  getAllTasks: function (callback) {
    return db.query (
      'Select Id,TransactionRecordID,TransactionType,ErrorMessage  from test.Ibor_ValidationErrors;',
      callback
    );
  },
  getTaskById: function (id, callback) {
    return db.query (
      'Select * FROM test.Ibor_ValidationErrors where id=?',
      [id],
      callback
    );
  },

  deleteAll: function (callback) {
    // return db.query ('truncate table test.Ibor_ValidationErrors ;', callback);
    // return db.query ('delete from test.Ibor_ValidationErrors ;', callback);

    return db.query ('DROP TABLE IF EXISTS test.Ibor_ValidationErrors ;CREATE TABLE IF NOT EXISTS test.Ibor_ValidationErrors(ID int not null AUTO_INCREMENT, TransactionRecordID  int not null,TransactionType varchar(100) NOT NULL,ErrorMessage varchar(250) Not Null,PRIMARY KEY (ID),FOREIGN KEY(TransactionRecordID) REFERENCES test.Ibor_Transactions(RecordID));', callback);
  },
};
module.exports = Task;