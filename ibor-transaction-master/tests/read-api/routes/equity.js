var express = require ('express');
var router = express.Router ();
var Task = require ('../models/equity');

router.get ('/:id?', function (req, res, next) {
  if (req.params.id) {
    Task.getTaskById (req.params.id, function (err, rows) {
      if (err) {
        res.json (err);
      } else {
        res.json (rows);
      }
    });
  } else {
    Task.getAllTasks (function (err, rows) {
      if (err) {
        res.json (err);
      } else {
        res.json (rows);
      }
    });

    // Task.deleteAll (function (err, count) {
    //     if (err) {
    //       res.json (err);
    //     } else {
    //       res.json (count);
    //     }
    //   });
  }
});
router.post ('/', function (req, res, next) {
  Task.addTask (req.body, function (err, count) {
    //console.log(req.body);
    if (err) {
      res.json (err);
    } else {
      res.json (req.body); //or return count for 1 & 0
    }
  });
});
// router.delete (function (req, res, next) {
//   Task.deleteAll (req.body, function (err, count) {
//     if (err) {
//       res.json (err);
//     } else {
//       res.json (count);
//     }
//   });
// });
router.delete ('/',function (req, res, next) {
  Task.deleteAll (function (err, count) {
    if (err) {
      res.json (err);
    } else {
      res.json (count);
    }
  });
});
router.put ('/:id', function (req, res, next) {
  Task.updateTask (req.params.id, req.body, function (err, rows) {
    if (err) {
      res.json (err);
    } else {
      res.json (rows);
    }
  });
});
module.exports = router;
