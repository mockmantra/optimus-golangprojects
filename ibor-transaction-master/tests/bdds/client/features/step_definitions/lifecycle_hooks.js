'use strict';
const Session = require('./../support/services/session');
const HttpService = require('./../support/services/http');

const fs = require('fs');
const { defineSupportCode } = require('cucumber');

defineSupportCode(function({BeforeAll, Before, After, AfterAll}) {
    let headerToken;
    console.log("INSIDE BEFORECALL -----------");
    Before(function() {
        this.httpService = new HttpService(process.env.API_ROOT);
    });
});