/*
Any common steps will be placed here for use and 
chai will be used for assertions
*/
'use strict';
// const kinesis = require('./../kinesisutil/index');



const {
    defineSupportCode
} = require('cucumber')
const chai = require('chai');
const chaiExclude = require('chai-exclude');
chai.use(chaiExclude);
var {
    setDefaultTimeout
} = require('cucumber');

var sortJsonArray = require('sort-json-array');
var rateId;
var nextRateId;
var ratePutId;
var rateNextPutId;

defineSupportCode(function (self) {
    let Given = self.Given;
    let When = self.When;
    let Then = self.Then;

    Given(/^a request to (.*)$/, function (endpoint) {
        this.httpService.setDefaults(endpoint);
    });

    When(/^I make a GET request to (.*)$/, async function (path) {
        var sleep = require('system-sleep');
        sleep(10000); 
        this.response = await this.httpService.get(path);
    });

    Given(/^Enabling of Database validation (.*)$/, async function (path) {
        
        process.env.API_ROOT="http://readapi:8004/api";
        console.log('Database Validation Enabled');
        var sleep = require('system-sleep');
        sleep(4000); 
    });
    Given(/^Disabling of Database validation (.*)$/, async function (path) {
        process.env.API_ROOT="http://localhost:8002/api";
        console.log('Database Validation Disabled');
    });
    When(/^I make a POST request for (.*)$/, async function (path,params) {
        this.response = await this.httpService.postp(path, params);
    });

    When(/^I make a POST request to (.*)$/, async function (path) {
        this.response = await this.httpService.postr(path);
    });

    When(/^I make a DELETE request to (.*)$/, async function (path) {
        this.response = await this.httpService.deleter(path);
        var sleep = require('system-sleep');
        sleep(4000); 
    });

    When(/^making request to (.*)$/, async function (path,body) {
        var sleep = require('system-sleep');
        sleep(10000); 
        this.response = await this.httpService.get(path);
        var fs = require("fs");
        var exfilename = ((JSON.parse(body)).name);
        var relativeFileName = __dirname + '/../JSONs/' + exfilename;
        var config = fs.readFileSync(relativeFileName, "utf8");
        chai.expect(this.response.body).contains(config);
    });

    Then(/^I should get a (\d{3}) response$/, function (responseCode) {
        chai.expect(this.response.statusCode).to.equal(parseInt(responseCode));
        var sleep = require('system-sleep');
        sleep(15000);
    });

    Then(/^the response should be equal to exactly the default data$/, function (expected) {
        var obj = this.response.body;
        console.log("GETTING THE FIELD HERE",obj);
        var json = JSON.parse(obj);
        var expectedvalue = expected;
        var S = require('string');
        var final = S(expected).trim();
        var validation = false;
        try {
            for (let a = 0; a < json.length; a++) {
                var check = S(json[a].ErrorMessage).trim();
                var contains = require("string-contains");
                if (check.contains(final) == true) {
                    validation = true;
                }
            }
        } catch (err) {
            console.log('IN CATCH BLOCK');
        }
        chai.assert.isTrue(validation, 'is True');
    });

    Then(/^verify the error table data$/, function (expected) {

        var obj = this.response.body;
        var json = JSON.parse(obj);
        var expectedvalue = expected;
        var S = require('string');
        var final = S(expected).trim();
        var validation = false;
        try {
            for (let a = 0; a < json.length; a++) {
                var check = S(json[a].errormessage).trim();
                var contains = require("string-contains");
                if (check.contains(final) == false) {
                    validation = true;
                }
            }
        } catch (err) {
            console.log('IN CATCH BLOCK');
        }
        chai.assert.isTrue(validation, 'is True');
    });


    Then(/^perform duplicate records validation (.*)$/, async function (path) {
        var obj = this.response.body;
        var json = JSON.parse(obj);
        var first_max_rec_id = 0;
        first_max_rec_id = json[0].RecordID;
        chai.expect(2).to.equal(first_max_rec_id);
    });
});

