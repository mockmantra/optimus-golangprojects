'use strict';



class kinesis {
    /**
     * Basic HTTP service
     * @param {string} headerToken Auth token to be used in all HTTP calls
     * @param {string} apiRoot Root for all http calls
     */
    constructor() {
        // this.headerToken = headerToken;
        // this.apiRoot = apiRoot;
        console.log("inside the constructor")
    }

    /**
     * 
     * @param {string} apiPath the path to the API after {base_url}/api/
     * @example
     * `/pricemaster/v2/`
     * 
     */




    /**
     * Method will accept the api endpoint and an object then fire a GET request 
     * @typedef {Object} params Object to be formatted into query string
     * @property {string|Number} queryParam the name of the query parameter and its value
     * @param {string} path The path to the method
     * @param {params} params
     * @example
     * ```get('/pricemaster/v2/fxRates', {'businessDate': '11-24-2012', 'logicalTimeId': 'StartOfDay'})```
     */
    get(path, params) {
        if (params) {
            params = JSON.parse(params);
        }
        return this.baseRequest(path, {
            method: 'GET',
            qs: params
        }).then(res => res.toJSON());

        console.log("!!!!!#############RESPONSE#############!!!! " + JSON.parse(this.response.body));
    }

    writeToKinesis() {


        const kinesisUtil = require('./kinesisutil');

        const kinesis_data = kinesisUtil('CastleAccountingTransaction01');


        var main_json =
            "{\"firmId\":2,\"firmAuthToken\":\"T71Q5\",\"userId\":1,\"userName\":\"nchennoju\",\"activityId\":\"0E050403-4286-3CEF-5B4B-EED15D08A097\",\"message\":\"{\\\"Allocations\\\":[{\\\"BaseCurrencySecurityId\\\":155,\\\"BaseToLocalFxRate\\\":1,\\\"BookTypeId\\\":1,\\\"CommissionCharges\\\":null,\\\"CustodianCounterpartyAccountId\\\":10,\\\"FeeCharges\\\":[],\\\"PortfolioId\\\":21,\\\"PositionTags\\\":[{\\\"IndexAttributeId\\\":1,\\\"IndexAttributeValueId\\\":3}],\\\"Quantity\\\":400},{\\\"BaseCurrencySecurityId\\\":155,\\\"BaseToLocalFxRate\\\":1,\\\"BookTypeId\\\":2,\\\"CommissionCharges\\\":[],\\\"CustodianCounterpartyAccountId\\\":10,\\\"FeeCharges\\\":[],\\\"PortfolioId\\\":21,\\\"PositionTags\\\":[],\\\"Quantity\\\":400},{\\\"BaseCurrencySecurityId\\\":155,\\\"BaseToLocalFxRate\\\":1,\\\"BookTypeId\\\":3,\\\"CommissionCharges\\\":null,\\\"FeeCharges\\\":null,\\\"PortfolioId\\\":21,\\\"PositionTags\\\":[{\\\"IndexAttributeId\\\":1,\\\"IndexAttributeValueId\\\":3}],\\\"Quantity\\\":400},{\\\"BaseCurrencySecurityId\\\":155,\\\"BaseToLocalFxRate\\\":1,\\\"BookTypeId\\\":1,\\\"Commission\\\":10,\\\"CommissionCharges\\\":[{\\\"CommissionCharge\\\":10,\\\"CommissionChargeId\\\":22},{\\\"CommissionCharge\\\":0,\\\"CommissionChargeId\\\":24}],\\\"CustodianCounterpartyAccountId\\\":10,\\\"ExecQuantity\\\":0,\\\"FeeCharges\\\":[{\\\"FeeCharge\\\":0.6555,\\\"FeeChargeId\\\":27},{\\\"FeeCharge\\\":0,\\\"FeeChargeId\\\":28},{\\\"FeeCharge\\\":0.7867,\\\"FeeChargeId\\\":29},{\\\"FeeCharge\\\":0.9834,\\\"FeeChargeId\\\":30},{\\\"FeeCharge\\\":19.668,\\\"FeeChargeId\\\":31},{\\\"FeeCharge\\\":25,\\\"FeeChargeId\\\":32},{\\\"FeeCharge\\\":15,\\\"FeeChargeId\\\":33},{\\\"FeeCharge\\\":98.34,\\\"FeeChargeId\\\":34}],\\\"Fees\\\":160.4336,\\\"NetAmount\\\":19838.4336,\\\"PortfolioId\\\":21,\\\"PositionTags\\\":[{\\\"IndexAttributeId\\\":1,\\\"IndexAttributeValueId\\\":3}],\\\"PrincipalAmount\\\":19668,\\\"Quantity\\\":200,\\\"RouteId\\\":1621,\\\"RouteName\\\":\\\"MANUAL\\\",\\\"SettleAmount\\\":19838.4336,\\\"SettleCurrencySecurityId\\\":155,\\\"SettlePrice\\\":196.68,\\\"SettleToBaseFXRate\\\":1,\\\"SettleToLocalFXRate\\\":1,\\\"SettleToSystemFXRate\\\":1},{\\\"BaseCurrencySecurityId\\\":155,\\\"BaseToLocalFxRate\\\":1,\\\"BookTypeId\\\":2,\\\"Commission\\\":10,\\\"CommissionCharges\\\":[{\\\"CommissionCharge\\\":10,\\\"CommissionChargeId\\\":22},{\\\"CommissionCharge\\\":0,\\\"CommissionChargeId\\\":24}],\\\"CustodianCounterpartyAccountId\\\":10,\\\"ExecQuantity\\\":100,\\\"FeeCharges\\\":[{\\\"FeeCharge\\\":0.6555,\\\"FeeChargeId\\\":27},{\\\"FeeCharge\\\":0,\\\"FeeChargeId\\\":28},{\\\"FeeCharge\\\":0.7867,\\\"FeeChargeId\\\":29},{\\\"FeeCharge\\\":0.9834,\\\"FeeChargeId\\\":30},{\\\"FeeCharge\\\":19.668,\\\"FeeChargeId\\\":31},{\\\"FeeCharge\\\":25,\\\"FeeChargeId\\\":32},{\\\"FeeCharge\\\":15,\\\"FeeChargeId\\\":33},{\\\"FeeCharge\\\":98.34,\\\"FeeChargeId\\\":34}],\\\"Fees\\\":160.4336,\\\"NetAmount\\\":19838.4336,\\\"PortfolioId\\\":21,\\\"PositionTags\\\":[],\\\"PrincipalAmount\\\":19668,\\\"Quantity\\\":200,\\\"RouteId\\\":1621,\\\"RouteName\\\":\\\"MANUAL\\\",\\\"SettleAmount\\\":19838.4336,\\\"SettleCurrencySecurityId\\\":155,\\\"SettlePrice\\\":196.68,\\\"SettleToBaseFXRate\\\":1,\\\"SettleToLocalFXRate\\\":1,\\\"SettleToSystemFXRate\\\":1},{\\\"BaseCurrencySecurityId\\\":155,\\\"BaseToLocalFxRate\\\":1,\\\"BookTypeId\\\":3,\\\"Commission\\\":10,\\\"CommissionCharges\\\":[{\\\"CommissionCharge\\\":10,\\\"CommissionChargeId\\\":22},{\\\"CommissionCharge\\\":0,\\\"CommissionChargeId\\\":24}],\\\"ExecQuantity\\\":100,\\\"FeeCharges\\\":[{\\\"FeeCharge\\\":0.6555,\\\"FeeChargeId\\\":27},{\\\"FeeCharge\\\":0,\\\"FeeChargeId\\\":28},{\\\"FeeCharge\\\":0.7867,\\\"FeeChargeId\\\":29},{\\\"FeeCharge\\\":0.9834,\\\"FeeChargeId\\\":30},{\\\"FeeCharge\\\":19.668,\\\"FeeChargeId\\\":31},{\\\"FeeCharge\\\":25,\\\"FeeChargeId\\\":32},{\\\"FeeCharge\\\":15,\\\"FeeChargeId\\\":33},{\\\"FeeCharge\\\":98.34,\\\"FeeChargeId\\\":34}],\\\"Fees\\\":160.4336,\\\"NetAmount\\\":19838.4336,\\\"PortfolioId\\\":21,\\\"PositionTags\\\":[{\\\"IndexAttributeId\\\":1,\\\"IndexAttributeValueId\\\":3}],\\\"PrincipalAmount\\\":19668,\\\"Quantity\\\":200,\\\"RouteId\\\":1621,\\\"SettleAmount\\\":19838.4336,\\\"SettleCurrencySecurityId\\\":155,\\\"SettlePrice\\\":196.68,\\\"SettleToBaseFXRate\\\":1,\\\"SettleToLocalFXRate\\\":1,\\\"SettleToSystemFXRate\\\":1}],\\\"BusinessDateTimeUTC\\\":\\\"2018-09-08T13:25:43.0000000Z\\\",\\\"EventTypeId\\\":0,\\\"IsFinalized\\\":true,\\\"LocalCurrencySecurityId\\\":0,\\\"OrderCreatedDate\\\":\\\"2018-09-08T13:25:43.0000000Z\\\",\\\"OrderQuantity\\\":600,\\\"SecurityId\\\":375,\\\"SecurityTemplateId\\\":1,\\\"SequenceNumber\\\":63,\\\"SettleDate\\\":\\\"2018-09-10T13:25:43Z\\\",\\\"SourceSystemName\\\":\\\"Trading\\\",\\\"SourceSystemReference\\\":\\\"4803\\\",\\\"SystemCurrencySecurityId\\\":155,\\\"SystemToLocalFXRate\\\":1,\\\"TradeDate\\\":\\\"2018-09-08T13:25:43Z\\\",\\\"TransactionDateTimeUTC\\\":\\\"2018-09-08T13:25:43.0000000Z\\\",\\\"TransactionTimeZoneId\\\":1,\\\"UserTimeZoneId\\\":12}\",\"userSessionToken\":\"good-glx2-token-hansolo\"}"


        kinesis_data.writeToKinesis(main_json);

    }
    /**
     * Make a DELETE request
     * @param {string} path path to the API (after the base url)
     * @param {*} params object to be passed as parameters
     * @example
     * `delete('positionEvents', {'sourceSystemName': ____, 'sourceSystemReference': _____, 'utcAsOfDateTime': _____})`
     */

    /**
     * Make a POST request
     * @param {string} path the path to the method
     * @param {{}} body the JSON object being passed to the POST call
     */




}

module.exports = kinesis;