const kinesisUtil = require('./kinesisutil');


const kinesis1Client = kinesisUtil('CastleAccountingTransaction01');
const kinesis2Client = kinesisUtil('CastleAccountingTransactionIntraday01');


kinesis1Client.createStreamIfNotCreated((err) => {
    console.log(err)
});

kinesis2Client.createStreamIfNotCreated((err) => {
    console.log(err)
});
