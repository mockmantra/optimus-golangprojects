var express = require ('express');
var bodyParser = require ('body-parser');
var app = express ();

const kinesisUtil = require ('./kinesisutil');

const kinesis_data = kinesisUtil ('CastleAccountingTransaction01');

app.use (bodyParser.urlencoded ({extended: false}));
app.use (bodyParser.json ());
var d = new Date (),
  month = '' + (d.getMonth () + 1),
  day = '' + d.getDate (),
  day2 = '' + (d.getDate () + 2),
  year = d.getFullYear ();

if (month.length < 2) month = '0' + month;
if (day.length < 2) day = '0' + day;
if (day2.length < 2) day2 = '0' + day2;
var date_value = [year, month, day].join ('-');
var settle_date = [year, month, day2].join ('-');

app.post ('/api/ibor/trans/v1/equity', function (req, res) {
  res.send ('Coming up');
  var main_json =
    '{"firmId":2,"firmAuthToken":"T71Q5","userId":1,"userName":"nchennoju","activityId":"0E050403-4286-3CEF-5B4B-EED15D08A097","message":"{\\"BusinessDateTimeUTC\\":\\"' +
    date_value +
    'T13:25:43.0000000Z\\",\\"TransactionDateTimeUTC\\":\\"' +
    date_value +
    'T13:25:43.0000000Z\\",\\"SettleDate\\":\\"' +
    settle_date +
    'T13:25:43Z\\",\\"TradeDate\\":\\"' +
    date_value +
    'T13:25:43Z\\",\\"OrderCreatedDate\\":\\"' +
    date_value +
    'T13:25:43.0000000Z\\",\\"EventTypeId\\":3,\\"LocalCurrencySecurityId\\":155,\\"OrderQuantity\\":600,\\"SecurityId\\":1000,\\"SecurityTemplateId\\":1,\\"SequenceNumber\\":63,\\"SourceSystemName\\":\\"Trading\\",\\"SourceSystemReference\\":\\"13679\\",\\"SystemCurrencySecurityId\\":155,\\"SystemToLocalFXRate\\":1,\\"TransactionTimeZoneId\\":1,\\"UserTimeZoneId\\":12}","userSessionToken":"good-glx2-token-hansolo"}';

  kinesis_data.writeToKinesis (main_json);
});

app.post ('/api/ibor/trans/v1/equity_commission', function (req, res) {
  res.send ('Coming up');
  var main_json =
    '{"firmId":2,"firmAuthToken":"T71Q5","userId":1,"userName":"nchennoju","activityId":"0E050403-9211-4B1E-2E3F-2E02135D957C","message":"{\\"BusinessDateTimeUTC\\":\\"' +
    date_value +
    'T13:25:43.0000000Z\\",\\"TransactionDateTimeUTC\\":\\"' +
    date_value +
    'T13:25:43.0000000Z\\",\\"SettleDate\\":\\"' +
    settle_date +
    'T13:25:43Z\\",\\"TradeDate\\":\\"2018-09-08T13:25:43Z\\",\\"OrderCreatedDate\\":\\"' +
    date_value +
    'T13:25:43.0000000Z\\",\\"Allocations\\":[{\\"BaseCurrencySecurityId\\":155,\\"BaseToLocalFxRate\\":1,\\"BookTypeId\\":2,\\"CommissionCharges\\":[],\\"FeeCharges\\":[],\\"IsFinalized\\":true,\\"PortfolioId\\":21,\\"Quantity\\":400}],\\"EventTypeId\\":14,\\"LocalCurrencySecurityId\\":155,\\"OrderQuantity\\":400,\\"SecurityId\\":1000,\\"SecurityTemplateId\\":1,\\"SequenceNumber\\":63,\\"SourceSystemName\\":\\"Trading\\",\\"SourceSystemReference\\":\\"13679\\",\\"SystemCurrencySecurityId\\":155,\\"SystemToLocalFXRate\\":1,\\"TransactionTimeZoneId\\":1,\\"UserTimeZoneId\\":12}","userSessionToken":"good-glx2-token-hansolo"}';

  kinesis_data.writeToKinesis (main_json);
});
app.listen (8002, function () {
  console.log ('Started on PORT 8002');
});
