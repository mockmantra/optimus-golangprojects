package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	appconfig "stash.ezesoft.net/imsacnt/ibor-transaction-master/src"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/kinesislistener"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/kinesisutil"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/logger"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/models"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/workdistro"
)

var (
	assignments = make(chan *models.Assignment)
)

func healthHandler() {
	http.HandleFunc("/api/ibor/trans/master/v1/health", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "OK")
	})
	addr := fmt.Sprintf(":%s", appconfig.Port)
	logger.Log.Infof("listen on%s", addr)
	logger.Log.Error(http.ListenAndServe(addr, nil))
}

func main() {
	go healthHandler()
	k := kinesisutil.NewKinesisUtil()
	kinesislistener.Start(assignments, k, logger.Log)
	workdistro.ListenWDAssignments(assignments, logger.Log)
	defer close(assignments)
	ch := make(chan os.Signal)
	signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)
	log.Println(<-ch)
}
