// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import credentials "github.com/aws/aws-sdk-go/aws/credentials"

import mock "github.com/stretchr/testify/mock"

// AwsCreds is an autogenerated mock type for the AwsCreds type
type AwsCreds struct {
	mock.Mock
}

// NewStaticCredentials provides a mock function with given fields: id, secret, token
func (_m *AwsCreds) NewStaticCredentials(id string, secret string, token string) *credentials.Credentials {
	ret := _m.Called(id, secret, token)

	var r0 *credentials.Credentials
	if rf, ok := ret.Get(0).(func(string, string, string) *credentials.Credentials); ok {
		r0 = rf(id, secret, token)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*credentials.Credentials)
		}
	}

	return r0
}
