package models

// PositionTag position tag
// swagger:model positionTag
type PositionTag struct {

	// index attribute Id
	// Required: true
	IndexAttributeID *int64 `json:"IndexAttributeId"`

	// index attribute value Id
	// Required: true
	IndexAttributeValueID *int64 `json:"IndexAttributeValueId"`
}

// Allocation allocation
// swagger:model allocation
type Allocation struct {

	// base currency security Id
	// Required: true
	BaseCurrencySecurityID *int64 `json:"BaseCurrencySecurityId"`

	// base to local fx rate
	// Required: true
	BaseToLocalFxRate *float64 `json:"BaseToLocalFxRate"`

	// book type Id
	// Required: true
	BookTypeID *int64 `json:"BookTypeId"`

	// commission
	Commission float64 `json:"Commission,omitempty"`

	// custodian counterparty account Id
	CustodianCounterpartyAccountID int64 `json:"CustodianCounterpartyAccountId,omitempty"`

	// exec quantity
	ExecQuantity int64 `json:"ExecQuantity,omitempty"`

	// executing broker counterparty Id
	ExecutingBrokerCounterpartyID int64 `json:"ExecutingBrokerCounterpartyId,omitempty"`

	// fees
	Fees float64 `json:"Fees,omitempty"`

	// is finalized
	IsFinalized bool `json:"IsFinalized,omitempty"`

	// net amount
	NetAmount float64 `json:"NetAmount,omitempty"`

	// portfolio Id
	// Required: true
	PortfolioID *int64 `json:"PortfolioId"`

	// position state ID
	PositionStateID int64 `json:"PositionStateID,omitempty"`

	// position tags
	PositionTags []*PositionTag `json:"PositionTags"`

	// principal amount
	PrincipalAmount float64 `json:"PrincipalAmount,omitempty"`

	// quantity
	// Required: true
	Quantity *int64 `json:"Quantity"`

	// route Id
	RouteID int64 `json:"RouteId,omitempty"`

	// route name
	RouteName string `json:"RouteName,omitempty"`

	// settle amount
	SettleAmount float64 `json:"SettleAmount,omitempty"`

	// settle currency security Id
	SettleCurrencySecurityID int64 `json:"SettleCurrencySecurityId,omitempty"`

	// settle price
	SettlePrice float64 `json:"SettlePrice,omitempty"`

	// settle to base f x rate
	SettleToBaseFXRate float64 `json:"SettleToBaseFXRate,omitempty"`

	// settle to local f x rate
	SettleToLocalFXRate float64 `json:"SettleToLocalFXRate,omitempty"`

	// settle to system f x rate
	SettleToSystemFXRate float64 `json:"SettleToSystemFXRate,omitempty"`
}
