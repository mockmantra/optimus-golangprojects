package models

// ImpactAllocation impactAllocation
// swagger:model impactAllocation
type ImpactAllocation struct {

	// base currency security Id
	// Required: true
	BaseCurrencySecurityID *int64 `json:"BaseCurrencySecurityId"`

	// base to local fx rate
	// Required: true
	BaseToLocalFxRate *float64 `json:"BaseToLocalFxRate"`

	// book type Id
	// Required: true
	BookTypeID *int64 `json:"BookTypeId"`

	// commission
	Commission float64 `json:"Commission,omitempty"`

	// custodian counterparty account Id
	CustodianCounterpartyAccountID int64 `json:"CustodianCounterpartyAccountId,omitempty"`

	// executing broker counterparty Id
	ExecutingBrokerCounterpartyID int64 `json:"ExecutingBrokerCounterpartyId,omitempty"`

	// fees
	Fees float64 `json:"Fees,omitempty"`

	// net amount
	NetAmount float64 `json:"NetAmount,omitempty"`

	// portfolio Id
	// Required: true
	PortfolioID *int64 `json:"PortfolioId"`

	// position state Id
	// Required: true
	PositionStateID *int64 `json:"PositionStateId"`

	// position tags
	PositionTags []*PositionTag `json:"PositionTags"`

	// principal amount
	PrincipalAmount float64 `json:"PrincipalAmount,omitempty"`

	// quantity
	// Required: true
	Quantity *int64 `json:"Quantity"`

	// route Id
	RouteID int64 `json:"RouteId,omitempty"`

	// route name
	RouteName string `json:"RouteName,omitempty"`

	// settle amount
	SettleAmount float64 `json:"SettleAmount,omitempty"`

	// settle currency security Id
	SettleCurrencySecurityID int64 `json:"SettleCurrencySecurityId,omitempty"`

	// settle price
	SettlePrice float64 `json:"SettlePrice,omitempty"`

	// settle to base f x rate
	SettleToBaseFXRate float64 `json:"SettleToBaseFXRate,omitempty"`

	// settle to local f x rate
	SettleToLocalFXRate float64 `json:"SettleToLocalFXRate,omitempty"`

	// settle to system f x rate
	SettleToSystemFXRate float64 `json:"SettleToSystemFXRate,omitempty"`
}
