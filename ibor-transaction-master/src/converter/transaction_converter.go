// Package converter provides functionality for converting Transaction Processor Transactions into
// Impact Generator Transactions. The important differences between Transaction Master and Transaction
// Processor are the additional requirements of the Transaction Processor:
// - Transaction Processor requires Allocations to be broken out by Position State (PositionStateID)
// - Transaction Processor requires Allocations to contain a break-down for the Net Book (BookTypeID = 1)
package converter

import (
	"context"

	"stash.ezesoft.net/imsacnt/accounting-utils/enums/booktype"
	"stash.ezesoft.net/imsacnt/accounting-utils/enums/positionstate"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/converter/models"
	"stash.ezesoft.net/ipc/ezelogger"
)

type allocKey struct {
	PortfolioID                   int64
	ExecutingBrokerCounterpartyID int64
}

// TransactionConverter is a type of Converter
type TransactionConverter struct {
	Context context.Context
	Logger  *ezelogger.EzeLog
}

// TransactionConverterInstance - Returns an instance of Converter struct
func TransactionConverterInstance(context context.Context, logger *ezelogger.EzeLog) *TransactionConverter {
	return &TransactionConverter{
		Context: context,
		Logger:  logger,
	}
}

// ConvertToImpactGeneratorTransaction will take a Transaction Processor Transaction and convert it
// into a Impact Generator Transaction. If the Transaction is from Trading, the partially filled
// allocations will be broken out into two allocations to meet the Impact Generator specifications.
// If the Transaction is missing Net Allocations, these will be also be generated.
func (tc *TransactionConverter) ConvertToImpactGeneratorTransaction(transaction *models.Transaction) *models.ImpactTransaction {
	// Allocate to the Net Book by rolling up the Location Book (if required)
	tc.tryAllocateNetBook(transaction)

	tc.Logger.Infof("Processing Stage - Converting Transaction into Impact Transaction")
	// Convert the Transaction into an Impact Transaction
	impGenTrans := createImpactGeneratorTransaction(transaction)

	// Convert the Allocations into Impact Allocations
	var impGenAllocs []*models.ImpactAllocation
	for _, alloc := range transaction.Allocations {
		if *transaction.SourceSystemName == "IMS-TRADING" && alloc.ExecQuantity != 0 && *alloc.Quantity != alloc.ExecQuantity {
			partialAllocs := breakoutPartialAllocation(alloc)
			impGenAllocs = append(impGenAllocs, partialAllocs...)
		} else {
			impGenAlloc := createImpactGeneratorAllocation(alloc)
			positionStateID := calculatePositionStateID(alloc)
			impGenAlloc.PositionStateID = positionStateID
			impGenAllocs = append(impGenAllocs, impGenAlloc)
		}

		impGenTrans.Allocations = impGenAllocs
	}

	return impGenTrans
}

// tryAllocateNetBook will take a Transaction Processor Transaction and, if the Net Allocation
// is missing, will generate Net Book Allocations based on the current Location  Book Allocations. The Quantity and
func (tc *TransactionConverter) tryAllocateNetBook(transaction *models.Transaction) {
	allocate := true
	for _, alloc := range transaction.Allocations {
		if *alloc.BookTypeID == booktype.Net {
			allocate = false
			break
		}
	}

	if allocate {
		tc.Logger.Infof("Processing Stage - Generating Allocations for the Net Book")
		var allocMap map[allocKey]*models.Allocation
		var allocKeys []allocKey
		allocMap = make(map[allocKey]*models.Allocation)

		for _, alloc := range transaction.Allocations {
			if *alloc.BookTypeID == booktype.Location {
				allocKey := allocKey{*alloc.PortfolioID, alloc.ExecutingBrokerCounterpartyID}
				curNetAlloc, ok := allocMap[allocKey]

				if ok == false {
					allocKeys = append(allocKeys, allocKey)
					newNetAlloc := createNetAlloc(alloc)
					allocMap[allocKey] = &newNetAlloc
				} else {
					updateNetAlloc(curNetAlloc, alloc)
				}
			}
		}

		for _, key := range allocKeys {
			transaction.Allocations = append(transaction.Allocations, allocMap[key])
		}
	}
}

// createNetAlloc will create a new Net Allocation
func createNetAlloc(alloc *models.Allocation) models.Allocation {
	netAlloc := *alloc

	var netBookID int64 = booktype.Net
	netAlloc.BookTypeID = &netBookID
	netAlloc.CustodianCounterpartyAccountID = 0

	return netAlloc
}

// updateNetAlloc will modify the Quantity, Cost, Price and FX Rate
// fields on the provided Net Allocations based on the Location
// Allocation.  Quantity and Cost fields will be summed.
// Price will be calculated using a weighted average.
func updateNetAlloc(netAlloc *models.Allocation, locAlloc *models.Allocation) {
	var quantity = *netAlloc.Quantity + *locAlloc.Quantity
	netAlloc.Quantity = &quantity
	netAlloc.SettleAmount = netAlloc.SettleAmount + locAlloc.SettleAmount
	netAlloc.PrincipalAmount = netAlloc.PrincipalAmount + locAlloc.PrincipalAmount
	netAlloc.NetAmount = netAlloc.NetAmount + locAlloc.NetAmount
	netAlloc.Commission = netAlloc.Commission + locAlloc.Commission
	netAlloc.Fees = netAlloc.Fees + locAlloc.Fees
	netAlloc.SettlePrice = netAlloc.PrincipalAmount / float64(*netAlloc.Quantity)
}

// breakoutPartialAllocation will break-out a partially filled Allocation into
// two Allocations by creating an Allocation for the Filled and Unfilled portions.
// The Quantities assigned will be based on the difference between the ExecQuantity
// and Quantity fields.
func breakoutPartialAllocation(allocation *models.Allocation) []*models.ImpactAllocation {
	var breakoutAllocs []*models.ImpactAllocation

	releasedAlloc := models.ImpactAllocation{
		BaseCurrencySecurityID:         allocation.BaseCurrencySecurityID,
		BaseToLocalFxRate:              allocation.BaseToLocalFxRate,
		BookTypeID:                     allocation.BookTypeID,
		CustodianCounterpartyAccountID: allocation.CustodianCounterpartyAccountID,
		PortfolioID:                    allocation.PortfolioID,
		PositionTags:                   allocation.PositionTags,
		Quantity:                       allocation.Quantity,
	}

	var releasedPositionStateID int64 = positionstate.ReleasedToMarket
	releasedAlloc.PositionStateID = &releasedPositionStateID
	releasedQuantity := *allocation.Quantity - allocation.ExecQuantity
	releasedAlloc.Quantity = &releasedQuantity

	filledAlloc := createImpactGeneratorAllocation(allocation)
	var filledPositionStateID int64 = positionstate.FillReceived
	filledAlloc.PositionStateID = &filledPositionStateID
	filledQuantity := allocation.ExecQuantity
	filledAlloc.Quantity = &filledQuantity

	breakoutAllocs = append(breakoutAllocs, &releasedAlloc, filledAlloc)

	return breakoutAllocs
}

// createImpactGeneratorTransaction will create an Impact Generator
// Transaction from a Transaction Processor transaction
func createImpactGeneratorTransaction(transaction *models.Transaction) *models.ImpactTransaction {
	return &models.ImpactTransaction{
		BusinessDateTimeUTC:      transaction.BusinessDateTimeUTC,
		EventTypeID:              transaction.EventTypeID,
		LocalCurrencySecurityID:  transaction.LocalCurrencySecurityID,
		OrderCreatedDate:         transaction.OrderCreatedDate,
		OrderQuantity:            transaction.OrderQuantity,
		SecurityID:               transaction.SecurityID,
		SecurityTemplateID:       transaction.SecurityTemplateID,
		SequenceNumber:           transaction.SequenceNumber,
		SettleDate:               transaction.SettleDate,
		SourceSystemName:         transaction.SourceSystemName,
		SourceSystemReference:    transaction.SourceSystemReference,
		SystemCurrencySecurityID: transaction.SystemCurrencySecurityID,
		SystemToLocalFXRate:      transaction.SystemToLocalFXRate,
		TradeDate:                transaction.TradeDate,
		TransactionDateTimeUTC:   transaction.TransactionDateTimeUTC,
		TransactionTimeZoneID:    transaction.TransactionTimeZoneID,
	}
}

// createImpactGeneratorAllocation will create an Impact Generator
// Allocation from a Transaction Processor Allocation
func createImpactGeneratorAllocation(allocation *models.Allocation) *models.ImpactAllocation {
	return &models.ImpactAllocation{
		BaseCurrencySecurityID:         allocation.BaseCurrencySecurityID,
		BaseToLocalFxRate:              allocation.BaseToLocalFxRate,
		BookTypeID:                     allocation.BookTypeID,
		Commission:                     allocation.Commission,
		CustodianCounterpartyAccountID: allocation.CustodianCounterpartyAccountID,
		ExecutingBrokerCounterpartyID:  allocation.ExecutingBrokerCounterpartyID,
		Fees:                           allocation.Fees,
		NetAmount:                      allocation.NetAmount,
		PortfolioID:                    allocation.PortfolioID,
		PositionTags:                   allocation.PositionTags,
		PrincipalAmount:                allocation.PrincipalAmount,
		Quantity:                       allocation.Quantity,
		SettleAmount:                   allocation.SettleAmount,
		SettleCurrencySecurityID:       allocation.SettleCurrencySecurityID,
		SettlePrice:                    allocation.SettlePrice,
		SettleToBaseFXRate:             allocation.SettleToBaseFXRate,
		SettleToLocalFXRate:            allocation.SettleToLocalFXRate,
		SettleToSystemFXRate:           allocation.SettleToSystemFXRate,
	}
}

// calculatePositionStateID will calculate the PositionStateID for a Impact Generator
// Allocation based on the Allocation properties
func calculatePositionStateID(allocation *models.Allocation) *int64 {
	var positionStateID int64

	if allocation.ExecutingBrokerCounterpartyID == 0 {
		positionStateID = positionstate.Proposed
	} else if allocation.ExecQuantity == 0 {
		positionStateID = positionstate.ReleasedToMarket
	} else if !allocation.IsFinalized {
		positionStateID = positionstate.FillReceived
	} else {
		positionStateID = positionstate.Finalized
	}

	return &positionStateID
}
