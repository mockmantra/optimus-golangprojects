package converter

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/converter/models"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/logger"
	"stash.ezesoft.net/ipc/ezelogger"

	"github.com/go-test/deep"
)

// convertToImpactGeneratorTransactionTests contains the list of test definitions
// that will be executed by the test. The input and output files for these tests can
// found in the testFiles folder.
var convertToImpactGeneratorTransactionTests = []struct {
	name string
}{
	{
		"Buy_Trading_Proposed_SinglePortfolio_SingleLocation_SingleStrategy",
	},
	{
		"Buy_Trading_Proposed_TwoPortfolio_SingleLocation_SingleStrategy",
	},
	{
		"Buy_Trading_Proposed_SinglePortfolio_SingleLocation_TwoStrategy",
	},
	{
		"Buy_Trading_Proposed_SinglePortfolio_TwoLocation_SingleStrategy",
	},
	{
		"Buy_Trading_PartialReleasedToMarket_SingleRoute_SinglePortfolio_SingleLocation_SingleStrategy",
	},
	{
		"Buy_Trading_ReleasedToMarket_SingleRoute_SinglePortfolio_SingleLocation_SingleStrategy",
	},
	{
		"Buy_Trading_ReleasedToMarket_TwoRoute_SinglePortfolio_SingleLocation_SingleStrategy",
	},
	{
		"Buy_Trading_PartialFill_SingleRoute_SinglePortfolio_SingleLocation_SingleStrategy",
	},
	{
		"Buy_Trading_PartialFill_TwoRoute_SinglePortfolio_SingleLocation_SingleStrategy",
	},
	{
		"Buy_Trading_FillReceived_SingleRoute_SinglePortfolio_SingleLocation_SingleStrategy",
	},
	{
		"Buy_Trading_FillReceived_TwoRoute_SinglePortfolio_SingleLocation_SingleStrategy",
	},
	{
		"Buy_Trading_Finalized_SingleRoute_SinglePortfolio_SingleLocation_SingleStrategy",
	},
	{
		"Buy_Trading_Finalized_TwoRoute_SinglePortfolio_SingleLocation_SingleStrategy",
	},
	{
		"Sell_Trading_Proposed_SinglePortfolio_SingleLocation_SingleStrategy",
	},
	{
		"Short_Trading_Proposed_SinglePortfolio_SingleLocation_SingleStrategy",
	},
	{
		"Cover_Trading_Proposed_SinglePortfolio_SingleLocation_SingleStrategy",
	},
}

// update is used to control whether the expected output files are updated when
// the converter tests are run.
var update = flag.Bool("update", false, "Update Expected Test Output Files")

type AllMocks struct {
	Context context.Context
	Logger  *ezelogger.EzeLog
}

func TestConvertToImpactGeneratorTransaction(t *testing.T) {
	for _, tt := range convertToImpactGeneratorTransactionTests {
		t.Run(tt.name, func(t *testing.T) {
			// Load Input
			var input models.Transaction
			inputBytes := loadTestInputFile("Input_" + tt.name)
			json.Unmarshal(inputBytes, &input)

			// Load Expected Output
			var output models.ImpactTransaction
			outputBytes := loadTestInputFile("Output_" + tt.name)
			json.Unmarshal(outputBytes, &output)

			allMocks := new(AllMocks)
			allMocks.Context = context.TODO()
			allMocks.Logger = logger.Log.WithContext(allMocks.Context)
			tc := TransactionConverterInstance(allMocks.Context, allMocks.Logger)
			res := tc.ConvertToImpactGeneratorTransaction(&input)

			if *update {
				updateTestOutputFile("Output_"+tt.name, res)
			} else {
				if diff := deep.Equal(*res, output); diff != nil {
					t.Error(diff)
				}
			}
		})
	}
}

var calculatePositionStateIDTests = []struct {
	in  *models.Allocation
	out int64
}{
	{&models.Allocation{
		ExecutingBrokerCounterpartyID: 0,
		ExecQuantity:                  0,
		IsFinalized:                   false,
	}, 10},
	{&models.Allocation{
		ExecutingBrokerCounterpartyID: 1,
		ExecQuantity:                  0,
		IsFinalized:                   false,
	}, 20},
	{&models.Allocation{
		ExecutingBrokerCounterpartyID: 1,
		ExecQuantity:                  100,
		IsFinalized:                   false,
	}, 30},
	{&models.Allocation{
		ExecutingBrokerCounterpartyID: 1,
		ExecQuantity:                  100,
		IsFinalized:                   true,
	}, 40},
}

func TestCalculatePositionStateID(t *testing.T) {
	for _, tt := range calculatePositionStateIDTests {
		t.Run("", func(t *testing.T) {
			res := calculatePositionStateID(tt.in)

			if *res != tt.out {
				t.Errorf("Expected %d, but returned %d", tt.out, *res)
			}
		})
	}
}

func loadTestInputFile(fileName string) []byte {
	filePath := "testFiles/" + fileName + ".json"
	jsonFile, err := os.Open(filePath)
	if err != nil {
		fmt.Println(err)
	}

	bytes, _ := ioutil.ReadAll(jsonFile)

	jsonFile.Close()

	return bytes
}

func updateTestOutputFile(fileName string, result interface{}) {
	filePath := "testFiles/" + fileName + ".json"
	resultJSON, _ := json.Marshal(result)
	_ = ioutil.WriteFile(filePath, resultJSON, 0644)
}
