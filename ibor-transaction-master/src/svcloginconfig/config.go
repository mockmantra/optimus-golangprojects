package svcloginconfig

import (
	"encoding/json"
	"fmt"
	"os"
	"time"

	"github.com/pkg/errors"
	utilsModels "stash.ezesoft.net/imsacnt/accounting-utils/models"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/logger"
)

const (
	rancherHostKey            = "RANCHER_HOST_NAME"
	rancherContainerKey       = "RANCHER_CONTAINER_NAME"
	baseUrlKey                = "IMS_BASE_URL"
	intiServiceUserKey        = "IMS_INTI_SVC_USER"
	awsRegionKey              = "AWS_REGION"
	consulUrlKey              = "CONSUL_URL"
	tenantSiloDefault         = "TenantSiloBlue"
	consulSettingRoot         = "ims/Settings/Blue/Configuration/"
	EnvironmentNameKey        = "EnvironmentName"
	SharedKeyKey              = "ServiceLoginSharedKey"
	DPermsKeyKey              = "DPermsConfigApplicationKey"
	DPermsVersionKey          = "DPermsConfigApplicationVersion"
	DPermsUriKey              = "DPermsConfigDPermsRestUri"
	ClusterHostSettingsKey    = "ClusterHostSettings"
	RabbitMQBrokerUserKey     = "RabbitMQBrokerUser"
	RabbitMQBrokerPasswordKey = "RabbitMQBrokerPassword"
)

var (
	Host                string
	Container           string
	Instance            string
	IntiServiceUser     string
	BaseUrl             string
	AWSRegion           string
	ConsulUrl           string
	LogStatPeriod       = time.Minute * 5
	consulMaxRetries    = 6
	consulRetryDelaySec = time.Second * 5
	ServiceSessionVars  *ServiceSessionConfig
	RestApiAssigments   bool
	RmqVars             *utilsModels.RmqConfig
)

//ClusterHostSettings values is [ { \"Host\": \"aecastle01rmq-elb.awsdev.ezesoftcloud.com\", \"Port\": 5672 } ]
var (
	consulVars map[string]interface{} = map[string]interface{}{
		EnvironmentNameKey:        "",
		SharedKeyKey:              "",
		DPermsKeyKey:              "",
		DPermsVersionKey:          "",
		DPermsUriKey:              "",
		ClusterHostSettingsKey:    "",
		RabbitMQBrokerUserKey:     "",
		RabbitMQBrokerPasswordKey: "",
	}
)

type ServiceSessionConfig struct {
	EnvName       string
	SharedKey     string
	DpermsIdKey   string
	DpermsVersion string
	DpermsUri     string
}

type RmqHostInfo struct {
	Host string `json:"Host"`
	Port int    `json:"Port"`
}

func init() {
	Host = os.Getenv(rancherHostKey)
	Container = os.Getenv(rancherContainerKey)
	Instance, _ = os.Hostname()
	BaseUrl = os.Getenv(baseUrlKey)
	if BaseUrl == "" {
		panic(fmt.Sprintf("environment variable '%s' should be set to non-empty value", baseUrlKey))
	}
	if BaseUrl[len(BaseUrl)-1:] != "/" {
		BaseUrl += "/"
	}
	IntiServiceUser = os.Getenv(intiServiceUserKey)
	if IntiServiceUser == "" {
		panic(fmt.Sprintf("environment variable '%s' should be set to non-empty value", intiServiceUserKey))
	}
	AWSRegion = os.Getenv(awsRegionKey)
	if AWSRegion == "" {
		panic(fmt.Sprintf("environment variable '%s' should be set to non-empty value", awsRegionKey))
	}

	// this should be set
	ConsulUrl = os.Getenv(consulUrlKey)
	if ConsulUrl == "" {
		ConsulUrl = "http://consul-agent:8500"

	}
	// RestApiAssigments, _ = strconv.ParseBool(os.Getenv(restApiAssignmentsEnvKey))

	// we don't set these...so it uses GetServiceSessionConfig() to fetch them
	envName := os.Getenv("CL_ENVIRONMENT_NAME")
	sharedKey := os.Getenv("CL_SHAREDKEY")
	dpermsAppKey := os.Getenv("CL_DPERMSAPPKEY")
	dpermsAppVersion := os.Getenv("CL_DPERMSAPPVERSION")
	dpermsRestUri := os.Getenv("CL_DPERMSRESTURI")
	rmqBrokerHostPort := os.Getenv("CL_RMQ_BROKER_PORT")
	rmqUser := os.Getenv("CL_RMQ_USER")
	rmqPassword := os.Getenv("CL_RMQ_PASSWORD")

	if rmqBrokerHostPort != "" && rmqUser != "" && rmqPassword != "" {
		RmqVars = &utilsModels.RmqConfig{
			HostPort: rmqBrokerHostPort,
			User:     rmqUser,
			Password: rmqPassword,
			Silo:     tenantSiloDefault,
		}
	}

	if envName == "" || sharedKey == "" || dpermsAppKey == "" ||
		dpermsAppVersion == "" || dpermsRestUri == "" {
		loadConsulVars(consulVars)
		var err error
		if RmqVars == nil {
			RmqVars, err = getRmqConfig()
			if err != nil {
				panic(fmt.Sprintf("failed to get rmq config from consul %v", err))
			}
		}
		ServiceSessionVars, err = GetServiceSessionConfig()
		if err != nil {
			panic(fmt.Sprintf("failed to get service session config from consul %v", err))
		}
	} else {
		ServiceSessionVars = &ServiceSessionConfig{
			EnvName:       envName,
			SharedKey:     sharedKey,
			DpermsIdKey:   dpermsAppKey,
			DpermsVersion: dpermsAppVersion,
			DpermsUri:     dpermsRestUri,
		}
	}

	RmqVars.RancherHost = Host
	RmqVars.RancherContainer = Container
}

func loadConsulVars(consulMap map[string]interface{}) {
	var err error
	for i := 0; i <= consulMaxRetries; i++ {
		err = updateConsulSettings(consulMap)
		if err == nil {
			break
		} else {
			logger.Log.Warnf("Failed reading consul variables, retrying %v", i+1)
		}
		<-time.After(consulRetryDelaySec)
	}
	if err != nil {
		panic(fmt.Sprintf("Failed reading consul variables %v %v", ConsulUrl, err.Error()))
	}
}

// getRmqConfig gets a key from consul in silo - silo not used because it is implied by the consul agent
func getRmqConfig() (*utilsModels.RmqConfig, error) {
	hv, err := GetConsulValue(ClusterHostSettingsKey)
	if err != nil {
		return nil, err
	}
	s, ok := hv.(string)
	if !ok {
		return nil, errors.Errorf("invalid interface conversion for consul key %s", ClusterHostSettingsKey)
	}
	var hi []RmqHostInfo
	err = json.Unmarshal([]byte(s), &hi)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to unmartial json for consul key %s", ClusterHostSettingsKey)
	}
	if len(hi) == 0 {
		return nil, errors.Errorf("no value at consul key %s", ClusterHostSettingsKey)
	}
	if hi[0].Host == "" {
		return nil, errors.Errorf("empty value found for consul key %s Host", ClusterHostSettingsKey)
	}
	if hi[0].Port == 0 {
		return nil, errors.Errorf("0 value found for consul key %s Port", ClusterHostSettingsKey)
	}
	user, err := GetConsulValueStr(RabbitMQBrokerUserKey)
	if err != nil {
		return nil, err
	}
	if user == "" {
		return nil, errors.Errorf("empty value found for consul key %s", RabbitMQBrokerUserKey)
	}
	pwd, err := GetConsulValueStr(RabbitMQBrokerPasswordKey)
	if err != nil {
		return nil, err
	}
	if pwd == "" {
		return nil, errors.Errorf("empty value found for consul key %s", RabbitMQBrokerPasswordKey)
	}
	return &utilsModels.RmqConfig{HostPort: fmt.Sprintf("%s:%d", hi[0].Host, hi[0].Port), User: user, Password: pwd, Silo: tenantSiloDefault}, nil
}

func GetConsulValue(key string) (interface{}, error) {
	setting, ok := consulVars[key]
	if !ok {
		return "", errors.Errorf("did not find consul setting for %s", key)
	}
	return setting, nil
}

func GetConsulValueStr(key string) (string, error) {
	setting, ok := consulVars[key]
	if !ok {
		return "", errors.Errorf("did not find consul setting for %s", key)
	}
	val, ok := setting.(string)
	if !ok {
		return "", errors.Errorf("invalid interface conversion value for consul key %s", key)
	}
	return trimQuotes(val), nil
}

func GetServiceSessionConfig() (*ServiceSessionConfig, error) {

	envName, err := GetConsulValueStr(EnvironmentNameKey)
	if err != nil {
		return nil, err
	}
	if envName == "" {
		return nil, errors.Errorf("empty value found for consul key %s", EnvironmentNameKey)
	}
	sharedKey, err := GetConsulValueStr(SharedKeyKey)
	if err != nil {
		return nil, err
	}
	if sharedKey == "" {
		return nil, errors.Errorf("empty value found for consul key %s", SharedKeyKey)
	}
	dpermsKey, err := GetConsulValueStr(DPermsKeyKey)
	if err != nil {
		return nil, err
	}
	if dpermsKey == "" {
		return nil, errors.Errorf("empty value found for consul key %s", DPermsKeyKey)
	}
	dpermsVersion, err := GetConsulValueStr(DPermsVersionKey)
	if err != nil {
		return nil, err
	}
	if dpermsVersion == "" {
		return nil, errors.Errorf("empty value found for consul key %s", DPermsVersionKey)
	}
	dpermsUri, err := GetConsulValueStr(DPermsUriKey)
	if err != nil {
		return nil, err
	}
	if dpermsUri == "" {
		return nil, errors.Errorf("empty value found for consul key %s", DPermsUriKey)
	}
	return &ServiceSessionConfig{EnvName: envName, SharedKey: sharedKey, DpermsIdKey: dpermsKey, DpermsVersion: dpermsVersion,
		DpermsUri: dpermsUri}, nil
}
