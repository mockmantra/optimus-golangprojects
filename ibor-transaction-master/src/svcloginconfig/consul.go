package svcloginconfig

import (
	"encoding/json"

	consul "github.com/hashicorp/consul/api"
	"github.com/pkg/errors"
)

func trimQuotes(s string) string {
	if len(s) >= 2 {
		if s[0] == '"' && s[len(s)-1] == '"' {
			return s[1 : len(s)-1]
		}
	}
	return s
}

// UpdateConsulSettings ... most of this code was copied from work-distribution project.
func updateConsulSettings(consulMap map[string]interface{}) error {
	conf := consul.DefaultConfig()
	conf.Address = ConsulUrl
	client, err := consul.NewClient(conf)

	if err != nil {
		return errors.Wrapf(err, "failed to initiate consul client %v", ConsulUrl)
	}
	kv := client.KV()
	var pairs consul.KVPairs
	pairs, _, err = kv.List(consulSettingRoot, nil)
	if err != nil {
		return errors.Wrapf(err, "read consul, length of pairs is %d\n", len(pairs))
	}

	for _, pair := range pairs { // go over all the keys and find the ones we are looking for
		key := pair.Key[len(consulSettingRoot):] // naked key, without the root
		if _, ok := consulMap[key]; ok {
			s := string(pair.Value)
			var vi interface{}
			err := json.Unmarshal([]byte(s), &vi)
			if err != nil {
				return errors.Errorf("error reading value for key %s%s", consulSettingRoot, key)
			}
			consulMap[key] = vi
		}
	}
	return nil
}
