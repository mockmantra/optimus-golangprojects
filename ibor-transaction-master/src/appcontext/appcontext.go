package appcontext

import (
	"context"

	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/interfaces"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/logger"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/models"
	"stash.ezesoft.net/ipc/ezeutils"
)

// GetLoggerWithCtx the logger with the context
var GetLoggerWithCtx = func(ctx context.Context) interfaces.ILogger {
	logger.Log = logger.Log.WithContext(ctx)
	return logger.Log
}

// GetFromKinesisMessage returns the context
func GetFromKinesisMessage(ctx context.Context, streamMessage *models.KinStreamMessage) context.Context {
	ctx = ezeutils.WithKeyValue(ctx, ezeutils.XRequestIdKey, streamMessage.ActivityID)       // Set ActivityId
	ctx = ezeutils.WithKeyValue(ctx, ezeutils.UserNameKey, streamMessage.UserName)           // Set UserName
	ctx = ezeutils.WithKeyValue(ctx, ezeutils.FirmAuthTokenKey, streamMessage.FirmAuthToken) // Set FirmAuthToken
	return ctx
}
