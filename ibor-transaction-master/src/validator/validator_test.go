package validator

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	appconfig "stash.ezesoft.net/imsacnt/ibor-transaction-master/src"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/httputil"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/mocks"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/models"
)

var validatorValidTestJSON = `{
	"Allocations": [
		{
			"BookTypeId": 2,
			"RouteId": 1,
			"Quantity": 100,
			"PortfolioId": 3
		}
	],
	"EventTypeId": 1013
  }`

var validatorPanicTestJSON = `{
	"SystemCurrencySecurityID": 1234,
	"EventTypeId": 1013,
	"Allocations":null
  }`
var validatorPanicAllocationTestJSON = `{
	"Allocations": [
		{
			"BookTypeId": 2,
			"RouteId": 1,
			"Quantity": 100,
			"PortfolioId": 3,
			"ExecQuantity":10,
			"SettleCurrencySecurityID":1
		}
	],
	"EventTypeId": 1013
  }`

type allMocks struct {
	logger mocks.Logger
}

func TestValidateValidData(t *testing.T) {
	var validatorTestData models.Transaction
	var allmocks allMocks
	log := &allmocks.logger
	httputil.UserSessionToken = "123"
	unmarshalErr := json.Unmarshal([]byte(validatorValidTestJSON), &validatorTestData)
	assert.Nil(t, unmarshalErr)
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	}))
	appconfig.BaseURI = ts.URL
	defer ts.Close()
	log.On("Errorf", mock.Anything, mock.Anything).Return()
	errors := Validate(validatorTestData, log)
	errs := []ErrorMessage{ErrorMessage{Field: "SourceSystemReference",
		Message: []string{"Invalid SourceSystemReference provided: "}},
		ErrorMessage{Field: "TransactionDateTimeUTC", Message: []string{"Invalid TransactionDateTimeUTC. Incorrect format"}},
		ErrorMessage{Field: "OrderQuantity", Message: []string{"Invalid OrderQuantity. The sum of Quantities for Book Type 2: 100 does not match OrderQuantity: 0"}}, ErrorMessage{Field: "SettleDate",
			Message: []string{"Invalid SettleDate. Invalid format"}},
		ErrorMessage{Field: "SystemToLocalFXRate",
			Message: []string{"Invalid SystemToLocalFXRate. The SystemCurrencySecurityID and LocalCurrencySecurityID are equal but the value of SystemToLocalFXRate is 0 instead of 1"}},
		ErrorMessage{Field: "TradeDate", Message: []string{"Invalid TradeDate. Invalid format"}}, ErrorMessage{Field: "EventTypeId", Message: []string{"EventTypeId : Service lookup failed"}},
		ErrorMessage{Field: "BusinessDateTimeUTC", Message: []string{"BusinessDateTimeUTC : Service lookup failed"}},
		ErrorMessage{Field: "PortfolioId", Message: []string{"Invalid PortfolioId: 3 does not exist"}},
		ErrorMessage{Field: "TransactionTimeZoneId", Message: []string{"Invalid TransactionTimeZoneId. TimeZoneId: 0 does not exist"}},
		ErrorMessage{Field: "LocalCurrencySecurityId", Message: []string{"Invalid LocalCurrencySecurityId. LocalCurrencySecurityId 0  does not exist"}},
		ErrorMessage{Field: "SecurityId", Message: []string{"Invalid SecurityId. SecurityId: 0 does not exist"}}, ErrorMessage{Field: "SecurityTemplateId",
			Message: []string{"Invalid SecurityTemplateId. SecurityTemplateId: 0 does not exist"}},
		ErrorMessage{Field: "UserTimeZoneId", Message: []string{"Invalid UserTimeZoneId. TimeZoneId: 0 does not exist"}}, ErrorMessage{Field: "SystemCurrencySecurityId",
			Message: []string{"Invalid SystemCurrencySecurityId. SystemCurrencySecurityId 0  does not exist"}}}
	assert.ElementsMatch(t, errors, errs)
	log.AssertCalled(t, "Errorf", "Unable to fetch the securities %v", []int{0})
	httputil.UserSessionToken = ""
}

func TestPanicInAllocationFieldsValidation(t *testing.T) {
	var validatorTestData models.Transaction
	var allmocks allMocks
	log := &allmocks.logger
	httputil.UserSessionToken = "123"
	unmarshalErr := json.Unmarshal([]byte(validatorPanicAllocationTestJSON), &validatorTestData)
	assert.Nil(t, unmarshalErr)
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	}))
	appconfig.BaseURI = ts.URL
	defer ts.Close()
	log.On("Errorf", mock.Anything, mock.Anything).Return()

	errors := Validate(validatorTestData, log)
	errs := []ErrorMessage{
		ErrorMessage{Field: "SettleDate", Message: []string{"Invalid SettleDate. Invalid format"}},
		ErrorMessage{Field: "OrderQuantity", Message: []string{"Invalid OrderQuantity. The sum of Quantities for Book Type 2: 100 does not match OrderQuantity: 0"}},
		ErrorMessage{Field: "SourceSystemReference", Message: []string{"Invalid SourceSystemReference provided: "}},
		ErrorMessage{Field: "SystemToLocalFXRate", Message: []string{"Invalid SystemToLocalFXRate. The SystemCurrencySecurityID and LocalCurrencySecurityID are equal but the value of SystemToLocalFXRate is 0 instead of 1"}},
		ErrorMessage{Field: "TradeDate", Message: []string{"Invalid TradeDate. Invalid format"}},
		ErrorMessage{Field: "TransactionDateTimeUTC", Message: []string{"Invalid TransactionDateTimeUTC. Incorrect format"}},
		ErrorMessage{Field: "UserTimeZoneId", Message: []string{"Invalid UserTimeZoneId. TimeZoneId: 0 does not exist"}},
		ErrorMessage{Field: "BusinessDateTimeUTC", Message: []string{"BusinessDateTimeUTC : Service lookup failed"}},
		ErrorMessage{Field: "TransactionTimeZoneId", Message: []string{"Invalid TransactionTimeZoneId. TimeZoneId: 0 does not exist"}},
		ErrorMessage{Field: "SecurityId", Message: []string{"Invalid SecurityId. SecurityId: 0 does not exist"}},
		ErrorMessage{Field: "SecurityTemplateId", Message: []string{"Invalid SecurityTemplateId. SecurityTemplateId: 0 does not exist"}},
		ErrorMessage{Field: "SystemCurrencySecurityId", Message: []string{"Invalid SystemCurrencySecurityId. SystemCurrencySecurityId 0  does not exist"}},
		ErrorMessage{Field: "PortfolioId", Message: []string{"Invalid PortfolioId: 3 does not exist"}},
		ErrorMessage{Field: "EventTypeId", Message: []string{"EventTypeId : Service lookup failed"}},
		ErrorMessage{Field: "LocalCurrencySecurityId", Message: []string{"Invalid LocalCurrencySecurityId. LocalCurrencySecurityId 0  does not exist"}},
		ErrorMessage{Field: "SettleCurrencySecurityId", Message: []string{"Invalid SettleCurrencySecurityId. Error fetching the SettleCurrencyID for PortfolioId: 3"}}}
	assert.ElementsMatch(t, errors, errs)
	log.AssertExpectations(t)
	log.AssertCalled(t, "Errorf", "Unable to fetch the securities %v", []int{0, 1})
	httputil.UserSessionToken = ""
}

// func TestPanicInMainFieldsValidation(t *testing.T) {
// 	var validatorTestData models.Transaction
// 	var allmocks allMocks
// 	log := &allmocks.logger
// 	httputil.UserSessionToken = "123"
// 	unmarshalErr := json.Unmarshal([]byte(validatorPanicTestJSON), &validatorTestData)
// 	assert.Nil(t, unmarshalErr)
// 	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
// 		_, err := w.Write(nil)
// 		if err != nil {
// 			t.Error("Error in writing to response")
// 		}
// 	}))
// 	appconfig.BaseURI = ts.URL
// 	defer ts.Close()
// 	monkey.Patch(models.NewSecurityRequest, func(securityID int) ([]models.Security, error) {
// 		return []models.Security{}, nil
// 	})
// 	log.On("Errorf", mock.Anything, mock.Anything, mock.Anything).Return()
// 	errors := Validate(validatorTestData, log)
// 	errs := []ErrorMessage{ErrorMessage{Field: "LocalCurrencySecurityId", Message: []string{"Failed to validate the field: LocalCurrencySecurityId"}}, ErrorMessage{Field: "SourceSystemReference", Message: []string{"Invalid SourceSystemReference. Not a valid integer value: "}}, ErrorMessage{Field: "TransactionDateTimeUTC", Message: []string{"Invalid TransactionDateTimeUTC. Incorrect format"}}, ErrorMessage{Field: "SettleDate", Message: []string{"Invalid SettleDate. Invalid format"}}, ErrorMessage{Field: "SystemCurrencySecurityId", Message: []string{"Failed to validate the field: SystemCurrencySecurityId"}}, ErrorMessage{Field: "SecurityId", Message: []string{"Failed to validate the field: SecurityId"}}, ErrorMessage{Field: "TradeDate", Message: []string{"Invalid TradeDate. Invalid format"}}, ErrorMessage{Field: "TransactionTimeZoneId", Message: []string{"TransactionTimeZoneId: Service lookup failed"}}, ErrorMessage{Field: "EventTypeId", Message: []string{"EventTypeId : Service lookup failed"}}, ErrorMessage{Field: "UserTimeZoneId", Message: []string{"UserTimeZoneId: Service lookup failed"}}, ErrorMessage{Field: "BusinessDateTimeUTC", Message: []string{"BusinessDateTimeUTC : Service lookup failed"}}, ErrorMessage{Field: "SecurityTemplateId", Message: []string{"Invalid SecurityTemplateId. Service lookup failed"}}}
// 	assert.ElementsMatch(t, errors, errs)
// 	log.AssertExpectations(t)
// 	httputil.UserSessionToken = ""

// }
