package kinesislistener

import (
	"context"
	"encoding/json"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/service/kinesis"
	"stash.ezesoft.net/ipc/work-distribution/workdistributionclient"

	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/handler"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/interfaces"

	tomb "gopkg.in/tomb.v2"
	appconfig "stash.ezesoft.net/imsacnt/ibor-transaction-master/src"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/models"
	"stash.ezesoft.net/ipc/ezeutils"
)

var tmb tomb.Tomb

// Start ...
func Start(assignments chan *models.Assignment, k interfaces.IKinesisUtil, logger interfaces.ILogger) {
	go listenToAssignment(assignments, k, logger)
}

func listenToAssignment(assignments chan *models.Assignment, k interfaces.IKinesisUtil, logger interfaces.ILogger) {
	processing := false
	quitRead := make(chan bool)
	for {
		select {
		case assignment := <-assignments:
			if assignment != nil { // Listen to kinesis stream only when there are assignments.
				if !processing { // Start processing if the processor is not available already.
					go readKinesisStream(*assignment, quitRead, k, logger)
					processing = true // Set the processing flag to false
				}
			} else if processing { // Stop if processing is in progress.
				logger.Info("Received the empty workitems in the assignments")
				processing = false // Reset the processing flag
				quitRead <- true
			}
		}
	}
}

func readKinesisStream(assignment models.Assignment, quitRead chan bool, k interfaces.IKinesisUtil, logger interfaces.ILogger) {
	currSequenceNumber := assignment.SequenceNumber
	latestSequenceNumber := &currSequenceNumber
	// if assignment.SequenceNumber is empty GetShardIterator uses "TRIM_HORIZON" to get last Sequence number
	shardIteratorOutput, err := k.GetShardIterator(appconfig.KinesisShardID, appconfig.AwsUpStreamName, currSequenceNumber, logger)
	if err == nil {
		shardIterator := shardIteratorOutput.ShardIterator
		shardIterator, latestSequenceNumber = getAndProcessRecords(shardIterator, latestSequenceNumber, k, logger) // Started reading the kinesis messages
		for {
			select {
			case <-quitRead:
				logger.Info("Stopped processing the kinesis messages")
				return
			case <-time.After(time.Second * 10): // Read and process the records after 10 secs.
				shardIterator, latestSequenceNumber = getAndProcessRecords(shardIterator, latestSequenceNumber, k, logger)
			}
		}
	}
}

// getAndProcessRecords reads and processes the kinesis messages for the supplied shardIterator
func getAndProcessRecords(shardIterator *string, latestSequenceNumber *string, k interfaces.IKinesisUtil, logger interfaces.ILogger) (*string, *string) {
	records, err := k.GetRecords(shardIterator, logger)
	if err == nil {
		recordsLength := len(records.Records)
		if recordsLength > 0 {
			latestSequenceNumber = records.Records[recordsLength-1].SequenceNumber
			for _, record := range records.Records {
				// process the kinesis records
				logger.Infof("Kinesis message Sequence number: %s", *record.SequenceNumber)
				handler.HandleTransaction(*record, logger)
			}
			ctx := ezeutils.WithKeyValue(tmb.Context(context.Background()), ezeutils.UserAgentKey, "Ibor_transaction_master/1.0 Go-http-client/1.1")
			// update workitem with latestSequenceNumber
			res, err := workdistributionclient.UpdateWorkItem(ctx, appconfig.WorkType, appconfig.WorkType, models.Assignment{SequenceNumber: *latestSequenceNumber})
			if err != nil {
				logger.Errorf("Failed to update sequence number in workItem: %v", err)
			} else {
				jsonResponse, err := json.Marshal(res)
				if err != nil {
					logger.Errorf("Error in parsing UpdateWorkItem response: %v", err)
				} else {
					logger.Infof("Successfully updated sequence number in workItem: %s", string(jsonResponse))
				}
			}
		} else {
			logger.Info("No new records found...")
		}
		shardIterator = records.NextShardIterator
		// Kinesis shard iterator is only valid for fixed duration of time, so refresh it if we run into ErrCodeExpiredIteratorException exception
	} else if strings.HasPrefix(err.Error(), kinesis.ErrCodeExpiredIteratorException) {
		logger.Infof("GetRecords returned ExpiredIteratorException hence refreshing the shardIterator with sequenceNumber: %s", latestSequenceNumber)
		shardIteratorOutput, errGetShard := k.GetShardIterator(appconfig.KinesisShardID, appconfig.AwsUpStreamName, *latestSequenceNumber, logger)
		if errGetShard == nil {
			shardIterator = shardIteratorOutput.ShardIterator
		}
	}
	logger.Info("Sleeping for 10 seconds...")
	return shardIterator, latestSequenceNumber
}
