package models

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/httputil"
)

var custodianTestJSON = `{
    "Allocations": [
      {
        "BookTypeID": 2,
		"PortfolioID" :12,
		"IsFinalized": true
      },
      {
        "BookTypeID": 1,
		"PortfolioID" :13,
		"IsFinalized": true
      }
	]
  }`

func TestCustodianCounterpartyIdValidBookTypeLocation(t *testing.T) {
	var custodianAccount CustodianCounterpartyAccountID = 4
	var testTransaction Transaction
	unmarhsalError := json.Unmarshal([]byte(custodianTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalError)
	str := "{\"data\": {\"custodianAccounts\": [{\"id\": 4, \"name\": \"GSCOa\"}]}}"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()

	err := custodianAccount.Validate(testTransaction, "CustodianCounterpartyAccountId", 0)
	assert.Nil(t, err)
}

func TestCustodianCounterpartyIdInvalidBookType(t *testing.T) {
	var custodianAccount CustodianCounterpartyAccountID = 4
	var testTransaction Transaction
	unmarhsalError := json.Unmarshal([]byte(custodianTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalError)
	str := "{\"data\": {\"custodianAccounts\": [{\"id\": 4, \"name\": \"GSCOa\"}]}}"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()

	err := custodianAccount.Validate(testTransaction, "CustodianCounterpartyAccountId", 1)
	assert.EqualError(t, err, "Error in allocation portfolioID 13. Invalid CustodianCounterpartyAccountId : Expected BookType 2 (for Location), Provided BookType 1")
}

func TestCustodianCounterpartyIdUnmarshallingError(t *testing.T) {
	var custodianAccount CustodianCounterpartyAccountID = 4
	var testTransaction Transaction
	unmarhsalError := json.Unmarshal([]byte(custodianTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalError)
	str := "{\"data\": {\"custodianAccounts\"\"id\": 4, \"name\": \"GSCOa\"}]}}"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()

	err := custodianAccount.Validate(testTransaction, "CustodianCounterpartyAccountId", 0)
	assert.EqualError(t, err, "Error in portfolioID 12. CustodianCounterpartyAccountID : Unmarshalling error")
}

func TestCustodianCounterpartyIdServiceLookupError(t *testing.T) {
	var custodianAccount CustodianCounterpartyAccountID = 4
	var testTransaction Transaction
	unmarhsalError := json.Unmarshal([]byte(custodianTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalError)
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadGateway)
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()

	err := custodianAccount.Validate(testTransaction, "CustodianCounterpartyAccountId", 0)
	assert.EqualError(t, err, "Error in portfolioID 12. CustodianCounterpartyAccountId : Service lookup failed")
}

func TestCustodianCounterpartyIdNotFound(t *testing.T) {
	var custodianAccount CustodianCounterpartyAccountID = 4
	var testTransaction Transaction
	unmarhsalError := json.Unmarshal([]byte(custodianTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalError)
	str := "{\"data\": {\"custodianAccounts\": []}}"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()

	err := custodianAccount.Validate(testTransaction, "CustodianCounterpartyAccountId", 0)
	assert.EqualError(t, err, "Error in allocation PortfolioID 12. Invalid CustodianCounterpartyAccountId : 4 does not exist")
}

func TestCustodianCounterpartyIdNotFinalized(t *testing.T) {
	var custodianAccount CustodianCounterpartyAccountID = 4
	var testTransaction Transaction
	unmarhsalError := json.Unmarshal([]byte(custodianTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalError)
	testTransaction.Allocations[0].IsFinalized = false

	err := custodianAccount.Validate(testTransaction, "CustodianCounterpartyAccountId", 0)
	assert.Nil(t, err)
}
