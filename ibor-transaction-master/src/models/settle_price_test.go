package models

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSettlePriceValid(t *testing.T) {
	var settlePrice SettlePrice = 36
	var settlePricestJSON = `{
		"Allocations": [
		  {
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity":12,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12
		  },
		  {
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity":12,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(settlePricestJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	err := settlePrice.Validate(testTransaction, "SettleAmount", 1)

	assert.Nil(t, err)
}

func TestSettlePriceValidErrExecQuantity(t *testing.T) {
	var settlePrice SettlePrice = 39
	var settlePricestJSON = `{
		"Allocations": [
		  {
			"BookTypeId": 1,
			"Quantity":   200,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12
		  },
		  {
			"BookTypeId": 1,
			"PortfolioId": 2,
			"Quantity":   200,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(settlePricestJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	err := settlePrice.Validate(testTransaction, "SettlePrice", 1)

	assert.EqualError(t, err, fmt.Sprintf("Invalid SettlePrice. Invalid Exec Quantity for PortfolioId: %d", *testTransaction.Allocations[1].PortfolioID))
}
func TestSettlePriceValidErr(t *testing.T) {
	var settlePrice SettlePrice = -39
	var settlePricestJSON = `{
		"Allocations": [
		  {
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity":12,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12
		  },
		  {
			"BookTypeId": 1,
			"PortfolioId": 2,
			"Quantity":   200,
			"ExecQuantity":12,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(settlePricestJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)

	err := settlePrice.Validate(testTransaction, "SettlePrice", 1)

	assert.EqualError(t, err, fmt.Sprintf("Invalid SettlePrice as the value is less than zero -39.000000 for PortfolioId: %d", *testTransaction.Allocations[1].PortfolioID))
}

func TestSettlePriceforFloatValues(t *testing.T) {
	var settlePrice SettlePrice = 0.0467
	var settlePricestJSON = `{
		"Allocations": [
		  {
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity":12,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12
		  },
		  {
			"BookTypeId": 1,
			"PortfolioId": 2,
			"Quantity":   200,
			"ExecQuantity":12,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(settlePricestJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)

	err := settlePrice.Validate(testTransaction, "SettlePrice", 1)

	assert.Nil(t, err)
}
