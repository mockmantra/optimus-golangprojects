package models

//"fmt"

// FeeCharges commission charge
type FeeCharges struct {
	FeeCharge   *FeeCharge   `json:"FeeCharge"`
	FeeChargeID *FeeChargeID `json:"FeeChargeId"`
}

// FeeChargesSlice type
type FeeChargesSlice []*FeeCharges

// Validate validates the field
func (value FeeChargesSlice) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	for chargeIndex, feeCharge := range value {
		err := feeCharge.FeeCharge.Validate(transaction, fieldName, allocationIndex, chargeIndex)
		if err != nil {
			return err
		}
		err = feeCharge.FeeChargeID.Validate(transaction, fieldName, allocationIndex, chargeIndex)
		if err != nil {
			return err
		}
	}
	return nil
}
