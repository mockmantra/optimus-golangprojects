package models

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

var fxRateSettleToSystemInValidSettleCurrencySecurityIDTestJSON = `{
	"Allocations": [
	  {
		"ExecQuantity": 200,
		"SettleCurrencySecurityId": 13
	  },
	  {
		"ExecQuantity": 200,
		"PortfolioId":12
	  }
	],
	"SystemCurrencySecurityId": 13
}`

func TestSettleToSystemFXRateInvalidIfSettleCurrencySecurityIdIsNil(t *testing.T) {
	var settleToSystemFxRate FxRate = 1

	//var a ExecQuantity = 12
	var transaction Transaction
	unmarshalErr := json.Unmarshal([]byte(fxRateSettleToSystemInValidSettleCurrencySecurityIDTestJSON), &transaction)
	assert.Nil(t, unmarshalErr)

	err := settleToSystemFxRate.Validate(transaction, "SettleToSystemFXRate", 1)

	assert.Equal(t, err, fmt.Errorf("Error in allocation portfolioID %d. Invalid %s - SettleCurrencySecurityID is missing", int(*transaction.Allocations[1].PortfolioID), "SettleToSystemFXRate"))
}

var fxRateSettleToSystemValidTestJSON = `{
	"Allocations": [
	  {
		"ExecQuantity": 200,
		"SettleCurrencySecurityId": 13
	  },
	  {
		"ExecQuantity": 200,
		"SettleCurrencySecurityId": 13
	  }
	],
	"SystemCurrencySecurityId": 13
}`

func TestSettleToSystemFXRateValid(t *testing.T) {
	var settleToSystemFxRate FxRate = 1

	//var a ExecQuantity = 12
	var transaction Transaction
	unmarshalErr := json.Unmarshal([]byte(fxRateSettleToSystemValidTestJSON), &transaction)
	assert.Nil(t, unmarshalErr)

	err := settleToSystemFxRate.Validate(transaction, "SettleToSystemFXRate", 1)

	assert.Nil(t, err)
}

var fxRateSettleToSystemInvalidTestJSON = `{
	"Allocations": [
	  {
		"ExecQuantity": 200,
		"PortfolioId":12,
		"SettleCurrencySecurityId": 13,
		"SettleToSystemFxRate" : 2
	  },
	  {
		"ExecQuantity": 200,
		"PortfolioId":13,
		"SettleCurrencySecurityId": 13
	  }
	],
	"SystemCurrencySecurityId": 13
}`

func TestSettleToSystemFXRateInValid(t *testing.T) {
	var settleToSystemFxRate FxRate = 2
	var transaction Transaction
	unmarshalErr := json.Unmarshal([]byte(fxRateSettleToSystemInvalidTestJSON), &transaction)
	assert.Nil(t, unmarshalErr)

	err := settleToSystemFxRate.Validate(transaction, "SettleToSystemFXRate", 1)

	assert.Equal(t, err, fmt.Errorf("Error in allocation portfolioID %d. Invalid %s - Expected value: %d, Provided value : %f", int(*transaction.Allocations[1].PortfolioID), "SettleToSystemFXRate", 1, settleToSystemFxRate))

}

var fxRateSettleToSystemInvalidExecQuantityTestJSON = `{
	"Allocations": [
	  {
		"ExecQuantity": 0,
		"PortfolioId" :12,
		"SettleCurrencySecurityId": 13
	  },
	  {
		"ExecQuantity": 0,
		"PortfolioId" :13,
		"SettleCurrencySecurityId": 13
	  }
	],
	"SystemCurrencySecurityId": 13
}`

func TestFXRateExecQuanatityLessThanZero(t *testing.T) {
	var settleToSystemFxRate FxRate = 2

	//var a ExecQuantity
	var transaction Transaction
	unmarshalErr := json.Unmarshal([]byte(fxRateSettleToSystemInvalidExecQuantityTestJSON), &transaction)
	assert.Nil(t, unmarshalErr)

	err := settleToSystemFxRate.Validate(transaction, "SettleToSystemFXRate", 1)

	assert.Equal(t, err, fmt.Errorf("Invalid %s for allocation portfolioID %d. Expected : ExecQuantity not valid", "SettleToSystemFXRate", int(*transaction.Allocations[1].PortfolioID)))

}

var fxRateSettleToLocalInvalidTestJSON = `{
	"Allocations": [
	  {
		"PortfolioId" : 12,
		"ExecQuantity": 200,
		"SettleCurrencySecurityId": 13
	  },
	  {
			"PortfolioId" : 13,
		"ExecQuantity": 200,
		"SettleCurrencySecurityId": 13
	  }
	],
	"LocalCurrencySecurityId": 13
}`

func TestSettleToLocalFXRateInValid(t *testing.T) {
	var settleToLocalFXRate FxRate = 2
	var transaction Transaction
	unmarshalErr := json.Unmarshal([]byte(fxRateSettleToLocalInvalidTestJSON), &transaction)
	assert.Nil(t, unmarshalErr)

	err := settleToLocalFXRate.Validate(transaction, "SettleToLocalFXRate", 1)

	assert.Equal(t, err, fmt.Errorf("Error in allocation portfolioID %d. Invalid %s - Expected value: %d, Provided value : %f", int(*transaction.Allocations[1].PortfolioID), "SettleToLocalFXRate", 1, settleToLocalFXRate))

}

var fxRateSettleToLocalInvalidTestJSONSettleCurrencySecurityID = `{
	"Allocations": [
	  {
		"PortfolioId" : 12,
		"ExecQuantity": 200,
		"SettleCurrencySecurityId": 13
	  },
	  {
			"PortfolioId" : 13,
		"ExecQuantity": 200
	  }
	],
	"LocalCurrencySecurityId": 13
}`

func TestSettleToLocalFXRateInValidIfSettleCurrencySecurityIDIsMissing(t *testing.T) {
	var settleToLocalFXRate FxRate = 2
	var transaction Transaction
	unmarshalErr := json.Unmarshal([]byte(fxRateSettleToLocalInvalidTestJSONSettleCurrencySecurityID), &transaction)
	assert.Nil(t, unmarshalErr)

	err := settleToLocalFXRate.Validate(transaction, "SettleToLocalFXRate", 1)

	assert.Equal(t, err, fmt.Errorf("Error in allocation portfolioID %d. Invalid %s - SettleCurrencySecurityID is missing", int(*transaction.Allocations[1].PortfolioID), "SettleToLocalFXRate"))

}

var fxRateSettleToLocalValidTestJSON = `{
	"Allocations": [
	  {
		"PortfolioId" : 13,
		"ExecQuantity": 200,
		"SettleCurrencySecurityId": 13
	  },
	  {
			"PortfolioId" : 14,
		"ExecQuantity": 200,
		"SettleCurrencySecurityId": 13
	  }
	],
	"LocalCurrencySecurityId": 13
}`

func TestSettleToLocalFXRateValid(t *testing.T) {
	var settleToLocalFXRate FxRate = 1
	var transaction Transaction
	unmarshalErr := json.Unmarshal([]byte(fxRateSettleToLocalValidTestJSON), &transaction)
	assert.Nil(t, unmarshalErr)

	err := settleToLocalFXRate.Validate(transaction, "SettleToLocalFXRate", 1)

	assert.Nil(t, err)

}

var fxRateSettleToBaseValidTestJSON = `{
	"Allocations": [
	  {
		"PortfolioId" : 14,
		"ExecQuantity": 200,
		"SettleCurrencySecurityId": 13,
		"BaseCurrencySecurityID": 13
	  },
	  {
		"PortfolioId" : 14,
		"ExecQuantity": 200,
		"SettleCurrencySecurityId": 13,
		"BaseCurrencySecurityID": 13
	  }
	]
}`

func TestSettleToBaseFXRateValid(t *testing.T) {
	var settleToBaseFXRate FxRate = 1
	var transaction Transaction
	unmarshalErr := json.Unmarshal([]byte(fxRateSettleToBaseValidTestJSON), &transaction)
	assert.Nil(t, unmarshalErr)

	err := settleToBaseFXRate.Validate(transaction, "SettleToBaseFXRate", 1)

	assert.Nil(t, err)
}

var fxRateSettleToBaseValidInvalidSettleCurrencySecurityIDTestJSON = `{
	"Allocations": [
	  {
		"PortfolioId" : 14,
		"ExecQuantity": 200,
		"SettleCurrencySecurityId": 13,
		"BaseCurrencySecurityID": 13
	  },
	  {
		"PortfolioId" : 14,
		"ExecQuantity": 200,
		"BaseCurrencySecurityID": 13
	  }
	]
}`

func TestSettleToBaseFXRateInValidIfSettleCurrencySecurityIdIsMissing(t *testing.T) {
	var settleToBaseFXRate FxRate = 1
	var transaction Transaction
	unmarshalErr := json.Unmarshal([]byte(fxRateSettleToBaseValidInvalidSettleCurrencySecurityIDTestJSON), &transaction)
	assert.Nil(t, unmarshalErr)

	err := settleToBaseFXRate.Validate(transaction, "SettleToBaseFXRate", 1)

	assert.Equal(t, err, fmt.Errorf("Error in allocation portfolioID %d. Invalid %s - SettleCurrencySecurityID is missing", int(*transaction.Allocations[1].PortfolioID), "SettleToBaseFXRate"))
}

var fxRateSettleToBaseInvalidTestJSON = `{
	"Allocations": [
	  {
			"PortfolioId" : 14,
		"ExecQuantity": 200,
		"SettleCurrencySecurityId": 13,
		"BaseCurrencySecurityID": 14
	  },
	  {
			"PortfolioId" : 15,
		"ExecQuantity": 200,
		"SettleCurrencySecurityId": 13,
		"BaseCurrencySecurityID": 13
	  }
	]
}`

func TestSettleToBaseFXRateInValid(t *testing.T) {
	var settleToBaseFXRate FxRate = 2
	var transaction Transaction
	unmarshalErr := json.Unmarshal([]byte(fxRateSettleToBaseInvalidTestJSON), &transaction)
	assert.Nil(t, unmarshalErr)

	err := settleToBaseFXRate.Validate(transaction, "SettleToBaseFXRate", 1)

	assert.Equal(t, err, fmt.Errorf("Error in allocation portfolioID %d. Invalid %s - Expected value: %d, Provided value : %f", int(*transaction.Allocations[1].PortfolioID), "SettleToBaseFXRate", 1, settleToBaseFXRate))

}

var baseToLocalFxRateInvalidOneTestJSON = `{
	"LocalCurrencySecurityID" : 12,
	"Allocations": [
	  {
			"PortfolioId" : 15,
		"ExecQuantity": 200,
		"SettleCurrencySecurityId": 13,
		"BaseCurrencySecurityID": 13
	  },
	  {
			"PortfolioId" : 16,
		"ExecQuantity": 200,
		"SettleCurrencySecurityId": 13,
		"BaseCurrencySecurityID": 12
	  }
	]
}`

func TestBaseToLocalFXRateInValidOne(t *testing.T) {
	var baseToLocalFxRate FxRate = 2
	var transaction Transaction
	unmarshalErr := json.Unmarshal([]byte(baseToLocalFxRateInvalidOneTestJSON), &transaction)
	assert.Nil(t, unmarshalErr)

	err := baseToLocalFxRate.Validate(transaction, "BaseToLocalFXRate", 1)

	assert.Equal(t, err, fmt.Errorf("Error in allocation portfolioID %d. Invalid %s - Expected value: %d, Provided value : %f", int(*transaction.Allocations[1].PortfolioID), "BaseToLocalFXRate", 1, baseToLocalFxRate))

}

func TestBaseToLocalFXRateNotEqual(t *testing.T) {
	var baseToLocalFxRate FxRate = 2
	var transaction Transaction
	unmarshalErr := json.Unmarshal([]byte(baseToLocalFxRateInvalidOneTestJSON), &transaction)
	assert.Nil(t, unmarshalErr)

	err := baseToLocalFxRate.Validate(transaction, "BaseToLocalFXRate", 0)

	assert.Nil(t, err)
}
