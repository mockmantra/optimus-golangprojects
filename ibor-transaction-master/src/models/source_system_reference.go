package models

import (
	"fmt"
	"strconv"
)

// SourceSystemReference field
type SourceSystemReference string

// Validate validates the fields
func (value SourceSystemReference) Validate(transaction Transaction, fieldName string) error {
	_, err := strconv.Atoi(string(value))
	if err != nil {
		return fmt.Errorf("Invalid SourceSystemReference provided: %s", value)
	}
	return nil
}