package models

import (
	"fmt"
	"reflect"
)

// SettlePrice type
type SettlePrice float64

// Validate Validates the field
func (value SettlePrice) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	if !reflect.ValueOf(transaction.Allocations[allocationIndex].ExecQuantity).IsNil() && *transaction.Allocations[allocationIndex].ExecQuantity > 0 {

		if (value) > 0 {
			return nil
		}
		return fmt.Errorf("Invalid %s as the value is less than zero %f for PortfolioId: %d", fieldName, value, *transaction.Allocations[allocationIndex].PortfolioID)
	}
	return fmt.Errorf("Invalid %s. Invalid Exec Quantity for PortfolioId: %d", fieldName, *transaction.Allocations[allocationIndex].PortfolioID)
}
