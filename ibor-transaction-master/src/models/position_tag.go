package models

import (
	"encoding/json"
	"fmt"
	"strings"

	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/httputil"
)

// PositionTag position tag
type PositionTag struct {
	IndexAttributeID      *int64 `json:"IndexAttributeId"`
	IndexAttributeValueID *int64 `json:"IndexAttributeValueId"`
}

// PositionTagsSlice type
type PositionTagsSlice []*PositionTag

// PositionTagWithMembersResponse type
type PositionTagWithMembersResponse []PositionTagWithMembers

// PositionTagWithMembers type
type PositionTagWithMembers struct {
	ID      int              `json:"Id"`
	Members []ResponseMember `json:"Members"`
}

// ResponseMember type
type ResponseMember struct {
	IndexAttributeID      *int64 `json:"IndexAttributeId"`
	IndexAttributeValueID *int64 `json:"IdxAttrValueId_IdxAttrValueId"`
}

const positionTagURL = "referencedata/v1/PositionTag/GetAllWithMembers"

// Validate validates the field
func (value PositionTagsSlice) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	if *transaction.Allocations[allocationIndex].BookTypeID != BookTypeStrategy {
		return fmt.Errorf("Invalid %s as the provided book type is %d. Expected %d (for Strategy)", fieldName, *transaction.Allocations[allocationIndex].BookTypeID, BookTypeStrategy)
	}
	bodyBytes, err := httputil.NewRequest("GET", positionTagURL, nil)
	var positionTagWithMembersResponse PositionTagWithMembersResponse
	if err == nil {
		unmarshalErr := json.Unmarshal(bodyBytes, &positionTagWithMembersResponse)
		if unmarshalErr != nil {
			return fmt.Errorf("Invalid %s. Service response error", fieldName)
		}
	} else {
		if strings.Contains(err.Error(), "error:404") {
			return fmt.Errorf("Invalid %s. Records not found", fieldName)
		}
		return fmt.Errorf("Invalid %s. Service lookup failed", fieldName)
	}
	var returnErr = fmt.Errorf("Error")
	for _, positionTag := range value {
		for _, positionTagWithMember := range positionTagWithMembersResponse {
			for _, member := range positionTagWithMember.Members {
				if *positionTag.IndexAttributeID == *member.IndexAttributeID && *positionTag.IndexAttributeValueID == *member.IndexAttributeValueID {
					returnErr = nil
				}
			}
		}
		if returnErr != nil {
			return fmt.Errorf("Invalid %s for PortfolioId: %d. IndexAttributeID %d and indexAttributeValueID %d does not refernce a valid position tag", fieldName, *transaction.Allocations[allocationIndex].PortfolioID, int(*positionTag.IndexAttributeID), (*positionTag.IndexAttributeValueID))
		}
		returnErr = fmt.Errorf("Error")

	}
	return nil
}
