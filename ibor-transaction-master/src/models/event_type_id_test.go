package models

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/httputil"
)

func TestEventTypeId_WithValidEventTypeId(t *testing.T) {
	var eventTypeID EventTypeID = 3
	str := "[{\"EventTypeName\":\"Buy\",\"EventTypeId\":3},{\"EventTypeName\":\"Cover\",\"EventTypeId\":6},{\"EventTypeName\":\"Sell\",\"EventTypeId\":14}]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()
	transaction := Transaction{}

	err := eventTypeID.Validate(transaction, "EventTypeId")

	assert.Nil(t, err)
	httputil.UserSessionToken = ""

}
func TestEventTypeId_WithInvalidEventTypeId(t *testing.T) {
	var eventTypeID EventTypeID = 7
	str := "[{\"EventTypeName\":\"Buy\",\"EventTypeId\":3},{\"EventTypeName\":\"Cover\",\"EventTypeId\":6},{\"EventTypeName\":\"Sell\",\"EventTypeId\":14}]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()
	transaction := Transaction{}

	err := eventTypeID.Validate(transaction, "EventTypeId")

	assert.EqualError(t, err, "Invalid EventTypeId. Must be 3(for Buy), 6(for Cover), 14(for Sell) or 17(for Short)")

	httputil.UserSessionToken = ""
}
func TestEventTypeId_WithErrorInHttpCall(t *testing.T) {
	var eventTypeID EventTypeID = 7
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadGateway)
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()
	transaction := Transaction{}

	err := eventTypeID.Validate(transaction, "EventTypeId")

	assert.EqualError(t, err, "EventTypeId : Service lookup failed")
	httputil.UserSessionToken = ""
}

func TestEventTypeId_WithErrorInHttpCallNotFound(t *testing.T) {
	var eventTypeID EventTypeID = 7
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"

	defer ts.Close()
	transaction := Transaction{}

	err := eventTypeID.Validate(transaction, "EventTypeId")

	assert.EqualError(t, err, "EventTypeId : Service lookup failed")
	httputil.UserSessionToken = ""

}

func TestEventTypeId_WithInvalidEventTypeIdUnmarshal(t *testing.T) {
	var eventTypeID EventTypeID = 7
	str := ""
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"

	defer ts.Close()
	transaction := Transaction{}

	err := eventTypeID.Validate(transaction, "EventTypeId")

	assert.EqualError(t, err, "EventTypeId : Service lookup failed")
	httputil.UserSessionToken = ""
}
