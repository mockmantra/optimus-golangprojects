package models

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

var feeChargeJSON = `{
	"Allocations": [
		{
			"PortfolioId": 12,
		  "ExecQuantity": 100,
		  "Fees": 10,
		  "FeeCharges": [
			{
			  "FeeCharge": 10,
			  "FeeChargeId": 22
			}
		  ]
		},
		{
			"PortfolioId": 13,
			"ExecQuantity": 0,
			"Fees": 10,
			"FeeCharges": [
			  {
				"FeeCharge": 10,
				"FeeChargeId": 22
			  }
			]
		},
		{
			"PortfolioId": 14,
			"ExecQuantity": 100,
			"Fees": 12,
			"FeeCharges": [
			  {
				"FeeCharge": 10
			  }
			]
		}
	]
}`

func TestFeeChargeValid(t *testing.T) {
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(feeChargeJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)

	feeCharges := *testTransaction.Allocations[0].FeeCharges
	err := feeCharges[0].FeeCharge.Validate(testTransaction, "FeeCharge", 0, 0)
	assert.Nil(t, err)
}

func TestFeeChargeInvalidExecQuantity(t *testing.T) {
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(feeChargeJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)

	feeCharges := *testTransaction.Allocations[0].FeeCharges
	err := feeCharges[0].FeeCharge.Validate(testTransaction, "FeeCharge", 1, 0)
	assert.NotNil(t, err)
}

func TestFeeChargeWithoutCommissionChargeId(t *testing.T) {
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(feeChargeJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)

	feeCharges := *testTransaction.Allocations[0].FeeCharges
	err := feeCharges[0].FeeCharge.Validate(testTransaction, "FeeCharge", 2, 0)
	assert.NotNil(t, err)
}
