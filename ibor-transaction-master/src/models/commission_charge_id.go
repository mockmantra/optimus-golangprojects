package models

import (
	"encoding/json"
	"fmt"
	"reflect"
	"strings"

	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/httputil"
)

// CommissionChargeID type
type CommissionChargeID int

const commissionURL = "referencedata/v1/GetCommissionById?chargeId=%d"

type commissionResponse []struct {
	ID int `json:"Id"`
}

//Validate validates the field
func (value CommissionChargeID) Validate(transaction Transaction, fieldName string, allocationIndex int, nestedIndex int) error {
	if !reflect.ValueOf(transaction.Allocations[allocationIndex].ExecQuantity).IsNil() && int(*transaction.Allocations[allocationIndex].ExecQuantity) > 0 {
		commCharges := *transaction.Allocations[allocationIndex].CommissionCharges
		if !reflect.ValueOf(commCharges[nestedIndex].CommissionCharge).IsNil() {
			bodyBytes, err := httputil.NewRequest("GET", fmt.Sprintf(commissionURL, int(value)), nil)
			var commResponse commissionResponse
			if err == nil {
				error := json.Unmarshal(bodyBytes, &commResponse)
				if error == nil && len(commResponse) > 0 {
					return nil
				}
				return fmt.Errorf("Error in portfolioID %d. CommissionChargeID : Unmarshlling error", int(*transaction.Allocations[allocationIndex].PortfolioID))
			} else if strings.Contains(err.Error(), "error:404") {
				return fmt.Errorf("Error in allocation portfolioID %d. Invalid CommissionChargeID : %d does not exist", int(*transaction.Allocations[allocationIndex].PortfolioID), int(*commCharges[nestedIndex].CommissionChargeID))
			}
			return fmt.Errorf("Error in allocation portfolioID %d. CommissionChargeID : Service lookup failed", int(*transaction.Allocations[allocationIndex].PortfolioID))
		}
		return fmt.Errorf("Error in allocation portfolioID %d. Invalid CommissionChargeID, Missing CommissionCharge for CommissionChargeID %d", int(*transaction.Allocations[allocationIndex].PortfolioID), int(*commCharges[nestedIndex].CommissionChargeID))

	}
	return fmt.Errorf("Error in allocation portfolioID %d. Invalid CommissionChargeID: ExecQuantity not valid", int(*transaction.Allocations[allocationIndex].PortfolioID))

}
