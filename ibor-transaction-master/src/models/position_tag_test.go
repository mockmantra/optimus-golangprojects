package models

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/httputil"
)

var positionTagTestJSON = `{
	"Allocations": [
	  {
		"BookTypeId": 3,
		"PortfolioId": 1,
		"PositionTags": [
		  {
			"IndexAttributeId": 1,
			"IndexAttributeValueId": 8
		  }
		]
	  },
	  {
		"BookTypeId": 3,
		"PortfolioId": 2,
		"PositionTags": [
		  {
			"IndexAttributeId": 1,
			"IndexAttributeValueId": 8
		  },
		  {
			"IndexAttributeId": 2,
			"IndexAttributeValueId": 6
		  }
		]
	  },
	  {
		"BookTypeId": 2,
		"PortfolioId": 3,
		"PositionTags": [
		  {
			"IndexAttributeId": 1,
			"IndexAttributeValueId": 8
		  },
		  {
			"IndexAttributeId": 2,
			"IndexAttributeValueId": 6
		  }
		] 
	  }
	]
}`

func TestPositionTagValidTag(t *testing.T) {
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(positionTagTestJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)

	str := `[
		{
		  "Id": 1,
		  "Members": [
			{
			  "IndexAttributeId": 1,
			  "IdxAttrValueId_IdxAttrValueId": 8
			}
		  ]
		},
		{
		  "Id": 2,
		  "Members": [
			{
			  "IndexAttributeId": 2,
			  "IdxAttrValueId_IdxAttrValueId": 9
			}
		  ]
		}
	]`
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, writeErr := w.Write([]byte(str))
		assert.Nil(t, writeErr)
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()

	err := testTransaction.Allocations[0].PositionTags.Validate(testTransaction, "PositionTags", 0)
	assert.Nil(t, err)
	httputil.UserSessionToken = ""
}

func TestPositionTagInvalidTag(t *testing.T) {
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(positionTagTestJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)

	str := `[
		{
		  "Id": 1,
		  "Members": [
			{
			  "IndexAttributeId": 1,
			  "IdxAttrValueId_IdxAttrValueId": 8
			}
		  ]
		},
		{
		  "Id": 2,
		  "Members": [
			{
			  "IndexAttributeId": 1,
			  "IdxAttrValueId_IdxAttrValueId": 8
			},
			{
				"IndexAttributeId": 2,
				"IdxAttrValueId_IdxAttrValueId": 9
			}
		  ]
		}
	]`
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, writeErr := w.Write([]byte(str))
		assert.Nil(t, writeErr)
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()

	err := testTransaction.Allocations[1].PositionTags.Validate(testTransaction, "PositionTags", 1)
	assert.EqualError(t, err, "Invalid PositionTags for PortfolioId: 2. IndexAttributeID 2 and indexAttributeValueID 6 does not refernce a valid position tag")
	httputil.UserSessionToken = ""
}

func TestPositionTagInvalidBookType(t *testing.T) {
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(positionTagTestJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)

	err := testTransaction.Allocations[2].PositionTags.Validate(testTransaction, "PositionTags", 2)

	assert.EqualError(t, err, fmt.Sprintf("Invalid PositionTags as the provided book type is 2. Expected %d (for Strategy)", BookTypeStrategy))
}

func TestPositionTagResponseUnmarshalError(t *testing.T) {
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(positionTagTestJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)

	str := `[
		{
			"BookType"
		  "Incorrect data"
		}
	]`
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, writeErr := w.Write([]byte(str))
		assert.Nil(t, writeErr)
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()

	err := testTransaction.Allocations[1].PositionTags.Validate(testTransaction, "PositionTags", 1)

	assert.EqualError(t, err, "Invalid PositionTags. Service response error")
	httputil.UserSessionToken = ""
}
func TestPositionTagHttpNotFound(t *testing.T) {
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(positionTagTestJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()

	err := testTransaction.Allocations[1].PositionTags.Validate(testTransaction, "PositionTags", 1)

	assert.EqualError(t, err, "Invalid PositionTags. Records not found")
	httputil.UserSessionToken = ""
}

func TestPositionTagHttpCallError(t *testing.T) {
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(positionTagTestJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadGateway)
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()

	err := testTransaction.Allocations[1].PositionTags.Validate(testTransaction, "PositionTags", 1)

	assert.EqualError(t, err, "Invalid PositionTags. Service lookup failed")
	httputil.UserSessionToken = ""
}
