package models

import (
	"fmt"
	"reflect"
)

// CommissionCharge type
type CommissionCharge float64

// Validate validates the field
func (value CommissionCharge) Validate(transaction Transaction, fieldName string, allocationIndex int, nestedIndex int) error {
	if !reflect.ValueOf(transaction.Allocations[allocationIndex].ExecQuantity).IsNil() && int(*transaction.Allocations[allocationIndex].ExecQuantity) > 0 {
		commCharges := *transaction.Allocations[allocationIndex].CommissionCharges
		if !reflect.ValueOf(commCharges[nestedIndex].CommissionChargeID).IsNil() {
			return nil
		}
		return fmt.Errorf("Error in allocation portfolioID %d. Invalid CommissionCharge: Missing CommissionChargeID for Commission Value %d", int(*transaction.Allocations[allocationIndex].PortfolioID), int(*commCharges[nestedIndex].CommissionCharge))
	}
	return fmt.Errorf("Error in allocation portfolioID %d. Invalid CommissionCharge: ExecQuantity not valid", int(*transaction.Allocations[allocationIndex].PortfolioID))

}
