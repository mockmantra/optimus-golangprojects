package models

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/httputil"
)

var baseCurrencySecurityIDTestJSON = `{
	"Allocations": [
	  {
		"Quantity":    200,
		"PortfolioId": 12
	  },
	  {
		"Quantity":    200,
		"PortfolioId": 12
	  }
	]
}`

func TestBaseCurrencySecurityIDWIthValidData(t *testing.T) {

	var baseCurrencySecurityID BaseCurrencySecurityID = 134
	str := "{\"BaseCurrencyId\":134,\"PortfolioId\":4,\"Id\":4}"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL

	defer ts.Close()

	httputil.UserSessionToken = "123"

	fieldName := "BaseCurrencySecurityID"
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(baseCurrencySecurityIDTestJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)

	err := baseCurrencySecurityID.Validate(testTransaction, fieldName, 1)

	assert.Nil(t, err)

	httputil.UserSessionToken = ""

}

func TestBaseCurrencySecurityIDWIthInValidData(t *testing.T) {

	var baseCurrencySecurityID BaseCurrencySecurityID = 155
	str := "{\"BaseCurrencyId\":134,\"PortfolioId\":4,\"Id\":4}"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"

	defer ts.Close()

	fieldName := "BaseCurrencySecurityID"
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(baseCurrencySecurityIDTestJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)

	err := baseCurrencySecurityID.Validate(testTransaction, fieldName, 1)

	assert.Equal(t, err, fmt.Errorf("Error in allocation PortfolioID %d. Invalid %s : Expected value %d, Provided Value %d", int(*testTransaction.Allocations[0].PortfolioID), fieldName, 134, 155))
	httputil.UserSessionToken = ""
}

func TestBaseCurrencySecurityIDAPICallError(t *testing.T) {

	var baseCurrencySecurityID BaseCurrencySecurityID = 155
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"

	defer ts.Close()

	fieldName := "BaseCurrencySecurityID"
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(baseCurrencySecurityIDTestJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	err := baseCurrencySecurityID.Validate(testTransaction, fieldName, 1)
	assert.Equal(t, err, fmt.Errorf("Invalid %s : PortfolioID %d does not exist", fieldName, int(*testTransaction.Allocations[0].PortfolioID)))

	httputil.UserSessionToken = ""
}

func TestBaseCurrencySecurityIDAPICallErrorLookup(t *testing.T) {

	var baseCurrencySecurityID BaseCurrencySecurityID = 155
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadGateway)
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"

	defer ts.Close()

	fieldName := "BaseCurrencySecurityID"
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(baseCurrencySecurityIDTestJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	err := baseCurrencySecurityID.Validate(testTransaction, fieldName, 1)
	assert.EqualError(t, err, "Error in allocation portfolioID 12. BaseCurrencySecurityID : Service lookup failed")

	httputil.UserSessionToken = ""

}
