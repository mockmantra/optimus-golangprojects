package models

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

var commissionTestJSON = `{
	"Allocations": [
		{
			"BookTypeId" : 2,
			"PortfolioId" :10,
		  	"ExecQuantity": 100,
		  	"Commission": 10,
		 	 "CommissionCharges": [
			{
			  "CommissionCharge": 10,
			  "CommissionChargeId": 22
			},
			{
			  "CommissionCharge": 0,
			  "CommissionChargeId": 24
			}
		  ]
		},
		{
			"BookTypeId" : 3,
			"PortfolioId" :11,
			"ExecQuantity": 0,
			"Commission": 10,
			"CommissionCharges": []
		},
		{
			"BookTypeId" : 2,
			"PortfolioId" :11,
			"ExecQuantity": 100,
			"Commission": 12,
			"CommissionCharges": [
			  {
				"CommissionCharge": 10,
				"CommissionChargeId": 22
			  },
			  {
				"CommissionCharge": 0,
				"CommissionChargeId": 24
			  }
			]
		},
		{
			"BookTypeId" : 3,
			"PortfolioId" :15,
			"ExecQuantity": 100,
			"Commission": 12
		}
	]
}`

func TestCommissionValid(t *testing.T) {
	var testTransaction Transaction
	unmarhsalErr := json.Unmarshal([]byte(commissionTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalErr)
	err := testTransaction.Allocations[0].Commission.Validate(testTransaction, "Commission", 0)
	assert.Nil(t, err)
}

func TestCommissionInvalidExecQuantity(t *testing.T) {
	var testTransaction Transaction
	unmarhsalErr := json.Unmarshal([]byte(commissionTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalErr)
	err := testTransaction.Allocations[1].Commission.Validate(testTransaction, "Commission", 1)
	assert.Equal(t, err, fmt.Errorf("Error in portfolioID 11. Invalid Commission : ExecQuantity not valid"))
}

func TestCommissionInvalidSum(t *testing.T) {
	var testTransaction Transaction
	unmarhsalErr := json.Unmarshal([]byte(commissionTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalErr)
	err := testTransaction.Allocations[2].Commission.Validate(testTransaction, "Commission", 2)
	assert.Equal(t, err, fmt.Errorf("Error in portfolioID 11. Invalid Commission : Expected value 12.000000, Provided value 10.000000"))
}

func TestCommissionInvalidNoCommissionCharges(t *testing.T) {
	var testTransaction Transaction
	unmarhsalErr := json.Unmarshal([]byte(commissionTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalErr)
	err := testTransaction.Allocations[2].Commission.Validate(testTransaction, "Commission", 3)
	assert.Equal(t, err, fmt.Errorf("Error in portfolioID 15. Invalid Commission : Value should be present"))
}

var commissionTestObjectInvalid = `{
	"Allocations": [
		{
			"BaseCurrencySecurityId": 155,
			"BaseToLocalFxRate": 1,
			"BookTypeId": 2,
			"Commission": 10,
			"CommissionCharges": [
			{
			"CommissionCharge": 10,
			"CommissionChargeId": 7
			}
			],
			"CustodianCounterpartyAccountId": 1,
			"ExecQuantity": 530,
			"ExecutingBrokerCounterpartyId": 1,
			"FeeCharges": [
			{
			"FeeCharge": 20,
			"FeeChargeId": 10
			}
			],
			"Fees": 20,
			"IsFinalized": false,
			"NetAmount": 41706.55,
			"PortfolioId": 1,
			"PositionTags": null,
			"PrincipalAmount": 41676.55,
			"Quantity": 530,
			"RouteId": 74665,
			"RouteName": "IMS-DEMO",
			"SettleAmount": 41706.55,
			"SettleCurrencySecurityId": 155,
			"SettlePrice": 78.635,
			"SettleToBaseFXRate": 1,
			"SettleToLocalFXRate": 1,
			"SettleToSystemFXRate": 1
			},
			{
			"BaseCurrencySecurityId": 155,
			"BaseToLocalFxRate": 1,
			"BookTypeId": 3,
			"Commission": 12,
			"CommissionCharges": [],
			"ExecQuantity": 530,
			"ExecutingBrokerCounterpartyId": 1,
			"FeeCharges": [],
			"Fees": 20,
			"IsFinalized": false,
			"NetAmount": 41706.55,
			"PortfolioId": 1,
			"PositionTags": null,
			"PrincipalAmount": 41676.55,
			"Quantity": 530,
			"RouteId": 74665,
			"RouteName": "IMS-DEMO",
			"SettleAmount": 41706.55,
			"SettleCurrencySecurityId": 155,
			"SettlePrice": 78.635,
			"SettleToBaseFXRate": 1,
			"SettleToLocalFXRate": 1,
			"SettleToSystemFXRate": 1
			}
		]
	}`

func TestCommissionInValidCommissionChargesForStrategy(t *testing.T) {
	var testTransaction Transaction
	unmarhsalErr := json.Unmarshal([]byte(commissionTestObjectInvalid), &testTransaction)
	assert.Nil(t, unmarhsalErr)
	err := testTransaction.Allocations[1].Commission.Validate(testTransaction, "Commission", 1)
	assert.Equal(t, err, fmt.Errorf("Error in portfolioID 1. Invalid Commission for BookTypeId : 3 "))
}

var commissionTestObjectValid = `{
	"Allocations": [
		{
			"BaseCurrencySecurityId": 155,
			"BaseToLocalFxRate": 1,
			"BookTypeId": 2,
			"Commission": 10,
			"CommissionCharges": [
			{
			"CommissionCharge": 10,
			"CommissionChargeId": 7
			}
			],
			"CustodianCounterpartyAccountId": 1,
			"ExecQuantity": 530,
			"ExecutingBrokerCounterpartyId": 1,
			"FeeCharges": [
			{
			"FeeCharge": 20,
			"FeeChargeId": 10
			}
			],
			"Fees": 20,
			"IsFinalized": false,
			"NetAmount": 41706.55,
			"PortfolioId": 1,
			"PositionTags": null,
			"PrincipalAmount": 41676.55,
			"Quantity": 530,
			"RouteId": 74665,
			"RouteName": "IMS-DEMO",
			"SettleAmount": 41706.55,
			"SettleCurrencySecurityId": 155,
			"SettlePrice": 78.635,
			"SettleToBaseFXRate": 1,
			"SettleToLocalFXRate": 1,
			"SettleToSystemFXRate": 1
		},
		{
			"BaseCurrencySecurityId": 155,
			"BaseToLocalFxRate": 1,
			"BookTypeId": 3,
			"Commission": 5,
			"CommissionCharges": [],
			"ExecQuantity": 530,
			"ExecutingBrokerCounterpartyId": 1,
			"FeeCharges": [],
			"Fees": 20,
			"IsFinalized": false,
			"NetAmount": 41706.55,
			"PortfolioId": 1,
			"PositionTags": null,
			"PrincipalAmount": 41676.55,
			"Quantity": 530,
			"RouteId": 74665,
			"RouteName": "IMS-DEMO",
			"SettleAmount": 41706.55,
			"SettleCurrencySecurityId": 155,
			"SettlePrice": 78.635,
			"SettleToBaseFXRate": 1,
			"SettleToLocalFXRate": 1,
			"SettleToSystemFXRate": 1
		},
		{
			"BaseCurrencySecurityId": 155,
			"BaseToLocalFxRate": 1,
			"BookTypeId": 3,
			"Commission": 5,
			"CommissionCharges": [],
			"ExecQuantity": 530,
			"ExecutingBrokerCounterpartyId": 1,
			"FeeCharges": [],
			"Fees": 20,
			"IsFinalized": false,
			"NetAmount": 41706.55,
			"PortfolioId": 1,
			"PositionTags": null,
			"PrincipalAmount": 41676.55,
			"Quantity": 530,
			"RouteId": 74665,
			"RouteName": "IMS-DEMO",
			"SettleAmount": 41706.55,
			"SettleCurrencySecurityId": 155,
			"SettlePrice": 78.635,
			"SettleToBaseFXRate": 1,
			"SettleToLocalFXRate": 1,
			"SettleToSystemFXRate": 1
		}
	]
}`

func TestCommissionValidCommissionChargesForStrategy(t *testing.T) {
	var testTransaction Transaction
	unmarhsalErr := json.Unmarshal([]byte(commissionTestObjectValid), &testTransaction)
	assert.Nil(t, unmarhsalErr)
	err := testTransaction.Allocations[2].Commission.Validate(testTransaction, "Commission", 2)
	assert.Nil(t, err)
}

var commissionTestObject = `{
	"Allocations": [
		{
			"BaseCurrencySecurityId": 155,
			"BaseToLocalFxRate": 1,
			"BookTypeId": 2,
			"CommissionCharges": [
			{
			"CommissionCharge": 0,
			"CommissionChargeId": 7
			}
			],
			"CustodianCounterpartyAccountId": 1,
			"ExecQuantity": 530,
			"ExecutingBrokerCounterpartyId": 1,
			"FeeCharges": [
			{
			"FeeCharge": 20,
			"FeeChargeId": 10
			}
			],
			"Fees": 20,
			"IsFinalized": false,
			"NetAmount": 41706.55,
			"PortfolioId": 1,
			"PositionTags": null,
			"PrincipalAmount": 41676.55,
			"Quantity": 530,
			"RouteId": 74665,
			"RouteName": "IMS-DEMO",
			"SettleAmount": 41706.55,
			"SettleCurrencySecurityId": 155,
			"SettlePrice": 78.635,
			"SettleToBaseFXRate": 1,
			"SettleToLocalFXRate": 1,
			"SettleToSystemFXRate": 1
			},
			{
			"BaseCurrencySecurityId": 155,
			"BaseToLocalFxRate": 1,
			"BookTypeId": 3,
			"Commission": 12,
			"CommissionCharges": [],
			"ExecQuantity": 530,
			"ExecutingBrokerCounterpartyId": 1,
			"FeeCharges": [],
			"Fees": 20,
			"IsFinalized": false,
			"NetAmount": 41706.55,
			"PortfolioId": 1,
			"PositionTags": null,
			"PrincipalAmount": 41676.55,
			"Quantity": 530,
			"RouteId": 74665,
			"RouteName": "IMS-DEMO",
			"SettleAmount": 41706.55,
			"SettleCurrencySecurityId": 155,
			"SettlePrice": 78.635,
			"SettleToBaseFXRate": 1,
			"SettleToLocalFXRate": 1,
			"SettleToSystemFXRate": 1
			}
		]
	}`

func TestCommissionWhenNotPresentInAllAllocations(t *testing.T) {
	var testTransaction Transaction
	unmarhsalErr := json.Unmarshal([]byte(commissionTestObject), &testTransaction)
	assert.Nil(t, unmarhsalErr)
	err := testTransaction.Allocations[1].Commission.Validate(testTransaction, "Commission", 1)
	assert.Equal(t, err, fmt.Errorf("Error in portfolioID 1. Invalid Commission for BookTypeId : 3 "))
}
