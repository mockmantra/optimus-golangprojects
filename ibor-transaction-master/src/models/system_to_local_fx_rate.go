package models

import (
	"fmt"
)

// SystemToLocalFXRate field
type SystemToLocalFXRate float64

// Validate validates the fields
func (value SystemToLocalFXRate) Validate(transaction Transaction, fieldName string) error {
	if transaction.SystemCurrencySecurityID == transaction.LocalCurrencySecurityID {
		if value != 1 {
			return fmt.Errorf("Invalid %s. The SystemCurrencySecurityID and LocalCurrencySecurityID are equal but the value of SystemToLocalFXRate is %d instead of 1", fieldName, int(value))
		}
	}
	return nil
}
