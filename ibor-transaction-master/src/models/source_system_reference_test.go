package models

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/httputil"
)

func TestSourceSystemReference_WhenNotAValidInteger(t *testing.T) {
	var sourceSystemReference SourceSystemReference = "7x"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadGateway)
	}))

	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SourceSystemReference"
	transaction := Transaction{}
	httputil.UserSessionToken = "123"

	err := sourceSystemReference.Validate(transaction, key)

	assert.EqualError(t, err, fmt.Sprintf("Invalid SourceSystemReference provided: %s", sourceSystemReference))
	httputil.UserSessionToken = ""
}

func TestSourceSystemReference_WhenAValidInteger(t *testing.T) {
	var sourceSystemReference SourceSystemReference = "7"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadGateway)
	}))

	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SourceSystemReference"
	transaction := Transaction{}
	httputil.UserSessionToken = "123"

	err := sourceSystemReference.Validate(transaction, key)

	assert.Nil(t, err)
	httputil.UserSessionToken = ""
}