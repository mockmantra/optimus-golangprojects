package models

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/httputil"
)

func TestSecurityID_WithErrorInSecurityAPIHttpCall(t *testing.T) {
	var securityID SecurityID = 7
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadGateway)
	}))

	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SecurityID"
	transaction := Transaction{}

	err := securityID.Validate(transaction, key)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. Service lookup failed", key))

}
func TestSecurityID_WithErrorSecurityNotFound(t *testing.T) {
	var securityID SecurityID = 7
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	}))

	httputil.UserSessionToken = "123"
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SecurityID"
	transaction := Transaction{}

	err := securityID.Validate(transaction, key)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. SecurityId: %d does not exist", key, securityID))
	httputil.UserSessionToken = "123"

}
func TestSecurityID_WithAssetClassNameIsNotEquityInSecurityAPIResponse(t *testing.T) {
	var securityID SecurityID = 7
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.Index(r.URL.Path, "securitymaster/v1/security") > -1 {
			_, err := w.Write([]byte("[{\"AssetClassName\":\"NonCash\",\"SecurityId\":5}]"))
			if err != nil {
				t.Error("Unable to write the stream")
			}
		} else {
			w.WriteHeader(http.StatusNotFound)
		}
	}))
	httputil.UserSessionToken = "123"
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SecurityID"
	transaction := Transaction{}

	err := securityID.Validate(transaction, key)

	assert.EqualError(t, err, "Invalid SecurityID. Expected asset class: Equity, Provided asset class: NonCash")
	httputil.UserSessionToken = ""
}

