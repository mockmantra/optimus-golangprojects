package models

import (
	"fmt"
	"reflect"
)

// ExecQuantity type
type ExecQuantity int

// Validate validates the field
func (value ExecQuantity) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	if !reflect.ValueOf(transaction.Allocations[allocationIndex].RouteID).IsNil() {
		if int(value) <= int(*transaction.Allocations[allocationIndex].Quantity) {
			return nil
		}
		return fmt.Errorf("Error in allocation portfolioID %d. Invalid %s :ExecQuantity should not be greater than Quantity", int(*transaction.Allocations[allocationIndex].PortfolioID), fieldName)
	}
	return fmt.Errorf("Error in allocation portfolioID %d. Invalid %s :RouteId not present", int(*transaction.Allocations[allocationIndex].PortfolioID), fieldName)
}
