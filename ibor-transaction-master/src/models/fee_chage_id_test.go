package models

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/httputil"
)

var feeChargeIDTestJSON = `{
	"Allocations": [
		{
			"ExecQuantity": 100,
			"PortfolioId" :12,
		  "Fees": 10,
		  "FeeCharges": [
			{
			  "FeeCharge": 10,
			  "FeeChargeId": 22
			},
			{
			  "FeeCharge": 0,
			  "FeeChargeId": 24
			}
		  ]
		},
		{
			"ExecQuantity": 0,
			"PortfolioId" :13,
			"Fees": 10,
			"FeeCharges": [
			  {
				"FeeCharge": 10,
				"FeeChargeId": 22
			  },
			  {
				"FeeCharge": 0,
				"FeeChargeId": 24
			  }
			]
		},
		{
			"ExecQuantity": 100,
			"Fees": 12,
			"PortfolioId" :14,
			"FeeCharges": [
			  {
				"FeeCharge": 10,
				"FeeChargeId": 22
			  },
			  {
				"FeeChargeId": 24
			  }
			]
		}
	]
}`

func TestFeeChargeIdValid(t *testing.T) {
	var testTransaction Transaction
	unMarshalError := json.Unmarshal([]byte(feeChargeIDTestJSON), &testTransaction)
	assert.Nil(t, unMarshalError)
	str := "[{\"Id\":32},{\"Id\":3}]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()

	feeCharges := *testTransaction.Allocations[0].FeeCharges
	err := feeCharges[0].FeeChargeID.Validate(testTransaction, "FeeChargeId", 0, 0)
	assert.Nil(t, err)
	httputil.UserSessionToken = ""
}

func TestFeeChargeIdInvalidExecQuantity(t *testing.T) {
	var testTransaction Transaction
	unMarshalError := json.Unmarshal([]byte(feeChargeIDTestJSON), &testTransaction)
	assert.Nil(t, unMarshalError)
	str := "[{\"Id\":32},{\"Id\":3}]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()

	feeCharges := *testTransaction.Allocations[0].FeeCharges
	err := feeCharges[0].FeeChargeID.Validate(testTransaction, "FeeChargeId", 1, 0)
	assert.NotNil(t, err)
	httputil.UserSessionToken = ""
}

func TestFeeChargeIdWithoutCommissionCharge(t *testing.T) {
	var testTransaction Transaction
	unMarshalError := json.Unmarshal([]byte(feeChargeIDTestJSON), &testTransaction)
	assert.Nil(t, unMarshalError)
	str := "[{\"Id\":32},{\"Id\":3}]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	httputil.UserSessionToken = "123"
	appconfig.BaseURI = ts.URL
	defer ts.Close()

	feeCharges := *testTransaction.Allocations[0].FeeCharges
	err := feeCharges[1].FeeChargeID.Validate(testTransaction, "FeeChargeId", 2, 1)
	assert.NotNil(t, err)
	httputil.UserSessionToken = ""
}

func TestFeeChargeIdInvalidLookUp(t *testing.T) {
	var testTransaction Transaction

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadGateway)
	}))
	unmarhsalErr := json.Unmarshal([]byte(feeChargeIDTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalErr)
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()

	feeCharges := *testTransaction.Allocations[0].FeeCharges
	err := feeCharges[0].FeeChargeID.Validate(testTransaction, "FeeChargeId", 0, 0)
	assert.Equal(t, err, fmt.Errorf("Error in allocation portfolioID 12. FeeChargeID : Service lookup failed"))
	httputil.UserSessionToken = ""
}

func TestFeeChargeIdInvalidUnmarshal(t *testing.T) {
	var testTransaction Transaction

	str := ""
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	unmarhsalErr := json.Unmarshal([]byte(feeChargeIDTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalErr)
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()

	feeCharges := *testTransaction.Allocations[0].FeeCharges
	err := feeCharges[0].FeeChargeID.Validate(testTransaction, "FeeChargeId", 0, 0)
	assert.Equal(t, err, fmt.Errorf("Error in portfolioID 12. FeeChargeID : Unmarshlling error"))
	httputil.UserSessionToken = ""
}

func TestFeeChargeIdInvalidNotFound(t *testing.T) {
	var testTransaction Transaction

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	}))
	unmarhsalErr := json.Unmarshal([]byte(feeChargeIDTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalErr)
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()

	feeCharges := *testTransaction.Allocations[0].FeeCharges
	err := feeCharges[0].FeeChargeID.Validate(testTransaction, "FeeChargeId", 0, 0)
	assert.Equal(t, err, fmt.Errorf("Error in portfolioID %d. Invalid FeeChargeID : %d does not exist", int(*testTransaction.Allocations[0].PortfolioID), int(*feeCharges[0].FeeChargeID)))

	httputil.UserSessionToken = ""
}
