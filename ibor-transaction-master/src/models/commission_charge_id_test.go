package models

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src"
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/httputil"
)

var commissionChargeIDTestJSON = `{
	"Allocations": [
		{
		  "ExecQuantity": 100,
			"Commission": 10,
			"PortfolioID" :12,
		  "CommissionCharges": [
			{
			  "CommissionCharge": 10,
			  "CommissionChargeId": 22
			},
			{
			  "CommissionCharge": 0,
			  "CommissionChargeId": 24
			}
		  ]
		},
		{
			"PortfolioID" :12,
			"Commission": 10,
			"CommissionCharges": [
			  {
				"CommissionCharge": 10,
				"CommissionChargeId": 22
			  },
			  {
				"CommissionCharge": 0,
				"CommissionChargeId": 24
			  }
			]
		},
		{
			"PortfolioID" :12,
			"ExecQuantity": 100,
			"Commission": 12,
			"CommissionCharges": [
			  {
				"CommissionCharge": 10,
				"CommissionChargeId": 22
			  },
			  {
				"CommissionChargeId": 24
			  }
			]
		}
	]
}`

func TestCommissionChargeIdValid(t *testing.T) {
	var testTransaction Transaction
	unmarhsalErr := json.Unmarshal([]byte(commissionChargeIDTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalErr)
	str := "[{\"Id\":32},{\"Id\":3}]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, error := w.Write([]byte(str))
		assert.Nil(t, error)
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"

	defer ts.Close()

	commCharges := *testTransaction.Allocations[0].CommissionCharges
	err := commCharges[0].CommissionChargeID.Validate(testTransaction, "CommissionChargeId", 0, 0)
	assert.Nil(t, err)

	httputil.UserSessionToken = ""
}

func TestCommissionChargeIdInvalid(t *testing.T) {
	var testTransaction Transaction
	unmarhsalErr := json.Unmarshal([]byte(commissionChargeIDTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalErr)
	str := ""
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, error := w.Write([]byte(str))
		assert.Nil(t, error)
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"

	defer ts.Close()

	commCharges := *testTransaction.Allocations[0].CommissionCharges
	err := commCharges[0].CommissionChargeID.Validate(testTransaction, "CommissionChargeId", 0, 0)
	assert.Equal(t, err, fmt.Errorf("Error in portfolioID 12. CommissionChargeID : Unmarshlling error"))

	httputil.UserSessionToken = ""
}

func TestCommissionChargeIdInvalidExecQuantity(t *testing.T) {
	var testTransaction Transaction
	unmarhsalErr := json.Unmarshal([]byte(commissionChargeIDTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalErr)
	str := "[{\"Id\":32},{\"Id\":3}]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, error := w.Write([]byte(str))
		assert.Nil(t, error)
	}))
	appconfig.BaseURI = ts.URL
	defer ts.Close()

	commCharges := *testTransaction.Allocations[0].CommissionCharges
	err := commCharges[0].CommissionChargeID.Validate(testTransaction, "CommissionChargeId", 1, 0)
	assert.Equal(t, err, fmt.Errorf("Error in allocation portfolioID %d. Invalid CommissionChargeID: ExecQuantity not valid", int(*testTransaction.Allocations[0].PortfolioID)))
	httputil.UserSessionToken = ""
}

func TestCommissionChargeIdWithoutCommissionCharge(t *testing.T) {
	var testTransaction Transaction
	unmarhsalErr := json.Unmarshal([]byte(commissionChargeIDTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalErr)
	str := "[{\"Id\":32},{\"Id\":3}]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, error := w.Write([]byte(str))
		assert.Nil(t, error)
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"

	defer ts.Close()

	commCharges := *testTransaction.Allocations[0].CommissionCharges
	err := commCharges[1].CommissionChargeID.Validate(testTransaction, "CommissionChargeId", 2, 1)
	assert.Equal(t, err, fmt.Errorf("Error in allocation portfolioID %d. Invalid CommissionChargeID, Missing CommissionCharge for CommissionChargeID %d", int(*testTransaction.Allocations[0].PortfolioID), int(*commCharges[1].CommissionChargeID)))
	httputil.UserSessionToken = ""
}

func TestCommissionChargeIdInvalidNotFound(t *testing.T) {
	var testTransaction Transaction

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	}))
	unmarhsalErr := json.Unmarshal([]byte(commissionChargeIDTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalErr)
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()

	commCharges := *testTransaction.Allocations[0].CommissionCharges
	err := commCharges[0].CommissionChargeID.Validate(testTransaction, "CommissionChargeId", 0, 0)
	assert.Equal(t, err, fmt.Errorf("Error in allocation portfolioID %d. Invalid CommissionChargeID : %d does not exist", int(*testTransaction.Allocations[0].PortfolioID), int(*commCharges[0].CommissionChargeID)))
	httputil.UserSessionToken = ""
}

func TestCommissionChargeIdInvalidLookUp(t *testing.T) {
	var testTransaction Transaction

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadGateway)
	}))
	unmarhsalErr := json.Unmarshal([]byte(commissionChargeIDTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalErr)
	appconfig.BaseURI = ts.URL
	defer ts.Close()
	httputil.UserSessionToken = "123"
	commCharges := *testTransaction.Allocations[0].CommissionCharges
	err := commCharges[0].CommissionChargeID.Validate(testTransaction, "CommissionChargeId", 0, 0)
	assert.Equal(t, err, fmt.Errorf("Error in allocation portfolioID 12. CommissionChargeID : Service lookup failed"))
	httputil.UserSessionToken = ""
}
