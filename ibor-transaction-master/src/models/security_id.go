package models

import (
	"fmt"
	"strings"

	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/logger"
)

// SecurityID field
type SecurityID int

// Validate validates the fields
func (value SecurityID) Validate(transaction Transaction, fieldName string) error {
	securities, err := NewSecurityRequest(int(value))
	if err == nil {
		logger.Log.Infof("Validating security. Symbol:  %s", securities[0].Symbol)
		if securities[0].AssetClassName != "Equity" {
			return fmt.Errorf("Invalid %s. Expected asset class: Equity, Provided asset class: %s", fieldName, securities[0].AssetClassName)
		}
	} else {
		if strings.Contains(err.Error(), "error:404") {
			return fmt.Errorf("Invalid %s. SecurityId: %d does not exist", fieldName, value)
		}
		return fmt.Errorf("Invalid %s. Service lookup failed", fieldName)

	}
	return nil
}
