package models

import (
	"encoding/json"
	"fmt"
	"strings"

	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/httputil"
)

// CurrencySecurityID field
type CurrencySecurityID int

// Security type
type Security struct {
	SecurityID     int    `json:"SecurityId"`
	AssetClassName string `json:"AssetClassName"`
	Symbol         string `json:"Symbol"`
}

var securitiesInPosition = make(map[int]Security)

const securityURL = "securitymaster/v1/security?securityId=%v"

// Validate validates the fields
func (value CurrencySecurityID) Validate(transaction Transaction, fieldName string) error {
	securities, err := NewSecurityRequest(int(value))
	if err == nil {
		if securities[0].AssetClassName != "Cash" {
			return fmt.Errorf("Invalid %s. Expected AssetClass : Cash, Provided Assetclass : %s", fieldName, securities[0].AssetClassName)
		}
	} else {
		if strings.Contains(err.Error(), "error:404") {
			return fmt.Errorf("Invalid %s. %s %d  does not exist", fieldName, fieldName, int(value))
		}
		return fmt.Errorf("%s : Service lookup failed", fieldName)
	}
	return nil
}

// NewSecurityRequest fetches the Orders by OrderId
func NewSecurityRequest(securityID int) ([]Security, error) {
	if security, ok := securitiesInPosition[securityID]; ok {
		return []Security{security}, nil
	}
	bodyBytes, err := httputil.NewRequest("GET", fmt.Sprintf(securityURL, securityID), nil)
	var securities []Security
	if err == nil {
		errMarshal := json.Unmarshal(bodyBytes, &securities)
		if errMarshal != nil {
			return securities, errMarshal
		}
	}
	return securities, err
}

// GetSecurities gets the securities for supplied Ids
func GetSecurities(securityIDs []int) error {
	bodyBytes, err := httputil.NewRequest("GET", fmt.Sprintf(securityURL, strings.Trim(strings.Replace(fmt.Sprint(securityIDs), " ", ",", -1), "[]")), nil)
	var securities []Security
	if err == nil {
		err = json.Unmarshal(bodyBytes, &securities)
		if err == nil {
			for _, security := range securities {
				securitiesInPosition[security.SecurityID] = security
			}
		}
	}
	return err
}
