package models

// AllocationValidator interface
type AllocationValidator interface {
	Validate(transaction Transaction, fieldName string, allocationIndex int) error
}

// Allocation allocation
type Allocation struct {
	BaseCurrencySecurityID         *BaseCurrencySecurityID         `json:"BaseCurrencySecurityId"`
	BookTypeID                     *BookTypeID                     `json:"BookTypeId"`
	CommissionCharges              *CommissionChargesSlice         `json:"CommissionCharges"`
	CustodianCounterpartyAccountID *CustodianCounterpartyAccountID `json:"CustodianCounterpartyAccountId,omitempty"`
	Commission                     *Commission                     `json:"Commission,omitempty"`
	ExecQuantity                   *ExecQuantity                   `json:"ExecQuantity,omitempty"`
	ExecutingBrokerCounterpartyID  *ExecutingBrokerCounterpartyID  `json:"ExecutingBrokerCounterpartyId,omitempty"`
	FeeCharges                     *FeeChargesSlice                `json:"FeeCharges"`
	Fees                           *Fee                            `json:"Fees,omitempty"`
	IsFinalized                    bool                            `json:"IsFinalized"`
	BaseToLocalFXRate              *FxRate                         `json:"BaseToLocalFXRate"`
	NetAmount                      *NetAmount                      `json:"NetAmount,omitempty"`
	PortfolioID                    *PortfolioID                    `json:"PortfolioId"`
	PositionTags                   *PositionTagsSlice              `json:"PositionTags"`
	PrincipalAmount                *PrincipalAmount                `json:"PrincipalAmount,omitempty"`
	Quantity                       *int                            `json:"Quantity"`
	RouteID                        *int                            `json:"RouteId,omitempty"`
	RouteName                      *RouteName                      `json:"RouteName,omitempty"`
	SettleAmount                   *SettleAmount                   `json:"SettleAmount,omitempty"`
	SettleCurrencySecurityID       *SettleCurrencySecurityID       `json:"SettleCurrencySecurityId,omitempty"`
	SettlePrice                    *SettlePrice                    `json:"SettlePrice,omitempty"`
	SettleToBaseFXRate             *FxRate                         `json:"SettleToBaseFXRate,omitempty"`
	SettleToLocalFXRate            *FxRate                         `json:"SettleToLocalFXRate,omitempty"`
	SettleToSystemFXRate           *FxRate                         `json:"SettleToSystemFXRate,omitempty"`
}
