package interfaces

import (
	"stash.ezesoft.net/imsacnt/ibor-transaction-master/src/converter/models"
)

type ITransactionConverter interface {
	ConvertToImpactGeneratorTransaction(*models.Transaction) *models.ImpactTransaction
}
