package workdistributionclient

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"github.com/pkg/errors"
	"stash.ezesoft.net/ipc/ezeutils"
)

const (
	defaultRetryEnrollmentAttempts               = 4
	defaultRetryEnrollmentPeriod   time.Duration = 5 * time.Second
	defaultRetryPollDelay          time.Duration = 1 * time.Second
)

var (
	defaultBaseURL string
)

func init() {
	defaultBaseURL = os.Getenv("IMS_BASE_URL")
}

type wdOptions struct {
	pollBeforeTTL           bool
	baseURL                 string
	retryEnrollmentPeriod   time.Duration
	retryPollDelay          time.Duration
	retryEnrollmentAttempts int
}

// WDClient is a struct which represents a work distribution client
type WDClient struct {
	options wdOptions
}

// NewClient creates an instance of work distribution client
func NewClient(optionsFuncs ...func(*wdOptions) error) (*WDClient, error) {
	options := wdOptions{
		baseURL:                 defaultBaseURL,
		retryEnrollmentPeriod:   defaultRetryEnrollmentPeriod,
		retryPollDelay:          defaultRetryPollDelay,
		retryEnrollmentAttempts: defaultRetryEnrollmentAttempts,
	}
	for _, opt := range optionsFuncs {
		err := opt(&options)
		if err != nil {
			return nil, errors.Wrap(err, "processing options")
		}
	}
	if options.baseURL == "" {
		return nil, errors.New("Base URL hasn't been provided neither via options, nor via IMS_BASE_URL environment variable")
	}
	return &WDClient{options: options}, nil
}

// OptionRetryPollDelay is to set a delay between subsequent poll attempts. Shouldn't be set too large, i.e. if it exceeds worker's TTL then most likely worker will expire between polls if omne poll fails
// Default is 1 sec
func OptionRetryEnrollmentAttempts(n int) func(*wdOptions) error {
	return func(options *wdOptions) error {
		options.retryEnrollmentAttempts = n
		return nil
	}
}

// OptionRetryPollDelay is to set a delay between subsequent poll attempts. Shouldn't be set too large, i.e. if it exceeds worker's TTL then most likely worker will expire between polls if omne poll fails
// Default is 1 sec
func OptionRetryPollDelay(p time.Duration) func(*wdOptions) error {
	return func(options *wdOptions) error {
		options.retryPollDelay = p
		return nil
	}
}

// OptionRetryEnrollmentPeriod set retry period for worker's enrollment into WD service
// Default is 5 sec
func OptionRetryEnrollmentPeriod(p time.Duration) func(*wdOptions) error {
	return func(options *wdOptions) error {
		options.retryEnrollmentPeriod = p
		return nil
	}
}

// OptionPollBeforeTTL is used to enable client to poll for the duration less than remaining worker's TTL, instead of an infinite poll (limited on WD serivce's side to 49sec)
// enabling this may help to reduce redistribution events should poll not hoit WD service and time out, and if worker's TTL is less than default networking timeout
// defualt is 'false' for backwards compatibiility
func OptionPollBeforeTTL(b bool) func(*wdOptions) error {
	return func(options *wdOptions) error {
		options.pollBeforeTTL = b
		return nil
	}
}

// OptionBaseURL used to set base URL to access work distribution service, i.e. http://internal-castle.ezesoftcloud.com, or http://eze-route:8001
// Default is read from IMS_BASE_URL environment variable
func OptionBaseURL(url string) func(wdOptions *wdOptions) error {
	return func(wdOptions *wdOptions) error {
		wdOptions.baseURL = url
		return nil
	}
}

// SubscribeToWork is obsolete - use WDClient's SubscribeToWork method instead
func SubscribeToWork(ctx context.Context, workerGroupName, workType string, metadata *map[string]string, optionsFuncs ...func(*wdOptions) error) (chan WorkerAssignments, chan ClientStatus, chan bool, error) {
	options := wdOptions{
		baseURL:                 defaultBaseURL,
		retryEnrollmentPeriod:   defaultRetryEnrollmentPeriod,
		retryPollDelay:          defaultRetryPollDelay,
		retryEnrollmentAttempts: defaultRetryEnrollmentAttempts,
	}
	for _, opt := range optionsFuncs {
		err := opt(&options)
		if err != nil {
			// log error
			return nil, nil, nil, errors.Wrap(err, "Failed to configure option")
		}
	}
	if options.baseURL == "" {
		return nil, nil, nil, errors.New("Base URL hasn't been provided neither via options, nor via IMS_BASE_URL environment variable")
	}
	wdc := WDClient{options: options}
	return wdc.SubscribeToWork(ctx, workerGroupName, workType, metadata)
}

// SubscribeToWork method is used to create a worker and subscribe this worker to work distribution service
// Note: caller must consume all the channels, otherwise daemon will block.
func (wdc *WDClient) SubscribeToWork(ctx context.Context, workerGroupName, workType string, metadata *map[string]string) (chan WorkerAssignments, chan ClientStatus, chan bool, error) {
	assingmentsChan := make(chan WorkerAssignments)
	clientStatusChan := make(chan ClientStatus, 100)
	enrollNewWorker := true
	noWork := WorkerAssignments{WorkItems: make(WorkItems, 0)}
	var assignment WorkerAssignments
	var remainingTTL string
	var err error
	quitWorker := make(chan string)
	quit := make(chan bool)

	go func() {
		failures := 0
		timeWorkerRetires := time.Now()
		var workerID string
		var assignmentIndex string
		var previousPollSuccessful bool
		var workerTTL time.Duration
		var recoverable bool
		for {
			if enrollNewWorker { // first time, and after every polling failure
				assignment, remainingTTL, err = wdc.announceWorker(ctx, workerGroupName, workType, metadata)
				if remainingTTL != "" {
					workerTTL, err = time.ParseDuration(remainingTTL)
					if err == nil {
						timeWorkerRetires = time.Now().Add(workerTTL)
					} else {
						select { // send error - non blocking.
						case clientStatusChan <- ClientStatus{Error: errors.Wrapf(err, "failed to parse worker's RemainigTTL '%s' from initial creation", remainingTTL)}:
						default:
						}
						workerTTL = time.Second * 10 // minimum TTL in consul/WD is 10s
					}
				}
				enrollNewWorker = err != nil
				if enrollNewWorker {
					select { // send error - non blocking
					case clientStatusChan <- ClientStatus{Error: err, Failures: failures}:
					default:
					}
					select {
					case <-time.After(wdc.options.retryEnrollmentPeriod):
					case <-ctx.Done():
						quitWorker <- workerID
						return
					}
					failures++
					if failures >= wdc.options.retryEnrollmentAttempts {
						// we could not enlist long enough - let someone else take the work.
						assingmentsChan <- noWork
					}
					continue
				} else {
					assingmentsChan <- assignment
					previousPollSuccessful = true
				}
				workerID = assignment.Id
				assignmentIndex = "0"
				failures = 0
				clientStatusChan <- ClientStatus{Message: "Try Polling worker"}
				time.Sleep(time.Second) // sleep the worker for 1 sec after enrolling to allow it's presence to propagate to all consul nodes
			}

			// this is why we're here for: we either announced or polled successfully, send results over the channel
			assignment, remainingTTL, recoverable, err = wdc.pollWorker(ctx, workerID, assignmentIndex, timeWorkerRetires)
			if remainingTTL != "" {
				duration, err := time.ParseDuration(remainingTTL)
				if err == nil {
					timeWorkerRetires = time.Now().Add(duration)
				} else {
					select { // send error - non blocking.
					case clientStatusChan <- ClientStatus{Error: errors.Wrapf(err, "failed to parse worker's RemainigTTL '%s' from assignment", remainingTTL)}:
					default:
					}
					duration = time.Second * 10 // minimum TTL in consul/WD is 10s
				}
			}
			if err == nil {
				assignmentIndex = assignment.Index
				assingmentsChan <- assignment
				enrollNewWorker = false
				previousPollSuccessful = true
			} else {
				if ctx.Err() != nil {
					quitWorker <- workerID
					return
				}
				if previousPollSuccessful && remainingTTL == "" {
					timeWorkerRetires = time.Now().Add(workerTTL * 2 / 3)
				}
				previousPollSuccessful = false
				if !recoverable || time.Now().After(timeWorkerRetires) {
					enrollNewWorker = true
					select { // send a warning - non blocking.
					case clientStatusChan <- ClientStatus{Warning: errors.Wrapf(err, "Retiring failed worker '%s'", workerID).Error()}:
					default:
					}
					assingmentsChan <- noWork
					err := wdc.resign(ctx, workerID)
					if err != nil {
						clientStatusChan <- ClientStatus{Warning: errors.Wrapf(err, "Failed to retire worker '%s'", workerID).Error()}
					}
				} else {
					select { // send a warning - non blocking.
					case clientStatusChan <- ClientStatus{Warning: errors.Wrapf(err, "Retrying failed poll until '%v'", timeWorkerRetires).Error()}:
					default:
					}
					select {
					case <-ctx.Done():
						quitWorker <- workerID
						return
					case <-time.After(wdc.options.retryPollDelay):
					}
				}
			}

			// Always send assignment, even if it hasn't changed
			select {
			case <-ctx.Done():
				quitWorker <- workerID
				//need to unlock here, but api doesn't currently allow that. or the api should be updated so that removing worker should release locks
				return
			default:
			}
		}
	}()

	go func() {
		workerID := <-quitWorker // block until quit
		// use fresh context to resign (with 2 sec timeout), since captured one most likely is cancelled, if this is due to shutdown of the service
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
		defer cancel()
		err := wdc.resign(ctx, workerID)
		if err != nil {
			clientStatusChan <- ClientStatus{Error: err}
		}
		clientStatusChan <- ClientStatus{Message: "Quit and resign"}
		close(quit)
	}()

	return assingmentsChan, clientStatusChan, quit, nil
}

func (wdc *WDClient) resign(ctx context.Context, workerID string) error {

	req, err := http.NewRequest(http.MethodDelete, wdc.options.baseURL+"/api/work-distribution/v1/workers/"+workerID, nil)
	if err != nil {
		return err
	}
	req = req.WithContext(ctx)

	ezeutils.SetRequestHeadersFromContext(ctx, req, nil)

	response, err := ezeutils.HttpClient.Do(req)
	if err != nil {
		return err
	}

	if response.StatusCode != http.StatusNoContent {
		return errors.Errorf("Deleting worker did not return %d, %s", response.StatusCode, response.Status)
	}
	return nil
}

// AnnounceWorker tells worker distribution service that a worker is available and should be added to the worker pool
func (wdc *WDClient) announceWorker(ctx context.Context, workerGroupName, workType string, metadata *map[string]string) (WorkerAssignments, string, error) {
	var response WorkerAssignments
	var remainingTTL string

	type workApplicationForm struct {
		WorkerGroup string
		WorkType    string
		Metadata    *map[string]string
	}

	application := workApplicationForm{
		WorkerGroup: workerGroupName,
		WorkType:    workType,
		Metadata:    metadata,
	}
	queryBody, err := json.Marshal(application)
	if err != nil {
		return response, remainingTTL, err
	}
	req, err := http.NewRequest(http.MethodPost, wdc.options.baseURL+"/api/work-distribution/v1/workers", bytes.NewBuffer(queryBody))
	if err != nil {
		return response, remainingTTL, err
	}
	req = req.WithContext(ctx)

	ezeutils.SetRequestHeadersFromContext(ctx, req, nil)

	req.Header.Set("Content-Type", "application/json")

	httpCl := ezeutils.HttpClient
	resp, err := httpCl.Do(req)

	if err != nil {
		err = errors.Wrap(err, "Error when sending work distribution post request")
		if resp != nil {
			return response, remainingTTL, errors.Wrap(err, resp.Status)
		}
		return response, remainingTTL, errors.Wrap(err, "Internal Error: got nil response")
	}
	defer resp.Body.Close()

	remainingTTLHeader := resp.Header["X-Remaining-Ttl"]
	if len(remainingTTLHeader) > 0 && len(remainingTTLHeader[0]) > 0 {
		remainingTTL = remainingTTLHeader[0]
	}

	switch resp.StatusCode {
	case 201:
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return response, remainingTTL, errors.Wrap(err, "Error when reading work distribution response body")
		}
		err = json.Unmarshal(body, &response)
		if err != nil {
			return response, remainingTTL, errors.Wrap(err, "announce worker: error when unmarshalling work distribution response body")
		}
		distributionIndex := resp.Header["X-Workdistribution-Index"]
		if len(distributionIndex) > 0 && len(distributionIndex[0]) > 0 {
			response.Index = distributionIndex[0]
		}
		return response, remainingTTL, nil
	default:
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("respAW3 scode %v\n", resp)
			return response, remainingTTL, errors.Wrap(err, "Error when reading work distribution response body")
		}

		var errorResp map[string]string
		err = json.Unmarshal(body, &errorResp)
		if err != nil {
			return response, remainingTTL, errors.Wrap(err, "announce worker: error when unmarshalling work distribution response body")
		}

		if errorMessage, ok := errorResp["message"]; ok {
			return response, remainingTTL, errors.Wrap(err, errorMessage)
		}
		return response, remainingTTL, errors.New(resp.Status)
	}
}

// PollWorker initiates a long poll against the service that returns either the same work data or new work data and a new index
func (wdc *WDClient) pollWorker(ctx context.Context, workerID, index string, timeWorkerRetires time.Time) (WorkerAssignments, string, bool, error) {
	var response WorkerAssignments
	var remainingTTL string
	req, err := http.NewRequest(http.MethodGet, wdc.options.baseURL+"/api/work-distribution/v1/workers/"+workerID+"?index="+index, nil)
	if err != nil {
		return response, remainingTTL, true, err
	}

	deadline := time.Now().Add(49 * time.Second)
	// move back retirement time to give us opportunity to poll again before worker retires
	timeWorkerRetires = timeWorkerRetires.Add(-wdc.options.retryPollDelay * 2)
	if wdc.options.pollBeforeTTL && deadline.After(timeWorkerRetires) {
		deadline = timeWorkerRetires
		// make sure we have at least a second to make a call
		minDeadline := time.Now().Add(time.Second)
		if deadline.Before(minDeadline) {
			deadline = minDeadline
		}
	}

	// create scratch context with a random activity id
	hdrCtx := ezeutils.WithActivityID(context.Background(), "")

	// set deadline into scratch context
	hdrCtx, cancel := context.WithDeadline(hdrCtx, deadline)
	defer cancel()

	// request is sent with the origianl context
	ctx, cancel1 := context.WithDeadline(ctx, deadline.Add(time.Second))
	defer cancel1()
	req = req.WithContext(ctx)

	// set activity id and the deadline as request headers (note that deadline only goes into the x-request-timeout header, it's not part of the request context)
	ezeutils.SetRequestHeadersFromContext(hdrCtx, req, nil)

	httpCl := ezeutils.HttpClient
	resp, err := httpCl.Do(req)

	if err != nil {
		err = errors.Wrap(err, "Error when sending work distribution get request")
		if resp != nil {
			return response, remainingTTL, true, errors.Wrap(err, resp.Status)
		}
		return response, remainingTTL, true, errors.Wrap(err, "Internal Error: got nil response")
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	remainingTTLHeader := resp.Header["X-Remaining-Ttl"]
	if len(remainingTTLHeader) > 0 && len(remainingTTLHeader[0]) > 0 {
		remainingTTL = remainingTTLHeader[0]
	}

	switch resp.StatusCode {
	case 200:
		if err != nil {
			return response, remainingTTL, true, errors.Wrap(err, "error when reading work distribution response body")
		}
		err = json.Unmarshal(body, &response)
		if err != nil {
			return response, remainingTTL, true, errors.Wrapf(err, "poll worker: error when unmarshalling work distribution response body='%s'", string(body))
		}

		distributionIndex := resp.Header["X-Workdistribution-Index"]
		if len(distributionIndex) > 0 && len(distributionIndex[0]) > 0 {
			response.Index = distributionIndex[0]
		}

		return response, remainingTTL, true, nil
	default:
		if err != nil {
			return response, remainingTTL, true, errors.Wrapf(err, "error when reading work distribution response body, code=%d, status='%s'", resp.StatusCode, resp.Status)
		}
		recoverable := resp.StatusCode != 410 // we can't recover if the worker is gone (code 410)
		return response, remainingTTL, recoverable, fmt.Errorf("failed to get assignments, code=%d, status='%s', body='%s'", resp.StatusCode, resp.Status, string(body))
	}
}

// UpdateWorkItem is now obsolete - use WDClient's UpdateWorkItem method instead
func UpdateWorkItem(ctx context.Context, workType, workID string, metadata interface{}, optionsFuncs ...func(*wdOptions) error) (*WorkWorkItem, error) {
	options := wdOptions{
		baseURL:                 defaultBaseURL,
		retryEnrollmentPeriod:   defaultRetryEnrollmentPeriod,
		retryPollDelay:          defaultRetryPollDelay,
		retryEnrollmentAttempts: defaultRetryEnrollmentAttempts,
	}
	for _, opt := range optionsFuncs {
		err := opt(&options)
		if err != nil {
			return nil, errors.Wrap(err, "Failed to configure option")
		}
	}
	if options.baseURL == "" {
		return nil, errors.New("Base URL hasn't been provided neither via options, nor via IMS_BASE_URL environment variable")
	}
	wdc := WDClient{options: options}
	return wdc.UpdateWorkItem(ctx, workType, workID, metadata)
}

// UpdateWorkItem updates metadata of the work item identified by workType and workID
func (wdc *WDClient) UpdateWorkItem(ctx context.Context, workType, workID string, metadata interface{}) (*WorkWorkItem, error) {
	workItem := WorkWorkItem{Id: workID, Metadata: metadata, Enabled: true}
	workData, err := json.Marshal(workItem)
	if err != nil {
		return nil, errors.Wrap(err, "Unable to marshal work items")
	}

	req, err := http.NewRequest(http.MethodPut, wdc.options.baseURL+"/api/work-distribution/v1/work/"+workType+"/"+workID, bytes.NewBuffer(workData))
	if err != nil {
		return nil, errors.Wrap(err, "failed to create new hhtp request object")
	}
	req = req.WithContext(ctx)

	ezeutils.SetRequestHeadersFromContext(ctx, req, nil)

	httpCl := ezeutils.HttpClient
	resp, err := httpCl.Do(req)

	if err != nil {
		err = errors.Wrap(err, "Error when updating work item through work distribution service")
		if resp != nil {
			err = errors.Wrap(err, resp.Status)
		} else {
			err = errors.Wrap(err, "Internal Error: nil response")
		}
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	switch resp.StatusCode {
	case 200:
		if err != nil {
			return nil, errors.Wrap(err, "Error when reading work distribution response body")
		}
		response := &WorkWorkItem{}
		err = json.Unmarshal(body, response)
		if err != nil {
			err = errors.Wrap(err, "Error unmarshaling work distribution 'work' response body")
			return nil, err
		}
		return response, nil
	default:
		return nil, errors.New(resp.Status)
	}
}

// GetWorkItem is now obsolete - use WDClient's GetWorkItem method instead
func GetWorkItem(ctx context.Context, workType, workID string, optionsFuncs ...func(*wdOptions) error) (*WorkWorkItem, error) {
	options := wdOptions{
		baseURL:                 defaultBaseURL,
		retryEnrollmentPeriod:   defaultRetryEnrollmentPeriod,
		retryPollDelay:          defaultRetryPollDelay,
		retryEnrollmentAttempts: defaultRetryEnrollmentAttempts,
	}
	for _, opt := range optionsFuncs {
		err := opt(&options)
		if err != nil {
			return nil, errors.Wrap(err, "Failed to configure option")
		}
	}
	if options.baseURL == "" {
		return nil, errors.New("Base URL hasn't been provided neither via options, nor via IMS_BASE_URL environment variable")
	}
	wdc := WDClient{options: options}
	return wdc.GetWorkItem(ctx, workType, workID)
}

// GetWorkItem gets metadata of the work item identified by workType and workID
func (wdc *WDClient) GetWorkItem(ctx context.Context, workType, workID string) (*WorkWorkItem, error) {

	req, err := http.NewRequest(http.MethodGet, wdc.options.baseURL+"/api/work-distribution/v1/work/"+workType+"/"+workID, nil)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create new hhtp request object")
	}
	req = req.WithContext(ctx)

	ezeutils.SetRequestHeadersFromContext(ctx, req, nil)

	httpCl := ezeutils.HttpClient
	resp, err := httpCl.Do(req)

	if err != nil {
		err = errors.Wrap(err, "Error when getting work item through work distribution service")
		if resp != nil {
			err = errors.Wrap(err, resp.Status)
		} else {
			err = errors.Wrap(err, "Internal Error: nil response")
		}
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	switch resp.StatusCode {
	case 200:
		if err != nil {
			return nil, errors.Wrap(err, "Error when reading work distribution response body")
		}
		response := &WorkWorkItem{}
		err = json.Unmarshal(body, response)
		if err != nil {
			err = errors.Wrap(err, "Error unmarshaling work distribution 'work' response body")
			return nil, err
		}
		return response, nil
	default:
		return nil, errors.New(resp.Status)
	}
}

// LockWorkItems is obsolete - use WDClient's LockWorkItems method instead
func LockWorkItems(ctx context.Context, workerID string, workItems []WorkItem, optionsFuncs ...func(*wdOptions) error) error {
	options := wdOptions{
		baseURL:                 defaultBaseURL,
		retryEnrollmentPeriod:   defaultRetryEnrollmentPeriod,
		retryPollDelay:          defaultRetryPollDelay,
		retryEnrollmentAttempts: defaultRetryEnrollmentAttempts,
	}
	for _, opt := range optionsFuncs {
		err := opt(&options)
		if err != nil {
			return errors.Wrap(err, "Failed to configure option")
		}
	}
	if options.baseURL == "" {
		return errors.New("Base URL hasn't been provided neither via options, nor via IMS_BASE_URL environment variable")
	}
	wdc := WDClient{options: options}
	return wdc.LockWorkItems(ctx, workerID, workItems)
}

// LockWorkItems tells worker distribution service to lock a list of work items to worker's ID
func (wdc *WDClient) LockWorkItems(ctx context.Context, workerID string, workItems []WorkItem) error {
	workItemSlice := make([]string, 0)
	for _, workItem := range workItems {
		workItemSlice = append(workItemSlice, workItem.WorkId)
	}

	workData, err := json.Marshal(workItemSlice)
	if err != nil {
		return errors.Wrap(err, "Unable to marshal work items")
	}

	req, err := http.NewRequest(http.MethodPost, wdc.options.baseURL+"/api/work-distribution/v1/workers/"+workerID+"/locks", bytes.NewBuffer(workData))
	if err != nil {
		return err
	}
	req = req.WithContext(ctx)

	ezeutils.SetRequestHeadersFromContext(ctx, req, nil)

	req.Header.Set("Content-Type", "application/json")

	httpCl := ezeutils.HttpClient
	resp, err := httpCl.Do(req)

	if err != nil {
		err = errors.Wrap(err, "Error when sending work distribution post request")
		if resp != nil {
			return errors.Wrap(err, resp.Status)
		}
		return errors.Wrap(err, "Internal Error: got nil response")
	}
	defer resp.Body.Close()

	switch resp.StatusCode {
	case 200:
		return nil
	default:
		return errors.New(resp.Status)
	}
}

// LockWorkItem tells work distribution service to lock a work item to worker's ID
func (wdc *WDClient) LockWorkItem(ctx context.Context, workerID, workItemID string) error {
	if workerID == "" || workItemID == "" {
		return errors.New("LockWorkItem: invalid arguments")
	}
	req, err := http.NewRequest(http.MethodPut, wdc.options.baseURL+"/api/work-distribution/v1/workers/"+workerID+"/locks/"+workItemID, nil)
	if err != nil {
		return err
	}
	req = req.WithContext(ctx)

	ezeutils.SetRequestHeadersFromContext(ctx, req, nil)

	httpCl := ezeutils.HttpClient
	resp, err := httpCl.Do(req)

	if err != nil {
		err = errors.Wrap(err, "Error when sending work distribution PUT request")
		if resp != nil {
			return errors.Wrap(err, resp.Status)
		}
		return errors.Wrap(err, "Internal Error: got nil response")
	}
	defer resp.Body.Close()

	switch resp.StatusCode {
	case 200:
		return nil
	default:
		return errors.New(resp.Status)
	}
}

// UnlockWorkItem tells work distribution service to unlock a work item from worker's ID
func (wdc *WDClient) UnlockWorkItem(ctx context.Context, workerID, workItemID string) error {
	if workerID == "" || workItemID == "" {
		return errors.New("UnlockWorkItem: invalid arguments")
	}
	req, err := http.NewRequest(http.MethodDelete, wdc.options.baseURL+"/api/work-distribution/v1/workers/"+workerID+"/locks/"+workItemID, nil)
	if err != nil {
		return err
	}
	req = req.WithContext(ctx)

	ezeutils.SetRequestHeadersFromContext(ctx, req, nil)

	httpCl := ezeutils.HttpClient
	resp, err := httpCl.Do(req)

	if err != nil {
		err = errors.Wrap(err, "Error when sending work distribution DELETE request")
		if resp != nil {
			return errors.Wrap(err, resp.Status)
		}
		return errors.Wrap(err, "Internal Error: got nil response")
	}
	defer resp.Body.Close()

	switch resp.StatusCode {
	case 204:
		return nil
	case 404:
		return nil
	default:
		return errors.New(resp.Status)
	}
}
