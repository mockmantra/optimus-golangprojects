package workdistributionclient

import "context"

// IWdClient - interface for accessing WorkdistributionClient
type IWdClient interface {
	SubscribeToWork(ctx context.Context, workerGroupName, workType string, metadata *map[string]string) (chan WorkerAssignments, chan ClientStatus, chan bool, error)
	GetWorkItem(ctx context.Context, workType, workID string) (*WorkWorkItem, error)
	UpdateWorkItem(ctx context.Context, workType, workID string, metadata interface{}) (*WorkWorkItem, error)
	LockWorkItems(ctx context.Context, workerID string, workItems []WorkItem) error
	LockWorkItem(ctx context.Context, workerID, workItemID string) error
	UnlockWorkItem(ctx context.Context, workerID, workItemID string) error
}
