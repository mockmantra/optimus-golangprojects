package ezeutils

// StatusError represents an error with an associated HTTP status code.
type StatusError struct {
	Code int
	Err  error
}

// Allows StatusError to satisfy the error interface.
func (se StatusError) Error() string {
	return se.Err.Error()
}

// Returns our HTTP status code.
func (se StatusError) Status() int {
	return se.Code
}

// Returns true if HTTP status code is a failure
func (se StatusError) Failed() bool {
	group := se.Code / 100
	return group > 3
}
