package ezeutils

import (
	"net/http"
	"time"
)

const (
	MaxIdleConnections int           = 12
	IdleConnTimeoutSec int           = 30
	RequestTimeoutSec  int           = 60
	RetryWaitMin       time.Duration = time.Second
	RetryWaitMax       time.Duration = time.Second * 10
	RetryMax           int           = 9 // 9 retries will have last retry not earlier than 55 sec in. No reason to try more (unless min/mx waits are changed)
)

var (
	HttpClient          *http.Client
	RetryableHttpClient *RetryableClient
)

func init() {
	HttpClient = CreateHTTPClient()
	RetryableHttpClient = CreateRetryableHTTPClient()
}

//CreateHTTPClient with default configuration
func CreateHTTPClient() *http.Client {
	return CreateHTTPClientWithConfig(&HttpConfiguration{
		MaxIdleConnections: MaxIdleConnections,
		IdleConnTimeoutSec: IdleConnTimeoutSec,
		RequestTimeoutSec:  RequestTimeoutSec})
}

//CreateRetryableHTTPClient with default configuration
func CreateRetryableHTTPClient() *RetryableClient {
	return CreateRetryableHTTPClientWithConfig(&HttpConfiguration{
		MaxIdleConnections: MaxIdleConnections,
		IdleConnTimeoutSec: IdleConnTimeoutSec,
		RequestTimeoutSec:  RequestTimeoutSec})
}

//CreateHTTPClient with custom configuration
func CreateHTTPClientWithConfig(config *HttpConfiguration) *http.Client {
	return &http.Client{
		Transport: getTransport(config),
		Timeout:   time.Duration(config.RequestTimeoutSec) * time.Second,
	}
}

//CreateRetryableHTTPClient with custom configuration
func CreateRetryableHTTPClientWithConfig(config *HttpConfiguration) *RetryableClient {
	client := NewRetryableClient()
	client.HTTPClient.Transport = getTransport(config)
	client.HTTPClient.Timeout = time.Duration(config.RequestTimeoutSec) * time.Second
	client.RetryWaitMin = RetryWaitMin
	client.RetryWaitMax = RetryWaitMax
	client.RetryMax = RetryMax
	client.CheckRetry = retryPolicyWithDeadline
	return client
}

func retryPolicyWithDeadline(req *http.Request, resp *http.Response, err error) (bool, error) {
	if err != nil {
		ctx := req.Context()
		if ctx != nil && ctx.Err() != nil {
			return false, ctx.Err()
		}
		return true, err
	}
	// Check the response code. We retry on 500-range responses to allow
	// the server time to recover, as 500's are typically not permanent
	// errors and may relate to outages on the server side. This will catch
	// invalid response codes as well, like 0 and 999.
	if resp.StatusCode == 0 || resp.StatusCode >= 500 {
		return true, nil
	}

	return false, nil
}

func getTransport(config *HttpConfiguration) *http.Transport {
	return &http.Transport{
		MaxIdleConnsPerHost: config.MaxIdleConnections,
		IdleConnTimeout:     time.Duration(config.IdleConnTimeoutSec) * time.Second,
		MaxIdleConns:        config.MaxIdleConnections * 3,
		// TLSClientConfig:     &tls.Config{InsecureSkipVerify: true},
	}
}

func SetConfiguration(config *HttpConfiguration) {
	HttpClient.Transport = getTransport(config)
	HttpClient.Timeout = time.Duration(config.RequestTimeoutSec) * time.Second
	RetryableHttpClient.HTTPClient.Transport = getTransport(config)
	RetryableHttpClient.HTTPClient.Timeout = time.Duration(config.RequestTimeoutSec) * time.Second
}

type HttpConfiguration struct {
	MaxIdleConnections int
	IdleConnTimeoutSec int
	RequestTimeoutSec  int
}
