package models

import (
	"context"

	"github.com/NeowayLabs/wabbit"
)

//ListenContract for listening to a queue on the rmq server
type ListenContract struct {
	Failure         chan error
	Ctx             context.Context
	Cancel          func()
	Deliveries      <-chan wabbit.Delivery //readonly channel
	RmqChan         wabbit.Channel
	ID              string
	QueueName       string
	RmqChannelClose chan wabbit.Error
}
