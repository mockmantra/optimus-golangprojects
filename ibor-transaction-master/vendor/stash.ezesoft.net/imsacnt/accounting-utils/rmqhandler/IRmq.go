package rmqhandler

import "stash.ezesoft.net/imsacnt/accounting-utils/models"

// IRmqConnector - interface for accessing Rmq
type IRmqConnector interface {
	Listen(contract *models.ListenContract, queueTemplate string, firm string, exchange string, prefetchCount int) (bool, error)
	Publish(data []byte, queueTemplate string, firm string, exchange string, statistics map[string]interface{}) error
	CloseConnection() error
}
