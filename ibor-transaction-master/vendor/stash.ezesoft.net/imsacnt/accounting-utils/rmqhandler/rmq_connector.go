package rmqhandler

import (
	"fmt"
	"net/url"
	"sync"
	"time"

	"github.com/NeowayLabs/wabbit"
	"github.com/NeowayLabs/wabbit/amqp"
	"github.com/fatih/stopwatch"
	"github.com/pkg/errors"
	streadwayAmqp "github.com/streadway/amqp"
	"stash.ezesoft.net/imsacnt/accounting-utils/models"
	"stash.ezesoft.net/ipc/ezelogger"
)

const exchangeKind = "direct"
const listenMaxRetry = 20
const listenRetryDelaySeconds int64 = 3

var consumerTag string
var connectionLock sync.Mutex

// RmqConnector stores the data to route to correct exchange and queue
type RmqConnector struct {
	Logger        *ezelogger.EzeLog
	ConfigRmq     *models.RmqConfig
	CallerName    string
	RmqConnection wabbit.Conn
}

// RmqConnectorInstance - Returns an instance of RmqConnector struct
func RmqConnectorInstance(logger *ezelogger.EzeLog, configRmq *models.RmqConfig, callerName string) *RmqConnector {
	return &RmqConnector{
		Logger:     logger,
		ConfigRmq:  configRmq,
		CallerName: callerName,
	}
}

// createConnection - initiates the connection to Rmq
func (r *RmqConnector) createConnection() error {
	connectionLock.Lock()
	defer connectionLock.Unlock()

	if r.RmqConnection != nil {
		return nil
	}

	var brokerURI = fmt.Sprintf("amqp://%s:%s@%s/%s", r.ConfigRmq.User, url.QueryEscape(r.ConfigRmq.Password), r.ConfigRmq.HostPort, r.ConfigRmq.Silo)

	var err error
	connection, err := rmqDial(brokerURI)
	if err != nil {
		return errors.Wrapf(err, "error dialing '%s' '%s'", r.ConfigRmq.HostPort, r.ConfigRmq.Silo)
	}
	r.RmqConnection = connection

	return nil
}

func queueAndKeyName(queueTemplate string, firm string) string {
	return fmt.Sprintf("%s.%s", queueTemplate, firm)
}

// Listen - declares the queue and exchange, binds them together and listens for deliveries
func (r *RmqConnector) Listen(contract *models.ListenContract, queueTemplate string, firm string, exchange string, prefetchCount int) (bool, error) {
	contract.Failure = make(chan error, 1)
	contract.RmqChannelClose = make(chan wabbit.Error, 37)
	contract.QueueName = queueAndKeyName(queueTemplate, firm)

	err := r.createConnection()
	if err != nil {
		err = errors.Wrap(err, "listen connect error")
		r.Logger.Error(err)
		return false, err
	}

	rmqChannel, err := rmqChannelCreate(r.RmqConnection)
	if err != nil {
		err = errors.Wrapf(err, "Listen error on create channel '%s'", contract.ID)
		r.Logger.Error(err)
		return false, err
	}

	err = rmqChannel.Qos(prefetchCount, 0, false)
	if err != nil {
		err = errors.Wrapf(err, "Listen error on channel Qos '%s'", contract.ID)
		closeErr := rmqChannel.Close()
		if closeErr != nil {
			closeErr = errors.Wrapf(err, "Listen error on close channel '%s' %v", contract.ID, closeErr)
			return false, closeErr
		}
		return false, err
	}

	_, err = rmqChannel.QueueDeclare(contract.QueueName, wabbit.Option{"durable": true, "autoDelete": false, "exclusive": false, "noWait": false})
	if err != nil {
		err = errors.Wrapf(err, "Listen error on Queue Declare '%s'", contract.ID)
		closeErr := rmqChannel.Close()
		if closeErr != nil {
			closeErr = errors.Wrapf(err, "Listen error on close channel '%s' %v", contract.ID, closeErr)
			return false, closeErr
		}
		return false, err
	}

	err = rmqChannel.ExchangeDeclare(exchange, exchangeKind,
		wabbit.Option{"durable": true, "autoDelete": false, "internal": false, "noWait": false})
	if err != nil {
		err = errors.Wrapf(err, "Listen error on Exchange Declare: exchange name: %s, %s", exchange, contract.ID)
		closeErr := rmqChannel.Close()
		if closeErr != nil {
			closeErr = errors.Wrapf(err, "Listen error on close channel '%s' %v", contract.ID, closeErr)
			return false, closeErr
		}
		return false, err
	}

	err = rmqChannel.QueueBind(contract.QueueName, contract.QueueName, exchange, wabbit.Option{"noWait": false})
	if err != nil {
		err = errors.Wrapf(err, "Listen error on Queue bind to queue %s, %s", contract.QueueName, contract.ID)
		closeErr := rmqChannel.Close()
		if closeErr != nil {
			closeErr = errors.Wrapf(err, "Listen error on close channel '%s' %v", contract.ID, closeErr)
			return false, closeErr
		}
		return false, err
	}

	var deliveries <-chan wabbit.Delivery
	for i := 0; i < listenMaxRetry && deliveries == nil; i++ {
		rmqChannel, err = rmqChannelCreate(r.RmqConnection)
		if err != nil {
			err = errors.Wrapf(err, "Listen error on create channel '%s'", contract.ID)
			return false, err
		}
		err = rmqChannel.Qos(prefetchCount, 0, false)
		if err != nil {
			err = errors.Wrapf(err, "Listen error on channel Qos '%s'", contract.ID)
			return false, err
		}
		consumerTag = fmt.Sprintf("%s %s:%s", r.CallerName, r.ConfigRmq.RancherHost, r.ConfigRmq.RancherContainer)
		deliveries, err = rmqChannel.Consume(contract.QueueName, fmt.Sprintf("%s:%s", contract.ID, consumerTag),
			wabbit.Option{"autoAck": false, "exclusive": true, "noLocal": false, "noWait": false})
		if err != nil && i < listenMaxRetry {
			select {
			case <-contract.Ctx.Done():
				closeErr := rmqChannel.Close()
				if closeErr != nil {
					closeErr = errors.Wrapf(err, "Listen error on close channel '%s' %v", contract.ID, closeErr)
					r.Logger.Error(closeErr)
				}
				return true, nil
			case <-time.After(time.Duration(int64(time.Second) * listenRetryDelaySeconds)):
			}
		}
	}
	if err != nil {
		err = errors.Wrapf(err, "Listen error trying to consume from queue %s, %s", contract.QueueName, contract.ID)
		r.Logger.Error(err)
		closeErr := rmqChannel.Close()
		if closeErr != nil {
			closeErr = errors.Wrapf(err, "Listen error on close channel '%s' %v", contract.ID, closeErr)
			return false, closeErr
		}
		return false, err
	}
	contract.Deliveries, contract.RmqChan = deliveries, rmqChannel
	contract.RmqChannelClose = rmqChannel.NotifyClose(contract.RmqChannelClose)

	return false, nil
}

// Publish - sends a message to the Rmq Exchange using firm to determine routing key
func (r *RmqConnector) Publish(data []byte, queueTemplate string, firm string, exchange string, statistics map[string]interface{}) error {
	watch := stopwatch.Start(0)
	defer func() {
		watch.Stop()
		statistics["PublishToRmq"] = watch.ElapsedTime()
	}()

	routingKeyName := queueAndKeyName(queueTemplate, firm)
	r.Logger.Infof("received publish request from %s for routing key %s", r.CallerName, routingKeyName)

	createConnWatch := stopwatch.Start(0)
	err := r.createConnection()
	if err != nil {
		err = errors.Wrap(err, "publish connect error")
		r.Logger.Error(err)
		return err
	}
	createConnWatch.Stop()
	statistics["CreateRmqConnection"] = createConnWatch.ElapsedTime()
	defer r.CloseConnection()

	createChanWatch := stopwatch.Start(0)
	rmqChannel, err := rmqChannelCreate(r.RmqConnection)
	if err != nil {
		r.Logger.Error(err)
		return err
	}
	createChanWatch.Stop()
	statistics["CreateRmqChannel"] = createChanWatch.ElapsedTime()
	defer rmqChannel.Close()

	err = rmqChannel.ExchangeDeclare(exchange, exchangeKind,
		wabbit.Option{"durable": true, "autoDelete": false, "internal": false, "noWait": false})
	if err != nil {
		r.Logger.Error(errors.Wrap(err, "publish exchangedeclare error "))
		return err
	}

	insertDataWatch := stopwatch.Start(0)
	err = rmqChannel.Publish(exchange, routingKeyName, data,
		wabbit.Option{"headers": streadwayAmqp.Table{}, "contentType": "text/plain", "contentEncoding": "text/plain", "deliveryMode": streadwayAmqp.Transient, "priority": uint8(0)})
	if err != nil {
		r.Logger.Error(errors.Wrap(err, "publish Publish error"))
		return err
	}
	insertDataWatch.Stop()
	statistics["InsertDataIntoQueue"] = insertDataWatch.ElapsedTime()

	r.Logger.Infof("publish Published successfully. key = %s data = %s", routingKeyName, string(data))
	return err
}

// CloseConnection - closes connection to Rmq
func (r *RmqConnector) CloseConnection() error {
	connectionLock.Lock()
	defer connectionLock.Unlock()

	if r.RmqConnection == nil {
		return nil
	}
	r.Logger.Debugf("closing connection to rmq at %v...", time.Now())
	err := r.RmqConnection.Close()
	if err != nil {
		r.Logger.Error(errors.Wrap(err, "Error closing Rmq connection"))
		return err
	}

	r.RmqConnection = nil
	return nil
}

// rmqDial dials to AMQP broker to obtain the rmq connection
// Variable function in use to enable unit tests to alter
// behaviour of the dial call for testing purposes
var rmqDial fnDial = func(url string) (wabbit.Conn, error) {
	return amqp.Dial(url)
}

// fnDial stores the function for creating rmq connection
type fnDial func(url string) (wabbit.Conn, error)

// rmqChannelCreate creates an AMQP channel
// Variable function in use to enable unit tests to alter
// behaviour of the dial call for testing purposes
var rmqChannelCreate fnChannelCreate = func(rmqConnection wabbit.Conn) (wabbit.Channel, error) {
	return rmqConnection.Channel()
}

// fnChannelCreate stores the function for creating rmq channel
type fnChannelCreate func(rmqConnection wabbit.Conn) (wabbit.Channel, error)
