package eventtype

const (
	// Unknown Event Type
	Unknown = 0
	// Buy Event Type
	Buy = 3
	// Cover Event Type
	Cover = 6
	// Sell Event Type
	Sell = 14
	// Short Event Type
	Short = 17
)
