package positionstate

const (
	// Unknown Position State
	Unknown = 0
	// Proposed Position State
	Proposed = 10
	// ReleasedToMarket Position State
	ReleasedToMarket = 20
	// FillReceived Position State
	FillReceived = 30
	// Finalized Position State
	Finalized = 40
	// Confirmed Position State
	Confirmed = 50
	// Settled Position State
	Settled = 60
)
