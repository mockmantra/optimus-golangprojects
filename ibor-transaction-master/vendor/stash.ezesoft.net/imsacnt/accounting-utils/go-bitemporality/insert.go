package bitemporality

import (
	"fmt"
	"reflect"

	"github.com/jinzhu/gorm"
)

const insertPrefix = "[go-bitemporality] Insert:"
const insertBulkPrefix = "[go-bitemporality] InsertBulk:"
const payloadMissingError = "%s Payload missing"

// Insert can be either :
// 1. Fresh Insert (first time)
// 2. Update
// 3. Insert a delete record
func (s *BtObj) Insert(isDeleted bool, payload interface{}, validDateTimeFields DateTimeUpdate) error {

	// Validate payload
	if payload == nil {
		return fmt.Errorf(payloadMissingError, insertPrefix)
	}

	inputErr := s.dateTimeValidation(validDateTimeFields, insertPrefix+" ValidDateTime")
	if inputErr != nil {
		return inputErr
	}

	inputErr = s.updateValidDateTime(isDeleted, validDateTimeFields)
	if inputErr != nil {
		return inputErr
	}

	// If Insert-Delete
	if isDeleted == true {
		payload = getPayloadToDelete(payload)
	}

	// Insert new record
	return insertNewRecord(s, payload, validDateTimeFields)
}

// InsertWithBusinessDateTime can be either :
// 1. Fresh Insert (first time)
// 2. Update
// 3. Insert a delete record
func (s *BtObj) InsertWithBusinessDateTime(isDeleted bool, payload interface{}, validDateTimeFields DateTimeUpdate, businessDateTimeFields DateTimeUpdate) error {

	// Validate payload
	if payload == nil {
		return fmt.Errorf(payloadMissingError, insertPrefix)
	}

	inputErr := s.dateTimeValidation(validDateTimeFields, insertPrefix+" ValidDateTime")
	if inputErr != nil {
		return inputErr
	}

	// if a businessdate update we want to check and update if a record exists with earlier businessdate
	if !isDeleted {
		err := s.updateBusinessDateTime(isDeleted, payload, validDateTimeFields, businessDateTimeFields)
		if err != nil {
			return err
		}
	}

	// Add the businessdate into the unique key for finding active records in the next step
	s.UniqueKey[businessDateTimeFields.FromTableKey] = businessDateTimeFields.NewDateTime

	inputErr = s.updateValidDateTime(isDeleted, validDateTimeFields)
	if inputErr != nil {
		return inputErr
	}

	// If Insert-Delete
	if isDeleted == true {
		payload = getPayloadToDelete(payload)
	}

	// Insert new record
	return insertNewRecordWithBusinessDateTime(s, payload, validDateTimeFields, businessDateTimeFields)

}

// InsertBulk can be either :
// 1. Fresh Bulk Insert records(first time)
// 2. Update
// 3. Bulk Insert delete record
func (s *BtObj) InsertBulk(isDeleted bool, payload interface{}, validDateTimeFields DateTimeUpdate) (insertCount int, err error) {

	// Validate payload
	if payload == nil {
		return 0, fmt.Errorf(payloadMissingError, insertBulkPrefix)
	}

	inputErr := s.dateTimeValidation(validDateTimeFields, insertBulkPrefix+" ValidDateTime")
	if inputErr != nil {
		return 0, inputErr
	}

	// Check payload is a slice of records
	payloadSlice, ok := getSlice(payload)
	if !ok {
		err := fmt.Errorf("%s Unable to get slice for bulk insert from payload array", insertBulkPrefix)
		return 0, err
	}

	payloadLength := payloadSlice.Len()

	// Get active record if exists
	count, err := s.SelectActiveCount(validDateTimeFields.ToTableKey, true)
	if err != nil && !gorm.IsRecordNotFoundError(err) {
		return 0, err
	}

	// If Insert is - Delete insert, and last active record NOT found, then nothing to delete
	if isDeleted == true && count != payloadLength {
		return 0, fmt.Errorf("%s Insert Delete failed...last active record(s) not found or not the same count as the payload array", insertBulkPrefix)
	}

	// update last active record's 'ValidToTableKey' column
	if count >= 1 {
		if err = s.Database.Table(s.TableName).Where(fmt.Sprintf("%s IS NULL", validDateTimeFields.ToTableKey)).
			Where(s.UniqueKey).Update(validDateTimeFields.ToTableKey, validDateTimeFields.NewDateTime).
			Debug().Error; err != nil {
			return 0, err
		}
	}

	for i := 0; i < payloadLength; i++ {
		record := payloadSlice.Index(i).Interface()

		// If Insert-Delete
		if isDeleted == true {
			record = getPayloadToDelete(record)
		}

		// Insert new record
		err = insertNewRecord(s, record, validDateTimeFields)

		if err != nil {
			return i, err
		}
	}

	return payloadLength, nil
}

// dateTimeValidation validates a DateTimeUpdate objetc contains a valid non-zero datetime, and the from/to field names are present
func (s *BtObj) dateTimeValidation(dateTimeFields DateTimeUpdate, errorPrefix string) error {
	// check date time
	if dateTimeFields.NewDateTime.IsZero() == true {
		return fmt.Errorf("%s Date-time is zero", errorPrefix)
	}
	// Validate ValidFromTableKey
	if len(dateTimeFields.FromTableKey) == 0 {
		return fmt.Errorf("%s FromTableKey missing", errorPrefix)
	}
	// Validate ValidToTableKey
	if len(dateTimeFields.ToTableKey) == 0 {
		return fmt.Errorf("%s ToTableKey missing", errorPrefix)
	}
	return nil
}

// updateBusinessDateTime checks if a record already exists with a previous businessdatetime and updates the To date
func (s *BtObj) updateBusinessDateTime(isDeleted bool,
	payload interface{},
	validDateTimeFields DateTimeUpdate,
	businessDateTimeFields DateTimeUpdate) (err error) {

	// Validate businessdatetime from/to fields and value
	err = s.dateTimeValidation(businessDateTimeFields, insertPrefix+" BusinessDateTime")
	if err != nil {
		return err
	}

	if s.Query == nil {
		s.Query = make(map[string]Query)
	}

	// Adds temporary constraint to ensure BusinessDateTime To field is null
	s.Query[businessDateTimeFields.ToTableKey] = Query{Value: nil, Operator: NullConstraint}

	// Get active record if exists on both ValidTo and BusinessDateTimeTo fields being set to null
	record, count, err := s.SelectActive(validDateTimeFields.ToTableKey, false)
	if err != nil && !gorm.IsRecordNotFoundError(err) {
		return err
	}

	// update last active record's BusinessDateTimeTo TableKey column
	if count == 1 {
		if errU := s.Database.Table(s.TableName).Model(record).
			Update(businessDateTimeFields.ToTableKey, businessDateTimeFields.NewDateTime).Debug().Error; errU != nil {
			return errU
		}
	}

	// the tableModel needs to be reset for any future queries
	s.resetTableModel()

	// removes the temporary BusinessDateTime To constraint
	delete(s.Query, businessDateTimeFields.ToTableKey)

	return nil
}

// updateValidDateTime checks if an active record exists with the matching unique key and
// sets the validTo field of the previously active to the validFrom value of the new record
func (s *BtObj) updateValidDateTime(isDeleted bool, validDateTimeFields DateTimeUpdate) error {
	// Get active record if exists
	record, count, err := s.SelectActive(validDateTimeFields.ToTableKey, true)
	if err != nil && !gorm.IsRecordNotFoundError(err) {
		return err
	}

	// If Insert is - Delete insert, and last active record NOT found, then nothing to delete
	if isDeleted == true && count == 0 {
		return fmt.Errorf("%s Insert Delete failed...last active record not found", insertPrefix)
	}

	// update last active record's 'ValidToTableKey' column
	if count == 1 {
		if errU := s.Database.Table(s.TableName).Model(record).
			Update(validDateTimeFields.ToTableKey, validDateTimeFields.NewDateTime).Debug().Error; errU != nil {
			return errU
		}
	}

	return nil
}

// resetTableModel sets the struct of the TableModel back to the original new value
// prevents the primary key being added to a future query's where constraint
func (s *BtObj) resetTableModel() {

	modelType := reflect.TypeOf(s.TableModel).Elem()
	s.TableModel = reflect.New(modelType).Interface()
}

// insertNewRecord inserts a fresh record with:
// 1. ValidFromTableKey column = dateTime of ValidToTableKey of previous row
// 2. ValidToTableKey column = NULL
func insertNewRecord(bitemporalObj *BtObj, payload interface{}, validDateTimeFields DateTimeUpdate) error {
	if errC := bitemporalObj.Database.Table(bitemporalObj.TableName).Create(payload).
		Update(validDateTimeFields.FromTableKey, validDateTimeFields.NewDateTime).
		Update(validDateTimeFields.ToTableKey, gorm.Expr("NULL")).Debug().Error; errC != nil {
		return errC
	}

	return nil
}

// insertNewRecordWithBusinessDateTime inserts a fresh record with:
// 1. ValidFromTableKey column = dateTime of ValidToTableKey of previous row
// 2. ValidToTableKey column = NULL
// 2. BusinessDateTimeToTableKey column = NULL
func insertNewRecordWithBusinessDateTime(bitemporalObj *BtObj, payload interface{}, validDateTimeFields DateTimeUpdate, businessDateTimeFields DateTimeUpdate) error {
	if errC := bitemporalObj.Database.Table(bitemporalObj.TableName).Create(payload).
		Update(validDateTimeFields.FromTableKey, validDateTimeFields.NewDateTime).
		Update(validDateTimeFields.ToTableKey, gorm.Expr("NULL")).
		Update(businessDateTimeFields.ToTableKey, gorm.Expr("NULL")).Debug().Error; errC != nil {
		return errC
	}

	return nil
}

func getPayloadToDelete(payload interface{}) (deletePayload interface{}) {
	val := reflect.ValueOf(payload).Elem()

	for i := 0; i < val.NumField(); i++ {
		typeField := val.Type().Field(i)

		// set IsDeleted in payload
		if typeField.Name == "IsDeleted" {
			val.Field(i).SetBool(true)
		}
	}

	return payload
}

func getSlice(payload interface{}) (val reflect.Value, ok bool) {
	val = reflect.ValueOf(payload)
	if val.Kind() == reflect.Slice {
		ok = true
	}
	return
}
