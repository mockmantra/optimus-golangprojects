package bitemporality

import (
	"time"

	"github.com/jinzhu/gorm"
)

// BtObj contains all the fields required for using the package
type BtObj struct {
	TableName  string                 // Name of the table to run this package on
	Database   *gorm.DB               // Instance of gorm based database object
	TableModel interface{}            // empty instance of table model
	UniqueKey  map[string]interface{} // map of the unique keys for the records
	Query      map[string]Query
	Joins	   *string
	Fields     string
}

// Query contains all the fields required for creating a custom where clause for use within go-bitemporaility
type Query struct {
	Value    interface{} // the value to perform the SQL operation against
	Operator string      //SQL operator such as '=', 'IN'
}

// DateTimeUpdate contains the fields required to find and update the DateTimeTO field of the
// previous record based on the DateTimeFROM field of the current record that is to be inserted
type DateTimeUpdate struct {
	NewDateTime  time.Time // the new value of the DateTime
	FromTableKey string    // the column name of the DateTime From field
	ToTableKey   string    // the column name of the DateTime To field
}
