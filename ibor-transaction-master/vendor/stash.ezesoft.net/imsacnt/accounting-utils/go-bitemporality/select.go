package bitemporality

import (
	"errors"
	"fmt"
	"sort"
	"time"

	"github.com/jinzhu/gorm"
)

// NullConstraint constant is for passing in fields where they should only be included if the value is null
const NullConstraint = "IS NULL"

// SelectFromTo selects all records for a unique identifier present in 'WHERE' and also has a range of inputs in 'WhereRange'
// SELECT * FROM 'TableName' WHERE (condition(s) in 'WHERE') AND (FromCol >= FromVal AND ToCol <= ToVal) <- this comes from 'WhereRange'
// Returns - all records, number of records, error if any
func (s *BtObj) SelectFromTo(whereRange WhereRange) (data interface{}, countData int, err error) {

	inputErr := s.commonSelectValidation()
	if inputErr != nil {
		return nil, 0, inputErr
	}

	database := s.addQueryConstraints()

	// Validate all fields of 'WhereRange'
	if len(whereRange.FromKey) == 0 || len(whereRange.FromVal) == 0 ||
		len(whereRange.ToKey) == 0 || len(whereRange.ToVal) == 0 {
		return nil, 0, errors.New(formatStr("SelectFromTo: Invalid WhereRange parameters, require all values - FromKey, FromVal, ToKey, ToVal"))
	}

	count := 0
	from := fmt.Sprintf("%s.%s", s.TableName, whereRange.FromKey)
	fVal := whereRange.FromVal
	to := fmt.Sprintf("%s.%s", s.TableName, whereRange.ToKey)
	tVal := whereRange.ToVal

	// need to do this because putting in .Where() directly adds extra ``
	w := from + ">=? AND " + to + "<=?"

	if err := database.Where(w, fVal, tVal).Where(s.UniqueKey).Debug().Find(s.TableModel).Count(&count).Error; err != nil {
		return nil, count, err
	}

	return s.TableModel, count, nil
}

// SelectFromToActive selects all ACTIVE records for a unique identifier present in 'WHERE' and also has a range of inputs in 'WhereRange'
// SELECT * FROM 'TableName' WHERE (condition(s) in 'WHERE') AND (FromCol >= FromVal AND ToCol <= ToVal) AND (ValidToUTC = null)
// Returns - all active records, number of records, error if any
func (s *BtObj) SelectFromToActive(whereRange WhereRange, validToTableKey string, includeDeleted bool) (data interface{}, countData int, err error) {

	inputErr := s.commonSelectValidation()
	if inputErr != nil {
		return nil, 0, inputErr
	}

	database := s.addQueryConstraints()

	// Validate all fields of 'WhereRange'
	if len(whereRange.FromKey) == 0 || len(whereRange.FromVal) == 0 ||
		len(whereRange.ToKey) == 0 || len(whereRange.ToVal) == 0 {
		return nil, 0, errors.New(formatStr("SelectFromTo: Invalid WhereRange parameters, require all values - FromKey, FromVal, ToKey, ToVal"))
	}

	if includeDeleted == false {
		database = database.Where(fmt.Sprintf("%s.IsDeleted =?", s.TableName), false)
	}

	count := 0
	from := fmt.Sprintf("%s.%s", s.TableName, whereRange.FromKey)
	fVal := whereRange.FromVal
	to := fmt.Sprintf("%s.%s", s.TableName, whereRange.ToKey)
	tVal := whereRange.ToVal
	vTo := fmt.Sprintf("%s.%s", s.TableName, validToTableKey)

	// need to do this because putting in .Where() directly adds extra ``
	w := "(" + from + ">=? AND " + to + "<=?) AND " + vTo + " IS NULL"

	if err := database.Where(w, fVal, tVal).Where(s.UniqueKey).Debug().Find(s.TableModel).Count(&count).Error; err != nil {
		return nil, count, err
	}

	return s.TableModel, count, nil
}

// SelectActive returns the currently active record for a unique identifier present in 'UniqueKey' and :
// ValidToTableKey column IS NULL
// Returns - all active records, number of records, error if any (Ideally there should be only 1 active record for a unique identifier)
func (s *BtObj) SelectActive(validToTableKey string, includeDeleted bool) (data interface{}, countData int, err error) {

	inputErr := s.commonSelectValidation()
	if inputErr != nil {
		return nil, 0, inputErr
	}

	if len(validToTableKey) == 0 {
		return nil, 0, errors.New(formatStr("SelectActive: ValidToTableKey missing"))
	}

	database := s.addQueryConstraints()
	where := fmt.Sprintf("%s.%s IS NULL", s.TableName, validToTableKey)
	count := 0

	if includeDeleted == false {
		database = database.Where(fmt.Sprintf("%s.IsDeleted =?", s.TableName), false)
	}

	if err := database.Where(where).Where(s.UniqueKey).Debug().Find(s.TableModel).Count(&count).Error; err != nil {
		return nil, count, err
	}

	return s.TableModel, count, nil
}

// SelectActiveCount returns the number of currently active records for a unique identifier present in 'UniqueKey' and :
// ValidToTableKey column IS NULL
// Returns - number of records, error if any
func (s *BtObj) SelectActiveCount(validToTableKey string, includeDeleted bool) (countData int, err error) {

	inputErr := s.commonSelectValidation()
	if inputErr != nil {
		return 0, inputErr
	}

	if len(validToTableKey) == 0 {
		return 0, errors.New(formatStr("SelectActiveCount: ValidToTableKey missing"))
	}

	database := s.addQueryConstraints()
	where := fmt.Sprintf("%s.%s IS NULL", s.TableName, validToTableKey)
	count := 0

	if includeDeleted == false {
		database = database.Where(fmt.Sprintf("%s.IsDeleted =?", s.TableName), false)
	}

	if err := database.Where(where).Where(s.UniqueKey).Debug().Count(&count).Error; err != nil {
		return count, err
	}

	return count, nil
}

// SelectDeleted returns the record marked as 'IsDeleted' for a unique identifier in 'WHERE'
// Returns - all deleted records, number of records, error if any (Ideally there should be only 1 delete record for a unique identifier)
func (s *BtObj) SelectDeleted() (data interface{}, countData int, err error) {

	inputErr := s.commonSelectValidation()
	if inputErr != nil {
		return nil, 0, inputErr
	}

	database := s.addQueryConstraints()
	count := 0
	if err := database.Where(fmt.Sprintf("%s.IsDeleted =?", s.TableName), true).Where(s.UniqueKey).Debug().Find(s.TableModel).Count(&count).Error; err != nil {
		return nil, count, err
	}

	return s.TableModel, count, nil
}

// SelectActiveAtPointOfTime returns the record which was active for unique identifier and passed point of time.
func (s *BtObj) SelectActiveAtPointOfTime(dateTime time.Time, validToTableKey string, validFromTableKey string, includeDeleted bool) (data interface{}, countData int, err error) {
	inputErr := s.commonSelectValidation()
	if inputErr != nil {
		return nil, 0, inputErr
	}

	database := s.addQueryConstraints()

	if includeDeleted == false {
		database = database.Where(fmt.Sprintf("%s.IsDeleted =?", s.TableName), false)
	}

	count := 0
	validFrom := fmt.Sprintf("%s.%s", s.TableName, validFromTableKey)
	validTo := fmt.Sprintf("%s.%s", s.TableName, validToTableKey)

	where := "(" + validFrom + "<=? AND " + validTo + ">?) OR (" + validFrom + "<=? AND " + validTo + " IS NULL)"
	if err := database.Where(where, dateTime, dateTime, dateTime).Where(s.UniqueKey).Debug().Find(s.TableModel).Count(&count).Error; err != nil {
		return nil, count, err
	}

	return s.TableModel, count, nil
}

// SelectActiveAtBusinessDate returns the record which wrere active for unique identifier and passed point of time.
func (s *BtObj) SelectActiveAtBusinessDate(validToTableKey string, bdate DateTimeUpdate, includeDeleted bool) (data interface{}, countData int, err error) {
	inputErr := s.commonSelectValidation()
	if inputErr != nil {
		return nil, 0, inputErr
	}

	database := s.addQueryConstraints()

	if includeDeleted == false {
		database = database.Where(fmt.Sprintf("%s.IsDeleted =?", s.TableName), false)
	}

	count := 0
	validTo := fmt.Sprintf("%s.%s", s.TableName, validToTableKey)
	businessFrom := fmt.Sprintf("%s.%s", s.TableName, bdate.FromTableKey)
	businessTo := fmt.Sprintf("%s.%s", s.TableName, bdate.ToTableKey)

	where := validTo + " IS NULL AND (" + businessFrom + "<=? AND " + businessTo + ">?) OR (" + businessFrom + "<=? AND " + businessTo + " IS NULL)"
	if err := database.Where(where, bdate.NewDateTime, bdate.NewDateTime, bdate.NewDateTime).Where(s.UniqueKey).Debug().Find(s.TableModel).Count(&count).Error; err != nil {
		return nil, count, err
	}

	return s.TableModel, count, nil
}

// Validate BtObj:
// 1. TableName
// 2. Database object
// 3. TableModel
func (s *BtObj) commonSelectValidation() error {
	if len(s.TableName) == 0 {
		return errors.New(formatStr("TableName missing"))
	}
	if s.Database == nil {
		return errors.New(formatStr("Database object missing"))
	}
	if s.TableModel == nil {
		return errors.New(formatStr("TableModel missing"))
	}
	if s.UniqueKey == nil {
		return errors.New(formatStr("UniqueKey missing, no clause for unique identifier"))
	}

	return nil
}

func (s *BtObj) addQueryConstraints() (database *gorm.DB) {
	if s.Joins == nil {
		database = s.Database.Table(s.TableName)
	} else {
		if len(s.Fields) > 0 {
			database = s.Database.Table(s.TableName).Select(s.Fields).Joins(*s.Joins)
		} else {
			database = s.Database.Table(s.TableName).Select("*").Joins(*s.Joins)
		}
	}

	var keys []string
	for k := range s.Query {
		keys = append(keys, k)
	}

	sort.Strings(keys)

	for _, column := range keys {
		if s.Query[column].Operator == NullConstraint {
			formattedClause := "%s %s"
			database = database.Where(fmt.Sprintf(formattedClause, column, NullConstraint))
		} else {
			formattedClause := "%s %s (?)"
			database = database.Where(fmt.Sprintf(formattedClause, column, s.Query[column].Operator), s.Query[column].Value)
		}
	}
	return
}

func formatStr(str string) string {
	return "[go-bitemporality]: " + str
}
