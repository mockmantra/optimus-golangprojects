#!/bin/bash
set -e

SERVICE="trx_master_service"
DB="sqldb"
NETWORK="Trx_Master_network"
API="db_api"
MOCK_IMS="mock-eclipse"
NGINX="nginx"
mockapi="mockapi"
readapi="readapi"

set -e

cleanup_network() {
    echo "Cleaning up network: $NETWORK"
    if docker network ls | grep $NETWORK
    then
        echo "found network: $NETWORK"
        docker network rm $NETWORK
   else
        echo "Network not found."
    fi
}

stop_containers(){
    echo "Step1 Stopping any zombie containers..."
    declare -a arr=($SERVICE $DB $API $MOCK_IMS $NGINX $mockapi $readapi)

    for i in "${arr[@]}"
    do
        echo "Removing $i ......"
        container_id=$(docker ps -aqf "name=$i")
        if [[ ! -z "$container_id" ]]
        then
            echo "Container found: " $container_id
           
            docker rm -f $container_id
        else
            echo "No container found for: $i"
        fi
    done
}

function cleanup {
    stop_containers
    cleanup_network
}

function run_tests {
    echo "called with param: $1"
    cleanup

    echo "Step 1 :Create network: $NETWORK"
    docker network create --driver bridge $NETWORK 

    echo "Step 2 :Build DataBase"
    docker build -t $DB -f tests/db_docker/Dockerfile tests/db_docker

    echo "Step 3 :Run DataBase at port 3306"
    docker run -d --name $DB --network $NETWORK $DB 
    
    echo "Step 4 :Build Read API....."
    docker build -t $readapi -f tests/read-api/Dockerfile  tests/read-api

    echo "Step 5 :Running Read API....8002"
    docker run -d --name $readapi --network $NETWORK $readapi

    echo "Step 6 :Building mock-eclipse"
    docker build -t $MOCK_IMS -f tests/mock-eclipse/Dockerfile tests/mock-eclipse
                                                           
    echo "Step 7 :Run mock-eclipse at port 3000"                                              
    docker run -d --name $MOCK_IMS  --network $NETWORK $MOCK_IMS

    echo "Step 8 :Build mock API....."
    docker build -t $mockapi -f tests/mocking/Dockerfile  tests/mocking

    echo "Step 9 :Running mock API.JSON-SERVER....8003"
    docker run -d --name $mockapi --network $NETWORK $mockapi

    echo "Step 10 :Build NGINX....."
    docker build -t $NGINX -f tests/nginx/Dockerfile .
                                                           
    echo "Step 11 :Run Nginx. at port 8001"                                                 
    docker run -d --name $NGINX --network $NETWORK $NGINX

    echo "Step 8 :Build service in bdd mode....."
    docker build -t $SERVICE -f tests/service/Dockerfile .
                                                           
    echo "Step 9 :Run service..+ BDD's..."          
    if (docker run --name $SERVICE -e Connection_String="root:123456@tcp($DB:3306)/test" --network $NETWORK $SERVICE);then  
    echo "Component Tests Successfull !!!"
		else
            echo "Component Tests Failed !!!!"
			exit 1
    fi

    docker cp $SERVICE:/go/src/stash.ezesoft.net/imsacnt/ibor-transaction-master/tests/bdds/client/report/cucumber_report.json ./tests/bdds/client/report
    
}

time run_tests $1

cleanup