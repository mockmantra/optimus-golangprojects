export Connection_String="root:admin123@tcp(127.0.0.1:3306)/testDb"
export VAULT_CERT="vault_cert_value"
export VAULT_KEY="vault_key_value"
export VAULT_TOKEN="vault_token_value"
export AWS_PROXY_URL="http://localhost:4568"
export AWS_UP_STREAM_NAME="CastleAccountingTransaction01"
export AWS_DOWN_STREAM_NAME="CastleAccountingTransactionIntraday01"
export PORT=6989
go run src/cmd/ibor-transaction-master/main.go