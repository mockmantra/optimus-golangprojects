#!/bin/bash

uuid=`uuidgen`

if [ -z "$stack_version" ] ; then
  echo "version not defined"
  exit -1
fi


cat > rancher-compose.yml <<EOF
version: '2'
.catalog:
  name: "ibor-transaction-read-stack"
  version: "$stack_version"
  description: "Ibor transaction master read api stack"
  uuid: $uuid
  questions:
  - variable: VaultAddr
    description: "Vault Address"
    label: "Vault Address"
    type: "string"
    required: true
    default: "https://vaultdev.ezesoft.net"
  - variable: imsbaseurl
    description: "IMS Base URL"
    label: "IMS Base URL"
    type: "string"
    required: true
    default: "http://eze-route:8001"
  - variable: awsregion
    description: "AWS Region"
    label: "AWS Region"
    type: "string"
    required: true
    default: "us-east-1"
  - variable: runenv
    label: "ENVIRONMENT"
    type: "string"
    required: true
    default: "Castle"
  - variable: dbinstancehosts
    description: "DB Instance hosts separated by commas to connect to Aurora"
    label: "DB Instance hosts"
    type: "string"
    required: false
    default: ""
services:
  ibor-transaction-read:
    scale: 2
    drain_timeout_ms: 10000
    upgrade_strategy:
      start_first: true
    health_check:
      port: 33380
      request_line: GET /api/ibor/transaction-read/v1/health
      interval: 2000
      initializing_timeout: 60000
      unhealthy_threshold: 2
      strategy: recreate
      healthy_threshold: 2
      response_timeout: 2000

  ibor-transaction-read-lb:
    scale: 2
    upgrade_strategy:
      start_first: true
    start_on_create: true
    lb_config:
      port_rules:
      - path: /api/ibor/transaction-read/v1
        priority: 1
        protocol: http
        service: ibor-transaction-read
        source_port: 36857
        target_port: 33380
EOF