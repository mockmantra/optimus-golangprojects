package ezeutils

import (
	"context"
	"crypto/rand"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"stash.ezesoft.net/ipc/ezelogger"
)

// FromRequest extracts the activity id from req, if present.
func FromRequest(req *http.Request) string {
	return req.Header.Get("X-Request-Id")
}

// The sessionKey type is unexported to prevent collisions with context keys defined in
// other packages.
type key string

// ActivityID type
type ActivityID struct {
	ID string
}

// sessionKey is the context key for the eze session context
const (
	sessionKey key = "sessionKey"
)

const (
	XRequestIdKey      = "X-Request-Id" // This is ActivityID
	UserNameKey        = "UserName"
	FirmAuthTokenKey   = "FirmAuthToken"
	AuthorizationKey   = "Authorization"
	XOperationKey      = "X-Operation"
	XTraceKey          = "X-Trace"
	XRequestTimeoutKey = "X-Request-Timeout"
	UserAgentKey       = "User-Agent"
)

func getActivityID() (string, error) {
	b := make([]byte, 16)
	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("0E050403-%X-%X-%X-%X", b[4:6], b[6:8], b[8:10], b[10:]), nil
}

func WithActivityID(ctx context.Context, activityID string) context.Context {
	if activityID == "" {
		newActivityId, err := getActivityID()
		if err != nil {
			log.Println("Error generating ActivityId: ", err)
			return ctx
		}
		activityID = newActivityId
	}
	return WithKeyValue(ctx, XRequestIdKey, activityID)
}

func WithKeyValue(ctx context.Context, key, value string) context.Context {
	// ctx.Value returns nil if ctx has no value for the key;
	if values, ok := ctx.Value(sessionKey).(SessionContext); ok {
		values.data[key] = value
		return ctx
	} else {
		keyValues := SessionContext{map[string]string{}}
		keyValues.data[key] = value
		return context.WithValue(ctx, sessionKey, keyValues)
	}
}

// NewContext returns a new Context carrying data from header
func WithRequest(ctx context.Context, req *http.Request) (context.Context, context.CancelFunc) {
	activityID := req.Header.Get(XRequestIdKey)
	ctx = WithActivityID(ctx, activityID)
	authHeaders := req.Header.Get(AuthorizationKey)
	ctx = WithKeyValue(ctx, AuthorizationKey, authHeaders)
	if len(authHeaders) > 0 {
		ctx = WithKeyValue(ctx, AuthorizationKey, authHeaders)
	}
	reqOperation := req.Header.Get(XOperationKey)
	if len(reqOperation) > 0 {
		ctx = WithKeyValue(ctx, XOperationKey, reqOperation)
	}
	reqTrace := req.Header.Get(XTraceKey)
	if len(reqTrace) > 0 {
		ctx = WithKeyValue(ctx, XTraceKey, reqTrace)
	}
	reqTimeout := req.Header.Get(XRequestTimeoutKey)
	if len(reqTimeout) > 0 {
		ctx = WithKeyValue(ctx, XRequestTimeoutKey, reqTimeout)
	}
	var timeoutDuration time.Duration
	timeout, err := strconv.Atoi(req.Header.Get(XRequestTimeoutKey))
	if err == nil {
		timeoutDuration = time.Duration(timeout) * time.Millisecond
	} else {
		timeoutDuration = time.Duration(49) * time.Second
	}
	ctx, cancel := context.WithDeadline(ctx, time.Now().Add(timeoutDuration))
	return ctx, cancel
}

// FromContext extracts the activity id from ctx, if present.
func FromContext(ctx context.Context, key string) (string, bool) {
	if ctx == nil {
		return "", false
	}
	// ctx.Value returns nil if ctx has no value for the key;
	if values, ok := ctx.Value(sessionKey).(SessionContext); ok {
		return values.Get(key)
	}
	return "", false
}

// setRequestHeadersFromContext transfers the headers of the context to the  http request
func SetRequestHeadersFromContext(ctx context.Context, req *http.Request, log *ezelogger.EzeLog) {

	if authHeaders, ok := FromContext(ctx, AuthorizationKey); ok {
		req.Header[AuthorizationKey] = []string{authHeaders}
	}

	if reqID, ok := FromContext(ctx, XRequestIdKey); ok {
		req.Header[XRequestIdKey] = []string{reqID}
	}

	if ope, ok := FromContext(ctx, XOperationKey); ok {
		req.Header[XOperationKey] = []string{ope}
	}

	if trace, ok := FromContext(ctx, XTraceKey); ok {
		req.Header[XTraceKey] = []string{trace}
	}

	if userAgent, ok := FromContext(ctx, UserAgentKey); ok {
		req.Header[UserAgentKey] = []string{userAgent}
	}

	req.Header.Set("Content-Type", "application/json")

	if deadline, ok := ctx.Deadline(); ok {
		timeRemaining := deadline.Sub(time.Now())
		timeoutString := strconv.Itoa(int(timeRemaining.Nanoseconds() / 1000000))
		if log != nil {
			log.WithContext(ctx).WithTracer().Infof("Time remaining %s ms", timeoutString)
		}
		req.Header[XRequestTimeoutKey] = []string{timeoutString}
	}
}

// setRequestHeaders transfers the headers of the original http request to another request
func SetRequestHeaders(ctx context.Context, originalHTTPRequest *http.Request, req *http.Request, log *ezelogger.EzeLog) {

	authHeaders := originalHTTPRequest.Header[AuthorizationKey]
	if len(authHeaders) > 0 && len(authHeaders[0]) > 0 {
		req.Header[AuthorizationKey] = authHeaders
	}

	reqID := originalHTTPRequest.Header[XRequestIdKey]
	if len(reqID) > 0 && len(reqID[0]) > 0 {
		req.Header[XRequestIdKey] = reqID
	}

	reqOperation := originalHTTPRequest.Header[XOperationKey]
	if len(reqOperation) > 0 && len(reqOperation[0]) > 0 {
		req.Header[XOperationKey] = reqOperation
	}

	reqTrace := originalHTTPRequest.Header[XTraceKey]
	if len(reqTrace) > 0 && len(reqTrace[0]) > 0 {
		req.Header[XTraceKey] = reqTrace
	}

	if userAgent, ok := FromContext(ctx, UserAgentKey); ok {
		req.Header[UserAgentKey] = []string{userAgent}
	}

	req.Header.Set("Content-Type", "application/json")

	if deadline, ok := ctx.Deadline(); ok {
		timeRemaining := deadline.Sub(time.Now())
		timeoutString := strconv.Itoa(int(timeRemaining.Nanoseconds() / 1000000))
		if log != nil {
			log.WithContext(ctx).WithTracer().Infof("Time remaining %s ms", timeoutString)
		}
		req.Header[XRequestTimeoutKey] = []string{timeoutString}
	}

}

func (s *SessionContext) LogMessageFromContext(ctx context.Context) (string, bool) {
	var msg []string
	if activityID, ok := FromContext(ctx, XRequestIdKey); ok {
		msg = append(msg, fmt.Sprintf(`ActivityId="%s"`, activityID))
	}

	if userName, ok := FromContext(ctx, UserNameKey); ok {
		msg = append(msg, fmt.Sprintf(`UserName="%s"`, userName))
	}

	if firmAuthToken, ok := FromContext(ctx, FirmAuthTokenKey); ok {
		msg = append(msg, fmt.Sprintf(`FirmAuthToken="%s"`, firmAuthToken))
	}

	if xTrace, ok := FromContext(ctx, XTraceKey); ok {
		msg = append(msg, fmt.Sprintf(`TraceLevel="%s"`, xTrace))
	}
	return strings.Join(msg, " "), true
}

type SessionContext struct {
	data map[string]string
}

func (sessionContext *SessionContext) Get(key string) (string, bool) {
	if val, ok := sessionContext.data[key]; ok {
		return val, ok
	}
	return "", false
}

func SetLogger(log *ezelogger.EzeLog) {
	log.AddLoggable(&SessionContext{})
	RetryableHttpClient.Logger = log
}
