package ezeutils

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/pkg/errors"
	"stash.ezesoft.net/ipc/ezelogger"
)

// UserSession defines the current context's user info fetch from the session
type UserSession struct {
	FirmID          int    `json:"FirmId"`
	UserID          int    `json:"UserId"`
	FirmName        string `json:"FirmName"`
	UserName        string `json:"UserName"`
	FirmAuthToken   string `json:"FirmAuthToken"`
	EnvironmentName string `json:"EnvironmentName"`
}

type sessionResponse struct {
	UserSession UserSession `json:"UserSession"`
}

//AuthorizeRequestFromContext copy the headers from the context first, then authorize the request using those headers
func AuthorizeRequestUsingContext(ctx context.Context, baseUrl string, log *ezelogger.EzeLog) (*UserSession, context.Context, error) {
	req, err := NewRequest("GET", baseUrl+"/api/platform/v1/session", nil)
	if err != nil {
		return nil, ctx, err
	}
	req.Request = req.WithContext(ctx)

	SetRequestHeadersFromContext(ctx, req.Request, log)

	if ctx.Err() != nil {
		return nil, nil, errors.Wrap(ctx.Err(), "Error before authorizing request")
	}

	return processAuthorizationRequest(ctx, req, log)

}

func AuthorizeRequest(ctx context.Context, baseUrl string, originalHTTPRequest *http.Request, log *ezelogger.EzeLog) (*UserSession, context.Context, error) {
	req, err := NewRequest("GET", baseUrl+"/api/platform/v1/session", nil)
	if err != nil {
		return nil, ctx, err
	}
	req.Request = req.WithContext(ctx)

	SetRequestHeaders(ctx, originalHTTPRequest, req.Request, log)

	if ctx.Err() != nil {
		return nil, nil, errors.Wrap(ctx.Err(), "Error before authorizing request")
	}

	return processAuthorizationRequest(ctx, req, log)

}

//AuthorizeRequest authorize the request and copy the headers from original request
func processAuthorizationRequest(ctx context.Context, req *Request, log *ezelogger.EzeLog) (*UserSession, context.Context, error) {

	log.WithContext(ctx).Info(fmt.Sprintf("Auth URL Protocol: %v, Host: %v, Path: %v", req.URL.Scheme, req.URL.Host, req.URL.Path))
	httpCl := RetryableHttpClient
	resp, err := httpCl.Do(req)

	if err != nil {
		err = errors.Wrap(err, "Error when sending session request")
		if resp != nil {
			return nil, ctx, &StatusError{Code: resp.StatusCode, Err: errors.Wrap(err, resp.Status)}
		}
		return nil, ctx, &StatusError{Code: 500, Err: errors.Wrap(err, "Internal Authorization Error: got nil response")}
	}
	defer resp.Body.Close()
	switch resp.StatusCode {
	case 200:
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, ctx, &StatusError{Code: 500, Err: errors.Wrap(err, "Error when reading session request body")}
		}
		user, err := getUser(body)
		activityId, _ := FromContext(ctx, XRequestIdKey)
		ctx = WithActivityID(ctx, activityId)
		ctx = WithKeyValue(ctx, UserNameKey, user.UserName)
		ctx = WithKeyValue(ctx, FirmAuthTokenKey, user.FirmAuthToken)
		if err != nil {
			return nil, ctx, &StatusError{Code: 500, Err: errors.Wrap(err, "Error when user from request body")}
		}
		return user, ctx, nil
	case 304:
		return nil, ctx, nil
	default:
		return nil, ctx, &StatusError{Code: resp.StatusCode, Err: errors.New(resp.Status)}
	}
}

func getUser(respBody []byte) (*UserSession, error) {
	var session sessionResponse
	err := json.Unmarshal(respBody, &session)
	if err != nil {
		return nil, err
	}
	return &session.UserSession, nil
}
