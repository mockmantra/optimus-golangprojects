package ezelogger

import "context"
import "fmt"

// The key type is unexported to prevent collisions with context keys defined in
// other packages.
type key string

// Tracer struct is used for storing tracing metadata to be propagated through call tree
type Tracer struct {
	Message string
}

// tracerKey is the context key for the tracer.
const tracerKey key = "Tracer"

// NewTracerContext returns a new Context carrying tracer.
func NewTracerContext(ctx context.Context, tracer Tracer) context.Context {
	if ctxTracer, ok := ctx.Value(tracerKey).(Tracer); ok {
		tracer.Message = fmt.Sprintf("%s: %s", ctxTracer.Message, tracer.Message)
	}
	return context.WithValue(ctx, tracerKey, tracer)
}

// tracerFromContext extracts the tracer from ctx, if present.
func tracerFromContext(ctx context.Context) (Tracer, bool) {
	// the string type assertion returns ok=false for nil.
	tracer, ok := ctx.Value(tracerKey).(Tracer)
	return tracer, ok
}
