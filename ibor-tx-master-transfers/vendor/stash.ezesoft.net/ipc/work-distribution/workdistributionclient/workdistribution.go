package workdistributionclient

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"github.com/pkg/errors"
	"stash.ezesoft.net/ipc/ezeutils"
)

const (
	failuresBeforeQuitting = 4
)

var baseUrl string
var retryMs int = 5000

func init() {
	//baseUrl = "https://castle.ezesoftcloud.com"
	baseUrl = os.Getenv("IMS_BASE_URL")
	if baseUrl == "" {
		panic("No environment base url set. Env variable IMS_BASE_URL must be set for service to run.")
	}
}

// note: caller must consume all the channels, otherwise daemon will block.
func SubscribeToWork(ctx context.Context, workerGroupName, workType string, metadata *map[string]string) (chan WorkerAssignments, chan ClientStatus, chan bool, error) {
	assingmentsChan := make(chan WorkerAssignments)
	clientStatusChan := make(chan ClientStatus, 100)
	enrollNewWorker := true
	noWork := WorkerAssignments{WorkItems: make(WorkItems, 0)}
	var assignment WorkerAssignments
	var remainingTTL string
	var err error
	quitWorker := make(chan string)
	quit := make(chan bool)
	go func() {
		failures := 0
		timeWorkerRetires := time.Now()
		var workerID string
		var assignmentIndex string
		var previousPollSuccessful bool
		var workerTTL time.Duration
		var recoverable bool
		for {
			if enrollNewWorker { // first time, and after every polling failure
				assignment, remainingTTL, err = announceWorker(ctx, workerGroupName, workType, metadata)
				if remainingTTL != "" {
					workerTTL, err = time.ParseDuration(remainingTTL)
					if err == nil {
						timeWorkerRetires = time.Now().Add(workerTTL)
					} else {
						select { // send error - non blocking.
						case clientStatusChan <- ClientStatus{Error: errors.Wrapf(err, "failed to parse worker's RemainigTTL '%s' from initial creation", remainingTTL)}:
						default:
						}
						workerTTL = time.Second * 10 // minimum TTL in consul/WD is 10s
					}
				}
				enrollNewWorker = err != nil
				if enrollNewWorker {
					select { // send error - non blocking
					case clientStatusChan <- ClientStatus{Error: err, Failures: failures}:
					default:
					}
					select {
					case <-time.After(time.Millisecond * time.Duration(retryMs)):
					case <-ctx.Done():
						quitWorker <- workerID
						return
					}
					failures++
					if failures >= failuresBeforeQuitting {
						// we could not enlist long enough - let someone else take the work.
						assingmentsChan <- noWork
					}
					continue
				} else {
					assingmentsChan <- assignment
					previousPollSuccessful = true
				}
				workerID = assignment.Id
				assignmentIndex = "0"
				failures = 0
				clientStatusChan <- ClientStatus{Message: "Try Polling worker"}
				time.Sleep(time.Second) // sleep the worker for 1 sec after enrolling to allow it's presence to propagate to all consul nodes
			}

			// this is why we're here for: we either announced or polled successfully, send results over the channel
			assignment, remainingTTL, recoverable, err = pollWorker(ctx, workerID, assignmentIndex)
			if remainingTTL != "" {
				duration, err := time.ParseDuration(remainingTTL)
				if err == nil {
					timeWorkerRetires = time.Now().Add(duration)
				} else {
					select { // send error - non blocking.
					case clientStatusChan <- ClientStatus{Error: errors.Wrapf(err, "failed to parse worker's RemainigTTL '%s' from assignment", remainingTTL)}:
					default:
					}
					duration = time.Second * 10 // minimum TTL in consul/WD is 10s
				}
			}
			if err == nil {
				assignmentIndex = assignment.Index
				assingmentsChan <- assignment
				enrollNewWorker = false
				previousPollSuccessful = true
			} else {
				if ctx.Err() != nil {
					quitWorker <- workerID
					return
				}
				if previousPollSuccessful && remainingTTL == "" {
					timeWorkerRetires = time.Now().Add(workerTTL * 2 / 3)
				}
				previousPollSuccessful = false
				if !recoverable || time.Now().After(timeWorkerRetires) {
					enrollNewWorker = true
					select { // send a warning - non blocking.
					case clientStatusChan <- ClientStatus{Warning: errors.Wrapf(err, "Retiring failed worker '%s'", workerID).Error()}:
					default:
					}
					assingmentsChan <- noWork
					err := resign(ctx, workerID)
					if err != nil {
						clientStatusChan <- ClientStatus{Warning: errors.Wrapf(err, "Failed to retire worker '%s'", workerID).Error()}
					}
				} else {
					select { // send a warning - non blocking.
					case clientStatusChan <- ClientStatus{Warning: errors.Wrapf(err, "Retrying failed poll until '%v'", timeWorkerRetires).Error()}:
					default:
					}
					select {
					case <-ctx.Done():
						quitWorker <- workerID
						return
					case <-time.After(time.Millisecond * 1000):
					}
				}
			}

			// Always send assignment, even if it hasn't changed
			select {
			case <-ctx.Done():
				quitWorker <- workerID
				//need to unlock here, but api doesn't currently allow that. or the api should be updated so that removing worker should release locks
				return
			default:
			}
		}
	}()

	go func() {
		workerID := <-quitWorker // block until quit
		err := resign(ctx, workerID)
		if err != nil {
			clientStatusChan <- ClientStatus{Error: err}
		}
		clientStatusChan <- ClientStatus{Message: "Quit and resign"}
		close(quit)
	}()

	return assingmentsChan, clientStatusChan, quit, nil
}

func resign(ctx context.Context, workerId string) error {

	req, err := http.NewRequest(http.MethodDelete, baseUrl+"/api/work-distribution/v1/workers/"+workerId, nil)
	if err != nil {
		return err
	}
	ctx = ezeutils.WithActivityID(ctx, "")
	ezeutils.SetRequestHeadersFromContext(ctx, req, nil)
	response, err := ezeutils.HttpClient.Do(req)
	if err != nil {
		return err
	}

	if response.StatusCode != http.StatusNoContent {
		return errors.Errorf("Deleting worker did not return %d, %s", response.StatusCode, response.Status)
	}
	return nil
}

// AnnounceWorker tells worker distribution service that a worker is available and should be added to the worker pool
func announceWorker(ctx context.Context, workerGroupName, workType string, metadata *map[string]string) (WorkerAssignments, string, error) {
	var response WorkerAssignments
	var remainingTTL string

	type workApplicationForm struct {
		WorkerGroup string
		WorkType    string
		Metadata    *map[string]string
	}

	application := workApplicationForm{
		WorkerGroup: workerGroupName,
		WorkType:    workType,
		Metadata:    metadata,
	}
	queryBody, err := json.Marshal(application)
	if err != nil {
		return response, remainingTTL, err
	}
	req, err := http.NewRequest(http.MethodPost, baseUrl+"/api/work-distribution/v1/workers", bytes.NewBuffer(queryBody))
	if err != nil {
		return response, remainingTTL, err
	}
	req = req.WithContext(ctx)
	ctx = ezeutils.WithActivityID(ctx, "")
	ezeutils.SetRequestHeadersFromContext(ctx, req, nil)

	req.Header.Set("Content-Type", "application/json")

	httpCl := ezeutils.HttpClient
	resp, err := httpCl.Do(req)

	if err != nil {
		err = errors.Wrap(err, "Error when sending work distribution post request")
		if resp != nil {
			return response, remainingTTL, errors.Wrap(err, resp.Status)
		}
		return response, remainingTTL, errors.Wrap(err, "Internal Error: got nil response")
	}
	defer resp.Body.Close()

	remainingTTLHeader := resp.Header["X-Remaining-Ttl"]
	if len(remainingTTLHeader) > 0 && len(remainingTTLHeader[0]) > 0 {
		remainingTTL = remainingTTLHeader[0]
	}

	switch resp.StatusCode {
	case 201:
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return response, remainingTTL, errors.Wrap(err, "Error when reading work distribution response body")
		}
		err = json.Unmarshal(body, &response)
		if err != nil {
			return response, remainingTTL, errors.Wrap(err, "announce worker: error when unmarshalling work distribution response body")
		}
		distributionIndex := resp.Header["X-Workdistribution-Index"]
		if len(distributionIndex) > 0 && len(distributionIndex[0]) > 0 {
			response.Index = distributionIndex[0]
		}
		return response, remainingTTL, nil
	default:
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("respAW3 scode %v\n", resp)
			return response, remainingTTL, errors.Wrap(err, "Error when reading work distribution response body")
		}

		var errorResp map[string]string
		err = json.Unmarshal(body, &errorResp)
		if err != nil {
			return response, remainingTTL, errors.Wrap(err, "announce worker: error when unmarshalling work distribution response body")
		}

		if errorMessage, ok := errorResp["message"]; ok {
			return response, remainingTTL, errors.Wrap(err, errorMessage)
		}
		return response, remainingTTL, errors.New(resp.Status)
	}
}

// PollWorker initiates a long poll against the service that returns either the same work data or new work data and a new index
func pollWorker(ctx context.Context, workerId, index string) (WorkerAssignments, string, bool, error) {
	var response WorkerAssignments
	var remainingTTL string
	req, err := http.NewRequest(http.MethodGet, baseUrl+"/api/work-distribution/v1/workers/"+workerId+"?index="+index, nil)
	if err != nil {
		return response, remainingTTL, true, err
	}
	req = req.WithContext(ctx)
	ctx = ezeutils.WithActivityID(ctx, "")
	ezeutils.SetRequestHeadersFromContext(ctx, req, nil)

	httpCl := ezeutils.HttpClient
	resp, err := httpCl.Do(req)

	if err != nil {
		err = errors.Wrap(err, "Error when sending work distribution get request")
		if resp != nil {
			return response, remainingTTL, true, errors.Wrap(err, resp.Status)
		}
		return response, remainingTTL, true, errors.Wrap(err, "Internal Error: got nil response")
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	remainingTTLHeader := resp.Header["X-Remaining-Ttl"]
	if len(remainingTTLHeader) > 0 && len(remainingTTLHeader[0]) > 0 {
		remainingTTL = remainingTTLHeader[0]
	}

	switch resp.StatusCode {
	case 200:
		if err != nil {
			return response, remainingTTL, true, errors.Wrap(err, "error when reading work distribution response body")
		}
		err = json.Unmarshal(body, &response)
		if err != nil {
			return response, remainingTTL, true, errors.Wrapf(err, "poll worker: error when unmarshalling work distribution response body='%s'", string(body))
		}

		distributionIndex := resp.Header["X-Workdistribution-Index"]
		if len(distributionIndex) > 0 && len(distributionIndex[0]) > 0 {
			response.Index = distributionIndex[0]
		}

		return response, remainingTTL, true, nil
	default:
		if err != nil {
			return response, remainingTTL, true, errors.Wrapf(err, "error when reading work distribution response body, code=%d, status='%s'", resp.StatusCode, resp.Status)
		}
		recoverable := resp.StatusCode != 410 // we can't recover if the worker is gone (code 410)
		return response, remainingTTL, recoverable, fmt.Errorf("failed to get assignments, code=%d, status='%s', body='%s'", resp.StatusCode, resp.Status, string(body))
	}
}

func UpdateWorkItem(ctx context.Context, workType, workId string, metadata interface{}) (*WorkWorkItem, error) {
	workItem := WorkWorkItem{Id: workId, Metadata: metadata, Enabled: true}
	workData, err := json.Marshal(workItem)
	if err != nil {
		return nil, errors.Wrap(err, "Unable to marshal work items")
	}

	req, err := http.NewRequest(http.MethodPut, baseUrl+"/api/work-distribution/v1/work/"+workType+"/"+workId, bytes.NewBuffer(workData))
	if err != nil {
		return nil, errors.Wrap(err, "failed to create new hhtp request object")
	}
	req = req.WithContext(ctx)
	ctx = ezeutils.WithActivityID(ctx, "")
	ezeutils.SetRequestHeadersFromContext(ctx, req, nil)

	httpCl := ezeutils.HttpClient
	resp, err := httpCl.Do(req)

	if err != nil {
		err = errors.Wrap(err, "Error when updating work item through work distribution service")
		if resp != nil {
			err = errors.Wrap(err, resp.Status)
		} else {
			err = errors.Wrap(err, "Internal Error: nil response")
		}
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	switch resp.StatusCode {
	case 200:
		if err != nil {
			return nil, errors.Wrap(err, "Error when reading work distribution response body")
		}
		response := &WorkWorkItem{}
		err = json.Unmarshal(body, response)
		if err != nil {
			err = errors.Wrap(err, "Error unmarshaling work distribution 'work' response body")
			return nil, err
		}
		return response, nil
	default:
		return nil, errors.New(resp.Status)
	}
}

// LockWorkItems tells worker distribution service to lock a list of work items on an id
func LockWorkItems(ctx context.Context, workerId string, workItems []WorkItem) error {
	workItemSlice := make([]string, len(workItems))

	for _, workItem := range workItems {
		workItemSlice = append(workItemSlice, workItem.WorkId)
	}

	workData, err := json.Marshal(workItemSlice)
	if err != nil {
		return errors.Wrap(err, "Unable to marshal work items")
	}

	req, err := http.NewRequest("POST", baseUrl+"/api/work-distribution/v1/workers/"+workerId+"/locks", bytes.NewBuffer(workData))
	if err != nil {
		return err
	}
	req = req.WithContext(ctx)
	ctx = ezeutils.WithActivityID(ctx, "")
	ezeutils.SetRequestHeadersFromContext(ctx, req, nil)

	req.Header.Set("Content-Type", "application/json")

	httpCl := ezeutils.HttpClient
	resp, err := httpCl.Do(req)

	if err != nil {
		err = errors.Wrap(err, "Error when sending work distribution post request")
		if resp != nil {
			return errors.Wrap(err, resp.Status)
		}
		return errors.Wrap(err, "Internal Error: got nil response")
	}
	defer resp.Body.Close()

	switch resp.StatusCode {
	case 200:
		return nil
	default:
		return errors.New(resp.Status)
	}
}
