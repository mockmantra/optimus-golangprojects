# Go-Bitemporality v1

This package supports bitemporal Selects and Insertions.

## Table of contents
* [Prerequisites](#Prerequisites)
* [Installation](#Installation)
* [Usage](#Usage)
    * [Initialization](#Initialization)
        * [BtObj](#BtObj)
            * [TableName](#TableName)
            * [Database](#Database)
            * [TableModel](#TableModel)
            * [UniqueKey](#UniqueKey)
        * [Others](#Others)
            * [WhereRange](#WhereRange)
            * [Payload](#Payload)
            * [ValidFromTableKey](#ValidFromTableKey)
            * [ValidToTableKey](#ValidToTableKey)
    * [Selects](#Selects)
        * [SelectFromTo](#SelectFromTo)
        * [SelectActive](#SelectActive)
        * [SelectDeleted](#SelectDeleted)
        * [SelectActiveAtPointOfTime](#SelectActiveAtPointOfTime)
    * [Inserts](#Inserts)
        * [Insert](Insert)
        * [Delete](Delete)
*   [Authors](#Authors)
*   [Contributing](#Contributing)
*   [License](#License)


## Prerequisites

Table being used needs 1 mandatory column:
1. "IsDeleted" - This column is the flag to check if the record has been deleted or not (0/1) 

## Installation 

Add the import to your go file

```
import bt "stash.ezesoft.net/imsacnt/go-bitemporality"
```
Run 
```
dep init
dep ensure
```

## Usage

### Initialization

Start by declaring a [BtObj](#BtObj) and it's required parameters.


#### BtObj

Create an instance of this structure, which will hold all the information required to use this package against your table. 

##### BtObj Struct
```
type BtObj struct {
	TableName  string                 // Name of the table to run this package on
	Database   *gorm.DB               // Instance of gorm based database object
	TableModel interface{}            // empty instance of table model
	UniqueKey  map[string]interface{} // map of the unique keys for the records
}
```

##### TableName
Name of the table the package is going to operated on.

##### Database
Instance of the database object (gorm type)

##### TableModel
Empty instance of the table model
```
var tableModel models.Transactiontable
var tableModelArr []models.Transactiontable
```

##### UniqueKey
This holds all the WHERE conditions which form the Unique Key identifier. __This paramater is required__. Can be a single column or combination of multiple columns

```
uniqueKey := make(map[string]interface{})
uniqueKey["UserName"] = "hansolo"

// Or can be a combination 
// where["OrderID"] = "123"
// where["RouteID"] = "456"
```

#### Others

##### WhereRange
This holds the WHERE condition which helps select multiple records between a range of 2 columns and their values. 

```
whereRange := bt.WhereRange{
    FromKey: "ValidFromUTC",                // Column 1 name
    FromVal: "2018-10-17 04:56:11.290595",  // Column 1 value
    ToKey:   "ValidToUTC",                  // Column 2 name
    ToVal:   "2018-10-17 05:04:04.853826",  // Column 2 value
}
```
is equal to
```
WHERE FromKey >= FromVal AND ToKey <= ToVal
```


##### Payload
This is an instance of table model loaded with data which needs to be saved into the database. 

##### ValidFromTableKey
This is the *column name* in the table which has the value of when that record was inserted. *It is equivalent to "ValidFromUTC" in transaction-master*.

##### ValidToTableKey
This is the *column name* in the table which has the value of when that record was updated with a new record. Whenever a new record is added to the table it updates the already existing record for a specific unique id with the "ValidToTableKey" = updation date/time. For the new record which is inserted - the "ValidToTableKey" = NULL. *It is equivalent to "ValidToUTC" in transaction-master*.


### Selects

1. [SelectFromTo()](#SelectFromTo)
2. [SelectActive()](#SelectActive)
3. [SelectDeleted()](#SelectDeleted)
4. [SelectActiveAtPointOfTime()](#SelectActiveAtPointOfTime)


#### SelectFromTo
Returns all the records based on the clause(s) in [UniqueKey](#UniqueKey) AND [WhereRange](#WhereRange). 
* Required:
    1. UniqueKey
        ```
        uniqueKey := make(map[string]interface{})      
        uniqueKey["UserName"] = "hansolo"    
        ```
    2. WhereFrom
        ```
        whereRange := bt.WhereRange{
            FromKey: "ValidFromUTC",
            FromVal: "2018-10-17 04:56:11.290595",
            ToKey:   "ValidToUTC",
            ToVal:   "2018-10-17 05:04:04.853826",
        }    
        ```

```
var tableModel []models.Transactiontable

whereRange := bt.WhereRange{
    FromKey: "ValidFromUTC",
    FromVal: "2018-10-17 04:56:11.290595",
    ToKey:   "ValidToUTC",
    ToVal:   "2018-10-17 05:04:04.853826",
}

uniqueKey := make(map[string]interface{})
uniqueKey["UserName"] = "hansolo"

bitemporalObj := bt.BtObj{
    TableName:          "Ibor_Transactions",
    Database:           da.db,
    TableModel:         &tableModel,
    UniqueKey:          uniqueKey,
}

records, count, err := bitemporalObj.SelectFromTo(whereRange)
if err != nil {
    return err
}

// check for 0 count as well
// gorm isn't consistent how it returns when there are no records - it's either an error, or no error with 0 records
if count == 0 {
    return errors.New("No records found")
}
```

#### SelectActive
Returns the record with:
1. [ValidToTableKey](#ValidToTableKey) column =  NULL   
2. [IsDeleted](#Prerequisites) = 0 

* Required:
    1. UniqueKey
        ```
        UniqueKey := make(map[string]interface{})      
        UniqueKey["UserName"] = "hansolo"    
        ```
    2. ValidToTableKey
        ```
        ValidToTableKey = "ValidToUTC"
        ```
        

```
var tableModel models.Transactiontable
uniqueKey := make(map[string]interface{})
uniqueKey["UserName"] = "hansolo"
validToTableKey := "ValidToUTC"

bitemporalObj := bt.BtObj{
    TableName:          "Ibor_Transactions",
    Database:           da.db,
    TableModel:         &tableModel,
    UniqueKey:          uniqueKey,
}

records, count, err := bitemporalObj.SelectActive(validToTableKey)
if err != nil {
    return err // can be record not found error
}

// check for 0 count as well
// gorm isn't consistent how it returns when there are no records - it's either an error, or no error with 0 records
if count == 0 {
    return errors.New("No records found")
}
```


#### SelectDeleted
Returns the record with [IsDeleted](#Prerequisites) column = 1  

* Required:
    1. UniqueKey
        ```
        UniqueKey := make(map[string]interface{})      
        UniqueKey["UserName"] = "hansolo"    
        ```

```
var tableModel models.Transactiontable
uniqueKey := make(map[string]interface{})
uniqueKey["UserName"] = "hansolo"
validToTableKey := "ValidToUTC"

bitemporalObj := bt.BtObj{
    TableName:          "Ibor_Transactions",
    Database:           da.db,
    TableModel:         &tableModel,
    UniqueKey:          uniqueKey,
}

records, count, err := bitemporalObj.SelectDeleted()
if err != nil {
    return err // can be record not found error
}

// check for 0 count as well
// gorm isn't consistent how it returns when there are no records - it's either an error, or no error with 0 records
if count == 0 {
    return errors.New("No records found")
}
```

#### SelectActiveAtPointOfTime

Returns the record which was active for unique identifier and passed point of time.

```
var tableModel models.Transactiontable
uniqueKey := make(map[string]interface{})
uniqueKey["UserName"] = "hansolo"
validToTableKey := "ValidToUTC"
validToTableKey := "ValidFromUTC"

bitemporalObj := bt.BtObj{
    TableName:          "Ibor_Transactions",
    Database:           da.db,
    TableModel:         &tableModel,
    UniqueKey:          uniqueKey,
}

records, count, err := bitemporalObj.SelectActiveAtPointOfTime(dateTime, validToTableKey, validFromTableKey)
if err != nil {
    return err // can be record not found error
}

// check for 0 count as well
// gorm isn't consistent how it returns when there are no records - it's either an error, or no error with 0 records
if count == 0 {
    return errors.New("No records found")
}

```


### Inserts

Types of inserts:
1. [Insert new record or Insert update record](#Insert)
2. [Insert delete record](#Delete) (*no actual deletion from database*)


#### Insert
Inserts a new record in database for a given [payload](#payload) and unique identifier in [UniqueKey](#UniqueKey). 

1. First insertion for a unique identifier: It adds a new record with ValidFromTableKey = provided data time and ValidToTableKey = NULL
2. Update insertion *(when record(s) with the unique identifier already exists)* : 
    * It updates the last record for that unique identifier with ValidToTableKey = NULL (*Active Record*) with ValidToTableKey = provided date time
    * It adds a new record with ValidFromTableKey = provided data time and ValidToTableKey = NULL


* *Note - arguement false when calling Insert(false)*

* Required:
    1. Payload
    2. UniqueKey
        ```
        where := make(map[string]interface{})      
        where["UserName"] = "hansolo"    
        ```
    3. [ValidFromTableKey](#ValidFromTableKey)
        ```
        ValidFromTableKey = "ValidFromUTC",
        ```
    4. [ValidToTableKey](#ValidToTableKey)
        ```
        ValidToTableKey =    "ValidToUTC",
        ```
    

```
var tableModel models.Transactiontable
uniqueKey := make(map[string]interface{})
uniqueKey["UserName"] = "hansolo"
validToTableKey := "ValidToUTC"
validFromTableKey := "ValidFromUTC"

bitemporalObj := bt.BtObj{
    TableName:          "Ibor_Transactions",
    Database:           da.db,
    TableModel:         &tableModel,
    UniqueKey:          uniqueKey,
}

useDateTime := time.Now().UTC()  // This can be any time which the user can pass
err := bitemporalObj.Insert(false, payload, useDateTime, validFromTableKey, validToTableKey)
if err != nil {
    return err
}
```


#### Delete

Exactly like [Insert](#Insert), except the __IsDeleted__ = true

* *Note - There is not actual deletion from database* 
* *Note - arguement true when calling Insert(true)*



```
err := bitemporalObj.Insert(true, payload, useDateTime, validFromTableKey, validToTableKey)

if err != nil {
    return err
}
```

## Authors
* Team Invictus *Eze.Eclipse.Accounting.Invictus.Hyd@ezesoft.com*
* Rohan Bajaj   *rbajaj@ezesoft.com*
* Sangeetha Araveeti *avijayalakshmi@ezesoft.com*
 

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D


## License

Copyright (c) 2018 Eze Software Group.   All rights reserved.

