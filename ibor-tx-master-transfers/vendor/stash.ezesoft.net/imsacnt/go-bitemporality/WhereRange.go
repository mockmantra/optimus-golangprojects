package bitemporality

// WhereRange contains the fields required to search for records within a range
// WHERE FromKey >= FromVal AND ToKey <= ToVal
type WhereRange struct {
	FromKey string
	FromVal string
	ToKey   string
	ToVal   string
}
