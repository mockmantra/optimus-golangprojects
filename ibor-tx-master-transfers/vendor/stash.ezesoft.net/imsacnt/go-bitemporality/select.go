package bitemporality

import (
	"errors"
	"time"
)

func formatStr(str string) string {
	return "[go-bitemporality]: " + str
}

// Validate BtObj:
// 1. TableName
// 2. Database object
// 3. TableModel
func (s *BtObj) commonValidation() error {
	if len(s.TableName) == 0 {
		return errors.New(formatStr("TableName missing"))
	}
	if s.Database == nil {
		return errors.New(formatStr("Database object missing"))
	}
	if s.TableModel == nil {
		return errors.New(formatStr("TableModel missing"))
	}

	return nil
}

// SelectFromTo selects all records for a unique identifier present in 'WHERE' and also has a range of inputs in 'WhereRange'
// SELECT * FROM 'TableName' WHERE (condition(s) in 'WHERE') AND (FromCol >= FromVal AND ToCol <= ToVal) <- this comes from 'WhereRange'
// Returns - all records, number of records, error if any
func (s *BtObj) SelectFromTo(whereRange WhereRange) (data interface{}, countData int, err error) {

	inputErr := s.commonValidation()
	if inputErr != nil {
		return nil, 0, inputErr
	}

	// Validate 'WHERE'
	if s.UniqueKey == nil {
		return nil, 0, errors.New(formatStr("SelectFromTo: UniqueKey missing, no clause for unique identifier"))
	}

	// Validate all fields of 'WhereRange'
	if len(whereRange.FromKey) == 0 || len(whereRange.FromVal) == 0 ||
		len(whereRange.ToKey) == 0 || len(whereRange.ToVal) == 0 {
		return nil, 0, errors.New(formatStr("SelectFromTo: Invalid WhereRange parameters, require all values - FromKey, FromVal, ToKey, ToVal"))
	}

	count := 0
	from := whereRange.FromKey
	fVal := whereRange.FromVal
	to := whereRange.ToKey
	tVal := whereRange.ToVal

	// need to do this because putting in .Where() directly adds extra ``
	w := from + ">=? AND " + to + "<=?"

	if err := s.Database.Table(s.TableName).Where(w, fVal, tVal).Where(s.UniqueKey).Debug().Find(s.TableModel).Count(&count).Error; err != nil {
		return nil, count, err
	}

	return s.TableModel, count, nil
}

// SelectActive returns the currently active record for a unique identifier present in 'UniqueKey' and :
// ValidToTableKey column IS NULL
// Returns - all active records, number of records, error if any (Ideally there should be only 1 active record for a unique identifier)
func (s *BtObj) SelectActive(validToTableKey string) (data interface{}, countData int, err error) {

	inputErr := s.commonValidation()
	if inputErr != nil {
		return nil, 0, inputErr
	}

	if s.UniqueKey == nil {
		return nil, 0, errors.New(formatStr("SelectActive: UniqueKey missing, no clause for unique identifier"))
	}

	if len(validToTableKey) == 0 {
		return nil, 0, errors.New(formatStr("SelectActive: ValidToTableKey missing"))
	}

	where := validToTableKey + " IS NULL"
	count := 0

	if err := s.Database.Table(s.TableName).Where(where).Where("IsDeleted =?", false).Where(s.UniqueKey).Debug().Find(s.TableModel).Count(&count).Error; err != nil {
		return nil, count, err
	}

	return s.TableModel, count, nil
}

// SelectDeleted returns the record marked as 'IsDeleted' for a unique identifier in 'WHERE'
// Returns - all deleted records, number of records, error if any (Ideally there should be only 1 delete record for a unique identifier)
func (s *BtObj) SelectDeleted() (data interface{}, countData int, err error) {

	inputErr := s.commonValidation()
	if inputErr != nil {
		return nil, 0, inputErr
	}

	if s.UniqueKey == nil {
		return nil, 0, errors.New(formatStr("SelectDeleted: Where missing, no clause for unique identifier"))
	}

	count := 0
	if err := s.Database.Table(s.TableName).Where("IsDeleted =?", true).Where(s.UniqueKey).Debug().Find(s.TableModel).Count(&count).Error; err != nil {
		return nil, count, err
	}

	return s.TableModel, count, nil
}

// SelectActiveAtPointOfTime returns the record which was active for unique identifier and passed point of time.
func (s *BtObj) SelectActiveAtPointOfTime(dateTime time.Time, validToTableKey string, validFromTableKey string) (data interface{}, countData int, err error) {
	inputErr := s.commonValidation()
	if inputErr != nil {
		return nil, 0, inputErr
	}

	// Validate 'WHERE'
	if s.UniqueKey == nil {
		return nil, 0, errors.New(formatStr("SelectActiveAtPointOfTime: UniqueKey missing, no clause for unique identifier"))
	}
	count := 0
	where := "(" + validFromTableKey + "<=? AND " + validToTableKey + ">?) OR (" + validFromTableKey + "<=? AND " + validToTableKey + " IS NULL)"
	if err := s.Database.Table(s.TableName).Where(where, dateTime, dateTime, dateTime).Where(s.UniqueKey).Debug().Find(s.TableModel).Count(&count).Error; err != nil {
		return nil, count, err
	}

	return s.TableModel, count, nil
}
