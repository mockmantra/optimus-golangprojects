package bitemporality

import "time"

// IBiTemporal ...
type IBiTemporal interface {
	SelectFromTo(whereRange WhereRange) (data interface{}, countData int, err error)
	SelectActive(validToTableKey string) (data interface{}, countData int, err error)
	SelectDeleted() (data interface{}, countData int, err error)
	Insert(isDeleted bool, payload interface{}, dateTime time.Time, validFromTableKey string, validToTableKey string) error
}
