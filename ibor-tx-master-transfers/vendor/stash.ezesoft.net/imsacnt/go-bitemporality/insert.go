package bitemporality

import (
	"errors"
	"reflect"
	"time"

	"github.com/jinzhu/gorm"
)

// Insert can be either :
// 1. Fresh Insert (first time)
// 2. Update
// 3. Insert a delete record
func (s *BtObj) Insert(isDeleted bool, payload interface{}, dateTime time.Time, validFromTableKey string, validToTableKey string) error {

	// If insert/update, but already marked as isDeleted for the unique identifier in 'WHERE'
	if isDeleted == false {
		deletedRecord, deleteCount, _ := s.SelectDeleted()
		if deletedRecord != nil || deleteCount > 0 {
			return errors.New(formatStr("Insert: Already marked as deleted, can't insert anymore"))
		}
	}

	// check date time
	if dateTime.IsZero() == true {
		return errors.New(formatStr("Insert: Date-time is zero"))
	}

	// Validate payload
	if payload == nil {
		return errors.New(formatStr("Insert: Payload missing"))
	}

	// Validate ValidFromTableKey
	if len(validFromTableKey) == 0 {
		return errors.New(formatStr("Insert: ValidFromTableKey missing"))
	}

	// Validate ValidToTableKey
	if len(validToTableKey) == 0 {
		return errors.New(formatStr("Insert: ValidToTableKey missing"))
	}

	// Get active record if exists
	record, count, err := s.SelectActive(validToTableKey)
	if err != nil && !gorm.IsRecordNotFoundError(err) {
		return err
	}

	// If Insert is - Delete insert, and last active record NOT found, then nothing to delete
	if isDeleted == true && count == 0 {
		return errors.New(formatStr("Insert Delete failed...last active record not found"))
	}

	// update last active record's 'ValidToTableKey' column
	if count == 1 {
		if errU := s.Database.Model(record).Update(validToTableKey, dateTime).Debug().Error; errU != nil {
			return errU
		}
	}

	// If Insert-Delete
	if isDeleted == true {
		payload = getPayloadToDelete(payload)
	}

	// Insert new record
	return insertNewRecord(s, payload, dateTime, validFromTableKey, validToTableKey)

}

// insertNewRecord inserts a fresh record with:
// 1. ValidFromTableKey column = dateTime of ValidToTableKey of previous row
// 2. ValidToTableKey column = NULL
func insertNewRecord(bitemporalObj *BtObj, payload interface{}, dateTime time.Time, validFromTableKey string, validToTableKey string) error {
	if errC := bitemporalObj.Database.Table(bitemporalObj.TableName).Create(payload).Update(validFromTableKey, dateTime).Update(validToTableKey, gorm.Expr("NULL")).Debug().Error; errC != nil {
		return errC
	}
	return nil
}

func getPayloadToDelete(payload interface{}) (deletePayload interface{}) {
	val := reflect.ValueOf(payload).Elem()
	for i := 0; i < val.NumField(); i++ {
		typeField := val.Type().Field(i)

		// set IsDeleted in payload
		if typeField.Name == "IsDeleted" {
			val.Field(i).SetBool(true)
		}
	}

	return payload
}
