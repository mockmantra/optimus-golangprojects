#!/bin/bash

source ./scripts/rancher-set-env.sh
set -e

echo "Starting ibor-transaction-master wrapper shell script"
# Get lowercase file path
envFile=`echo ./rancher-environments/$ENVIRONMENT.$REGION.env | tr '[:upper:]' '[:lower:]'`
if [ -f $envFile ]
then
    set -o allexport
    source $envFile
    set +o allexport
    echo "Environment variables loaded from $envFile."
else
    echo "Environment file $envFile does not exist. Environment variables not loaded from file."
fi
echo "Environment variable list:"
env | sed 's/^/  /'

echo "Starting service..."


if [[ “$APP_ENV” != “dev” && -n “$VAULT_ADDR” ]] ; then
	echo "Determining vault settings"
	if [ -z "$VAULT_SKIP_VERIFY" ]; then
		echo "VAULT_SKIP_VERIFY is not defined. Defaulting to false."
		VAULT_SKIP_VERIFY=false
	fi
	if [ -z "$VAULT_TOKEN" ]; then
		echo "VAULT_TOKEN is not defined. Defaulting to value from ~/.vault-token."
		export VAULT_TOKEN=$(cat ~/.vault-token)
	fi
	echo "Using $VAULT_ADDR as vault address"

	AWS_ACCESS_KEY=$(vault read -field=accesskey  /secret/ims/ibor-tx-master-transfers/aws)
   	if [ -n "$AWS_ACCESS_KEY" ]; then
		echo "Got aws accesskey from Vault."
	fi

   	AWS_SECRET_KEY=$(vault read -field=secretkey  /secret/ims/ibor-tx-master-transfers/aws)
   	if [ -n "$AWS_SECRET_KEY" ]; then
		echo "Got aws secretaccesskey from Vault."
	fi

	if [ -z "$VAULT_CERT" ]; then
		echo "VAULT_CERT is not defined. Defaulting to value from /cert/vault.pem."
		export VAULT_CERT="/cert/vault.pem"
	fi
	if [ -z "$VAULT_KEY" ]; then
		echo "VAULT_KEY is not defined. Defaulting to value from /cert/key.pem."
		export VAULT_KEY="/cert/key.pem"
	fi

	export  VAULT_ADDR VAULT_SKIP_VERIFY AWS_ACCESS_KEY AWS_SECRET_KEY VAULT_CERT VAULT_KEY
fi

export RANCHER_HOST_NAME=`curl http://rancher-metadata/latest/self/host/name`
export RANCHER_CONTAINER_NAME=`curl http://rancher-metadata/latest/self/container/name`
export PORT=6989

chmod +x $GOPATH/bin/ibor-tx-master-transfers
$GOPATH/bin/ibor-tx-master-transfers