set -e

BDD_CLIENT="bdd_client"
SERVICE="service"
MOCK_ECLIPSE="mock-eclipse"
DB="db"
NETWORK="bdd_network"

set -e

function cleanup {
    stop_containers
    cleanup_network
}

cleanup_network() {
    echo "Cleaning up network: $NETWORK"
    if docker network ls | grep $NETWORK
    then
        echo "found network: $NETWORK"
        docker network rm $NETWORK
   else
        echo "Network not found."
    fi
}

stop_containers(){
    echo "Step1 Stopping any zombie containers..."
    declare -a arr=($BDD_CLIENT $SERVICE $MOCK_IMS localstack/localstack)

    for i in "${arr[@]}"
    do
        echo "Removing $i ......"
        container_id=$(docker ps -aqf "name=$i")
        if [[ ! -z "$container_id" ]]
        then
            echo "Container found: " $container_id
            docker rm -f $container_id
        else
            echo "No container found for: $i"
        fi
    done

}

function run_tests {
    echo "called with param: $1"
    cleanup

    echo "Create network: $NETWORK"
    docker network create --driver bridge $NETWORK

    echo "Step 2 - Build mock-ims....."
    docker build -t $MOCK_ECLIPSE -f mock-eclipse/Dockerfile  mock-eclipse/

    echo "Step 3 - Run mock-ims....."
    docker run  -d  --name $MOCK_ECLIPSE --network $NETWORK $MOCK_ECLIPSE

    echo "Step 4 - Build client-bdd....."
    docker build -t $BDD_CLIENT -f tests/bdds/client/Dockerfile  --build-arg service=$SERVICE tests/bdds/client

    echo "Step 5 - Run client-bdd....."
    docker run  --name $BDD_CLIENT --network $NETWORK $BDD_CLIENT $1
    print_stats
}

time run_tests $1
 cleanup