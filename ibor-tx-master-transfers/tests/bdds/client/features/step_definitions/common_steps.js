/*
Any common steps will be placed here for use and 
chai will be used for assertions
*/
'use strict';

const {
    defineSupportCode
} = require('cucumber')
const chai = require('chai');
const chaiExclude = require('chai-exclude');
chai.use(chaiExclude);
var {
    setDefaultTimeout
} = require('cucumber');
var jsontype = require('jsontype');

// setDefaultTimeout(120 * 1000);
var sortJsonArray = require('sort-json-array');
var rateId;
var nextRateId;
var ratePutId;
var rateNextPutId;

defineSupportCode(function (self) {
    let Given = self.Given;
    let When = self.When;
    let Then = self.Then;

    Given(/^a request to (.*)$/, function (endpoint) {
        this.httpService.setDefaults(endpoint);
    });

    When(/^I make a GET request to (.*)$/, async function (path) {
        this.response = await this.httpService.get(path);
        console.log(this.response);
    });

    When(/^I make a POST request to (.*) with the JSON:$/, async function (path, body) {

        var exfilename = ((JSON.parse(body)).name);
        var fs = require("fs");
        var relativeFileName = __dirname + '/../JSONs/' + exfilename;
        var config = fs.readFileSync(relativeFileName, "utf8");

        console.log("COMING FROM TEXT FILE" + config);
        this.response = await this.httpService.postj(path, config);
        var sleep = require('system-sleep');
        sleep(10 * 1000); // sleep for 30 seconds
    });

    Then(/^the response should contain the following fields:$/, function (table) {

        let fields = this.parseTableArray(table)
        let body = JSON.parse(this.response.body);
        chai.expect(body[0]).to.include.all.keys(fields);
    });

    When(/^I make get request to validate the fields  (.*)$/, async function (path) {

        this.response = await this.httpService.get(path);
        // console.log("inside test");
        var obj = this.response.body;
        // var str =data.replace(" ","");
        var json = JSON.parse(obj);
   

        if (json[0].SystemCurrencySecurityId == json[0].LocalCurrencySecurityId) {
            console.log(" SystemToLocalFXRate validated successfully")
        }

        //Commission

        // if(json[0].ExecQuantity > 0 ){
        //     console.log(" Commission validated successfully")

        // }
        
        // console.log("checking the length here", Object.keys(obj).length);
        // for (let a = 0; a <= 10; a++) {
        //     console.log("checking here", json[0].Allocations[a]);
        // }
        // // CommissionCharge
        // if (json[0].ExecQuantity > 0) {
        //     if (json[0].CommissionChargeId >= 0 | json[0].Allocations.CommissionChargeId <= 0) {
        //         console.log("CommissionCharge validated");
        //     }
        // }
        // // ExecQuantity       
        // if (json[0].RouteId >= 0 | json[0].RouteId <= 0) {
        //     if (json[0].ExecQuantity <= json[0].Quantity) {
        //         console.log("ExecQuantity validated");
        //     }
        // }
        // // FeeCharge
        // if (json[0].ExecQuantity > 0) {
        //     if (json[0].FeeChargeId >= 0 | json[0].FeeChargeId <= 0) {
        //         console.log("FeeCharge validated");
        //     }
        // }
    });

    Then(/^I should get a (\d{3}) response$/, function (responseCode) {
        chai.expect(this.response.statusCode).to.equal(parseInt(responseCode));
    });


});








