const { SummaryFormatter } = require('cucumber');
const colors = require('colors/safe');
/**
 * Prints the test results in a nice format
 * This is filling the gap left by the old school pretty printer from Cucumber 2.0
 * The npm script includes running this by default
 * @example
 * `cucumberjs --format "./features/support/pretty_formatter.js"`
 */
class PrettyPrint extends SummaryFormatter {
    /**
     * The class extends the SummaryFormatter
     * https://github.com/cucumber/cucumber-js/blob/master/src/formatter/summary_formatter.js
     * 
     * We then bind the functions we define here to the options object, so all references to `this`
     * will be pointing to the `options` object
     * 
     * When the `eventBroadcaster` emits an event, it will call the functions defined here
     * @param {*} options object passed at runtime by cucumber
     */
    constructor(options) {
        super(options);
        options.eventBroadcaster
            .on('test-case-started', this.logTestCaseName.bind(options))
            .on('test-step-finished', this.logTestStep.bind(options))
            .on('test-case-finished', this.logSeparator.bind(options))
    }

    /**
     * Will log the scenario name and its location in the project 
     */
    logTestCaseName({sourceLocation}) {
        const {gherkinDocument, pickle} = this.eventDataCollector.getTestCaseData(sourceLocation);
        this.log(` Scenario: ${pickle.name}`);
        this.log(colors.gray(` # ${sourceLocation.uri}:${sourceLocation.line}\n`));
    }

    /**
     * Logs each step as it runs, along with its location in code base
     * 
     * The switch statement will determine how each step is formatted depending on the step result
     */
    logTestStep({testCase, index, result}) {
        const {gherkinKeyword, pickleStep, testStep} = this.eventDataCollector.getTestStepData({testCase, index});
        if (pickleStep) {
            switch (result.status) {
                case 'passed':
                    this.log(colors.green(`   √ ${gherkinKeyword}${pickleStep.text}`));
                    break;
                case 'failed':
                    this.log(colors.red(`   × ${gherkinKeyword}${pickleStep.text}\n`));
                    this.log(colors.red(`      ${result.exception.name}\n`));
                    this.log(colors.red(`         ${result.exception.message}`));
                    break;
                case 'undefined':
                    this.log(colors.yellow(`   ? ${gherkinKeyword}${pickleStep.text}`));
                    break;
                case 'skipped':
                    this.log(colors.blue(`   - ${gherkinKeyword}${pickleStep.text}`));
                    break;
                default:
                    this.log(colors.blue(`   ${gherkinKeyword}${pickleStep.text}`));
                    break;
            }
            this.log(colors.gray(` # ${testCase.sourceLocation.uri}:${pickleStep.locations[0].line}\n`));
        }
    }
    
    /**
     * At the end of each case, there will be a newline character
     */
    logSeparator() {
        this.log('\n')
    }
}

module.exports = PrettyPrint