const request = require('request-promise');
const PLATFORM_SESSION_API = '/platform/v1/session';

/**
 * Class that makes it easy to login
 */
class SessionHelper {
    /**
     * Creates session object
     * @param {string} apiRoot root of the api all following requests will make requests against
     * @param {string} username username to authenticate with
     * @param {string} password password to authenticate with
     * @param {string} firmCode firm code
     * @example 
     * ```new SessionHelper('http://localhost:8001/api, 'fLastName', 'password', 'TJX12')```
     */
    constructor(apiRoot, username, password, firmCode) {
        this.apiRoot = apiRoot;
        this.username = username;
        this.password = password;
        this.firmCode = firmCode;
        this.request = request.defaults({
            timeout: 60000,
            json: true
        });
        this.sessionEndPoint = this.apiRoot + PLATFORM_SESSION_API;
    }

    get authHeader() {
        return {
            'user': `${this.firmCode}\\${this.username}`,
            'password': this.password
        }
    }

    /**
     * Makes the POST request to the session API and returns the session token
     */
    login() {
        return this.request.post({
            url: this.sessionEndPoint,
            auth: this.authHeader,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.UserSession.UserSessionToken)
    }
}

module.exports = SessionHelper;