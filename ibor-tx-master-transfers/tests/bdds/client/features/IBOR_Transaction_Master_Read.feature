Feature:Posting data into IBOR Transaction Master,to perform Full validations for allocation and main fields.
 # Calling the api to post IBOR data



# Scenario: Making a GET request to Ibor message and validate Main fields and allocation fields and status for ibor message
#     Given a request to /ibor
#     When I make a GET request to /trans/v1/ 
#     Then I should get a 200 response
#     And the response should contain the following fields:
#     | EventTypeId | UserTimeZoneId | LocalCurrencySecurityId | SystemToLocalFXRate | OrderCreatedDate | OrderQuantity | SecurityId | SecurityTemplateId | SequenceNumber | SettleDate | SourceSystemName | SourceSystemReference | TransactionTimeZoneId | TradeDate | TransactionDateTimeUTC | IsFinalized | BookTypeID | BaseCurrencySecurityId | BaseToLocalFXRate | Commission | CommissionCharge | CommissionChargeId | CustodianCounterpartyAccountId |ExecQuantity | ExecutingBrokerCounterpartyId | FeeCharge | FeeChargeId | Fees | IndexAttributeId | NetAmount | PortfolioId | PrincipalAmount | Quantity | RouteName | RouteId | SettleAmount | SettleCurrencySecurityId | SettlePrice | SettleToBaseFXRate | SettleToLocalFXRate | SettleToSystemFXRate |




Scenario: Validation for all logical fields 
  Given a request to /ibor
  When I make get request to validate the fields  /trans/v1
  Then I should get a 200 response
# ######################################################################################################################################################################################################################


Scenario: Validation for SystemCurrencySecurityId
  Given a request to /v1
  When I make a GET request to /SystemCurrencySecurityId
  Then I should get a 200 response
# ######################################################################################################################################################################################################################

Scenario: Validation for RouteID
  Given a request to /trading/v1
  When I make a GET request to /routes
  Then I should get a 200 response
# ######################################################################################################################################################################################################################


Scenario: Validation for PortfolioID
  Given a request to /v1
  When I make a GET request to /PortfolioId
  Then I should get a 200 response
# ######################################################################################################################################################################################################################


Scenario: Validation for FeeChargeID
  Given a request to /v1
  When I make a GET request to /GetFeeById
  Then I should get a 200 response
# ######################################################################################################################################################################################################################


Scenario: Validation for ExecutingBroker
  Given a request to /v1
  When I make a GET request to /ExecutingBroker
  Then I should get a 200 response
# ######################################################################################################################################################################################################################

Scenario: Validation for CommissionChargeId
  Given a request to /v1
  When I make a GET request to /GetCommissionById
  Then I should get a 200 response
# ######################################################################################################################################################################################################################
Scenario: Validation for EventTypeID
  Given a request to /securitymaster
  When I make a GET request to /v1/Templates/GetEventTypesForTemplate
  Then I should get a 200 response

# ######################################################################################################################################################################################################################

Scenario: Validation for UserTimeZoneID
  Given a request to /v1
  When I make a GET request to /TimeZoneById
  Then I should get a 200 response

# ######################################################################################################################################################################################################################

Scenario: Validation for  Field LocalCurrencySecurityId
 Given a request to /v1
  When I make a GET request to /LocalCurrencySecurityId 
  Then I should get a 200 response

# ######################################################################################################################################################################################################################

Scenario: Validation for Main Field "SecurityID"
 Given a request to /v1
  When I make a GET request to /SecurityId 
  Then I should get a 200 response


# # ######################################################################################################################################################################################################################













   