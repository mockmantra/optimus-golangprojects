def folder='accounting/ibor-tx-master-transfers/build_ibor_transaction_master_component_tests'

//This is the main container build
job("$folder/cucumber-tests/build_cucumber_tests") {
  label('autoscale')
  //This is where we tell the build where to get the source from
  multiscm {
//This first repo is the code repository for a container
    git {
      remote {
        name('origin')
        url('https://nchennoju@stash.ezesoft.net/scm/imsacnt/ibor-tx-master-transfers.git')
        credentials('stash_global')     
      }
//We are building from the master branch here.  Story branches are comming soon.
      branch('ETI-155-execution-of-component-tests-for-transaction-master-in-containers')
    }
  }
   triggers {
    scm('H/1 * * * *')
  }
  wrappers {
            environmentVariables {
        propertiesFile('./tests/bdds/client/properties')
    }
    credentialsBinding {
  
	  usernamePassword('npm_username', 'npm_password', 'npm_user')
    }
    maskPasswords()
    ansiColorBuildWrapper {
      colorMapName('XTerm')
      defaultFg(15)
      defaultBg(1) 
    }
  buildName('${ENV,var="container_name"} ${ENV,var="version"}')
  
}
  steps {
  
  // Pull the latest jenkins-scripts
  //  shell('if [ -d $scripts ]; then rm -rf $scripts; fi && git clone $scripts_repo')
      
  // Build the container
    shell('/bin/sh ./tests/bdds/build.sh')
    
  }


  
}