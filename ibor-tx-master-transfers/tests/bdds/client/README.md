# Welcome to Pickle!

#### Contents:
1. [Preamble](#preamble)
1. [Installation](#installation)
1. [Local Development Setup](#local-development-setup)
1. [Running Tests](#running-tests)

## Preamble

**This project requires Node 7.9+, preferably Node 8!**

Reason being, the project uses Async/Await functions, which are not available in lower version of Node. If you need a different version of Node for work on your local machine, you can employ tools like [nvm](https://github.com/creationix/nvm).

If you are on a machine with Docker installed, the project is also containerized. There are both Powershell and BASH scripts available to run the project with. The Docker file uses Node 8-alpine as its source.

## Installation

Clone the project into your preferred directory

`git clone ssh://git@stash.ezesoft.net:7999/~ashamim/cucumber-template.git`

Navigate into the directory and install the project dependencies

```sh
cd cucumber-template
npm install
```

## Local Development setup

We wanted to allow devs to run tests locally for faster testing and not making Docker a required dependency, while also allowing environment variables to be configured via Docker if running in Jenkins.

Enter: `dotenv`.

`dotenv` is a dev dependency that will load environment variables from a `.env` file.

Setting up a `.env` file is simple, just create a new file in the project root, and fill in the variables as follows

```
API_ROOT=https://base-url/api
USER=username
PASSWORD=password
FIRM=firmcode
```

At run time (if running locally), the `dotenv` package will load the environment variables into the process.

## Running tests

Once all dependencies are taken care of, running tests is simple as running `npm test`

Any arguments that need to be added to cucumberjs can be added as such

`npm test -- --arg1 --arg2`

Anything after the first pair of dashes will be appended to the run script.

## Anything futher

If there are any questions, please don't hesitate to reach out!

Asim Shamim: ashamim@ezesoft.com |
Nate Bonilla: nbonilla@ezesoft.com