def folder='platformapp/mock-eclipse'

//This is the main container build
job("$folder/build_mock-eclipse") {
  label('autoscale')
  multiscm {
    git {
      remote {
        name('origin')
        url('ssh://git@stash.ezesoft.net:7999/ipa/mock-eclipse.git')
        credentials('stash_global')      
      }
      branch('master')
    }
  }

  triggers {
    scm('H/5 * * * *')
  }

  wrappers {
    
    buildInDocker {
      dockerfile('./seed/slave/')      
      volume('/var/run/docker.sock', '/var/run/docker.sock')
      userGroup('2000')
      verbose()
    }
    environmentVariables {
    	propertiesFile('properties')
    }
    credentialsBinding {
      usernamePassword('dtr_user', 'dtr_password', 'dtr-builder')
    }
    maskPasswords()
    ansiColorBuildWrapper {
      colorMapName('XTerm')
      defaultFg(15)
      defaultBg(1) 
    }
    buildName('${ENV,var="container_name"} ${ENV,var="version"}')
  }

  steps {
    // Pull the latest jenkins-scripts
    shell('if [ -d $scripts ]; then rm -rf $scripts; fi && git clone $scripts_repo')
    shell('bash jenkins-scripts/build-container-step.sh')
    
    shell('bash build-scripts/publish-latest-tag.sh')
  }
  publishers {
    stashNotifier()

    //Yes we need all these settings :(
    slackNotifier {
      teamDomain(null)
      authToken(null)
      room('platform-app-team')
      startNotification(false)
      notifyNotBuilt(false)
      notifyAborted(false)
      notifyFailure(true)
      notifySuccess(false)
      notifyUnstable(false)
      notifyBackToNormal(true)
      notifyRepeatedFailure(true)
      includeTestSummary(false)
      includeCustomMessage(false)
      customMessage(null)
      buildServerUrl(null)
      sendAs(null)
      commitInfoChoice('NONE')
    }

  }
}