const settingByNameRoute = "/api/ipa/v1/settings/:productName/:settingName";
const settingsByProductRoute = "/api/ipa/v1/settings/:productName";
const userStore = require("../../common/sessions");
const settingStore = require("./settings-store");
const basicAuth = require("basic-auth");

const allProducts = ['home', 'trading', 'analytics', 'operations', 'accounting', 'compliance', 'risk', 'reporting', 'commissions', 'sandbox', 'system'];

function validateProduct(productName) {
    if (allProducts.indexOf(productName) === -1) {
        return false;
    }
    return true;
};

function findSettingByName(req, res, userInfo) {
    if(req.query.firmId) {
        let firmSetting = settingStore.getFirmLevelSetting(userInfo.FirmAuthToken, req.params.productName, req.params.settingName);
        if(firmSetting) {
            res.status(200).json(firmSetting);
            return;
        }else {
            res.status(404).json("Not Found");
            return false;
        }
    } else {
        let userSetting = settingStore.getUserLevelSetting(userInfo.FirmAuthToken, userInfo.UserName, req.params.productName, req.params.settingName);
        if(userSetting) {
            res.status(200).json(userSetting);
            return;
        }else {
            res.status(404).json("Not Found");
            return false;
        }
    }
}

function findAllSettingsByProduct(req, res, userInfo) {
    if(req.query.firmId) {
        let firmSettings = settingStore.getFirmLevelSettingsByProduct(userInfo.FirmAuthToken, req.params.productName, req.params.settingName);
        if(firmSettings) {
            res.status(200).json({
                items: firmSettings,
                href: `/api/ipa/v1/settings/${req.params.productName}?firmId=${userInfo.FirmAuthToken}`
            });
            return;
        }else {
            res.status(404).json("Not Found");
            return false;
        }
    } else {
        let userSettings = settingStore.getUserLevelSettingsByProduct(userInfo.FirmAuthToken, userInfo.UserName, req.params.productName);
        if(userSettings) {
            res.status(200).json({
                items: userSettings,
                href: `/api/ipa/v1/settings/${req.params.productName}?userId=${userInfo.UserName}`
            });
            return;
        }else {
            res.status(404).json("Not Found");
            return false;
        }
    }
}

function deleteSettingByName(req, res, userInfo) {
    if(req.query.firmId) {
        let firmSettingDeleted = settingStore.deleteFirmLevelSetting(userInfo.FirmAuthToken, req.params.productName, req.params.settingName);
        if(firmSettingDeleted) {
            res.status(200).json();
            return;
        }else {
            res.status(404).json("Not Found");
            return false;
        }
    } else {
        let userSettingDeleted = settingStore.deleteUserLevelSetting(userInfo.FirmAuthToken, userInfo.UserName, req.params.productName, req.params.settingName);
        if(userSettingDeleted) {
            res.status(200).json();
            return;
        }else {
            res.status(404).json("Not Found");
            return false;
        }
    }
}

const settingByNameApi = {
    route: settingByNameRoute,
    get: function (req, res, next) {
        if(!req.headers.authorization) {
            res.status(401).send();
            return false;
        }

        if(!req.query.userId && !req.query.firmId) {
            res.status(400).json("Must provide either a userId or firmId.");
            return false;
        }

        let credentials = basicAuth(req);
        let glx2Token = credentials.name;

        let userSession = userStore.getSessionByGlxToken(glx2Token);

        if(!userSession) {
            res.status(401).send();
            return false;
        }

        let userInfo = userSession.UserSession;

        if(req.query.firmId && req.query.firmId.toUpperCase() !== userInfo.FirmAuthToken) {
            res.status(403).json("Invalid Firm code");
            return false;
        }

        if(!validateProduct(req.params.productName)) {
            res.status(400).json("Invalid product name");
            return false;
        }
        return findSettingByName(req, res, userInfo);
    },

    delete:function(req, res, next){
        if(!req.headers.authorization) {
            res.status(401).send();
            return false;
        }

        if(!req.query.userId && !req.query.firmId) {
            res.status(400).json("Must provide either a userId or firmId.");
            return false;
        }

        let credentials = basicAuth(req);
        let glx2Token = credentials.name;

        let userSession = userStore.getSessionByGlxToken(glx2Token);

        if(!userSession) {
            res.status(401).send();
            return false;
        }

        let userInfo = userSession.UserSession;

        if(req.query.firmId && req.query.firmId.toUpperCase() !== userInfo.FirmAuthToken) {
            res.status(403).json("Invalid Firm code");
            return false;
        }

        if(!validateProduct(req.params.productName)) {
            res.status(400).json("Invalid product name");
            return false;
        }

        return deleteSettingByName(req,res, userInfo);
    }
}

const settingsByProductApi = {
    route: settingsByProductRoute,
    get: function (req, res, next) {
        if(!req.headers.authorization) {
            res.status(401).send();
            return false;
        }

        if(!req.query.userId && !req.query.firmId) {
            res.status(400).json("Must provide either a userId or firmId.");
            return false;
        }

        let credentials = basicAuth(req);
        let glx2Token = credentials.name;

        let userSession = userStore.getSessionByGlxToken(glx2Token);

        if(!userSession) {
            res.status(401).send();
            return false;
        }

        let userInfo = userSession.UserSession;

        if(req.query.firmId && req.query.firmId.toUpperCase() !== userInfo.FirmAuthToken) {
            res.status(403).json("Invalid Firm code");
            return false;
        }

        if(!validateProduct(req.params.productName)) {
            res.status(400).json("Invalid product name");
            return false;
        }
        return findAllSettingsByProduct(req, res, userInfo);
    },
    post: function (req, res, next) {
        if(!req.headers.authorization) {
            res.status(401).send();
            return false;
        }

        let credentials = basicAuth(req);
        let glx2Token = credentials.name;

        let userSession = userStore.getSessionByGlxToken(glx2Token);

        if(!userSession) {
            res.status(401).send();
            return false;
        }

        let userInfo = userSession.UserSession;

        if(!validateProduct(req.params.productName)) {
            res.status(400).json("Invalid product name");
            return false;
        }

        const allowedScopes = ["user", "firm"];

        if(!req.body || !req.body.scope || !req.body.settingName || !req.body.value || !(allowedScopes.indexOf(req.body.scope) >= 0)) {
            res.status(400).json("Invalid request body");
            return false;
        }

        if(req.body.scope === "user") {
            settingStore.addUserLevelSetting(userInfo.FirmAuthToken, userInfo.UserName, req.params.productName, req.body.settingName, req.body.value);
            let userSetting = settingStore.getUserLevelSetting(userInfo.FirmAuthToken, userInfo.UserName, req.params.productName, req.body.settingName);
            if(userSetting) {
                res.status(201).json(userSetting);
                return;
            }else {
                res.status(500).json("internal server error");
                return false;
            }
        } else {
            settingStore.addFirmLevelSetting(userInfo.FirmAuthToken, req.params.productName, req.body.settingName, req.body.value);
            let firmSetting = settingStore.getFirmLevelSetting(userInfo.FirmAuthToken, req.params.productName, req.body.settingName);
            if(firmSetting) {
                res.status(201).json(firmSetting);
                return;
            }else {
                res.status(500).json("internal server error");
                return false;
            }
        }        
    }
}


module.exports = {
    settingByNameApi,
    settingsByProductApi
}