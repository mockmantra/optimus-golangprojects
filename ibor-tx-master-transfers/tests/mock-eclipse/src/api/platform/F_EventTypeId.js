

let response =[
    {
        "EventTypeId": 45,
        "EventTypeName": "BonusRights",
        "EventTypeDescription": "BonusRights",
        "IsTradingEvent": false,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 3,
        "EventTypeName": "Buy",
        "EventTypeDescription": "Buy",
        "IsTradingEvent": true,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 8,
        "EventTypeName": "BuyExerciseSecurityIn",
        "EventTypeDescription": "BuyExerciseSecurityIn",
        "IsTradingEvent": false,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 9,
        "EventTypeName": "CashDividend",
        "EventTypeDescription": "CashDividend",
        "IsTradingEvent": false,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 10,
        "EventTypeName": "CashLiquidation/ReturnofCapital",
        "EventTypeDescription": "CashLiquidation/ReturnofCapital",
        "IsTradingEvent": false,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 6,
        "EventTypeName": "Cover",
        "EventTypeDescription": "Cover",
        "IsTradingEvent": true,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 47,
        "EventTypeName": "ExchangeNonTaxableIn",
        "EventTypeDescription": "ExchangeNonTaxableIn",
        "IsTradingEvent": false,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 18,
        "EventTypeName": "ExchangeNonTaxableOut",
        "EventTypeDescription": "ExchangeNonTaxableOut",
        "IsTradingEvent": false,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 19,
        "EventTypeName": "ExchangeTaxableIn",
        "EventTypeDescription": "ExchangeTaxableIn",
        "IsTradingEvent": false,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 20,
        "EventTypeName": "ExchangeTaxableOut",
        "EventTypeDescription": "ExchangeTaxableOut",
        "IsTradingEvent": false,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 28,
        "EventTypeName": "FreeDeliver",
        "EventTypeDescription": "FreeDeliver",
        "IsTradingEvent": false,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 30,
        "EventTypeName": "FreeReceipt",
        "EventTypeDescription": "FreeReceipt",
        "IsTradingEvent": false,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 31,
        "EventTypeName": "Merger/Acquisition",
        "EventTypeDescription": "Merger/Acquisition",
        "IsTradingEvent": false,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 32,
        "EventTypeName": "NameChange",
        "EventTypeDescription": "NameChange",
        "IsTradingEvent": false,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 38,
        "EventTypeName": "RegistrationSecurity",
        "EventTypeDescription": "RegistrationSecurity",
        "IsTradingEvent": false,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 39,
        "EventTypeName": "ReverseStockSplit",
        "EventTypeDescription": "ReverseStockSplit",
        "IsTradingEvent": false,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 43,
        "EventTypeName": "SecurityLiquidation",
        "EventTypeDescription": "SecurityLiquidation",
        "IsTradingEvent": false,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 14,
        "EventTypeName": "Sell",
        "EventTypeDescription": "Sell",
        "IsTradingEvent": true,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 46,
        "EventTypeName": "SellExerciseSecurityOut",
        "EventTypeDescription": "SellExerciseSecurityOut",
        "IsTradingEvent": false,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 17,
        "EventTypeName": "Short",
        "EventTypeDescription": "Short",
        "IsTradingEvent": true,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 48,
        "EventTypeName": "ShortSellExerciseSecurityOut",
        "EventTypeDescription": "ShortSellExerciseSecurityOut",
        "IsTradingEvent": false,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 50,
        "EventTypeName": "ShortTermCapitalGainsDistribution",
        "EventTypeDescription": "ShortTermCapitalGainsDistribution",
        "IsTradingEvent": false,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 49,
        "EventTypeName": "SODOpeningBalance",
        "EventTypeDescription": "SODOpeningBalance",
        "IsTradingEvent": false,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 51,
        "EventTypeName": "SpinoffNon-TaxableIn",
        "EventTypeDescription": "SpinoffNon-TaxableIn",
        "IsTradingEvent": false,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 52,
        "EventTypeName": "SpinoffNon-TaxableOut",
        "EventTypeDescription": "SpinoffNon-TaxableOut",
        "IsTradingEvent": false,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 53,
        "EventTypeName": "SpinoffTaxableIn",
        "EventTypeDescription": "SpinoffTaxableIn",
        "IsTradingEvent": false,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 54,
        "EventTypeName": "SpinoffTaxableOut",
        "EventTypeDescription": "SpinoffTaxableOut",
        "IsTradingEvent": false,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 56,
        "EventTypeName": "StockDividend",
        "EventTypeDescription": "StockDividend",
        "IsTradingEvent": false,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 55,
        "EventTypeName": "StockSplit",
        "EventTypeDescription": "StockSplit",
        "IsTradingEvent": false,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 58,
        "EventTypeName": "Tender",
        "EventTypeDescription": "Tender",
        "IsTradingEvent": false,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      },
      {
        "EventTypeId": 59,
        "EventTypeName": "Valuation",
        "EventTypeDescription": "Valuation",
        "IsTradingEvent": false,
        "ResourceId": null,
        "Revision": null,
        "AuthorizationTopicName": null
      }
]

const F_EventTypeId_Route = "/api/securitymaster/v1/Templates/GetEventTypesForTemplate";
const userStore = require("../../common/sessions");
const basicAuth = require("basic-auth");    



const F_EventTypeId = {
    route: F_EventTypeId_Route,
    get: function(req, res, next) {
        return res.status(200).json(response);
    },

    head: function(req, res, next) {
        if(req.headers.authorization) {
            var credentials = basicAuth(req);
            if(credentials) {
                let session = userStore.getSessionByGlxToken(credentials.name);
                if(session) {
                    return res.status(200).json({});
                }
            }
            res.status(401).json("not authorized");
        }else {
            res.status(401).json("not authorized");
        }
    }
}

module.exports = {
    F_EventTypeId
}