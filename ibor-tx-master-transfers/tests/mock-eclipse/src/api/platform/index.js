const common = require("./common");
const session = require("./session");
const scheduler = require("./schedule-job");
const dashboardConfig = require("./dashboardconfig");
const eventTypesApi = require("./eventTypesApi");
const ibor_trx_api = require("./Ibor_trx_api");
const F_CommisionChargeId = require("./F_CommissionChargeId");
const F_EventTypeId = require("./F_EventTypeId");

const F_ExecutingBroker = require("./F_ExecutingBroker");
const F_FeeChargeID = require("./F_FeeChargeID");
const F_LocalCurrencySecurityId = require("./F_LocalCurrencySecurityId");
const F_PortfolioId = require("./F_PortfolioId");
const F_RouteId = require("./F_RouteId");
const F_SecurityId = require("./F_SecurityId");
const F_SystemCurrencySecurityId = require("./F_SystemCurrencySecurityId");
const F_UserTimeZone = require("./F_UserTimeZone");

let mergedApiDefs = {};
Object.assign(mergedApiDefs, common, session, 
    dashboardConfig, scheduler,eventTypesApi,ibor_trx_api,
    F_CommisionChargeId,
    F_EventTypeId,
    F_UserTimeZone,
    F_SystemCurrencySecurityId,
    F_SecurityId,
    F_RouteId,
    F_PortfolioId,
    F_LocalCurrencySecurityId,
    F_FeeChargeID,
    F_ExecutingBroker
);

module.exports = mergedApiDefs;