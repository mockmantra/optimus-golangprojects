let response = [
    {
        "Id": 29,
        "EffectiveDate": "2017-01-24T00:00:00",
        "IsActive": true,
        "ParentPortfolioId": null,
        "PortfolioName": "EURO",
        "PortfolioDesc": "",
        "PortfolioNote": null,
        "BaseCurrencyId": 53,
        "EndDate": null,
        "PortfolioTypeCode": "012",
        "SystemStartDate": "0001-01-01T00:00:00",
        "SystemEndDate": null,
        "CreatedBy": "rbyatt              ",
        "ModifiedBy": "IMSDMC-8355",
        "CreatedDate": "2017-01-24T12:00:58.33",
        "ModifiedDate": "2018-09-25T08:06:58.337",
        "OldValue": null,
        "PortfolioCode": null,
        "ReferenceId": null,
        "PortfolioId": 12,
        "SourceCode": "IMS",
        "InceptionDate": "2017-01-25T00:00:00",
        "TerminationDate": null,
        "TotalAssetBasis": 100000000000,
        "OverdraftBasis": 100000000000,
        "PortfolioCash": 100000000000,
        "PortfolioNav": 100000000000,
        "PortfolioIdentifiers": [],
        "PortfolioClosingMethods": [],
        "PortfolioViews": null,
        "AuthorizationTopicName": null,
        "FriendlyName": "EURO(29)",
        "PortfolioNavBasis": null,
        "ResourceId": null,
        "Revision": null
      }
]
      


const F_PortfolioId_Route = "/api/v1/PortfolioId";
const userStore = require("../../common/sessions");
const basicAuth = require("basic-auth");    

const F_PortfolioId = {
    route: F_PortfolioId_Route,
    get: function(req, res, next) {
        return res.status(200).json(response);
    },

    head: function(req, res, next) {
        if(req.headers.authorization) {
            var credentials = basicAuth(req);
            if(credentials) {
                let session = userStore.getSessionByGlxToken(credentials.name);
                if(session) {
                    return res.status(200).json({});
                }
            }
            res.status(401).json("not authorized");
        }else {
            res.status(401).json("not authorized");
        }
    }
}

module.exports = {
    F_PortfolioId
}