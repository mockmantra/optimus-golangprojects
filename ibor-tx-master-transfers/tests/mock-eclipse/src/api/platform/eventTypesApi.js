let response = [
    {
      "EventTypeId": 1,
      "EventTypeName": "Analytics",
      "EventTypeDescription": "Analytics",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 2,
      "EventTypeName": "AssignmentIn/AssignmentonaShortPut(Exercise(AtCost))",
      "EventTypeDescription": "AssignmentIn/AssignmentonaShortPut(Exercise(AtCost))",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 3,
      "EventTypeName": "Buy",
      "EventTypeDescription": "Buy",
      "IsTradingEvent": true,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 4,
      "EventTypeName": "AssignmentOut/AssignmentonaShortCall(Exercise(AtCost))",
      "EventTypeDescription": "AssignmentOut/AssignmentonaShortCall(Exercise(AtCost))",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 5,
      "EventTypeName": "AssignmentOut/AssignmentonaShortCall(Exercise(AtZero))",
      "EventTypeDescription": "AssignmentOut/AssignmentonaShortCall(Exercise(AtZero))",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 6,
      "EventTypeName": "Cover",
      "EventTypeDescription": "Cover",
      "IsTradingEvent": true,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 7,
      "EventTypeName": "AssignmentIn/AssignmentonaShortPut(Exercise(AtZero))",
      "EventTypeDescription": "AssignmentIn/AssignmentonaShortPut(Exercise(AtZero))",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 8,
      "EventTypeName": "BuyExerciseSecurityIn",
      "EventTypeDescription": "BuyExerciseSecurityIn",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 9,
      "EventTypeName": "CashDividend",
      "EventTypeDescription": "CashDividend",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 10,
      "EventTypeName": "CashLiquidation/ReturnofCapital",
      "EventTypeDescription": "CashLiquidation/ReturnofCapital",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 11,
      "EventTypeName": "CashSettlement",
      "EventTypeDescription": "CashSettlement",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 12,
      "EventTypeName": "CloseBuy",
      "EventTypeDescription": "CloseBuy",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 13,
      "EventTypeName": "CloseSell",
      "EventTypeDescription": "CloseSell",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 14,
      "EventTypeName": "Sell",
      "EventTypeDescription": "Sell",
      "IsTradingEvent": true,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 15,
      "EventTypeName": "DividendAccrual",
      "EventTypeDescription": "DividendAccrual",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 16,
      "EventTypeName": "EquitySwapPeriodPayment",
      "EventTypeDescription": "EquitySwapPeriodPayment",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 17,
      "EventTypeName": "Short",
      "EventTypeDescription": "Short",
      "IsTradingEvent": true,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 18,
      "EventTypeName": "ExchangeNonTaxableOut",
      "EventTypeDescription": "ExchangeNonTaxableOut",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 19,
      "EventTypeName": "ExchangeTaxableIn",
      "EventTypeDescription": "ExchangeTaxableIn",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 20,
      "EventTypeName": "ExchangeTaxableOut",
      "EventTypeDescription": "ExchangeTaxableOut",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 21,
      "EventTypeName": "ExerciseSecurityIn/ExerciseLongCall(Exercise(AtCost))",
      "EventTypeDescription": "ExerciseSecurityIn/ExerciseLongCall(Exercise(AtCost))",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 22,
      "EventTypeName": "ExerciseSecurityIn/ExerciseLongCall(Exercise(AtZero))",
      "EventTypeDescription": "ExerciseSecurityIn/ExerciseLongCall(Exercise(AtZero))",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 23,
      "EventTypeName": "ExerciseSecurityOut/ExerciseLongPut(Exercise(AtCost))",
      "EventTypeDescription": "ExerciseSecurityOut/ExerciseLongPut(Exercise(AtCost))",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 24,
      "EventTypeName": "ExerciseSecurityOut/ExerciseLongPut(Exercise(AtZero))",
      "EventTypeDescription": "ExerciseSecurityOut/ExerciseLongPut(Exercise(AtZero))",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 25,
      "EventTypeName": "ExpireLongCall",
      "EventTypeDescription": "ExpireLongCall",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 26,
      "EventTypeName": "ExpireLongPut",
      "EventTypeDescription": "ExpireLongPut",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 27,
      "EventTypeName": "FinancingAccrual",
      "EventTypeDescription": "FinancingAccrual",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 28,
      "EventTypeName": "FreeDeliver",
      "EventTypeDescription": "FreeDeliver",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 29,
      "EventTypeName": "IntheMoney",
      "EventTypeDescription": "IntheMoney",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 30,
      "EventTypeName": "FreeReceipt",
      "EventTypeDescription": "FreeReceipt",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 31,
      "EventTypeName": "Merger/Acquisition",
      "EventTypeDescription": "Merger/Acquisition",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 32,
      "EventTypeName": "NameChange",
      "EventTypeDescription": "NameChange",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 33,
      "EventTypeName": "OpenBuy",
      "EventTypeDescription": "OpenBuy",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 34,
      "EventTypeName": "OpenSell",
      "EventTypeDescription": "OpenSell",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 35,
      "EventTypeName": "ParValueChange",
      "EventTypeDescription": "ParValueChange",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 36,
      "EventTypeName": "PriceChangeandContractSizeChange",
      "EventTypeDescription": "PriceChangeandContractSizeChange",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 37,
      "EventTypeName": "PriceChangeandShareMultiplierChange",
      "EventTypeDescription": "PriceChangeandShareMultiplierChange",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 38,
      "EventTypeName": "RegistrationSecurity",
      "EventTypeDescription": "RegistrationSecurity",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 39,
      "EventTypeName": "ReverseStockSplit",
      "EventTypeDescription": "ReverseStockSplit",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 40,
      "EventTypeName": "SecurityCall/Put",
      "EventTypeDescription": "SecurityCall/Put",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 41,
      "EventTypeName": "SecurityConversion",
      "EventTypeDescription": "SecurityConversion",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 42,
      "EventTypeName": "SecurityDeliveredOut",
      "EventTypeDescription": "SecurityDeliveredOut",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 43,
      "EventTypeName": "SecurityLiquidation",
      "EventTypeDescription": "SecurityLiquidation",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 44,
      "EventTypeName": "SecurityReceivedIn",
      "EventTypeDescription": "SecurityReceivedIn",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 45,
      "EventTypeName": "BonusRights",
      "EventTypeDescription": "BonusRights",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 46,
      "EventTypeName": "SellExerciseSecurityOut",
      "EventTypeDescription": "SellExerciseSecurityOut",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 47,
      "EventTypeName": "ExchangeNonTaxableIn",
      "EventTypeDescription": "ExchangeNonTaxableIn",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 48,
      "EventTypeName": "ShortSellExerciseSecurityOut",
      "EventTypeDescription": "ShortSellExerciseSecurityOut",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 49,
      "EventTypeName": "SODOpeningBalance",
      "EventTypeDescription": "SODOpeningBalance",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 50,
      "EventTypeName": "ShortTermCapitalGainsDistribution",
      "EventTypeDescription": "ShortTermCapitalGainsDistribution",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 51,
      "EventTypeName": "SpinoffNon-TaxableIn",
      "EventTypeDescription": "SpinoffNon-TaxableIn",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 52,
      "EventTypeName": "SpinoffNon-TaxableOut",
      "EventTypeDescription": "SpinoffNon-TaxableOut",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 53,
      "EventTypeName": "SpinoffTaxableIn",
      "EventTypeDescription": "SpinoffTaxableIn",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 54,
      "EventTypeName": "SpinoffTaxableOut",
      "EventTypeDescription": "SpinoffTaxableOut",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 55,
      "EventTypeName": "StockSplit",
      "EventTypeDescription": "StockSplit",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 56,
      "EventTypeName": "StockDividend",
      "EventTypeDescription": "StockDividend",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 57,
      "EventTypeName": "TaxLotAllocation",
      "EventTypeDescription": "TaxLotAllocation",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 58,
      "EventTypeName": "Tender",
      "EventTypeDescription": "Tender",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 59,
      "EventTypeName": "Valuation",
      "EventTypeDescription": "Valuation",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 60,
      "EventTypeName": "VariationMargin",
      "EventTypeDescription": "VariationMargin",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 61,
      "EventTypeName": "FreeDeliver-Forward",
      "EventTypeDescription": "FreeDeliver-Forward",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 62,
      "EventTypeName": "FreeDeliver-NDF",
      "EventTypeDescription": "FreeDeliver-NDF",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 63,
      "EventTypeName": "FreeDeliver-Spot",
      "EventTypeDescription": "FreeDeliver-Spot",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 64,
      "EventTypeName": "FreeReceipt-NDF",
      "EventTypeDescription": "FreeReceipt-NDF",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 65,
      "EventTypeName": "FreeReceipt-Spot",
      "EventTypeDescription": "FreeReceipt-Spot",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 66,
      "EventTypeName": "FXForward-Open",
      "EventTypeDescription": "FXForward-Open",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 67,
      "EventTypeName": "FXForwardSODOpeningBalance",
      "EventTypeDescription": "FXForwardSODOpeningBalance",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 68,
      "EventTypeName": "FXSODOpeningBalance",
      "EventTypeDescription": "FXSODOpeningBalance",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 69,
      "EventTypeName": "FXSpot-Open",
      "EventTypeDescription": "FXSpot-Open",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 70,
      "EventTypeName": "NDF-Open",
      "EventTypeDescription": "NDF-Open",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 71,
      "EventTypeName": "NDFSODOpeningBalance",
      "EventTypeDescription": "NDFSODOpeningBalance",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 72,
      "EventTypeName": "Unwind/Novate Buy",
      "EventTypeDescription": "Unwind or Novate Buy",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 73,
      "EventTypeName": "Unwind/Novate Sell",
      "EventTypeDescription": "Unwind or Novate Sell",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 74,
      "EventTypeName": "Valuation-Forward",
      "EventTypeDescription": "Valuation-Forward",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 75,
      "EventTypeName": "Valuation-FX",
      "EventTypeDescription": "Valuation-FX",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 76,
      "EventTypeName": "Valuation-NDF",
      "EventTypeDescription": "Valuation-NDF",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 77,
      "EventTypeName": "FreeReceipt-Forward",
      "EventTypeDescription": "FreeReceipt-Forward",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 78,
      "EventTypeName": "NDF-Fixing",
      "EventTypeDescription": "NDF-Fixing",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 79,
      "EventTypeName": "Accrued Interest/Payment",
      "EventTypeDescription": "Accrued Interest/Payment",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 80,
      "EventTypeName": "Cashflows",
      "EventTypeDescription": "Cashflows",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 81,
      "EventTypeName": "Coupon Payment",
      "EventTypeDescription": "Coupon Payment",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 82,
      "EventTypeName": "Floating Leg Reset",
      "EventTypeDescription": "Floating Leg Reset",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 83,
      "EventTypeName": "Maturity (Final Redemption)",
      "EventTypeDescription": "Maturity (Final Redemption)",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 84,
      "EventTypeName": "Open Contract",
      "EventTypeDescription": "Open Contract",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 85,
      "EventTypeName": "Unwind/Novate Contract",
      "EventTypeDescription": "Unwind/Novate Contract",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 86,
      "EventTypeName": "BuyOpen",
      "EventTypeDescription": "Buy to Open",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 87,
      "EventTypeName": "Call/Put",
      "EventTypeDescription": "Security Call/Put",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 88,
      "EventTypeName": "Conversion",
      "EventTypeDescription": "Security Conversion",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 89,
      "EventTypeName": "Deposit",
      "EventTypeDescription": "Deposit of Currency",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 90,
      "EventTypeName": "Expire",
      "EventTypeDescription": "Expire",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 91,
      "EventTypeName": "Liquidation",
      "EventTypeDescription": "Security Liquidation",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 92,
      "EventTypeName": "Maturity",
      "EventTypeDescription": "Maturity",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 93,
      "EventTypeName": "NovateBuy",
      "EventTypeDescription": "NovateBuy",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 94,
      "EventTypeName": "NovateSell",
      "EventTypeDescription": "NovateSell",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 95,
      "EventTypeName": "ResetPayment",
      "EventTypeDescription": "EquitySwapPeriodPayment",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 96,
      "EventTypeName": "ReturnOfCapital",
      "EventTypeDescription": "Return Of Capita (also Cash Liquidation)l",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 97,
      "EventTypeName": "SellOpen",
      "EventTypeDescription": "Sell to Open",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 98,
      "EventTypeName": "UnwindBuy",
      "EventTypeDescription": "UnwindBuy",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 99,
      "EventTypeName": "UnwindSell",
      "EventTypeDescription": "UnwindSell",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 100,
      "EventTypeName": "Valuation-FXForward",
      "EventTypeDescription": "Valuation FX Forward Contract",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 101,
      "EventTypeName": "Valuation-FXSpot",
      "EventTypeDescription": "Valuation FX Spot Contract",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    },
    {
      "EventTypeId": 102,
      "EventTypeName": "Withdrawal",
      "EventTypeDescription": "Withdrawal of Currency",
      "IsTradingEvent": false,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    }
  ]
  


const eventTypeRoute = "/api/v1/Templates/GetAllEventTypes";
const userStore = require("../../common/sessions");
const basicAuth = require("basic-auth");    



const eventTypesApi = {
    route: eventTypeRoute,
    get: function(req, res, next) {
        return res.status(200).json(response);
    },

    head: function(req, res, next) {
        if(req.headers.authorization) {
            var credentials = basicAuth(req);
            if(credentials) {
                let session = userStore.getSessionByGlxToken(credentials.name);
                if(session) {
                    return res.status(200).json({});
                }
            }

            res.status(401).json("not authorized");
        }else {
            res.status(401).json("not authorized");
        }
    }
}

module.exports = {
    eventTypesApi
}