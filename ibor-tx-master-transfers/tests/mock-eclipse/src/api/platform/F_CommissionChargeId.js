

let response =[
    {
      "Id": 102,
      "ChargeId_ChargeId": 1,
      "IsActive": true,
      "ChargeRoundingAffectId": 1,
      "ChargeTierBasisId": 2,
      "ChargeTypeId": 2,
      "TypeName": "PerUnit",
      "ChargeRoundingTypeId": 1,
      "Name": "$10E",
      "ChargeDesc": "Description:$10E",
      "Charge_CurrencyId": 0,
      "EffectiveDate": "1900-01-01T00:00:00",
      "EndDate": null,
      "Rate": 10,
      "MaxTotal": 0,
      "MinTotal": 0,
      "Decimal": 2,
      "IsUseFeeTiers": false,
      "UTCCreateDt": "2016-09-15T16:23:23.643",
      "UTCModifyDt": "2018-07-09T15:51:36.903",
      "SystemStartDatetime": "2018-06-06T11:00:57.547",
      "SystemEndDatetime": null,
      "CreatedDate": "0001-01-01T00:00:00",
      "ModifiedDate": "0001-01-01T00:00:00",
      "CreatedBy": "Eze Managed Services",
      "ModifiedBy": "IMSDMC-8048",
      "IsAddToPrincipal": false,
      "RoundPrincipalTo": 0,
      "CommOrFeeType": "C",
      "ScheduleType": "Route",
      "ActionCode": null,
      "IsRoute": false,
      "IsAllRoute": true,
      "IsExchange": false,
      "IsAllExchange": true,
      "IsAssetType": false,
      "IsAllAssetType": true,
      "IsCounterparty": false,
      "IsAllCounterparty": true,
      "IsCurrency": false,
      "IsAllCurrency": true,
      "IsOrderAction": false,
      "IsAllOrderAction": false,
      "IsPortfolio": false,
      "IsAllPortfolio": true,
      "IsAllCountry": false,
      "SourceCode": "IMS",
      "CommissionBudgetTypeId": null,
      "ChargeAssetTypes": [],
      "ChargeRoutes": [],
      "ChargeCurrencies": [],
      "ChargeExchanges": [],
      "ChargeCounterparties": [],
      "ChargeCountries": [],
      "CommissionTiers": [],
      "ChargeOrderActions": [],
      "ChargePortfolios": [],
      "ChildCharges": [],
      "MarketCap": {
        "Value": null,
        "CurrencyId": null,
        "ChargeId": 1
      },
      "ParentCharges": [],
      "OldValue": null,
      "AuthorizationTopicName": null,
      "HashKey": null,
      "ChargeFeeBasisId": null,
      "RateMin": null,
      "RateMax": null,
      "RoundingRuleId": null,
      "IsImplied": false,
      "IsAllCounterpartyCustodian": true,
      "ChargeCounterpartyCustodians": [],
      "ResourceId": null,
      "Revision": null
    }
  ]

const F_CommisionChargeId_Route = "/api/v1/GetCommissionById";
const userStore = require("../../common/sessions");
const basicAuth = require("basic-auth");    



const F_CommisionChargeId = {
    route: F_CommisionChargeId_Route,
    get: function(req, res, next) {
        return res.status(200).json(response);
    },

    head: function(req, res, next) {
        if(req.headers.authorization) {
            var credentials = basicAuth(req);
            if(credentials) {
                let session = userStore.getSessionByGlxToken(credentials.name);
                if(session) {
                    return res.status(200).json({});
                }
            }

            res.status(401).json("not authorized");
        }else {
            res.status(401).json("not authorized");
        }
    }
}

module.exports = {
    F_CommisionChargeId
}