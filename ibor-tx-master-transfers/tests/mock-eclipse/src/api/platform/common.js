

// let response =[
//     {
//     "EventTypeId": 100,
//     "EventTypeName": "Valuation-FXForward",
//     "EventTypeDescription": "Valuation FX Forward Contract",
//     "IsTradingEvent": false,
//     "ResourceId": null,
//     "Revision": null,
//     "AuthorizationTopicName": null
//   }
// ]
// testing the scenario

const alertApi = {
    route: "/api/platform/v1/alert",
    get: (req, res, next) => {
        res.status(200).json('working');
    }
}

const health = {
    route: "/api/platform/v1/health",
    get: (req, res, next) => {
        res.status(200).json("Ok");
    }
}



module.exports = {
    alertApi,
    health
}