let response =[
    {
      "CreatedBy": null,
      "CreatedDate": "0001-01-01T00:00:00",
      "ModifiedBy": null,
      "ModifiedDate": "0001-01-01T00:00:00",
      "OldValue": null,
      "Id": 2,
      "SecurityId": 2,
      "Source": "RealTick",
      "Symbol": "ALL",
      "DisplayName": "ALL",
      "LongName": "Lek",
      "IsTradable": true,
      "AssetClassName": "Cash",
      "AssetClassId": 1,
      "TemplateName": "Cash",
      "TemplateId": 8,
      "AssetTypes": [],
      "EffectiveDate": null,
      "AsOnDate": null,
      "Identifiers": [
        {
          "AttributeNameId": 15,
          "AttributeName": "CurrencyCode",
          "AttributeType": null,
          "AttributeValue": "ALL",
          "AttributeDataType": null,
          "Id": "CurrencyCode",
          "ResourceId": null,
          "Revision": null,
          "AuthorizationTopicName": null
        },
        {
          "AttributeNameId": 1,
          "AttributeName": "Symbol",
          "AttributeType": null,
          "AttributeValue": "ALL",
          "AttributeDataType": null,
          "Id": "Symbol",
          "ResourceId": null,
          "Revision": null,
          "AuthorizationTopicName": null
        },
        {
          "AttributeNameId": 14,
          "AttributeName": "BloombergID",
          "AttributeType": null,
          "AttributeValue": "ALL",
          "AttributeDataType": null,
          "Id": "BloombergID",
          "ResourceId": null,
          "Revision": null,
          "AuthorizationTopicName": null
        }
      ],
      "IdentifiersWithType": [
        {
          "AttributeNameId": 15,
          "AttributeName": "CurrencyCode",
          "AttributeType": null,
          "AttributeValue": "ALL",
          "AttributeDataType": null,
          "Id": "CurrencyCode",
          "ResourceId": null,
          "Revision": null,
          "AuthorizationTopicName": null
        },
        {
          "AttributeNameId": 1,
          "AttributeName": "Symbol",
          "AttributeType": null,
          "AttributeValue": "ALL",
          "AttributeDataType": null,
          "Id": "Symbol",
          "ResourceId": null,
          "Revision": null,
          "AuthorizationTopicName": null
        },
        {
          "AttributeNameId": 14,
          "AttributeName": "BloombergID",
          "AttributeType": null,
          "AttributeValue": "ALL",
          "AttributeDataType": null,
          "Id": "BloombergID",
          "ResourceId": null,
          "Revision": null,
          "AuthorizationTopicName": null
        }
      ],
      "TemplateAttributes": [
        {
          "AttributeName": "Country",
          "AttributeType": "General",
          "AttributeValue": "3",
          "AttributeDataType": "Varchar",
          "Id": "Country",
          "ResourceId": null,
          "Revision": null,
          "AuthorizationTopicName": null
        },
        {
          "AttributeName": "Currency",
          "AttributeType": "General",
          "AttributeValue": "",
          "AttributeDataType": "Varchar",
          "Id": "Currency",
          "ResourceId": null,
          "Revision": null,
          "AuthorizationTopicName": null
        },
        {
          "AttributeName": "RiskCountry",
          "AttributeType": "General",
          "AttributeValue": "",
          "AttributeDataType": "Varchar",
          "Id": "RiskCountry",
          "ResourceId": null,
          "Revision": null,
          "AuthorizationTopicName": null
        },
        {
          "AttributeName": "RiskCurrency",
          "AttributeType": "General",
          "AttributeValue": "",
          "AttributeDataType": "Varchar",
          "Id": "RiskCurrency",
          "ResourceId": null,
          "Revision": null,
          "AuthorizationTopicName": null
        },
        {
          "AttributeName": "MaturityDate",
          "AttributeType": "Accrual",
          "AttributeValue": "",
          "AttributeDataType": "Date",
          "Id": "MaturityDate",
          "ResourceId": null,
          "Revision": null,
          "AuthorizationTopicName": null
        },
        {
          "AttributeName": "Symbol",
          "AttributeType": "Identifiers",
          "AttributeValue": "",
          "AttributeDataType": "Varchar",
          "Id": "Symbol",
          "ResourceId": null,
          "Revision": null,
          "AuthorizationTopicName": null
        }
      ],
      "Currencies": [
        {
          "AttributeNameId": 4,
          "AttributeName": "LocalCurrency",
          "AttributeType": null,
          "AttributeValue": "2",
          "AttributeDataType": null,
          "Id": "LocalCurrency",
          "ResourceId": null,
          "Revision": null,
          "AuthorizationTopicName": null
        }
      ],
      "CurrenciesWithType": [
        {
          "AttributeNameId": 4,
          "AttributeName": "LocalCurrency",
          "AttributeType": null,
          "AttributeValue": "2",
          "AttributeDataType": null,
          "Id": "LocalCurrency",
          "ResourceId": null,
          "Revision": null,
          "AuthorizationTopicName": null
        }
      ],
      "TemplateLegs": [],
      "UnderlyingSymbol": null,
      "SecurityClassifications": [],
      "ClientClassficationsAssociations": null,
      "ResourceId": null,
      "Revision": null,
      "AuthorizationTopicName": null
    }
  ]


const F_LocalCurrencySecurityId_Route = "/api/v1/LocalCurrencySecurityId";
const userStore = require("../../common/sessions");
const basicAuth = require("basic-auth");    



const F_LocalCurrencySecurityId = {
    route: F_LocalCurrencySecurityId_Route,
    get: function(req, res, next) {
        return res.status(200).json(response);
    },

    head: function(req, res, next) {
        if(req.headers.authorization) {
            var credentials = basicAuth(req);
            if(credentials) {
                let session = userStore.getSessionByGlxToken(credentials.name);
                if(session) {
                    return res.status(200).json({});
                }
            }
            res.status(401).json("not authorized");
        }else {
            res.status(401).json("not authorized");
        }
    }
}

module.exports = {
    F_LocalCurrencySecurityId
}