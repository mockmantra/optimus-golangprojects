let response=[
{
    "Allocations": [
      {
        "BaseCurrencySecurityId": 155,
        "BaseToLocalFXRate": 1,
        "BookTypeId": 1,
        "CustodianCounterpartyAccountId": 10,
        "FeeCharges": [],
        "PortfolioId": 21,
        "PositionTags": [
          {
            "IndexAttributeId": 1,
            "IndexAttributeValueId": 3
          }
        ],
        "Quantity": 400
      },
      {
        "BaseCurrencySecurityId": 155,
        "BaseToLocalFXRate": 1,
        "BookTypeId": 2,
        "CommissionCharges": [],
        "CustodianCounterpartyAccountId": 10,
        "FeeCharges": [],
        "PortfolioId": 21,
        "PositionTags": [],
        "Quantity": 400
      },
      {
        "BaseCurrencySecurityId": 155,
        "BaseToLocalFXRate": 1,
        "BookTypeId": 3,
        "PortfolioId": 21,
        "PositionTags": [
          {
            "IndexAttributeId": 1,
            "IndexAttributeValueId": 3
          }
        ],
        "Quantity": 400
      },
      {
        "BaseCurrencySecurityId": 155,
        "BaseToLocalFXRate": 1,
        "BookTypeId": 1,
        "Commission": 10,
        "CommissionCharges": [
          {
            "CommissionCharge": 10,
            "CommissionChargeId": 22
          },
          {
            "CommissionCharge": 0,
            "CommissionChargeId": 24
          }
        ],
        "CustodianCounterpartyAccountId": 10,
        "ExecQuantity": 100,
        "FeeCharges": [
          {
            "FeeCharge": 0.6555,
            "FeeChargeId": 27
          },
          {
            "FeeCharge": 0,
            "FeeChargeId": 28
          },
          {
            "FeeCharge": 0.7867,
            "FeeChargeId": 29
          },
          {
            "FeeCharge": 0.9834,
            "FeeChargeId": 30
          },
          {
            "FeeCharge": 19.668,
            "FeeChargeId": 31
          },
          {
            "FeeCharge": 25,
            "FeeChargeId": 32
          },
          {
            "FeeCharge": 15,
            "FeeChargeId": 33
          },
          {
            "FeeCharge": 98.34,
            "FeeChargeId": 34
          }
        ],
        "Fees": 160.4336,
        "NetAmount": 19838.4336,
        "PortfolioId": 21,
        "PositionTags": [
          {
            "IndexAttributeId": 1,
            "IndexAttributeValueId": 3
          }
        ],
        "PrincipalAmount": 19668,
        "Quantity": 200,
        "RouteId": 1621,
        "RouteName": "MANUAL",
        "SettleAmount": 19838.4336,
        "SettleCurrencySecurityId": 155,
        "SettlePrice": 196.68,
        "SettleToBaseFXRate": 1,
        "SettleToLocalFXRate": 1,
        "SettleToSystemFXRate": 1
      },
      {
        "BaseCurrencySecurityId": 155,
        "BaseToLocalFXRate": 1,
        "BookTypeId": 2,
        "Commission": 10,
        "CommissionCharges": [
          {
            "CommissionCharge": 10,
            "CommissionChargeId": 22
          },
          {
            "CommissionCharge": 0,
            "CommissionChargeId": 24
          }
        ],
        "CustodianCounterpartyAccountId": 10,
        "ExecQuantity": 100,
        "FeeCharges": [
          {
            "FeeCharge": 0.6555,
            "FeeChargeId": 27
          },
          {
            "FeeCharge": 0,
            "FeeChargeId": 28
          },
          {
            "FeeCharge": 0.7867,
            "FeeChargeId": 29
          },
          {
            "FeeCharge": 0.9834,
            "FeeChargeId": 30
          },
          {
            "FeeCharge": 19.668,
            "FeeChargeId": 31
          },
          {
            "FeeCharge": 25,
            "FeeChargeId": 32
          },
          {
            "FeeCharge": 15,
            "FeeChargeId": 33
          },
          {
            "FeeCharge": 98.34,
            "FeeChargeId": 34
          }
        ],
        "Fees": 160.4336,
        "NetAmount": 19838.4336,
        "PortfolioId": 21,
        "PositionTags": [],
        "PrincipalAmount": 19668,
        "Quantity": 200,
        "RouteId": 1621,
        "RouteName": "MANUAL",
        "SettleAmount": 19838.4336,
        "SettleCurrencySecurityId": 155,
        "SettlePrice": 196.68,
        "SettleToBaseFXRate": 1,
        "SettleToLocalFXRate": 1,
        "SettleToSystemFXRate": 1
      },
      {
        "BaseCurrencySecurityId": 155,
        "BaseToLocalFXRate": 1,
        "BookTypeId": 3,
        "Commission": 10,
        "CommissionCharges": [
          {
            "CommissionCharge": 10,
            "CommissionChargeId": 22
          },
          {
            "CommissionCharge": 0,
            "CommissionChargeId": 24
          }
        ],
        "ExecQuantity": 100,
        "FeeCharges": [
          {
            "FeeCharge": 0.6555,
            "FeeChargeId": 27
          },
          {
            "FeeCharge": 0,
            "FeeChargeId": 28
          },
          {
            "FeeCharge": 0.7867,
            "FeeChargeId": 29
          },
          {
            "FeeCharge": 0.9834,
            "FeeChargeId": 30
          },
          {
            "FeeCharge": 19.668,
            "FeeChargeId": 31
          },
          {
            "FeeCharge": 25,
            "FeeChargeId": 32
          },
          {
            "FeeCharge": 15,
            "FeeChargeId": 33
          },
          {
            "FeeCharge": 98.34,
            "FeeChargeId": 34
          }
        ],
        "Fees": 160.4336,
        "NetAmount": 19838.4336,
        "PortfolioId": 21,
        "PositionTags": [
          {
            "IndexAttributeId": 1,
            "IndexAttributeValueId": 3
          }
        ],
        "PrincipalAmount": 19668,
        "Quantity": 200,
        "RouteId": 1621,
        "SettleAmount": 19838.4336,
        "SettleCurrencySecurityId": 155,
        "SettlePrice": 196.68,
        "SettleToBaseFXRate": 1,
        "SettleToLocalFXRate": 1,
        "SettleToSystemFXRate": 1
      }
    ],
    "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
    "EventTypeId": 14,
    "IsFinalized": true,
    "LocalCurrencySecurityId": 155,
    "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
    "OrderQuantity": 600,
    "SecurityId": 375,
    "SecurityTemplateId": 1,
    "SequenceNumber": 63,
    "SettleDate": "2018-09-10T13:25:43Z",
    "SourceSystemName": "Trading",
    "SourceSystemReference": "4803",
    "SystemCurrencySecurityId": 155,
    "SystemToLocalFXRate": 1,
    "TradeDate": "2018-09-08T13:25:43Z",
    "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
    "TransactionTimeZoneId": 1,
    "UserTimeZoneId": 12
  }

]

  const ibor_trx_api_Route = "/api/ibor/trans/v1";
const userStore = require("../../common/sessions");
const basicAuth = require("basic-auth");    



const ibor_trx_api = {
    route: ibor_trx_api_Route,
    get: function(req, res, next) {
        return res.status(200).json(response);
    },

    head: function(req, res, next) {
        if(req.headers.authorization) {
            var credentials = basicAuth(req);
            if(credentials) {
                let session = userStore.getSessionByGlxToken(credentials.name);
                if(session) {
                    return res.status(200).json({});
                }
            }

            res.status(401).json("not authorized");
        }else {
            res.status(401).json("not authorized");
        }
    }
}


module.exports = {
    ibor_trx_api
}