let response = [
    {
        "Offset": 0,
        "Size": 0,
        "Items": [
          {
            "Href": "/api/trading/v1/routes/4445",
            "RouteId": 4445,
            "DeskId": 78,
            "DeskName": "MANUAL",
            "DestinationId": 6,
            "OrderId": 4803,
            "Amount": 1000,
            "SecurityId": 1062,
            "Side": "Sell",
            "PriceType": 1,
            "Price": 0,
            "StopPrice": 0,
            "TimeInForce": 2,
            "WorkflowName": "TradeWorkflow",
            "TradeDate": "2017-09-29T00:00:00",
            "TenantFirmId": 0,
            "TotalFee": 3933.6037,
            "TotalCommission": 45272.5,
            "TotalFillAmount": 1000,
            "TotalCost": 630000,
            "Status": 8,
            "StatusS": "Manual",
            "RouteStatus": 0,
            "CurrencyId": 154,
            "RouteType": 1,
            "RouteAction": 0,
            "CTSOrderId": "",
            "OriginalAmount": 1000,
            "AveragePriceOverride": 0,
            "CustomFields": {},
            "ModifiedDate": "2017-10-17T14:35:25.027",
            "CommissionId": {
              "href": "/api/trading/v1/charge/0"
            },
            "FeeId": {
              "href": "/api/trading/v1/charge/0"
            },
            "Fees": [
              {
                "Href": "/api/trading/v1/routecharge/37382",
                "RouteChargeId": 37382,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 27,
                "Rate": 0.454,
                "TotalCharge": 21.6912,
                "OriginalRate": 0.454,
                "OriginalTotalCharge": 21.6912,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37383",
                "RouteChargeId": 37383,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 28,
                "Rate": 0,
                "TotalCharge": 0,
                "OriginalRate": 0,
                "OriginalTotalCharge": 0,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37384",
                "RouteChargeId": 37384,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 29,
                "Rate": 0.4,
                "TotalCharge": 25.2,
                "OriginalRate": 0.4,
                "OriginalTotalCharge": 25.2,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37385",
                "RouteChargeId": 37385,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 30,
                "Rate": 0.5,
                "TotalCharge": 31.5,
                "OriginalRate": 0.5,
                "OriginalTotalCharge": 31.5,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37386",
                "RouteChargeId": 37386,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 31,
                "Rate": 10,
                "TotalCharge": 630,
                "OriginalRate": 10,
                "OriginalTotalCharge": 630,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37387",
                "RouteChargeId": 37387,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 32,
                "Rate": 25,
                "TotalCharge": 25,
                "OriginalRate": 25,
                "OriginalTotalCharge": 25,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37388",
                "RouteChargeId": 37388,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 33,
                "Rate": 15,
                "TotalCharge": 15,
                "OriginalRate": 15,
                "OriginalTotalCharge": 15,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37389",
                "RouteChargeId": 37389,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 34,
                "Rate": 50,
                "TotalCharge": 3150,
                "OriginalRate": 50,
                "OriginalTotalCharge": 3150,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37390",
                "RouteChargeId": 37390,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 35,
                "Rate": 0.5,
                "TotalCharge": 19.0449,
                "OriginalRate": 0.5,
                "OriginalTotalCharge": 19.0449,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37391",
                "RouteChargeId": 37391,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 37,
                "Rate": 1,
                "TotalCharge": 1,
                "OriginalRate": 1,
                "OriginalTotalCharge": 1,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37392",
                "RouteChargeId": 37392,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 39,
                "Rate": 20,
                "TotalCharge": 15.1676,
                "OriginalRate": 20,
                "OriginalTotalCharge": 15.1676,
                "OverrideType": "None"
              }
            ],
            "Commissions": [
              {
                "Href": "/api/trading/v1/routecharge/37393",
                "RouteChargeId": 37393,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 1,
                "Rate": 10,
                "TotalCharge": 10000,
                "OriginalRate": 10,
                "OriginalTotalCharge": 10000,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37394",
                "RouteChargeId": 37394,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 2,
                "Rate": 15,
                "TotalCharge": 15000,
                "OriginalRate": 15,
                "OriginalTotalCharge": 15000,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37395",
                "RouteChargeId": 37395,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 3,
                "Rate": 1,
                "TotalCharge": 1000,
                "OriginalRate": 1,
                "OriginalTotalCharge": 1000,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37396",
                "RouteChargeId": 37396,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 4,
                "Rate": 2.5,
                "TotalCharge": 2500,
                "OriginalRate": 2.5,
                "OriginalTotalCharge": 2500,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37397",
                "RouteChargeId": 37397,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 5,
                "Rate": 2,
                "TotalCharge": 2000,
                "OriginalRate": 2,
                "OriginalTotalCharge": 2000,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37398",
                "RouteChargeId": 37398,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 6,
                "Rate": 3,
                "TotalCharge": 3000,
                "OriginalRate": 3,
                "OriginalTotalCharge": 3000,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37399",
                "RouteChargeId": 37399,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 7,
                "Rate": 4,
                "TotalCharge": 4000,
                "OriginalRate": 4,
                "OriginalTotalCharge": 4000,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37400",
                "RouteChargeId": 37400,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 8,
                "Rate": 6,
                "TotalCharge": 6000,
                "OriginalRate": 6,
                "OriginalTotalCharge": 6000,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37401",
                "RouteChargeId": 37401,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 9,
                "Rate": 0.01,
                "TotalCharge": 10,
                "OriginalRate": 0.01,
                "OriginalTotalCharge": 10,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37402",
                "RouteChargeId": 37402,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 10,
                "Rate": 0.02,
                "TotalCharge": 20,
                "OriginalRate": 0.02,
                "OriginalTotalCharge": 20,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37403",
                "RouteChargeId": 37403,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 11,
                "Rate": 0.02,
                "TotalCharge": 20,
                "OriginalRate": 0.02,
                "OriginalTotalCharge": 20,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37404",
                "RouteChargeId": 37404,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 12,
                "Rate": 0.03,
                "TotalCharge": 30,
                "OriginalRate": 0.03,
                "OriginalTotalCharge": 30,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37405",
                "RouteChargeId": 37405,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 13,
                "Rate": 0.03,
                "TotalCharge": 30,
                "OriginalRate": 0.03,
                "OriginalTotalCharge": 30,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37406",
                "RouteChargeId": 37406,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 14,
                "Rate": 0.04,
                "TotalCharge": 40,
                "OriginalRate": 0.04,
                "OriginalTotalCharge": 40,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37407",
                "RouteChargeId": 37407,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 15,
                "Rate": 0.05,
                "TotalCharge": 50,
                "OriginalRate": 0.05,
                "OriginalTotalCharge": 50,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37408",
                "RouteChargeId": 37408,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 16,
                "Rate": 0.05,
                "TotalCharge": 50,
                "OriginalRate": 0.05,
                "OriginalTotalCharge": 50,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37409",
                "RouteChargeId": 37409,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 17,
                "Rate": 0.06,
                "TotalCharge": 60,
                "OriginalRate": 0.06,
                "OriginalTotalCharge": 60,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37410",
                "RouteChargeId": 37410,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 18,
                "Rate": 0.06,
                "TotalCharge": 60,
                "OriginalRate": 0.06,
                "OriginalTotalCharge": 60,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37411",
                "RouteChargeId": 37411,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 19,
                "Rate": 0.07,
                "TotalCharge": 70,
                "OriginalRate": 0.07,
                "OriginalTotalCharge": 70,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37412",
                "RouteChargeId": 37412,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 20,
                "Rate": 0.08,
                "TotalCharge": 80,
                "OriginalRate": 0.08,
                "OriginalTotalCharge": 80,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37413",
                "RouteChargeId": 37413,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 21,
                "Rate": 0.09,
                "TotalCharge": 90,
                "OriginalRate": 0.09,
                "OriginalTotalCharge": 90,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37414",
                "RouteChargeId": 37414,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 22,
                "Rate": 0.1,
                "TotalCharge": 100,
                "OriginalRate": 0.1,
                "OriginalTotalCharge": 100,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37415",
                "RouteChargeId": 37415,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 23,
                "Rate": 0.0625,
                "TotalCharge": 62.5,
                "OriginalRate": 0.0625,
                "OriginalTotalCharge": 62.5,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37416",
                "RouteChargeId": 37416,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 24,
                "Rate": 0,
                "TotalCharge": 0,
                "OriginalRate": 0,
                "OriginalTotalCharge": 0,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37417",
                "RouteChargeId": 37417,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 25,
                "Rate": 1,
                "TotalCharge": 1000,
                "OriginalRate": 1,
                "OriginalTotalCharge": 1000,
                "OverrideType": "None"
              },
              {
                "Href": "/api/trading/v1/routecharge/37418",
                "RouteChargeId": 37418,
                "OrderId": 4803,
                "RouteId": 4445,
                "ChargeId_ChargeId": 26,
                "Rate": 0,
                "TotalCharge": 0,
                "OriginalRate": 0,
                "OriginalTotalCharge": 0,
                "OverrideType": "None"
              }
            ],
            "Executions": [
              {
                "href": "/api/trading/v1/executions/4335"
              }
            ],
            "Tags": []
          }
        ]
      }
]
      


const F_RouteId_Route = "/api/trading/v1/routes";
const userStore = require("../../common/sessions");
const basicAuth = require("basic-auth");    

const F_RouteId = {
    route: F_RouteId_Route,
    get: function(req, res, next) {
        return res.status(200).json(response);
    },

    head: function(req, res, next) {
        if(req.headers.authorization) {
            var credentials = basicAuth(req);
            if(credentials) {
                let session = userStore.getSessionByGlxToken(credentials.name);
                if(session) {
                    return res.status(200).json({});
                }
            }
            res.status(401).json("not authorized");
        }else {
            res.status(401).json("not authorized");
        }
    }
}

module.exports = {
    F_RouteId
}