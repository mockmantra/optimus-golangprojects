let response =[
    {
      "Id": 1,
      "Name": "Dateline Standard Time",
      "StandardName": "Dateline Standard Time",
      "DaylightName": "Dateline Daylight Time",
      "StandardCode": null,
      "DaylightCode": null,
      "SupportsDst": false,
      "BaseUtcOffset": -720,
      "DstUtcOffset": -720,
      "USDstRule": false,
      "EUDstRule": false,
      "RowVersion": "AAAAAAqijGY=",
      "UtcCreateDt": "2016-09-15T16:22:15.123",
      "UtcModifyDt": "2018-08-03T14:18:27.483",
      "CreatedBy": null,
      "CreatedDate": "0001-01-01T00:00:00",
      "ModifiedBy": null,
      "ModifiedDate": "0001-01-01T00:00:00",
      "OldValue": null,
      "AuthorizationTopicName": null,
      "ResourceId": null,
      "Revision": null
    }
  ]
      


const F_UserTimeZone_Route = "/api/v1/TimeZoneById";
const userStore = require("../../common/sessions");
const basicAuth = require("basic-auth");    



const F_UserTimeZone = {
    route: F_UserTimeZone_Route,
    get: function(req, res, next) {
        return res.status(200).json(response);
    },

    head: function(req, res, next) {
        if(req.headers.authorization) {
            var credentials = basicAuth(req);
            if(credentials) {
                let session = userStore.getSessionByGlxToken(credentials.name);
                if(session) {
                    return res.status(200).json({});
                }
            }
            res.status(401).json("not authorized");
        }else {
            res.status(401).json("not authorized");
        }
    }
}

module.exports = {
    F_UserTimeZone
}