﻿var argsParser = require("minimist");
var express = require("express");
var colors = require("colors");
var http = require("http");
const bodyParser = require("body-parser");

var args = argsParser(process.argv.slice(2));

var app = express();

app.set("port", args.port || 3000);
var postBodyLimit = '100mb';
app.use(bodyParser.json())

const apiDefs = require("./api");

apiDefs(app);


http.createServer(app).listen(app.get("port"), function () {
    console.log("Starting Ims WebApp Server on port ".green + colors.yellow(app.get("port")));
});