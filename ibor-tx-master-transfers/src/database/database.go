package database

import (
	"fmt"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql" //using mysql package in gorm.open()
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	bitemporalLayer "stash.ezesoft.net/imsacnt/go-bitemporality"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/interfaces"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/logger"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/models"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/vault"
)

// var lock sync.Mutex
var gormOpen = gorm.Open

const tableName = "Ibor_ValidationErrors"
const transactionTableName = "Ibor_Transfers_Transactions"
const validFromUTC = "ValidFromUTC"
const validToUTC = "ValidToUTC"

// TransactionType contains the type of transaction.
const TransactionType = "TransfersTransaction"

// DataAccessor is used to connect to db and execute db operation
type DataAccessor struct {
	db    *gorm.DB
	firm  string
	Log   interfaces.ILogger
	vault interfaces.IVault
}

func init() {
	if !appconfig.IsDevMode {
		da := DataAccessorInstance()
		mapOfFirmDbConnections := da.vault.GetDbCredentials()
		if len(mapOfFirmDbConnections) < 1 {
			logger.Log.Error("Error reading tenant db credentials from vault")
			panic("Error loading db credentials from vault")
		}
	}
}

// DataAccessorInstance - Returns an instance of DataAccessor struct
func DataAccessorInstance() *DataAccessor {
	vaultInstance := vault.Instance()
	return &DataAccessor{
		Log:   logger.Log,
		vault: vaultInstance,
	}
}

// InsertTransaction will insert the records into the transaction table in the database.
func (da *DataAccessor) InsertTransaction(payload *models.Transactiontable, transactionDateTimeUTC time.Time) error {
	var bitemporalStruct bitemporalLayer.BtObj
	bitemporalStruct.TableName = transactionTableName
	uniqueKey := make(map[string]interface{})
	uniqueKey["SourceSystemName"] = payload.SourceSystemName
	uniqueKey["SourceSystemReference"] = payload.SourceSystemReference
	bitemporalStruct.UniqueKey = uniqueKey
	bitemporalStruct.Database = da.db
	var table models.Transactiontable
	bitemporalStruct.TableModel = &table
	if err := bitemporalStruct.Insert(false, payload, transactionDateTimeUTC, validFromUTC, validToUTC); err != nil {
		return err
	}
	return nil

}

// InsertValidationErrors will insert the records into the transaction table in the database.
func (da *DataAccessor) InsertValidationErrors(recordid int, validationErrors []string) error {
	da.Log.Infof("Validating Transaction type: %s", TransactionType)
	sqlStr := "INSERT INTO " + tableName + "(TransactionRecordID,ErrorMessage, TransactionType) VALUES "
	for _, elem := range validationErrors {
		sqlStr += fmt.Sprintf("(%v, %q, %#v),", recordid, elem, TransactionType)
	}
	sqlStr = sqlStr[:len(sqlStr)-1]
	if err := da.db.Exec(sqlStr).Error; err != nil {
		return err
	}
	return nil
}

func (da *DataAccessor) getTenantCredentials(firmToken string) (*models.AuroraConnection, error) {
	con, ok := da.vault.GetDbCredentials()[firmToken]
	if !ok {
		return nil, fmt.Errorf("No db credentials found for firm: -%s-", firmToken)
	}
	da.Log.Debugf("Vault credentials loaded for tenant %s", firmToken)
	return con, nil
}

func (da *DataAccessor) getSchemaName() string {
	return fmt.Sprintf(appconfig.DbSchemaTemplate, da.firm)
}

// Close closes the database connection of a data accessor
func (da *DataAccessor) Close() (err error) {

	if da.db != nil {
		if err = da.db.Close(); err != nil {
			err = errors.Wrapf(err, "Error while closing connection with database %v", da.getSchemaName())
			da.Log.Error(err)
		} else {
			err = nil
		}
	}
	*da = DataAccessor{}
	return err
}

// Connect establishes connection to firm's database
// TODO: check if lock required on this!
func (da *DataAccessor) Connect(firmAuthToken string) (err error) {

	err = retry(appconfig.DbConnectAttempts, time.Second, func() error {

		connectionString, err := da.getConnectionString(firmAuthToken)
		if err != nil {
			return err
		}

		db, err := gormOpen("mysql", connectionString)
		if err != nil {
			return err
		}

		da.db = db
		return nil
	})

	return

}

func (da *DataAccessor) getConnectionString(firmToken string) (connectionString string, err error) {
	if appconfig.IsDevMode {
		connectionString = os.Getenv("Connection_String")
		if len(connectionString) == 0 {
			return "", errors.New("Connection string is not set")
		}
		return
	}

	connectionInfo, er := da.vault.GetTenantCredentials(firmToken)

	if er != nil {
		return "", er
	}
	da.firm = firmToken
	connectionString = buildConnectionString(connectionInfo, da.getSchemaName())
	return
}

func buildConnectionString(connectionInfo *models.AuroraConnection, dbName string) string {
	return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?parseTime=True&loc=UTC",
		connectionInfo.User,
		connectionInfo.Password,
		connectionInfo.Host,
		connectionInfo.Port,
		dbName)
}
func retry(attempts int, sleep time.Duration, f func() error) (err error) {
	for {
		if err = f(); err == nil {
			return nil
		}
		attempts--
		if attempts <= 0 {
			break
		}
		time.Sleep(sleep)
	}
	return errors.Wrap(err, "Failed to open database connection. Exiting...") // do not log connectionstring
}
