package httputil

import (
	"context"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"time"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/logger"
	"stash.ezesoft.net/ipc/ezeutils"
)

// UserSessionToken set in main.go
var UserSessionToken = ""

// Ctx is currentContext
var Ctx context.Context

// NewRequest method
func NewRequest(httpMethod string, url string, body io.ReadSeeker) ([]byte, error) {

	retryClient := ezeutils.NewRetryableClient()
	retryClient.Logger = logger.Log
	retryClient.RetryWaitMin = time.Second * 1
	retryClient.RetryWaitMax = time.Second * 1
	retryClient.RetryMax = 3

	apiURL := fmt.Sprintf("%s/api/%s", appconfig.BaseURI, url)

	req, _ := ezeutils.NewRequest(httpMethod, apiURL, body)

	if Ctx != nil {
		req.Request = req.Request.WithContext(Ctx)
		ezeutils.SetRequestHeadersFromContext(Ctx, req.Request, logger.Log)
	}
	if UserSessionToken == "" {
		uErr := fmt.Errorf("Authentication Failed, No UserSessionToken")
		logger.Log.Errorf(uErr.Error())
		return nil, uErr
	}

	req.Header.Add("authorization", getAuthorizationToken(UserSessionToken))
	resp, err := retryClient.Do(req)

	if resp != nil && resp.StatusCode != 200 {
		errorMsg := fmt.Sprintf("%s, error:%s", apiURL, resp.Status)
		logger.Log.Debug(errorMsg)
		return nil, errors.New(errorMsg)
	} else if err != nil {
		return nil, err
	}
	bodyBytes, _ := ioutil.ReadAll(resp.Body)
	return bodyBytes, nil
}

func getAuthorizationToken(UserSessionToken string) string {
	str := UserSessionToken + ":"
	return "Basic " + base64.StdEncoding.EncodeToString([]byte(str))
}
