package httputil

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src"
)

func TestHttpUtil_NewRequestWhenServiceReturnsError(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusServiceUnavailable)
	}))

	UserSessionToken = "123"

	defer ts.Close()
	appconfig.BaseURI = ts.URL

	_, err := NewRequest("get", "test", nil)
	assert.EqualError(t, err, fmt.Sprintf("get %s/api/test giving up after 4 attempts", ts.URL))

	UserSessionToken = ""
}

func TestHttpUtil_NewRequestWhenNoUserSessionToken(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	}))
	defer ts.Close()
	appconfig.BaseURI = ts.URL

	resp, err := NewRequest("get", "test", nil)
	assert.Nil(t, resp)
	assert.EqualError(t, err, fmt.Sprintf("Authentication Failed, No UserSessionToken"))
}

func TestHttpUtil_NewRequestWhenServiceDoesntExists(t *testing.T) {
	appconfig.BaseURI = "http://www.test123445.com"

	UserSessionToken = "123"

	response, err := NewRequest("get", "test", nil)
	assert.Nil(t, response)

	fmt.Println(err.Error())

	assert.Equal(t, "get http://www.test123445.com/api/test giving up after 4 attempts", err.Error())

	UserSessionToken = ""
}
func TestHttpUtil_NewRequestWhenServiceReturnsValidJson(t *testing.T) {
	str := "{\"test\":123}"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Error in writing to response")
		}
	}))

	UserSessionToken = "123"

	defer ts.Close()
	appconfig.BaseURI = ts.URL
	response, err := NewRequest("get", "test", nil)
	assert.NotNil(t, response)
	assert.Equal(t, string(response), str)
	assert.Nil(t, err)

	UserSessionToken = ""
}
func TestHttpUtil_NewRequestWithContext(t *testing.T) {
	str := "{\"test\":123}"
	Ctx = context.Background()
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Error in writing to response")
		}
	}))

	UserSessionToken = "123"

	defer ts.Close()
	appconfig.BaseURI = ts.URL
	response, err := NewRequest("get", "test", nil)
	assert.NotNil(t, response)
	assert.Equal(t, string(response), str)
	assert.Nil(t, err)

	UserSessionToken = ""
}
