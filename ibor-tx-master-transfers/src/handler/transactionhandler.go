package handler

import (
	"context"
	"encoding/json"
	"runtime/debug"

	"github.com/aws/aws-sdk-go/service/kinesis"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/appcontext"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/database"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/httputil"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/interfaces"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/models"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/persisttransaction"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/servicesession"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/validator"
)

// HandleTransaction process the kinesis record
func HandleTransaction(record kinesis.Record, k interfaces.IKinesisUtil, log interfaces.ILogger) {
	defer func() {
		if p := recover(); p != nil {
			log.Errorf("Error occured while processing transaction: %s", string(record.Data))
			log.Errorf("Recover on HandleTransaction %s: %s", p, string(debug.Stack()))
		}
	}()
	var streamMessage models.KinStreamMessage
	err := json.Unmarshal(record.Data, &streamMessage)
	if err == nil {
		emptyCtx := context.Background()
		ctx := appcontext.GetFromKinesisMessage(emptyCtx, &streamMessage)
		// setting the updated context for the log
		log = appcontext.GetLoggerWithCtx(ctx)
		// setting the context for the http request
		httputil.Ctx = ctx
		var transaction models.Transaction
		err := json.Unmarshal([]byte(*streamMessage.Message), &transaction)
		if err == nil {
			// set the user session token variable in httputil.go
			tokenErr := servicesession.SetUserSessionToken(streamMessage.FirmAuthToken)
			if tokenErr == nil {
				// persist validation msg and errors to db
				var dbData models.Transactiontable
				da := database.DataAccessorInstance()
				// code to insert the message to db
				errDbConnect := da.Connect(streamMessage.FirmAuthToken)
				if errDbConnect == nil {
					recordID, errMsg := persisttransaction.PersistTransactionsMaster(
						&dbData, log, transaction.EventTypeID, transaction.SourceSystemName,
						*transaction.Allocations[0].SecurityID, *transaction.Allocations[0].Quantity, da,
						string(*streamMessage.Message), streamMessage.UserID,
						streamMessage.UserName, streamMessage.UserSessionToken,
						streamMessage.ActivityID, string(transaction.BusinessDateTimeUTC),
						string(transaction.SourceSystemReference), string(transaction.TransactionDateTimeUTC))

					if errMsg == nil {
						// validate transaction message
						errorStrings := validator.Validate(transaction, log)
						// If there are any validation errors, then log to the database.
						if len(errorStrings) > 0 {
							var errStringArray []string
							for _, errString := range errorStrings {
								errStringArray = append(errStringArray, errString.Message...)
							}
							errInsertion := da.InsertValidationErrors(recordID, errStringArray)
							if errInsertion != nil {
								log.Errorf("Error in saving rejections for recordId:%d to db, error:%v", recordID, errInsertion)
							} else {
								log.Infof("Success saving rejections for recordId:%d to db", recordID)
								errorStrings = append(errorStrings, validator.ErrorMessage{Field: "Rejected Error Strings", Message: []string{""}})
								e, _ := json.Marshal(errorStrings)
								log.Infof(string(e))
							}
						} else {
							k.HandleMessage(record.Data, appconfig.AwsDownStreamName, log)
						}
					} else {
						log.Errorf("Error in saving transaction to db, error:%v", errMsg)
					}
					errDbClose := da.Close()
					if errDbClose != nil {
						log.Errorf("Error in closing the db connection, error:%v", errDbClose)
					}
				} else {
					log.Errorf("Connection to db failed, error:%v", errDbConnect)
				}
			} else {
				log.Errorf("Failed to get user session token for FirmId: %d, UserName: %s, ActivityId: %s, Error: %v", streamMessage.FirmID, streamMessage.UserName, streamMessage.ActivityID, tokenErr)
			}
		} else {
			log.Errorf("Error in Unmarshal streamMessage.Message: %v", err)
		}
		// Resetting the context for the log
		log = appcontext.GetLoggerWithCtx(emptyCtx)
	} else {
		log.Errorf("Error in Unmarshal streamMessage: %v", err)
	}
}
