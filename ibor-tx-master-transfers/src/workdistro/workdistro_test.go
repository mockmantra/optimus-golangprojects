package workdistro

// import (
// 	"context"
// 	"errors"
// 	"testing"
// 	"time"

// 	"github.com/stretchr/testify/mock"
// 	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/mocks"
// 	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/models"
// 	"stash.ezesoft.net/ipc/work-distribution/workdistributionclient"
// )

// type allMocks struct {
// 	logger mocks.Logger
// }

// func TestListenWDAssignments_WithSubscribeToWorkMethodError(t *testing.T) {
// 	assignments := make(chan models.Assignment)
// 	defer close(assignments)

// 	done := make(chan bool)
// 	defer close(done)
// 	var allmocks allMocks
// 	go func() {
// 		// 	assignments <- models.Assignment{SequenceNumber: "123"}
// 		time.Sleep(time.Duration(time.Second * 1))
// 		done <- true
// 	}()

// 	allmocks.logger.On("Info", mock.Anything).Return()
// 	allmocks.logger.On("Error", mock.Anything).Return()
// 	err := errors.New("Error in subscribing to work")
// 	monkey.Patch(workdistributionclient.SubscribeToWork, func(ctx context.Context, workerGroupName, workType string, metadata *map[string]string) (chan workdistributionclient.WorkerAssignments, chan workdistributionclient.ClientStatus, chan bool, error) {
// 		return nil, nil, nil, err
// 	})
// 	ListenWDAssignments(assignments, &allmocks.logger)
// 	<-done
// 	allmocks.logger.AssertCalled(t, "Info", "subscribing to work distribution service...")
// 	allmocks.logger.AssertCalled(t, "Error", "error when start subscribe to work distribution service: Error in subscribing to work")
// 	allmocks.logger.AssertCalled(t, "Info", "subscribed to work...")
// }
// func TestListenWDAssignments_WithPanic(t *testing.T) {
// 	assignments := make(chan models.Assignment)
// 	defer close(assignments)

// 	done := make(chan bool)
// 	defer close(done)
// 	var allmocks allMocks
// 	go func() {
// 		// 	assignments <- models.Assignment{SequenceNumber: "123"}
// 		time.Sleep(time.Duration(time.Second * 1))
// 		done <- true
// 	}()

// 	go func() {

// 	}()

// 	allmocks.logger.On("Info", mock.Anything).Return()
// 	allmocks.logger.On("Errorf", mock.Anything, mock.Anything, mock.Anything).Return()
// 	monkey.Patch(workdistributionclient.SubscribeToWork, func(ctx context.Context, workerGroupName, workType string, metadata *map[string]string) (chan workdistributionclient.WorkerAssignments, chan workdistributionclient.ClientStatus, chan bool, error) {
// 		panic("Error")
// 	})
// 	ListenWDAssignments(assignments, &allmocks.logger)
// 	<-done
// 	allmocks.logger.AssertCalled(t, "Info", "subscribing to work distribution service...")
// 	allmocks.logger.AssertExpectations(t)
// }
// func TestListenWDAssignments_WithQuitChannel(t *testing.T) {
// 	assignments := make(chan models.Assignment)
// 	defer close(assignments)

// 	done := make(chan bool)
// 	defer close(done)

// 	quitChan := make(chan bool)
// 	var allmocks allMocks
// 	go func() {
// 		// 	assignments <- models.Assignment{SequenceNumber: "123"}
// 		quitChan <- true
// 		time.Sleep(time.Duration(time.Second * 1))
// 		done <- true
// 	}()

// 	allmocks.logger.On("Info", mock.Anything).Return()
// 	monkey.Patch(workdistributionclient.SubscribeToWork, func(ctx context.Context, workerGroupName, workType string, metadata *map[string]string) (chan workdistributionclient.WorkerAssignments, chan workdistributionclient.ClientStatus, chan bool, error) {
// 		return nil, nil, quitChan, nil
// 	})
// 	ListenWDAssignments(assignments, &allmocks.logger)
// 	<-done
// 	allmocks.logger.AssertCalled(t, "Info", "subscribing to work distribution service...")
// 	allmocks.logger.AssertCalled(t, "Info", "stop looking for assignments - WD client quit")
// 	allmocks.logger.AssertCalled(t, "Info", "subscribed to work...")
// }
// func TestListenWDAssignments_WithDistributionStatusChanChannelMessage(t *testing.T) {
// 	assignments := make(chan models.Assignment)
// 	defer close(assignments)

// 	done := make(chan bool)
// 	defer close(done)

// 	distributionStatusChan := make(chan workdistributionclient.ClientStatus)
// 	var allmocks allMocks
// 	go func() {
// 		// 	assignments <- models.Assignment{SequenceNumber: "123"}
// 		distributionStatusChan <- workdistributionclient.ClientStatus{Message: "test message"}
// 		time.Sleep(time.Duration(time.Second * 1))
// 		done <- true
// 	}()
// 	allmocks.logger.On("Info", mock.Anything)
// 	allmocks.logger.On("Infof", mock.Anything, mock.Anything).Return()
// 	monkey.Patch(workdistributionclient.SubscribeToWork, func(ctx context.Context, workerGroupName, workType string, metadata *map[string]string) (chan workdistributionclient.WorkerAssignments, chan workdistributionclient.ClientStatus, chan bool, error) {
// 		return nil, distributionStatusChan, nil, nil
// 	})
// 	ListenWDAssignments(assignments, &allmocks.logger)
// 	<-done
// 	allmocks.logger.AssertCalled(t, "Info", "subscribing to work distribution service...")
// 	allmocks.logger.AssertCalled(t, "Info", "subscribed to work...")
// 	allmocks.logger.AssertCalled(t, "Infof", "WD subscription message: %s", "test message")
// }
// func TestListenWDAssignments_WithDistributionStatusChanChannelError(t *testing.T) {
// 	assignments := make(chan models.Assignment)
// 	defer close(assignments)

// 	done := make(chan bool)
// 	defer close(done)

// 	distributionStatusChan := make(chan workdistributionclient.ClientStatus)
// 	var allmocks allMocks
// 	err := errors.New("Error")
// 	go func() {
// 		// 	assignments <- models.Assignment{SequenceNumber: "123"}
// 		distributionStatusChan <- workdistributionclient.ClientStatus{Error: err}
// 		time.Sleep(time.Duration(time.Second * 1))
// 		done <- true
// 	}()
// 	allmocks.logger.On("Info", mock.Anything)
// 	allmocks.logger.On("Errorf", mock.Anything, mock.Anything)
// 	monkey.Patch(workdistributionclient.SubscribeToWork, func(ctx context.Context, workerGroupName, workType string, metadata *map[string]string) (chan workdistributionclient.WorkerAssignments, chan workdistributionclient.ClientStatus, chan bool, error) {
// 		return nil, distributionStatusChan, nil, nil
// 	})
// 	ListenWDAssignments(assignments, &allmocks.logger)
// 	<-done
// 	allmocks.logger.AssertCalled(t, "Info", "subscribing to work distribution service...")
// 	allmocks.logger.AssertCalled(t, "Info", "subscribed to work...")
// 	allmocks.logger.AssertCalled(t, "Errorf", "WD subscription error, error:%v", err)
// }
// func TestListenWDAssignments_WithDistributionStatusChanChannelWarning(t *testing.T) {
// 	assignments := make(chan models.Assignment)
// 	defer close(assignments)

// 	done := make(chan bool)
// 	defer close(done)

// 	distributionStatusChan := make(chan workdistributionclient.ClientStatus)
// 	var allmocks allMocks
// 	warn := "Warning"
// 	go func() {
// 		// 	assignments <- models.Assignment{SequenceNumber: "123"}
// 		distributionStatusChan <- workdistributionclient.ClientStatus{Warning: warn}
// 		time.Sleep(time.Duration(time.Second * 1))
// 		done <- true
// 	}()
// 	allmocks.logger.On("Info", mock.Anything)
// 	allmocks.logger.On("Warnf", mock.Anything, mock.Anything)
// 	monkey.Patch(workdistributionclient.SubscribeToWork, func(ctx context.Context, workerGroupName, workType string, metadata *map[string]string) (chan workdistributionclient.WorkerAssignments, chan workdistributionclient.ClientStatus, chan bool, error) {
// 		return nil, distributionStatusChan, nil, nil
// 	})
// 	ListenWDAssignments(assignments, &allmocks.logger)
// 	<-done
// 	allmocks.logger.AssertCalled(t, "Info", "subscribing to work distribution service...")
// 	allmocks.logger.AssertCalled(t, "Info", "subscribed to work...")
// 	allmocks.logger.AssertCalled(t, "Warnf", "WD subscription warning: %s", warn)
// }
// func TestListenWDAssignments_DistibutionAssignmentChanWithoutWorkItems(t *testing.T) {
// 	assignments := make(chan models.Assignment)
// 	defer close(assignments)

// 	done := make(chan bool)
// 	defer close(done)

// 	workerAssignmentsChan := make(chan workdistributionclient.WorkerAssignments)
// 	var allmocks allMocks
// 	go func() {
// 		// 	assignments <- models.Assignment{SequenceNumber: "123"}
// 		workerAssignmentsChan <- workdistributionclient.WorkerAssignments{WorkItems: workdistributionclient.WorkItems{}}
// 		time.Sleep(time.Duration(time.Second * 1))
// 		done <- true
// 	}()
// 	allmocks.logger.On("Info", mock.Anything)
// 	allmocks.logger.On("Errorf", mock.Anything, mock.Anything)
// 	monkey.Patch(workdistributionclient.SubscribeToWork, func(ctx context.Context, workerGroupName, workType string, metadata *map[string]string) (chan workdistributionclient.WorkerAssignments, chan workdistributionclient.ClientStatus, chan bool, error) {
// 		return workerAssignmentsChan, nil, nil, nil
// 	})
// 	ListenWDAssignments(assignments, &allmocks.logger)
// 	<-done
// 	allmocks.logger.AssertCalled(t, "Info", "subscribing to work distribution service...")
// 	allmocks.logger.AssertCalled(t, "Info", "subscribed to work...")
// }
// func TestListenWDAssignments_DistibutionAssignmentChanWithMetaDataNilInWorkItemsAndWithAssignmentListener(t *testing.T) {
// 	assignments := make(chan models.Assignment)
// 	defer close(assignments)

// 	done := make(chan bool)
// 	defer close(done)

// 	workerAssignmentsChan := make(chan workdistributionclient.WorkerAssignments)
// 	var allmocks allMocks
// 	go func() {
// 		// 	assignments <- models.Assignment{SequenceNumber: "123"}
// 		workerAssignmentsChan <- workdistributionclient.WorkerAssignments{WorkItems: workdistributionclient.WorkItems{
// 			workdistributionclient.WorkItem{Metadata: nil}}}
// 		time.Sleep(time.Duration(time.Second * 1))
// 		done <- true
// 	}()
// 	go func() {
// 		<-assignments
// 	}()
// 	allmocks.logger.On("Info", mock.Anything)
// 	allmocks.logger.On("Warn", mock.Anything)
// 	monkey.Patch(workdistributionclient.SubscribeToWork, func(ctx context.Context, workerGroupName, workType string, metadata *map[string]string) (chan workdistributionclient.WorkerAssignments, chan workdistributionclient.ClientStatus, chan bool, error) {
// 		return workerAssignmentsChan, nil, nil, nil
// 	})
// 	ListenWDAssignments(assignments, &allmocks.logger)
// 	<-done

// 	allmocks.logger.AssertCalled(t, "Info", "subscribing to work distribution service...")
// 	allmocks.logger.AssertCalled(t, "Info", "subscribed to work...")
// 	allmocks.logger.AssertCalled(t, "Warn", "invalid work item nil metadata")
// }
// func TestListenWDAssignments_DistibutionAssignmentChanWithMetaDataNilInWorkItemsAndWithNoAssignmentListener(t *testing.T) {
// 	assignments := make(chan models.Assignment)
// 	defer close(assignments)

// 	done := make(chan bool)
// 	defer close(done)

// 	workerAssignmentsChan := make(chan workdistributionclient.WorkerAssignments)
// 	var allmocks allMocks
// 	go func() {
// 		// 	assignments <- models.Assignment{SequenceNumber: "123"}
// 		workerAssignmentsChan <- workdistributionclient.WorkerAssignments{WorkItems: workdistributionclient.WorkItems{
// 			workdistributionclient.WorkItem{Metadata: nil}}}
// 		time.Sleep(time.Duration(time.Second * 1))
// 		done <- true
// 	}()
// 	allmocks.logger.On("Info", mock.Anything)
// 	allmocks.logger.On("Warn", mock.Anything)
// 	allmocks.logger.On("Debug", mock.Anything)
// 	monkey.Patch(workdistributionclient.SubscribeToWork, func(ctx context.Context, workerGroupName, workType string, metadata *map[string]string) (chan workdistributionclient.WorkerAssignments, chan workdistributionclient.ClientStatus, chan bool, error) {
// 		return workerAssignmentsChan, nil, nil, nil
// 	})
// 	ListenWDAssignments(assignments, &allmocks.logger)
// 	<-done
// 	allmocks.logger.AssertCalled(t, "Info", "subscribing to work distribution service...")
// 	allmocks.logger.AssertCalled(t, "Info", "subscribed to work...")
// 	allmocks.logger.AssertCalled(t, "Warn", "invalid work item nil metadata")
// 	allmocks.logger.AssertCalled(t, "Debug", "assignments channel blocked")
// }

// func TestListenWDAssignments_DistibutionAssignmentChanWithInvalidMetaDataInWorkItems(t *testing.T) {
// 	assignments := make(chan models.Assignment)
// 	defer close(assignments)

// 	done := make(chan bool)
// 	defer close(done)

// 	workerAssignmentsChan := make(chan workdistributionclient.WorkerAssignments)
// 	var allmocks allMocks
// 	go func() {
// 		// 	assignments <- models.Assignment{SequenceNumber: "123"}
// 		workerAssignmentsChan <- workdistributionclient.WorkerAssignments{WorkItems: workdistributionclient.WorkItems{
// 			workdistributionclient.WorkItem{Metadata: "test"}}}
// 		time.Sleep(time.Duration(time.Second * 1))
// 		done <- true
// 	}()
// 	go func() {
// 		<-assignments
// 	}()
// 	allmocks.logger.On("Info", mock.Anything)
// 	allmocks.logger.On("Warnf", mock.Anything, mock.Anything)
// 	monkey.Patch(workdistributionclient.SubscribeToWork, func(ctx context.Context, workerGroupName, workType string, metadata *map[string]string) (chan workdistributionclient.WorkerAssignments, chan workdistributionclient.ClientStatus, chan bool, error) {
// 		return workerAssignmentsChan, nil, nil, nil
// 	})
// 	ListenWDAssignments(assignments, &allmocks.logger)
// 	<-done

// 	allmocks.logger.AssertCalled(t, "Info", "subscribing to work distribution service...")
// 	allmocks.logger.AssertCalled(t, "Info", "subscribed to work...")
// 	allmocks.logger.AssertCalled(t, "Warnf", "invalid md interface conversion: %T", "test")
// 	allmocks.logger.AssertCalled(t, "Warnf", "invalid interface conversion: %T", nil)
// }
// func TestListenWDAssignments_DistibutionAssignmentChanWithValidMetaDataInWorkItems(t *testing.T) {
// 	assignments := make(chan models.Assignment)
// 	defer close(assignments)

// 	done := make(chan bool)
// 	defer close(done)

// 	workerAssignmentsChan := make(chan workdistributionclient.WorkerAssignments)
// 	var allmocks allMocks
// 	assignmentMap := make(map[string]interface{})
// 	go func() {
// 		// 	assignments <- models.Assignment{SequenceNumber: "123"}
// 		assignmentMap["SequenceNumber"] = "123"
// 		workerAssignmentsChan <- workdistributionclient.WorkerAssignments{WorkItems: workdistributionclient.WorkItems{
// 			workdistributionclient.WorkItem{Metadata: assignmentMap}}}
// 		time.Sleep(time.Duration(time.Second * 1))
// 		done <- true
// 	}()
// 	go func() {
// 		assignment := <-assignments
// 		if assignment.SequenceNumber != "123" {
// 			t.Error("Invalid Assignment Value")
// 		}
// 	}()
// 	allmocks.logger.On("Info", mock.Anything)
// 	monkey.Patch(workdistributionclient.SubscribeToWork, func(ctx context.Context, workerGroupName, workType string, metadata *map[string]string) (chan workdistributionclient.WorkerAssignments, chan workdistributionclient.ClientStatus, chan bool, error) {
// 		return workerAssignmentsChan, nil, nil, nil
// 	})
// 	ListenWDAssignments(assignments, &allmocks.logger)
// 	<-done

// 	allmocks.logger.AssertCalled(t, "Info", "subscribing to work distribution service...")
// 	allmocks.logger.AssertCalled(t, "Info", "subscribed to work...")
// }
