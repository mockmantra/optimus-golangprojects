package logger

import (
	"stash.ezesoft.net/ipc/ezelogger"
	"stash.ezesoft.net/ipc/ezeutils"
)

var (
	// Log instance
	Log *ezelogger.EzeLog
)

func init() {
	Log = ezelogger.GetLogger("ibor-tx-master-transfers").Configure(ezelogger.CfgReplaceNewlines, ezelogger.CfgReplaceDoubleQuotes, ezelogger.CfgMessageLinesLimit(100))
	ezeutils.SetLogger(Log)
}
