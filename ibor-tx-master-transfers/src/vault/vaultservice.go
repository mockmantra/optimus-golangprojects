package vault

import (
	"io"

	"github.com/hashicorp/vault/api"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/interfaces"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/logger"
)

//Instance ...
func Instance() *Data {
	return &Data{
		VHandler: &API{},
		Log:      logger.Log,
	}

}

// API ...
type API struct {
}

// ParseSecret ...
func (v *API) ParseSecret(r io.Reader) (*api.Secret, error) {
	return api.ParseSecret(r)
}

// DefaultConfig ...
func (v *API) DefaultConfig() interfaces.IVaultConfig {
	return api.DefaultConfig()
}

//NewClient ...
func (v *API) NewClient(c interfaces.IVaultConfig) (interfaces.IVaultClient, error) {
	return api.NewClient(c.(*api.Config))
}
