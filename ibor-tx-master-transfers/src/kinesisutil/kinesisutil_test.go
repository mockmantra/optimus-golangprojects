package kinesisutil

import (
	"errors"
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/kinesis"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	appconfig "stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/mocks"
)

type allMocks struct {
	client      mocks.KinesisAPI
	logger      mocks.Logger
	credentials mocks.AwsCreds
	session     mocks.AwsSession
	kinesis     mocks.Kinesis
}

func TestKinesisUtil_createClient_InLocal(t *testing.T) {
	resetDevMode(true)

	// Arrange
	var allMocks allMocks

	k := &KinesisUtil{
		credentials: &allMocks.credentials,
		session:     &allMocks.session,
		kinesis:     &allMocks.kinesis,
	}

	if k.client != nil {
		t.Error("Kinesis Client should be nil intially")
	}

	allMocks.logger.On("Info", "Kinesis Session has been created successfully.")
	allMocks.logger.On("Debug", "Running locally with localstack")

	var (
		credentials   = &credentials.Credentials{}
		session       = &session.Session{}
		kinesisClient = &kinesis.Kinesis{}
	)
	allMocks.credentials.On("NewStaticCredentials", "foo", "var", "").Return(
		credentials,
	).Once()
	allMocks.session.On("New", mock.Anything).Return(
		session,
	).Once()
	allMocks.kinesis.On("New", session).Return(
		kinesisClient,
	).Once()

	// Act
	k.createClient(&allMocks.logger)

	// Assert
	if k.client == nil {
		t.Error("Kinesis Client should be updated")
	}
	assert.Equal(t, k.client, kinesisClient)
}
func TestKinesisUtil_createClient_InCastle(t *testing.T) {
	// Arrange
	var allMocks allMocks

	k := &KinesisUtil{
		credentials: &allMocks.credentials,
		session:     &allMocks.session,
		kinesis:     &allMocks.kinesis,
	}

	if k.client != nil {
		t.Error("Kinesis Client should be nil intially")
	}

	allMocks.logger.On("Debug", "Running locally with localstack")

	appconfig.AwsAccessKey = "test_key"
	appconfig.AwsSecretKey = "test_secret"
	defer resetDevMode(appconfig.IsDevMode)
	appconfig.IsDevMode = false
	var (
		credentials   = &credentials.Credentials{}
		session       = &session.Session{}
		kinesisClient = &kinesis.Kinesis{}
	)
	allMocks.credentials.On("NewStaticCredentials", "test_key", "test_secret", "").Return(
		credentials,
	).Once()
	allMocks.session.On("New", mock.Anything).Return(
		session,
	).Once()
	allMocks.kinesis.On("New", session).Return(
		kinesisClient,
	).Once()
	allMocks.logger.On("Info", "Kinesis Session has been created successfully.")

	// Act
	k.createClient(&allMocks.logger)
	if k.client == nil {
		t.Error("Kinesis Client should be updated")
	}

	// Assert
	assert.Equal(t, k.client, kinesisClient)
}
func TestInsertRecordError(t *testing.T) {
	mockClient := new(mocks.KinesisAPI)
	k := KinesisUtil{
		client: mockClient,
	}

	mockClient.On("PutRecord", mock.Anything).Return(nil, errors.New("Cannot insert record"))

	_, err := k.InsertRecord("streamName", "key", []byte("value"))
	assert.NotNil(t, err)
}

func TestInsertRecordSuccess(t *testing.T) {
	mockClient := new(mocks.KinesisAPI)
	k := KinesisUtil{
		client: mockClient,
	}

	mockClient.On("PutRecord", mock.Anything).Return(&kinesis.PutRecordOutput{}, nil)

	_, err := k.InsertRecord("streamName", "key", []byte("value"))
	assert.Nil(t, err)
}

func TestHandleMessageInsertError(t *testing.T) {
	mockLogger := new(mocks.Logger)
	mockClient := new(mocks.KinesisAPI)
	k := KinesisUtil{
		client: mockClient,
	}

	mockClient.On("PutRecord", mock.Anything).Return(nil, errors.New("Cannot insert record")).Times(2)
	mockClient.On("PutRecord", mock.Anything).Return(&kinesis.PutRecordOutput{}, nil).Times(1)
	mockLogger.On("Infof", mock.Anything, mock.Anything).Return()
	mockLogger.On("Errorf", mock.Anything, mock.Anything, mock.Anything).Return()

	k.HandleMessage([]byte("data"), "streamName", mockLogger)
	mockLogger.AssertCalled(t, "Infof", "%sStarted inserting the record into kinesis.", "")
	mockLogger.AssertCalled(t, "Infof", "%sStarted inserting the record into kinesis.", "Retry:1 ")
	mockLogger.AssertCalled(t, "Infof", "%sStarted inserting the record into kinesis.", "Retry:2 ")
	mockLogger.AssertCalled(t, "Errorf", "%sError in kenisis PutRecord(), error: %v", "", errors.New("Cannot insert record"))
	mockLogger.AssertCalled(t, "Errorf", "%sError in kenisis PutRecord(), error: %v", "Retry:1 ", errors.New("Cannot insert record"))
	mockLogger.AssertCalled(t, "Infof", "%sSuccessfully inserted record into kinesis.", "Retry:2 ")
}

func TestHandleMessageSuccess(t *testing.T) {
	mockLogger := new(mocks.Logger)
	mockClient := new(mocks.KinesisAPI)
	k := KinesisUtil{
		client: mockClient,
	}

	mockClient.On("PutRecord", mock.Anything).Return(&kinesis.PutRecordOutput{}, nil)
	mockLogger.On("Infof", mock.Anything, mock.Anything).Return()
	mockLogger.On("Errorf", mock.Anything, mock.Anything, mock.Anything).Return()

	k.HandleMessage([]byte("data"), "streamName", mockLogger)

	mockLogger.AssertCalled(t, "Infof", "%sStarted inserting the record into kinesis.", "")
	mockLogger.AssertCalled(t, "Infof", "%sSuccessfully inserted record into kinesis.", "")
}

func resetDevMode(isDevMode bool) {
	appconfig.IsDevMode = isDevMode
}

func TestGetShardIteratorWithEmptySequenceAndError(t *testing.T) {
	mockClient := new(mocks.KinesisAPI)
	mockLogger := new(mocks.Logger)
	k := KinesisUtil{
		client: mockClient,
	}
	shardID := "shardId-000000000001"
	streamName := "some-stream"
	mockClient.On("GetShardIterator", &kinesis.GetShardIteratorInput{
		ShardId:           aws.String(shardID),
		ShardIteratorType: aws.String("TRIM_HORIZON"),
		StreamName:        aws.String(streamName),
	}).Return(nil, errors.New("Failed to get Shard Iterator"))
	mockLogger.On("Errorf", mock.Anything, mock.Anything, mock.Anything)
	_, err := k.GetShardIterator("shardId-000000000001", "some-stream", "", mockLogger)
	assert.NotNil(t, err)
	mockLogger.AssertCalled(t, "Errorf", "Failed to get the shard iterator, error:%v", err)
}

func TestGetShardIteratorWithEmptySequenceNumberAndSuccess(t *testing.T) {
	mockClient := new(mocks.KinesisAPI)
	mockLogger := new(mocks.Logger)
	k := KinesisUtil{
		client: mockClient,
	}
	shardID := "shardId-000000000001"
	streamName := "some-stream"
	mockClient.On("GetShardIterator", &kinesis.GetShardIteratorInput{
		ShardId:           aws.String(shardID),
		ShardIteratorType: aws.String("TRIM_HORIZON"),
		StreamName:        aws.String(streamName),
	}).Return(nil, nil)
	_, err := k.GetShardIterator(shardID, streamName, "", mockLogger)
	assert.Nil(t, err)
}
func TestGetShardIteratorWithNonEmptySequenceAndError(t *testing.T) {
	mockClient := new(mocks.KinesisAPI)
	mockLogger := new(mocks.Logger)
	k := KinesisUtil{
		client: mockClient,
	}
	shardID := "shardId-000000000001"
	streamName := "some-stream"
	sequenceNumber := "1234"
	mockClient.On("GetShardIterator", &kinesis.GetShardIteratorInput{
		ShardId:                aws.String(shardID),
		ShardIteratorType:      aws.String("AFTER_SEQUENCE_NUMBER"),
		StartingSequenceNumber: aws.String(sequenceNumber),
		StreamName:             aws.String(streamName),
	}).Return(nil, errors.New("Failed to get Shard Iterator"))
	mockLogger.On("Errorf", mock.Anything, mock.Anything, mock.Anything)
	_, err := k.GetShardIterator("shardId-000000000001", "some-stream", sequenceNumber, mockLogger)
	assert.NotNil(t, err)
	mockLogger.AssertCalled(t, "Errorf", "Failed to get the shard iterator, error:%v", err)
}

func TestGetShardIteratorWithNonEmptySequenceNumberAndSuccess(t *testing.T) {
	mockClient := new(mocks.KinesisAPI)
	mockLogger := new(mocks.Logger)
	k := KinesisUtil{
		client: mockClient,
	}
	shardID := "shardId-000000000001"
	streamName := "some-stream"
	sequenceNumber := "1234"
	mockClient.On("GetShardIterator", &kinesis.GetShardIteratorInput{
		ShardId:                aws.String(shardID),
		StartingSequenceNumber: aws.String(sequenceNumber),
		ShardIteratorType:      aws.String("AFTER_SEQUENCE_NUMBER"),
		StreamName:             aws.String(streamName),
	}).Return(nil, nil)
	_, err := k.GetShardIterator(shardID, streamName, sequenceNumber, mockLogger)
	assert.Nil(t, err)
}

func TestGetRecordsError(t *testing.T) {
	mockClient := new(mocks.KinesisAPI)
	mockLogger := new(mocks.Logger)
	k := KinesisUtil{
		client: mockClient,
	}

	mockClient.On("GetRecords", mock.Anything).Return(nil, errors.New("failed to get records"))
	mockLogger.On("Errorf", mock.Anything, mock.Anything, mock.Anything)
	shardID := "shardId-000000000001"
	_, err := k.GetRecords(&shardID, mockLogger)
	assert.NotNil(t, err)
	mockLogger.AssertCalled(t, "Errorf", "Failed to get records, error:%v", err)
}

func TestGetRecordsSuccess(t *testing.T) {
	mockClient := new(mocks.KinesisAPI)
	mockLogger := new(mocks.Logger)
	k := KinesisUtil{
		client: mockClient,
	}
	shardID := "shardId-000000000001"
	mockClient.On("GetRecords", mock.Anything).Return(nil, nil)
	_, err := k.GetRecords(&shardID, mockLogger)
	assert.Nil(t, err)
}
