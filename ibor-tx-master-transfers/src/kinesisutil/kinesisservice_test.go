package kinesisutil

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_NewKinesisUtil(t *testing.T) {
	//Arrange
	var ku *KinesisUtil

	//Act
	kinesisUtil := NewKinesisUtil()

	assert.IsType(t, ku, kinesisUtil)
	assert.NotNil(t, kinesisUtil.session)
	assert.NotNil(t, kinesisUtil.session)
}
