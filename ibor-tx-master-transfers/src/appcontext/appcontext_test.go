package appcontext

import (
	"context"
	"testing"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/models"
	"stash.ezesoft.net/ipc/ezeutils"
)

func TestGetFromKinesisMessage(t *testing.T) {
	activityID := "testActivityId"
	userName := "honsolo"
	firmAuthToken := "tbzqk"
	streamMessage := models.KinStreamMessage{
		ActivityID:    activityID,
		UserName:      userName,
		FirmAuthToken: firmAuthToken,
	}
	ctx := context.Background()
	ctxUpdated := GetFromKinesisMessage(ctx, &streamMessage)
	if val, ok := ezeutils.FromContext(ctxUpdated, ezeutils.XRequestIdKey); !ok {
		t.Errorf("Error in setting %s in context", ezeutils.XRequestIdKey)
	} else if val != activityID {
		t.Errorf("Invalid ActivityId exists in context")
	}
	if val, ok := ezeutils.FromContext(ctxUpdated, ezeutils.UserNameKey); !ok {
		t.Errorf("Error in setting %s in context", ezeutils.UserNameKey)
	} else if val != userName {
		t.Errorf("Invalid ActivityId exists in context")
	}
	if val, ok := ezeutils.FromContext(ctxUpdated, ezeutils.FirmAuthTokenKey); !ok {
		t.Errorf("Error in setting %s in context", ezeutils.FirmAuthTokenKey)
	} else if val != firmAuthToken {
		t.Errorf("Invalid ActivityId exists in context")
	}
}
