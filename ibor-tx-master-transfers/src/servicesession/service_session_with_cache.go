package servicesession

import (
	"time"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/httputil"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/logger"
)

var tokenTimeStamp time.Time
var userSessionToken string
var _GetServiceToken = GetServiceToken

// Use old token if:
// new request less than 2 mins since last request && userSessionToken already exists
func isTokenValid() bool {
	currTime := time.Now()
	isTokenTimeStampSet := !tokenTimeStamp.IsZero()

	// first time initialization
	if isTokenTimeStampSet == false {
		tokenTimeStamp = currTime
		logger.Log.Infof("[SetUserSessionToken][isTokenValid]: Initializing token for first time")
		return false
	}

	durationInMins := currTime.Sub(tokenTimeStamp).Minutes()
	if durationInMins < 2 && len(userSessionToken) > 0 {
		logger.Log.Infof("[SetUserSessionToken][isTokenValid]: Previous token still valid...duration less than 2 mins : %f", durationInMins)
		return true
	}

	return false
}

// SetUserSessionToken ...
func SetUserSessionToken(firmToken string) error {

	logger.Log.Infof("[main.go][SetUserSessionToken]: set user session token for firmToken: %s", firmToken)

	// //Creating the new session token Each time.
	// isTokenStillValid := isTokenValid()

	// if isTokenStillValid == true {
	// 	httputil.UserSessionToken = userSessionToken
	// 	return nil
	// }

	token, err := _GetServiceToken(firmToken)
	if err != nil {
		return err
	}

	userSessionToken = token
	tokenTimeStamp = time.Now()
	logger.Log.Infof("[main.go][SetUserSessionToken]:  New User session token set to: %s at time: %s for FirmToken: %s", userSessionToken, tokenTimeStamp.String(), firmToken)
	httputil.UserSessionToken = userSessionToken

	return nil
}
