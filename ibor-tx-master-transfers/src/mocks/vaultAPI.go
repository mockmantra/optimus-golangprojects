// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import (
	"io"

	"github.com/hashicorp/vault/api"
	"github.com/stretchr/testify/mock"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/interfaces"
)

// IVaultAPI is an autogenerated mock type for the IVaultAPI type
type VaultAPI struct {
	mock.Mock
}

// DefaultConfig provides a mock function with given fields:
func (_m *VaultAPI) DefaultConfig() interfaces.IVaultConfig {
	ret := _m.Called()

	var r0 interfaces.IVaultConfig
	if rf, ok := ret.Get(0).(func() interfaces.IVaultConfig); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(interfaces.IVaultConfig)
		}
	}

	return r0
}

// NewClient provides a mock function with given fields: config
func (_m *VaultAPI) NewClient(config interfaces.IVaultConfig) (interfaces.IVaultClient, error) {
	ret := _m.Called(config)

	var r0 interfaces.IVaultClient
	if rf, ok := ret.Get(0).(func(interfaces.IVaultConfig) interfaces.IVaultClient); ok {
		r0 = rf(config)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(interfaces.IVaultClient)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(interfaces.IVaultConfig) error); ok {
		r1 = rf(config)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ParseSecret provides a mock function with given fields: r
func (_m *VaultAPI) ParseSecret(r io.Reader) (*api.Secret, error) {
	ret := _m.Called(r)

	var r0 *api.Secret
	if rf, ok := ret.Get(0).(func(io.Reader) *api.Secret); ok {
		r0 = rf(r)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.Secret)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(io.Reader) error); ok {
		r1 = rf(r)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
