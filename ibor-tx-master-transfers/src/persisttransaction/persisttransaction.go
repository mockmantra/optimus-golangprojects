package persisttransaction

import (
	"time"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/interfaces"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/models"
)

// PersistTransactionsMaster ...
func PersistTransactionsMaster(trs *models.Transactiontable, logger interfaces.ILogger,
	eventTypeID models.EventTypeID, sourceSystemName models.SourceSystemName, securityID models.SecurityID, orderQuantity models.Quantity,
	da interfaces.IDataAccessor, rawDataStr string, userID int, userName string, userSessionToken string, activityID string,
	businessDateTime string, sourceSystemReference string, transactionDateTimeUTC string) (int, error) {
	var err error
	trs.BusinessDateTimeUTC, err = time.Parse(time.RFC3339, businessDateTime)
	if err != nil {
		return 0, err
	}
	trs.Message = rawDataStr
	trs.UserID = userID
	trs.UserName = userName
	trs.UserSessionToken = userSessionToken
	trs.ActivityID = activityID
	trs.ValidFromUTC = time.Now().UTC()
	trs.IsDeleted = false
	trs.EventTypeID = int(eventTypeID)
	trs.SourceSystemName = string(sourceSystemName)
	trs.SecurityID = int(securityID)
	trs.OrderQuantity = int(orderQuantity)
	trs.SourceSystemReference = sourceSystemReference
	transactionTimeUTC, parsingErr := time.Parse(time.RFC3339, transactionDateTimeUTC)
	if parsingErr != nil {
		return 0, parsingErr
	}
	insertError := da.InsertTransaction(trs, transactionTimeUTC)
	if insertError != nil {
		return 0, insertError
	}
	logger.Infof("Success saving transaction to db, recordId:%d", trs.RecordID)

	return trs.RecordID, nil
}
