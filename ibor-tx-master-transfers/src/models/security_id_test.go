package models

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	appconfig "stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/httputil"
	"stash.ezesoft.net/ipc/ezeutils"
)

func TestSecurityID_WithErrorInSecurityAPIHttpCall(t *testing.T) {
	var securityID SecurityID = 7
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadGateway)
	}))

	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SecurityID"
	transaction := Transaction{}

	err := securityID.Validate(transaction, key, 0)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. Service lookup failed", key))

}
func TestSecurityID_WithErrorSecurityNotFound(t *testing.T) {
	var securityID SecurityID = 71
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	}))

	httputil.UserSessionToken = "123"
	httputil.Ctx = ezeutils.WithKeyValue(context.Background(), "FirmAuthToken", "TBZQK")
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SecurityID"
	transaction := Transaction{}

	err := securityID.Validate(transaction, key, 0)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. SecurityId: %d does not exist", key, securityID))
	httputil.UserSessionToken = "123"

}
func TestSecurityID_WithAssetClassNameIsNotCashInSecurityAPIResponse(t *testing.T) {
	var securityID SecurityID = 72
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.Index(r.URL.Path, "securitymaster/v1/security") > -1 {
			_, err := w.Write([]byte("[{\"AssetClassName\":\"Equity\",\"SecurityId\":5}]"))
			if err != nil {
				t.Error("Unable to write the stream")
			}
		} else {
			w.WriteHeader(http.StatusNotFound)
		}
	}))
	httputil.UserSessionToken = "123"
	httputil.Ctx = ezeutils.WithKeyValue(context.Background(), "FirmAuthToken", "TBZQK")
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SecurityID"
	transaction := Transaction{}

	err := securityID.Validate(transaction, key, 0)

	assert.EqualError(t, err, "Invalid SecurityID. Expected asset class: Cash, Provided asset class: Equity")
	httputil.UserSessionToken = ""
}
func TestSecurityID_WithSecurityAPIResponseError(t *testing.T) {
	var securityID SecurityID = 73
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.Index(r.URL.Path, "securitymaster/v1/security") > -1 {
			_, err := w.Write([]byte("[{\"AssetClassName\":\"Equity\",\"SecurityId\":5"))
			if err != nil {
				t.Error("Unable to write the stream")
			}
		} else {
			_, err := w.Write([]byte("{\"Items\":[{\"SecurityID\":73}]}"))
			if err != nil {
				t.Error("Unable to write the stream")
			}
		}
	}))
	httputil.UserSessionToken = "123"
	httputil.Ctx = ezeutils.WithKeyValue(context.Background(), "FirmAuthToken", "TBZQK")
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SecurityID"
	transaction := Transaction{
		SourceSystemReference: "8",
	}

	err := securityID.Validate(transaction, key, 0)

	assert.EqualError(t, err, "Invalid SecurityID. Service lookup failed")
	httputil.UserSessionToken = ""

}
func TestSecurityID_WithAssetClassNameIsCashInSecurityAPIResponse(t *testing.T) {
	var securityID SecurityID = 74
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.Index(r.URL.Path, "securitymaster/v1/security") > -1 {
			_, err := w.Write([]byte("[{\"AssetClassName\":\"Cash\",\"SecurityId\":5}]"))
			if err != nil {
				t.Error("Unable to write the stream")
			}
		} else {
			_, err := w.Write([]byte("{\"Items\":[{\"SecurityID\":74}]}"))
			if err != nil {
				t.Error("Unable to write the stream")
			}
		}
	}))
	httputil.UserSessionToken = "123"
	httputil.Ctx = ezeutils.WithKeyValue(context.Background(), "FirmAuthToken", "TBZQK")
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SecurityID"
	transaction := Transaction{
		SourceSystemReference: "8",
	}

	err := securityID.Validate(transaction, key, 0)

	assert.Nil(t, err)
	httputil.UserSessionToken = ""

}
