package models

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

var transferPriceValidTestJSON = `{
    "Allocations": [
      {
        "TransferPrice" : 12.12
	  },
	  {
	  }
	],
	"EventTypeId": 1013
  }`

var transferPriceInvalidTestJSON = `{
    "Allocations": [
      {
        "TransferPrice" : 12.31
      }
	],
	"EventTypeId": 1009
  }`

func TestTransferPrice_TransferAtPriceValid(t *testing.T) {
	var testTransaction Transaction
	var testTransferPrice TransferPrice = 12.12
	unmarhsalErr := json.Unmarshal([]byte(transferPriceValidTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalErr)

	err := testTransferPrice.Validate(testTransaction, "TransferPrice", 0)
	assert.Nil(t, err)
}

func TestTransferPrice_TransferAtPriceInvalidEventType(t *testing.T) {
	var testTransaction Transaction
	var testTransferPrice TransferPrice = 12.31
	unmarhsalErr := json.Unmarshal([]byte(transferPriceInvalidTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalErr)

	err := testTransferPrice.Validate(testTransaction, "TransferPrice", 0)
	assert.EqualError(t, err, "TransferPrice : Field not valid for EventTypeID 1009")
}

func TestTransferPrice_TransferAtPriceInvalidTransferPrice(t *testing.T) {
	var testTransaction Transaction
	var testTransferPrice TransferPrice = 12.12
	unmarhsalErr := json.Unmarshal([]byte(transferPriceValidTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalErr)

	err := testTransferPrice.Validate(testTransaction, "TransferPrice", 1)
	assert.EqualError(t, err, "TransferPrice : Data not provided")
}
