package models

import (
	"encoding/json"
	"fmt"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/httputil"
	"strings"
)

var timeZoneURL = "referencedata/v1/GetTimeZoneById"

// TimeZoneResponse type
type TimeZoneResponse struct {
	BaseUtcOffset int `json:"BaseUtcOffset"`
	DstUtcOffset  int `json:"DstUtcOffset"`
}

// TimeZoneID type
type TimeZoneID int

// Validate validates the fields
func (value TimeZoneID) Validate(transaction Transaction, fieldName string) error {
	_, err := NewTimezoneRequest(int(value))
	if err != nil {
		if strings.Contains(err.Error(), "error:404") {
			return fmt.Errorf("Invalid %s. TimeZoneId: %d does not exist", fieldName, value)
		}
		return fmt.Errorf("%s: Service lookup failed", fieldName)
	}
	return nil
}

// NewTimezoneRequest fetches the TimeZone details by timezoneid
func NewTimezoneRequest(timeZoneID int) (TimeZoneResponse, error) {
	bodyBytes, err := httputil.NewRequest("GET", fmt.Sprintf("%s?timeZoneId=%d", timeZoneURL, timeZoneID), nil)
	if err == nil {
		var timeZoneResponse []TimeZoneResponse
		unmarshalErr := json.Unmarshal(bodyBytes, &timeZoneResponse)
		if unmarshalErr != nil {
			return TimeZoneResponse{}, unmarshalErr
		}
		return timeZoneResponse[0], err
	}
	return TimeZoneResponse{}, err
}
