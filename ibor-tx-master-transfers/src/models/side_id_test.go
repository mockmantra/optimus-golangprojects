package models

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

var errSideID = fmt.Errorf("Invalid SideId. Must be 1(Long) or 2(Short)")

var testCasesSideID = []struct {
	in  SideID
	out error
}{
	{1, nil},
	{2, nil},
	{20, errSideID},
	{0, errSideID},
}

func TestSideId_WithCases(t *testing.T) {
	transaction := Transaction{}

	for _, testCase := range testCasesSideID {
		err := testCase.in.Validate(transaction, "SideId")
		assert.Equal(t, testCase.out, err)
	}
}
