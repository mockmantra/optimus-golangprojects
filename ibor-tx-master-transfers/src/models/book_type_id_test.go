package models

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

var bookTypeIDTestJSONForStrategy = `{
	"Allocations": [
	  {
		"BookTypeID": 3,
		"Quantity":    200
	  }
	],
	"EventTypeId": 1013
}`

var bookTypeIDTestJSONForLocation = `{
	"Allocations": [
	  {
		"BookTypeId": 3,
		"Quantity":    200
	  }
	],
	"EventTypeId": 1013
}`

var bookTypeIDTestJSONBookTypeEventTypeMismatch = `{
	"Allocations": [
	  {
		"BookTypeId": 3,
		"Quantity":    200
	  }
	],
	"EventTypeId": 1009
}`

func TestBookTypeIDValidForStrategy(t *testing.T) {
	var testTransaction Transaction
	unMarshalErr := json.Unmarshal([]byte(bookTypeIDTestJSONForStrategy), &testTransaction)
	assert.Nil(t, unMarshalErr)
	err := testTransaction.Allocations[0].BookTypeID.Validate(testTransaction, "BookTypeID", 0)
	assert.Nil(t, err)
}

func TestBookTypeIDValidForLocation(t *testing.T) {
	var testTransaction Transaction
	unMarshalErr := json.Unmarshal([]byte(bookTypeIDTestJSONForLocation), &testTransaction)
	assert.Nil(t, unMarshalErr)
	err := testTransaction.Allocations[0].BookTypeID.Validate(testTransaction, "BookTypeID", 0)
	assert.Nil(t, err)
}
func TestBookTypeIDInValid(t *testing.T) {
	var testTransaction Transaction
	unMarshalErr := json.Unmarshal([]byte(bookTypeIDTestJSONBookTypeEventTypeMismatch), &testTransaction)
	assert.Nil(t, unMarshalErr)
	err := testTransaction.Allocations[0].BookTypeID.Validate(testTransaction, "BookTypeID", 0)
	assert.EqualError(t, err, fmt.Sprintf("Invalid BookTypeID : Expected 2 (for Location) or 3 (for Strategy), Provided value %v", *testTransaction.Allocations[0].BookTypeID))
}
