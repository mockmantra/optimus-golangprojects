package models

import (
	"fmt"
	"strings"
)

// BaseCurrencySecurityID field
type BaseCurrencySecurityID int

// Validate validates the fields
func (value BaseCurrencySecurityID) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	currentAllocation := transaction.Allocations[allocationIndex]
	var portfolioID int
	if strings.Contains(fieldName, "FromPrt") {
		portfolioID = int(*currentAllocation.FromPortfolioID)
	} else {
		portfolioID = int(*currentAllocation.ToPortfolioID)
	}
	CurrencyPortfolio, err := NewPortfolioRequest(portfolioID)
	if err == nil {
		if CurrencyPortfolio.BaseCurrencyID == int(value) {
			return nil
		}
		return fmt.Errorf("Invalid %s : Expected value %d, Provided Value %d", fieldName, CurrencyPortfolio.BaseCurrencyID, value)
	} else if strings.Contains(err.Error(), "error:404") {
		return fmt.Errorf("Invalid %s : PortfolioID %d does not exist", fieldName, portfolioID)
	}
	return fmt.Errorf("Error in allocation PortfolioID %d. %s : Service lookup failed", portfolioID, fieldName)
}
