package models

// AllocationValidator interface
type AllocationValidator interface {
	Validate(transaction Transaction, fieldName string, allocationIndex int) error
}

// Allocation allocation
type Allocation struct {
	FromPrtBaseCurrencySecurityID      *BaseCurrencySecurityID         `json:"FromPrtBaseCurrencySecurityId"`
	ToPrtBaseCurrencySecurityID        *BaseCurrencySecurityID         `json:"ToPrtBaseCurrencySecurityId"`
	BookTypeID                         *BookTypeID                     `json:"BookTypeId"`
	FromCustodianCounterpartyAccountID *CustodianCounterpartyAccountID `json:"FromCustodianCounterpartyAccountId,omitempty"`
	ToCustodianCounterpartyAccountID   *CustodianCounterpartyAccountID `json:"ToCustodianCounterpartyAccountId,omitempty"`
	FromPortfolioID                    *PortfolioID                    `json:"FromPortfolioId"`
	ToPortfolioID                      *PortfolioID                    `json:"ToPortfolioId"`
	Quantity                           *Quantity                       `json:"Quantity"`
	SecurityID                         *SecurityID                     `json:"SecurityId"`
	SecurityTemplateID                 *SecurityTemplateID             `json:"TemplateId"`
	SettleToSystemFXRate               *float64                        `json:"SettleToSystemFXRate,omitempty"`
	FromPrtSettleToBaseFXRate          *float64                        `json:"FromPrtSettleToBaseFXRate,omitempty"`
	ToPrtSettleToBaseFXRate            *float64                        `json:"ToPrtSettleToBaseFXRate,omitempty"`
	TransferPrice                      *TransferPrice                  `json:"TransferPrice"`
}
