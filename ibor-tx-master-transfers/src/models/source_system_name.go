package models

import (
	"encoding/json"
	"fmt"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/httputil"
)

// SourceSystemName field
type SourceSystemName string

// SourceSystem type
type SourceSystem struct {
	ID   int    `json:"Id"`
	Name string `json:"Name"`
}

const sourceSystemURL = "ibor/v2/sourceSystems?includeDeleted=false"

// Validate validates the fields
func (value SourceSystemName) Validate(transaction Transaction, fieldName string) error {
	sourceSystems, err := NewSourceSystemRequest()
	if err == nil {
		for _, sourceSystem := range sourceSystems {
			if sourceSystem.Name == string(value) {
				return nil
			}
		}
		return fmt.Errorf("Invalid %s. %s %s does not exist", fieldName, fieldName, string(value))
	}
	return fmt.Errorf("%s : Service lookup failed", fieldName)
}

// NewSourceSystemRequest fetches all the SourceSystem Names
func NewSourceSystemRequest() ([]SourceSystem, error) {
	bodyBytes, err := httputil.NewRequest("GET", sourceSystemURL, nil)
	var sourceSystems []SourceSystem
	if err == nil {
		errMarshal := json.Unmarshal(bodyBytes, &sourceSystems)
		if errMarshal != nil {
			return sourceSystems, errMarshal
		}
	}
	return sourceSystems, err
}
