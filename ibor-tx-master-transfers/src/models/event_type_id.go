package models

import (
	"fmt"
)

// EventTypeID field
type EventTypeID int

// TransferAtCost enum
var TransferAtCost EventTypeID = 1009

// TransferAtPrice enum
var TransferAtPrice EventTypeID = 1013

// Validate validates the fields
func (value EventTypeID) Validate(transaction Transaction, fieldName string) error {
	if value == TransferAtCost || value == TransferAtPrice {
		return nil
	}
	return fmt.Errorf("Invalid %s. Must be 1009(Transfer At Cost) or 1013(Transfer At Price)", fieldName)
}
