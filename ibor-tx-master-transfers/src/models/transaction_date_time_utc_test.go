package models

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/httputil"
)

func TestTransactionDateTimeUTC_InvalidDateFormat(t *testing.T) {
	str := "[{\"BaseUtcOffset\":-300}]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"

	var transactionDateTimeUTC TransactionDateTimeUTC = "2018-09-08 13:25:43Z"
	transaction := Transaction{}
	key := "TransactionDateTimeUTC"
	err := transactionDateTimeUTC.Validate(transaction, key)
	httputil.UserSessionToken = ""

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. Incorrect format", key))
}
func TestTransactionDateTimeUTC_ValidDateFormatAndNotEqualsToCurrentDate(t *testing.T) {
	str := "[{\"BaseUtcOffset\":-300}]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"

	var transactionDateTimeUTC TransactionDateTimeUTC = "2018-09-08T13:25:43.1234567Z"
	transaction := Transaction{}
	key := "TransactionDateTimeUTC"
	err := transactionDateTimeUTC.Validate(transaction, key)
	httputil.UserSessionToken = ""

	currentDateUTC := time.Now().UTC().Format(time.RFC3339)[:10]
	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. TransactionDate must match current date. Current Date: %s, Provided TransactionDate: %s", key, currentDateUTC, transactionDateTimeUTC[:10]))
}
func TestTransactionDateTimeUTC_InvalidResponse(t *testing.T) {
	str := "[{\"BaseUtcOffset\":-300"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"

	currentDate := time.Now().UTC().Add(time.Minute * time.Duration(300)).Format(time.RFC3339Nano)
	transactionDateTimeUTC := TransactionDateTimeUTC(currentDate)
	transaction := Transaction{}
	key := "TransactionDateTimeUTC"
	err := transactionDateTimeUTC.Validate(transaction, key)
	httputil.UserSessionToken = ""

	assert.Equal(t, err, fmt.Errorf("%s : Service lookup failed", key))
}

func TestTransactionDateTimeUTC_ValidDateFormatAndEqualsToCurrentDate(t *testing.T) {
	str := "[{\"BaseUtcOffset\":300}]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"

	currentDate := time.Now().UTC().Add(time.Minute * time.Duration(300)).Format(time.RFC3339Nano)
	transactionDateTimeUTC := TransactionDateTimeUTC(currentDate)
	transaction := Transaction{}
	key := "TransactionDateTimeUTC"
	err := transactionDateTimeUTC.Validate(transaction, key)
	httputil.UserSessionToken = ""

	assert.Nil(t, err)
}
