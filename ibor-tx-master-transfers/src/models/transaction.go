package models

// Validator interface
type Validator interface {
	Validate(transaction Transaction, fieldName string) error
}

// Transaction transaction
type Transaction struct {
	Allocations              []Allocation           `json:"Allocations"`
	BusinessDateTimeUTC      string                 `json:"BusinessDateTimeUTC"`
	EventTypeID              EventTypeID            `json:"EventTypeId"`
	LogicalTimeID            LogicalTimeID          `json:"LogicalTimeId"`
	SequenceNumber           int                    `json:"SequenceNumber"`
	SideID                   SideID                 `json:"SideId"`
	SourceSystemName         SourceSystemName       `json:"SourceSystemName"`
	SourceSystemReference    string                 `json:"SourceSystemReference"`
	SystemCurrencySecurityID CurrencySecurityID     `json:"SystemCurrencySecurityId"`
	TransactionDateTimeUTC   TransactionDateTimeUTC `json:"TransactionDateTimeUTC"`
	TransactionTimeZoneID    TimeZoneID             `json:"TransactionTimeZoneId"`
}
