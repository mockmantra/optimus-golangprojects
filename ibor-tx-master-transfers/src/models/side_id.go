package models

import (
	"fmt"
)

// SideIDLong enum
var SideIDLong SideID = 1

// SideIDShort enum
var SideIDShort SideID = 2

// SideID field
type SideID int

// Validate validates the field
func (value SideID) Validate(transaction Transaction, fieldName string) error {
	if value == SideIDLong || value == SideIDShort {
		return nil
	}
	return fmt.Errorf("Invalid %s. Must be 1(Long) or 2(Short)", fieldName)
}
