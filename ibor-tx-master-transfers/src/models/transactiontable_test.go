package models

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTableNameTransactiontable(t *testing.T) {

	var transTable Transactiontable
	tableName := transTable.TableName()
	assert.Equal(t, tableName, "Ibor_Transfers_Transactions")

}
