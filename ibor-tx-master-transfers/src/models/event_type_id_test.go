package models

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

var errEventTypeID = fmt.Errorf("Invalid EventTypeId. Must be 1009(Transfer At Cost) or 1013(Transfer At Price)")

var testCasesEventTypeID = []struct {
	in  EventTypeID
	out error
}{
	{1009, nil},
	{1013, nil},
	{150, errEventTypeID},
	{200, errEventTypeID},
}

func TestEventTypeId_WithCases(t *testing.T) {
	transaction := Transaction{}

	for _, testCase := range testCasesEventTypeID {
		err := testCase.in.Validate(transaction, "EventTypeId")
		assert.Equal(t, testCase.out, err)
	}
}
