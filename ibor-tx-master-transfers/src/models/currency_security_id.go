package models

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"github.com/patrickmn/go-cache"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/httputil"
	"stash.ezesoft.net/ipc/ezeutils"
)

// CurrencySecurityID field
type CurrencySecurityID int

// Security type
type Security struct {
	SecurityID         int    `json:"SecurityId"`
	AssetClassName     string `json:"AssetClassName"`
	SecurityTemplateID int    `json:"TemplateId"`
	Symbol             string `json:"Symbol"`
}

var securitiesInPosition = cache.New(cache.NoExpiration, cache.NoExpiration)

const securityURL = "securitymaster/v1/security?securityId=%v"

// Validate validates the fields
func (value CurrencySecurityID) Validate(transaction Transaction, fieldName string) error {
	securities, err := NewSecurityRequest(int(value))
	if err == nil {
		if securities[0].AssetClassName != "Cash" {
			return fmt.Errorf("Invalid %s. Expected AssetClass : Cash, Provided Assetclass : %s", fieldName, securities[0].AssetClassName)
		}
	} else {
		if strings.Contains(err.Error(), "error:404") {
			return fmt.Errorf("Invalid %s. %s %d  does not exist", fieldName, fieldName, int(value))
		}
		return fmt.Errorf("%s : Service lookup failed", fieldName)
	}
	return nil
}

// NewSecurityRequest fetches the Orders by OrderId
func NewSecurityRequest(securityID int) ([]Security, error) {
	var key string
	if firmId, ok := ezeutils.FromContext(httputil.Ctx, ezeutils.FirmAuthTokenKey); ok {
		key = firmId + "_" + strconv.Itoa(securityID)
		if securitiesbyte, found := securitiesInPosition.Get(key); found {
			return securitiesbyte.([]Security), nil
		}
	}

	bodyBytes, err := httputil.NewRequest("GET", fmt.Sprintf(securityURL, securityID), nil)
	var securities []Security
	if err == nil {
		errMarshal := json.Unmarshal(bodyBytes, &securities)
		if errMarshal != nil {
			return securities, errMarshal
		}
		securitiesInPosition.Set(key, securities, cache.NoExpiration)
	}
	return securities, err
}
