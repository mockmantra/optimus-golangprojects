package models

import (
	"fmt"
)

// BookTypeStrategy Enum
var BookTypeStrategy BookTypeID = 3

// BookTypeLocation Enum
var BookTypeLocation BookTypeID = 2

// BookTypeID type
type BookTypeID int

// Validate validates the field
func (value BookTypeID) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	if (value == BookTypeStrategy && transaction.EventTypeID == TransferAtPrice) || (value == BookTypeLocation && transaction.EventTypeID == TransferAtCost) {
		return nil
	}
	return fmt.Errorf("Invalid %s : Expected 2 (for Location) or 3 (for Strategy), Provided value %d", fieldName, value)
}
