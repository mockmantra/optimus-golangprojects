package models

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	appconfig "stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/httputil"
	"stash.ezesoft.net/ipc/ezeutils"
)

func TestCurrencySecurityId_WithValidSecurityIdAndAssetClassIsNotCash(t *testing.T) {
	var currencySecurityID CurrencySecurityID = 5
	str := "[{\"AssetClassName\":\"NonCash\",\"SecurityId\":5}]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))

	httputil.UserSessionToken = "123"
	httputil.Ctx = ezeutils.WithKeyValue(context.Background(), "FirmAuthToken", "TBZQK")

	appconfig.BaseURI = ts.URL
	defer ts.Close()
	transaction := Transaction{}

	err := currencySecurityID.Validate(transaction, "CurrencySecurityId")

	assert.Equal(t, err, fmt.Errorf("Invalid %s. Expected AssetClass : Cash, Provided Assetclass : %s", "CurrencySecurityId", "NonCash"))

	httputil.UserSessionToken = ""
}
func TestCurrencySecurityId_WithValidSecurityIdAndAssetClassIsCash(t *testing.T) {
	var currencySecurityID CurrencySecurityID = 4
	str := "[{\"AssetClassName\":\"Cash\",\"SecurityId\":4}]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"
	httputil.Ctx = ezeutils.WithKeyValue(context.Background(), "FirmAuthToken", "TBZQK")

	defer ts.Close()
	transaction := Transaction{}

	err := currencySecurityID.Validate(transaction, "CurrencySecurityId")

	assert.Nil(t, err)

	httputil.UserSessionToken = ""
}

func TestCurrencySecurityId_WithValidSecurityIdAndAssetClassIsCash_Should_Hit_Cache(t *testing.T) {
	var currencySecurityID CurrencySecurityID = 4
	str := "[{\"AssetClassName\":\"Cash\",\"SecurityId\":4}]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"

	httputil.Ctx = ezeutils.WithKeyValue(context.Background(), "FirmAuthToken", "TBZQK")

	transaction := Transaction{}

	err := currencySecurityID.Validate(transaction, "CurrencySecurityId")

	assert.Nil(t, err)

	ts.Close()

	//creating a new instance of httptest where it returns not found for every request sent
	ts = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	}))
	appconfig.BaseURI = ts.URL
	defer ts.Close()

	var newCurrencySecurityID CurrencySecurityID = 555
	err = newCurrencySecurityID.Validate(transaction, "CurrencySecurityId")
	assert.EqualError(t, err, "Invalid CurrencySecurityId. CurrencySecurityId 555  does not exist")

	//sending validation for securityId 4 again, it should give us the value from cache
	//and http call, which gives not found error, won't be made
	err = currencySecurityID.Validate(transaction, "CurrencySecurityId")

	assert.Nil(t, err)

	httputil.UserSessionToken = ""
}

func TestCurrencySecurityId_WithInvalidSecurityId(t *testing.T) {
	var currencySecurityID CurrencySecurityID = 1
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	}))
	appconfig.BaseURI = ts.URL

	defer ts.Close()

	httputil.UserSessionToken = "123"
	httputil.Ctx = ezeutils.WithKeyValue(context.Background(), "FirmAuthToken", "TBZQK")

	transaction := Transaction{}

	err := currencySecurityID.Validate(transaction, "CurrencySecurityId")

	assert.EqualError(t, err, "Invalid CurrencySecurityId. CurrencySecurityId 1  does not exist")

	httputil.UserSessionToken = ""
}

func TestCurrencySecurityId_WithInvalidSecurityIdLookup(t *testing.T) {
	var currencySecurityID CurrencySecurityID = 2
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadGateway)
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"
	httputil.Ctx = ezeutils.WithKeyValue(context.Background(), "FirmAuthToken", "TBZQK")

	defer ts.Close()
	transaction := Transaction{}

	err := currencySecurityID.Validate(transaction, "CurrencySecurityId")

	assert.Equal(t, err, fmt.Errorf("%s : Service lookup failed", "CurrencySecurityId"))

	httputil.UserSessionToken = ""
}

func TestCurrencySecurityId_WithUnmarhsalError(t *testing.T) {
	var currencySecurityID CurrencySecurityID = 3
	str := ""
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"
	httputil.Ctx = ezeutils.WithKeyValue(context.Background(), "FirmAuthToken", "TBZQK")

	defer ts.Close()
	transaction := Transaction{}

	err := currencySecurityID.Validate(transaction, "CurrencySecurityId")

	assert.Equal(t, err, fmt.Errorf("%s : Service lookup failed", "CurrencySecurityId"))

	httputil.UserSessionToken = ""
}
