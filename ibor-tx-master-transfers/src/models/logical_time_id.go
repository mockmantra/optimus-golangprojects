package models

import (
	"fmt"
)

// LogicalTimeID field
type LogicalTimeID int

// StartOfDay enum
var StartOfDay LogicalTimeID = 50

// TradingDay enum
var TradingDay LogicalTimeID = 100

// EndOfDay enum
var EndOfDay LogicalTimeID = 150

// Validate validates the fields
func (value LogicalTimeID) Validate(transaction Transaction, fieldName string) error {
	if value == StartOfDay || value == TradingDay || value == EndOfDay {
		return nil
	}
	return fmt.Errorf("Invalid %s. Must be 50(Start Of Day), 100(Trading Day) or 150(End Of Day)", fieldName)
}
