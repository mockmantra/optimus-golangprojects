package models

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	appconfig "stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/httputil"
	"stash.ezesoft.net/ipc/ezeutils"
)

func TestSecurityTemplateID_WithErrorInSecurityAPIHttpCall(t *testing.T) {
	var securityTemplateID SecurityTemplateID = 7
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadGateway)
	}))

	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SecurityTemplateID"
	var id SecurityID = 5
	transaction := Transaction{
		Allocations: []Allocation{
			{
				SecurityID: &id,
			},
		},
	}

	err := securityTemplateID.Validate(transaction, key, 0)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. Service lookup failed", key))

}
func TestSecurityTemplateID_WithErrorSecurityNotFound(t *testing.T) {
	var securityTemplateID SecurityTemplateID = 71
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	}))

	httputil.UserSessionToken = "123"
	httputil.Ctx = ezeutils.WithKeyValue(context.Background(), "FirmAuthToken", "TBZQK")
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SecurityTemplateID"
	var id SecurityID = 123
	transaction := Transaction{
		Allocations: []Allocation{
			{
				SecurityID: &id,
			},
		},
	}

	err := securityTemplateID.Validate(transaction, key, 0)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. SecurityTemplateId: %d does not exist", key, securityTemplateID))
	httputil.UserSessionToken = "123"

}
func TestSecurityTemplateID_WithSecurityTemplateIdDoesNotMatchWithTemplateForSecurityId(t *testing.T) {
	var securityTemplateID SecurityTemplateID = 71
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.Index(r.URL.Path, "securitymaster/v1/security") > -1 {
			_, err := w.Write([]byte("[{\"SecurityId\":5, \"TemplateId\":8}]"))
			if err != nil {
				t.Error("Unable to write the stream")
			}
		} else {
			w.WriteHeader(http.StatusNotFound)
		}
	}))
	httputil.UserSessionToken = "123"
	httputil.Ctx = ezeutils.WithKeyValue(context.Background(), "FirmAuthToken", "TBZQK")
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SecurityTemplateID"

	var id SecurityID = 5
	transaction := Transaction{
		Allocations: []Allocation{
			{
				SecurityID: &id,
			},
		},
	}

	err := securityTemplateID.Validate(transaction, key, 0)

	assert.EqualError(t, err, "Invalid SecurityTemplateID. SecurityTemplateId: 71 does not match with template for SecurityID : 5")
	httputil.UserSessionToken = ""
}

func TestSecurityTemplateID_WithValidResponse(t *testing.T) {
	var securityTemplateID SecurityTemplateID = 8
	str := "[{\"SecurityId\":5, \"TemplateId\":8}]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))

	httputil.UserSessionToken = "123"
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SecurityTemplateID"
	var id SecurityID = 5
	transaction := Transaction{
		Allocations: []Allocation{
			{
				SecurityID: &id,
			},
		},
	}

	err := securityTemplateID.Validate(transaction, key, 0)
	assert.Nil(t, err)
	httputil.UserSessionToken = ""
}

func TestSecurityTemplateID_WithInvalidResponse(t *testing.T) {
	var securityTemplateID SecurityTemplateID = 7
	str := "[{\"Id\":7,\"SecurityTemplateId\":8]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))

	httputil.UserSessionToken = "123"
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SecurityTemplateID"
	var id SecurityID = 18
	transaction := Transaction{
		Allocations: []Allocation{
			{
				SecurityID: &id,
			},
		},
	}

	err := securityTemplateID.Validate(transaction, key, 0)
	assert.EqualError(t, err, "Invalid SecurityTemplateID. Service lookup failed")
	httputil.UserSessionToken = ""
}
