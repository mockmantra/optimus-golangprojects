package models

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

var errLogicalTimeID = fmt.Errorf("Invalid LogicalTimeId. Must be 50(Start Of Day), 100(Trading Day) or 150(End Of Day)")

var testCasesLogicalTimeID = []struct {
	in  LogicalTimeID
	out error
}{
	{50, nil},
	{100, nil},
	{150, nil},
	{200, errLogicalTimeID},
}

func TestLogicalTimeId_WithCases(t *testing.T) {
	transaction := Transaction{}

	for _, testCase := range testCasesLogicalTimeID {
		err := testCase.in.Validate(transaction, "LogicalTimeId")
		assert.Equal(t, testCase.out, err)
	}
}
