package models

import (
	"encoding/json"
	"fmt"
	"reflect"
	"strings"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/httputil"
)

// Counterparty field
type Counterparty struct {
	ID                 int `json:"Id"`
	CounterpartyTypeID int `json:"CounterpartyTypeId"`
}

// CustodianCounterpartyAccountID field
type CustodianCounterpartyAccountID int

// CustodianAccount type
type CustodianAccount struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// GraphQLCustodianResponse type
type GraphQLCustodianResponse struct {
	Data struct {
		CustodianAccounts []CustodianAccount `json:"custodianAccounts"`
	} `json:"data"`
}

const custodianGraphQL = "coredata/v2/graphql?query={custodianAccounts(ids:[%d]){id,name}}"

// Validate validates the field
func (value CustodianCounterpartyAccountID) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	if transaction.EventTypeID == TransferAtCost {
		if strings.Contains(fieldName, "From") && reflect.ValueOf(transaction.Allocations[allocationIndex].FromCustodianCounterpartyAccountID).IsNil() {
			return fmt.Errorf("Invalid %s : Data not provided", fieldName)
		} else if strings.Contains(fieldName, "To") && reflect.ValueOf(transaction.Allocations[allocationIndex].ToCustodianCounterpartyAccountID).IsNil() {
			return fmt.Errorf("Invalid %s : Data not provided", fieldName)
		} else {
			if *transaction.Allocations[allocationIndex].BookTypeID == BookTypeLocation {
				bodyBytes, err := httputil.NewRequest("GET", fmt.Sprintf(custodianGraphQL, value), nil)
				var custodianResponse GraphQLCustodianResponse
				if err == nil {
					unmarshalErr := json.Unmarshal(bodyBytes, &custodianResponse)
					if unmarshalErr == nil {
						if len(custodianResponse.Data.CustodianAccounts) > 0 {
							return nil
						}
						return fmt.Errorf("Invalid %s : %d does not exist", fieldName, value)
					}
					return fmt.Errorf("%s : Unmarshalling error", fieldName)
				}
				return fmt.Errorf("%s : Service lookup failed", fieldName)
			}
			return fmt.Errorf("Invalid %s : Expected BookType %d, Provided BookType %d ", fieldName, BookTypeLocation, int(*transaction.Allocations[allocationIndex].BookTypeID))
		}
	}
	return fmt.Errorf("%s : Field not valid for EventTypeID %d", fieldName, transaction.EventTypeID)
}
