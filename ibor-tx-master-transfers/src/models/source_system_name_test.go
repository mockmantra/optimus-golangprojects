package models

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/httputil"
)

func TestSourceSystemName_WithValidResponseWithItems(t *testing.T) {
	var sourceSystemName SourceSystemName = "ImsAccounting"
	str := "[{\"Id\":1,\"Name\":\"ImsAccounting\"},{\"Id\":2,\"Name\":\"SOD\"},{\"Id\":3,\"Name\":\"OMS\"}]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))

	httputil.UserSessionToken = "123"
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SourceSystemName"
	transaction := Transaction{}

	err := sourceSystemName.Validate(transaction, key)
	assert.Nil(t, err)
	httputil.UserSessionToken = ""
}

func TestSourceSystemName_WithInvalidResponseWithItems(t *testing.T) {
	var sourceSystemName SourceSystemName = "ImsAccounting"
	str := "[{\"Id\":1,\"Name\":\"ImsAccounting\"{\"Id\":2,\"Name\":\"SOD\"},{\"Id\":3,\"Name\":\"OMS\"}]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))

	httputil.UserSessionToken = "123"
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SourceSystemName"
	transaction := Transaction{}

	err := sourceSystemName.Validate(transaction, key)
	assert.Equal(t, err, fmt.Errorf("%s : Service lookup failed", key))
	httputil.UserSessionToken = ""
}

func TestSourceSystemName_WithInvalidSourceSystemName(t *testing.T) {
	var sourceSystemName SourceSystemName = "testSystemName"
	str := "[{\"Id\":1,\"Name\":\"ImsAccounting\"},{\"Id\":2,\"Name\":\"SOD\"},{\"Id\":3,\"Name\":\"OMS\"}]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))

	httputil.UserSessionToken = "123"
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SourceSystemName"
	transaction := Transaction{}

	err := sourceSystemName.Validate(transaction, key)
	assert.Equal(t, err, fmt.Errorf("Invalid %s. %s %s does not exist", key, key, sourceSystemName))
	httputil.UserSessionToken = ""
}
