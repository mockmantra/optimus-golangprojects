package models

import (
	"fmt"
	"reflect"
)

// TransferPrice field
type TransferPrice float64

// Validate validates the TransferPrice
func (value TransferPrice) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	if transaction.EventTypeID == TransferAtPrice {
		if !reflect.ValueOf(transaction.Allocations[allocationIndex].TransferPrice).IsNil() {
			return nil
		}
		return fmt.Errorf("%s : Data not provided", fieldName)
	}
	return fmt.Errorf("%s : Field not valid for EventTypeID %d", fieldName, transaction.EventTypeID)
}
