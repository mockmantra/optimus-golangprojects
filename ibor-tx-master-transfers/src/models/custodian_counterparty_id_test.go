package models

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/httputil"
)

var custodianTestJSON = `{
    "Allocations": [
      {
		"BookTypeID": 2,
		"FromCustodianCounterpartyAccountId": 13,
        "PortfolioID" :12
      },
      {
		"BookTypeID": 1,
		"ToCustodianCounterpartyAccountId" : 13,
        "PortfolioID" :13
      }
	],
	"EventTypeId": 1009
  }`

var custodianInvalidEventTypeTestJSON = `{
    "Allocations": [
      {
		"BookTypeID": 2,
		"FromCustodianCounterpartyAccountId": 13,
        "PortfolioID" :12
      },
      {
		"BookTypeID": 1,
		"ToCustodianCounterpartyAccountId" : 13,
        "PortfolioID" :13
	  }
	],
	"EventTypeId": 1013
  }`

var errCustodian = fmt.Errorf("Invalid CustodianCounterpartyId")
var errFromCustodian = fmt.Errorf("Invalid FromCustodianCounterpartyId : 13 does not exist")

var testCasesCustodian = []struct {
	in  CustodianCounterpartyAccountID
	out error
}{
	{4, nil},
}

func TestCustodianCounterpartyIdValidBookTypeLocation(t *testing.T) {
	var testTransaction Transaction
	unmarhsalError := json.Unmarshal([]byte(custodianTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalError)
	str := "{\"data\": {\"custodianAccounts\": [{\"id\": 4, \"name\": \"GSCOa\"}]}}"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()

	for _, testCase := range testCasesCustodian {
		err := testCase.in.Validate(testTransaction, "FromCustodianCounterpartyId", 0)
		assert.Equal(t, testCase.out, err)
	}
}

func TestCustodianCounterpartyIdDoesNotExist(t *testing.T) {
	var testTransaction Transaction
	unmarhsalError := json.Unmarshal([]byte(custodianTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalError)
	str := "{\"data\": {\"custodianAccounts\": []}}"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()

	err := testTransaction.Allocations[0].FromCustodianCounterpartyAccountID.Validate(testTransaction, "FromCustodianCounterpartyId", 0)
	assert.Equal(t, err, errFromCustodian)

}

func TestCustodianCounterpartyIdMissingToCustodianAccountId(t *testing.T) {
	var testTransaction Transaction
	unmarhsalError := json.Unmarshal([]byte(custodianTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalError)
	str := "{\"data\": {\"custodianAccounts\": [{\"id\": 4, \"name\": \"GSCOa\"}]}}"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()

	for _, testCase := range testCasesCustodian {
		err := testCase.in.Validate(testTransaction, "ToCustodianCounterpartyId", 0)
		assert.Equal(t, fmt.Errorf("Invalid ToCustodianCounterpartyId : Data not provided"), err)
	}
}

func TestCustodianCounterpartyIdMissingFromCustodianAccountId(t *testing.T) {
	var testTransaction Transaction
	unmarhsalError := json.Unmarshal([]byte(custodianTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalError)
	str := "{\"data\": {\"custodianAccounts\": [{\"id\": 4, \"name\": \"GSCOa\"}]}}"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()

	for _, testCase := range testCasesCustodian {
		err := testCase.in.Validate(testTransaction, "FromCustodianCounterpartyId", 1)
		assert.Equal(t, fmt.Errorf("Invalid FromCustodianCounterpartyId : Data not provided"), err)
	}
}

func TestCustodianCounterpartyIdInvalidBookType(t *testing.T) {
	var testTransaction Transaction
	unmarhsalErr := json.Unmarshal([]byte(custodianTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalErr)
	str := "{\"data\": {\"custodianAccounts\": [{\"id\": 4, \"name\": \"GSCOa\"}]}}"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL
	defer ts.Close()

	for _, testCase := range testCasesCustodian {
		err := testCase.in.Validate(testTransaction, "ToCustodianCounterpartyId", 1)
		assert.Equal(t, fmt.Errorf("Invalid %s : Expected BookType %d, Provided BookType %d ", "ToCustodianCounterpartyId", BookTypeLocation, int(*testTransaction.Allocations[1].BookTypeID)), err)
	}
}

func TestCustodianCounterpartyIdUnFromCustodianError(t *testing.T) {
	var testTransaction Transaction
	unmarhsalError := json.Unmarshal([]byte(custodianTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalError)
	str := "[{\"Id\":33},{\"Id\":27]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()

	for _, testCase := range testCasesCustodian {
		err := testCase.in.Validate(testTransaction, "FromCustodianCounterpartyId", 0)
		assert.Equal(t, fmt.Errorf("%s : Unmarshalling error", "FromCustodianCounterpartyId"), err)
	}
}

func TestCustodianCounterpartyIdInvalidLookup(t *testing.T) {
	var testTransaction Transaction
	unmarhsalErr := json.Unmarshal([]byte(custodianTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalErr)
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadGateway)
	}))
	appconfig.BaseURI = ts.URL
	defer ts.Close()

	for _, testCase := range testCasesCustodian {
		err := testCase.in.Validate(testTransaction, "FromCustodianCounterpartyId", 0)
		assert.Equal(t, fmt.Errorf("FromCustodianCounterpartyId : Service lookup failed"), err)
	}
}

func TestCustodianCounterpartyIdInvalidEventType(t *testing.T) {
	var testTransaction Transaction
	unmarhsalError := json.Unmarshal([]byte(custodianInvalidEventTypeTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalError)
	str := "{\"data\": {\"custodianAccounts\": [{\"id\": 4, \"name\": \"GSCOa\"}]}}"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()

	for _, testCase := range testCasesCustodian {
		err := testCase.in.Validate(testTransaction, "FromCustodianCounterpartyId", 0)
		assert.Equal(t, fmt.Errorf("FromCustodianCounterpartyId : Field not valid for EventTypeID 1013"), err)
	}
}
