package models

import (
	"fmt"
	"time"
)

// TransactionDateTimeUTC type
type TransactionDateTimeUTC string

// Validate validates the fields
func (value TransactionDateTimeUTC) Validate(transaction Transaction, fieldName string) error {
	timeZoneResponse, err := NewTimezoneRequest(int(transaction.TransactionTimeZoneID))
	if err == nil {
		transactionDateTimeUTC, err := time.Parse(time.RFC3339Nano, string(value))
		currentDateUTC := time.Now().UTC()
		if err == nil {
			transactionDateTimeUTC := transactionDateTimeUTC.Add(-time.Minute * time.Duration(timeZoneResponse.BaseUtcOffset))
			businessYear, businessMonth, businessDay := transactionDateTimeUTC.Date()
			currentYear, currentMonth, currentDay := currentDateUTC.Date()
			if businessYear == currentYear && businessMonth == currentMonth && businessDay == currentDay {
				return nil
			}
			return fmt.Errorf("Invalid %s. TransactionDate must match current date. Current Date: %s, Provided TransactionDate: %s", fieldName, currentDateUTC.Format(time.RFC3339)[:10], transactionDateTimeUTC.Format(time.RFC3339)[:10])
		}
		return fmt.Errorf("Invalid %s. Incorrect format", fieldName)
	}
	return fmt.Errorf("%s : Service lookup failed", fieldName)
}
