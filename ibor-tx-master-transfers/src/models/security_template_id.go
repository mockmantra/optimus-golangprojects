package models

import (
	"fmt"
	"strings"
)

// SecurityTemplateID field

type SecurityTemplateID int

// Validate validates the field
func (value SecurityTemplateID) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	securities, err := NewSecurityRequest(int(*(transaction.Allocations[allocationIndex].SecurityID)))
	if err != nil {
		if strings.Contains(err.Error(), "error:404") {
			return fmt.Errorf("Invalid %s. SecurityTemplateId: %d does not exist", fieldName, value)
		}
		return fmt.Errorf("Invalid %s. Service lookup failed", fieldName)
	} else {
		if securities[0].SecurityTemplateID != int(value) {
			return fmt.Errorf("Invalid %s. SecurityTemplateId: %d does not match with template for SecurityID : %d", fieldName, value, securities[0].SecurityID)
		}
	}
	return nil
}
