package validator

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/httputil"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/mocks"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/models"
)

var validatorValidTestJSON = `{
	"SecurityID": 1,
	"EventTypeId": 1013
  }`

var validatorPanicTestJSON = `{
	"SystemCurrencySecurityID": 1234,
	"EventTypeId": 1013
  }`
var validatorPanicAllocationTestJSON = `{
	"Allocations": [
		{
			"BookTypeId": 4,
			"FromPrtBaseCurrencySecurityId" :1
		}
	],
	"SystemCurrencySecurityID": 1234
  }`

type allMocks struct {
	logger mocks.Logger
}

// Validate ...

func TestValidateValidData(t *testing.T) {
	var validatorTestData models.Transaction
	httputil.UserSessionToken = "123"
	var allmocks allMocks
	log := &allmocks.logger
	unmarshalErr := json.Unmarshal([]byte(validatorValidTestJSON), &validatorTestData)
	assert.Nil(t, unmarshalErr)
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	}))
	appconfig.BaseURI = ts.URL
	defer ts.Close()

	errors := Validate(validatorTestData, log)
	errs := []ErrorMessage{ErrorMessage{Field: "SideId", Message: []string{"Invalid SideId. Must be 1(Long) or 2(Short)"}}, ErrorMessage{Field: "LogicalTimeId", Message: []string{"Invalid LogicalTimeId. Must be 50(Start Of Day), 100(Trading Day) or 150(End Of Day)"}}, ErrorMessage{Field: "TransactionDateTimeUTC", Message: []string{"TransactionDateTimeUTC : Service lookup failed"}}, ErrorMessage{Field: "SystemCurrencySecurityId", Message: []string{"Invalid SystemCurrencySecurityId. SystemCurrencySecurityId 0  does not exist"}}, ErrorMessage{Field: "TransactionTimeZoneId", Message: []string{"Invalid TransactionTimeZoneId. TimeZoneId: 0 does not exist"}}, ErrorMessage{Field: "SourceSystemName", Message: []string{"SourceSystemName : Service lookup failed"}}}
	assert.ElementsMatch(t, errors, errs)
	httputil.UserSessionToken = ""
}

func TestPanicInAllocationFieldsValidation(t *testing.T) {
	var validatorTestData models.Transaction
	var allmocks allMocks
	log := &allmocks.logger
	httputil.UserSessionToken = "123"
	unmarshalErr := json.Unmarshal([]byte(validatorPanicAllocationTestJSON), &validatorTestData)
	assert.Nil(t, unmarshalErr)
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.Contains(r.URL.Path, "referencedata/v1/Custodian") {
			_, err := w.Write(nil)
			if err != nil {
				t.Error("Unable to write the stream")
			}
		}
		w.WriteHeader(http.StatusNotFound)
	}))
	appconfig.BaseURI = ts.URL
	defer ts.Close()
	log.On("Errorf", mock.Anything, mock.Anything, mock.Anything).Return()
	errors := Validate(validatorTestData, log)
	errs := []ErrorMessage{ErrorMessage{Field: "EventTypeId", Message: []string{"Invalid EventTypeId. Must be 1009(Transfer At Cost) or 1013(Transfer At Price)"}}, ErrorMessage{Field: "BookTypeId", Message: []string{"Invalid BookTypeId : Expected 2 (for Location) or 3 (for Strategy), Provided value 4"}}, ErrorMessage{Field: "LogicalTimeId", Message: []string{"Invalid LogicalTimeId. Must be 50(Start Of Day), 100(Trading Day) or 150(End Of Day)"}}, ErrorMessage{Field: "SideId", Message: []string{"Invalid SideId. Must be 1(Long) or 2(Short)"}}, ErrorMessage{Field: "FromPrtBaseCurrencySecurityId", Message: []string{"Failed to validate field : FromPrtBaseCurrencySecurityId in allocation"}}, ErrorMessage{Field: "TransactionDateTimeUTC", Message: []string{"TransactionDateTimeUTC : Service lookup failed"}}, ErrorMessage{Field: "SystemCurrencySecurityId", Message: []string{"Invalid SystemCurrencySecurityId. SystemCurrencySecurityId 1234  does not exist"}}, ErrorMessage{Field: "TransactionTimeZoneId", Message: []string{"Invalid TransactionTimeZoneId. TimeZoneId: 0 does not exist"}}, ErrorMessage{Field: "SourceSystemName", Message: []string{"SourceSystemName : Service lookup failed"}}}
	assert.ElementsMatch(t, errors, errs)
	log.AssertExpectations(t)
	httputil.UserSessionToken = ""
}

// func TestPanicInMainFieldsValidation(t *testing.T) {
// 	var validatorTestData models.Transaction
// 	var allmocks allMocks
// 	log := &allmocks.logger
// 	httputil.UserSessionToken = "123"
// 	unmarshalErr := json.Unmarshal([]byte(validatorPanicTestJSON), &validatorTestData)
// 	assert.Nil(t, unmarshalErr)
// 	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
// 		_, err := w.Write(nil)
// 		if err != nil {
// 			t.Error("Error in writing to response")
// 		}
// 	}))
// 	appconfig.BaseURI = ts.URL
// 	defer ts.Close()
// 	monkey.Patch(models.NewSecurityRequest, func(securityID int) ([]models.Security, error) {
// 		return []models.Security{}, nil
// 	})
// 	log.On("Errorf", mock.Anything, mock.Anything, mock.Anything).Return()
// 	errors := Validate(validatorTestData, log)
// 	errs := []ErrorMessage{ErrorMessage{Field: "LogicalTimeId", Message: []string{"Invalid LogicalTimeId. Must be 50(Start Of Day), 100(Trading Day) or 150(End Of Day)"}}, ErrorMessage{Field: "SideId", Message: []string{"Invalid SideId. Must be 1(Long) or 2(Short)"}}, ErrorMessage{Field: "SystemCurrencySecurityId", Message: []string{"Failed to validate the field: SystemCurrencySecurityId"}}, ErrorMessage{Field: "TransactionTimeZoneId", Message: []string{"TransactionTimeZoneId: Service lookup failed"}}, ErrorMessage{Field: "TransactionDateTimeUTC", Message: []string{"TransactionDateTimeUTC : Service lookup failed"}}, ErrorMessage{Field: "SourceSystemName", Message: []string{"SourceSystemName : Service lookup failed"}}}
// 	assert.ElementsMatch(t, errors, errs)
// 	httputil.UserSessionToken = ""
// }
