package appconfig

import (
	"context"
	"fmt"
	"os"
	"strings"

	"github.com/gofrs/uuid"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/logger"
)

const (
	// ServiceName is work distribution service name
	ServiceName = "ibor_tx_master_transfers"
	// WorkType is worktype
	WorkType            = "ibor_tx_master_transfers_leader_election"
	rancherHostKey      = "RANCHER_HOST_NAME"
	rancherContainerKey = "RANCHER_CONTAINER_NAME"
	// KinesisShardID is shardId
	KinesisShardID = "shardId-000000000000"
)

var (
	// BaseURI is the base URI of the corresponding IMS environment
	BaseURI string
	// IsDevMode should be set to true when running in dev env
	IsDevMode bool
	// RunEnv gives the current environment
	RunEnv string
	// AwsProxyURL gives the proxy url
	AwsProxyURL string
	// AwsRegionName gives the aws region
	AwsRegionName string
	// AwsDownStreamName is the down stream name
	AwsDownStreamName string
	// AwsUpStreamName is the up stream name
	AwsUpStreamName string
	// AwsAccessKey is aws access key
	AwsAccessKey string
	// AwsSecretKey is aws secret key
	AwsSecretKey string
	// DbSchemaTemplate is aws secret key
	DbSchemaTemplate string
	// DbConnectAttempts is db retry attempts
	DbConnectAttempts int
	// Port is where health endpoint
	Port string
	// TxMasterHost is the rancher host value
	TxMasterHost string
	// TxMasterContainer is the is rancher container value
	TxMasterContainer string
	// TxMasterInstance is the transactionMaster service instance
	TxMasterInstance string
)

func init() {
	var cx context.Context
	loggerWithCtx := logger.Log.WithContext(cx)

	DbSchemaTemplate = "Eclipse_%s"
	DbConnectAttempts = 3

	bu := os.Getenv("IMS_BASE_URL")
	if bu == "" {
		loggerWithCtx.Debugf("Please, set IMS_BASE_URL environment variable")
	}
	BaseURI = bu

	ae := os.Getenv("APP_ENV")
	if strings.EqualFold(ae, "local") {
		loggerWithCtx.Debugf("IN DEV MODE")
		IsDevMode = true
	}
	RunEnv = ae

	AwsRegionName = os.Getenv("AWS_REGION")
	if len(AwsRegionName) == 0 {
		loggerWithCtx.Debugf("AWS_REGION variable is not set")
	}

	AwsProxyURL = os.Getenv("AWS_PROXY_URL")

	AwsDownStreamName = os.Getenv("AWS_DOWN_STREAM_NAME")

	AwsUpStreamName = os.Getenv("AWS_UP_STREAM_NAME")

	AwsAccessKey = os.Getenv("AWS_ACCESS_KEY")

	AwsSecretKey = os.Getenv("AWS_SECRET_KEY")

	Port = os.Getenv("PORT")

	TxMasterHost = os.Getenv(rancherHostKey)
	TxMasterContainer = os.Getenv(rancherContainerKey)
	TxMasterInstance = txMasterInstance()
}

func txMasterInstance() string {
	host, err := os.Hostname()
	if err != nil {
		host = fmt.Sprintf("ibor_tx_master_transfers_%s", uuid.Must(uuid.NewV4()))
	}
	return host
}
