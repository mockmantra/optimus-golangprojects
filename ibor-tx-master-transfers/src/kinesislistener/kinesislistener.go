package kinesislistener

import (
	"context"
	"encoding/json"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/service/kinesis"
	"stash.ezesoft.net/ipc/work-distribution/workdistributionclient"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/handler"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/interfaces"

	tomb "gopkg.in/tomb.v2"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/models"
	"stash.ezesoft.net/ipc/ezeutils"
)

var tmb tomb.Tomb

// Start ...
func Start(assignments chan models.Assignment, k interfaces.IKinesisUtil, logger interfaces.ILogger) {
	go listenToAssignment(assignments, k, logger)
}

func listenToAssignment(assignments chan models.Assignment, k interfaces.IKinesisUtil, logger interfaces.ILogger) {
	select {
	case assignment := <-assignments:
		currSequenceNumber := assignment.SequenceNumber
		latestSequenceNumber := currSequenceNumber
		// if assignment.SequenceNumber is empty GetShardIterator uses "TRIM_HORIZON" to get last Sequence number
		shardIteratorOutput, err := k.GetShardIterator(appconfig.KinesisShardID, appconfig.AwsUpStreamName, currSequenceNumber, logger)
		if err == nil {
			shardIterator := shardIteratorOutput.ShardIterator
			for {
				records, err := k.GetRecords(shardIterator, logger)
				if err == nil {
					recordsLength := len(records.Records)
					if recordsLength > 0 {
						latestSequenceNumber = *records.Records[recordsLength-1].SequenceNumber
						for _, record := range records.Records {
							// process the kinesis records
							logger.Infof("Kinesis message Sequence number: %s", *record.SequenceNumber)
							handler.HandleTransaction(*record, k, logger)
						}
						ctx := ezeutils.WithKeyValue(tmb.Context(context.Background()), ezeutils.UserAgentKey, "ibor_tx_master_transfers/1.0 Go-http-client/1.1")
						// update workitem with latestSequenceNumber
						res, err := workdistributionclient.UpdateWorkItem(ctx, appconfig.WorkType, appconfig.WorkType, models.Assignment{SequenceNumber: latestSequenceNumber})
						if err != nil {
							logger.Errorf("Failed to update sequence number in workItem: %v", err)
						} else {
							jsonResponse, err := json.Marshal(res)
							if err != nil {
								logger.Errorf("Error in parsing UpdateWorkItem response: %v", err)
							} else {
								logger.Infof("Successfully updated sequence number in workItem: %s", string(jsonResponse))
							}
						}
					} else {
						logger.Info("No new records found...")
					}
					shardIterator = records.NextShardIterator
					// Kinesis shard iterator is only valid for fixed duration of time, so refresh it if we run into ErrCodeExpiredIteratorException exception
				} else if strings.HasPrefix(err.Error(), kinesis.ErrCodeExpiredIteratorException) {
					logger.Infof("GetRecords returned ExpiredIteratorException hence refreshing the shardIterator with sequenceNumber: %s", latestSequenceNumber)
					shardIteratorOutput, errGetShard := k.GetShardIterator(appconfig.KinesisShardID, appconfig.AwsUpStreamName, latestSequenceNumber, logger)
					if errGetShard == nil {
						shardIterator = shardIteratorOutput.ShardIterator
					}
				}
				logger.Info("Sleeping for 10 seconds...")
				time.Sleep(time.Duration(time.Second * 10))
			}
		}
	}
}
