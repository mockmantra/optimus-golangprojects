package svcloginconfig

import (
	"fmt"
	"os"
	"time"

	"github.com/pkg/errors"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-transfers/src/logger"
)

const (
	rancherHostKey         = "RANCHER_HOST_NAME"
	rancherContainerKey    = "RANCHER_CONTAINER_NAME"
	baseUrlKey             = "IMS_BASE_URL"
	intiServiceUserKey     = "IMS_INTI_SVC_USER"
	awsRegionKey           = "AWS_REGION"
	consulUrlKey           = "CONSUL_URL"
	tenantSiloDefault      = "TenantSiloBlue"
	consulSettingRoot      = "ims/Settings/Blue/Configuration/"
	EnvironmentNameKey     = "EnvironmentName"
	SharedKeyKey           = "ServiceLoginSharedKey"
	DPermsKeyKey           = "DPermsConfigApplicationKey"
	DPermsVersionKey       = "DPermsConfigApplicationVersion"
	DPermsUriKey           = "DPermsConfigDPermsRestUri"
	ClusterHostSettingsKey = "ClusterHostSettings"
)

var (
	Host                string
	Container           string
	Instance            string
	IntiServiceUser     string
	BaseUrl             string
	AWSRegion           string
	ConsulUrl           string
	LogStatPeriod       = time.Minute * 5
	consulMaxRetries    = 6
	consulRetryDelaySec = time.Second * 5
	ServiceSessionVars  *ServiceSessionConfig
	RestApiAssigments   bool
)

//ClusterHostSettings values is [ { \"Host\": \"aecastle01rmq-elb.awsdev.ezesoftcloud.com\", \"Port\": 5672 } ]
var (
	consulVars map[string]interface{} = map[string]interface{}{
		EnvironmentNameKey:     "",
		SharedKeyKey:           "",
		DPermsKeyKey:           "",
		DPermsVersionKey:       "",
		DPermsUriKey:           "",
		ClusterHostSettingsKey: "",
	}
)

type ServiceSessionConfig struct {
	EnvName       string
	SharedKey     string
	DpermsIdKey   string
	DpermsVersion string
	DpermsUri     string
}

func init() {
	Host = os.Getenv(rancherHostKey)
	Container = os.Getenv(rancherContainerKey)
	Instance, _ = os.Hostname()
	BaseUrl = os.Getenv(baseUrlKey)
	if BaseUrl == "" {
		panic(fmt.Sprintf("environment variable '%s' should be set to non-empty value", baseUrlKey))
	}
	if BaseUrl[len(BaseUrl)-1:] != "/" {
		BaseUrl += "/"
	}
	IntiServiceUser = os.Getenv(intiServiceUserKey)
	if IntiServiceUser == "" {
		panic(fmt.Sprintf("environment variable '%s' should be set to non-empty value", intiServiceUserKey))
	}
	AWSRegion = os.Getenv(awsRegionKey)
	if AWSRegion == "" {
		panic(fmt.Sprintf("environment variable '%s' should be set to non-empty value", awsRegionKey))
	}

	// this should be set
	ConsulUrl = os.Getenv(consulUrlKey)
	if ConsulUrl == "" {
		ConsulUrl = "http://consul-agent:8500"

	}
	// RestApiAssigments, _ = strconv.ParseBool(os.Getenv(restApiAssignmentsEnvKey))

	// we don't set these...so it uses GetServiceSessionConfig() to fetch them
	envName := os.Getenv("CL_ENVIRONMENT_NAME")
	sharedKey := os.Getenv("CL_SHAREDKEY")
	dpermsAppKey := os.Getenv("CL_DPERMSAPPKEY")
	dpermsAppVersion := os.Getenv("CL_DPERMSAPPVERSION")
	dpermsRestUri := os.Getenv("CL_DPERMSRESTURI")

	if envName == "" || sharedKey == "" || dpermsAppKey == "" ||
		dpermsAppVersion == "" || dpermsRestUri == "" {
		loadConsulVars(consulVars)
		var err error
		if err != nil {
			panic(fmt.Sprintf("failed to get rmq config from consul %v", err))
		}
		ServiceSessionVars, err = GetServiceSessionConfig()
		if err != nil {
			panic(fmt.Sprintf("failed to get service session config from consul %v", err))
		}
	} else {
		ServiceSessionVars = &ServiceSessionConfig{
			EnvName:       envName,
			SharedKey:     sharedKey,
			DpermsIdKey:   dpermsAppKey,
			DpermsVersion: dpermsAppVersion,
			DpermsUri:     dpermsRestUri,
		}
	}
}

func loadConsulVars(consulMap map[string]interface{}) {
	var err error
	for i := 0; i <= consulMaxRetries; i++ {
		err = updateConsulSettings(consulMap)
		if err == nil {
			break
		} else {
			logger.Log.Warnf("Failed reading consul variables, retrying %v", i+1)
		}
		<-time.After(consulRetryDelaySec)
	}
	if err != nil {
		panic(fmt.Sprintf("Failed reading consul variables %v %v", ConsulUrl, err.Error()))
	}
}

func GetConsulValue(key string) (interface{}, error) {
	setting, ok := consulVars[key]
	if !ok {
		return "", errors.Errorf("did not find consul setting for %s", key)
	}
	return setting, nil
}

func GetConsulValueStr(key string) (string, error) {
	setting, ok := consulVars[key]
	if !ok {
		return "", errors.Errorf("did not find consul setting for %s", key)
	}
	val, ok := setting.(string)
	if !ok {
		return "", errors.Errorf("invalid interface conversion value for consul key %s", key)
	}
	return trimQuotes(val), nil
}

func GetServiceSessionConfig() (*ServiceSessionConfig, error) {

	envName, err := GetConsulValueStr(EnvironmentNameKey)
	if err != nil {
		return nil, err
	}
	if envName == "" {
		return nil, errors.Errorf("empty value found for consul key %s", EnvironmentNameKey)
	}
	sharedKey, err := GetConsulValueStr(SharedKeyKey)
	if err != nil {
		return nil, err
	}
	if sharedKey == "" {
		return nil, errors.Errorf("empty value found for consul key %s", SharedKeyKey)
	}
	dpermsKey, err := GetConsulValueStr(DPermsKeyKey)
	if err != nil {
		return nil, err
	}
	if dpermsKey == "" {
		return nil, errors.Errorf("empty value found for consul key %s", DPermsKeyKey)
	}
	dpermsVersion, err := GetConsulValueStr(DPermsVersionKey)
	if err != nil {
		return nil, err
	}
	if dpermsVersion == "" {
		return nil, errors.Errorf("empty value found for consul key %s", DPermsVersionKey)
	}
	dpermsUri, err := GetConsulValueStr(DPermsUriKey)
	if err != nil {
		return nil, err
	}
	if dpermsUri == "" {
		return nil, errors.Errorf("empty value found for consul key %s", DPermsUriKey)
	}
	return &ServiceSessionConfig{EnvName: envName, SharedKey: sharedKey, DpermsIdKey: dpermsKey, DpermsVersion: dpermsVersion,
		DpermsUri: dpermsUri}, nil
}
