# IBOR_TX_MASTER_TRANSFERS

It's a back-end service which validates the Transfers events. (Equity events passed from Transfers endpoint of ibor-transaction-api swagger page)

## Installation

* Go Programming Language
  should be installed in the machine, if not it can be downloaded from https://golang.org/dl/
* Install [dep](https://golang.github.io/dep/docs/installation.html), a dependency management tool for Go.

* Install and run `localstack` using below command.
    ```bash
    docker run -it -p 4567-4578-4568:4567-4578-4568 -p 8080:8080 localstack/localstack
    ```
    ##### AWS CLI installation

    Link for installation of aws cli with was

    https://docs.aws.amazon.com/streams/latest/dev/kinesis-tutorial-cli-installation.html

    please run below command so as to create streams in kinesis
    ```bash
    Creation of upstream:
    aws --endpoint-url=http://localhost:4568 kinesis create-stream --stream-name CastleAccountingTxTransfers01 --shard-count 1

    Creation of downstream:
    aws --endpoint-url=http://localhost:4568 kinesis create-stream --stream-name CastleAccountingTxTransfersIntraday01 --shard-count 1
    ```
* Install and run mysql. Update your credentials in launch.json before spinnig this service up.
  Link for installation

  https://dev.mysql.com/downloads/installer/

## Steps to debug in local

* Comment out the below line in main.go
```bash
    kinesislistener.Start(assignments, k, logger.Log)
	//workdistro.ListenWDAssignments(assignments, logger.Log)
	defer close(assignments)
```

* Comment out the below mentioned lines in listenToAssignment method of kinesislistener.go class
```bash
//select {
	//case assignment := <-assignments:
		//currSequenceNumber := assignment.SequenceNumber
        currSequenceNumber := ""
```
* In transactionhandler.go, replace the parameter with any of the firmname.
```bash
tokenErr := servicesession.SetUserSessionToken(streamMessage.FirmAuthToken)
tokenErr := servicesession.SetUserSessionToken("T71Q5")
```

### Tests
* Run all tests in the project.
    ```bash
    make test
    ```
*  Run all tests with coverage in terminal.
    ```bash
    make test_cover
    ```
*  Run all tests with coverage in browser.
    ```bash
    make test_cover_html
    ```
* If you want to run/debug a test with go extension(test At Cursor) in vscode, keep below setting at `.vscode/settings.json`
    ```
    {
        "go.testEnvVars":{ "APP_ENV": "local"}
    }
    ```

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## History

TODO: Write history

## Credits

TODO: Write credits

## License

TODO: Copyright (c) 2015 Eze Software Group.   All rights reserved.