# Project Name
Transaction Master Transfers Stack Project

## Usage

This repository contains the files that are required to build the stacks in Ranchers for ``` https://stash.ezesoft.net/projects/IMSACNT/repos/ibor-tx-master-transfers/browse```.

docker-compose.yml and rancher-compose.yml are for defining and running multi-container Docker applications.
YAML file are used to configure application's services.
The files decides the behaviour of Docker containers in castle/production.

dockerfile is used to define app's environment so it can be reproduced anywhere.

docker-compose.yml define the services that builds the application so that they can be run together in an isolated environment.

rancher-compose.yml defines the scale of services, health checks and port mapping.

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## Credits

Team Invictus

## License

TODO: Copyright (c) 2015 Eze Software Group.   All rights reserved.