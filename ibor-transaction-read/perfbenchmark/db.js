const request = require('request');


var pushData = (actBody, callback) => {
    request.post({
        url: 'http://imsperformance.qalab.net:8086/write?db=PerfCI',
        body: actBody,
        json: false
    }, (error, response, body) => {

        if (error) {
            callback('Unable to connect to server');
        } else {
            callback(undefined, "Data Posted to Influx" + JSON.stringify(response));
        }
    });
};

module.exports.pushData = pushData;