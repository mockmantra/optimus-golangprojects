const platform = require("./platform");
const settings = require("./settings");
const express = require("express");

const router = express.Router();


function wire(apiModule, app){
    Object.keys(apiModule).forEach(key => {
        let apiDef = apiModule[key]
        app.use(apiDef.route, (req,res,next) => {
            switch(req.method) {
                case "GET":
                    if(apiDef.get){
                        apiDef.get(req, res, next);
                        break;
                    }
                case "POST": 
                    if(apiDef.post) {
                        apiDef.post(req, res, next);
                        break;
                    }
                case "PUT": 
                    if(apiDef.put) {
                        apiDef.put(req, res, next);
                        break;
                    }
                case "HEAD": 
                    if(apiDef.head){
                        apiDef.head(req, res, next);
                        break;
                    }
                case "DELETE": 
                    if(apiDef.delete){
                        apiDef.delete(req, res, next);
                        break;
                    }
                default:
                    res.status(501).json("not supported");
            }

            
        })
        
    })
}

module.exports = function (app) {
    wire(platform, app);
    wire(settings, app);
}