const alertApi = {
    route: "/api/platform/v1/alert",
    get: (req, res, next) => {
        res.status(200).json([]);
    }
}

const health = {
    route: "/api/platform/v1/health",
    get: (req, res, next) => {
        res.status(200).json("Ok");
    }
}



module.exports = {
    alertApi,
    health
}