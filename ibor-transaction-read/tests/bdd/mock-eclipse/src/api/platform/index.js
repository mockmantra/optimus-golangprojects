const common = require("./common");
const session = require("./session");
const eventTypes = require("./eventTypes");
const graphql = require("./graphql");

let mergedApiDefs = {};
Object.assign(mergedApiDefs, common, session,eventTypes,graphql);

module.exports = mergedApiDefs;