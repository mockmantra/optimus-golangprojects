'use strict';

const got = require('got');
const config = require('../config');


async function Get(url, data=null, token=null) {
    try{
        let localToken = token != null ? token : config.authToken;
        const response = await got.get(config.baseUrl , {
            headers: {
                Authorization: 'Basic ' + Buffer.from(localToken+':').toString('base64'), //should end with :
                Accept: 'application/json'
            },
            json: true
        });
        return response;
    }
    catch(err){
        throw new Error('Failure attempting to get from ' + url +'. Error = ' + err);
    }
}

async function GetAsString(url, data=null, token=null) {
    try{
        let localToken = token != null ? token : config.authToken;
        const response = await got.get(config.baseUrl + url, {
            headers: {
                Authorization: 'Basic ' + Buffer.from(localToken+':').toString('base64'), //should end with :
                Accept: 'application/json'
            },
            json: false
        });
        return response;
    }
    catch(err){
        throw new Error('Failure attempting to get from ' + url +'. Error = ' + err);
    }
}

async function Post(url, body=null, activityId=null, token=null){
    try {
        let localToken = token != null ? token : config.authToken;
        var headers = {
            Authorization: 'Basic ' + Buffer.from(localToken+':').toString('base64'), //should end with :
            Accept: 'application/json'
        }
        if (activityId) {
            headers['x-request-id'] = activityId;
        }

        const response = await got.post(config.baseUrl + url, {
            method: 'POST',
            headers: headers,
            json: true,
            body: body
        });
         
        return response;
    }
    catch(err){
        console.log('Failure to Post to ' + url + '. Error = ' + err);
        throw err;
    }
}

async function PostWithoutThrowingError(url, body=null, activityId=null, token=null){
    try{
        return await Post(url, body, activityId, token);
    }
    catch(err){
       return err;
    }
}

module.exports= {Get, GetAsString, Post, PostWithoutThrowingError};