set -e

if [ "$#" -eq "1" ]; then
    echo "Executing specific BDD(s): " $1
    ./node_modules/.bin/cucumber-js $1 --format "./features/support/pretty_formatter.js"

else
    echo "Triggering all BDDs"
    ./node_modules/.bin/cucumber-js --format "./features/support/pretty_formatter.js"
fi



