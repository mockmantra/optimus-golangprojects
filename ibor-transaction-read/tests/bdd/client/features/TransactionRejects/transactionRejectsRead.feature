 Feature: Transaction Rejects

    Scenario Outline: Check if Read API is returning results when all fields are passed

        Given feature folder is TransactionRejects
        And the test case being run is <file>
        When make a Transaction Rejects request
        Then the status code should be 200
        And The transaction results should match the expected response

        Examples:
            | file                        |
            | ReadAPI_AllFeilds.json      |
            | ReadAPI_AllTxnTypes.json    |
            | ReadAPI_NoData.json         |
            | ReadAPI_DateRange.json      |
             | ReadAPI_MultipleData.json  |
            | ReadAPI_Singleerror.json    |

    Scenario Outline: Check if Read API is returning the proper error when invalid data is passed
        Given feature folder is TransactionRejects
        And the test case being run is <file>
        When make a Transaction Rejects request
        Then the status code should be 400
        And The transaction results should match the expected response

        Examples:
            | file                                     |
            | ReadAPI_Invalid.json                     |
            | ReadAPI_StartDateGreaterthanEndDate.json |

    Scenario Outline: Validating the 500 error
        Given feature folder is TransactionRejects
        And the test case being run is <file>
        When make a Transaction Rejects request
        Then the status code should be 500
        And The transaction results should match the expected response
        Examples:
            | file                     |
            | ReadAPI_Returns_500.json |
