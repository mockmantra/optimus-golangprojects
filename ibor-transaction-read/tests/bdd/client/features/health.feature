Feature: Health tests

    Scenario: Get Health stats
        When I send a GET request to /api/ibor/transaction-read/v1/health
        Then I should get a 200 response