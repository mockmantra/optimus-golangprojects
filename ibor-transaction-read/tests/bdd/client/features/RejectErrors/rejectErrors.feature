Feature: Reject Errors

    Scenario Outline: Check if RejectErrors API is returning results when all fields are passed

        Given feature folder is RejectErrors
        And the test case being run is <file>
        When make a Reject Errors request
        Then the status code should be 200
        And The transaction results should match the expected response

        Examples:
            | file             |
            | ValidData.json   |
            | invalidData.json |

    Scenario Outline: Bad Requests
        Given feature folder is RejectErrors
        And the test case being run is <file>
        When make a Reject Errors request
        Then the status code should be 400
        Examples:
            | file                     |
            | invalidTxnType_400.json  |
            | txnTypeAbsent_400.json   |