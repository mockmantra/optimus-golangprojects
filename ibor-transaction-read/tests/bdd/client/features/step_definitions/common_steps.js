/*
Any common steps will be placed here for use and 
chai will be used for assertions
*/
'use strict';

const { defineSupportCode } = require('cucumber')
const chai = require('chai');
const chaiExclude = require('chai-exclude');
chai.use(chaiExclude);
var { setDefaultTimeout } = require('cucumber');
const _ = require('lodash');
const fs = require('fs-extra');
const path = require('path');
var globParent = require('glob-parent');
setDefaultTimeout(120 * 1000);


defineSupportCode(function (self) {
    let Given = self.Given;
    let When = self.When;
    let Then = self.Then;

    Given(/^a request to (.*)$/, function (endpoint) {
        this.httpService.setDefaults(endpoint);
    });

    When(/^I send a GET request to (.*)$/, async function (path) {
        this.serviceResponse = await this.httpService.get(path);

    });

    When(/^I make a GET request to (.*) with the JSON:$/, async function (path, body) {

        var exfilename = ((JSON.parse(body)).name);
        var fs = require("fs");
        var relativeFileName = __dirname + '/../JSONs/' + exfilename;
        var config = fs.readFileSync(relativeFileName, "utf8");

        console.log("COMING FROM TEXT FILE" + config);
        this.response = await this.httpService.get(path, config);
        var sleep = require('system-sleep');
        sleep(10 * 1000); // sleep for 30 seconds

    });



    Then(/^I should get a (\d{3}) response$/, function (responseCode) {
        chai.expect(this.serviceResponse.statusCode).to.equal(parseInt(responseCode));
    });
    Given(/feature folder is (.*)/, function (folderName) {
        this.folderName = folderName;
    });

    Given(/the test case being run is (.*)/, function (testFile) {
        const parentPath = globParent(__dirname)
        let file = parentPath + path.sep + this.folderName + '/specs/' + testFile;
        var data = fs.readJsonSync(file);
        this.request = data.request;
        this.expectedResponse = data.response;
    });
    When(/make a Transaction Rejects request/, async function () {

        this.serviceResponse = await this.httpService.get("/api/ibor/transaction-read/v1/transaction-rejects", this.request);

    });
    When(/make a Reject Errors request/, async function () {

        this.serviceResponse = await this.httpService.get("/api/ibor/transaction-read/v1/reject-errors", this.request);

    });
    // part of change

    Then(/the status code should be (\d+)/, function (statusCode) {
        chai.expect(this.serviceResponse.statusCode).to.equal(statusCode);
    });

    Then(/the status code should not be (\d+)/, function (statusCode) {
        chai.expect(this.serviceResponse.statusCode).to.not.equal(statusCode);
    });




    Then(/The transaction results should match the expected response/, function () {

        let serviceResult = this.serviceResponse.body;
        let areEqual = true;

        if (serviceResult.length !== JSON.stringify(this.expectedResponse).length) {
            areEqual = false;
        }

        var isArrayEqual = function(x, y) {
            return _(x).differenceWith(y, _.isEqual).isEmpty();
          };

        var comparedResult = isArrayEqual(JSON.parse(serviceResult), this.expectedResponse);
        if (!areEqual || ! comparedResult) {
            console.log("actual result", serviceResult);
            console.log("expected result", JSON.stringify(this.expectedResponse));
        }
        chai.expect(areEqual).to.be.true;
        chai.expect(comparedResult).to.be.true;
    });
});
