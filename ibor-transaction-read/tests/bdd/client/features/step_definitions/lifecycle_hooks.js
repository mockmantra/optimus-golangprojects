'use strict';
const Session = require('./../support/services/session');
const HttpService = require('./../support/services/http');
const { defineSupportCode } = require('cucumber');

defineSupportCode(function({BeforeAll, Before, After, AfterAll}) {
    let headerToken;
    console.log("INSIDE BEFORECALL -----------");
    BeforeAll(async function() {
        const authorize = async (session) => {
            const authToken = await session.login();
            const auth = new Buffer(authToken + ':').toString('base64');
            return `Basic ${auth}`;
        }
        
        if(process.env.NODE_ENV && process.env.NODE_ENV.trim() === 'testing') {
            require('dotenv').config()
        }
         const apiRoot_Auth = process.env.Auth_API_ROOT;
        const user = process.env.USER;
        const password = process.env.PASSWORD;
        
        const firm = process.env.FIRM;

        
        const session = new Session(apiRoot_Auth, user, password, firm)



        headerToken = await authorize(session);
    });

    Before(function() {
        this.httpService = new HttpService(headerToken, process.env.API_ROOT);
    });
});