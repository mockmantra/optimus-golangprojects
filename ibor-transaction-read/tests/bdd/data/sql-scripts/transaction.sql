
DROP DATABASE IF EXISTS testDb;
create database testDb;
use testDb;
DROP TABLE IF EXISTS Ibor_Transactions;
DROP TABLE IF EXISTS Ibor_Fx_Transactions;
DROP TABLE IF EXISTS Ibor_ValidationErrors;


CREATE TABLE IF NOT EXISTS Ibor_Transactions(
    RecordID            int NOT NULL AUTO_INCREMENT,
    MessageBody         text NOT NULL,
    BusinessDateTimeUTC datetime(6) NOT NULL,
    IsDeleted           boolean NOT NULL default FALSE,
    ActivityID          varchar(100) NOT NULL,
    ValidFromUTC		datetime(6) NOT NULL,
    ValidToUTC			datetime(6) default null ,
    UserID              int NOT NULL,
    UserName            varchar(250) NOT NULL,
    UserSessionToken    varchar(250) NOT NULL,
    EventTypeID         int NOT NULL,
    OrderQuantity       int NOT NULL,
    SecurityID          int NOT NULL,
    SourceSystemName    varchar(100) NOT NULL,
    SourceSystemReference varchar(200) NOT NULL,
    PRIMARY KEY (RecordID)
);

CREATE TABLE IF NOT EXISTS Ibor_Fx_Transactions (
    RecordID            int NOT NULL AUTO_INCREMENT,
    MessageBody         text NOT NULL,
    BusinessDateTimeUTC datetime(6) NOT NULL,
    IsDeleted           boolean NOT NULL default FALSE,
    ActivityID          varchar(100) NOT NULL,
    ValidFromUTC		datetime(6) NOT NULL,
    ValidToUTC			datetime(6) default null ,
    UserID              int NOT NULL,
    UserName            varchar(250) NOT NULL,
    UserSessionToken    varchar(250) NOT NULL,
    EventTypeID         int NOT NULL,
    OrderQuantity       int NOT NULL,
    SecurityID          int NOT NULL,
    SourceSystemName    varchar(100) NOT NULL,
    SourceSystemReference varchar(200) NOT NULL,
    PRIMARY KEY (RecordID)
);

CREATE TABLE IF NOT EXISTS Ibor_ValidationErrors (
  ID int NOT NULL AUTO_INCREMENT,
  TransactionRecordID int NOT NULL,
  TransactionType varchar(100) NOT NULL,
  ErrorMessage varchar(250) NOT NULL,
  PRIMARY KEY (ID)
);

INSERT INTO testDb.Ibor_Transactions
(UserName,
SourceSystemName,
SourceSystemReference,
UserID,
OrderQuantity,
MessageBody,
ValidToUTC,
ActivityID,
SecurityID,
EventTypeID,
IsDeleted,
BusinessDateTimeUTC,
ValidFromUTC,
UserSessionToken)
VALUES
("test",
"trading",
"400",
11,
15,
'\{"Allocations":[{"BaseCurrencySecurityId":155,"BaseToLocalFxRate":1,"BookTypeId":1,"CommissionCharges":null,"CustodianCounterpartyAccountId":10,"FeeCharges":[],"PortfolioId":21,"PositionTags":[{"IndexAttributeId":1,"IndexAttributeValueId":3}],"Quantity":400},{"BaseCurrencySecurityId":155,"BaseToLocalFxRate":1,"BookTypeId":2,"CommissionCharges":[],"CustodianCounterpartyAccountId":10,"FeeCharges":[],"PortfolioId":21,"PositionTags":[],"Quantity":400},{"BaseCurrencySecurityId":155,"BaseToLocalFxRate":1,"BookTypeId":3,"CommissionCharges":null,"FeeCharges":null,"PortfolioId":21,"PositionTags":[{"IndexAttributeId":1,"IndexAttributeValueId":3}],"Quantity":400},{"BaseCurrencySecurityId":155,"BaseToLocalFxRate":1,"BookTypeId":1,"Commission":10,"CommissionCharges":[{"CommissionCharge":10,"CommissionChargeId":22},{"CommissionCharge":0,"CommissionChargeId":24}],"CustodianCounterpartyAccountId":10,"ExecQuantity":100,"FeeCharges":[{"FeeCharge":0.6555,"FeeChargeId":27},{"FeeCharge":0,"FeeChargeId":28},{"FeeCharge":0.7867,"FeeChargeId":29},{"FeeCharge":0.9834,"FeeChargeId":30},{"FeeCharge":19.668,"FeeChargeId":31},{"FeeCharge":25,"FeeChargeId":32},{"FeeCharge":15,"FeeChargeId":33},{"FeeCharge":98.34,"FeeChargeId":34}],"Fees":160.4336,"NetAmount":19838.4336,"PortfolioId":21,"PositionTags":[{"IndexAttributeId":1,"IndexAttributeValueId":3}],"PrincipalAmount":19668,"Quantity":200,"RouteId":1621,"RouteName":"MANUAL","SettleAmount":19838.4336,"SettleCurrencySecurityId":155,"SettlePrice":196.68,"SettleToBaseFXRate":1,"SettleToLocalFXRate":1,"SettleToSystemFXRate":1},{"BaseCurrencySecurityId":155,"BaseToLocalFxRate":1,"BookTypeId":2,"Commission":10,"CommissionCharges":[{"CommissionCharge":10,"CommissionChargeId":22},{"CommissionCharge":0,"CommissionChargeId":24}],"CustodianCounterpartyAccountId":10,"ExecQuantity":100,"FeeCharges":[{"FeeCharge":0.6555,"FeeChargeId":27},{"FeeCharge":0,"FeeChargeId":28},{"FeeCharge":0.7867,"FeeChargeId":29},{"FeeCharge":0.9834,"FeeChargeId":30},{"FeeCharge":19.668,"FeeChargeId":31},{"FeeCharge":25,"FeeChargeId":32},{"FeeCharge":15,"FeeChargeId":33},{"FeeCharge":98.34,"FeeChargeId":34}],"Fees":160.4336,"NetAmount":19838.4336,"PortfolioId":21,"PositionTags":[],"PrincipalAmount":19668,"Quantity":200,"RouteId":1621,"RouteName":"MANUAL","SettleAmount":19838.4336,"SettleCurrencySecurityId":155,"SettlePrice":196.68,"SettleToBaseFXRate":1,"SettleToLocalFXRate":1,"SettleToSystemFXRate":1},{"BaseCurrencySecurityId":155,"BaseToLocalFxRate":1,"BookTypeId":3,"Commission":10,"CommissionCharges":[{"CommissionCharge":10,"CommissionChargeId":22},{"CommissionCharge":0,"CommissionChargeId":24}],"ExecQuantity":100,"FeeCharges":[{"FeeCharge":0.6555,"FeeChargeId":27},{"FeeCharge":0,"FeeChargeId":28},{"FeeCharge":0.7867,"FeeChargeId":29},{"FeeCharge":0.9834,"FeeChargeId":30},{"FeeCharge":19.668,"FeeChargeId":31},{"FeeCharge":25,"FeeChargeId":32},{"FeeCharge":15,"FeeChargeId":33},{"FeeCharge":98.34,"FeeChargeId":34}],"Fees":160.4336,"NetAmount":19838.4336,"PortfolioId":21,"PositionTags":[{"IndexAttributeId":1,"IndexAttributeValueId":3}],"PrincipalAmount":19668,"Quantity":200,"RouteId":1621,"SettleAmount":19838.4336,"SettleCurrencySecurityId":155,"SettlePrice":196.68,"SettleToBaseFXRate":1,"SettleToLocalFXRate":1,"SettleToSystemFXRate":1}],"BusinessDateTimeUTC":"2018-09-08T13:25:43.0000000Z","EventTypeId":14,"IsFinalized":true,"LocalCurrencySecurityId":155,"OrderCreatedDate":"2018-09-08T13:25:43.0000000Z","OrderQuantity":600,"SecurityId":375,"SecurityTemplateId":1,"SequenceNumber":63,"SettleDate":"2018-09-10T13:25:43Z","SourceSystemName":"Trading","SourceSystemReference":"4803","SystemCurrencySecurityId":155,"SystemToLocalFXRate":1,"TradeDate":"2018-09-08T13:25:43Z","TransactionDateTimeUTC":"2018-09-08T13:25:43.0000000Z","TransactionTimeZoneId":1,"UserTimeZoneId":12}',
'2018-10-07 07:40:51',
'87fcef07-8672-4c44-a3b7-8bb7f6715de4',
373,
13,
0,
'2018-10-05 07:40:51',
'2018-10-03 07:40:51',
'good-glx2-token-hansolo');

INSERT INTO testDb.Ibor_Transactions
(UserName,
SourceSystemName,
SourceSystemReference,
UserID,
OrderQuantity,
MessageBody,
ValidToUTC,
ActivityID,
SecurityID,
EventTypeID,
IsDeleted,
BusinessDateTimeUTC,
ValidFromUTC,
UserSessionToken)
VALUES
("test",
"Recon",
"400",
12,
150,
'{ "Allocations": [ { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 1, "CustodianCounterpartyAccountId": 10, "FeeCharges": [], "PortfolioId": 21, "PositionTags": [ { "IndexAttributeId": 1, "IndexAttributeValueId": 3 } ], "Quantity": 400 }, { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 2, "CommissionCharges": [], "CustodianCounterpartyAccountId": 10, "FeeCharges": [], "PortfolioId": 21, "PositionTags": [], "Quantity": 400 }, { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 3, "PortfolioId": 21, "PositionTags": [ { "IndexAttributeId": 1, "IndexAttributeValueId": 3 } ], "Quantity": 400 }, { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 1, "Commission": 10, "CommissionCharges": [ { "CommissionCharge": 10, "CommissionChargeId": 22 }, { "CommissionCharge": 0, "CommissionChargeId": 24 } ], "CustodianCounterpartyAccountId": 10, "ExecQuantity": 100, "FeeCharges": [ { "FeeCharge": 0.6555, "FeeChargeId": 27 }, { "FeeCharge": 0, "FeeChargeId": 28 }, { "FeeCharge": 0.7867, "FeeChargeId": 29 }, { "FeeCharge": 0.9834, "FeeChargeId": 30 }, { "FeeCharge": 19.668, "FeeChargeId": 31 }, { "FeeCharge": 25, "FeeChargeId": 32 }, { "FeeCharge": 15, "FeeChargeId": 33 }, { "FeeCharge": 98.34, "FeeChargeId": 34 } ], "Fees": 160.4336, "NetAmount": 19838.4336, "PortfolioId": 21, "PositionTags": [ { "IndexAttributeId": 1, "IndexAttributeValueId": 3 } ], "PrincipalAmount": 19668, "Quantity": 200, "RouteId": 1621, "RouteName": "MANUAL", "SettleAmount": 19838.4336, "SettleCurrencySecurityId": 155, "SettlePrice": 196.68, "SettleToBaseFXRate": 1, "SettleToLocalFXRate": 1, "SettleToSystemFXRate": 1 }, { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 2, "Commission": 10, "CommissionCharges": [ { "CommissionCharge": 10, "CommissionChargeId": 22 }, { "CommissionCharge": 0, "CommissionChargeId": 24 } ], "CustodianCounterpartyAccountId": 10, "ExecQuantity": 100, "FeeCharges": [ { "FeeCharge": 0.6555, "FeeChargeId": 27 }, { "FeeCharge": 0, "FeeChargeId": 28 }, { "FeeCharge": 0.7867, "FeeChargeId": 29 }, { "FeeCharge": 0.9834, "FeeChargeId": 30 }, { "FeeCharge": 19.668, "FeeChargeId": 31 }, { "FeeCharge": 25, "FeeChargeId": 32 }, { "FeeCharge": 15, "FeeChargeId": 33 }, { "FeeCharge": 98.34, "FeeChargeId": 34 } ], "Fees": 160.4336, "NetAmount": 19838.4336, "PortfolioId": 21, "PositionTags": [], "PrincipalAmount": 19668, "Quantity": 200, "RouteId": 1621, "RouteName": "MANUAL", "SettleAmount": 19838.4336, "SettleCurrencySecurityId": 155, "SettlePrice": 196.68, "SettleToBaseFXRate": 1, "SettleToLocalFXRate": 1, "SettleToSystemFXRate": 1 }, { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 3, "Commission": 10, "CommissionCharges": [ { "CommissionCharge": 10, "CommissionChargeId": 22 }, { "CommissionCharge": 0, "CommissionChargeId": 24 } ], "ExecQuantity": 100, "FeeCharges": [ { "FeeCharge": 0.6555, "FeeChargeId": 27 }, { "FeeCharge": 0, "FeeChargeId": 28 }, { "FeeCharge": 0.7867, "FeeChargeId": 29 }, { "FeeCharge": 0.9834, "FeeChargeId": 30 }, { "FeeCharge": 19.668, "FeeChargeId": 31 }, { "FeeCharge": 25, "FeeChargeId": 32 }, { "FeeCharge": 15, "FeeChargeId": 33 }, { "FeeCharge": 98.34, "FeeChargeId": 34 } ], "Fees": 160.4336, "NetAmount": 19838.4336, "PortfolioId": 21, "PositionTags": [ { "IndexAttributeId": 1, "IndexAttributeValueId": 3 } ], "PrincipalAmount": 19668, "Quantity": 200, "RouteId": 1621, "SettleAmount": 19838.4336, "SettleCurrencySecurityId": 155, "SettlePrice": 196.68, "SettleToBaseFXRate": 1, "SettleToLocalFXRate": 1, "SettleToSystemFXRate": 1 } ], "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z", "EventTypeId": 11, "IsFinalized": true, "LocalCurrencySecurityId": 155, "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z", "OrderQuantity": 600, "SecurityId": 375, "SecurityTemplateId": 1, "SequenceNumber": 63, "SettleDate": "2018-09-08T13:25:43Z", "SourceSystemName": "Trading", "SourceSystemReferenceId": 123, "SystemCurrencySecurityId": 155, "SystemToLocalFXRate": 1, "TradeDate": "2018-09-08T13:25:43Z", "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z", "TransactionTimeZoneId": 1, "UserTimeZoneId": 12 }',
'2018-10-07 07:40:51',
'0E050403-7233-3FAD-6D34-B019A24A9802',
373,
14,
0,
'2018-10-05 07:40:51',
'2018-10-03 07:40:51',
'good-glx2-token-hansolo');

INSERT INTO testDb.Ibor_Transactions
(UserName,
SourceSystemName,
SourceSystemReference,
UserID,
OrderQuantity,
MessageBody,
ValidToUTC,
ActivityID,
SecurityID,
EventTypeID,
IsDeleted,
BusinessDateTimeUTC,
ValidFromUTC,
UserSessionToken)
VALUES
("test",
"Recon",
"400",
11,
150,
'{ "Allocations": [ { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 1, "CustodianCounterpartyAccountId": 10, "FeeCharges": [], "PortfolioId": 21, "PositionTags": [ { "IndexAttributeId": 1, "IndexAttributeValueId": 3 } ], "Quantity": 400 }, { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 2, "CommissionCharges": [], "CustodianCounterpartyAccountId": 10, "FeeCharges": [], "PortfolioId": 21, "PositionTags": [], "Quantity": 400 }, { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 3, "PortfolioId": 21, "PositionTags": [ { "IndexAttributeId": 1, "IndexAttributeValueId": 3 } ], "Quantity": 400 }, { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 1, "Commission": 10, "CommissionCharges": [ { "CommissionCharge": 10, "CommissionChargeId": 22 }, { "CommissionCharge": 0, "CommissionChargeId": 24 } ], "CustodianCounterpartyAccountId": 10, "ExecQuantity": 100, "FeeCharges": [ { "FeeCharge": 0.6555, "FeeChargeId": 27 }, { "FeeCharge": 0, "FeeChargeId": 28 }, { "FeeCharge": 0.7867, "FeeChargeId": 29 }, { "FeeCharge": 0.9834, "FeeChargeId": 30 }, { "FeeCharge": 19.668, "FeeChargeId": 31 }, { "FeeCharge": 25, "FeeChargeId": 32 }, { "FeeCharge": 15, "FeeChargeId": 33 }, { "FeeCharge": 98.34, "FeeChargeId": 34 } ], "Fees": 160.4336, "NetAmount": 19838.4336, "PortfolioId": 21, "PositionTags": [ { "IndexAttributeId": 1, "IndexAttributeValueId": 3 } ], "PrincipalAmount": 19668, "Quantity": 200, "RouteId": 1621, "RouteName": "MANUAL", "SettleAmount": 19838.4336, "SettleCurrencySecurityId": 155, "SettlePrice": 196.68, "SettleToBaseFXRate": 1, "SettleToLocalFXRate": 1, "SettleToSystemFXRate": 1 }, { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 2, "Commission": 10, "CommissionCharges": [ { "CommissionCharge": 10, "CommissionChargeId": 22 }, { "CommissionCharge": 0, "CommissionChargeId": 24 } ], "CustodianCounterpartyAccountId": 10, "ExecQuantity": 100, "FeeCharges": [ { "FeeCharge": 0.6555, "FeeChargeId": 27 }, { "FeeCharge": 0, "FeeChargeId": 28 }, { "FeeCharge": 0.7867, "FeeChargeId": 29 }, { "FeeCharge": 0.9834, "FeeChargeId": 30 }, { "FeeCharge": 19.668, "FeeChargeId": 31 }, { "FeeCharge": 25, "FeeChargeId": 32 }, { "FeeCharge": 15, "FeeChargeId": 33 }, { "FeeCharge": 98.34, "FeeChargeId": 34 } ], "Fees": 160.4336, "NetAmount": 19838.4336, "PortfolioId": 21, "PositionTags": [], "PrincipalAmount": 19668, "Quantity": 200, "RouteId": 1621, "RouteName": "MANUAL", "SettleAmount": 19838.4336, "SettleCurrencySecurityId": 155, "SettlePrice": 196.68, "SettleToBaseFXRate": 1, "SettleToLocalFXRate": 1, "SettleToSystemFXRate": 1 }, { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 3, "Commission": 10, "CommissionCharges": [ { "CommissionCharge": 10, "CommissionChargeId": 22 }, { "CommissionCharge": 0, "CommissionChargeId": 24 } ], "ExecQuantity": 100, "FeeCharges": [ { "FeeCharge": 0.6555, "FeeChargeId": 27 }, { "FeeCharge": 0, "FeeChargeId": 28 }, { "FeeCharge": 0.7867, "FeeChargeId": 29 }, { "FeeCharge": 0.9834, "FeeChargeId": 30 }, { "FeeCharge": 19.668, "FeeChargeId": 31 }, { "FeeCharge": 25, "FeeChargeId": 32 }, { "FeeCharge": 15, "FeeChargeId": 33 }, { "FeeCharge": 98.34, "FeeChargeId": 34 } ], "Fees": 160.4336, "NetAmount": 19838.4336, "PortfolioId": 21, "PositionTags": [ { "IndexAttributeId": 1, "IndexAttributeValueId": 3 } ], "PrincipalAmount": 19668, "Quantity": 200, "RouteId": 1621, "SettleAmount": 19838.4336, "SettleCurrencySecurityId": 155, "SettlePrice": 196.68, "SettleToBaseFXRate": 1, "SettleToLocalFXRate": 1, "SettleToSystemFXRate": 1 } ], "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z", "EventTypeId": 11, "IsFinalized": true, "LocalCurrencySecurityId": 155, "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z", "OrderQuantity": 600, "SecurityId": 375, "SecurityTemplateId": 1, "SequenceNumber": 63, "SettleDate": "2018-09-08T13:25:43Z", "SourceSystemName": "Trading", "SourceSystemReferenceId": 123, "SystemCurrencySecurityId": 155, "SystemToLocalFXRate": 1, "TradeDate": "2018-09-08T13:25:43Z", "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z", "TransactionTimeZoneId": 1, "UserTimeZoneId": 12 }',
'2018-10-07 07:40:51',
'0E050403-7233-3FAD-6D34-B019A24A9802',
500,
96,
0,
'2010-10-05 07:40:51',
'2010-10-05 07:40:51',
'good-glx2-token-hansolo');

INSERT INTO testDb.Ibor_Transactions
(UserName,
SourceSystemName,
SourceSystemReference,
UserID,
OrderQuantity,
MessageBody,
ValidToUTC,
ActivityID,
SecurityID,
EventTypeID,
IsDeleted,
BusinessDateTimeUTC,
ValidFromUTC,
UserSessionToken)
VALUES
("test",
"Recon",
"400",
11,
150,
'{ "Allocations": [ { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 1, "CustodianCounterpartyAccountId": 10, "FeeCharges": [], "PortfolioId": 21, "PositionTags": [ { "IndexAttributeId": 1, "IndexAttributeValueId": 3 } ], "Quantity": 400 }, { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 2, "CommissionCharges": [], "CustodianCounterpartyAccountId": 10, "FeeCharges": [], "PortfolioId": 21, "PositionTags": [], "Quantity": 400 }, { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 3, "PortfolioId": 21, "PositionTags": [ { "IndexAttributeId": 1, "IndexAttributeValueId": 3 } ], "Quantity": 400 }, { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 1, "Commission": 10, "CommissionCharges": [ { "CommissionCharge": 10, "CommissionChargeId": 22 }, { "CommissionCharge": 0, "CommissionChargeId": 24 } ], "CustodianCounterpartyAccountId": 10, "ExecQuantity": 100, "FeeCharges": [ { "FeeCharge": 0.6555, "FeeChargeId": 27 }, { "FeeCharge": 0, "FeeChargeId": 28 }, { "FeeCharge": 0.7867, "FeeChargeId": 29 }, { "FeeCharge": 0.9834, "FeeChargeId": 30 }, { "FeeCharge": 19.668, "FeeChargeId": 31 }, { "FeeCharge": 25, "FeeChargeId": 32 }, { "FeeCharge": 15, "FeeChargeId": 33 }, { "FeeCharge": 98.34, "FeeChargeId": 34 } ], "Fees": 160.4336, "NetAmount": 19838.4336, "PortfolioId": 21, "PositionTags": [ { "IndexAttributeId": 1, "IndexAttributeValueId": 3 } ], "PrincipalAmount": 19668, "Quantity": 200, "RouteId": 1621, "RouteName": "MANUAL", "SettleAmount": 19838.4336, "SettleCurrencySecurityId": 155, "SettlePrice": 196.68, "SettleToBaseFXRate": 1, "SettleToLocalFXRate": 1, "SettleToSystemFXRate": 1 }, { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 2, "Commission": 10, "CommissionCharges": [ { "CommissionCharge": 10, "CommissionChargeId": 22 }, { "CommissionCharge": 0, "CommissionChargeId": 24 } ], "CustodianCounterpartyAccountId": 10, "ExecQuantity": 100, "FeeCharges": [ { "FeeCharge": 0.6555, "FeeChargeId": 27 }, { "FeeCharge": 0, "FeeChargeId": 28 }, { "FeeCharge": 0.7867, "FeeChargeId": 29 }, { "FeeCharge": 0.9834, "FeeChargeId": 30 }, { "FeeCharge": 19.668, "FeeChargeId": 31 }, { "FeeCharge": 25, "FeeChargeId": 32 }, { "FeeCharge": 15, "FeeChargeId": 33 }, { "FeeCharge": 98.34, "FeeChargeId": 34 } ], "Fees": 160.4336, "NetAmount": 19838.4336, "PortfolioId": 21, "PositionTags": [], "PrincipalAmount": 19668, "Quantity": 200, "RouteId": 1621, "RouteName": "MANUAL", "SettleAmount": 19838.4336, "SettleCurrencySecurityId": 155, "SettlePrice": 196.68, "SettleToBaseFXRate": 1, "SettleToLocalFXRate": 1, "SettleToSystemFXRate": 1 }, { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 3, "Commission": 10, "CommissionCharges": [ { "CommissionCharge": 10, "CommissionChargeId": 22 }, { "CommissionCharge": 0, "CommissionChargeId": 24 } ], "ExecQuantity": 100, "FeeCharges": [ { "FeeCharge": 0.6555, "FeeChargeId": 27 }, { "FeeCharge": 0, "FeeChargeId": 28 }, { "FeeCharge": 0.7867, "FeeChargeId": 29 }, { "FeeCharge": 0.9834, "FeeChargeId": 30 }, { "FeeCharge": 19.668, "FeeChargeId": 31 }, { "FeeCharge": 25, "FeeChargeId": 32 }, { "FeeCharge": 15, "FeeChargeId": 33 }, { "FeeCharge": 98.34, "FeeChargeId": 34 } ], "Fees": 160.4336, "NetAmount": 19838.4336, "PortfolioId": 21, "PositionTags": [ { "IndexAttributeId": 1, "IndexAttributeValueId": 3 } ], "PrincipalAmount": 19668, "Quantity": 200, "RouteId": 1621, "SettleAmount": 19838.4336, "SettleCurrencySecurityId": 155, "SettlePrice": 196.68, "SettleToBaseFXRate": 1, "SettleToLocalFXRate": 1, "SettleToSystemFXRate": 1 } ], "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z", "EventTypeId": 11, "IsFinalized": true, "LocalCurrencySecurityId": 155, "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z", "OrderQuantity": 600, "SecurityId": 375, "SecurityTemplateId": 1, "SequenceNumber": 63, "SettleDate": "2018-09-08T13:25:43Z", "SourceSystemName": "Trading", "SourceSystemReferenceId": 123, "SystemCurrencySecurityId": 155, "SystemToLocalFXRate": 1, "TradeDate": "2018-09-08T13:25:43Z", "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z", "TransactionTimeZoneId": 1, "UserTimeZoneId": 12 }',
'2018-10-07 07:40:51',
'0E050403-7233-3FAD-6D34-B019A24A9802',
5000,
96,
0,
'2001-10-05 07:40:51',
'2001-10-05 07:40:51',
'good-glx2-token-hansolo');


INSERT INTO testDb.Ibor_ValidationErrors (TransactionRecordID, ErrorMessage, TransactionType)
VALUES
(
   1,
   'OrderQty mismatch in Strategy Book',
   'EquityTransaction'
  );
  INSERT INTO testDb.Ibor_ValidationErrors (TransactionRecordID, ErrorMessage, TransactionType)
VALUES
(
   1,
   'OrderQty mismatch',
   'EquityTransaction'
  );
  INSERT INTO testDb.Ibor_ValidationErrors (TransactionRecordID, ErrorMessage, TransactionType)
VALUES
(
   2,
   'Invalid TransactionDateTimeUTC',
   'EquityTransaction'
  );
  INSERT INTO testDb.Ibor_ValidationErrors (TransactionRecordID, ErrorMessage, TransactionType)
VALUES
(
   3,
   'Invalid BusinessDateTimeUTC',
   'EquityTransaction'
  );
INSERT INTO testDb.Ibor_ValidationErrors (TransactionRecordID, ErrorMessage, TransactionType)
VALUES
(
   4,
   'Invalid BusinessDateTimeUTC',
   'EquityTransaction'
  );

INSERT INTO testDb.Ibor_Fx_Transactions
(UserName,
SourceSystemName,
SourceSystemReference,
UserID,
OrderQuantity,
MessageBody,
ValidToUTC,
ActivityID,
SecurityID,
EventTypeID,
IsDeleted,
BusinessDateTimeUTC,
ValidFromUTC,
UserSessionToken)
VALUES
("test",
"trading",
"300",
12,
1000,
'\{"Allocations":[{"BaseCurrencySecurityId":155,"BaseToLocalFxRate":1,"BookTypeId":1,"CommissionCharges":null,"CustodianCounterpartyAccountId":10,"FeeCharges":[],"PortfolioId":21,"PositionTags":[{"IndexAttributeId":1,"IndexAttributeValueId":3}],"Quantity":400},{"BaseCurrencySecurityId":155,"BaseToLocalFxRate":1,"BookTypeId":2,"CommissionCharges":[],"CustodianCounterpartyAccountId":10,"FeeCharges":[],"PortfolioId":21,"PositionTags":[],"Quantity":400},{"BaseCurrencySecurityId":155,"BaseToLocalFxRate":1,"BookTypeId":3,"CommissionCharges":null,"FeeCharges":null,"PortfolioId":21,"PositionTags":[{"IndexAttributeId":1,"IndexAttributeValueId":3}],"Quantity":400},{"BaseCurrencySecurityId":155,"BaseToLocalFxRate":1,"BookTypeId":1,"Commission":10,"CommissionCharges":[{"CommissionCharge":10,"CommissionChargeId":22},{"CommissionCharge":0,"CommissionChargeId":24}],"CustodianCounterpartyAccountId":10,"ExecQuantity":100,"FeeCharges":[{"FeeCharge":0.6555,"FeeChargeId":27},{"FeeCharge":0,"FeeChargeId":28},{"FeeCharge":0.7867,"FeeChargeId":29},{"FeeCharge":0.9834,"FeeChargeId":30},{"FeeCharge":19.668,"FeeChargeId":31},{"FeeCharge":25,"FeeChargeId":32},{"FeeCharge":15,"FeeChargeId":33},{"FeeCharge":98.34,"FeeChargeId":34}],"Fees":160.4336,"NetAmount":19838.4336,"PortfolioId":21,"PositionTags":[{"IndexAttributeId":1,"IndexAttributeValueId":3}],"PrincipalAmount":19668,"Quantity":200,"RouteId":1621,"RouteName":"MANUAL","SettleAmount":19838.4336,"SettleCurrencySecurityId":155,"SettlePrice":196.68,"SettleToBaseFXRate":1,"SettleToLocalFXRate":1,"SettleToSystemFXRate":1},{"BaseCurrencySecurityId":155,"BaseToLocalFxRate":1,"BookTypeId":2,"Commission":10,"CommissionCharges":[{"CommissionCharge":10,"CommissionChargeId":22},{"CommissionCharge":0,"CommissionChargeId":24}],"CustodianCounterpartyAccountId":10,"ExecQuantity":100,"FeeCharges":[{"FeeCharge":0.6555,"FeeChargeId":27},{"FeeCharge":0,"FeeChargeId":28},{"FeeCharge":0.7867,"FeeChargeId":29},{"FeeCharge":0.9834,"FeeChargeId":30},{"FeeCharge":19.668,"FeeChargeId":31},{"FeeCharge":25,"FeeChargeId":32},{"FeeCharge":15,"FeeChargeId":33},{"FeeCharge":98.34,"FeeChargeId":34}],"Fees":160.4336,"NetAmount":19838.4336,"PortfolioId":21,"PositionTags":[],"PrincipalAmount":19668,"Quantity":200,"RouteId":1621,"RouteName":"MANUAL","SettleAmount":19838.4336,"SettleCurrencySecurityId":155,"SettlePrice":196.68,"SettleToBaseFXRate":1,"SettleToLocalFXRate":1,"SettleToSystemFXRate":1},{"BaseCurrencySecurityId":155,"BaseToLocalFxRate":1,"BookTypeId":3,"Commission":10,"CommissionCharges":[{"CommissionCharge":10,"CommissionChargeId":22},{"CommissionCharge":0,"CommissionChargeId":24}],"ExecQuantity":100,"FeeCharges":[{"FeeCharge":0.6555,"FeeChargeId":27},{"FeeCharge":0,"FeeChargeId":28},{"FeeCharge":0.7867,"FeeChargeId":29},{"FeeCharge":0.9834,"FeeChargeId":30},{"FeeCharge":19.668,"FeeChargeId":31},{"FeeCharge":25,"FeeChargeId":32},{"FeeCharge":15,"FeeChargeId":33},{"FeeCharge":98.34,"FeeChargeId":34}],"Fees":160.4336,"NetAmount":19838.4336,"PortfolioId":21,"PositionTags":[{"IndexAttributeId":1,"IndexAttributeValueId":3}],"PrincipalAmount":19668,"Quantity":200,"RouteId":1621,"SettleAmount":19838.4336,"SettleCurrencySecurityId":155,"SettlePrice":196.68,"SettleToBaseFXRate":1,"SettleToLocalFXRate":1,"SettleToSystemFXRate":1}],"BusinessDateTimeUTC":"2018-09-08T13:25:43.0000000Z","EventTypeId":14,"IsFinalized":true,"LocalCurrencySecurityId":155,"OrderCreatedDate":"2018-09-08T13:25:43.0000000Z","OrderQuantity":600,"SecurityId":375,"SecurityTemplateId":1,"SequenceNumber":63,"SettleDate":"2018-09-10T13:25:43Z","SourceSystemName":"Trading","SourceSystemReference":"4803","SystemCurrencySecurityId":155,"SystemToLocalFXRate":1,"TradeDate":"2018-09-08T13:25:43Z","TransactionDateTimeUTC":"2018-09-08T13:25:43.0000000Z","TransactionTimeZoneId":1,"UserTimeZoneId":12}',
'2018-10-07 07:40:51',
'87fcef07-8672-4c44-a3b7-8bb7f6715de4',
173,
13,
0,
'2018-10-05 07:40:51',
'2018-10-03 07:40:51',
'good-glx2-token-hansolo');

INSERT INTO testDb.Ibor_Fx_Transactions
(UserName,
SourceSystemName,
SourceSystemReference,
UserID,
OrderQuantity,
MessageBody,
ValidToUTC,
ActivityID,
SecurityID,
EventTypeID,
IsDeleted,
BusinessDateTimeUTC,
ValidFromUTC,
UserSessionToken)
VALUES
("test",
"Recon",
"300",
12,
1001,
'{ "Allocations": [ { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 1, "CustodianCounterpartyAccountId": 10, "FeeCharges": [], "PortfolioId": 21, "PositionTags": [ { "IndexAttributeId": 1, "IndexAttributeValueId": 3 } ], "Quantity": 400 }, { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 2, "CommissionCharges": [], "CustodianCounterpartyAccountId": 10, "FeeCharges": [], "PortfolioId": 21, "PositionTags": [], "Quantity": 400 }, { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 3, "PortfolioId": 21, "PositionTags": [ { "IndexAttributeId": 1, "IndexAttributeValueId": 3 } ], "Quantity": 400 }, { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 1, "Commission": 10, "CommissionCharges": [ { "CommissionCharge": 10, "CommissionChargeId": 22 }, { "CommissionCharge": 0, "CommissionChargeId": 24 } ], "CustodianCounterpartyAccountId": 10, "ExecQuantity": 100, "FeeCharges": [ { "FeeCharge": 0.6555, "FeeChargeId": 27 }, { "FeeCharge": 0, "FeeChargeId": 28 }, { "FeeCharge": 0.7867, "FeeChargeId": 29 }, { "FeeCharge": 0.9834, "FeeChargeId": 30 }, { "FeeCharge": 19.668, "FeeChargeId": 31 }, { "FeeCharge": 25, "FeeChargeId": 32 }, { "FeeCharge": 15, "FeeChargeId": 33 }, { "FeeCharge": 98.34, "FeeChargeId": 34 } ], "Fees": 160.4336, "NetAmount": 19838.4336, "PortfolioId": 21, "PositionTags": [ { "IndexAttributeId": 1, "IndexAttributeValueId": 3 } ], "PrincipalAmount": 19668, "Quantity": 200, "RouteId": 1621, "RouteName": "MANUAL", "SettleAmount": 19838.4336, "SettleCurrencySecurityId": 155, "SettlePrice": 196.68, "SettleToBaseFXRate": 1, "SettleToLocalFXRate": 1, "SettleToSystemFXRate": 1 }, { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 2, "Commission": 10, "CommissionCharges": [ { "CommissionCharge": 10, "CommissionChargeId": 22 }, { "CommissionCharge": 0, "CommissionChargeId": 24 } ], "CustodianCounterpartyAccountId": 10, "ExecQuantity": 100, "FeeCharges": [ { "FeeCharge": 0.6555, "FeeChargeId": 27 }, { "FeeCharge": 0, "FeeChargeId": 28 }, { "FeeCharge": 0.7867, "FeeChargeId": 29 }, { "FeeCharge": 0.9834, "FeeChargeId": 30 }, { "FeeCharge": 19.668, "FeeChargeId": 31 }, { "FeeCharge": 25, "FeeChargeId": 32 }, { "FeeCharge": 15, "FeeChargeId": 33 }, { "FeeCharge": 98.34, "FeeChargeId": 34 } ], "Fees": 160.4336, "NetAmount": 19838.4336, "PortfolioId": 21, "PositionTags": [], "PrincipalAmount": 19668, "Quantity": 200, "RouteId": 1621, "RouteName": "MANUAL", "SettleAmount": 19838.4336, "SettleCurrencySecurityId": 155, "SettlePrice": 196.68, "SettleToBaseFXRate": 1, "SettleToLocalFXRate": 1, "SettleToSystemFXRate": 1 }, { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 3, "Commission": 10, "CommissionCharges": [ { "CommissionCharge": 10, "CommissionChargeId": 22 }, { "CommissionCharge": 0, "CommissionChargeId": 24 } ], "ExecQuantity": 100, "FeeCharges": [ { "FeeCharge": 0.6555, "FeeChargeId": 27 }, { "FeeCharge": 0, "FeeChargeId": 28 }, { "FeeCharge": 0.7867, "FeeChargeId": 29 }, { "FeeCharge": 0.9834, "FeeChargeId": 30 }, { "FeeCharge": 19.668, "FeeChargeId": 31 }, { "FeeCharge": 25, "FeeChargeId": 32 }, { "FeeCharge": 15, "FeeChargeId": 33 }, { "FeeCharge": 98.34, "FeeChargeId": 34 } ], "Fees": 160.4336, "NetAmount": 19838.4336, "PortfolioId": 21, "PositionTags": [ { "IndexAttributeId": 1, "IndexAttributeValueId": 3 } ], "PrincipalAmount": 19668, "Quantity": 200, "RouteId": 1621, "SettleAmount": 19838.4336, "SettleCurrencySecurityId": 155, "SettlePrice": 196.68, "SettleToBaseFXRate": 1, "SettleToLocalFXRate": 1, "SettleToSystemFXRate": 1 } ], "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z", "EventTypeId": 11, "IsFinalized": true, "LocalCurrencySecurityId": 155, "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z", "OrderQuantity": 600, "SecurityId": 375, "SecurityTemplateId": 1, "SequenceNumber": 63, "SettleDate": "2018-09-08T13:25:43Z", "SourceSystemName": "Trading", "SourceSystemReferenceId": 123, "SystemCurrencySecurityId": 155, "SystemToLocalFXRate": 1, "TradeDate": "2018-09-08T13:25:43Z", "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z", "TransactionTimeZoneId": 1, "UserTimeZoneId": 12 }',
'2018-10-07 07:40:51',
'0E050403-7233-3FAD-6D34-B019A24A9802',
273,
14,
0,
'2018-10-05 07:40:51',
'2018-10-03 07:40:51',
'good-glx2-token-hansolo');

INSERT INTO testDb.Ibor_Fx_Transactions
(UserName,
SourceSystemName,
SourceSystemReference,
UserID,
OrderQuantity,
MessageBody,
ValidToUTC,
ActivityID,
SecurityID,
EventTypeID,
IsDeleted,
BusinessDateTimeUTC,
ValidFromUTC,
UserSessionToken)
VALUES
("test",
"Recon",
"400",
20,
2001,
'{ "Allocations": [ { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 1, "CustodianCounterpartyAccountId": 10, "FeeCharges": [], "PortfolioId": 21, "PositionTags": [ { "IndexAttributeId": 1, "IndexAttributeValueId": 3 } ], "Quantity": 400 }, { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 2, "CommissionCharges": [], "CustodianCounterpartyAccountId": 10, "FeeCharges": [], "PortfolioId": 21, "PositionTags": [], "Quantity": 400 }, { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 3, "PortfolioId": 21, "PositionTags": [ { "IndexAttributeId": 1, "IndexAttributeValueId": 3 } ], "Quantity": 400 }, { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 1, "Commission": 10, "CommissionCharges": [ { "CommissionCharge": 10, "CommissionChargeId": 22 }, { "CommissionCharge": 0, "CommissionChargeId": 24 } ], "CustodianCounterpartyAccountId": 10, "ExecQuantity": 100, "FeeCharges": [ { "FeeCharge": 0.6555, "FeeChargeId": 27 }, { "FeeCharge": 0, "FeeChargeId": 28 }, { "FeeCharge": 0.7867, "FeeChargeId": 29 }, { "FeeCharge": 0.9834, "FeeChargeId": 30 }, { "FeeCharge": 19.668, "FeeChargeId": 31 }, { "FeeCharge": 25, "FeeChargeId": 32 }, { "FeeCharge": 15, "FeeChargeId": 33 }, { "FeeCharge": 98.34, "FeeChargeId": 34 } ], "Fees": 160.4336, "NetAmount": 19838.4336, "PortfolioId": 21, "PositionTags": [ { "IndexAttributeId": 1, "IndexAttributeValueId": 3 } ], "PrincipalAmount": 19668, "Quantity": 200, "RouteId": 1621, "RouteName": "MANUAL", "SettleAmount": 19838.4336, "SettleCurrencySecurityId": 155, "SettlePrice": 196.68, "SettleToBaseFXRate": 1, "SettleToLocalFXRate": 1, "SettleToSystemFXRate": 1 }, { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 2, "Commission": 10, "CommissionCharges": [ { "CommissionCharge": 10, "CommissionChargeId": 22 }, { "CommissionCharge": 0, "CommissionChargeId": 24 } ], "CustodianCounterpartyAccountId": 10, "ExecQuantity": 100, "FeeCharges": [ { "FeeCharge": 0.6555, "FeeChargeId": 27 }, { "FeeCharge": 0, "FeeChargeId": 28 }, { "FeeCharge": 0.7867, "FeeChargeId": 29 }, { "FeeCharge": 0.9834, "FeeChargeId": 30 }, { "FeeCharge": 19.668, "FeeChargeId": 31 }, { "FeeCharge": 25, "FeeChargeId": 32 }, { "FeeCharge": 15, "FeeChargeId": 33 }, { "FeeCharge": 98.34, "FeeChargeId": 34 } ], "Fees": 160.4336, "NetAmount": 19838.4336, "PortfolioId": 21, "PositionTags": [], "PrincipalAmount": 19668, "Quantity": 200, "RouteId": 1621, "RouteName": "MANUAL", "SettleAmount": 19838.4336, "SettleCurrencySecurityId": 155, "SettlePrice": 196.68, "SettleToBaseFXRate": 1, "SettleToLocalFXRate": 1, "SettleToSystemFXRate": 1 }, { "BaseCurrencySecurityId": 155, "BaseToLocalFxRate": 1, "BookTypeId": 3, "Commission": 10, "CommissionCharges": [ { "CommissionCharge": 10, "CommissionChargeId": 22 }, { "CommissionCharge": 0, "CommissionChargeId": 24 } ], "ExecQuantity": 100, "FeeCharges": [ { "FeeCharge": 0.6555, "FeeChargeId": 27 }, { "FeeCharge": 0, "FeeChargeId": 28 }, { "FeeCharge": 0.7867, "FeeChargeId": 29 }, { "FeeCharge": 0.9834, "FeeChargeId": 30 }, { "FeeCharge": 19.668, "FeeChargeId": 31 }, { "FeeCharge": 25, "FeeChargeId": 32 }, { "FeeCharge": 15, "FeeChargeId": 33 }, { "FeeCharge": 98.34, "FeeChargeId": 34 } ], "Fees": 160.4336, "NetAmount": 19838.4336, "PortfolioId": 21, "PositionTags": [ { "IndexAttributeId": 1, "IndexAttributeValueId": 3 } ], "PrincipalAmount": 19668, "Quantity": 200, "RouteId": 1621, "SettleAmount": 19838.4336, "SettleCurrencySecurityId": 155, "SettlePrice": 196.68, "SettleToBaseFXRate": 1, "SettleToLocalFXRate": 1, "SettleToSystemFXRate": 1 } ], "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z", "EventTypeId": 11, "IsFinalized": true, "LocalCurrencySecurityId": 155, "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z", "OrderQuantity": 600, "SecurityId": 375, "SecurityTemplateId": 1, "SequenceNumber": 63, "SettleDate": "2018-09-08T13:25:43Z", "SourceSystemName": "Trading", "SourceSystemReferenceId": 123, "SystemCurrencySecurityId": 155, "SystemToLocalFXRate": 1, "TradeDate": "2018-09-08T13:25:43Z", "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z", "TransactionTimeZoneId": 1, "UserTimeZoneId": 12 }',
'2018-10-07 07:40:51',
'0E050403-7233-3FAD-6D34-B019A24A9802',
501,
96,
0,
'2010-10-05 07:40:51',
'2010-10-05 07:40:51',
'good-glx2-token-hansolo');

INSERT INTO testDb.Ibor_ValidationErrors (TransactionRecordID, ErrorMessage, TransactionType)
VALUES
(
   1,
   'OrderQty mismatch in Strategy Book',
   'FxTransaction'
  );
  INSERT INTO testDb.Ibor_ValidationErrors (TransactionRecordID, ErrorMessage, TransactionType)
VALUES
(
   1,
   'OrderQty mismatch',
   'FxTransaction'
  );
  INSERT INTO testDb.Ibor_ValidationErrors (TransactionRecordID, ErrorMessage, TransactionType)
VALUES
(
   2,
   'Invalid TransactionDateTimeUTC',
   'FxTransaction'
  );
  INSERT INTO testDb.Ibor_ValidationErrors (TransactionRecordID, ErrorMessage, TransactionType)
VALUES
(
   3,
   'Invalid BusinessDateTimeUTC',
   'FxTransaction'
  );
