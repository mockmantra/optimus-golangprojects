# Project Name

TODO: Write a project description

## Installation

TODO: Describe the installation process

## Usage
### Steps to configure authentication api through nginx.

* Clone and run the repo [mock-eclipse](https://stash.ezesoft.net/projects/IPA/repos/mock-eclipse/browse). 
And run `npm start` at root directory. This api runs on the port 3000.

* Install nginx in local if not done already and add below configuration in `nginx.conf` file under `/usr/local/etc/nginx/nginx.conf` and restart the nginx service.

    Add upstreams in `http` section

    ```
    upstream Platform_Api_api_platform_v1 {
        server localhost:3000;
    }

    upstream Ibor_transaction_read_api_v1 {
        server localhost:33380;
    }
    ```

    Add/Update following in `server` section

    ```
    listen 8001

    location /api/platform/v1 {
        proxy_pass http://Platform_Api_api_platform_v1;
    }
    location /api/ibor/transaction-read/v1 {
        proxy_pass http://Ibor_transaction_read_api_v1;
    }
    ```
    Note: Now the api can be accessed using http://localhost:8001/api/ibor/transaction-read/v1/doc/swagger


## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## History

TODO: Write history

## Credits

TODO: Write credits

## License

TODO: Copyright (c) 2015 Eze Software Group.   All rights reserved.