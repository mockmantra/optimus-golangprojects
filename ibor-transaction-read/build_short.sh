# use this to shorten time to re-build. Build using build.sh first and then use build_short.sh
set -e

BDD_CLIENT="bdd_client"
SERVICE="service"
MOCK_IMS="mock_ims"
DB="sqldb"
NETWORK="bdd_network"


set -e

 container_id=$(docker ps -aqf "name=bdd_client")
        if [[ ! -z "$container_id" ]]
        then
            echo "Container found: " $container_id
            docker rm -f $container_id
        else
            echo "No container found for: bdd_client"
        fi

echo "Step 8 : Build client-bdd....."
    docker build -t $BDD_CLIENT --build-arg service=$SERVICE -f tests/bdd/client/Dockerfile tests/bdd/client/
    echo "Step 9 : Run client-bdd....."
    docker run  --name $BDD_CLIENT -e Auth_API_ROOT=http://$MOCK_IMS:3000 -e API_ROOT=http://$SERVICE:33380 --network $NETWORK $BDD_CLIENT $1
    