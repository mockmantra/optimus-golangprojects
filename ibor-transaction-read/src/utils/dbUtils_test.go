package utils

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	domain "stash.ezesoft.net/imsacnt/ibor-transaction-read/src/domainEntities"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/logger"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/mocks"
)

func Test_CreateRejectedTxnMap(t *testing.T) {
	dbUtils := DbUtils{}
	workflowContext := &domain.WorkflowContext{
		Logger: logger.GetLogger(nil, "", ""),
	}
	rows := new(mocks.ISqlRow)
	rows.On("Next").Return(true).Once()
	rows.On("Next").Return(false).Once()
	rows.On("Scan", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
	rows.On("Err").Return(nil)
	rows.On("Close").Return(nil)
	result, err := dbUtils.CreateRejectedTxnMap(workflowContext, rows)
	assert.Nil(t, err)
	assert.Equal(t, len(result), 1)
}

func Test_CreateRejectedTxnMap_DBScanError(t *testing.T) {
	dbUtils := DbUtils{}
	workflowContext := &domain.WorkflowContext{
		Logger: logger.GetLogger(nil, "", ""),
	}
	rows := new(mocks.ISqlRow)
	rows.On("Next").Return(true).Once()
	rows.On("Next").Return(false).Once()
	rows.On("Scan", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("scan error"))
	rows.On("Err").Return(nil)
	rows.On("Close").Return(nil)
	_, err := dbUtils.CreateRejectedTxnMap(workflowContext, rows)
	assert.NotNil(t, err)
}

func Test_CreateRejectedTxnMap_MultipleValidationErrors(t *testing.T) {
	dbUtils := DbUtils{}
	workflowContext := &domain.WorkflowContext{
		Logger: logger.GetLogger(nil, "", ""),
	}
	rows := new(mocks.ISqlRow)
	rows.On("Next").Return(true).Twice()
	rows.On("Next").Return(false).Once()
	rows.On("Scan", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
	rows.On("Err").Return(nil)
	rows.On("Close").Return(nil)
	result, err := dbUtils.CreateRejectedTxnMap(workflowContext, rows)
	assert.Nil(t, err)
	assert.Equal(t, result[0].RejectedMessage, "Multiple Validation Errors Found.")
}

func Test_CreateErrorsList_MultipleValidationErrors(t *testing.T) {
	dbUtils := DbUtils{}
	workflowContext := &domain.WorkflowContext{
		Logger: logger.GetLogger(nil, "", ""),
	}
	rows := new(mocks.ISqlRow)
	rows.On("Next").Return(true).Twice()
	rows.On("Next").Return(false).Once()
	rows.On("Scan", mock.Anything).Return(nil)
	rows.On("Err").Return(nil)
	rows.On("Close").Return(nil)
	result, err := dbUtils.CreateErrorsList(workflowContext, rows)
	assert.Nil(t, err)
	assert.Equal(t, len(result), 2)
}

func Test_CreateErrorsList_ReturnsDBScanError(t *testing.T) {
	dbUtils := DbUtils{}
	workflowContext := &domain.WorkflowContext{
		Logger: logger.GetLogger(nil, "", ""),
	}
	rows := new(mocks.ISqlRow)
	rows.On("Next").Return(true).Once()
	rows.On("Next").Return(false).Once()
	rows.On("Scan", mock.Anything).Return(errors.New("scan error"))
	rows.On("Close").Return(nil)
	result, err := dbUtils.CreateErrorsList(workflowContext, rows)
	assert.Nil(t, result)
	assert.NotNil(t, err)
}

func Test_CreateRejectedTxnMap_IteratingRowsError(t *testing.T) {
	dbUtils := DbUtils{}
	workflowContext := &domain.WorkflowContext{
		Logger: logger.GetLogger(nil, "", ""),
	}
	rows := new(mocks.ISqlRow)
	rows.On("Next").Return(false).Once()
	rows.On("Err").Return(errors.New("scan error"))
	_, err := dbUtils.CreateRejectedTxnMap(workflowContext, rows)
	assert.NotNil(t, err)
}

func Test_CreateErrorsList_IteratingRowsError(t *testing.T) {
	dbUtils := DbUtils{}
	workflowContext := &domain.WorkflowContext{
		Logger: logger.GetLogger(nil, "", ""),
	}
	rows := new(mocks.ISqlRow)
	rows.On("Next").Return(false).Once()
	rows.On("Err").Return(errors.New("scan error"))
	result, err := dbUtils.CreateErrorsList(workflowContext, rows)
	assert.Nil(t, result)
	assert.NotNil(t, err)
}
