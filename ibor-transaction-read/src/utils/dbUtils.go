package utils

import (
	domain "stash.ezesoft.net/imsacnt/ibor-transaction-read/src/domainEntities"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/interfaces"
)

// DbUtils ..
type DbUtils struct {
}

// CreateRejectedTxnMap ...
func (dbUtils *DbUtils) CreateRejectedTxnMap(context *domain.WorkflowContext, rows interfaces.ISqlRow) ([]*domain.Transaction, error) {
	mapResult := make(map[int]*domain.Transaction)
	var dbResults []*domain.Transaction

	for rows.Next() {
		var result domain.Transaction
		err := rows.Scan(&result.RecordID, &result.EventTypeID, &result.RejectedMessage, &result.SourceSystemName, &result.SecurityID, &result.OrderQuantity, &result.BusinessDateTimeUTC, &result.SourceSystemReference, &result.TransactionType)
		if err != nil {
			context.Logger.Error(err)
			return nil, err
		}

		if val, ok := mapResult[result.RecordID]; ok {
			val.RejectedMessage = "Multiple Validation Errors Found."
		} else {
			// initialize the map for the particular recordId.
			mapResult[result.RecordID] = &result
			dbResults = append(dbResults, mapResult[result.RecordID])
		}
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	defer rows.Close()
	return dbResults, nil
}

// CreateErrorsList ...
func (dbUtils *DbUtils) CreateErrorsList(context *domain.WorkflowContext, rows interfaces.ISqlRow) ([]domain.RejectList, error) {
	dbResults := []domain.RejectList{}
	index := 0
	for rows.Next() {
		var result string
		err := rows.Scan(&result)
		if err != nil {
			context.Logger.Error(err)
			return nil, err
		}
		index++
		dbResults = append(dbResults, domain.RejectList{RejectMessage: result, Index: index})
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}
	return dbResults, nil
}
