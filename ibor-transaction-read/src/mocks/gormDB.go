// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import gorm "github.com/jinzhu/gorm"

import mock "github.com/stretchr/testify/mock"
import sql "database/sql"

// IDB is an autogenerated mock type for the IDB type
type IDB struct {
	mock.Mock
}

// Close provides a mock function with given fields:
func (_m *IDB) Close() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Create provides a mock function with given fields: _a0
func (_m *IDB) Create(_a0 interface{}) *gorm.DB {
	ret := _m.Called(_a0)

	var r0 *gorm.DB
	if rf, ok := ret.Get(0).(func(interface{}) *gorm.DB); ok {
		r0 = rf(_a0)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*gorm.DB)
		}
	}

	return r0
}

// CreateTable provides a mock function with given fields: _a0
func (_m *IDB) CreateTable(_a0 ...interface{}) *gorm.DB {
	var _ca []interface{}
	_ca = append(_ca, _a0...)
	ret := _m.Called(_ca...)

	var r0 *gorm.DB
	if rf, ok := ret.Get(0).(func(...interface{}) *gorm.DB); ok {
		r0 = rf(_a0...)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*gorm.DB)
		}
	}

	return r0
}

// Exec provides a mock function with given fields: _a0, values
func (_m *IDB) Exec(_a0 string, values ...interface{}) *gorm.DB {
	var _ca []interface{}
	_ca = append(_ca, _a0)
	_ca = append(_ca, values...)
	ret := _m.Called(_ca...)

	var r0 *gorm.DB
	if rf, ok := ret.Get(0).(func(string, ...interface{}) *gorm.DB); ok {
		r0 = rf(_a0, values...)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*gorm.DB)
		}
	}

	return r0
}

// HasTable provides a mock function with given fields: _a0
func (_m *IDB) HasTable(_a0 interface{}) bool {
	ret := _m.Called(_a0)

	var r0 bool
	if rf, ok := ret.Get(0).(func(interface{}) bool); ok {
		r0 = rf(_a0)
	} else {
		r0 = ret.Get(0).(bool)
	}

	return r0
}

// Joins provides a mock function with given fields: query, args
func (_m *IDB) Joins(query string, args ...interface{}) *gorm.DB {
	var _ca []interface{}
	_ca = append(_ca, query)
	_ca = append(_ca, args...)
	ret := _m.Called(_ca...)

	var r0 *gorm.DB
	if rf, ok := ret.Get(0).(func(string, ...interface{}) *gorm.DB); ok {
		r0 = rf(query, args...)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*gorm.DB)
		}
	}

	return r0
}

// LogMode provides a mock function with given fields: enable
func (_m *IDB) LogMode(enable bool) *gorm.DB {
	ret := _m.Called(enable)

	var r0 *gorm.DB
	if rf, ok := ret.Get(0).(func(bool) *gorm.DB); ok {
		r0 = rf(enable)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*gorm.DB)
		}
	}

	return r0
}

// ScanRows provides a mock function with given fields: rows, result
func (_m *IDB) ScanRows(rows *sql.Rows, result interface{}) error {
	ret := _m.Called(rows, result)

	var r0 error
	if rf, ok := ret.Get(0).(func(*sql.Rows, interface{}) error); ok {
		r0 = rf(rows, result)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Select provides a mock function with given fields: query, args
func (_m *IDB) Select(query interface{}, args ...interface{}) *gorm.DB {
	var _ca []interface{}
	_ca = append(_ca, query)
	_ca = append(_ca, args...)
	ret := _m.Called(_ca...)

	var r0 *gorm.DB
	if rf, ok := ret.Get(0).(func(interface{}, ...interface{}) *gorm.DB); ok {
		r0 = rf(query, args...)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*gorm.DB)
		}
	}

	return r0
}

// Table provides a mock function with given fields: name
func (_m *IDB) Table(name string) *gorm.DB {
	ret := _m.Called(name)

	var r0 *gorm.DB
	if rf, ok := ret.Get(0).(func(string) *gorm.DB); ok {
		r0 = rf(name)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*gorm.DB)
		}
	}

	return r0
}

// Where provides a mock function with given fields: query, args
func (_m *IDB) Where(query interface{}, args ...interface{}) *gorm.DB {
	var _ca []interface{}
	_ca = append(_ca, query)
	_ca = append(_ca, args...)
	ret := _m.Called(_ca...)

	var r0 *gorm.DB
	if rf, ok := ret.Get(0).(func(interface{}, ...interface{}) *gorm.DB); ok {
		r0 = rf(query, args...)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*gorm.DB)
		}
	}

	return r0
}
