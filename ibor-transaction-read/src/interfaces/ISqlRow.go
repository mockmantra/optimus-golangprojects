package interfaces

// ISqlRow ...
type ISqlRow interface {
	Next() bool
	Scan(dest ...interface{}) error
	Close() error
	Err() error
}
