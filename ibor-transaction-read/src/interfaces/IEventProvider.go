package interfaces

import (
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/models"
)

//IEventProvider ...
type IEventProvider interface {
	GetAllEventTypes() (map[int]models.EventType, error)
}
