package interfaces

import (
	"database/sql"
	"time"

	"github.com/jinzhu/gorm"
	domain "stash.ezesoft.net/imsacnt/ibor-transaction-read/src/domainEntities"
)

// IDB - Interface for DB struct in gorm
type IDB interface {
	HasTable(interface{}) bool
	CreateTable(...interface{}) *gorm.DB
	Create(interface{}) *gorm.DB
	Close() error
	Exec(sql string, values ...interface{}) *gorm.DB
	Table(name string) *gorm.DB
	ScanRows(rows *sql.Rows, result interface{}) error
	Where(query interface{}, args ...interface{}) *gorm.DB
	Select(query interface{}, args ...interface{}) *gorm.DB
	Joins(query string, args ...interface{}) *gorm.DB
	LogMode(enable bool) *gorm.DB
}

// IDataAccessor - Interface for accessing Database
type IDataAccessor interface {
	Close() error
	Connect(string) error
	ReadTransaction(context *domain.WorkflowContext, startDate time.Time, endDate time.Time,
		eventTypeID int, sourceSystem string, transactionType []string) ([]*domain.Transaction, error)
	ReadErrors(context *domain.WorkflowContext, recorID string, txnType string) ([]domain.RejectList, error)
}

// IDBUtils - Interface for accessing Database utils
type IDBUtils interface {
	CreateRejectedTxnMap(context *domain.WorkflowContext, rows ISqlRow) ([]*domain.Transaction, error)
	CreateErrorsList(context *domain.WorkflowContext, rows ISqlRow) ([]domain.RejectList, error)
}
