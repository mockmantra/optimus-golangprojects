package interfaces

import (
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/models"
)

//ISecuritySymbolProvider ...
type ISecuritySymbolProvider interface {
	GetSecuritySymbols(idList []int) (map[int]models.Security, error)
}
