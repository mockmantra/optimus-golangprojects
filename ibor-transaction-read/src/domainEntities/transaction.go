package domain

import (
	"database/sql"
	"encoding/json"
	"time"
)

// Transaction transaction
type Transaction struct {
	EventTypeID           int       `json:"EventTypeID"`
	OrderQuantity         NullInt64 `json:"OrderQuantity"`
	SecurityID            int       `json:"SecurityID"`
	SourceSystemName      string    `json:"SourceSystemName"`
	BusinessDateTimeUTC   time.Time `json:"BusinessDateTimeUTC"`
	RejectedMessage       string    `json:"ErrorMessage"`
	RecordID              int       `json:"RecordID"`
	SourceSystemReference string    `json:"SourceSystemReference"`
	TransactionType       string    `json:"TransactionType"`
}

// NullInt64 is an alias for sql.NullInt64 data type
type NullInt64 struct {
	sql.NullInt64
}

// MarshalJSON for NullInt64
func (ni *NullInt64) MarshalJSON() ([]byte, error) {
	if !ni.Valid {
		return []byte("0"), nil
	}
	return json.Marshal(ni.Int64)
}
