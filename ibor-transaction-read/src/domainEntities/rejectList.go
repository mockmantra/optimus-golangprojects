package domain

// RejectList ...
type RejectList struct {
	RejectMessage string `json:"rejectMessage"`
	Index         int    `json:"index"`
}
