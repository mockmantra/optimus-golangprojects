package domain

// InvalidTransaction ...
type InvalidTransaction struct {
	Transaction string
	Errors      []string
}
