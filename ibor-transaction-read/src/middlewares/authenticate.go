package middlewares

import (
	"encoding/json"
	"fmt"
	"math"
	"net/http"
	"time"

	"stash.ezesoft.net/ipc/ezelogger"

	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/config"
	domain "stash.ezesoft.net/imsacnt/ibor-transaction-read/src/domainEntities"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/httpServices"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/logger"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/utils"

	gcontext "github.com/gorilla/context"

	"github.com/gin-gonic/gin"
)

func authenticate(c *gin.Context) {
	if !config.Config.SkipAuth(c.Request.URL.Path) {
		activityID := c.Request.Header.Get("X-Request-Id")
		log := logger.GetLogger(nil, activityID, c.Request.URL.Path)
		token := c.Request.Header.Get("Authorization")
		if len(token) == 0 || token == "undefined" {
			log.Info("Auth token not present")
			c.AbortWithStatusJSON(401, gin.H{"message": "Please login before making a request"})
			return
		}
		log.Info("Start Authentication")
		start := time.Now()
		session, err := authenticateSession(c, 0, log)
		if err != nil {
			log.Error("Error Authenticating. ", err)
			c.AbortWithStatusJSON(401, gin.H{"message": "unauthorized"})
			return
		}
		elapsed := time.Since(start)

		log.WithFields(logger.Fields{"ExecutionDuration": math.Round(elapsed.Seconds() * 1000)}).Info("End Authentication")
		gcontext.Set(c.Request, domain.UserSessionContextKey, &session.UserSession)

		log.Info("End Authenticating")
	}
	c.Next()
}

type userSession struct {
	UserSession domain.UserSession
}

func authenticateSession(c *gin.Context, count int, log *ezelogger.EzeLog) (*userSession, error) {
	log.Info("Calling Auth API. Count = ", count)

	// incrementally sleep for 0,1 and 2 seconds before call the auth api
	time.Sleep(time.Second * time.Duration(1*count))

	token := c.Request.Header.Get("Authorization")
	activityID := c.Request.Header.Get("X-Request-Id")

	url := config.Config.BaseURL + "/api/platform/v1/session"
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Add("Authorization", token)
	req.Header.Add("X-Request-Id", activityID)

	resp, err := httpServices.GetHTTPClient().Do(req)
	if err != nil {
		if count < 3 {
			return authenticateSession(c, count+1, log)
		}
		return nil, err
	}
	defer utils.CallClose(resp.Body)
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("auth API returned %d code", resp.StatusCode)
	}
	session := &userSession{}
	errors := json.NewDecoder(resp.Body).Decode(session)
	if errors != nil {
		logger.GetLogger(nil, c.Request.Header.Get("X-Request-Id"), c.Request.URL.Path).Error(errors)
	}
	return session, nil
}
