package middlewares

import (
	"github.com/gin-gonic/gin"
	gcontext "github.com/gorilla/context"
	"github.com/satori/go.uuid"
)

func assignActivityID(c *gin.Context) {
	// clear the context when done.
	defer gcontext.Clear(c.Request)

	requestID := c.Request.Header.Get("X-Request-Id")
	if requestID == "" {
		requestID = uuid.NewV4().String()
		// Set X-Request-Id header
		c.Request.Header.Set("X-Request-Id", requestID)
	}

	c.Next()
}
