package middlewares

import "github.com/gin-gonic/gin"

// RegisterMiddlewares register all the middlewares
func RegisterMiddlewares(router *gin.Engine) {
	router.Use(assignActivityID)
	router.Use(authenticate)
	router.Use(assignWorkflowContext)
	router.Use(logRequest)
}
