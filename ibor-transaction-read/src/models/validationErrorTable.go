package models

// ErrorsTable....
const (
	ErrorsTableName = "Ibor_ValidationErrors"
)

// ErrorsTable ...
type ErrorsTable struct {
	RecordID        int    `gorm:"column:RecordID"`
	ErrorMessage    string `gorm:"column:ErrorMessage"`
	TransactionType string `gorm:"column:TransactionType"`
}

// TableName ...
func (ErrorsTable) TableName() string {
	return ErrorsTableName
}
