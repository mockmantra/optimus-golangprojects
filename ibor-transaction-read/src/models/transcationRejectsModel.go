package models

import domain "stash.ezesoft.net/imsacnt/ibor-transaction-read/src/domainEntities"

// TransactionRejects response entity
type TransactionRejects struct {
	RecordID              int              `json:"recordId"`
	SourceSystem          string           `json:"sourceSystem"`
	SourceSystemReference string           `json:"sourceSystemReference"`
	EventType             string           `json:"eventType"`
	Symbol                string           `json:"symbol"`
	Quantity              domain.NullInt64 `json:"quantity"`
	Date                  string           `json:"date"`
	Time                  string           `json:"time"`
	RejectMessage         string           `json:"rejectMessage"`
	TransactionType       string           `json:"transactionType"`
}
