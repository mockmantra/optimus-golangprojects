package models

// AuroraConnection auroraConnection
type AuroraConnection struct {

	// host
	Host string `json:"host,omitempty"`

	// password
	Password string `json:"password,omitempty"`

	// port
	Port int64 `json:"port,omitempty"`

	// user
	User string `json:"user,omitempty"`
}
