package models

import "time"

// TransactionsTableName....
const (
	TransactionsTableName = "Ibor_Transactions"
)

// Transactiontable ...
type Transactiontable struct {
	RecordID            int       `gorm:"column:RecordID;primary_key;AUTO_INCREMENT"`
	Message             string    `gorm:"column:MessageBody"`
	ActivityID          string    `gorm:"column:ActivityID"`
	CreateDateTimeUTC   time.Time `gorm:"column:CreateDateTimeUTC"`
	BusinessDateTimeUTC time.Time `gorm:"column:BusinessDateTimeUTC"`
	IsDeleted           bool      `gorm:"column:IsDeleted"`
	ValidFromUTC        time.Time `gorm:"column:ValidFromUTC"`
	ValidToUTC          time.Time `gorm:"column:ValidToUTC;default:null"`
	EventTypeID         int       `gorm:"column:EventTypeID"`
	SecurityID          int       `gorm:"column:SecurityID"`
	OrderQuantity       int       `gorm:"column:OrderQuantity"`
	SourceSystemName    string    `gorm:"column:SourceSystemName"`
	UserID              int       `gorm:"column:UserID"`
	UserName            string    `gorm:"column:UserName"`
	UserSessionToken    string    `gorm:"column:UserSessionToken"`
}

// TableName ...
func (Transactiontable) TableName() string {
	return TransactionsTableName
}
