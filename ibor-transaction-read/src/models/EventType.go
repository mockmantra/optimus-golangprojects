package models

// EventType  ...
type EventType struct {
	EventTypeID            int    `json:"EventTypeId"`
	EventTypeName          string `json:"EventTypeName"`
	EventTypeDescription   string `json:"EventTypeDescription"`
	IsTradingEvent         bool   `json:"IsTradingEvent"`
	ResourceID             string `json:"ResourceId"`
	Revision               string `json:"Revision"`
	AuthorizationTopicName string `json:"AuthorizationTopicName"`
}
