package database

import (
	"bytes"
	"fmt"
	"os"
	"sync"
	"time"

	_ "github.com/go-sql-driver/mysql" //using mysql package in gorm.open()
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	appconfig "stash.ezesoft.net/imsacnt/ibor-transaction-read/src/config"
	domain "stash.ezesoft.net/imsacnt/ibor-transaction-read/src/domainEntities"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/interfaces"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/logger"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/models"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/utils"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/vault"
)

const (
	selectClause = "RecordID, EventTypeID, ErrorMessage, SourceSystemName, SecurityID, OrderQuantity, BusinessDateTimeUTC, SourceSystemReference, TransactionType"
)

// var lock sync.Mutex
var gormOpen = gorm.Open

// DataAccessor is used to connect to db and execute db operation
type DataAccessor struct {
	db      interfaces.IDB
	firm    string
	Log     interfaces.ILogger
	vault   interfaces.IVault
	dbUtils interfaces.IDBUtils
}

func init() {
	if !appconfig.IsDevMode {
		da := DataAccessorInstance()
		mapOfFirmDbConnections := da.vault.GetDbCredentials()
		if len(mapOfFirmDbConnections) < 1 {
			logger.Log.Error("Error reading tenant db credentials from vault")
			panic("Error loading db credentials from vault")
		}
	}
}

// DataAccessorInstance - Returns an instance of DataAccessor struct
func DataAccessorInstance() *DataAccessor {
	vaultInstance := vault.VaultInstance()
	return &DataAccessor{
		Log:     logger.Log,
		vault:   vaultInstance,
		dbUtils: &utils.DbUtils{},
	}
}

// ReadTransaction will read the records from the innerjoin of transaction & errors table in the database.
func (da *DataAccessor) ReadTransaction(context *domain.WorkflowContext, startDate time.Time, endDate time.Time,
	eventTypeID int, sourceSystem string, txnTables []string) (transactions []*domain.Transaction, e error) {
	//Uncomment to see underlying sql query
	// da.db.LogMode(true)

	txnChannel := make(chan []*domain.Transaction)
	var wg sync.WaitGroup

	for i := range txnTables {
		wg.Add(1)
		go func(tableName string) {
			rows, e := da.db.Table(tableName).
				Where("BusinessDateTimeUTC BETWEEN ? AND ?", startDate, endDate).
				Where(&models.Transactiontable{EventTypeID: eventTypeID, SourceSystemName: sourceSystem}).
				Where(buildTxnTypeWhereCondition(tableName)).
				Select(selectClause).
				Joins(buildJoinClause(tableName)).
				Rows()

			if e != nil {
				context.Logger.Error(e)
				txnChannel <- nil
			} else {
				tx, e := da.dbUtils.CreateRejectedTxnMap(context, rows)
				if e == nil {
					txnChannel <- tx
				} else {
					context.Logger.Error(e)
					txnChannel <- nil
				}
			}
		}(txnTables[i])
	}

	go func() {
		for txn := range txnChannel {
			transactions = append(transactions, txn...)
			wg.Done()
		}
	}()

	wg.Wait()
	return transactions, e
}

func buildTxnTypeWhereCondition(tableName string) string {
	var txnTypeBuffer bytes.Buffer
	txnTypeBuffer.WriteString(models.ErrorsTable{}.TableName())
	txnTypeBuffer.WriteString(".TransactionType = '")
	switch tableName {
	case "Ibor_Transactions":
		txnTypeBuffer.WriteString("EquityTransaction")
	case "Ibor_Fx_Transactions":
		txnTypeBuffer.WriteString("FxTransaction")
	case "Ibor_NTE_Transactions":
		txnTypeBuffer.WriteString("NTETransaction")
	case "Ibor_Options_Transactions":
		txnTypeBuffer.WriteString("OptionsTransaction")
	case "Ibor_Transfers_Transactions":
		txnTypeBuffer.WriteString("TransfersTransaction")
	case "Ibor_Swaps_Transactions":
		txnTypeBuffer.WriteString("EquitySwapsTransaction")
	}

	txnTypeBuffer.WriteString("'")
	return txnTypeBuffer.String()
}

func buildJoinClause(tableName string) string {
	var buffer bytes.Buffer
	joinCls := "inner join Ibor_ValidationErrors on Ibor_ValidationErrors.TransactionRecordID = "
	buffer.WriteString(joinCls)
	buffer.WriteString(tableName)
	buffer.WriteString(".RecordID")
	return buffer.String()
}

// ReadErrors ...
func (da *DataAccessor) ReadErrors(context *domain.WorkflowContext, recorID string, txnType string) ([]domain.RejectList, error) {
	//Uncomment to see underlying sql query
	// da.db.LogMode(true)

	rows, err := da.db.Table("Ibor_ValidationErrors").
		Where("Ibor_ValidationErrors.TransactionRecordID = ?", recorID).
		Where("Ibor_ValidationErrors.TransactionType = ?", txnType).
		Select("Ibor_ValidationErrors.ErrorMessage").
		Rows()

	if err != nil {
		context.Logger.Error(err)
		return nil, err
	}

	defer func() {
		rows.Close()
	}()

	return da.dbUtils.CreateErrorsList(context, rows)
}

func (da *DataAccessor) getTenantCredentials(firmToken string) (*models.AuroraConnection, error) {
	con, ok := da.vault.GetDbCredentials()[firmToken]
	if !ok {
		return nil, fmt.Errorf("No db credentials found for firm: -%s-", firmToken)
	}
	da.Log.Debugf("Vault credentials loaded for tenant %s", firmToken)
	return con, nil
}

func (da *DataAccessor) getSchemaName() string {
	return fmt.Sprintf(appconfig.DB_SCHEMA_TEMPLATE, da.firm)
}

// Close closes the database connection of a data accessor
func (da *DataAccessor) Close() (err error) {

	if da.db != nil {
		if err = da.db.Close(); err != nil {
			err = errors.Wrapf(err, "Error while closing connection with database %v", da.getSchemaName())
			da.Log.Error(err)
		} else {
			err = nil
		}
	}
	*da = DataAccessor{}
	return err
}

// Connect establishes connection to firm's database
func (da *DataAccessor) Connect(firmAuthToken string) (err error) {
	return retry(appconfig.DB_CONNECT_ATTEMPTS, time.Second, func() error {

		connectionString, err := da.getConnectionString(firmAuthToken)
		if err != nil {
			return err
		}

		db, err := gormOpen("mysql", connectionString)
		if err != nil {
			return err
		}

		da.db = db
		return nil
	})
}

func (da *DataAccessor) getConnectionString(firmToken string) (connectionString string, err error) {
	if appconfig.IsDevMode {
		connectionString = os.Getenv("Connection_String")
		if len(connectionString) == 0 {
			return "", errors.New("Connection string is not set")
		}
		return
	}

	connectionInfo, er := da.vault.GetTenantCredentials(firmToken)

	if er != nil {
		return "", er
	}
	da.firm = firmToken
	connectionString = buildConnectionString(connectionInfo, da.getSchemaName())
	return
}

func buildConnectionString(connectionInfo *models.AuroraConnection, dbName string) string {
	return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?parseTime=True&loc=UTC",
		connectionInfo.User,
		connectionInfo.Password,
		connectionInfo.Host,
		connectionInfo.Port,
		dbName)
}
func retry(attempts int, sleep time.Duration, f func() error) (err error) {
	for {
		if err = f(); err == nil {
			return nil
		}
		attempts--
		if attempts <= 0 {
			break
		}
		time.Sleep(sleep)
	}
	return errors.Wrap(err, "Failed to open database connection. Exiting...") // do not log connectionstring
}
