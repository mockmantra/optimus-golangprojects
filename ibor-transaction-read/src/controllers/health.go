package controllers

import (
	"github.com/gin-gonic/gin"
)

func registerHealthCheck(router *gin.RouterGroup) {
	router.GET("/health", healthCheck)
}

func healthCheck(c *gin.Context) {
	c.JSON(200, gin.H{"message": "OK"})

}
