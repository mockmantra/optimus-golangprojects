package controllers

import (
	"net/url"
	"strings"

	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/database"

	"github.com/gin-gonic/gin"
	gcontext "github.com/gorilla/context"
	domain "stash.ezesoft.net/imsacnt/ibor-transaction-read/src/domainEntities"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/handlers"
)

func registerRejectErrors(router *gin.RouterGroup) {
	router.GET("/reject-errors", rejectErrors)
}

func rejectErrors(c *gin.Context) {
	workflowContext := gcontext.Get(c.Request, domain.WorkflowContextKey).(*domain.WorkflowContext)
	workflowContext.Logger.Info("Starting Rejected-Invalid-Messages read request")

	query, err := url.ParseQuery(c.Request.URL.RawQuery)
	if err != nil {
		c.JSON(400, gin.H{"message": "Query parametrs missing"})
		return
	}

	recordID := query.Get("recordId")

	txnType := query.Get("transactionType")

	if recordID == "" || txnType == "" {
		c.JSON(400, "Both recordID and txnType are required fields")
		return
	}

	if validateTxnType(strings.ToLower(txnType)) == false {
		c.JSON(400, "Invalid txnType provided. Supported types: fxtransaction, equitytransaction, equityswapstransaction, ntetransaction, optionstranscation, transferstransaction")
		return
	}

	da := database.DataAccessorInstance()
	dbResult, err := handlers.ReadRejectErrors(da, workflowContext, recordID, strings.ToLower(txnType))
	if err != nil {
		workflowContext.Logger.Error(err)
		c.JSON(500, "Oops something went wrong. Please retry.")
		return
	}

	c.JSON(200, dbResult)
}

func validateTxnTypeForRejectErrors(txnType string) bool {
	if _, ok := GetValidTxnTypes()[txnType]; ok {
		return true
	}
	return false
}
