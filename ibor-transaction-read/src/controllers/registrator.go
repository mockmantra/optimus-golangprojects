package controllers

import (
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/config"

	"github.com/gin-gonic/gin"
)

// RegisterRoutes register all the routes with gin
func RegisterRoutes(router *gin.Engine) {
	transactionReadRouter := router.Group(config.Config.APIBase)
	registerHealthCheck(transactionReadRouter)
	registerTransactionRejects(transactionReadRouter)
	registerRejectErrors(transactionReadRouter)
}
