package controllers

import (
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"

	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/database"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/httpServices"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/providers"

	"github.com/gin-gonic/gin"
	gcontext "github.com/gorilla/context"
	domain "stash.ezesoft.net/imsacnt/ibor-transaction-read/src/domainEntities"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/handlers"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/logger"
)

const (
	startDate       = "startDate"
	endDate         = "endDate"
	eventType       = "eventType"
	sourceSystem    = "sourceSystem"
	transactionType = "transactionType"
	dateRegEx       = "^(0[1-9]|1[0-2])\\/(0[1-9]|1\\d|2\\d|3[01])\\/\\d{4}$"
	layout          = "01/02/2006"
)

func registerTransactionRejects(router *gin.RouterGroup) {
	router.GET("/transaction-rejects", transactionRejects)
}

func transactionRejects(c *gin.Context) {
	workflowContext := gcontext.Get(c.Request, domain.WorkflowContextKey).(*domain.WorkflowContext)
	workflowContext.Logger.Info("Starting Tx-Rejects read request")

	query, err := url.ParseQuery(c.Request.URL.RawQuery)
	if err != nil {
		c.JSON(400, gin.H{"message": "Query parametrs missing"})
		return
	}

	matchStart, _ := regexp.MatchString(dateRegEx, query.Get(startDate))
	matchEnd, _ := regexp.MatchString(dateRegEx, query.Get(endDate))

	if !matchEnd || !matchStart {
		c.JSON(400, gin.H{"message": "Incorrect start or end Date provided in request. MM/DD/YYYY format only"})
		return
	}

	parsedStart, _ := time.Parse(layout, query.Get(startDate))
	parsedEnd, _ := time.Parse(layout, query.Get(endDate))

	if parsedStart.After(parsedEnd) {
		c.JSON(400, gin.H{"message": "Start date is greater than end date"})
		return
	}

	// default endDate till the last minute/second of the day to filter correctly
	parsedEnd = parsedEnd.Add((time.Hour*23 + time.Minute*59 + time.Second*59))

	sourceSystem := query.Get(sourceSystem)

	eventType, err := strconv.Atoi(query.Get(eventType))
	if err != nil {
		workflowContext.Logger.Info(err)
	}

	transactionType := query.Get(transactionType)
	if validateTxnType(strings.ToLower(transactionType)) == false {
		c.JSON(400, gin.H{"message": "Invalid transactionType in input. Supported types: fxTransaction, equityTransaction, equitySwapsTransaction, nteTransaction, optionsTransaction, transfersTransaction"})
		return
	}

	da := database.DataAccessorInstance()
	dbResult, err := handlers.ReadRejectedTransactions(da, workflowContext, parsedStart, parsedEnd, eventType, sourceSystem, strings.ToLower(transactionType))
	if err != nil {
		workflowContext.Logger.Error(err)
		c.JSON(500, gin.H{"message": "Oops something went wrong. Please retry."})
		return
	}
	eventProvider := providers.GetEventProviderInstance(c, httpServices.GetHTTPClient())
	symbolProvider := providers.GetSymbolProviderInstance(c, httpServices.GetHTTPClient())
	transactionTranslator := handlers.GetTransactionTranslatorInstance(&eventProvider, &symbolProvider, logger.Log.WithContext(c))
	apiResponse, err := transactionTranslator.TranslateToModel(dbResult)
	if err != nil {
		workflowContext.Logger.Error(err)
		c.JSON(500, gin.H{"message": "Oops something went wrong. Please retry."})
		return
	}

	c.JSON(200, apiResponse)
}

func validateTxnType(txnType string) bool {
	if txnType == "" {
		return true
	}

	if _, ok := GetValidTxnTypes()[txnType]; ok {
		return true
	}
	return false
}

// GetValidTxnTypes ...
func GetValidTxnTypes() map[string]string {
	return map[string]string{
		"fxtransaction":          "fxtransaction",
		"equitytransaction":      "equitytransaction",
		"ntetransaction":         "ntetransaction",
		"optionstransaction":     "optionstransaction",
		"transferstransaction":   "transferstransaction",
		"equityswapstransaction": "equityswapstransaction",
	}
}
