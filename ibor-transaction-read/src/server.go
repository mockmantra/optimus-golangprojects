package main

import (
	"os"

	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/app"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/config"
)

func main() {
	initConfig()
	app.Run()
}

func initConfig() {
	err := config.InitializeFromVault()
	if err != nil {
		os.Exit(1)
	}
}
