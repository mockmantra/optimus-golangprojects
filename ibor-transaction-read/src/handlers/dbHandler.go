package handlers

import (
	"time"

	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/interfaces"

	domain "stash.ezesoft.net/imsacnt/ibor-transaction-read/src/domainEntities"
)

// ReadRejectedTransactions ...
func ReadRejectedTransactions(da interfaces.IDataAccessor, context *domain.WorkflowContext, startDate time.Time, endDate time.Time, eventType int, sourceSystem string, transactionType string) ([]*domain.Transaction, error) {
	err := da.Connect(context.UserSession.FirmAuthToken)

	if err != nil {
		context.Logger.Error(err)
		return nil, err
	}

	defer func() {
		err = da.Close()
		if err != nil {
			context.Logger.Error(err)
		}
	}()

	return da.ReadTransaction(context, startDate, endDate, eventType, sourceSystem, getTxnTables(transactionType))
}

// ReadRejectErrors ...
func ReadRejectErrors(da interfaces.IDataAccessor, context *domain.WorkflowContext, recordId string, txnType string) ([]domain.RejectList, error) {
	err := da.Connect(context.UserSession.FirmAuthToken)

	if err != nil {
		context.Logger.Error(err)
		return nil, err
	}

	defer func() {
		err = da.Close()
		if err != nil {
			context.Logger.Error(err)
		}
	}()

	return da.ReadErrors(context, recordId, txnType)
}

func getTxnTables(requestedTxnType string) []string {
	m := map[string]string{
		"fxtransaction":          "Ibor_Fx_Transactions",
		"equitytransaction":      "Ibor_Transactions",
		"ntetransaction":         "Ibor_NTE_Transactions",
		"optionstransaction":     "Ibor_Options_Transactions",
		"transferstransaction":   "Ibor_Transfers_Transactions",
		"equityswapstransaction": "Ibor_Swaps_Transactions",
	}

	if value, ok := m[requestedTxnType]; ok {
		return []string{value}
	}
	return []string{"Ibor_Transactions", "Ibor_Fx_Transactions", "Ibor_NTE_Transactions", "Ibor_Options_Transactions", "Ibor_Transfers_Transactions", "Ibor_Swaps_Transactions"}
}
