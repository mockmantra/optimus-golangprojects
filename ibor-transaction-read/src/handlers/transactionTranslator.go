package handlers

import (
	domain "stash.ezesoft.net/imsacnt/ibor-transaction-read/src/domainEntities"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/interfaces"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/models"
)

// TransactionTranslator Struct ...
type TransactionTranslator struct {
	eventProvider          interfaces.IEventProvider
	securitySymbolProvider interfaces.ISecuritySymbolProvider
	logger                 interfaces.ILogger
}

type securitiesMap map[int]models.Security
type eventTypesMap map[int]models.EventType

//GetTransactionTranslatorInstance ...
func GetTransactionTranslatorInstance(eventProvider interfaces.IEventProvider, securitySymbolProvider interfaces.ISecuritySymbolProvider, logger interfaces.ILogger) *TransactionTranslator {

	return &TransactionTranslator{
		eventProvider:          eventProvider,
		securitySymbolProvider: securitySymbolProvider,
		logger:                 logger,
	}

}

// Translate will translate <business entity> to <api response entity>
func translate(transaction *domain.Transaction, eventTypes eventTypesMap, securities securitiesMap) models.TransactionRejects {

	//datetimeUtc := "2018-09-08T13:25:43.0000000Z"
	//t, _ := time.Parse(time.RFC3339, datetimeUtc)
	t := transaction.BusinessDateTimeUTC
	dateString := t.Format("01/02/2006") // MM/DD/YYYY
	timeString := t.Format("03:04:05")

	transactionRejects := models.TransactionRejects{
		RecordID:              transaction.RecordID,
		SourceSystem:          transaction.SourceSystemName,
		Date:                  dateString,
		Time:                  timeString,
		EventType:             eventTypes.getEventTypeName(transaction.EventTypeID),
		Symbol:                securities.getSecuritySymbol(transaction.SecurityID),
		Quantity:              transaction.OrderQuantity,
		RejectMessage:         transaction.RejectedMessage,
		SourceSystemReference: transaction.SourceSystemReference,
		TransactionType:       transaction.TransactionType,
	}
	return transactionRejects
}

func (eventTypes eventTypesMap) getEventTypeName(eventID int) string {
	var eventTypeName string
	if eventType, ok := eventTypes[eventID]; ok {
		eventTypeName = eventType.EventTypeName
	}
	return eventTypeName
}
func (securities securitiesMap) getSecuritySymbol(securityID int) string {
	var securitySymbol string

	if security, ok := securities[securityID]; ok {
		securitySymbol = security.Symbol
	}
	return securitySymbol
}

// TranslateToModel will translate <business entity> to <api response entity>
func (transactionTranslatorInstance TransactionTranslator) TranslateToModel(transactionList []*domain.Transaction) ([]models.TransactionRejects, error) {

	if transactionList == nil {
		return []models.TransactionRejects{}, nil
	}

	eventTypes, err := transactionTranslatorInstance.eventProvider.GetAllEventTypes()
	if err != nil {
		// log error
		transactionTranslatorInstance.logger.Errorf("Error while getting eventTypes: %s", err)
		return nil, err
	}

	symbols, err := transactionTranslatorInstance.securitySymbolProvider.GetSecuritySymbols(getSecurityIds(transactionList))

	if err != nil {
		// log error
		transactionTranslatorInstance.logger.Errorf("Error while getting SecuritySymbols: %s", err)
		return nil, err
	}

	transactionRejects := []models.TransactionRejects{}

	for _, transcationRejectsDB := range transactionList {
		transactionRejects = append(transactionRejects, translate(transcationRejectsDB, eventTypes, symbols))
	}

	return transactionRejects, nil
}

func getSecurityIds(transactionList []*domain.Transaction) []int {
	ids := []int{}
	contains := make(map[int]bool)
	for _, item := range transactionList {
		if _, ok := contains[item.SecurityID]; !ok {
			contains[item.SecurityID] = true
			ids = append(ids, item.SecurityID)
		}

	}
	return ids
}
