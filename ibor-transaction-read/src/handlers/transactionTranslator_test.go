package handlers

import (
	"net/http/httptest"
	"reflect"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	domain "stash.ezesoft.net/imsacnt/ibor-transaction-read/src/domainEntities"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/interfaces"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/logger"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/mocks"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/models"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/providers"
)

func TestGetTransactionTranslatorInstance(t *testing.T) {
	context, _ := gin.CreateTestContext(httptest.NewRecorder())
	instance := GetTransactionTranslatorInstance(&providers.EventProvider{}, &providers.SecuritySymbolProvider{}, logger.Log.WithContext(context))
	assert.NotNil(t, instance)
}

func Test_transactionTranslator_TranslateToModel(t *testing.T) {

	eventProvider := &mocks.EventProvider{}
	eventProvider.
		On("GetAllEventTypes").
		Return(getEventTypes(), nil)

	securityProvider := &mocks.SecuritySymbolProvider{}
	securityProvider.
		On("GetSecuritySymbols", mock.Anything).
		Return(getSecurities(), nil)

	transactions := getTransactions()
	context, _ := gin.CreateTestContext(httptest.NewRecorder())
	type fields struct {
		eventProvider          interfaces.IEventProvider
		securitySymbolProvider interfaces.ISecuritySymbolProvider
		logger                 interfaces.ILogger
	}
	type args struct {
		transactionList []*domain.Transaction
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   []models.TransactionRejects
	}{
		{
			name: "name",
			fields: fields{
				eventProvider:          eventProvider,
				securitySymbolProvider: securityProvider,
				logger:                 logger.Log.WithContext(context),
			},
			args: args{
				transactionList: transactions,
			},
			want: []models.TransactionRejects{
				models.TransactionRejects{
					RecordID:      transactions[0].RecordID,
					SourceSystem:  transactions[0].SourceSystemName,
					EventType:     "event Name 1",
					Symbol:        "symbol 1",
					Quantity:      transactions[0].OrderQuantity,
					Date:          transactions[0].BusinessDateTimeUTC.Format("01/02/2006"),
					Time:          transactions[0].BusinessDateTimeUTC.Format("03:04:05"),
					RejectMessage: transactions[0].RejectedMessage,
				},
				models.TransactionRejects{
					RecordID:      transactions[1].RecordID,
					SourceSystem:  transactions[1].SourceSystemName,
					EventType:     "event Name 2",
					Symbol:        "symbol 2",
					Quantity:      transactions[1].OrderQuantity,
					Date:          transactions[1].BusinessDateTimeUTC.Format("01/02/2006"),
					Time:          transactions[1].BusinessDateTimeUTC.Format("03:04:05"),
					RejectMessage: transactions[1].RejectedMessage,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			transcationTranslatorInstance := GetTransactionTranslatorInstance(tt.fields.eventProvider, tt.fields.securitySymbolProvider, tt.fields.logger)
			if got, _ := transcationTranslatorInstance.TranslateToModel(tt.args.transactionList); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("transactionTranslator.TranslateToModel() was incorrect got = %v, want %v", got, tt.want)
			}
		})
	}
}

func getEventTypes() map[int]models.EventType {
	eventTypeMap := make(map[int]models.EventType)

	eventType := models.EventType{
		EventTypeID:            1,
		EventTypeName:          "event Name 1",
		EventTypeDescription:   "Event description",
		IsTradingEvent:         true,
		ResourceID:             "resource Id",
		Revision:               "revision",
		AuthorizationTopicName: "Authorization",
	}
	eventTypeMap[1] = eventType
	eventTypeMap[2] = models.EventType{
		EventTypeID:            2,
		EventTypeName:          "event Name 2",
		EventTypeDescription:   "Event description",
		IsTradingEvent:         true,
		ResourceID:             "resource Id",
		Revision:               "revision",
		AuthorizationTopicName: "Authorization",
	}
	return eventTypeMap
}
func getSecurities() map[int]models.Security {
	securityMap := make(map[int]models.Security)

	securityMap[1] = models.Security{
		ID:     1,
		Symbol: "symbol 1",
	}
	securityMap[2] = models.Security{
		ID:     2,
		Symbol: "symbol 2",
	}
	return securityMap
}
func getTransactions() []*domain.Transaction {
	var orderQuantity domain.NullInt64
	orderQuantity.Int64 = 234
	return []*domain.Transaction{
		&domain.Transaction{
			EventTypeID:         1,
			OrderQuantity:       orderQuantity,
			SecurityID:          1,
			SourceSystemName:    "trading",
			BusinessDateTimeUTC: time.Now(),
			RejectedMessage:     "error 1",
			RecordID:            1,
		},
		&domain.Transaction{
			EventTypeID:         2,
			OrderQuantity:       orderQuantity,
			SecurityID:          2,
			SourceSystemName:    "trading",
			BusinessDateTimeUTC: time.Now(),
			RejectedMessage:     "error 1",
			RecordID:            1,
		},
	}
}

func Test_getSecurityIds(t *testing.T) {
	type args struct {
		transactionList []*domain.Transaction
	}
	var orderQuantity domain.NullInt64
	orderQuantity.Int64 = 234
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "should return unique security ids",
			args: args{
				transactionList: []*domain.Transaction{
					&domain.Transaction{
						EventTypeID:         1,
						OrderQuantity:       orderQuantity,
						SecurityID:          1,
						SourceSystemName:    "trading",
						BusinessDateTimeUTC: time.Now(),
						RejectedMessage:     "error 1",
						RecordID:            1,
					},
					&domain.Transaction{
						EventTypeID:         1,
						OrderQuantity:       orderQuantity,
						SecurityID:          1,
						SourceSystemName:    "trading",
						BusinessDateTimeUTC: time.Now(),
						RejectedMessage:     "error 1",
						RecordID:            1,
					},
					&domain.Transaction{
						EventTypeID:         2,
						OrderQuantity:       orderQuantity,
						SecurityID:          2,
						SourceSystemName:    "trading",
						BusinessDateTimeUTC: time.Now(),
						RejectedMessage:     "error 1",
						RecordID:            1,
					},
				},
			},
			want: []int{1, 2},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getSecurityIds(tt.args.transactionList); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getSecurityIds() = %v, want %v", got, tt.want)
			}
		})
	}
}
