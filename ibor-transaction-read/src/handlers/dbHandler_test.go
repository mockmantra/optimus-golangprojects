package handlers

import (
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"github.com/stretchr/testify/mock"
	domain "stash.ezesoft.net/imsacnt/ibor-transaction-read/src/domainEntities"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/logger"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/mocks"
)

func TestReadRejectedTransactions_AllTxnTypes(t *testing.T) {
	mockDA := new(mocks.IDataAccessor)
	var orderQuantity domain.NullInt64
	orderQuantity.Int64 = 600
	txn1 := domain.Transaction{EventTypeID: 2, OrderQuantity: orderQuantity, RecordID: 2, RejectedMessage: "Rejected", SecurityID: 343, SourceSystemName: "Trading", SourceSystemReference: "400", TransactionType: "FxTransaction"}
	txn2 := domain.Transaction{EventTypeID: 2, OrderQuantity: orderQuantity, RecordID: 2, RejectedMessage: "Rejected", SecurityID: 343, SourceSystemName: "Trading", SourceSystemReference: "600", TransactionType: "EquityTransaction"}
	trs := []*domain.Transaction{&txn1}
	trs = append(trs, &txn2)
	mockDA.On("Connect", mock.Anything).Return(nil)
	mockDA.On("ReadTransaction", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(trs, nil)
	mockDA.On("Close").Return(nil)
	workflowContext := &domain.WorkflowContext{
		UserSession: &domain.UserSession{FirmAuthToken: "xyz"},
		Logger:      logger.GetLogger(nil, "", ""),
	}

	result, err := ReadRejectedTransactions(mockDA, workflowContext, time.Now(), time.Now(), 12, "Trading", "")
	if err != nil {
		assert.Error(t, err)
	}
	assert.NotNil(t, result)
	assert.Equal(t, result, trs)
}

func TestReadRejectedTransactions_EquityTransactionType(t *testing.T) {
	mockDA := new(mocks.IDataAccessor)
	var orderQuantity domain.NullInt64
	orderQuantity.Int64 = 600
	txn := domain.Transaction{EventTypeID: 2, OrderQuantity: orderQuantity, RecordID: 2, RejectedMessage: "Rejected", SecurityID: 343, SourceSystemName: "Trading", SourceSystemReference: "400", TransactionType: "EquityTransaction"}
	trs := []*domain.Transaction{&txn}
	mockDA.On("Connect", mock.Anything).Return(nil)
	mockDA.On("ReadTransaction", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(trs, nil)
	mockDA.On("Close").Return(nil)
	workflowContext := &domain.WorkflowContext{
		UserSession: &domain.UserSession{FirmAuthToken: "xyz"},
		Logger:      logger.GetLogger(nil, "", ""),
	}

	result, err := ReadRejectedTransactions(mockDA, workflowContext, time.Now(), time.Now(), 12, "Trading", "EquityTransaction")
	if err != nil {
		assert.Error(t, err)
	}
	assert.NotNil(t, result)
	assert.Equal(t, result, trs)
}

func TestReadRejectedTransactions_ErrorConnecting(t *testing.T) {
	mockDA := new(mocks.IDataAccessor)
	mockDA.On("Connect", mock.Anything).Return(errors.New("Error Connecting"))
	mockDA.On("Close").Return(nil)
	workflowContext := &domain.WorkflowContext{
		UserSession: &domain.UserSession{FirmAuthToken: "xyz"},
		Logger:      logger.GetLogger(nil, "", ""),
	}
	result, err := ReadRejectedTransactions(mockDA, workflowContext, time.Now(), time.Now(), 12, "Trading", "")
	if err != nil {
		assert.Error(t, err)
	}
	assert.Nil(t, result)
}

func TestReadRejectedTransactions_ErrorReading(t *testing.T) {
	mockDA := new(mocks.IDataAccessor)
	mockDA.On("Connect", mock.Anything).Return(nil)
	mockDA.On("Close").Return(nil)
	mockDA.On("ReadTransaction", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("error reading."))
	workflowContext := &domain.WorkflowContext{
		UserSession: &domain.UserSession{FirmAuthToken: "xyz"},
		Logger:      logger.GetLogger(nil, "", ""),
	}
	result, err := ReadRejectedTransactions(mockDA, workflowContext, time.Now(), time.Now(), 12, "Trading", "")
	if err != nil {
		assert.Error(t, err)
	}
	assert.Nil(t, result)
}

func TestReadRejectErrors(t *testing.T) {
	mockDA := new(mocks.IDataAccessor)
	errs := []domain.RejectList{domain.RejectList{RejectMessage: "Invalid OrderQuantity", Index: 1}, domain.RejectList{RejectMessage: "Invalid PositionTags as the book type is 3", Index: 1}}
	mockDA.On("Connect", mock.Anything).Return(nil)
	mockDA.On("Close").Return(nil)
	mockDA.On("ReadErrors", mock.Anything, mock.Anything, mock.Anything).Return(errs, nil)
	workflowContext := &domain.WorkflowContext{
		UserSession: &domain.UserSession{FirmAuthToken: "xyz"},
		Logger:      logger.GetLogger(nil, "", ""),
	}
	result, err := ReadRejectErrors(mockDA, workflowContext, "2", "fxTransaction")
	if err != nil {
		assert.Error(t, err)
	}
	assert.NotNil(t, result)
}

func TestReadRejectErrors_ConnectionError(t *testing.T) {
	mockDA := new(mocks.IDataAccessor)
	mockDA.On("Connect", mock.Anything).Return(errors.New("error connecting"))
	mockDA.On("Close").Return(nil)
	workflowContext := &domain.WorkflowContext{
		UserSession: &domain.UserSession{FirmAuthToken: "xyz"},
		Logger:      logger.GetLogger(nil, "", ""),
	}
	result, err := ReadRejectErrors(mockDA, workflowContext, "2", "fxTransaction")
	if err != nil {
		assert.Error(t, err)
	}
	assert.Nil(t, result)
}

func TestReadRejectErrors_ErrorGettingErrors(t *testing.T) {
	mockDA := new(mocks.IDataAccessor)
	mockDA.On("Connect", mock.Anything).Return(nil)
	mockDA.On("Close").Return(nil)
	mockDA.On("ReadErrors", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("error getting errors from db"))
	workflowContext := &domain.WorkflowContext{
		UserSession: &domain.UserSession{FirmAuthToken: "xyz"},
		Logger:      logger.GetLogger(nil, "", ""),
	}
	result, err := ReadRejectErrors(mockDA, workflowContext, "2", "fxTransaction")
	if err != nil {
		assert.Error(t, err)
	}
	assert.Nil(t, result)
}
