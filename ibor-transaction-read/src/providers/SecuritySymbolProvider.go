package providers

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"time"

	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/config"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/logger"

	"github.com/gin-gonic/gin"
	gcontext "github.com/gorilla/context"
	domain "stash.ezesoft.net/imsacnt/ibor-transaction-read/src/domainEntities"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/models"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/utils"
)

// SecuritySymbolProvider ...
type SecuritySymbolProvider struct {
	context    *gin.Context
	httpClient *http.Client
}

// Securities ...
type Securities []models.Security

//SecurityResponse ...
type SecurityResponse struct {
	Data struct {
		Securities Securities `json:"securities"`
	} `json:"data"`
}

// GetSymbolProviderInstance ...
func GetSymbolProviderInstance(context *gin.Context, httpClient *http.Client) SecuritySymbolProvider {
	return SecuritySymbolProvider{
		context:    context,
		httpClient: httpClient,
	}
}

// GetSecuritySymbols ...
func (symbolProvider *SecuritySymbolProvider) GetSecuritySymbols(idList []int) (map[int]models.Security, error) {

	context := symbolProvider.context
	//todo: need to use token from reqeust
	token := context.Request.Header.Get("Authorization")
	// tokenTemp := "09368290_2ab621687cc868c8" + ":"
	// token := "Basic " + base64.StdEncoding.EncodeToString([]byte(tokenTemp))
	activityID := context.Request.Header.Get("X-Request-Id")
	if len(token) == 0 {
		return map[int]models.Security{}, errors.New("Auth token not present")
	}
	url := makeRequestURL(idList)
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Add("Authorization", token)
	req.Header.Add("X-Request-Id", activityID)

	workflowContext := gcontext.Get(context.Request, domain.WorkflowContextKey).(*domain.WorkflowContext)
	log := workflowContext.Logger.WithFields(logger.Fields{"Url": url})
	log.Info("Start api Request")
	start := time.Now()

	resp, err := symbolProvider.httpClient.Do(req)
	log.WithFields(logger.Fields{"ResponseTime": time.Since(start).Nanoseconds() / 1000000}).Info("End api Request")
	if err != nil {
		return map[int]models.Security{}, err
	}
	defer utils.CallClose(resp.Body)
	if resp.StatusCode != 200 {
		return map[int]models.Security{}, fmt.Errorf("coredata returned %d status code for symbols", resp.StatusCode)
	}
	response := &SecurityResponse{}
	errors := json.NewDecoder(resp.Body).Decode(response)
	if errors != nil {
		return map[int]models.Security{}, errors
	}

	securityMap := make(map[int]models.Security)
	for _, security := range response.Data.Securities {
		securityMap[security.ID] = security
	}

	return securityMap, nil
}

func makeRequestURL(idList []int) string {
	query := makeQuery(idList)
	//todo: need to update base url
	//baseURL := "https://castle.ezesoftcloud.com"
	requestURL := config.Config.BaseURL + "/api/coredata/v1/graphql"
	safeQuery := url.QueryEscape(query)

	return fmt.Sprintf("%s?query=%s", requestURL, safeQuery)
}
func makeQuery(ids []int) string {

	idList := strings.Trim(strings.Join(strings.Fields(fmt.Sprint(ids)), ","), "[]")
	queryFormat := `{
		securities (ids:[%s]) {
		  id,
		  symbol
		}
	   }`

	query := fmt.Sprintf(queryFormat, idList)
	return query
}
