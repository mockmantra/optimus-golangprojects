package providers

import (
	"bytes"
	"fmt"
	"github.com/gin-gonic/gin"
	gcontext "github.com/gorilla/context"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"reflect"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/config"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/domainEntities"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/logger"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/mocks"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/models"
	"strings"
	"testing"
)

func TestSecuritySymbolProvider_GetSecuritySymbols(t *testing.T) {
	buf := new(bytes.Buffer)

	context, _ := gin.CreateTestContext(httptest.NewRecorder())
	context.Request, _ = http.NewRequest("GET", makeTestRequestURL([]int{1, 2}), buf)
	workflowContext := &domain.WorkflowContext{
		Logger: logger.GetLogger(nil, "", ""),
	}
	gcontext.Set(context.Request, domain.WorkflowContextKey, workflowContext)
	context.Request.Header.Set("Authorization", "MDkzNjgyOTBfMmFiNjIxNjg3Y2M4NjhjODo=")
	context.Request.Header.Set("X-Request-Id", "5fb91f90-e79d-4737-8f5c-26960b483418")

	client := mocks.NewTestClient(func(req *http.Request) *http.Response {

		assert.Equal(t, req.URL.Path, "/api/coredata/v1/graphql")
		assert.Equal(t, req.Header.Get("Authorization"), "MDkzNjgyOTBfMmFiNjIxNjg3Y2M4NjhjODo=")
		assert.Equal(t, req.Header.Get("X-Request-Id"), "5fb91f90-e79d-4737-8f5c-26960b483418")

		return &http.Response{
			StatusCode: 200,
			// Send response to be tested
			Body: ioutil.NopCloser(bytes.NewBufferString(`{
				"data": {
				  "securities": [
					{
					  "id": 1,
					  "symbol": "AFN"
					},
					{
					  "id": 2,
					  "symbol": "ALL"
					}
				  ]
				}
			  }`)),
			// Must be set to non-nil value or it panics
			Header: make(http.Header),
		}
	})

	expected := make(map[int]models.Security)
	expected[1] = models.Security{
		ID:     1,
		Symbol: "AFN",
	}
	expected[2] = models.Security{
		ID:     2,
		Symbol: "ALL",
	}

	instance := GetSymbolProviderInstance(context, client)
	result, _ := instance.GetSecuritySymbols([]int{1, 2})

	assert.NotNil(t, result)
	assert.Equal(t, reflect.DeepEqual(result, expected), true)
}

func makeTestRequestURL(idList []int) string {
	query := makeTestQuery(idList)

	requestURL := config.Config.BaseURL + "/api/coredata/v1/graphql"
	safeQuery := url.QueryEscape(query)

	return fmt.Sprintf("%s?query=%s", requestURL, safeQuery)
}
func makeTestQuery(ids []int) string {

	idList := strings.Trim(strings.Join(strings.Fields(fmt.Sprint(ids)), ","), "[]")
	queryFormat := `{
		securities (ids:[%s]) {
		  id,
		  symbol
		}
	   }`

	query := fmt.Sprintf(queryFormat, idList)
	return query
}
