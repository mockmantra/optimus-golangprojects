package providers

import (
	"bytes"
	"github.com/gin-gonic/gin"
	gcontext "github.com/gorilla/context"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"reflect"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/domainEntities"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/logger"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/mocks"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/models"
	"testing"
)

func TestEventProvider_GetAllEventTypes(t *testing.T) {

	buf := new(bytes.Buffer)

	context, _ := gin.CreateTestContext(httptest.NewRecorder())

	context.Request, _ = http.NewRequest("GET", "/api/securitymaster/v1/Templates/GetAllEventTypes", buf)
	workflowContext := &domain.WorkflowContext{
		Logger: logger.GetLogger(nil, "", ""),
	}
	gcontext.Set(context.Request, domain.WorkflowContextKey, workflowContext)
	context.Request.Header.Set("Authorization", "MDkzNjgyOTBfMmFiNjIxNjg3Y2M4NjhjODo=")
	context.Request.Header.Set("X-Request-Id", "5fb91f90-e79d-4737-8f5c-26960b483418")

	client := mocks.NewTestClient(func(req *http.Request) *http.Response {

		assert.Equal(t, req.URL.Path, "/api/securitymaster/v1/Templates/GetAllEventTypes")
		assert.Equal(t, req.Header.Get("Authorization"), "MDkzNjgyOTBfMmFiNjIxNjg3Y2M4NjhjODo=")
		assert.Equal(t, req.Header.Get("X-Request-Id"), "5fb91f90-e79d-4737-8f5c-26960b483418")

		return &http.Response{
			StatusCode: 200,
			// Send response to be tested
			Body: ioutil.NopCloser(bytes.NewBufferString(`[{
				"EventTypeId": 1,
				"EventTypeName": "Analytics",
				"EventTypeDescription": "Analytics",
				"IsTradingEvent": false,
				"ResourceId": null,
				"Revision": null,
				"AuthorizationTopicName": null
			},
			{
				"EventTypeId": 2,
				"EventTypeName": "AssignmentIn/AssignmentonaShortPut(Exercise(AtCost))",
				"EventTypeDescription": "AssignmentIn/AssignmentonaShortPut(Exercise(AtCost))",
				"IsTradingEvent": false,
				"ResourceId": null,
				"Revision": null,
				"AuthorizationTopicName": null
			}]`)),
			// Must be set to non-nil value or it panics
			Header: make(http.Header),
		}
	})
	expected := make(map[int]models.EventType)
	expected[1] = models.EventType{
		EventTypeID:            1,
		EventTypeName:          "Analytics",
		EventTypeDescription:   "Analytics",
		IsTradingEvent:         false,
		ResourceID:             "",
		Revision:               "",
		AuthorizationTopicName: "",
	}
	expected[2] = models.EventType{
		EventTypeID:            2,
		EventTypeName:          "AssignmentIn/AssignmentonaShortPut(Exercise(AtCost))",
		EventTypeDescription:   "AssignmentIn/AssignmentonaShortPut(Exercise(AtCost))",
		IsTradingEvent:         false,
		ResourceID:             "",
		Revision:               "",
		AuthorizationTopicName: "",
	}

	instance := GetEventProviderInstance(context, client)
	result, _ := instance.GetAllEventTypes()

	assert.NotNil(t, result)
	assert.Equal(t, reflect.DeepEqual(result, expected), true)

}
