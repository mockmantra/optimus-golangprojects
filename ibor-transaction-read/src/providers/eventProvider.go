package providers

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/logger"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/models"
	"time"

	"github.com/gin-gonic/gin"
	gcontext "github.com/gorilla/context"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/config"
	domain "stash.ezesoft.net/imsacnt/ibor-transaction-read/src/domainEntities"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/utils"
)

//EventProvider ...
type EventProvider struct {
	context    *gin.Context
	httpClient *http.Client
}

//GetEventProviderInstance ...
func GetEventProviderInstance(context *gin.Context, httpClient *http.Client) EventProvider {
	return EventProvider{
		context:    context,
		httpClient: httpClient,
	}
}

type eventResponse []models.EventType

//GetAllEventTypes ...
func (eventProvider *EventProvider) GetAllEventTypes() (map[int]models.EventType, error) {

	context := eventProvider.context
	//todo: need to use token from reqeust
	token := context.Request.Header.Get("Authorization")

	// tokenTemp := "09368290_2ab621687cc868c8" + ":"
	// token := "Basic " + base64.StdEncoding.EncodeToString([]byte(tokenTemp))

	activityID := context.Request.Header.Get("X-Request-Id")
	if len(token) == 0 {
		return map[int]models.EventType{}, errors.New("Auth token not present")
	}
	//todo: need to update base url
	//baseURL := "https://castle.ezesoftcloud.com"
	url := config.Config.BaseURL + "/api/securitymaster/v1/Templates/GetAllEventTypes"

	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Add("Authorization", token)
	req.Header.Add("X-Request-Id", activityID)

	workflowContext := gcontext.Get(context.Request, domain.WorkflowContextKey).(*domain.WorkflowContext)
	log := workflowContext.Logger.WithFields(logger.Fields{"Url": url})
	log.Info("Start api Request")
	start := time.Now()

	resp, err := eventProvider.httpClient.Do(req)
	log.WithFields(logger.Fields{"ResponseTime": time.Since(start).Nanoseconds() / 1000000}).Info("End api Request")
	if err != nil {
		return map[int]models.EventType{}, err
	}
	defer utils.CallClose(resp.Body)
	if resp.StatusCode != 200 {
		return map[int]models.EventType{}, fmt.Errorf("coredata returned %d status code for symbols", resp.StatusCode)
	}
	response := &eventResponse{}
	errors := json.NewDecoder(resp.Body).Decode(response)
	if errors != nil {
		return map[int]models.EventType{}, errors
	}
	eventTypeMap := make(map[int]models.EventType)
	for _, eventType := range *response {
		eventTypeMap[eventType.EventTypeID] = eventType
	}
	return eventTypeMap, nil
}
