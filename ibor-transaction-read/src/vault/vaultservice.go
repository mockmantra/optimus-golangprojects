package vault

import (
	"io"

	"github.com/hashicorp/vault/api"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/interfaces"
	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/logger"
)

//VaultInstance ...
func VaultInstance() *VaultData {
	return &VaultData{
		VHandler: &VaultAPI{},
		Log:      logger.Log,
	}

}

// VaultAPI ...
type VaultAPI struct {
}

// ParseSecret ...
func (v *VaultAPI) ParseSecret(r io.Reader) (*api.Secret, error) {
	return api.ParseSecret(r)
}

// DefaultConfig ...
func (v *VaultAPI) DefaultConfig() interfaces.IVaultConfig {
	return api.DefaultConfig()
}

//NewClient ...
func (v *VaultAPI) NewClient(c interfaces.IVaultConfig) (interfaces.IVaultClient, error) {
	return api.NewClient(c.(*api.Config))
}
