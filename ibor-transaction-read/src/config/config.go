package config

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

// Configuration - struct to stores service configuration
type Configuration struct {
	BaseURL             string
	APIBase             string
	UnAuthenticatedUrls []string
}

// Config - holds the configuration
var Config = &Configuration{
	APIBase:             "/api/ibor/transaction-read/v1",
	UnAuthenticatedUrls: []string{"/health", "/doc"},
}

// InitializeFromFile - initialize from a json file
func InitializeFromFile(filePath string) error {
	content, err := ioutil.ReadFile(filePath)
	if err != nil {
		log.Fatal(err)
		return err
	}
	errors := json.Unmarshal(content, Config)
	if errors != nil {
		log.Fatal(errors)
	}
	return nil
}

// InitializeFromVault initializes config from vault
func InitializeFromVault() error {
	Config.BaseURL = os.Getenv("IMS_BASE_URL")
	return nil
}

// SkipAuth - return true/false for skipping auth - health and swagger
func (c *Configuration) SkipAuth(path string) bool {
	fullPath := strings.ToLower(path)
	for _, unauthenticatedURL := range c.UnAuthenticatedUrls {
		if strings.HasPrefix(fullPath, c.APIBase+unauthenticatedURL) {
			return true
		}
	}
	return false
}
