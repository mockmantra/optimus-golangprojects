package config

import (
	"context"
	"os"
	"strings"

	"stash.ezesoft.net/imsacnt/ibor-transaction-read/src/logger"
)

var (
	// BaseURI is the base URI of the corresponding IMS environment
	BaseURI string
	// IsDevMode should be set to true when running in dev env
	IsDevMode bool

	RUN_ENV string

	AWS_REGION_NAME string

	ENV_CASTLE string

	DB_SCHEMA_TEMPLATE string

	DB_CONNECT_ATTEMPTS int

	Port string
)

func init() {
	var cx context.Context
	loggerWithCtx := logger.Log.WithContext(cx)

	ENV_CASTLE = "castle"

	DB_SCHEMA_TEMPLATE = "Eclipse_%s"
	DB_CONNECT_ATTEMPTS = 3

	bu := os.Getenv("IMS_BASE_URL")
	if bu == "" {
		loggerWithCtx.Debugf("Please, set IMS_BASE_URL environment variable")
	}
	BaseURI = bu

	ae := os.Getenv("APP_ENV")
	if strings.EqualFold(ae, "local") {
		loggerWithCtx.Debugf("IN DEV MODE")
		IsDevMode = true
	}
	RUN_ENV = ae

	AWS_REGION_NAME = os.Getenv("AWS_REGION")
	if len(AWS_REGION_NAME) == 0 {
		loggerWithCtx.Debugf("AWS_REGION variable is not set")
	}

	Port = os.Getenv("PORT")
}
