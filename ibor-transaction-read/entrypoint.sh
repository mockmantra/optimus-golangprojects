#!/bin/bash

set -e
echo "Starting ibor-transaction-read wrapper shell script"


if [[ “$APP_ENV” != “dev” && -n “$VAULT_ADDR” ]] ; then
	# sh  $ROUTING_SCRIPTS_DIR/certs.sh
	# sh  $ROUTING_SCRIPTS_DIR/install-vault-deps.sh
	sh  $ROUTING_SCRIPTS_DIR/vault-auth.sh

	echo "Determining vault settings"
	if [ -z "$VAULT_SKIP_VERIFY" ]; then
		echo "VAULT_SKIP_VERIFY is not defined. Defaulting to false."
		VAULT_SKIP_VERIFY=false
	fi
	if [ -z "$VAULT_TOKEN" ]; then
		echo "VAULT_TOKEN is not defined. Defaulting to value from ~/.vault-token."
		export VAULT_TOKEN=$(cat ~/.vault-token)
	fi
	echo "Using $VAULT_ADDR as vault address"

	if [ -z "$VAULT_CERT" ]; then
		echo "VAULT_CERT is not defined. Defaulting to value from /cert/vault.pem."
		export VAULT_CERT="/cert/vault.pem"
	fi
	if [ -z "$VAULT_KEY" ]; then
		echo "VAULT_KEY is not defined. Defaulting to value from /cert/key.pem."
		export VAULT_KEY="/cert/key.pem"
	fi

	export  VAULT_ADDR VAULT_SKIP_VERIFY VAULT_CERT VAULT_KEY
fi

/go/src/stash.ezesoft.net/imsacnt/ibor-transaction-read/src/out