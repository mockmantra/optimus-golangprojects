#!/bin/bash
set -e -o verbose

src="stash.ezesoft.net/imsacnt/ibor-transaction-read/src"
reportDir="reports"

# Clean up and recreate report directory
rm -rf "$reportDir"
mkdir -p "$reportDir"

testCmd="\
go get -t $src/... ; \
go get \
  github.com/axw/gocov/gocov github.com/AlekSi/gocov-xml \
  gopkg.in/alecthomas/gometalinter.v1 ; \
gometalinter.v1 --install ; \
gometalinter.v1 --deadline 5m --checkstyle $src/... > /$reportDir/lint.xml || true ; \
export APP_ENV=local; \
go test -v -coverprofile=/$reportDir/coverage.out $src/...  || exit $1 ;  \
gocov convert /$reportDir/coverage.out | gocov-xml > /$reportDir/coverage.xml ; \
chmod -R a+rwx /$reportDir ; \
"

echo "~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~"
echo "Starting UNIT TESTS!"
echo "~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~"

docker run -i --rm -v "$(pwd)/$reportDir:/$reportDir" --entrypoint /bin/bash "$tag" -c "$testCmd"

# Making the file paths in the reports relative
sed -i -e "s|/go/src/stash.ezesoft.net/imsacnt/ibor-transaction-read/||g" "$reportDir/lint.xml"
sed -i -e "s|/go/src/stash.ezesoft.net/imsacnt/ibor-transaction-read/||g" "$reportDir/coverage.xml"

echo "~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~"
echo "Finished executing unit tests"
echo "~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~"