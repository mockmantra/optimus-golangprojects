const request = require('request');
const benchmarks = require('./perfcontract.json');
const influx = require('./db.js');

request(`http://imsperformance.qalab.net:8086/query?db=PerfCI&q=SELECT Request, percentile,commitid FROM (SELECT percentile("responseTime",95 ) AS "percentile", requestName AS "Request", commitid FROM "Accounting_TransactionAPI"  WHERE commitid='${process.env.GIT_COMMIT}' GROUP BY requestName)`, function (error, response, body) {
    console.log('error:', error);
    console.log('statuscode:', response && response.statusCode);
    //console.log('body:', body);
    const results = JSON.parse(body);
    const series = results.results[0].series;
    let buildStatus = true;
    for (let i = 0; i < series.length; i++) {
        const result = series[i];
        const values = result.values;
        for (let j = 0; j < values.length; j++) {
            const value = values[j];
            const requestName = value[1];
            const percentile = value[2];
            console.log('requestName', requestName);
            console.log('percentile', percentile);
            console.log('benchmarks[requestName]', benchmarks[requestName]);

            if (percentile < benchmarks[requestName]) {
                //Checkforhealthyservice
                const body = `Accounting_TransactionAPI_Request,RequestName=${requestName},APIStatus=Good,CommitID=${process.env.GIT_COMMIT} Flag=0,Percentile=${percentile},KPI=${benchmarks[requestName]}`;

                influx.pushData(body, (errorMessage, output) => {
                    if (errorMessage) {
                        console.log(errormessage);
                    } else {
                        console.log(output);

                    }
                });
            }
            else {
                buildStatus = false;
                //check for violated service
                const body = `Accounting_TransactionAPI_Request,RequestName=${requestName},APIStatus=Warning,CommitID=${process.env.GIT_COMMIT} Flag=1,Percentile=${percentile},KPI=${benchmarks[requestName]}`;

                influx.pushData(body, (errorMessage, output) => {
                    if (errorMessage) {
                        console.log(errormessage);
                    } else {
                        if(benchmarks.PerformanceQualityGate == false) {
                         console.log("Violation of PerfContract for this Build.");
                         console.log("PerformanceQualityGate=" + benchmarks.PerformanceQualityGate);
                         process.exit(0);
                        }
                    else{
                        console.log("Violation of PerfContract for this Build.");
                         console.log("PerformanceQualityGate=" + benchmarks.PerformanceQualityGate);
                         process.exit(1);
                    } 
                        
                    }
                });

            }

        }
        let body;
        if (buildStatus == false) //checkfor Performance Quality gate
        {
            body = `PerCIStatus,Pipeline=IBOR_Transaction_API BuildStatus="Violated",Flag=1,CommitID="${process.env.GIT_COMMIT}"`;
        }
        else {
            body = `PerCIStatus,Pipeline=IBOR_Transaction_API BuildStatus="Pass",Flag=0,CommitID="${process.env.GIT_COMMIT}"`;
        }
        influx.pushData(body, (errorMessage, output) => {
            if (errorMessage) {
                console.log(errormessage);
            } else {
                console.log(output);
            }
        });
    }
});


