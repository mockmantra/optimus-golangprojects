import argparse
import json
import sys
import os
import glob
from string import Template


def generate_substitution_maps(filenames):
    """
    """
    maps = []
    if not filenames:
        return None
    for filename in filenames:
        map = generate_substitution_map(filename)
        if map:
            maps += [map]
    return maps


def generate_substitution_map(filename):
    """
    """
    if not filename:
        return None
    try:
        with open(filename) as f:
            substitution_map = json.load(f)
            return substitution_map
    except IOError as e:
        print("Error occurred opening file '%s'" % filename, e)
        raise
    except Exception as e:
        print("Exception occurred when generating map from '%s'" % filename, e)
        raise


def substitute_in_files(filenames, substitution_maps):
    """
    """
    successfully_processed = []
    failed_to_process = []
    for filename in filenames:
        result = substitute_in_file(filename, substitution_maps)
        if result:
            successfully_processed += [filename]
        else:
            failed_to_process += [filename]
    success = len(failed_to_process) == 0
    print("Successfully processed files: '%s'" % successfully_processed)
    if not success:
        print("Failed to process files: '%s'" % failed_to_process)
    return success


def substitute_in_file(filename, substitution_maps):
    """
    """
    content = ''
    try:
        with open(filename,'r') as f:
            content = f.read()
        for substitution_map in substitution_maps:
            content = substitute_in_string(content, substitution_map)
        with open(filename, 'w') as f:
            f.write(content)
        return True
    except Exception as e:
        print("Exception occurred when substituting map(s) in file", e)
        return False


def substitute_in_string(template_string, substitution_map):
    """
    """
    template_string = Template(template_string).safe_substitute(substitution_map)
    return template_string


def parse_commandline():
    """
    :rtype: dict
    """
    # read from commandline
    # http://stackoverflow.com/a/26910, http://stackoverflow.com/a/979871
    parser = argparse.ArgumentParser(description = 'Replace routing variables for template')
    parser.add_argument('files', metavar = "files", help = "File templates need to be replaced, comma-separated string")
    parser.add_argument("-j", "--json", metavar = "JSONFILES", dest = "json_files", default= "", help = "Json file(s) to load map from, comma-separated string")
    parser.add_argument("-e", "--env", action = "store_true", dest = "env_supplemental", default = False, help = "Supplemental replacement for settings not in json files")
    config = vars(parser.parse_args())
    config['files'] = [x.strip() for x in config['files'].split(',')]
    config['json_files'] = [x.strip() for x in config['json_files'].split(',')]
    return config


def main(argv):
    config = parse_commandline()
    substitution_maps = generate_substitution_maps(config['json_files'])
    if not substitution_maps or len(substitution_maps) == 0:
        print("Skip json file replacement since no json files exist.")
    elif not substitute_in_files(config['files'], substitution_maps):
        sys.exit(2)
    if config['env_supplemental']:
        print("replacing environment Supplemental....")
        if not substitute_in_files(config['files'], [os.environ]):
            sys.exit(2)


if __name__ == "__main__":
    main(sys.argv[1:])
