#!/bin/bash
mkdir -p /tmp
echo "Installing curl-7.48"
curl https://s3.amazonaws.com/linux-tarballs/curl/curl-7.48-trusty-2.tar.gz -o /tmp/curl-7.48.tar.gz && \
    tar -xvzf /tmp/curl-7.48.tar.gz -C /tmp && \
    mv /tmp/curl /bin/curl-7.48 && \
    rm /tmp/curl-7.48.tar.gz
echo "Installing vault command line binary"
curl https://releases.hashicorp.com/vault/0.8.3/vault_0.8.3_linux_amd64.zip -o /tmp/vault.zip && \
    unzip -o /tmp/vault.zip -d /bin && \
    rm /tmp/vault.zip
echo "Installing consul-template"
curl https://releases.hashicorp.com/consul-template/0.14.0/consul-template_0.14.0_linux_amd64.zip -o /tmp/consul-template.zip && \
    unzip -o /tmp/consul-template.zip -d /bin && \
    rm /tmp/consul-template.zip
