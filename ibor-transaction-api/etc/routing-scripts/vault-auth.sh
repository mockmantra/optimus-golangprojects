#!/bin/bash
set -e
echo "Creating certificate and private key"
mkdir -p /cert
openssl req -x509 -newkey rsa:2048 -keyout /cert/key.pem -out /cert/vault.pem -days 366 -nodes -subj '/CN=localhost'

cid=`cat /proc/self/cgroup | grep 'docker' | sed 's/^.*\///' | tail -n1`
echo "Container ID is $cid"

echo "Calling secret agent to register certificate with vault"
curl-7.48 -v --unix-socket /var/lib/poll-vault/agent.sock http:/setupauth/$cid

echo "Authenticating with vault"
vault auth -method=cert -client-cert=/cert/vault.pem -client-key=/cert/key.pem >/dev/null
