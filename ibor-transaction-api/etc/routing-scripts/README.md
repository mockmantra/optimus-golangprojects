# Routing Scripts

This is common scripts module that other repos can use.

## Dependencies

The following dependencies should be installed in your container before using these scripts:
1. python3
2. curl
3. unzip
4. openssl

## Usage
Other repos should use gitsubmodules to include this, for example:

```
[submodule "etc/routing-scripts"]
	path = etc/routing-scripts
	url = https://stash.ezesoft.net/scm/ipc/routing-scripts.git
```

## Scripts

Use substitute-secrets.sh along with the following env vars: SUBSTITUTION_FILES, ROUTING_MAP, and ROUTING_SCRIPTS_DIR. This will use the ROUTING_MAP dev values found in ROUTING_SCRIPTS_DIR to substitute configuration values defined in SUBSTITUTION_FILES. To use the secrets volume, mount /opt/secrets in your container and store the location in SECRETS_DIR (make sure ROUTING_MAP exists in SECRETS_DIR). If VAULT_ADDR is defined (and optionally SECRETS_TMPL and SECRETS_MAP), then the script will attempt to get secrets from vault and key/values from consul. Additionally it will provide you the certs in /cert needed to run consul-template. Curl-7.48, vault, and consul-template binaries will be added to /bin after this script executes, enabling your container to further manage the vault connection or to populate additional templates. See the Quick Reference and Environment Variables for more details.

### Quick Reference

| Script Name                     	| Description                                                                                                                                                                                                                                                                                                                                             	| Order 	|
|---------------------------------	|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	|-------	|
| certs.sh                        	| Registers the vault CA to the trusted CAs in your container.                                                                                                                                                                                                                                                                                            	| 1     	|
| install-vault-deps.sh           	| Installs curl-7.48 (needed for connecting to the unix domain socket used to communicate with the poll-vault agent), vault CLI, and consul-template to /bin (/bin/curl-7.48, /bin/vault, and /bin/consul-template respectively).  Requires curl and unzip to be installed in your container via apt-get or apk (Alpine Linux)                            	| 2     	|
| vault-auth.sh                   	| Creates a self-signed certificate in /cert using openssl and registers it with the poll-vault agent's unix domain socket at /var/lib/poll-vault/agent.sock using the container's docker id. Then uses the vault CLI to authenticate with the vault server defined in VAULT_ADDR and stores the token received in the VAULT_TOKEN env var.               	| 3     	|
| generate-secrets.sh             	| Obtains consul-template certificate secrets from vault, stores them in /cert, and exports ca_file, cert_file, and key_file environment variables. If a template (passed with -t TEMPLATE_FILE) and an output (passed with -o OUTPUT_FILE) are specified, the OUTPUT_FILE will be populated with secrets from vault and key/values from consul.          	| 4     	|
| substitute-routing-variables.py 	| Uses optional json map files (passed in with -j "JSON_FILE") and environment variables (use -e to enable) to substitute values into passed in files (1st positional argument). Both the json file optional argument and the files positional arguments support a comma separated list, e.g. "FILE1, FILE2, FILE3".                                      	| 5     	|
| substitute-secrets.sh           	| Uses the json map file specified by SECRETS_MAP if VAULT_ADDR is defined, ROUTING_MAP from SECRETS_DIR, or ROUTING_MAP from ROUTING_SCRIPTS_DIR and environment variables to substitute values into configuration files defined in SUBSTITUTION_FILES. This script uses the others to get certificates, connect to vault/consul, and substitute values. 	|       	|

### generate-secrets.sh
generate-secrets.sh will query the vault server defined in VAULT_ADDR environment variable and creates and exports ca_file, cert_file, and key_file environment variables for the CA and certificates needed to talk with a consul agent in /cert. Optionally, you can pass in a consul template file (-t arg) and desired output file location (-o arg) using the vault server and consul server defined via environment variables to render the template file to the output file once. Both optional arguments must be specified in order for the output to be generated correctly. If you need to continuously poll for consul changes, you can run your own consul-template command/configuration with the certs in /cert using the exported environment variables.

```
usage: generate-secrets.sh [-t TEMPLATE_FILE -o OUTPUT_FILE]

optional arguments:
    -t TEMPLATE_FILE            Consul template file containing vault secrets and values from the consul key/value store
    -o OUTPUT_FILE              Name of the output file to generate from the passed in template.
```
    
###  substitute-routing-variables.py
To use substitute-routing-variables.py, type substitute-routing-variables.py -h or see below

```
usage: substitute-routing-variables.py [-h] [-j JSONFILES] [-e] files

Replace routing variables for template

positional arguments:
  files                 File templates need to be replaced, comma-separated
                        string

optional arguments:
  -h, --help            show this help message and exit
  -j JSONFILES, --json JSONFILES
                        Json file(s) to load map from, comma-separated string
  -e, --env             Supplemental replacement for settings not in json
                        files
```

## Environment Variables

### substitute-secrets.sh

| Variable            	| Description                                                                                                                                                                                                                                                                                                             	| Required 	|
|---------------------	|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	|----------	|
| ROUTING_MAP         	| The json map to use to substitute from. Can be empty if json map substitution is not needed.                                                                                                                                                                                                                            	| True     	|
| ROUTING_SCRIPTS_DIR 	| Directory the routing-script repo is mapped to.                                                                                                                                                                                                                                                                         	| True     	|
| SECRETS_DIR         	| Directory the /opt/secret volume is mounted at. This will take precedence over ROUTING_SCRIPTS_DIR\ROUTING_MAP if SECRETS_DIR\ROUTING_MAP exists.                                                                                                                                                                       	| False    	|
| SECRETS_MAP         	| The name of the output file consul template populates with secrets and consul key/values. Requires SECRETS_TMPL to also be provided to produce an output. This will only be considered if VAULT_ADDR env var is defined and will take precedence over both SECRETS_DIR\ROUTING_MAP and ROUTING_SCRIPTS_DIR\ROUTING_MAP. 	| False    	|
| SECRETS_TMPL        	| The name of the template file consul template renders to generate the SECRETS_MAP. Requires SECRETS_MAP to also be provided to produce an output.                                                                                                                                                                       	| False    	|
| SUBSTITUTION_FILES  	| The configuration files to populate json map values and environment variables into. It should be a non-empty comma separated list of files.                                                                                                                                                                             	| True     	|
| VAULT_ADDR          	| The address of the vault server to query. If defined it will cause substitute-secrets.sh to attempt to configure secrets from vault only and will affect the address the vault CLI hits. See other vault CLI env vars to further manipulate behavior.                                                                   	| False    	|

### generate-secrets.sh

| Variable               	| Description                                                                                                                                                                                                                                                      	| Required 	|
|------------------------	|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	|----------	|
| CONSUL_HTTP_ADDR       	| The address of the consul agent to query in consul template. Defaults to the rancher agent's host ip (or localhost) at CONSUL_AGENT_PORT (or 8500).                                                                                                              	| False    	|
| CONSUL_HTTP_SSL        	| True/False, whether ssl is enabled when communicating to the consul server. Defaults to False.                                                                                                                                                                   	| False    	|
| CONSUL_HTTP_SSL_VERIFY 	| True/False, whether to skip verification of the consul server's ssl certificate. Defaults to False.                                                                                                                                                              	| False    	|
| CONSUL_TOKEN           	| Acl token used to talk with the consul server. Value is retrieved from vault. Override it when using a custom CONSUL_HTTP_ADDR.                                                                                                                                  	| False    	|
| VAULT_ADDR             	| The address of the vault server to query. If it is not defined it will default to the rancher agent's host ip (or localhost) at port 8200 with ssl enabled.                                                                                                      	| False    	|
| VAULT_SKIP_VERIFY      	| True/False, whether to skip verification of the vault server's ssl certificate. Defaults to False.                                                                                                                                                               	| False    	|
| ca_file                	| Path to certificate authority to use as a CA to validate the consul server. Value is retrieved from vault. Override it when using a custom CONSUL_HTTP_ADDR with ssl enabled. E.g. https://127.0.0.1:8200                                                        	| False    	|
| cert_file              	| Path to the ssl client certificate sent to the consul server for authentication. Value is retrieved from vault. Override it when using a custom CONSUL_HTTP_ADDR with verify_incoming enabled.                                                                   	| False    	|
| key_file               	| Path to the ssl client certificate key used with cert_file to convert to a valid x509 certificate to send to the consul server for authentication. Value is retrieved from vault. Override it when using a custom CONSUL_HTTP_ADDR with verify_incoming enabled. 	| False    	|


### vault-auth.sh

| Variable    	| Description                                                                                                                                           	| Required 	|
|-------------	|-------------------------------------------------------------------------------------------------------------------------------------------------------	|----------	|
| VAULT_TOKEN 	| The token generated after successfully authenticating with the vault server defined in VAULT_ADDR. It will be overriden on successful authentication. 	| False    	|


## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## License

Copyright (c) 2016 Eze Software Group.   All rights reserved.