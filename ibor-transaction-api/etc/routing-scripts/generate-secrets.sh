#!/bin/bash

echo "Parsing command line arguments"
while getopts ":t:o:" opt; do
  case $opt in
    t)
      secret_template="$OPTARG"
      ;;
    o)
      secret_output="$OPTARG"
      ;;
    \?)
      echo "Invalid option: -$OPTARG. Valid options are: -t TEMPLATE_FILE -o OUTPUT_FILE" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done


echo "Determining rancher agent IP"
tmp=$(curl --header "Accept: application/json"  "http://rancher-metadata/2015-07-25/self/host/agent_ip")
ip=`echo "$tmp" | grep -o "[0-9.]\+"`
if [ -z "$ip" ]; then
    echo "Could not determine rancher agent IP. Defaulting to 127.0.0.1"
    ip=127.0.0.1
fi
echo "Using $ip as the container host IP"

set -e
echo "Determining vault settings"
if [ -z "$VAULT_ADDR" ]; then
	echo "VAULT_ADDR was not defined. Defaulting to $ip:8200"
	VAULT_ADDR=https://$ip:8200
fi
if [ -z "$VAULT_SKIP_VERIFY" ]; then
    echo "VAULT_SKIP_VERIFY is not defined. Defaulting to false."
    VAULT_SKIP_VERIFY=false
fi
if [ -z "$VAULT_TOKEN" ]; then
    echo "VAULT_TOKEN is not defined. Defaulting to value from ~/.vault-token."
    export VAULT_TOKEN=$(cat ~/.vault-token)
fi
echo "Using $VAULT_ADDR as vault address"


echo "Determining consul template settings"
if [ -z "$CONSUL_HTTP_ADDR" ]; then
    echo "CONSUL_HTTP_ADDR env var is not defined. Defaulting to $ip:${CONSUL_AGENT_PORT:-8500}"
	CONSUL_HTTP_ADDR=$ip:${CONSUL_AGENT_PORT:-8500}
fi
if [ -z "$CONSUL_HTTP_SSL" ]; then
    echo "CONSUL_HTTP_SSL is not defined. Defaulting to false."
    CONSUL_HTTP_SSL=false
fi
if [ -z "$CONSUL_HTTP_SSL_VERIFY" ]; then
    echo "CONSUL_HTTP_SSL_VERIFY is not defined. Defaulting to false."
    CONSUL_HTTP_SSL_VERIFY=false
fi
export  VAULT_ADDR VAULT_SKIP_VERIFY CONSUL_HTTP_ADDR CONSUL_HTTP_SSL CONSUL_HTTP_SSL_VERIFY
echo "Using $CONSUL_HTTP_ADDR as consul agent address"


echo "Configuring consul template certs from vault"
mkdir -p /cert
if [ -z "$CONSUL_TOKEN" ]; then
    CONSUL_TOKEN=$(vault read -field=value /secret/consul/consultemplate)
    export CONSUL_TOKEN
fi
if [ -z "$ca_file" ]; then
    vault read -field=value /secret/consul/ezecomm_ca_cer > /cert/ezecomm_ca.cer
    ca_file=/cert/ezecomm_ca.cer
    export ca_file
fi
if [ -z "$cert_file" ]; then
    vault read -field=value /secret/consul/consul_cer > /cert/consul.cer
    cert_file=/cert/consul.cer
    export cert_file
fi
if [ -z "$key_file" ]; then
    vault read -field=value /secret/consul/consul_key > /cert/consul.key
    key_file=/cert/consul.key
    export key_file
fi
if [ -s "$ca_file" -a -s "$cert_file" -a -s "$key_file" ]; then
    echo "Successfully created the following certs: $ca_file, $cert_file, and $key_file"
fi

if [ -n "$secret_template" -a -n "$secret_output" ]; then
    echo "Creating consul template config"
    mkdir -p /tmp
    cat << EOF > /tmp/config.hcl
consul = "$CONSUL_HTTP_ADDR"

vault {
  address = "$VAULT_ADDR"
  renew = false
}

token = "$CONSUL_TOKEN"

ssl {
  enabled = $CONSUL_HTTP_SSL
  verify = $CONSUL_HTTP_SSL_VERIFY
  cert = "$cert_file"
  key = "$key_file"
  ca_cert = "$ca_file"
}

template {
  source = "$secret_template"
  destination = "$secret_output"
}
EOF

    echo "Generating secret map with consul-template using $secret_template and writing to $secret_output"
    /bin/consul-template -config /tmp/config.hcl -once

    if [ -e "$secret_output" ]; then
        echo "Successfully generated secret map $secret_output"
    fi
fi
