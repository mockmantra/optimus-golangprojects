#!/bin/bash
set -e
echo "Attempting to configure secrets for container using vault addr ($VAULT_ADDR), secrets volume ($SECRETS_DIR), or defaults in $ROUTING_SCRIPTS_DIR"
if [ -n "$VAULT_ADDR" ] ; then
    # sh  $ROUTING_SCRIPTS_DIR/certs.sh
    # sh  $ROUTING_SCRIPTS_DIR/install-vault-deps.sh
    sh  $ROUTING_SCRIPTS_DIR/vault-auth.sh
    if [  -n "$SECRETS_TMPL" -a -n "$SECRETS_MAP" ] ; then
        sh  $ROUTING_SCRIPTS_DIR/generate-secrets.sh -t "$SECRETS_TMPL" -o "$SECRETS_MAP"
    else
        sh  $ROUTING_SCRIPTS_DIR/generate-secrets.sh
    fi
    JSON_MAP_FILES="$SECRETS_MAP"
elif [ -d "$SECRETS_DIR" -a -r "$SECRETS_DIR/$ROUTING_MAP" ]; then
	JSON_MAP_FILES="$SECRETS_DIR/$ROUTING_MAP"
else
	JSON_MAP_FILES="$ROUTING_SCRIPTS_DIR/$ROUTING_MAP"
fi

export JSON_MAP_FILES

echo "Performing routing map substitution: $ROUTING_SCRIPTS_DIR/substitute-routing-variables.py -e --json '$JSON_MAP_FILES' '$SUBSTITUTION_FILES' "
/usr/bin/env python3 $ROUTING_SCRIPTS_DIR/substitute-routing-variables.py -e --json "$JSON_MAP_FILES" "$SUBSTITUTION_FILES"
