##! python


import unittest
import os
import stat


class TestFileSubstituter(unittest.TestCase):
    fn1, fn2, fndict, fndict2 = "substitution_test_temp_file1", "substitution_test_temp_file2", "substitution_test_temp_json_dict", "substitution_test_temp_json_dict2"
    raw_content = """
        something = ${key1}
        something_else = $key2
        something_env = ${key1_env}
        something_else_env = $key2_env
        what = ${nokey1}
        where = $nokey2
    """
    expected_content_solo_json = """
        something = repl1
        something_else = repl2
        something_env = ${key1_env}
        something_else_env = $key2_env
        what = ${nokey1}
        where = $nokey2
    """
    expected_content_solo_env = """
        something = ${key1}
        something_else = $key2
        something_env = repl1_env
        something_else_env = repl2_env
        what = ${nokey1}
        where = $nokey2
    """
    expected_content_json_envsupplemental = """
        something = repl1
        something_else = repl2
        something_env = repl1_env
        something_else_env = repl2_env
        what = ${nokey1}
        where = $nokey2
    """
    expected_content_duo_json = """
        something = repl1
        something_else = repl2
        something_env = repl1_json2
        something_else_env = repl2_json2
        what = ${nokey1}
        where = $nokey2
    """
    substitution_dict_str = """
        {
            "key1": "repl1",
            "key2": "repl2",
            "not_in_raw": ""
        }
        """
    substitution_dict_str2 = """
        {
            "key1_env": "repl1_json2",
            "key2_env": "repl2_json2"
        }
        """
    environ_dict = {
        "key1_env": "repl1_env",
        "key2_env": "repl2_env",
    }


    def test_subst_expect_success_using_jsonfile(self):
        # set the raw files content
        try:
            for fn in [self.fn1, self.fn2]:
                with open(fn, "w") as f:
                    f.write(self.raw_content)
            # prepare the json substitution dictionary
            with open(self.fndict, "w") as f:
                f.write(self.substitution_dict_str)
            # call function to execute the substitution
            command = 'python substitute-routing-variables.py --json %s "%s, %s"' % ( self.fndict, self.fn1, self.fn2 )
            res = os.system(command)
            self.assertEqual(res, 0)
            result = True
            s = ''
            for fn in [self.fn1, self.fn2]:
                with open(fn, "r") as f:
                    s = f.read()
                result = result and ( s == self.expected_content_solo_json )
            # verification succeeded!
            self.assertTrue(result)
        except BaseException as e:
            print("caught and exception: %s" % e)
            self.assertRaises(e)
        finally: # cleanup: delete any file created.
            for fn in [self.fn1, self.fn2, self.fndict]:
                try:
                    os.remove(fn)
                except: # nothing to do
                    print("surprize!")

    def test_subst_expect_success_no_jsonfile_from_env(self):
        try:
            # set env variables
            for k, v in self.environ_dict.items():
                os.environ[k] = v
            # set the raw files content
            for fn in [self.fn1, self.fn2]:
                with open(fn, "w") as f:
                    f.write(self.raw_content)
            # call function to execute the substitution
            command = 'python substitute-routing-variables.py -e "%s, %s"' % ( self.fn1, self.fn2 )
            res = os.system(command)
            self.assertEqual(res, 0)
            result = True
            s = ''
            for fn in [self.fn1, self.fn2]:
                with open(fn, "r") as f:
                    s = f.read()
                result = result and ( s == self.expected_content_solo_env )
            # verification succeeded!
            self.assertTrue(result)
        except BaseException as e:
            print("caught and exception: %s" % e)
            self.assertRaises(e)
        finally: # cleanup: delete any file/environment variables created.
            for k in self.environ_dict:
                del os.environ[k]
            for fn in [self.fn1, self.fn2]:
                try:
                    os.remove(fn)
                except: # nothing to do
                    print("surprize!")

    def test_subst_expect_success_using_jsonfile_env_supplemental(self):
        try:
            # set env variables
            for k, v in self.environ_dict.items():
                os.environ[k] = v
            # set the raw files content
            for fn in [self.fn1, self.fn2]:
                with open(fn, "w") as f:
                    f.write(self.raw_content)
            # prepare the json substitution dictionary
            with open(self.fndict, "w") as f:
                f.write(self.substitution_dict_str)
            # call function to execute the substitution
            command = 'python substitute-routing-variables.py -j %s -e "%s, %s"' % ( self.fndict, self.fn1, self.fn2 )
            res = os.system(command)
            self.assertEqual(res, 0)
            result = True
            s = ''
            for fn in [self.fn1, self.fn2]:
                with open(fn, "r") as f:
                    s = f.read()
                result = result and ( s == self.expected_content_json_envsupplemental )
            # verification succeeded!
            self.assertTrue(result)
        except BaseException as e:
            print("caught and exception: %s" % e)
            self.assertRaises(e)
        finally: # cleanup: delete any file/environment variables created.
            for k in self.environ_dict:
                del os.environ[k]
            for fn in [self.fn1, self.fn2]:
                try:
                    os.remove(fn)
                except: # nothing to do
                    print("surprize!")
                    
    def test_subst_expect_success_using_duo_jsonfile(self):
        # set the raw files content
        try:
            for fn in [self.fn1, self.fn2]:
                with open(fn, "w") as f:
                    f.write(self.raw_content)
            # prepare the json substitution dictionaries
            with open(self.fndict, "w") as f:
                f.write(self.substitution_dict_str)
            with open(self.fndict2, "w") as f:
                f.write(self.substitution_dict_str2)
            # call function to execute the substitution
            command = 'python substitute-routing-variables.py --json "%s, %s" "%s, %s"' % ( self.fndict, self.fndict2, self.fn1, self.fn2 )
            res = os.system(command)
            self.assertEqual(res, 0)
            result = True
            s = ''
            for fn in [self.fn1, self.fn2]:
                with open(fn, "r") as f:
                    s = f.read()
                result = result and ( s == self.expected_content_duo_json )
            # verification succeeded!
            self.assertTrue(result)
        except BaseException as e:
            print("caught and exception: %s" % e)
            self.assertRaises(e)
        finally: # cleanup: delete any file created.
            for fn in [self.fn1, self.fn2, self.fndict, self.fndict2]:
                try:
                    os.remove(fn)
                except: # nothing to do
                    print("surprize!")

    def test_subst_expect_success_using_jsonfile_emptyenv_supplemental_samewith_jsonsolo(self):
        try:
            # set the raw files content
            for fn in [self.fn1, self.fn2]:
                with open(fn, "w") as f:
                    f.write(self.raw_content)
            # prepare the json substitution dictionary
            with open(self.fndict, "w") as f:
                f.write(self.substitution_dict_str)
            # call function to execute the substitution
            command = 'python substitute-routing-variables.py -j %s -e "%s, %s"' % ( self.fndict, self.fn1, self.fn2 )
            res = os.system(command)
            self.assertEqual(res, 0)
            result = True
            s = ''
            for fn in [self.fn1, self.fn2]:
                with open(fn, "r") as f:
                    s = f.read()
                result = result and ( s == self.expected_content_solo_json )
            # verification succeeded!
            self.assertTrue(result)
        except BaseException as e:
            print("caught and exception: %s" % e)
            self.assertRaises(e)
        finally: # cleanup: delete any file created.
            for fn in [self.fn1, self.fn2]:
                try:
                    os.remove(fn)
                except: # nothing to do
                    print("surprize!")

    def test_subst_illegal_dict(self):
        # set the raw files content
        try:
            for fn in [self.fn1, self.fn2]:
                with open(fn, "w") as f:
                    f.write(self.raw_content)
            # prepare the json substitution dictionary
            with open(self.fndict, "w") as f:
                f.write(self.substitution_dict_str + "some extra junk that should render this illegal json")
            # call function to execute the substitution
            command = 'python substitute-routing-variables.py --json %s "%s, %s"' % ( self.fndict, self.fn1, self.fn2 )
            res = os.system(command)
            self.assertNotEqual(res, 0)
        except BaseException as e:
            print("caught and exception: %s" % e)
            self.assertRaises(e)
        finally: # cleanup: delete any file created.
            for fn in [self.fn1, self.fn2, self.fndict]:
                try:
                    os.remove(fn)
                except: # nothing to do
                    print("surprize!")

    def test_subst_file_ro(self):
        # set the raw files content
        try:
            for fn in [self.fn1, self.fn2]:
                with open(fn, "w") as f:
                    f.write(self.raw_content)
                os.chmod(fn, stat.S_IREAD)
            # prepare the json substitution dictionary
            with open(self.fndict, "w") as f:
                f.write(self.substitution_dict_str)
            # call function to execute the substitution
            command = 'python substitute-routing-variables.py --json %s "%s, %s"' % ( self.fndict, self.fn1, self.fn2 )
            res = os.system(command)
            self.assertNotEqual(res, 0)
        except BaseException as e:
            print("caught and exception: %s" % e)
            self.assertRaises(e)
        finally: # cleanup: delete any file created.
            for fn in [self.fn1, self.fn2, self.fndict]:
                try:
                    os.chmod(fn, stat.S_IWRITE)
                    os.remove(fn)
                except: # nothing to do
                    print("surprize!")

    def test_subst_file_notthere(self):
        """
        add a non-existant file to the file list. we expect the substitution to still occur in all the
        existing files, but the command itself should return error
        """
        # set the raw files content
        try:
            for fn in [self.fn1, self.fn2]:
                with open(fn, "w") as f:
                    f.write(self.raw_content)
            # prepare the json substitution dictionary
            with open(self.fndict, "w") as f:
                f.write(self.substitution_dict_str)
            # call function to execute the substitution. notice the "nosuchfile" added to the list
            command = 'python substitute-routing-variables.py --json %s "%s, nosuchfile, %s"' % ( self.fndict, self.fn1, self.fn2 )
            res = os.system(command)
            self.assertNotEqual(res, 0)
            result = True
            s = ''
            for fn in [self.fn1, self.fn2]:
                with open(fn, "r") as f:
                    s = f.read()
                result = result and ( s == self.expected_content_solo_json )
            # verification succeeded!
            self.assertTrue(result)
        except BaseException as e:
            print("caught and exception: %s" % e)
            self.assertRaises(e)
        finally: # cleanup: delete any file created.
            for fn in [self.fn1, self.fn2, self.fndict]:
                try:
                    os.remove(fn)
                except: # nothing to do
                    print("surprize!")

    def test_illegal_command(self):
        # set the raw files content
        try:
            # call function to execute the substitution
            command = 'python substitute-routing-variables.py --json %s "%s, %s" --non_exist_option' % ( self.fndict, self.fn1, self.fn2 )
            res = os.system(command)
            self.assertNotEqual(res, 0)
        except BaseException as e:
            print("caught and exception: %s" % e)
            self.assertRaises(e)


if __name__ == "__main__":
    unittest.main()
