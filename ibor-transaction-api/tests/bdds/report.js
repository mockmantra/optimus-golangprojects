const cli = require('commander');
const influx = require('./influx');
const path = require('path');
cli
    .option('--influxDb <token>', 'user token to send messages and upload screenshots which is generated after install IQA-Bot')
    .option('--resultFile <messageTitle>', 'title of the message (first line of the message)')
    .option('--suiteName <messageTitle>', 'title of the message (first line of the message)')
    .parse(process.argv);
function report() {
    const resultObj = require(path.resolve(cli.resultFile));
    const result = parseReport(resultObj, cli.suiteName);
    return influx.postTestResults(result, cli.influxDb);

}
function parseReport(resultObj) {
    //console.log("into parse report");
    let PassCount = 0, FailCount = 0, TestRunStatus = true;
    const myObj = resultObj;
    const SuiteName = 'IborTxApi';
    for (let j = 1; j < myObj.length; j++) {
        const elementslength = myObj[j].elements.length;
        for (let i = 0; i < elementslength; i++) {
            let steps = myObj[j].elements[i].steps;
            const testStatus = steps.find(step => step.result.status == 'failed');
            if (testStatus) {
                FailCount = FailCount + 1;
            } else {
                PassCount = PassCount + 1;
            }

        }
        if (FailCount > 0) {
            TestRunStatus = false;

        }
    }
    console.log("suitename:", SuiteName, "Pass count of component tests:", PassCount, "Fail count :", FailCount, "Test run status:", TestRunStatus);
    return { SuiteName, PassCount, FailCount, TestRunStatus };
}
report();