#!/bin/bash

export APP_ENV="local"
export REVERSE_PROXY_URL="http://mock-eclipse:3000"
export IMS_BASE_URL=${REVERSE_PROXY_URL}
export AWS_REGION="us-east-1"
export AWS_PROXY_URL="http://localhost:4568"
export EQUITY_AWS_STREAM_NAME="CastleAccountingTransaction01"
export FX_AWS_STREAM_NAME="CastleAccountingTxFx01"
export OPTIONS_AWS_STREAM_NAME="CastleAccountingTxOptions01"
export NTE_AWS_STREAM_NAME="CastleAccountingTxNte01"
export TRANSFERS_AWS_STREAM_NAME="CastleAccountingTxTransfers01"
export EQUITY_SWAPS_AWS_STREAM_NAME="CastleAccountingTxSwaps01"
export VAULT_CERT=""
export VAULT_KEY=""
export VAULT_TOKEN=""

# Running the kinesis server
node /go/src/stash.ezesoft.net/imsacnt/ibor-transaction-api/tests/bdds/kinesis_server.js &
# Creating streams
node /go/src/stash.ezesoft.net/imsacnt/ibor-transaction-api/tests/bdds/Create_Stream.js &

# Running the service
go run /go/src/stash.ezesoft.net/imsacnt/ibor-transaction-api/src/cmd/ibor-transaction-server/main.go --host "0.0.0.0" --port 6988



