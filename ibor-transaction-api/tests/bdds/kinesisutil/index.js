const AWS = require('aws-sdk');

const config = require('./config');

const rec = 'RECORD STRING HERE'
const kinesis = new AWS.Kinesis({
    region: config.region,
    accessKeyId: config.accessKeyId,
    secretAccessKey: config.secretAccessKey,
    endpoint: config.endpoint

});


const waitForStreamToBecomeActive = (streamName, callback) => {
    kinesis.describeStream({
        StreamName: streamName
    }, (err, data) => {
        if (!err) {
            // log.info(util.format('Current status of the stream is %s.', data.StreamDescription.StreamStatus));
            if (data.StreamDescription.StreamStatus === 'ACTIVE') {
                callback(null);
            } else {
                setTimeout(function () {
                    waitForStreamToBecomeActive(streamName, callback);
                }, 1000 * config.waitBetweenDescribeCallsInSeconds);
            }
        }
    });
};

const createStreamIfNotCreated = (streamName, callback) => {
    var params = {
        ShardCount: config.shards,
        StreamName: streamName
    };
    kinesis.createStream(params, (err, data) => {
        if (err) {
            if (err.code !== 'ResourceInUseException') {
                callback(err);
                return;
            } else {
              
                console.log('stream is already created. Re-using it.')
                // log.info(util.format('%s stream is already created. Re-using it.', config.stream));
            }
        } else {
         
            console.log('stream doesnt exist. Created a new stream with that name ')
            // log.info(util.format("%s stream doesn't exist. Created a new stream with that name ..", config.stream));
        }
    });
    waitForStreamToBecomeActive(streamName, (err) => {
        console.log("unable to create stream", err)
       
    });
}


const writeToKinesis = (streamName, record) => {
    // var json = [{"allcoationName":"something"}]
   let jsondata = record;
   const JSON5 = require('json5')
   var stringify = require("json-stringify-pretty-compact")
   try {
    
    // jsondata = JSON.parse(record);
    // jsondata = stringify(record)
    // jsondata = JSON5.stringify(record);
    // jsondata = record.toString();
    // Buffer.from(JSON.S)
    // jsondata = JSON.stringify(record)
    // jsondata = record.toString();
    console.log(jsondata);
  } catch (e) {
    console.log(e);
  }
    var recordParams = {
        Data: jsondata,
        PartitionKey: 'test123',
        StreamName: streamName
    };
    kinesis.putRecord(recordParams, (err, data) => {
        if (err) {
            // log.error(err);
            console.log("getting the data here---",data);
            console.log("getting the data here---",err);
        } else {
             console.log('Successfully sent data to Kinesis.');
        }
    });
}






module.exports = (streamName) => {
    return {
        createStreamIfNotCreated: (callback) => createStreamIfNotCreated(streamName, callback),
        writeToKinesis: (record) => writeToKinesis(streamName, record)
    };
}