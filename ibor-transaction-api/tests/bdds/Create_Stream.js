
const kinesisUtil = require('./kinesisutil');

const kinesisClient_options = kinesisUtil('CastleAccountingTxOptions01');
const kinesisClient_FxTransaction = kinesisUtil('CastleAccountingTxFx01');
const kinesisClient_transaction = kinesisUtil('CastleAccountingTransaction01');
const kinesisClient_nte = kinesisUtil('CastleAccountingTxNte01');
const kinesisClient_transfers = kinesisUtil('CastleAccountingTxTransfers01');
const kinesisClient_equitySwaps = kinesisUtil('CastleAccountingTxSwaps01');



kinesisClient_nte.createStreamIfNotCreated((err) => {
    console.log(err)
});
kinesisClient_transaction.createStreamIfNotCreated((err) => {
    console.log(err)
});
kinesisClient_FxTransaction.createStreamIfNotCreated((err) => {
    console.log(err)
});

kinesisClient_options.createStreamIfNotCreated((err) => {
    console.log(err)
});

kinesisClient_transfers.createStreamIfNotCreated((err) => {
    console.log(err)
});
kinesisClient_equitySwaps.createStreamIfNotCreated((err) => {
    console.log(err)
});