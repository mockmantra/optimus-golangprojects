const users = {

    "TBZQK-HANSOLO": {
        credentials: {
            userName: "hansolo",
            password: "gooduser",
            firm: "TBZQK"
        },
        session:{
            UserSession: {
                FirmAuthToken: "TBZQK",
                UserSessionToken: "good-glx2-token-hansolo",
                FirmId: 2,
                UserId: 1,
                FirmName: "Mock Ims",
                UserName: "hansolo"
            },
            UserResourceId: "foobar",
            ResourceId: "good-glx2-token-hansolo",
            Roles: ["84F0F78F-00BD-441B-9127-B456EBDD76E4", "1a9ccbde-a149-4320-839a-9fb7fefe5415", "3DBC2CC0-53AB-4AF6-833D-2A224A773303"]
        },       
    },

    "TBZQK-HANSOLO2":{
        credentials: {
            userName: "hansolo2",
            password: "gooduser2",
            firm: "TBZQK"
        },
        session:{
            UserSession: {
                FirmAuthToken: "TBZQK",
                UserSessionToken: "good-glx2-token-hansolo2",
                FirmId: 2,
                UserId: 2,
                FirmName: "Mock Ims",
                UserName: "hansolo2"
            },
            UserResourceId: "foobar2",
            ResourceId: "good-glx2-token-hansolo2",
            Roles: ["84F0F78F-00BD-441B-9127-B456EBDD76E4", "1a9ccbde-a149-4320-839a-9fb7fefe5415", "3DBC2CC0-53AB-4AF6-833D-2A224A773303"]
        }, 
    }

}


module.exports = {
    getUserData: (fqUserId) => {
        fqUserId = fqUserId.toUpperCase();
        return users[fqUserId];
    },
    getSessionByGlxToken: (glxToken) => {
        for(user in users){
            currentUser = users[user];
            if(currentUser.session.ResourceId === glxToken) {
                return currentUser.session;
            }
        }
        return null;
    }
}