const rp = require('request-promise');
const config = require('./config');

const influx = {
  /**
   * Post TestSuite results to influxDB
   * @param {TestResults} testResults
   */
  postTestResults(testResults, influxDb) {
    //const name = testResults.SuiteName;
    const name = 'IborTxApi';
    const type = "Component";
    const status = testResults.TestRunStatus ? 1 : 0;
    const pass = testResults.PassCount;
    const fail = testResults.FailCount;
    const payLoad = `ComponentResults,SuiteName=${name},TestType=${type} Status="${status}",Pass=${pass},Fail=${fail}`;
    return post(payLoad, config.influxDB, config.influxRetryCount);
  }
  //ComponentResults
}
function post(payLoad, influxDB, retry) {
  if (typeof retry !== 'number') {
    retry = 0;
  }
  const requestPayload = {
    uri: `${config.influxURI}/write?db=${influxDB}`,
    body: payLoad,
    timeout: 5000
  };
  return rp.post(requestPayload)
    .then((response) => {
      if (response && response.error) {
        console.error('Error while posting data to InfluxDB', response.error);
        if (retry !== 0) {
          return retryPost(payLoad, retry);
        }
      } else {
        console.log("Posted data to InfluxDB");
      }
    })
    .catch((err) => {
      console.error('Error while posting data to InfluxDB');
      console.error('Payload :', requestPayload);
      if (retry !== 0) {
        return sleep(1000).then(() => {
          return post(payLoad, influxDB, --retry);
        })
      } else {
        throw err;
      }
    });
}
function sleep(time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}
module.exports = influx;