Feature:Posting data into IBOR fx
 # Calling the api to post IBOR data

######################################################################################################################################################################################################################
Scenario: To perform POST Request on Ibor FxTransaction API
  Given a request to /ibor
  When I make a POST request to /trans/v1/fx with the JSON file
  """
            {
            "name":"Trx_api_Fx.json"
            }
            """
    Then I should get a 200 response
######################################################################################################################################################################################################################
     Scenario: To perform POST Request on Ibor FxTransaction API without mandatory fields
  Given a request to /ibor
  When I make a POST request to /trans/v1/fx with JSON:
  """
            {
  "Allocations": [
    {
      "BaseCurrencySecurityId": 155,
      "BookTypeId": 1,
      "CounterSettleAmount": 11,
      "CounterToBaseFXRate": 11,
      "CounterToSystemFXRate": 11,
      "CustodianCounterpartyAccountId": 37,
      "DealRate": 11,
      "DealtSettleAmount": 1,
      "DealtToBaseFXRate": 11,
      "DealtToSystemFXRate": 11,
      "ExecutingBrokerCounterpartyId": 32,
      "FwdPoints": 10,
      "IsFinalized": true,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ]
    }
  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "CounterCurrencySecurityId": 25,
  "DealtCurrencySecurityId": 49,
  "EventTypeId": 14,
  "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z"
}
            """
    Then I should get a 422 response
    And I should get TransactionTimeZoneId in body is required in response
    ######################################################################################################################
    Scenario: To perform POST Request on Ibor FxTransaction API by passing wrong data type
  Given a request to /ibor
  When I make a POST request to /trans/v1/fx with JSON:
  """
     {
       "Allocations": [
         {
           "BaseCurrencySecurityId": 155,
           "BookTypeId": 1,
           "CounterSettleAmount": 11,
           "CounterToBaseFXRate": 11,
           "CounterToSystemFXRate": 11,
           "CustodianCounterpartyAccountId": 37,
           "DealRate": 11,
           "DealtSettleAmount": 1,
           "DealtToBaseFXRate": 11,
           "DealtToSystemFXRate": 11,
           "ExecutingBrokerCounterpartyId": 32,
           "FwdPoints": 10,
           "IsFinalized": true,
           "PortfolioId": 21,
           "PositionTags": [
             {
               "IndexAttributeId": 1,
               "IndexAttributeValueId": 3
             }
           ]
         }
       ],
       "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
       "CounterCurrencySecurityId": "25",
       "DealtCurrencySecurityId": 49,
       "EventTypeId": 14,
       "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
       "SecurityId": 375,
       "SecurityTemplateId": 1,
       "SequenceNumber": 63,
       "SettleDate": "2018-09-10T13:25:43Z",
       "SourceSystemName": "Trading",
       "SourceSystemReference": "4803",
       "SystemCurrencySecurityId": 155,
       "TradeDate": "2018-09-08T13:25:43Z",
       "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
       "TransactionTimeZoneId": 1
       }
            """
    Then I should get a 400 response
    And I should get Unable to parse JSON payload, .CounterCurrencySecurityId: Expected type: int64 but received type: string. in response
    ######################################################################################################################
    Scenario: To perform POST Request on Ibor FxTransaction API
  Given a request to /ibor
  When I make a POST request to /trans/v1/fx with the JSON file
  """
            {
            "name":"Trx_api_Fx_DuplicateFields.json"
            }
            """
    Then I should get a 200 response
######################################################################################################################################################################################################################
      Scenario: To perform POST Request on Ibor FxTransaction API by passing wrong data type
  Given a request to /ibor
  When I make a POST request to /trans/v1/fx with JSON:
  """
     {
       "Allocations": [
         {
           "BaseCurrencySecurityId": 155,
           "BookTypeId": 1,
           "CounterSettleAmount": 11,
           "CounterToBaseFXRate": 11,
           "CounterToSystemFXRate": 11,
           "CustodianCounterpartyAccountId": 37,
           "DealRate": 11,
           "DealtSettleAmount": 1,
           "DealtToBaseFXRate": 11,
           "DealtToSystemFXRate": 11,
           "ExecutingBrokerCounterpartyId": 32,
           "FwdPoints": 10,
           "IsFinalized": true,
           "PortfolioId": 21,
           "PositionTags": [
             {
               "IndexAttributeId": 1,
               "IndexAttributeValueId": 3
             }
           ]
         }
       ],
       "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
       "CounterCurrencySecurityId": 25,
       "DealtCurrencySecurityId": 49,
       "EventTypeId": 14,
       "SecurityId": 375,
       "SecurityTemplateId": 1,
       "SequenceNumber": 63,
       "SettleDate": "2018-09-10T13:25:43Z",
       "SourceSystemName": "Trading",
       "SourceSystemReference": "4803",
       "SystemCurrencySecurityId": 155,
       "TradeDate": "2018-09-08T13:25:43Z",
       "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
       "TransactionTimeZoneId": 1
       }
            """
    Then I should get a 422 response
    And I should get OrderCreatedDate in body is required in response