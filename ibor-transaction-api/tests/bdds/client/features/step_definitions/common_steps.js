/*
Any common steps will be placed here for use and 
chai will be used for assertions
*/
'use strict';

const {
    defineSupportCode
} = require('cucumber')
const chai = require('chai');
const chaiExclude = require('chai-exclude');
chai.use(chaiExclude);
var {
    setDefaultTimeout
} = require('cucumber');
var jsontype = require('jsontype');

setDefaultTimeout(120 * 1000);
var sortJsonArray = require('sort-json-array');
var rateId;
var nextRateId;
var ratePutId;
var rateNextPutId;

defineSupportCode(function (self) {
    let Given = self.Given;
    let When = self.When;
    let Then = self.Then;

    Given(/^a request to (.*)$/, function (endpoint) {
        this.httpService.setDefaults(endpoint);
    });

    When(/^I make a GET request to (.*)$/, async function (path) {
        this.response = await this.httpService.get(path);
        console.log(this.response);
    });

    When(/^I make a POST request to (.*) with the JSON file$/, async function (path, body) {

        var exfilename = ((JSON.parse(body)).name);
        var fs = require("fs");
        var relativeFileName = __dirname + '/../JSONs/' + exfilename;
        var config = fs.readFileSync(relativeFileName, "utf8");
        this.response = await this.httpService.postj(path, config);

    });

    Then(/^I should get a (\d{3}) response$/, function (responseCode) {
        chai.expect(this.response.statusCode).to.equal(parseInt(responseCode));
    });
    Then(/^I should get (.*) in response$/, function (responseMessage) {
        chai.expect(this.response.body).contains(responseMessage);
    });

    When(/^I make a POST request to (.*) with JSON:$/, async function (path, body) {
        this.response = await this.httpService.postj(path, body)
    });

});