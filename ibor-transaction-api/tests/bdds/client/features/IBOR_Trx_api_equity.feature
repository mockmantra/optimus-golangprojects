Feature:Posting data into IBOR Transaction api -Equity
 # Calling the api to post IBOR data

######################################################################################################################################################################################################################
 Scenario: Making a GET request to know the response status for health end point
    Given a request to /ibor
    When I make a GET request to /trans/v1/health
    Then I should get a 200 response
######################################################################################################################################################################################################################
Scenario: To perform POST Request on Ibor Transaction API
  Given a request to /ibor
  When I make a POST request to /trans/v1/equity with the JSON file
  """
            {
            "name":"Trx_api_equity.json"
            }
            """
    Then I should get a 200 response
######################################################################################################################################################################################################################

Scenario: To perform negative test on POST Request with strings in place of ids
  Given a request to /ibor
    When I make a POST request to /trans/v1/equity with the JSON file
  """
            {
            "name":"Trx_api_equity_Negative_field_inputs.json"
            }
            """
    Then I should get a 400 response

######################################################################################################################################################################################################################
Scenario: To perform negative test on POST Request without required fields
  Given a request to /ibor
    When I make a POST request to /trans/v1/equity with the JSON file
  """
            {
            "name":"Trx_api_equity_Negative_required_fields.json"
            }
            """
    Then I should get a 422 response


######################################################################################################################################################################################################################


Scenario: TO perform negative test on mandatory field - BusinessDateTimeUTC
Given a request to /ibor
When I make a POST request to /trans/v1/equity with JSON:
"""
{
  "Allocations": [
    {
     "BaseCurrencySecurityId": 155, "IsFinalized": true,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 1,
      "CustodianCounterpartyAccountId": 10,
      "FeeCharges": [],
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "Quantity": 400
    }
  ],
  "EventTypeId": 14,
  "IsFinalized": true,
  "LocalCurrencySecurityId": 155,
  "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
  "OrderQuantity": 600,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "SystemToLocalFXRate": 1,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1,
  "UnderlyingSecurityId": 1123,
  "UserTimeZoneId": 12
}
"""
Then I should get a 422 response
And I should get BusinessDateTimeUTC in body is required in response

###################################################################################################################################################

Scenario: TO perform negative test on mandatory field - EventTypeId
Given a request to /ibor
When I make a POST request to /trans/v1/equity with JSON:
"""
{
  "Allocations": [
    {
     "BaseCurrencySecurityId": 155, "IsFinalized": true,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 1,
      "CustodianCounterpartyAccountId": 10,
      "FeeCharges": [],
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "Quantity": 400
    }
  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "IsFinalized": true,
  "LocalCurrencySecurityId": 155,
  "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
  "OrderQuantity": 600,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "SystemToLocalFXRate": 1,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1,
  "UnderlyingSecurityId": 1123,
  "UserTimeZoneId": 12
}
"""
Then I should get a 422 response
And I should get EventTypeId in body is required in response

###################################################################################################################################################

Scenario: TO perform negative test on mandatory field - IsFinalized
Given a request to /ibor
When I make a POST request to /trans/v1/equity with JSON:
"""
{
  "Allocations": [
    {
     "BaseCurrencySecurityId": 155,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 1,
      "CustodianCounterpartyAccountId": 10,
      "FeeCharges": [],
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "Quantity": 400
    }
  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "LocalCurrencySecurityId": 155,
  "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
  "OrderQuantity": 600,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "SystemToLocalFXRate": 1,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1,
  "UnderlyingSecurityId": 1123,
  "UserTimeZoneId": 12
}
"""
Then I should get a 422 response
And I should get IsFinalized in body is required in response

###################################################################################################################################################
###################################################################################################################################################

Scenario: TO perform negative test on mandatory field - LocalCurrencySecurityId
Given a request to /ibor
When I make a POST request to /trans/v1/equity with JSON:
"""
{
  "Allocations": [
    {
     "BaseCurrencySecurityId": 155, "IsFinalized": true,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 1,
      "CustodianCounterpartyAccountId": 10,
      "FeeCharges": [],
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "Quantity": 400
    }
  ],
   "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "IsFinalized": true,
  "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
  "OrderQuantity": 600,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "SystemToLocalFXRate": 1,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1,
  "UnderlyingSecurityId": 1123,
  "UserTimeZoneId": 12
}
"""
Then I should get a 422 response
And I should get LocalCurrencySecurityId in body is required in response

###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - OrderCreatedDate
Given a request to /ibor
When I make a POST request to /trans/v1/equity with JSON:
"""
{
  "Allocations": [
    {
     "BaseCurrencySecurityId": 155, "IsFinalized": true,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 1,
      "CustodianCounterpartyAccountId": 10,
      "FeeCharges": [],
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "Quantity": 400
    }
  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "IsFinalized": true,
  "LocalCurrencySecurityId": 155,
  "OrderQuantity": 600,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "SystemToLocalFXRate": 1,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1,
  "UnderlyingSecurityId": 1123,
  "UserTimeZoneId": 12
}
"""
Then I should get a 422 response
And I should get OrderCreatedDate in body is required in response


###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - OrderQuantity
Given a request to /ibor
When I make a POST request to /trans/v1/equity with JSON:
"""
{
  "Allocations": [
    {
     "BaseCurrencySecurityId": 155, "IsFinalized": true,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 1,
      "CustodianCounterpartyAccountId": 10,
      "FeeCharges": [],
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "Quantity": 400
    }
  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "IsFinalized": true,
  "LocalCurrencySecurityId": 155,
  "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "SystemToLocalFXRate": 1,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1,
  "UnderlyingSecurityId": 1123,
  "UserTimeZoneId": 12
}
"""
Then I should get a 422 response
And I should get OrderQuantity in body is required in response

###################################################################################################################################################

Scenario: TO perform negative test on mandatory field - SecurityId
Given a request to /ibor
When I make a POST request to /trans/v1/equity with JSON:
"""
{
  "Allocations": [
    {
     "BaseCurrencySecurityId": 155, "IsFinalized": true,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 1,
      "CustodianCounterpartyAccountId": 10,
      "FeeCharges": [],
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "Quantity": 400
    }
  ],
   "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "IsFinalized": true,
  "LocalCurrencySecurityId": 155,
  "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
  "OrderQuantity": 600,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "SystemToLocalFXRate": 1,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1,
  "UnderlyingSecurityId": 1123,
  "UserTimeZoneId": 12
}
"""
Then I should get a 422 response
And I should get SecurityId in body is required in response
###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - SecurityTemplateId
Given a request to /ibor
When I make a POST request to /trans/v1/equity with JSON:
"""
{
  "Allocations": [
    {
     "BaseCurrencySecurityId": 155, "IsFinalized": true,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 1,
      "CustodianCounterpartyAccountId": 10,
      "FeeCharges": [],
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "Quantity": 400
    }
  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "IsFinalized": true,
  "LocalCurrencySecurityId": 155,
  "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
  "OrderQuantity": 600,
  "SecurityId": 375,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "SystemToLocalFXRate": 1,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1,
  "UnderlyingSecurityId": 1123,
  "UserTimeZoneId": 12
}
"""
Then I should get a 422 response
And I should get SecurityTemplateId in body is required in response
###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - SequenceNumber
Given a request to /ibor
When I make a POST request to /trans/v1/equity with JSON:
"""
{
  "Allocations": [
    {
     "BaseCurrencySecurityId": 155, "IsFinalized": true,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 1,
      "CustodianCounterpartyAccountId": 10,
      "FeeCharges": [],
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "Quantity": 400
    }
  ],
   "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "IsFinalized": true,
  "LocalCurrencySecurityId": 155,
  "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
  "OrderQuantity": 600,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "SystemToLocalFXRate": 1,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1,
  "UnderlyingSecurityId": 1123,
  "UserTimeZoneId": 12
}
"""
Then I should get a 422 response
And I should get SequenceNumber in body is required in response
###################################################################################################################################################

Scenario: TO perform negative test on mandatory field - SettleDate
Given a request to /ibor
When I make a POST request to /trans/v1/equity with JSON:
"""
{
  "Allocations": [
    {
     "BaseCurrencySecurityId": 155, "IsFinalized": true,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 1,
      "CustodianCounterpartyAccountId": 10,
      "FeeCharges": [],
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "Quantity": 400
    }
  ],
   "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "IsFinalized": true,
  "LocalCurrencySecurityId": 155,
  "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
  "OrderQuantity": 600,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "SystemToLocalFXRate": 1,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1,
  "UnderlyingSecurityId": 1123,
  "UserTimeZoneId": 12
}
"""
Then I should get a 422 response
And I should get SettleDate in body is required in response
###################################################################################################################################################

Scenario: TO perform negative test on mandatory field - SourceSystemName
Given a request to /ibor
When I make a POST request to /trans/v1/equity with JSON:
"""
{
  "Allocations": [
    {
     "BaseCurrencySecurityId": 155, "IsFinalized": true,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 1,
      "CustodianCounterpartyAccountId": 10,
      "FeeCharges": [],
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "Quantity": 400
    }
  ],
  
 
    "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "IsFinalized": true,
  "LocalCurrencySecurityId": 155,
  "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
  "OrderQuantity": 600,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "SystemToLocalFXRate": 1,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1,
  "UnderlyingSecurityId": 1123,
  "UserTimeZoneId": 12
}
"""
Then I should get a 422 response
And I should get SourceSystemName in body is required in response
###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - SourceSystemReference
Given a request to /ibor
When I make a POST request to /trans/v1/equity with JSON:
"""
{
  "Allocations": [
    {
     "BaseCurrencySecurityId": 155, "IsFinalized": true,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 1,
      "CustodianCounterpartyAccountId": 10,
      "FeeCharges": [],
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "Quantity": 400
    }
  ],
  
   "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "IsFinalized": true,
  "LocalCurrencySecurityId": 155,
  "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
  "OrderQuantity": 600,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SystemCurrencySecurityId": 155,
  "SystemToLocalFXRate": 1,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1,
  "UnderlyingSecurityId": 1123,
  "UserTimeZoneId": 12
}
"""
Then I should get a 422 response
And I should get SourceSystemReference in body is required in response
###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - SystemCurrencySecurityId
Given a request to /ibor
When I make a POST request to /trans/v1/equity with JSON:
"""
{
  "Allocations": [
    {
     "BaseCurrencySecurityId": 155, "IsFinalized": true,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 1,
      "CustodianCounterpartyAccountId": 10,
      "FeeCharges": [],
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "Quantity": 400
    }
  ],
  
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "IsFinalized": true,
  "LocalCurrencySecurityId": 155,
  "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
  "OrderQuantity": 600,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemToLocalFXRate": 1,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1,
  "UnderlyingSecurityId": 1123,
  "UserTimeZoneId": 12
}
"""
Then I should get a 422 response
And I should get SystemCurrencySecurityId in body is required in response
###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - SystemToLocalFXRate
Given a request to /ibor
When I make a POST request to /trans/v1/equity with JSON:
"""
{
  "Allocations": [
    {
     "BaseCurrencySecurityId": 155, "IsFinalized": true,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 1,
      "CustodianCounterpartyAccountId": 10,
      "FeeCharges": [],
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "Quantity": 400
    }
  ],

  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "IsFinalized": true,
  "LocalCurrencySecurityId": 155,
  "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
  "OrderQuantity": 600,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1,
  "UnderlyingSecurityId": 1123,
  "UserTimeZoneId": 12
}
"""
Then I should get a 422 response
And I should get SystemToLocalFXRate in body is required in response
###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - TradeDate
Given a request to /ibor
When I make a POST request to /trans/v1/equity with JSON:
"""
{
  "Allocations": [
    {
     "BaseCurrencySecurityId": 155, "IsFinalized": true,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 1,
      "CustodianCounterpartyAccountId": 10,
      "FeeCharges": [],
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "Quantity": 400
    }
  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "IsFinalized": true,
  "LocalCurrencySecurityId": 155,
  "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
  "OrderQuantity": 600,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "SystemToLocalFXRate": 1,
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1,
  "UnderlyingSecurityId": 1123,
  "UserTimeZoneId": 12
}
"""
Then I should get a 422 response
And I should get TradeDate in body is required in response
###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - TransactionDateTimeUTC
Given a request to /ibor
When I make a POST request to /trans/v1/equity with JSON:
"""
{
  "Allocations": [
    {
     "BaseCurrencySecurityId": 155, "IsFinalized": true,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 1,
      "CustodianCounterpartyAccountId": 10,
      "FeeCharges": [],
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "Quantity": 400
    }
  ],

  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "IsFinalized": true,
  "LocalCurrencySecurityId": 155,
  "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
  "OrderQuantity": 600,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "SystemToLocalFXRate": 1,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionTimeZoneId": 1,
  "UnderlyingSecurityId": 1123,
  "UserTimeZoneId": 12
}
"""
Then I should get a 422 response
And I should get TransactionDateTimeUTC in body is required in response
###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - TransactionTimeZoneId
Given a request to /ibor
When I make a POST request to /trans/v1/equity with JSON:
"""
{
  "Allocations": [
    {
     "BaseCurrencySecurityId": 155, "IsFinalized": true,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 1,
      "CustodianCounterpartyAccountId": 10,
      "FeeCharges": [],
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "Quantity": 400
    }
  ],

  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "IsFinalized": true,
  "LocalCurrencySecurityId": 155,
  "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
  "OrderQuantity": 600,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "SystemToLocalFXRate": 1,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "UnderlyingSecurityId": 1123,
  "UserTimeZoneId": 12
}
"""
Then I should get a 422 response
And I should get TransactionTimeZoneId in body is required in response
###################################################################################################################################################
