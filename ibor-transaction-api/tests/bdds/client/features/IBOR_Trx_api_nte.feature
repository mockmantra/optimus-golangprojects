Feature:Posting data into IBOR Transaction api - NTE
 # Calling the api to post IBOR data - options


######################################################################################################################################################################################################################
Scenario: To perform POST Request on Ibor Transaction API
  Given a request to /ibor
  When I make a POST request to /trans/v1/nte with the JSON file
  """
            {
            "name":"Trx_api_nte.json"
            }
            """
    Then I should get a 200 response
######################################################################################################################################################################################################################
Scenario: To perform negative test on POST Request with strings in place of ids
  Given a request to /ibor
    When I make a POST request to /trans/v1/nte with the JSON file
  """
            {
            "name":"Trx_api_nte_Negative_field_inputs.json"
            }
            """
    Then I should get a 400 response

######################################################################################################################################################################################################################
         
Scenario: To perform negative test on POST Request without required fields
  Given a request to /ibor
    When I make a POST request to /trans/v1/nte with the JSON file
  """
            {
            "name":"Trx_api_nte_Negative_required_fields.json"
            }
            """
    Then I should get a 422 response

###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - BusinessDateTimeUTC
Given a request to /ibor
When I make a POST request to /trans/v1/nte with JSON:
"""
{
  "Allocations": [
    {
      "BaseCurrencySecurityId": 155,
      "BookTypeId": 2,
      "CustodianCounterpartyAccountId": 10,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "SettleAmount": 300,
      "SettleCurrencySecurityId": 1,
      "SettleToBaseFXRate": 2,
      "SettleToSystemFXRate": 1
    }

  ],
  
  "EventTypeId": 14,
  "IncomeExpenseTypeId": 12,
  "LogicalTimeId": 100,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get BusinessDateTimeUTC in body is required in response

###################################################################################################################################################

Scenario: TO perform negative test on mandatory field - EventTypeId
Given a request to /ibor
When I make a POST request to /trans/v1/nte with JSON:
"""
{
  "Allocations": [
    {
      "BaseCurrencySecurityId": 155,
      "BookTypeId": 2,
      "CustodianCounterpartyAccountId": 10,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "SettleAmount": 300,
      "SettleCurrencySecurityId": 1,
      "SettleToBaseFXRate": 2,
      "SettleToSystemFXRate": 1
    }

  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "IncomeExpenseTypeId": 12,
  "LogicalTimeId": 100,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get EventTypeId in body is required in response

###################################################################################################################################################

Scenario: TO perform negative test on mandatory field - IncomeExpenseTypeId
Given a request to /ibor
When I make a POST request to /trans/v1/nte with JSON:
"""
{
  "Allocations": [
    {
      "BaseCurrencySecurityId": 155,
      "BookTypeId": 2,
      "CustodianCounterpartyAccountId": 10,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "SettleAmount": 300,
      "SettleCurrencySecurityId": 1,
      "SettleToBaseFXRate": 2,
      "SettleToSystemFXRate": 1
    }

  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "LogicalTimeId": 100,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get IncomeExpenseTypeId in body is required in response

###################################################################################################################################################
###################################################################################################################################################

Scenario: TO perform negative test on mandatory field - LogicalTimeId
Given a request to /ibor
When I make a POST request to /trans/v1/nte with JSON:
"""
{
  "Allocations": [
    {
      "BaseCurrencySecurityId": 155,
      "BookTypeId": 2,
      "CustodianCounterpartyAccountId": 10,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "SettleAmount": 300,
      "SettleCurrencySecurityId": 1,
      "SettleToBaseFXRate": 2,
      "SettleToSystemFXRate": 1
    }

  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "IncomeExpenseTypeId": 12,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get LogicalTimeId in body is required in response

###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - SecurityId
Given a request to /ibor
When I make a POST request to /trans/v1/nte with JSON:
"""
{
  "Allocations": [
    {
      "BaseCurrencySecurityId": 155,
      "BookTypeId": 2,
      "CustodianCounterpartyAccountId": 10,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "SettleAmount": 300,
      "SettleCurrencySecurityId": 1,
      "SettleToBaseFXRate": 2,
      "SettleToSystemFXRate": 1
    }

  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "IncomeExpenseTypeId": 12,
  "LogicalTimeId": 100,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get SecurityId in body is required in response


###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - SecurityTemplateId
Given a request to /ibor
When I make a POST request to /trans/v1/nte with JSON:
"""
{
  "Allocations": [
    {
      "BaseCurrencySecurityId": 155,
      "BookTypeId": 2,
      "CustodianCounterpartyAccountId": 10,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "SettleAmount": 300,
      "SettleCurrencySecurityId": 1,
      "SettleToBaseFXRate": 2,
      "SettleToSystemFXRate": 1
    }

  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "IncomeExpenseTypeId": 12,
  "LogicalTimeId": 100,
  "SecurityId": 375,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get SecurityTemplateId in body is required in response

###################################################################################################################################################

Scenario: TO perform negative test on mandatory field - SequenceNumber
Given a request to /ibor
When I make a POST request to /trans/v1/nte with JSON:
"""
{
  "Allocations": [
    {
      "BaseCurrencySecurityId": 155,
      "BookTypeId": 2,
      "CustodianCounterpartyAccountId": 10,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "SettleAmount": 300,
      "SettleCurrencySecurityId": 1,
      "SettleToBaseFXRate": 2,
      "SettleToSystemFXRate": 1
    }
  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "IncomeExpenseTypeId": 12,
  "LogicalTimeId": 100,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get SequenceNumber in body is required in response

###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - SettleDate
Given a request to /ibor
When I make a POST request to /trans/v1/nte with JSON:
"""
{
  "Allocations": [
    {
      "BaseCurrencySecurityId": 155,
      "BookTypeId": 2,
      "CustodianCounterpartyAccountId": 10,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "SettleAmount": 300,
      "SettleCurrencySecurityId": 1,
      "SettleToBaseFXRate": 2,
      "SettleToSystemFXRate": 1
    }
  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "IncomeExpenseTypeId": 12,
  "LogicalTimeId": 100,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get SettleDate in body is required in response
###################################################################################################################################################

Scenario: TO perform negative test on mandatory field - SourceSystemName
Given a request to /ibor
When I make a POST request to /trans/v1/nte with JSON:
"""
{
  "Allocations": [
    {
      "BaseCurrencySecurityId": 155,
      "BookTypeId": 2,
      "CustodianCounterpartyAccountId": 10,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "SettleAmount": 300,
      "SettleCurrencySecurityId": 1,
      "SettleToBaseFXRate": 2,
      "SettleToSystemFXRate": 1
    }
  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "IncomeExpenseTypeId": 12,
  "LogicalTimeId": 100,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get SourceSystemName in body is required in response
###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - SourceSystemReference
Given a request to /ibor
When I make a POST request to /trans/v1/nte with JSON:
"""
{
  "Allocations": [
    {
      "BaseCurrencySecurityId": 155,
      "BookTypeId": 2,
      "CustodianCounterpartyAccountId": 10,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "SettleAmount": 300,
      "SettleCurrencySecurityId": 1,
      "SettleToBaseFXRate": 2,
      "SettleToSystemFXRate": 1
    }
  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "IncomeExpenseTypeId": 12,
  "LogicalTimeId": 100,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SystemCurrencySecurityId": 155,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get SourceSystemReference in body is required in response
###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - SystemCurrencySecurityId
Given a request to /ibor
When I make a POST request to /trans/v1/nte with JSON:
"""
{
  "Allocations": [
    {
      "BaseCurrencySecurityId": 155,
      "BookTypeId": 2,
      "CustodianCounterpartyAccountId": 10,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "SettleAmount": 300,
      "SettleCurrencySecurityId": 1,
      "SettleToBaseFXRate": 2,
      "SettleToSystemFXRate": 1
    }
  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "IncomeExpenseTypeId": 12,
  "LogicalTimeId": 100,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get SystemCurrencySecurityId in body is required in response
###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - TradeDate
Given a request to /ibor
When I make a POST request to /trans/v1/nte with JSON:
"""
{
  "Allocations": [
    {
      "BaseCurrencySecurityId": 155,
      "BookTypeId": 2,
      "CustodianCounterpartyAccountId": 10,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "SettleAmount": 300,
      "SettleCurrencySecurityId": 1,
      "SettleToBaseFXRate": 2,
      "SettleToSystemFXRate": 1
    }
  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "IncomeExpenseTypeId": 12,
  "LogicalTimeId": 100,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get TradeDate in body is required in response
###################################################################################################################################################

Scenario: TO perform negative test on mandatory field - TransactionDateTimeUTC
Given a request to /ibor
When I make a POST request to /trans/v1/nte with JSON:
"""
{
  "Allocations": [
    {
      "BaseCurrencySecurityId": 155,
      "BookTypeId": 2,
      "CustodianCounterpartyAccountId": 10,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "SettleAmount": 300,
      "SettleCurrencySecurityId": 1,
      "SettleToBaseFXRate": 2,
      "SettleToSystemFXRate": 1
    }
  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "IncomeExpenseTypeId": 12,
  "LogicalTimeId": 100,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get TransactionDateTimeUTC in body is required in response
###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - TransactionTimeZoneId
Given a request to /ibor
When I make a POST request to /trans/v1/nte with JSON:
"""
{
  "Allocations": [
    {
      "BaseCurrencySecurityId": 155,
      "BookTypeId": 2,
      "CustodianCounterpartyAccountId": 10,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "SettleAmount": 300,
      "SettleCurrencySecurityId": 1,
      "SettleToBaseFXRate": 2,
      "SettleToSystemFXRate": 1
    }
  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "IncomeExpenseTypeId": 12,
  "LogicalTimeId": 100,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z"
}
"""
Then I should get a 422 response
And I should get TransactionTimeZoneId in body is required in response
###################################################################################################################################################
