Feature:Posting data into IBOR Transaction api -EQUITY SWAPS
 # Calling the api to post IBOR data - options


######################################################################################################################################################################################################################
Scenario: To perform POST Request on Ibor Transaction API-EQUITY SWAPS
  Given a request to /ibor
  When I make a POST request to /trans/v1/equity-swaps with the JSON file
  """
            {
            "name":"Trx_api_equityswaps.json"
            }
            """
    Then I should get a 200 response
######################################################################################################################################################################################################################
Scenario: To perform negative test on POST Request with strings in place of ids
  Given a request to /ibor
    When I make a POST request to /trans/v1/equity-swaps with the JSON file
  """
            {
            "name":"Trx_api_equityswaps_Negative_field_inputs.json"
            }
            """
    Then I should get a 400 response
    And I should get Unable to parse JSON payload, EquityAllocation.Quantity: Expected type: int64 but received type: string. in response

#######################################################################################################################################################################################################################
         
Scenario: To perform negative test on POST Request without required fields
  Given a request to /ibor
    When I make a POST request to /trans/v1/equity-swaps with the JSON file
  """
            {
            "name":"Trx_api_equityswaps_Negative_required_fields.json"
            }
            """
    Then I should get a 422 response
And I should get EventTypeId in body is required in response


# ###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - BusinessDateTimeUTC
Given a request to /ibor
When I make a POST request to /trans/v1/equity-swaps with JSON:
"""

  {
  "AccrualMethodId": 10,
  "AdjustmentConventionId": 15,
  "AdjustmentDays": 21,
  "Allocations": [
   
    {
      "BaseCurrencySecurityId": 155,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 3,
      "Commission": 10,
      "CommissionCharges": [
        {
          "CommissionCharge": 10,
          "CommissionChargeId": 22
        },
        {
          "CommissionCharge": 0,
          "CommissionChargeId": 24
        }
      ],
      "ExecQuantity": 100,
      "FeeCharges": [
        {
          "FeeCharge": 0.6555,
          "FeeChargeId": 27
        },
        {
          "FeeCharge": 0,
          "FeeChargeId": 28
        },
        {
          "FeeCharge": 0.7867,
          "FeeChargeId": 29
        },
        {
          "FeeCharge": 0.9834,
          "FeeChargeId": 30
        },
        {
          "FeeCharge": 19.668,
          "FeeChargeId": 31
        },
        {
          "FeeCharge": 25,
          "FeeChargeId": 32
        },
        {
          "FeeCharge": 15,
          "FeeChargeId": 33
        },
        {
          "FeeCharge": 98.34,
          "FeeChargeId": 34
        }
      ],
      "Fees": 160.4336,
      "IsFinalized": true,
      "NetAmount": 19838.4336,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "PrincipalAmount": 19668,
      "Quantity": 200,
      "RouteId": 1621,
      "SettleAmount": 19838.4336,
      "SettleCurrencySecurityId": 155,
      "SettlePrice": 196.68,
      "SettleToBaseFXRate": 1,
      "SettleToLocalFXRate": 1,
      "SettleToSystemFXRate": 1
    }
  ],
  "BenchmarkId": 6,
  "BusinessDayConventionAssetLegId": 22,
  "BusinessDayConventionFinancingLegId": 13,
  "CounterpartyId": 18,
  "DividendRate": 25,
  "EffectiveDate": "2018-09-10T13:25:43Z",
  "EventTypeId": 14,
  "InitialMargin": 112,
  "LocalCurrencySecurityId": 155,
  "MaturityDate": "2018-09-10T13:25:43Z",
  "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
  "OrderQuantity": 600,
  "PaymentFrequencyAssetLegId": 33,
  "PaymentFrequencyFinancingLegId": 32,
  "Rate": 100,
  "ResetDayOfMonth": 21,
  "ResetFrequencyAssetLegId": 19,
  "ResetFrequencyFinancingLegId": 31,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceFinancingTemplateId": 42,
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "Spread": 11,
  "SystemCurrencySecurityId": 155,
  "SystemToLocalFXRate": 1,
  "TenorInMonths": 17,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get BusinessDateTimeUTC in body is required in response


# ###################################################################################################################################################

Scenario: TO perform negative test on mandatory field - AccrualMethodId
Given a request to /ibor
When I make a POST request to /trans/v1/equity-swaps with JSON:
"""
{
  
  "AdjustmentConventionId": 15,
  "AdjustmentDays": 21,
  "Allocations": [
   
    {
      "BaseCurrencySecurityId": 155,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 3,
      "Commission": 10,
      "CommissionCharges": [
        {
          "CommissionCharge": 10,
          "CommissionChargeId": 22
        },
        {
          "CommissionCharge": 0,
          "CommissionChargeId": 24
        }
      ],
      "ExecQuantity": 100,
      "FeeCharges": [
        {
          "FeeCharge": 0.6555,
          "FeeChargeId": 27
        },
        {
          "FeeCharge": 0,
          "FeeChargeId": 28
        },
        {
          "FeeCharge": 0.7867,
          "FeeChargeId": 29
        },
        {
          "FeeCharge": 0.9834,
          "FeeChargeId": 30
        },
        {
          "FeeCharge": 19.668,
          "FeeChargeId": 31
        },
        {
          "FeeCharge": 25,
          "FeeChargeId": 32
        },
        {
          "FeeCharge": 15,
          "FeeChargeId": 33
        },
        {
          "FeeCharge": 98.34,
          "FeeChargeId": 34
        }
      ],
      "Fees": 160.4336,
      "IsFinalized": true,
      "NetAmount": 19838.4336,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "PrincipalAmount": 19668,
      "Quantity": 200,
      "RouteId": 1621,
      "SettleAmount": 19838.4336,
      "SettleCurrencySecurityId": 155,
      "SettlePrice": 196.68,
      "SettleToBaseFXRate": 1,
      "SettleToLocalFXRate": 1,
      "SettleToSystemFXRate": 1
    }
  ],
  "BenchmarkId": 6,
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "BusinessDayConventionAssetLegId": 22,
  "BusinessDayConventionFinancingLegId": 13,
  "CounterpartyId": 18,
  "DividendRate": 25,
  "EffectiveDate": "2018-09-10T13:25:43Z",
  "EventTypeId": 14,
  "InitialMargin": 112,
  "LocalCurrencySecurityId": 155,
  "MaturityDate": "2018-09-10T13:25:43Z",
  "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
  "OrderQuantity": 600,
  "PaymentFrequencyAssetLegId": 33,
  "PaymentFrequencyFinancingLegId": 32,
  "Rate": 100,
  "ResetDayOfMonth": 21,
  "ResetFrequencyAssetLegId": 19,
  "ResetFrequencyFinancingLegId": 31,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceFinancingTemplateId": 42,
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "Spread": 11,
  "SystemCurrencySecurityId": 155,
  "SystemToLocalFXRate": 1,
  "TenorInMonths": 17,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
     
"""
Then I should get a 422 response
And I should get AccrualMethodId in body is required in response
# ###################################################################################################################################################

Scenario: TO perform negative test on mandatory field - AdjustmentDays
Given a request to /ibor
When I make a POST request to /trans/v1/equity-swaps with JSON:
"""
{
  "AccrualMethodId": 10,
  "AdjustmentConventionId": 15,
  "Allocations": [
   
    {
      "BaseCurrencySecurityId": 155,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 3,
      "Commission": 10,
      "CommissionCharges": [
        {
          "CommissionCharge": 10,
          "CommissionChargeId": 22
        },
        {
          "CommissionCharge": 0,
          "CommissionChargeId": 24
        }
      ],
      "ExecQuantity": 100,
      "FeeCharges": [
        {
          "FeeCharge": 0.6555,
          "FeeChargeId": 27
        },
        {
          "FeeCharge": 0,
          "FeeChargeId": 28
        },
        {
          "FeeCharge": 0.7867,
          "FeeChargeId": 29
        },
        {
          "FeeCharge": 0.9834,
          "FeeChargeId": 30
        },
        {
          "FeeCharge": 19.668,
          "FeeChargeId": 31
        },
        {
          "FeeCharge": 25,
          "FeeChargeId": 32
        },
        {
          "FeeCharge": 15,
          "FeeChargeId": 33
        },
        {
          "FeeCharge": 98.34,
          "FeeChargeId": 34
        }
      ],
      "Fees": 160.4336,
      "IsFinalized": true,
      "NetAmount": 19838.4336,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "PrincipalAmount": 19668,
      "Quantity": 200,
      "RouteId": 1621,
      "SettleAmount": 19838.4336,
      "SettleCurrencySecurityId": 155,
      "SettlePrice": 196.68,
      "SettleToBaseFXRate": 1,
      "SettleToLocalFXRate": 1,
      "SettleToSystemFXRate": 1
    }
  ],
  "BenchmarkId": 6,
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "BusinessDayConventionAssetLegId": 22,
  "BusinessDayConventionFinancingLegId": 13,
  "CounterpartyId": 18,
  "DividendRate": 25,
  "EffectiveDate": "2018-09-10T13:25:43Z",
  "EventTypeId": 14,
  "InitialMargin": 112,
  "LocalCurrencySecurityId": 155,
  "MaturityDate": "2018-09-10T13:25:43Z",
  "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
  "OrderQuantity": 600,
  "PaymentFrequencyAssetLegId": 33,
  "PaymentFrequencyFinancingLegId": 32,
  "Rate": 100,
  "ResetDayOfMonth": 21,
  "ResetFrequencyAssetLegId": 19,
  "ResetFrequencyFinancingLegId": 31,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceFinancingTemplateId": 42,
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "Spread": 11,
  "SystemCurrencySecurityId": 155,
  "SystemToLocalFXRate": 1,
  "TenorInMonths": 17,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get AdjustmentDays in body is required in response

# ###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - BenchmarkId
Given a request to /ibor
When I make a POST request to /trans/v1/equity-swaps with JSON:
"""
{
  "AccrualMethodId": 10,
  "AdjustmentConventionId": 15,
  "AdjustmentDays": 21,
  "Allocations": [
   
    {
      "BaseCurrencySecurityId": 155,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 3,
      "Commission": 10,
      "CommissionCharges": [
        {
          "CommissionCharge": 10,
          "CommissionChargeId": 22
        },
        {
          "CommissionCharge": 0,
          "CommissionChargeId": 24
        }
      ],
      "ExecQuantity": 100,
      "FeeCharges": [
        {
          "FeeCharge": 0.6555,
          "FeeChargeId": 27
        },
        {
          "FeeCharge": 0,
          "FeeChargeId": 28
        },
        {
          "FeeCharge": 0.7867,
          "FeeChargeId": 29
        },
        {
          "FeeCharge": 0.9834,
          "FeeChargeId": 30
        },
        {
          "FeeCharge": 19.668,
          "FeeChargeId": 31
        },
        {
          "FeeCharge": 25,
          "FeeChargeId": 32
        },
        {
          "FeeCharge": 15,
          "FeeChargeId": 33
        },
        {
          "FeeCharge": 98.34,
          "FeeChargeId": 34
        }
      ],
      "Fees": 160.4336,
      "IsFinalized": true,
      "NetAmount": 19838.4336,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "PrincipalAmount": 19668,
      "Quantity": 200,
      "RouteId": 1621,
      "SettleAmount": 19838.4336,
      "SettleCurrencySecurityId": 155,
      "SettlePrice": 196.68,
      "SettleToBaseFXRate": 1,
      "SettleToLocalFXRate": 1,
      "SettleToSystemFXRate": 1
    }
  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "BusinessDayConventionAssetLegId": 22,
  "BusinessDayConventionFinancingLegId": 13,
  "CounterpartyId": 18,
  "DividendRate": 25,
  "EffectiveDate": "2018-09-10T13:25:43Z",
  "EventTypeId": 14,
  "InitialMargin": 112,
  "LocalCurrencySecurityId": 155,
  "MaturityDate": "2018-09-10T13:25:43Z",
  "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
  "OrderQuantity": 600,
  "PaymentFrequencyAssetLegId": 33,
  "PaymentFrequencyFinancingLegId": 32,
  "Rate": 100,
  "ResetDayOfMonth": 21,
  "ResetFrequencyAssetLegId": 19,
  "ResetFrequencyFinancingLegId": 31,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceFinancingTemplateId": 42,
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "Spread": 11,
  "SystemCurrencySecurityId": 155,
  "SystemToLocalFXRate": 1,
  "TenorInMonths": 17,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get BenchmarkId in body is required in response

# ###################################################################################################################################################

Scenario: TO perform negative test on mandatory field - CounterpartyId
Given a request to /ibor
When I make a POST request to /trans/v1/equity-swaps with JSON:
"""
{
  "AccrualMethodId": 10,
  "AdjustmentConventionId": 15,
  "AdjustmentDays": 21,
  "Allocations": [
   
    {
      "BaseCurrencySecurityId": 155,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 3,
      "Commission": 10,
      "CommissionCharges": [
        {
          "CommissionCharge": 10,
          "CommissionChargeId": 22
        },
        {
          "CommissionCharge": 0,
          "CommissionChargeId": 24
        }
      ],
      "ExecQuantity": 100,
      "FeeCharges": [
        {
          "FeeCharge": 0.6555,
          "FeeChargeId": 27
        },
        {
          "FeeCharge": 0,
          "FeeChargeId": 28
        },
        {
          "FeeCharge": 0.7867,
          "FeeChargeId": 29
        },
        {
          "FeeCharge": 0.9834,
          "FeeChargeId": 30
        },
        {
          "FeeCharge": 19.668,
          "FeeChargeId": 31
        },
        {
          "FeeCharge": 25,
          "FeeChargeId": 32
        },
        {
          "FeeCharge": 15,
          "FeeChargeId": 33
        },
        {
          "FeeCharge": 98.34,
          "FeeChargeId": 34
        }
      ],
      "Fees": 160.4336,
      "IsFinalized": true,
      "NetAmount": 19838.4336,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "PrincipalAmount": 19668,
      "Quantity": 200,
      "RouteId": 1621,
      "SettleAmount": 19838.4336,
      "SettleCurrencySecurityId": 155,
      "SettlePrice": 196.68,
      "SettleToBaseFXRate": 1,
      "SettleToLocalFXRate": 1,
      "SettleToSystemFXRate": 1
    }
  ],
  "BenchmarkId": 6,
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "BusinessDayConventionAssetLegId": 22,
  "BusinessDayConventionFinancingLegId": 13,
  "DividendRate": 25,
  "EffectiveDate": "2018-09-10T13:25:43Z",
  "EventTypeId": 14,
  "InitialMargin": 112,
  "LocalCurrencySecurityId": 155,
  "MaturityDate": "2018-09-10T13:25:43Z",
  "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
  "OrderQuantity": 600,
  "PaymentFrequencyAssetLegId": 33,
  "PaymentFrequencyFinancingLegId": 32,
  "Rate": 100,
  "ResetDayOfMonth": 21,
  "ResetFrequencyAssetLegId": 19,
  "ResetFrequencyFinancingLegId": 31,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceFinancingTemplateId": 42,
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "Spread": 11,
  "SystemCurrencySecurityId": 155,
  "SystemToLocalFXRate": 1,
  "TenorInMonths": 17,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get CounterpartyId in body is required in response

# ###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - DividendRate
Given a request to /ibor
When I make a POST request to /trans/v1/equity-swaps with JSON:
"""
{
  "AccrualMethodId": 10,
  "AdjustmentConventionId": 15,
  "AdjustmentDays": 21,
  "Allocations": [
   
    {
      "BaseCurrencySecurityId": 155,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 3,
      "Commission": 10,
      "CommissionCharges": [
        {
          "CommissionCharge": 10,
          "CommissionChargeId": 22
        },
        {
          "CommissionCharge": 0,
          "CommissionChargeId": 24
        }
      ],
      "ExecQuantity": 100,
      "FeeCharges": [
        {
          "FeeCharge": 0.6555,
          "FeeChargeId": 27
        },
        {
          "FeeCharge": 0,
          "FeeChargeId": 28
        },
        {
          "FeeCharge": 0.7867,
          "FeeChargeId": 29
        },
        {
          "FeeCharge": 0.9834,
          "FeeChargeId": 30
        },
        {
          "FeeCharge": 19.668,
          "FeeChargeId": 31
        },
        {
          "FeeCharge": 25,
          "FeeChargeId": 32
        },
        {
          "FeeCharge": 15,
          "FeeChargeId": 33
        },
        {
          "FeeCharge": 98.34,
          "FeeChargeId": 34
        }
      ],
      "Fees": 160.4336,
      "IsFinalized": true,
      "NetAmount": 19838.4336,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "PrincipalAmount": 19668,
      "Quantity": 200,
      "RouteId": 1621,
      "SettleAmount": 19838.4336,
      "SettleCurrencySecurityId": 155,
      "SettlePrice": 196.68,
      "SettleToBaseFXRate": 1,
      "SettleToLocalFXRate": 1,
      "SettleToSystemFXRate": 1
    }
  ],
  "BenchmarkId": 6,
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "BusinessDayConventionAssetLegId": 22,
  "BusinessDayConventionFinancingLegId": 13,
  "CounterpartyId": 18,
  "EffectiveDate": "2018-09-10T13:25:43Z",
  "EventTypeId": 14,
  "InitialMargin": 112,
  "LocalCurrencySecurityId": 155,
  "MaturityDate": "2018-09-10T13:25:43Z",
  "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
  "OrderQuantity": 600,
  "PaymentFrequencyAssetLegId": 33,
  "PaymentFrequencyFinancingLegId": 32,
  "Rate": 100,
  "ResetDayOfMonth": 21,
  "ResetFrequencyAssetLegId": 19,
  "ResetFrequencyFinancingLegId": 31,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceFinancingTemplateId": 42,
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "Spread": 11,
  "SystemCurrencySecurityId": 155,
  "SystemToLocalFXRate": 1,
  "TenorInMonths": 17,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get DividendRate in body is required in response
####################################################################################################################################################

Scenario: TO perform negative test on mandatory field - MaturityDate
Given a request to /ibor
When I make a POST request to /trans/v1/equity-swaps with JSON:
"""
{
  "AccrualMethodId": 10,
  "AdjustmentConventionId": 15,
  "AdjustmentDays": 21,
  "Allocations": [
    {
      "BaseCurrencySecurityId": 155,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 1,
      "CustodianCounterpartyAccountId": 10,
      "FeeCharges": [],
      "IsFinalized": true,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "Quantity": 400
    },
    {
      "BaseCurrencySecurityId": 155,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 2,
      "CommissionCharges": [],
      "CustodianCounterpartyAccountId": 10,
      "FeeCharges": [],
      "IsFinalized": true,
      "PortfolioId": 21,
      "PositionTags": [],
      "Quantity": 400
    },
    {
      "BaseCurrencySecurityId": 155,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 3,
      "IsFinalized": true,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "Quantity": 400
    },
    {
      "BaseCurrencySecurityId": 155,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 1,
      "Commission": 10,
      "CommissionCharges": [
        {
          "CommissionCharge": 10,
          "CommissionChargeId": 22
        },
        {
          "CommissionCharge": 0,
          "CommissionChargeId": 24
        }
      ],
      "CustodianCounterpartyAccountId": 10,
      "ExecQuantity": 100,
      "FeeCharges": [
        {
          "FeeCharge": 0.6555,
          "FeeChargeId": 27
        },
        {
          "FeeCharge": 0,
          "FeeChargeId": 28
        },
        {
          "FeeCharge": 0.7867,
          "FeeChargeId": 29
        },
        {
          "FeeCharge": 0.9834,
          "FeeChargeId": 30
        },
        {
          "FeeCharge": 19.668,
          "FeeChargeId": 31
        },
        {
          "FeeCharge": 25,
          "FeeChargeId": 32
        },
        {
          "FeeCharge": 15,
          "FeeChargeId": 33
        },
        {
          "FeeCharge": 98.34,
          "FeeChargeId": 34
        }
      ],
      "Fees": 160.4336,
      "IsFinalized": true,
      "NetAmount": 19838.4336,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "PrincipalAmount": 19668,
      "Quantity": 200,
      "RouteId": 1621,
      "RouteName": "MANUAL",
      "SettleAmount": 19838.4336,
      "SettleCurrencySecurityId": 155,
      "SettlePrice": 196.68,
      "SettleToBaseFXRate": 1,
      "SettleToLocalFXRate": 1,
      "SettleToSystemFXRate": 1
    },
    {
      "BaseCurrencySecurityId": 155,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 2,
      "Commission": 10,
      "CommissionCharges": [
        {
          "CommissionCharge": 10,
          "CommissionChargeId": 22
        },
        {
          "CommissionCharge": 0,
          "CommissionChargeId": 24
        }
      ],
      "CustodianCounterpartyAccountId": 10,
      "ExecQuantity": 100,
      "FeeCharges": [
        {
          "FeeCharge": 0.6555,
          "FeeChargeId": 27
        },
        {
          "FeeCharge": 0,
          "FeeChargeId": 28
        },
        {
          "FeeCharge": 0.7867,
          "FeeChargeId": 29
        },
        {
          "FeeCharge": 0.9834,
          "FeeChargeId": 30
        },
        {
          "FeeCharge": 19.668,
          "FeeChargeId": 31
        },
        {
          "FeeCharge": 25,
          "FeeChargeId": 32
        },
        {
          "FeeCharge": 15,
          "FeeChargeId": 33
        },
        {
          "FeeCharge": 98.34,
          "FeeChargeId": 34
        }
      ],
      "Fees": 160.4336,
      "IsFinalized": true,
      "NetAmount": 19838.4336,
      "PortfolioId": 21,
      "PositionTags": [],
      "PrincipalAmount": 19668,
      "Quantity": 200,
      "RouteId": 1621,
      "RouteName": "MANUAL",
      "SettleAmount": 19838.4336,
      "SettleCurrencySecurityId": 155,
      "SettlePrice": 196.68,
      "SettleToBaseFXRate": 1,
      "SettleToLocalFXRate": 1,
      "SettleToSystemFXRate": 1
    },
    {
      "BaseCurrencySecurityId": 155,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 3,
      "Commission": 10,
      "CommissionCharges": [
        {
          "CommissionCharge": 10,
          "CommissionChargeId": 22
        },
        {
          "CommissionCharge": 0,
          "CommissionChargeId": 24
        }
      ],
      "ExecQuantity": 100,
      "FeeCharges": [
        {
          "FeeCharge": 0.6555,
          "FeeChargeId": 27
        },
        {
          "FeeCharge": 0,
          "FeeChargeId": 28
        },
        {
          "FeeCharge": 0.7867,
          "FeeChargeId": 29
        },
        {
          "FeeCharge": 0.9834,
          "FeeChargeId": 30
        },
        {
          "FeeCharge": 19.668,
          "FeeChargeId": 31
        },
        {
          "FeeCharge": 25,
          "FeeChargeId": 32
        },
        {
          "FeeCharge": 15,
          "FeeChargeId": 33
        },
        {
          "FeeCharge": 98.34,
          "FeeChargeId": 34
        }
      ],
      "Fees": 160.4336,
      "IsFinalized": true,
      "NetAmount": 19838.4336,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "PrincipalAmount": 19668,
      "Quantity": 200,
      "RouteId": 1621,
      "SettleAmount": 19838.4336,
      "SettleCurrencySecurityId": 155,
      "SettlePrice": 196.68,
      "SettleToBaseFXRate": 1,
      "SettleToLocalFXRate": 1,
      "SettleToSystemFXRate": 1
    }
  ],
  "BenchmarkId": 6,
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "BusinessDayConventionAssetLegId": 22,
  "BusinessDayConventionFinancingLegId": 13,
  "CounterpartyId": 18,
  "DividendRate": 25,
  "EffectiveDate": "2018-09-10T13:25:43Z",
  "EventTypeId": 14,
  "InitialMargin": 112,
  "LocalCurrencySecurityId": 155,
  "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
  "OrderQuantity": 600,
  "PaymentFrequencyAssetLegId": 33,
  "PaymentFrequencyFinancingLegId": 32,
  "Rate": 100,
  "ResetDayOfMonth": 21,
  "ResetFrequencyAssetLegId": 19,
  "ResetFrequencyFinancingLegId": 31,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceFinancingTemplateId": 42,
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "Spread": 11,
  "SystemCurrencySecurityId": 155,
  "SystemToLocalFXRate": 1,
  "TenorInMonths": 17,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get MaturityDate in body is required in response
# ###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - PaymentFrequencyAssetLegId
Given a request to /ibor
When I make a POST request to /trans/v1/equity-swaps with JSON:
"""
{
  "AccrualMethodId": 10,
  "AdjustmentConventionId": 15,
  "AdjustmentDays": 21,
  "Allocations": [
    
    
    
    {
      "BaseCurrencySecurityId": 155,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 1,
      "Commission": 10,
      "CommissionCharges": [
        {
          "CommissionCharge": 10,
          "CommissionChargeId": 22
        },
        {
          "CommissionCharge": 0,
          "CommissionChargeId": 24
        }
      ],
      "CustodianCounterpartyAccountId": 10,
      "ExecQuantity": 100,
      "FeeCharges": [
        
        {
          "FeeCharge": 0.9834,
          "FeeChargeId": 30
        },
        {
          "FeeCharge": 19.668,
          "FeeChargeId": 31
        },
        {
          "FeeCharge": 25,
          "FeeChargeId": 32
        },
        {
          "FeeCharge": 15,
          "FeeChargeId": 33
        },
        {
          "FeeCharge": 98.34,
          "FeeChargeId": 34
        }
      ],
      "Fees": 160.4336,
      "IsFinalized": true,
      "NetAmount": 19838.4336,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "PrincipalAmount": 19668,
      "Quantity": 200,
      "RouteId": 1621,
      "RouteName": "MANUAL",
      "SettleAmount": 19838.4336,
      "SettleCurrencySecurityId": 155,
      "SettlePrice": 196.68,
      "SettleToBaseFXRate": 1,
      "SettleToLocalFXRate": 1,
      "SettleToSystemFXRate": 1
    },
    
    {
      "BaseCurrencySecurityId": 155,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 3,
      "Commission": 10,
      "CommissionCharges": [
        {
          "CommissionCharge": 10,
          "CommissionChargeId": 22
        },
        {
          "CommissionCharge": 0,
          "CommissionChargeId": 24
        }
      ],
      "ExecQuantity": 100,
      "FeeCharges": [
        {
          "FeeCharge": 15,
          "FeeChargeId": 33
        },
        {
          "FeeCharge": 98.34,
          "FeeChargeId": 34
        }
      ],
      "Fees": 160.4336,
      "IsFinalized": true,
      "NetAmount": 19838.4336,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "PrincipalAmount": 19668,
      "Quantity": 200,
      "RouteId": 1621,
      "SettleAmount": 19838.4336,
      "SettleCurrencySecurityId": 155,
      "SettlePrice": 196.68,
      "SettleToBaseFXRate": 1,
      "SettleToLocalFXRate": 1,
      "SettleToSystemFXRate": 1
    }
  ],
  "BenchmarkId": 6,
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "BusinessDayConventionAssetLegId": 22,
  "BusinessDayConventionFinancingLegId": 13,
  "CounterpartyId": 18,
  "DividendRate": 25,
  "EffectiveDate": "2018-09-10T13:25:43Z",
  "EventTypeId": 14,
  "InitialMargin": 112,
  "LocalCurrencySecurityId": 155,
  "MaturityDate": "2018-09-10T13:25:43Z",
  "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
  "OrderQuantity": 600,
  "PaymentFrequencyFinancingLegId": 32,
  "Rate": 100,
  "ResetDayOfMonth": 21,
  "ResetFrequencyAssetLegId": 19,
  "ResetFrequencyFinancingLegId": 31,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceFinancingTemplateId": 42,
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "Spread": 11,
  "SystemCurrencySecurityId": 155,
  "SystemToLocalFXRate": 1,
  "TenorInMonths": 17,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
  
"""
Then I should get a 422 response
And I should get PaymentFrequencyAssetLegId in body is required in response
####################################################################################################################################################
Scenario: TO perform negative test on mandatory field - Spread
Given a request to /ibor
When I make a POST request to /trans/v1/equity-swaps with JSON:
"""
{
  "AccrualMethodId": 10,
  "AdjustmentConventionId": 15,
  "AdjustmentDays": 21,
  "Allocations": [
    
    
    
    {
      "BaseCurrencySecurityId": 155,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 1,
      "Commission": 10,
      "CommissionCharges": [
        {
          "CommissionCharge": 10,
          "CommissionChargeId": 22
        },
        {
          "CommissionCharge": 0,
          "CommissionChargeId": 24
        }
      ],
      "CustodianCounterpartyAccountId": 10,
      "ExecQuantity": 100,
      "FeeCharges": [
        
        {
          "FeeCharge": 0.9834,
          "FeeChargeId": 30
        },
        {
          "FeeCharge": 19.668,
          "FeeChargeId": 31
        },
        {
          "FeeCharge": 25,
          "FeeChargeId": 32
        },
        {
          "FeeCharge": 15,
          "FeeChargeId": 33
        },
        {
          "FeeCharge": 98.34,
          "FeeChargeId": 34
        }
      ],
      "Fees": 160.4336,
      "IsFinalized": true,
      "NetAmount": 19838.4336,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "PrincipalAmount": 19668,
      "Quantity": 200,
      "RouteId": 1621,
      "RouteName": "MANUAL",
      "SettleAmount": 19838.4336,
      "SettleCurrencySecurityId": 155,
      "SettlePrice": 196.68,
      "SettleToBaseFXRate": 1,
      "SettleToLocalFXRate": 1,
      "SettleToSystemFXRate": 1
    },
    
    {
      "BaseCurrencySecurityId": 155,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 3,
      "Commission": 10,
      "CommissionCharges": [
        {
          "CommissionCharge": 10,
          "CommissionChargeId": 22
        },
        {
          "CommissionCharge": 0,
          "CommissionChargeId": 24
        }
      ],
      "ExecQuantity": 100,
      "FeeCharges": [
        {
          "FeeCharge": 15,
          "FeeChargeId": 33
        },
        {
          "FeeCharge": 98.34,
          "FeeChargeId": 34
        }
      ],
      "Fees": 160.4336,
      "IsFinalized": true,
      "NetAmount": 19838.4336,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "PrincipalAmount": 19668,
      "Quantity": 200,
      "RouteId": 1621,
      "SettleAmount": 19838.4336,
      "SettleCurrencySecurityId": 155,
      "SettlePrice": 196.68,
      "SettleToBaseFXRate": 1,
      "SettleToLocalFXRate": 1,
      "SettleToSystemFXRate": 1
    }
  ],
  "BenchmarkId": 6,
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "BusinessDayConventionAssetLegId": 22,
  "BusinessDayConventionFinancingLegId": 13,
  "CounterpartyId": 18,
  "DividendRate": 25,
  "EffectiveDate": "2018-09-10T13:25:43Z",
  "EventTypeId": 14,
  "InitialMargin": 112,
  "LocalCurrencySecurityId": 155,
  "MaturityDate": "2018-09-10T13:25:43Z",
  "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
  "OrderQuantity": 600,
  "PaymentFrequencyFinancingLegId": 32,
  "Rate": 100,
  "ResetDayOfMonth": 21,
  "ResetFrequencyAssetLegId": 19,
  "ResetFrequencyFinancingLegId": 31,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceFinancingTemplateId": 42,
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
 "PaymentFrequencyAssetLegId": 33,
  "SystemCurrencySecurityId": 155,
  "SystemToLocalFXRate": 1,
  "TenorInMonths": 17,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
      
  
"""
Then I should get a 422 response
And I should get Spread in body is required in response
####################################################################################################################################################
Scenario: TO perform negative test on mandatory field - TenorInMonths
Given a request to /ibor
When I make a POST request to /trans/v1/equity-swaps with JSON:
"""
{
  "AccrualMethodId": 10,
  "AdjustmentConventionId": 15,
  "AdjustmentDays": 21,
  "Allocations": [
    
    
    
    {
      "BaseCurrencySecurityId": 155,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 1,
      "Commission": 10,
      "CommissionCharges": [
        {
          "CommissionCharge": 10,
          "CommissionChargeId": 22
        },
        {
          "CommissionCharge": 0,
          "CommissionChargeId": 24
        }
      ],
      "CustodianCounterpartyAccountId": 10,
      "ExecQuantity": 100,
      "FeeCharges": [
        
        {
          "FeeCharge": 0.9834,
          "FeeChargeId": 30
        },
        {
          "FeeCharge": 19.668,
          "FeeChargeId": 31
        },
        {
          "FeeCharge": 25,
          "FeeChargeId": 32
        },
        {
          "FeeCharge": 15,
          "FeeChargeId": 33
        },
        {
          "FeeCharge": 98.34,
          "FeeChargeId": 34
        }
      ],
      "Fees": 160.4336,
      "IsFinalized": true,
      "NetAmount": 19838.4336,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "PrincipalAmount": 19668,
      "Quantity": 200,
      "RouteId": 1621,
      "RouteName": "MANUAL",
      "SettleAmount": 19838.4336,
      "SettleCurrencySecurityId": 155,
      "SettlePrice": 196.68,
      "SettleToBaseFXRate": 1,
      "SettleToLocalFXRate": 1,
      "SettleToSystemFXRate": 1
    },
    
    {
      "BaseCurrencySecurityId": 155,
      "BaseToLocalFxRate": 1,
      "BookTypeId": 3,
      "Commission": 10,
      "CommissionCharges": [
        {
          "CommissionCharge": 10,
          "CommissionChargeId": 22
        },
        {
          "CommissionCharge": 0,
          "CommissionChargeId": 24
        }
      ],
      "ExecQuantity": 100,
      "FeeCharges": [
        {
          "FeeCharge": 15,
          "FeeChargeId": 33
        },
        {
          "FeeCharge": 98.34,
          "FeeChargeId": 34
        }
      ],
      "Fees": 160.4336,
      "IsFinalized": true,
      "NetAmount": 19838.4336,
      "PortfolioId": 21,
      "PositionTags": [
        {
          "IndexAttributeId": 1,
          "IndexAttributeValueId": 3
        }
      ],
      "PrincipalAmount": 19668,
      "Quantity": 200,
      "RouteId": 1621,
      "SettleAmount": 19838.4336,
      "SettleCurrencySecurityId": 155,
      "SettlePrice": 196.68,
      "SettleToBaseFXRate": 1,
      "SettleToLocalFXRate": 1,
      "SettleToSystemFXRate": 1
    }
  ],
  "BenchmarkId": 6,
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "BusinessDayConventionAssetLegId": 22,
  "BusinessDayConventionFinancingLegId": 13,
  "CounterpartyId": 18,
  "DividendRate": 25,
  "EffectiveDate": "2018-09-10T13:25:43Z",
  "EventTypeId": 14,
  "InitialMargin": 112,
  "LocalCurrencySecurityId": 155,
  "MaturityDate": "2018-09-10T13:25:43Z",
  "OrderCreatedDate": "2018-09-08T13:25:43.0000000Z",
  "OrderQuantity": 600,
  "PaymentFrequencyFinancingLegId": 32,
  "Rate": 100,
  "ResetDayOfMonth": 21,
  "ResetFrequencyAssetLegId": 19,
  "ResetFrequencyFinancingLegId": 31,
  "SecurityId": 375,
  "SecurityTemplateId": 1,
  "SequenceNumber": 63,
  "SettleDate": "2018-09-10T13:25:43Z",
  "SourceFinancingTemplateId": 42,
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "Spread": 11,
 "PaymentFrequencyAssetLegId": 33,
  "SystemCurrencySecurityId": 155,
  "SystemToLocalFXRate": 1,
  "TradeDate": "2018-09-08T13:25:43Z",
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
      
  
"""
Then I should get a 422 response
And I should get TenorInMonths in body is required in response