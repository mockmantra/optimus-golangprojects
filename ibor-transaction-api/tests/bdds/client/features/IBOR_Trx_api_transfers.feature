Feature:Posting data into IBOR Transaction api - Transfers
 # Calling the api to post IBOR data - Transfers


######################################################################################################################################################################################################################
Scenario: To perform POST Request on Ibor Transaction API
  Given a request to /ibor
  When I make a POST request to /trans/v1/transfers with the JSON file
  """
            {
            "name":"Trx_api_transfers.json"
            }
            """
    Then I should get a 200 response
######################################################################################################################################################################################################################
     
Scenario: To perform negative test on POST Request with strings in place of ids
  Given a request to /ibor
    When I make a POST request to /trans/v1/transfers with the JSON file
  """
            {
            "name":"Trx_api_transfers_Negative_field_inputs.json"
            }
            """
    Then I should get a 400 response

######################################################################################################################################################################################################################
         
Scenario: To perform negative test on POST Request without required fields
  Given a request to /ibor
    When I make a POST request to /trans/v1/transfers with the JSON file
  """
            {
            "name":"Trx_api_transfers_Negative_required_fields.json"
            }
            """
    Then I should get a 422 response

###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - BusinessDateTimeUTC
Given a request to /ibor
When I make a POST request to /trans/v1/transfers with JSON:
"""
{
  "Allocations": [
    {
      "BookTypeId": 1,
      "FromPrtBaseCurrencySecurityId": 155,
      "FromPrtSettleToBaseFXRate": 67,
      "FromPortfolioId": 11,
      "Quantity": 12,
      "SecurityId": 1,
      "SecurityTemplateId": 2,
      "SettleToSystemFXRate": 1,
      "ToPrtBaseCurrencySecurityId": 400,
      "ToPrtSettleToBaseFXRate": 12,
      "ToPortfolioId": 21,
      "TransferPrice": 10
    }
  ],
 
  "EventTypeId": 14,
  "LogicalTimeId": 100,
  "SequenceNumber": 63,
  "SideID": 12,
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get BusinessDateTimeUTC in body is required in response
###################################################################################################################################################

Scenario: TO perform negative test on mandatory field - EventTypeId
Given a request to /ibor
When I make a POST request to /trans/v1/transfers with JSON:
"""
{
  "Allocations": [
    {
      "BookTypeId": 1,
      "FromPrtBaseCurrencySecurityId": 155,
      "FromPrtSettleToBaseFXRate": 67,
      "FromPortfolioId": 11,
      "Quantity": 12,
      "SecurityId": 1,
      "SecurityTemplateId": 2,
      "SettleToSystemFXRate": 1,
      "ToPrtBaseCurrencySecurityId": 400,
      "ToPrtSettleToBaseFXRate": 12,
      "ToPortfolioId": 21,
      "TransferPrice": 10
    }
  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  
  "LogicalTimeId": 100,
  "SequenceNumber": 63,
  "SideID": 12,
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get EventTypeId in body is required in response

###################################################################################################################################################

Scenario: TO perform negative test on mandatory field - LogicalTimeId
Given a request to /ibor
When I make a POST request to /trans/v1/transfers with JSON:
"""
{
  "Allocations": [
    {
      "BookTypeId": 1,
      "FromPrtBaseCurrencySecurityId": 155,
      "FromPrtSettleToBaseFXRate": 67,
      "FromPortfolioId": 11,
      "Quantity": 12,
      "SecurityId": 1,
      "SecurityTemplateId": 2,
      "SettleToSystemFXRate": 1,
      "ToPrtBaseCurrencySecurityId": 400,
      "ToPrtSettleToBaseFXRate": 12,
      "ToPortfolioId": 21,
      "TransferPrice": 10
    }
  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  
  "SequenceNumber": 63,
  "SideID": 12,
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get LogicalTimeId in body is required in response

###################################################################################################################################################

Scenario: TO perform negative test on mandatory field - SequenceNumber
Given a request to /ibor
When I make a POST request to /trans/v1/transfers with JSON:
"""
{
  "Allocations": [
    {
      "BookTypeId": 1,
      "FromPrtBaseCurrencySecurityId": 155,
      "FromPrtSettleToBaseFXRate": 67,
      "FromPortfolioId": 11,
      "Quantity": 12,
      "SecurityId": 1,
      "SecurityTemplateId": 2,
      "SettleToSystemFXRate": 1,
      "ToPrtBaseCurrencySecurityId": 400,
      "ToPrtSettleToBaseFXRate": 12,
      "ToPortfolioId": 21,
      "TransferPrice": 10
    }
  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "LogicalTimeId": 100,
  "SideID": 12,
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get SequenceNumber in body is required in response

###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - SideID
Given a request to /ibor
When I make a POST request to /trans/v1/transfers with JSON:
"""
{
  "Allocations": [
    {
      "BookTypeId": 1,
      "FromPrtBaseCurrencySecurityId": 155,
      "FromPrtSettleToBaseFXRate": 67,
      "FromPortfolioId": 11,
      "Quantity": 12,
      "SecurityId": 1,
      "SecurityTemplateId": 2,
      "SettleToSystemFXRate": 1,
      "ToPrtBaseCurrencySecurityId": 400,
      "ToPrtSettleToBaseFXRate": 12,
      "ToPortfolioId": 21,
      "TransferPrice": 10
    }
  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "LogicalTimeId": 100,
  "SequenceNumber": 63,
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get SideID in body is required in response

###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - SourceSystemName
Given a request to /ibor
When I make a POST request to /trans/v1/transfers with JSON:
"""
{
  "Allocations": [
    {
      "BookTypeId": 1,
      "FromPrtBaseCurrencySecurityId": 155,
      "FromPrtSettleToBaseFXRate": 67,
      "FromPortfolioId": 11,
      "Quantity": 12,
      "SecurityId": 1,
      "SecurityTemplateId": 2,
      "SettleToSystemFXRate": 1,
      "ToPrtBaseCurrencySecurityId": 400,
      "ToPrtSettleToBaseFXRate": 12,
      "ToPortfolioId": 21,
      "TransferPrice": 10
    }
  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "LogicalTimeId": 100,
  "SequenceNumber": 63,
  "SideID": 12,

  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get SourceSystemName in body is required in response

###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - SourceSystemReference
Given a request to /ibor
When I make a POST request to /trans/v1/transfers with JSON:
"""
{
  "Allocations": [
    {
      "BookTypeId": 1,
      "FromPrtBaseCurrencySecurityId": 155,
      "FromPrtSettleToBaseFXRate": 67,
      "FromPortfolioId": 11,
      "Quantity": 12,
      "SecurityId": 1,
      "SecurityTemplateId": 2,
      "SettleToSystemFXRate": 1,
      "ToPrtBaseCurrencySecurityId": 400,
      "ToPrtSettleToBaseFXRate": 12,
      "ToPortfolioId": 21,
      "TransferPrice": 10
    }
  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "LogicalTimeId": 100,
  "SequenceNumber": 63,
  "SideID": 12,
  "SourceSystemName": "Trading",
  "SystemCurrencySecurityId": 155,
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get SourceSystemReference in body is required in response

###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - SystemCurrencySecurityId
Given a request to /ibor
When I make a POST request to /trans/v1/transfers with JSON:
"""
{
  "Allocations": [
    {
      "BookTypeId": 1,
      "FromPrtBaseCurrencySecurityId": 155,
      "FromPrtSettleToBaseFXRate": 67,
      "FromPortfolioId": 11,
      "Quantity": 12,
      "SecurityId": 1,
      "SecurityTemplateId": 2,
      "SettleToSystemFXRate": 1,
      "ToPrtBaseCurrencySecurityId": 400,
      "ToPrtSettleToBaseFXRate": 12,
      "ToPortfolioId": 21,
      "TransferPrice": 10
    }
  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "LogicalTimeId": 100,
  "SequenceNumber": 63,
  "SideID": 12,
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
 
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get SystemCurrencySecurityId in body is required in response

###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - TransactionDateTimeUTC
Given a request to /ibor
When I make a POST request to /trans/v1/transfers with JSON:
"""
{
  "Allocations": [
    {
      "BookTypeId": 1,
      "FromPrtBaseCurrencySecurityId": 155,
      "FromPrtSettleToBaseFXRate": 67,
      "FromPortfolioId": 11,
      "Quantity": 12,
      "SecurityId": 1,
      "SecurityTemplateId": 2,
      "SettleToSystemFXRate": 1,
      "ToPrtBaseCurrencySecurityId": 400,
      "ToPrtSettleToBaseFXRate": 12,
      "ToPortfolioId": 21,
      "TransferPrice": 10
    }
  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "LogicalTimeId": 100,
  "SequenceNumber": 63,
  "SideID": 12,
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  
  "TransactionTimeZoneId": 1
}
"""
Then I should get a 422 response
And I should get TransactionDateTimeUTC in body is required in response

###################################################################################################################################################
Scenario: TO perform negative test on mandatory field - TransactionTimeZoneId
Given a request to /ibor
When I make a POST request to /trans/v1/transfers with JSON:
"""
{
  "Allocations": [
    {
      "BookTypeId": 1,
      "FromPrtBaseCurrencySecurityId": 155,
      "FromPrtSettleToBaseFXRate": 67,
      "FromPortfolioId": 11,
      "Quantity": 12,
      "SecurityId": 1,
      "SecurityTemplateId": 2,
      "SettleToSystemFXRate": 1,
      "ToPrtBaseCurrencySecurityId": 400,
      "ToPrtSettleToBaseFXRate": 12,
      "ToPortfolioId": 21,
      "TransferPrice": 10
    }
  ],
  "BusinessDateTimeUTC": "2018-09-08T13:25:43.0000000Z",
  "EventTypeId": 14,
  "LogicalTimeId": 100,
  "SequenceNumber": 63,
  "SideID": 12,
  "SourceSystemName": "Trading",
  "SourceSystemReference": "4803",
  "SystemCurrencySecurityId": 155,
  "TransactionDateTimeUTC": "2018-09-08T13:25:43.0000000Z"

}
"""
Then I should get a 422 response
And I should get TransactionTimeZoneId in body is required in response

###################################################################################################################################################