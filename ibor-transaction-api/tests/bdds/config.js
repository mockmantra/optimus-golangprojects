const config = {
    influxURI: 'http://imsperformance.qalab.net:8086',
    influxDB: 'ACC_NextGen',
    influxRetryCount: 1,
    influxRetrySleep: 1000
  }
  module.exports = config;