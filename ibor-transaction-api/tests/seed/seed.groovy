


// Build for triggering ui-integration tests after deployment
def projectFolder="Accounting/ibor-transaction-api/CastleIntegrationTests"
def team="Accounting-Invictus/Optimus"
def teamSuite="Accounting/Ibor_Transaction"
def teamSlackRoom="hyd-ims-accounting"
def destinationJobFolder="AQA/integration/castle/integration"

def resultCommand='result":"\\w*'.toString()
def prevStatusResult= '/result\\"\\:\\"/'.toString()
def prevBuildNum='number":\\w*'.toString()
def prevBuildResult='/number\\"\\:/'.toString()
def currentBuildResult='/number\\"\\:/'.toString()

def prevBuildDetailsStep="""\
#!/bin/bash 

# Get previous run status, returns like: result":"UNSTABLE
prevStatus=`curl -u \${JenkinsUser}:\${JenkinsAPIKey} -k \${JOB_URL}/lastCompletedBuild/api/json?tree=result | grep -iEo '${resultCommand}'`
# Strip out leading identifier, i.e: result":"
prevStatus=\${prevStatus${prevStatusResult}}
echo "Previous Execution Result:" \$prevStatus 
echo PreviousBuildStatus=\$prevStatus >> build.properties

# Get previous Number i.e test suite run
previousBuildNumber=`curl -u \${JenkinsUser}:\${JenkinsAPIKey} -k \${JOB_URL}/lastCompletedBuild/api/json?tree=number | grep -iEo '${prevBuildNum}'`
# Strip out leading identifier, i.e: number":"
previousBuildNumber=\${previousBuildNumber${prevBuildResult}}
echo "Previous Build Number:" \$previousBuildNumber
echo PreviousBuildNumber=\$previousBuildNumber >> build.properties
"""

def predefinedParams="""\
TeamSuite=\${TeamSuite}
Team=\${Team}
SlackRoom=\${SlackRoom}
Version=\${Version}
Component=\${Component}
PreviousBuildNumber=\${previousBuildNumber}
PreviousBuildStatus=\${PreviousBuildStatus}
"""
def reportStep="""\
#!/bin/bash

# Get previous run status, returns like: result":"UNSTABLE
prevStatus=`curl -u \${JenkinsUser}:\${JenkinsAPIKey} -k \${JOB_URL}/lastCompletedBuild/api/json?tree=result | grep -iEo '${resultCommand}'`
# Strip out leading identifier, i.e: result":"
prevStatus=\${prevStatus${prevStatusResult}}

echo "Previous Execution Result:" \$prevStatus
echo PreviousBuildStatus=\$prevStatus > build.properties
"""

def predefinedAPIParams="""\
DisabledTests=NONE
PreviousBuildStatus=\${PreviousBuildStatus}
SlackRoom=\${SlackRoom}
Version=\${Version}
Component=\${Component}
Team=\${Team}
IntegrationTests=\${IntegrationTests}
"""

job("$projectFolder/api-integration-test-trigger") {
  label('autoscale')
    logRotator {
    daysToKeep(3)
  }
  parameters {
    stringParam('IntegrationTests', "\${ApiAutomationDir}/Tests/Ibor_Transaction", 'Tests to Run')
    stringParam('Version', '${Version}', 'Deployment Version')
    stringParam('Component', '${Component}', 'Component Stack Name')
    stringParam('Team', "${team}", 'Name of the Team.')
    stringParam('SlackRoom', "${teamSlackRoom}", 'Slack channel to notify aborted build.')
    stringParam('JenkinsUser', 'svcAutoQA', 'User to get previous build status')
  }
  wrappers {
    credentialsBinding {
      string('JenkinsAPIKey', 'JenkinsAPIKey')
    }
    maskPasswords()
    timeout {
      absolute(60)
      abortBuild()
    }
    timestamps()
    ansiColorBuildWrapper {
      colorMapName('XTerm')
      defaultFg(15)
      defaultBg(1)
    }
  }
  steps {
    shell(reportStep)
     environmentVariables {
      propertiesFile('build.properties')
    }
  }
  steps {
    conditionalSteps {
      condition { 
        or {
          stringsMatch("\${Component}", "ibor-transaction-api-stack", true) 
        }
        {
          stringsMatch("\${Component}", "ibor-transaction-master-stack", true)
        }
        { 
          stringsMatch("\${Component}", "ibor-transaction-read-stack", true) 
        }
        {
          stringsMatch("\${Component}", "ibor-tx-master-nte-stack", true)
        }
        {
          stringsMatch("\${Component}", "ibor-tx-master-options-stack", true)
        }
        {
          stringsMatch("\${Component}", "ibor-tx-master-swaps", true)
        }
        {
          stringsMatch("\${Component}", "ibor-tx-master-transfers-stack", true)
        }
        {
          stringsMatch("\${Component}", "ibor-tx-master-fx-stack", true)
        }
      }
      runner('DontRun')
      steps {
        triggerBuilder {
          configs {
            blockableBuildTriggerConfig{
            	projects("$destinationJobFolder/api-integration-suite") 
              block {
                buildStepFailureThreshold('never')
                failureThreshold('never')
                unstableThreshold('UNSTABLE')
              }
              configs {
                parameters {
                  predefinedBuildParameters {
                    properties(predefinedAPIParams)
                  }
                }
              }
            } 
          }
        }
      }
    }
  }
}
