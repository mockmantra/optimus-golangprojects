
#!/bin/bash
set -e

BDD_CLIENT="bdd_client"
SERVICE="service"
MOCK_IMS="mock-eclipse"
NETWORK="bdd_network"
JMETER=dockertrustedregistry.ezesoft.net/aqa/jmeterimage:1.0.14
set -e

cleanup_network() {
    echo "Cleaning up network: $NETWORK"
    if docker network ls | grep $NETWORK
    then
        echo "found network: $NETWORK"
      
		docker network rm $NETWORK
   else
        echo "Network not found."
    fi
}

stop_containers(){
    echo "Step1 Stopping any zombie containers..."
    declare -a arr=($BDD_CLIENT $SERVICE $MOCK_IMS)

    for i in "${arr[@]}"
    do
        echo "Removing $i ......"
        container_id=$(docker ps -aqf "name=$i")
        if [[ ! -z "$container_id" ]]
        then
            echo "Container found: " $container_id
            docker rm -f $container_id
        else
            echo "No container found for: $i"
        fi
    done
}

function cleanup {
    stop_containers
    cleanup_network
}

function run_tests {
    echo "called with param: $1"
    cleanup

    echo "Step 1 :Create network: $NETWORK"
    docker network create --driver bridge $NETWORK 
    
    echo "Step 2 :Build mock-ims....."
    docker build -t $MOCK_IMS -f tests/bdds/mock-eclipse/Dockerfile  tests/bdds/mock-eclipse

    echo "Step 3 :Run mock-ims....."
    docker run -d --name $MOCK_IMS --network $NETWORK $MOCK_IMS

    echo "Step 6 :Build service in bdd mode....."
    docker build -t $SERVICE -f tests/bdds/services/Dockerfile .
                                                           
    echo "Step 7 :Run service....."                                                 
    docker run -d --name $SERVICE --network $NETWORK $SERVICE

    sleep 10
    echo "Step 8 : Build client-bdd....."
    docker build -t $BDD_CLIENT -f tests/bdds/client/Dockerfile tests/bdds/client/
    echo "Step 9 : Run client-bdd....."

    if (docker run --name $BDD_CLIENT --network $NETWORK $BDD_CLIENT);then  
    echo "Component Tests Successfull !!!"
		else
            echo "Component Tests Failed !!!!"
			exit 1
    fi

    echo "step 10 : Fetching Test-Report from container"
    docker cp $BDD_CLIENT:usr/src/app/report/cucumber_report.json ./tests/bdds/client/report
    echo "step 11 : posting the execution results to influx db"
    cd ./tests/bdds/
    npm install 
    node ./report.js --resultFile './client/report/cucumber_report.json' --suiteName 'IborTxApi'
    echo "Step 12 : Run Performance_test..."
	 	if (docker run -v "$(pwd)/perfbenchmark:/app/perfbenchmark" --network $NETWORK --rm $JMETER $GIT_BRANCH $GIT_COMMIT );then  
    echo "Perf Test Completed!!!"
	 	else
			exit 0
     fi
}

time run_tests $1

cleanup
