reportDir="$(pwd)/reports"
src="stash.ezesoft.net/imsacnt/ibor-transaction-api/src"

# Clean up and recreate report directory
rm -rf "$reportDir"
mkdir -p "$reportDir"

# Generate lint report
docker run --rm -v "$reportDir:/reports" --entrypoint /bin/bash $tag -c "\
go get gopkg.in/alecthomas/gometalinter.v1 ; \
go get -t $src/... ; \
gometalinter.v1 --install ; \
gometalinter.v1 --checkstyle $src/... > /reports/lint.xml || true ; \
chmod -R a+rwx /reports"

# Making the file paths in lint.xml relative
sed -i -e "s|/go/src/$src|src|g" "$reportDir/lint.xml"

# Workaround for a bug in gometalinter where the "src" path can be duplicated
sed -i -e "s|$src/||g" "$reportDir/lint.xml"