set -e

export PATH=/usr/local/go/bin:$PATH
export GOPATH=/go
export GOBIN=/go/bin
export PATH=$GOBIN:$PATH

#Copy build files into local GOPATH in slave container
mkdir -p /go/src/stash.ezesoft.net/imsacnt/ibor-transaction-api && cp -r $WORKSPACE/* /go/src/stash.ezesoft.net/imsacnt/ibor-transaction-api

# Install tools required for project
go get github.com/golang/dep/...

cd /go/src/stash.ezesoft.net/imsacnt/ibor-transaction-api/

# Install library dependencies
dep ensure 

#Build and install the libraries
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -ldflags="-w -s" -o $WORKSPACE/ibor-transaction-server stash.ezesoft.net/imsacnt/ibor-transaction-api/src/cmd/ibor-transaction-server/




