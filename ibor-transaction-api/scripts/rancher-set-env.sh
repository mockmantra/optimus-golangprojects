#!/bin/bash
#
# Name: rancher-set-env.sh
#
# Description: Environment configuration script for Rancher services. Sets the ENVIRONMENT and REGION variables in your container
#
# Preconditions:
#   1. Executed within a Rancher environment
#   2. Access to the rancher-metadata service
#   3. wget is installed
#   4. ENVIRONMENT and REGION tags exist on every host in the Rancher environment
#
# Postconditions:
#   1. The ENVIRONMENT environment variable is set to the Rancher host ENVIRONMENT tag
#   2. The REGION environment variable is set to the Rancher host REGION tag

if [ -z "$ENVIRONMENT" ]
then
	export ENVIRONMENT=$(wget -qO- http://rancher-metadata/latest/self/host/labels/ENVIRONMENT)
fi
echo "ENVIRONMENT=$ENVIRONMENT"

if [ -z "$REGION" ]
then
	export REGION=$(wget -qO- http://rancher-metadata/latest/self/host/labels/REGION)
fi
echo "REGION=$REGION"