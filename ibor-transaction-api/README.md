## Ibor-Transaction-API

Ibor-Transaction-API is a rest api built using [Open API](https://swagger.io/docs/specification/2-0/basic-structure/) specification.

## Installation

* Go Programming Language
 should be installed in the machine, if not it can be downloaded from https://golang.org/dl/
* Install [dep](https://golang.github.io/dep/docs/installation.html), a dependency management tool for Go.

* Install and run `localstack` using below command.
    ```bash
    docker run -it -p 4567-4578-4568:4567-4578-4568 -p 8080:8080 localstack/localstack
    ```
    ##### AWS CLI installation

    Link for installation of aws cli with was

    https://docs.aws.amazon.com/streams/latest/dev/kinesis-tutorial-cli-installation.html

    please run below command so as to create streams in kinesis
    ```bash
    Equity Stream:
    aws --endpoint-url=http://localhost:4568 kinesis create-stream --stream-name CastleAccountingTransaction01 --shard-count 1
    Fx Stream:
    aws --endpoint-url=http://localhost:4568 kinesis create-stream --stream-name CastleAccountingTxFx01 --shard-count 1
    Options Stream:
    aws --endpoint-url=http://localhost:4568 kinesis create-stream --stream-name CastleAccountingTxOptions01 --shard-count 1
    NTE Stream:
    aws --endpoint-url=http://localhost:4568 kinesis create-stream --stream-name CastleAccountingTxNte01 --shard-count 1
    Transfers Stream:
    aws --endpoint-url=http://localhost:4568 kinesis create-stream --stream-name CastleAccountingTxTransfers01 --shard-count 1
    Swaps Stream:
    aws --endpoint-url=http://localhost:4568 kinesis create-stream --stream-name CastleAccountingTxSwaps01 --shard-count 1
    ```
## Steps to update the swagger api.

* Make necessary changes to the `swagger.json` file. 
* Update the golang files of api using following command from src folder.
    ```bash
    swagger generate server
    ```

## Steps to run in local
* Run below command to run app in local.
    ```bash
    make run
    ```
* To debug application in vscode, goto debug tab and click on play icon.(Make sure that all the necessary tools installed for debugging go).

    *Note*: App listens at port 6988 when we launch it from shell script or debugger but it does not work as expected unless we integrate the authentication api.

## Installing the git hook
* To make sure code is not committed with failing test cases.
    ```bash
    chmod +x scripts/run-tests.bash scripts/pre-commit.bash scripts/install-hooks.bash
    ./scripts/install-hooks.bash
    ```

### Steps to configure authentication api through nginx.

* Clone and run the repo [mock-eclipse](https://stash.ezesoft.net/projects/IPA/repos/mock-eclipse/browse). This api runs on the port 3000.

* Install nginx in local if not done already and add below configuration in `nginx.conf` file under `usr/local/etc/nginx` and restart the nginx service.

    Add upstreams in `http` section

    ```
    upstream Platform_Api_api_platform_v1 {
        server localhost:3000;
    }

    upstream Ibor_trans_api_v1 {
        server localhost:6988;
    }
    ```

    Add/Update following in `server` section

    ```
    listen 8001

    location /api/platform/v1 {
        proxy_pass http://Platform_Api_api_platform_v1;
    }
    location /api/ibor/trans/v1 {
        proxy_pass http://Ibor_trans_api_v1;
    }
    ```
    Note: Now the api can be accessed using http://localhost:8001/api/ibor/trans/v1/
### Tests
* Run all tests in the project.
    ```bash
    make test
    ```
*  Run all tests with coverage in terminal.
    ```bash
    make test_cover
    ```
*  Run all tests with coverage in browser.
    ```bash
    make test_cover_html
    ```
* If you want to run/debug a test with go extension(test At Cursor) in vscode, keep below setting at `.vscode/settings.json`
    ```
    {
        "go.testEnvVars":{ "APP_ENV": "local"}
    }
    ```
## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## History

TODO: Write history

## Credits

TODO: Write credits

## License

TODO: Copyright (c) 2015 Eze Software Group.   All rights reserved.

PS: Swagger-UI folder is the clone of latest [Swagger 2.x dist folder](https://github.com/swagger-api/swagger-ui/tree/2.x/dist) along with our custom code for login and and token generation.

