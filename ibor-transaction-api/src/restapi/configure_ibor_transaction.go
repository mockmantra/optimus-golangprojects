// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"net/http"
	"os"

	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src"

	runtime "github.com/go-openapi/runtime"
	middleware "github.com/go-openapi/runtime/middleware"
	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src/errorhandler"
	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src/handler"
	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src/logger"
	"stash.ezesoft.net/ipc/ezelogger"
	"stash.ezesoft.net/ipc/ezeutils"

	"github.com/go-openapi/loads"
	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src/kinesisutil"
	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src/middlewares"
	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src/restapi/operations"
	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src/restapi/operations/equity"
	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src/restapi/operations/equity_swaps"
	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src/restapi/operations/fx"
	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src/restapi/operations/health"
	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src/restapi/operations/non_trading_event"
	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src/restapi/operations/options"
	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src/restapi/operations/transfers"
)

var basePath = "/api/ibor/trans/v1"
var swaggerJSONPath = basePath + "/ui/swagger.json"

//go:generate swagger generate server --target .. --name  --spec ../swagger.json

func configureFlags(api *operations.IborTransactionAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

func configureAPI(api *operations.IborTransactionAPI) http.Handler {
	// configure the api here
	api.ServeError = errorhandler.ServeError

	// Set your custom logger if needed. Default one is log.Printf
	// Expected interface func(string, ...interface{})
	//
	// Example:
	// api.Logger = log.Printf

	api.JSONConsumer = runtime.JSONConsumer()

	api.JSONProducer = runtime.JSONProducer()

	api.HealthHealthCheckHandler = health.HealthCheckHandlerFunc(func(params health.HealthCheckParams) middleware.Responder {
		healthCheckResponse := &health.HealthCheckOK{}
		return healthCheckResponse.WithPayload("OK")
	})

	api.EquityEquityTranHandler = equity.EquityTranHandlerFunc(func(params equity.EquityTranParams) middleware.Responder {

		ctx, cancelFunc := ezeutils.WithRequest(params.HTTPRequest.Context(), params.HTTPRequest)
		defer cancelFunc()
		loggerWithCtx := logger.Log.WithContext(ctx)

		usersessionJSON, _ := ezeutils.FromContext(ctx, "userSession")
		userSession := &ezeutils.UserSession{}
		err := json.Unmarshal([]byte(usersessionJSON), userSession)
		if err != nil {
			loggerWithCtx.Error(err)
		}

		dataJSON, errToJSON := json.Marshal(*params.EquityTransaction)
		if errToJSON != nil {
			loggerWithCtx.Error(errToJSON)
		}

		rawData := string(dataJSON)
		loggerWithCtx.Debug(rawData)

		handler.ProcessTransactions(ctx, &rawData, userSession, logger.Log, getKinesisUtilClient("EquityAwsKinesisStream"))

		return equity.NewEquityTranOK()
	})

	api.NonTradingEventNonTradingEventTranHandler = non_trading_event.NonTradingEventTranHandlerFunc(func(params non_trading_event.NonTradingEventTranParams) middleware.Responder {

		ctx, cancelFunc := ezeutils.WithRequest(params.HTTPRequest.Context(), params.HTTPRequest)
		defer cancelFunc()
		loggerWithCtx := logger.Log.WithContext(ctx)

		usersessionJSON, _ := ezeutils.FromContext(ctx, "userSession")
		userSession := &ezeutils.UserSession{}
		err := json.Unmarshal([]byte(usersessionJSON), userSession)
		if err != nil {
			loggerWithCtx.Error(err)
		}

		dataJSON, errToJSON := json.Marshal(*params.NonTradingEventTransaction)
		if errToJSON != nil {
			loggerWithCtx.Error(errToJSON)
		}

		rawData := string(dataJSON)
		loggerWithCtx.Debug(rawData)

		handler.ProcessTransactions(ctx, &rawData, userSession, logger.Log, getKinesisUtilClient("NteAwsKinesisStream"))

		return non_trading_event.NewNonTradingEventTranOK()
	})

	// ***** ETO: Modified Code for Fx-Transaction*****
	api.FxPostFxTranHandler = fx.PostFxTranHandlerFunc(func(params fx.PostFxTranParams) middleware.Responder {

		ctx, cancelFunc := ezeutils.WithRequest(params.HTTPRequest.Context(), params.HTTPRequest)
		defer cancelFunc()
		loggerWithCtx := logger.Log.WithContext(ctx)

		usersessionJSON, _ := ezeutils.FromContext(ctx, "userSession")
		userSession := &ezeutils.UserSession{}
		err := json.Unmarshal([]byte(usersessionJSON), userSession)
		if err != nil {
			loggerWithCtx.Error(err)
		}
		// json all input data (when validation passes)
		// when validation fails -> TOBE handled in ServeError(), still needs to be presisted
		dataJSON, errToJSON := json.Marshal(*params.FxTransaction)
		if errToJSON != nil {
			loggerWithCtx.Error(errToJSON)
		}

		rawData := string(dataJSON)
		loggerWithCtx.Debug(rawData)

		handler.ProcessTransactions(ctx, &rawData, userSession, logger.Log, getKinesisUtilClient("FxAwsKinesisStream"))

		return fx.NewPostFxTranOK()

	})

	api.OptionsOptionTranHandler = options.OptionTranHandlerFunc(func(params options.OptionTranParams) middleware.Responder {
		ctx, cancelFunc := ezeutils.WithRequest(params.HTTPRequest.Context(), params.HTTPRequest)
		defer cancelFunc()
		loggerWithCtx := logger.Log.WithContext(ctx)

		usersessionJSON, _ := ezeutils.FromContext(ctx, "userSession")
		userSession := &ezeutils.UserSession{}
		err := json.Unmarshal([]byte(usersessionJSON), userSession)
		if err != nil {
			loggerWithCtx.Error(err)
		}
		// json all input data (when validation passes)
		// when validation fails -> TOBE handled in ServeError(), still needs to be presisted
		dataJSON, errToJSON := json.Marshal(*params.OptionsTransaction)
		if errToJSON != nil {
			loggerWithCtx.Error(errToJSON)
		}

		rawData := string(dataJSON)
		loggerWithCtx.Debug(rawData)

		handler.ProcessTransactions(ctx, &rawData, userSession, logger.Log, getKinesisUtilClient("OptionsAwsKinesisStream"))

		return options.NewOptionTranOK()
	})

	api.TransfersTransfersTranHandler = transfers.TransfersTranHandlerFunc(func(params transfers.TransfersTranParams) middleware.Responder {
		ctx, cancelFunc := ezeutils.WithRequest(params.HTTPRequest.Context(), params.HTTPRequest)
		defer cancelFunc()
		loggerWithCtx := logger.Log.WithContext(ctx)

		usersessionJSON, _ := ezeutils.FromContext(ctx, "userSession")
		userSession := &ezeutils.UserSession{}
		err := json.Unmarshal([]byte(usersessionJSON), userSession)
		if err != nil {
			loggerWithCtx.Error(err)
		}
		// json all input data (when validation passes)
		// when validation fails -> TOBE handled in ServeError(), still needs to be presisted
		dataJSON, errToJSON := json.Marshal(*params.TransfersTransaction)
		if errToJSON != nil {
			loggerWithCtx.Error(errToJSON)
		}

		rawData := string(dataJSON)
		loggerWithCtx.Debug(rawData)

		handler.ProcessTransactions(ctx, &rawData, userSession, logger.Log, getKinesisUtilClient("TransfersAwsKinesisStream"))

		return transfers.NewTransfersTranOK()
	})

	api.EquitySwapsEquitySwapsTranHandler = equity_swaps.EquitySwapsTranHandlerFunc(func(params equity_swaps.EquitySwapsTranParams) middleware.Responder {
		ctx, cancelFunc := ezeutils.WithRequest(params.HTTPRequest.Context(), params.HTTPRequest)
		defer cancelFunc()
		loggerWithCtx := logger.Log.WithContext(ctx)

		usersessionJSON, _ := ezeutils.FromContext(ctx, "userSession")
		userSession := &ezeutils.UserSession{}
		err := json.Unmarshal([]byte(usersessionJSON), userSession)
		if err != nil {
			loggerWithCtx.Error(err)
		}
		// json all input data (when validation passes)
		// when validation fails -> TOBE handled in ServeError(), still needs to be presisted
		dataJSON, errToJSON := json.Marshal(*params.EquitySwapsTransaction)
		if errToJSON != nil {
			loggerWithCtx.Error(errToJSON)
		}

		rawData := string(dataJSON)
		loggerWithCtx.Debug(rawData)

		handler.ProcessTransactions(ctx, &rawData, userSession, logger.Log, getKinesisUtilClient("EquitySwapsAwsKinesisStream"))

		return equity_swaps.NewEquitySwapsTranOK()
	})

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix"
func configureServer(s *http.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation
func setupMiddlewares(handler http.Handler) http.Handler {
	//return handler
	return middlewares.Authorize(&authorizeHandler{}, handler)
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	handler = installSwaggerJSONHandler(middlewares.InstallUImiddleware(handler))
	if os.Getenv("PPROF") == "1" {
		handler = ezeutils.PprofHandler(handler)
	}
	return handler
}

// InstallSwaggerJSONHandler serves the swagger.json at eos api location rather than server's root path
func installSwaggerJSONHandler(handler http.Handler) http.Handler {
	handlerFunction := func(w http.ResponseWriter, r *http.Request) {

		if r.URL.Path == swaggerJSONPath {
			swaggerSpec, err := loads.Analyzed(SwaggerJSON, "")
			if err != nil {
				//logger.Log.Error("Failed to get swagger spec json.")
			}
			rawSpec := swaggerSpec.Raw()
			rootHandler := middleware.Spec(basePath+"/ui/", rawSpec, http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
				rw.Header().Set("Content-Type", "application/json")
				rw.WriteHeader(http.StatusFound)
				return
			}))
			rootHandler.ServeHTTP(w, r)
			return
		}
		handler.ServeHTTP(w, r)
		return
	}
	return http.HandlerFunc(handlerFunction)
}

func getKinesisUtilClient(streamName string) *kinesisutil.KinesisUtil {
	return kinesisutil.NewKinesisUtil(appconfig.AwsKinesisStreams[streamName])
}

type authorizeHandler struct{}

func (authorizeHandler *authorizeHandler) AuthorizeRequestUsingContext(ctx context.Context, baseURI string, logger *ezelogger.EzeLog) (*ezeutils.UserSession, context.Context, error) {
	return ezeutils.AuthorizeRequestUsingContext(ctx, baseURI, logger)
}
