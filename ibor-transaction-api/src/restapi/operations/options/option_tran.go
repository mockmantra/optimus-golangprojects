// Code generated by go-swagger; DO NOT EDIT.

package options

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	middleware "github.com/go-openapi/runtime/middleware"
)

// OptionTranHandlerFunc turns a function with the right signature into a option tran handler
type OptionTranHandlerFunc func(OptionTranParams) middleware.Responder

// Handle executing the request and returning a response
func (fn OptionTranHandlerFunc) Handle(params OptionTranParams) middleware.Responder {
	return fn(params)
}

// OptionTranHandler interface for that can handle valid option tran params
type OptionTranHandler interface {
	Handle(OptionTranParams) middleware.Responder
}

// NewOptionTran creates a new http.Handler for the option tran operation
func NewOptionTran(ctx *middleware.Context, handler OptionTranHandler) *OptionTran {
	return &OptionTran{Context: ctx, Handler: handler}
}

/*OptionTran swagger:route POST /options Options optionTran

OptionTran option tran API

*/
type OptionTran struct {
	Context *middleware.Context
	Handler OptionTranHandler
}

func (o *OptionTran) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		r = rCtx
	}
	var Params = NewOptionTranParams()

	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params) // actually handle the request

	o.Context.Respond(rw, r, route.Produces, route, res)

}
