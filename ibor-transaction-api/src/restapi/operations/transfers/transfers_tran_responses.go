// Code generated by go-swagger; DO NOT EDIT.

package transfers

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	models "stash.ezesoft.net/imsacnt/ibor-transaction-api/src/models"
)

// TransfersTranOKCode is the HTTP code returned for type TransfersTranOK
const TransfersTranOKCode int = 200

/*TransfersTranOK Ok

swagger:response transfersTranOK
*/
type TransfersTranOK struct {
}

// NewTransfersTranOK creates TransfersTranOK with default headers values
func NewTransfersTranOK() *TransfersTranOK {

	return &TransfersTranOK{}
}

// WriteResponse to the client
func (o *TransfersTranOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.Header().Del(runtime.HeaderContentType) //Remove Content-Type on empty responses

	rw.WriteHeader(200)
}

/*TransfersTranDefault error

swagger:response transfersTranDefault
*/
type TransfersTranDefault struct {
	_statusCode int

	/*
	  In: Body
	*/
	Payload *models.Error `json:"body,omitempty"`
}

// NewTransfersTranDefault creates TransfersTranDefault with default headers values
func NewTransfersTranDefault(code int) *TransfersTranDefault {
	if code <= 0 {
		code = 500
	}

	return &TransfersTranDefault{
		_statusCode: code,
	}
}

// WithStatusCode adds the status to the transfers tran default response
func (o *TransfersTranDefault) WithStatusCode(code int) *TransfersTranDefault {
	o._statusCode = code
	return o
}

// SetStatusCode sets the status to the transfers tran default response
func (o *TransfersTranDefault) SetStatusCode(code int) {
	o._statusCode = code
}

// WithPayload adds the payload to the transfers tran default response
func (o *TransfersTranDefault) WithPayload(payload *models.Error) *TransfersTranDefault {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the transfers tran default response
func (o *TransfersTranDefault) SetPayload(payload *models.Error) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *TransfersTranDefault) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(o._statusCode)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}
