package errorhandler

import (
	"encoding/json"
	"fmt"
	errors "github.com/go-openapi/errors"
	"net/http"
	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src/logger"
	"strings"
)

const head = "HEAD"

func errorAsJSON(err errors.Error) []byte {
	b, _ := json.Marshal(struct {
		Message string `json:"message"`
	}{err.Error()})
	return b
}

func asHTTPCode(input int) int {
	if input >= 600 {
		return 422
	}
	return input
}

func flattenComposite(errs *errors.CompositeError) *errors.CompositeError {
	var res []error
	for _, er := range errs.Errors {
		switch e := er.(type) {
		case *errors.CompositeError:
			if len(e.Errors) > 0 {
				flat := flattenComposite(e)
				if len(flat.Errors) > 0 {
					res = append(res, flat.Errors...)
				}
			}
		default:
			if e != nil {
				res = append(res, e)
			}
		}
	}
	return errors.CompositeValidationError(res...)
}

func wrtieError(rw http.ResponseWriter, errJSON []byte) {
	_, err := rw.Write(errJSON)
	if err != nil {
		logger.Log.Error(err)
	}
}

// ServeError the error handler interface implementation
func ServeError(rw http.ResponseWriter, r *http.Request, err error) {

	rw.Header().Set("Content-Type", "application/json")
	switch e := err.(type) {
	case *errors.ParseError:
		switch err1 := e.Reason.(type) {
		case *json.UnmarshalTypeError:
			logger.Log.Error(err1.Type)
			rw.WriteHeader(asHTTPCode(int(e.Code())))
			if r == nil || r.Method != "HEAD" {
				msg := fmt.Sprintf("Unable to parse JSON payload, %s.%s: Expected type: %s but received type: %s.", err1.Struct, err1.Field, err1.Type, err1.Value)
				logger.Log.Error(msg)
				errJSON := errorAsJSON(errors.New(e.Code(), msg))
				wrtieError(rw, errJSON)
			}
		default:
			errJSON := errorAsJSON(errors.New(e.Code(), err1.Error()))
			rw.WriteHeader(asHTTPCode(int(e.Code())))
			wrtieError(rw, errJSON)

			logger.Log.Error(err1.Error())
		}
	case *errors.CompositeError:
		er := flattenComposite(e)
		// strips composite errors to first element only
		if len(er.Errors) > 0 {
			ServeError(rw, r, er.Errors[0])
		} else {
			// guard against empty CompositeError (invalid construct)
			ServeError(rw, r, nil)
		}
	case *errors.MethodNotAllowedError:
		rw.Header().Add("Allow", strings.Join(err.(*errors.MethodNotAllowedError).Allowed, ","))
		rw.WriteHeader(asHTTPCode(int(e.Code())))
		if r == nil || r.Method != head {
			errJSON := errorAsJSON(e)
			wrtieError(rw, errJSON)
		}
	case errors.Error:
		if e == nil {
			rw.WriteHeader(http.StatusInternalServerError)
			errJSON := errorAsJSON(errors.New(http.StatusInternalServerError, "Unknown error"))
			wrtieError(rw, errJSON)
			return
		}
		rw.WriteHeader(asHTTPCode(int(e.Code())))
		if r == nil || r.Method != head {
			errJSON := errorAsJSON(e)
			wrtieError(rw, errJSON)
		}
	case nil:
		rw.WriteHeader(http.StatusInternalServerError)
		errJSON := errorAsJSON(errors.New(http.StatusInternalServerError, "Unknown error"))
		wrtieError(rw, errJSON)

	default:
		rw.WriteHeader(http.StatusInternalServerError)
		if r == nil || r.Method != head {
			errJSON := errorAsJSON(errors.New(http.StatusInternalServerError, err.Error()))
			wrtieError(rw, errJSON)
		}
	}
}
