package appconfig

import (
	"context"
	"os"
	"strings"

	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src/logger"
)

var (
	// BaseURI is the base URI of the corresponding IMS environment
	BaseURI string
	// IsDevMode should be set to true when running in dev env
	IsDevMode bool
	// RunEnv can be "dev" or "castle"
	RunEnv string
	// AwsProxyURL proxy URL for AWS Kinesis service
	AwsProxyURL string

	// AwsKinesisStreams is the Map for Kinesis streams
	AwsKinesisStreams map[string]string
	// EnvCastle name of Castle environment
	EnvCastle string
)

func init() {
	var cx context.Context
	loggerWithCtx := logger.Log.WithContext(cx)

	EnvCastle = "castle"

	bu := os.Getenv("IMS_BASE_URL")
	if bu == "" {
		loggerWithCtx.Debugf("Please, set IMS_BASE_URL environment variable")
	}
	BaseURI = bu

	ae := os.Getenv("APP_ENV")
	if strings.EqualFold(ae, "local") {
		loggerWithCtx.Debugf("IN DEV MODE")
		IsDevMode = true
	}
	RunEnv = ae

	AwsProxyURL = os.Getenv("AWS_PROXY_URL")

	AwsKinesisStreams = make(map[string]string)

	AwsKinesisStreams["EquityAwsKinesisStream"] = os.Getenv("EQUITY_AWS_STREAM_NAME")
	AwsKinesisStreams["FxAwsKinesisStream"] = os.Getenv("FX_AWS_STREAM_NAME")
	AwsKinesisStreams["OptionsAwsKinesisStream"] = os.Getenv("OPTIONS_AWS_STREAM_NAME")
	AwsKinesisStreams["NteAwsKinesisStream"] = os.Getenv("NTE_AWS_STREAM_NAME")
	AwsKinesisStreams["TransfersAwsKinesisStream"] = os.Getenv("TRANSFERS_AWS_STREAM_NAME")
	AwsKinesisStreams["EquitySwapsAwsKinesisStream"] = os.Getenv("EQUITY_SWAPS_AWS_STREAM_NAME")
}
