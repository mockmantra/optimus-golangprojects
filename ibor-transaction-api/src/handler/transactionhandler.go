package handler

import (
	"context"
	"encoding/base64"

	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src/domain"
	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src/interfaces"
	"stash.ezesoft.net/ipc/ezeutils"
)

// ProcessTransactions ...
func ProcessTransactions(ctx context.Context, rawMessage *string, userSession *ezeutils.UserSession,
	logger interfaces.ILogger, kinesisUtil interfaces.IKinesisUtil) {
	loggerWithCtx := logger.WithContext(ctx)
	activityID, _ := ezeutils.FromContext(ctx, ezeutils.XRequestIdKey)
	authHeader, _ := ezeutils.FromContext(ctx, ezeutils.AuthorizationKey)
	data, _ := base64.StdEncoding.DecodeString(authHeader[6:len(authHeader)])
	sessionToken := string(data)

	// Insert into kinesis
	kinesisUtil.HandleMessage(&domain.KinStreamMessage{
		UserID:           userSession.UserID,
		FirmAuthToken:    userSession.FirmAuthToken,
		UserName:         userSession.UserName,
		FirmID:           userSession.FirmID,
		ActivityID:       activityID,
		Message:          rawMessage,
		UserSessionToken: sessionToken[:len(sessionToken)-1],
	}, loggerWithCtx)
}
