package handler

// import (
// 	"context"
// 	"encoding/base64"
// 	"testing"

// 	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src/domain"
// 	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src/logger"
// 	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src/mocks"
// 	"stash.ezesoft.net/ipc/ezeutils"
// )

// type allMocks struct {
// 	ctx         mocks.Context
// 	logger      mocks.Logger
// 	kinesisUtil mocks.KinesisUtil
// }

// func TestProcessTransactions(t *testing.T) {
// 	// Arrange
// 	var allMocks allMocks
// 	ctx := &allMocks.ctx
// 	rawMessage := "Test Message"
// 	userSession := &ezeutils.UserSession{
// 		FirmID:        1234,
// 		UserID:        1,
// 		FirmAuthToken: "T71Q5",
// 		UserName:      "Test",
// 	}
// 	activityID := "0E050403-CD97-9417-7E14-85DB2ABC76C5"
// 	authHeader := "Basic Z29vZC1nbHgyLXRva2VuLWhhbnNvbG86"
// 	data, _ := base64.StdEncoding.DecodeString(authHeader[6:len(authHeader)])
// 	sessionToken := string(data)
// 	sessionTokenTrim := sessionToken[:len(sessionToken)-1]
// 	allMocks.logger.On("WithContext", ctx).Return(logger.Log).Once()
// 	allMocks.kinesisUtil.On("HandleMessage", &domain.KinStreamMessage{
// 		FirmID:           userSession.FirmID,
// 		Message:          &rawMessage,
// 		FirmAuthToken:    userSession.FirmAuthToken,
// 		UserID:           userSession.UserID,
// 		UserName:         userSession.UserName,
// 		ActivityID:       activityID,
// 		UserSessionToken: sessionTokenTrim,
// 	}, logger.Log).Once()

// 	.Patch(ezeutils.FromContext, func(ctx context.Context, key string) (string, bool) {
// 		if key == "Authorization" {
// 			return authHeader, true
// 		}
// 		return activityID, true
// 	})

// 	// Act
// 	ProcessTransactions(ctx, &rawMessage, userSession, &allMocks.logger, &allMocks.kinesisUtil)

// 	// Assert
// 	allMocks.kinesisUtil.AssertExpectations(t)
// }
