package kinesisutil

import (
	"encoding/json"
	"fmt"
	"os"

	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src/interfaces"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/kinesis"
	"github.com/aws/aws-sdk-go/service/kinesis/kinesisiface"

	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src"
	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src/domain"
)

var awsProxyURL = appconfig.AwsProxyURL

// KinesisUtil type
type KinesisUtil struct {
	client      kinesisiface.KinesisAPI
	streamName  string
	credentials interfaces.IAwsCreds
	session     interfaces.IAwsSession
	kinesis     interfaces.IKinesis
}

func (k *KinesisUtil) createClient(logger interfaces.ILogger) {
	var s *session.Session
	var awsAccessKey string
	var awsSecretKey string
	region := os.Getenv("AWS_REGION")
	if !appconfig.IsDevMode {
		awsAccessKey = os.Getenv("AWS_ACCESS_KEY")
		awsSecretKey = os.Getenv("AWS_SECRET_KEY")
		s = k.session.New(&aws.Config{
			// replace the credentials
			Credentials:      k.credentials.NewStaticCredentials(awsAccessKey, awsSecretKey, ""),
			S3ForcePathStyle: aws.Bool(true),
			Region:           &region,
		})
	} else {
		logger.Debug("Running locally with localstack")
		s = k.session.New(&aws.Config{
			// replace the credentials
			Credentials:      k.credentials.NewStaticCredentials("foo", "var", ""),
			S3ForcePathStyle: aws.Bool(true),
			Region:           &region,
			Endpoint:         aws.String(awsProxyURL),
		})
	}
	logger.Info("Kinesis Session has been created successfully.")
	k.client = k.kinesis.New(s)
}

// insertRecord puts a record into kenisis stream
func (k *KinesisUtil) insertRecord(key string, value []byte) (*kinesis.PutRecordOutput, error) {
	putRecordOutput, err := k.client.PutRecord(&kinesis.PutRecordInput{
		Data:         value,
		StreamName:   aws.String(k.streamName),
		PartitionKey: aws.String(key),
	})

	return putRecordOutput, err
}

// HandleMessage forwards the message to kinesis
func (k *KinesisUtil) HandleMessage(msg *domain.KinStreamMessage, logger interfaces.ILogger) {
	const MaxRetries = 3
	data, _ := json.Marshal(msg)
	if k.client == nil {
		k.createClient(logger)
	}
	retryStr := ""
	for i := 0; i < MaxRetries; i++ {
		if i > 0 {
			retryStr = fmt.Sprintf("Retry:%d ", i)
		}
		logger.Infof("%sStarted inserting the record into kinesis stream: %s", retryStr, k.streamName)
		_, errInsertRecord := k.insertRecord("key123", data)
		if errInsertRecord != nil {
			logger.Warnf("%sError in kenisis PutRecord(): %v", retryStr, errInsertRecord)
		} else {
			logger.Infof("%sSuccessfully inserted record into kinesis stream: %s", retryStr, k.streamName)
			break
		}
		if i == 2 {
			logger.Errorf("%sFailed to insert record into kinesis stream: %s", retryStr, k.streamName)
		}
	}
}
