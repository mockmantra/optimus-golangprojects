package kinesisutil

import (
	"encoding/json"
	"errors"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/stretchr/testify/mock"

	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/service/kinesis"
	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src"
	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src/domain"
	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src/mocks"
)

type allMocks struct {
	client      mocks.KinesisAPI
	logger      mocks.Logger
	credentials mocks.AwsCreds
	session     mocks.AwsSession
	kinesis     mocks.Kinesis
}

func TestKinesisUtil_createClient_InLocal(t *testing.T) {
	// Arrange
	var allMocks allMocks

	k := &KinesisUtil{
		streamName:  "TestStream",
		credentials: &allMocks.credentials,
		session:     &allMocks.session,
		kinesis:     &allMocks.kinesis,
	}

	if k.client != nil {
		t.Error("Kinesis Client should be nil intially")
	}

	allMocks.logger.On("Info", "Kinesis Session has been created successfully.")
	allMocks.logger.On("Debug", "Running locally with localstack")

	var (
		credentials   = &credentials.Credentials{}
		session       = &session.Session{}
		kinesisClient = &kinesis.Kinesis{}
	)
	allMocks.credentials.On("NewStaticCredentials", "foo", "var", "").Return(
		credentials,
	).Once()
	allMocks.session.On("New", mock.Anything).Return(
		session,
	).Once()
	allMocks.kinesis.On("New", session).Return(
		kinesisClient,
	).Once()

	// Act
	k.createClient(&allMocks.logger)

	// Assert
	if k.client == nil {
		t.Error("Kinesis Client should be updated")
	}
	assert.Equal(t, k.client, kinesisClient)
}
func TestKinesisUtil_createClient_InCastle(t *testing.T) {
	// Arrange
	var allMocks allMocks

	k := &KinesisUtil{
		streamName:  "TestStream",
		credentials: &allMocks.credentials,
		session:     &allMocks.session,
		kinesis:     &allMocks.kinesis,
	}

	if k.client != nil {
		t.Error("Kinesis Client should be nil intially")
	}

	allMocks.logger.On("Debug", "Running locally with localstack")

	setEnvErrAccessKey := os.Setenv("AWS_ACCESS_KEY", "test_key")
	if setEnvErrAccessKey != nil {
		t.Errorf("Error while setting AWS_ACCESS_KEY environment variable")
	}
	setEnvErrSecretKey := os.Setenv("AWS_SECRET_KEY", "test_secret")
	if setEnvErrSecretKey != nil {
		t.Errorf("Error while setting AWS_SECRET_KEY environment variable")
	}
	defer resetDevMode(appconfig.IsDevMode)
	appconfig.IsDevMode = false
	var (
		credentials   = &credentials.Credentials{}
		session       = &session.Session{}
		kinesisClient = &kinesis.Kinesis{}
	)
	allMocks.credentials.On("NewStaticCredentials", "test_key", "test_secret", "").Return(
		credentials,
	).Once()
	allMocks.session.On("New", mock.Anything).Return(
		session,
	).Once()
	allMocks.kinesis.On("New", session).Return(
		kinesisClient,
	).Once()
	allMocks.logger.On("Info", "Kinesis Session has been created successfully.")

	// Act
	k.createClient(&allMocks.logger)
	if k.client == nil {
		t.Error("Kinesis Client should be updated")
	}

	// Assert
	assert.Equal(t, k.client, kinesisClient)
}

func TestKinesisUtil_insertRecord_WithoutError(t *testing.T) {
	// Arrange
	var allMocks allMocks

	k := &KinesisUtil{
		client:      &allMocks.client,
		streamName:  "TestStream",
		credentials: &allMocks.credentials,
		session:     &allMocks.session,
		kinesis:     &allMocks.kinesis,
	}
	key := "test123"
	value := []byte("Test")
	putRecord := &kinesis.PutRecordOutput{}
	allMocks.client.On("PutRecord",
		&kinesis.PutRecordInput{
			Data:         value,
			StreamName:   aws.String(k.streamName),
			PartitionKey: aws.String(key),
		}).Return(putRecord, nil).Once()

	// Act
	putResponse, err := k.insertRecord(key, value)

	// Assert
	assert.Nil(t, err)
	assert.Equal(t, putRecord, putResponse)
}

func TestKinesisUtil_insertRecord_WithError(t *testing.T) {
	// Arrange
	var allMocks allMocks

	k := &KinesisUtil{
		client:      &allMocks.client,
		streamName:  "TestStream",
		credentials: &allMocks.credentials,
		session:     &allMocks.session,
		kinesis:     &allMocks.kinesis,
	}
	key := "test123"
	value := []byte("Test")
	allMocks.client.On("PutRecord",
		&kinesis.PutRecordInput{
			Data:         value,
			StreamName:   aws.String(k.streamName),
			PartitionKey: aws.String(key),
		}).Return(nil, errors.New("unable to put the record")).Once()

	// Act
	_, err := k.insertRecord(key, value)

	// Assert
	assert.NotNil(t, err)
}
func TestKinesisUtil_HandleMessage_WithoutClientAndDescribeStream(t *testing.T) {
	// Arrange
	message := "Test"
	msg := domain.KinStreamMessage{FirmID: 1, Message: &message}
	var allMocks allMocks
	appconfig.RunEnv = "dev"
	k := &KinesisUtil{
		streamName:  "TestStream",
		credentials: &allMocks.credentials,
		session:     &allMocks.session,
		kinesis:     &allMocks.kinesis,
	}

	if k.client != nil {
		t.Error("Kinesis Client should be nil intially")
	}

	// createclient expects
	allMocks.logger.On("Debug", "Running locally with localstack")
	allMocks.logger.On("Info", "Kinesis Session has been created successfully.")
	allMocks.logger.On("Infof", "%sStarted inserting the record into kinesis stream: %s", "", "TestStream")
	allMocks.logger.On("Infof", "%sSuccessfully inserted record into kinesis stream: %s", "", "TestStream")

	type KinesisClient struct {
		kinesis.Kinesis
	}

	var (
		credentials   = &credentials.Credentials{}
		session       = &session.Session{}
		kinesisClient = &allMocks.client
	)
	allMocks.credentials.On("NewStaticCredentials", "foo", "var", "").Return(
		credentials,
	).Once()
	allMocks.session.On("New", mock.Anything).Return(
		session,
	).Once()
	allMocks.kinesis.On("New", session).Return(
		kinesisClient,
	).Once()

	// insert record
	key := "key123"
	value, _ := json.Marshal(msg)
	putRecord := &kinesis.PutRecordOutput{}
	allMocks.client.On("PutRecord",
		&kinesis.PutRecordInput{
			Data:         value,
			StreamName:   aws.String(k.streamName),
			PartitionKey: aws.String(key),
		}).Return(putRecord, nil).Once()

	// Act
	k.HandleMessage(&msg, &allMocks.logger)

	// Assert
	allMocks.client.AssertExpectations(t)
}

func TestKinesisUtil_HandleMessage_WithInsertRecordErrorsFirstButPassLater(t *testing.T) {
	// Arrange
	msg := domain.KinStreamMessage{}
	var allMocks allMocks
	appconfig.RunEnv = "dev"
	k := &KinesisUtil{
		streamName:  "TestStream",
		client:      &allMocks.client,
		credentials: &allMocks.credentials,
		session:     &allMocks.session,
		kinesis:     &allMocks.kinesis,
	}

	allMocks.logger.On("Infof", mock.Anything, mock.Anything, mock.Anything)

	// insert record
	key := "key123"
	value, _ := json.Marshal(msg)
	err1 := errors.New("unable put record")
	allMocks.client.On("PutRecord",
		&kinesis.PutRecordInput{
			Data:         value,
			StreamName:   aws.String(k.streamName),
			PartitionKey: aws.String(key),
		}).Return(nil, err1).Times(2)
	allMocks.client.On("PutRecord",
		&kinesis.PutRecordInput{
			Data:         value,
			StreamName:   aws.String(k.streamName),
			PartitionKey: aws.String(key),
		}).Return(&kinesis.PutRecordOutput{}, nil).Times(1)
	allMocks.logger.On("Warnf", mock.Anything, mock.Anything, mock.Anything)
	allMocks.logger.On("Errorf", mock.Anything, mock.Anything, mock.Anything)

	// Act
	k.HandleMessage(&msg, &allMocks.logger)

	// Assert
	allMocks.logger.AssertCalled(t, "Infof", "%sStarted inserting the record into kinesis stream: %s", "", k.streamName)
	allMocks.logger.AssertCalled(t, "Infof", "%sStarted inserting the record into kinesis stream: %s", "Retry:1 ", k.streamName)
	allMocks.logger.AssertCalled(t, "Warnf", "%sError in kenisis PutRecord(): %v", "", err1)
	allMocks.logger.AssertCalled(t, "Warnf", "%sError in kenisis PutRecord(): %v", "Retry:1 ", err1)
	allMocks.logger.AssertCalled(t, "Infof", "%sSuccessfully inserted record into kinesis stream: %s", "Retry:2 ", k.streamName)
	allMocks.client.AssertExpectations(t)
}

func TestKinesisUtil_HandleMessage_WithInsertRecordError(t *testing.T) {
	// Arrange
	msg := domain.KinStreamMessage{}
	var allMocks allMocks
	appconfig.RunEnv = "dev"
	k := &KinesisUtil{
		streamName:  "TestStream",
		client:      &allMocks.client,
		credentials: &allMocks.credentials,
		session:     &allMocks.session,
		kinesis:     &allMocks.kinesis,
	}

	allMocks.logger.On("Infof", mock.Anything, mock.Anything, mock.Anything)

	// insert record
	key := "key123"
	value, _ := json.Marshal(msg)
	err1 := errors.New("unable put record")
	allMocks.client.On("PutRecord",
		&kinesis.PutRecordInput{
			Data:         value,
			StreamName:   aws.String(k.streamName),
			PartitionKey: aws.String(key),
		}).Return(nil, err1).Times(3)
	allMocks.logger.On("Warnf", mock.Anything, mock.Anything, mock.Anything)
	allMocks.logger.On("Errorf", mock.Anything, mock.Anything, mock.Anything)

	// Act
	k.HandleMessage(&msg, &allMocks.logger)

	// Assert
	allMocks.logger.AssertCalled(t, "Infof", "%sStarted inserting the record into kinesis stream: %s", "", k.streamName)
	allMocks.logger.AssertCalled(t, "Infof", "%sStarted inserting the record into kinesis stream: %s", "Retry:1 ", k.streamName)
	allMocks.logger.AssertCalled(t, "Infof", "%sStarted inserting the record into kinesis stream: %s", "Retry:2 ", k.streamName)
	allMocks.logger.AssertCalled(t, "Warnf", "%sError in kenisis PutRecord(): %v", "", err1)
	allMocks.logger.AssertCalled(t, "Warnf", "%sError in kenisis PutRecord(): %v", "Retry:1 ", err1)
	allMocks.logger.AssertCalled(t, "Warnf", "%sError in kenisis PutRecord(): %v", "Retry:2 ", err1)
	allMocks.logger.AssertCalled(t, "Errorf", "%sFailed to insert record into kinesis stream: %s", "Retry:2 ", k.streamName)
	allMocks.client.AssertExpectations(t)
}

func resetDevMode(isDevMode bool) {
	appconfig.IsDevMode = isDevMode
}
