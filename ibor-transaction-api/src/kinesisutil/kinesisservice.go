package kinesisutil

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/kinesis"
	"github.com/aws/aws-sdk-go/service/kinesis/kinesisiface"
)

// NewKinesisUtil returns an instance for KinesisUtil struct
func NewKinesisUtil(streamName string) *KinesisUtil {
	return &KinesisUtil{
		kinesis:     &kinesisAPIClient{},
		credentials: &awsCredsClient{},
		session:     &awsSessionClient{},
		streamName:  streamName,
	}

}

type kinesisAPIClient struct {
}

func (kc *kinesisAPIClient) New(s *session.Session) kinesisiface.KinesisAPI {
	return kinesis.New(s)
}

type awsCredsClient struct {
}

func (ac *awsCredsClient) NewStaticCredentials(id, secret, token string) *credentials.Credentials {
	return credentials.NewStaticCredentials(id, secret, token)
}

type awsSessionClient struct {
}

func (ac *awsSessionClient) New(cfgs ...*aws.Config) *session.Session {
	return session.New(cfgs...)
}
