package kinesisutil

import (
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/kinesis"
	"github.com/stretchr/testify/assert"
)

func Test_NewKinesisUtil(t *testing.T) {
	//Arrange
	streamName := "Test"
	var ku *KinesisUtil

	//Act
	kinesisUtil := NewKinesisUtil(streamName)

	assert.IsType(t, ku, kinesisUtil)
	assert.NotNil(t, kinesisUtil.session)
	assert.NotNil(t, kinesisUtil.session)
	assert.Equal(t, streamName, kinesisUtil.streamName)
}

func Test_KinesisAPIClient_New(t *testing.T) {
	kc := kinesisAPIClient{}
	cfg := &aws.Config{}
	s := session.New(cfg)
	knew := kc.New(s)
	assert.IsType(t, &kinesis.Kinesis{}, knew)
}

func Test_AWSCredsClient_NewStatisCredentials(t *testing.T) {
	ac := awsCredsClient{}
	creds := ac.NewStaticCredentials("1", "test_secret", "test_token")
	value, _ := creds.Get()
	expectedCreds := credentials.NewStaticCredentials("1", "test_secret", "test_token")
	expectedValue, _ := expectedCreds.Get()
	assert.Equal(t, value, expectedValue)
}

func Test_AWSSessionClient_New(t *testing.T) {
	ac := awsSessionClient{}
	cfg := &aws.Config{}
	anew := ac.New(cfg)
	assert.IsType(t, &session.Session{}, anew)
}
