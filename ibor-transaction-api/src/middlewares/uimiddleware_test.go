package middlewares

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInstallUIMiddlewareRedirect(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/ibor/trans/v1", nil)
	assert.Nil(t, err)

	rr := httptest.NewRecorder()
	inputHandler := func(w http.ResponseWriter, r *http.Request) {}
	handler := InstallUImiddleware(http.HandlerFunc(inputHandler))
	handler.ServeHTTP(rr, req)

	assert.Equal(t, rr.Code, 302)
}

func TestInstallUIMiddlewareGetUI(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/ibor/trans/v1/", nil)
	assert.Nil(t, err)

	rr := httptest.NewRecorder()
	inputHandler := func(w http.ResponseWriter, r *http.Request) {}
	handler := InstallUImiddleware(http.HandlerFunc(inputHandler))
	handler.ServeHTTP(rr, req)

	assert.Equal(t, rr.Code, 200)
}

func TestInstallUIMiddlewareInputHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/some/url", nil)
	assert.Nil(t, err)

	rr := httptest.NewRecorder()
	inputHandler := func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "some/url/", 111)
	}
	handler := InstallUImiddleware(http.HandlerFunc(inputHandler))
	handler.ServeHTTP(rr, req)

	assert.Equal(t, rr.Code, 111)
}
