package middlewares

import (
	"context"
	"encoding/json"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"net/http"
	"net/http/httptest"
	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src/mocks"
	"stash.ezesoft.net/ipc/ezelogger"
	"stash.ezesoft.net/ipc/ezeutils"
	"testing"
)

func TestAuthorizeMiddleware_success(t *testing.T) {

	req, err := http.NewRequest("GET", "/route", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	handler := Authorize(&mockAuthorizeHandeler{}, http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		ctx, cancelFunc := ezeutils.WithRequest(req.Context(), req)
		defer cancelFunc()
		usersessionJSON, _ := ezeutils.FromContext(ctx, "userSession")
		userSession := &ezeutils.UserSession{}
		err := json.Unmarshal([]byte(usersessionJSON), userSession)
		assert.Nil(t, err)
		assert.NotNil(t, userSession)
	}))

	handler.ServeHTTP(rr, req)

	assert.Equal(t, rr.Code, http.StatusOK)
}
func TestAuthorization_skip(t *testing.T) {

	req, err := http.NewRequest("GET", "/api/ibor/trans/v1/health", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	handler := Authorize(&mockAuthorizeHandeler{}, http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		ctx, cancelFunc := ezeutils.WithRequest(req.Context(), req)
		defer cancelFunc()
		usersessionJSON, _ := ezeutils.FromContext(ctx, "userSession")
		assert.Empty(t, usersessionJSON)
	}))

	handler.ServeHTTP(rr, req)

	assert.Equal(t, rr.Code, http.StatusOK)
}
func TestAuthorization_error(t *testing.T) {

	req, err := http.NewRequest("GET", "route", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	mockhanlder := &mocks.AuthorizeHandler{}
	mockhanlder.On("AuthorizeRequestUsingContext", mock.Anything, mock.Anything, mock.Anything).Return(
		nil, nil, &ezeutils.StatusError{Code: 401, Err: errors.New("unauthorized")},
	).Once()

	handler := Authorize(mockhanlder, http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		ctx, cancelFunc := ezeutils.WithRequest(req.Context(), req)
		defer cancelFunc()
		usersessionJSON, _ := ezeutils.FromContext(ctx, "userSession")
		assert.Empty(t, usersessionJSON)
	}))

	handler.ServeHTTP(rr, req)

	assert.Equal(t, rr.Code, 401)
}

func TestAuthorization_errorAlternate(t *testing.T) {

	req, err := http.NewRequest("GET", "route", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	mockhanlder := &mocks.AuthorizeHandler{}
	mockhanlder.On("AuthorizeRequestUsingContext", mock.Anything, mock.Anything, mock.Anything).Return(
		nil, nil, errors.New("Other error"),
	).Once()

	handler := Authorize(mockhanlder, http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		ctx, cancelFunc := ezeutils.WithRequest(req.Context(), req)
		defer cancelFunc()
		usersessionJSON, _ := ezeutils.FromContext(ctx, "userSession")
		assert.Empty(t, usersessionJSON)
	}))

	handler.ServeHTTP(rr, req)

	assert.Equal(t, rr.Code, 500)
}

type mockAuthorizeHandeler struct{}

func (authorizeHandeler *mockAuthorizeHandeler) AuthorizeRequestUsingContext(ctx context.Context, baseURI string, logger *ezelogger.EzeLog) (*ezeutils.UserSession, context.Context, error) {

	userSession := &ezeutils.UserSession{FirmID: 2, UserID: 1, FirmName: "Mock Ims", UserName: "hansolo", FirmAuthToken: "TBZQK"}

	return userSession, ctx, nil
}
