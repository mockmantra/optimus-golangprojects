// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
	"github.com/go-openapi/validate"
)

// FeeCharge fee charge
// swagger:model feeCharge
type FeeCharge struct {

	// fee charge
	// Required: true
	FeeCharge *float64 `json:"FeeCharge"`

	// fee charge Id
	// Required: true
	FeeChargeID *int64 `json:"FeeChargeId"`
}

// Validate validates this fee charge
func (m *FeeCharge) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateFeeCharge(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateFeeChargeID(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *FeeCharge) validateFeeCharge(formats strfmt.Registry) error {

	if err := validate.Required("FeeCharge", "body", m.FeeCharge); err != nil {
		return err
	}

	return nil
}

func (m *FeeCharge) validateFeeChargeID(formats strfmt.Registry) error {

	if err := validate.Required("FeeChargeId", "body", m.FeeChargeID); err != nil {
		return err
	}

	return nil
}

// MarshalBinary interface implementation
func (m *FeeCharge) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *FeeCharge) UnmarshalBinary(b []byte) error {
	var res FeeCharge
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
