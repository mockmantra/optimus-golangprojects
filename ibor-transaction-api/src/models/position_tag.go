// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
	"github.com/go-openapi/validate"
)

// PositionTag position tag
// swagger:model positionTag
type PositionTag struct {

	// index attribute Id
	// Required: true
	IndexAttributeID *int64 `json:"IndexAttributeId"`

	// index attribute value Id
	// Required: true
	IndexAttributeValueID *int64 `json:"IndexAttributeValueId"`
}

// Validate validates this position tag
func (m *PositionTag) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateIndexAttributeID(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateIndexAttributeValueID(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *PositionTag) validateIndexAttributeID(formats strfmt.Registry) error {

	if err := validate.Required("IndexAttributeId", "body", m.IndexAttributeID); err != nil {
		return err
	}

	return nil
}

func (m *PositionTag) validateIndexAttributeValueID(formats strfmt.Registry) error {

	if err := validate.Required("IndexAttributeValueId", "body", m.IndexAttributeValueID); err != nil {
		return err
	}

	return nil
}

// MarshalBinary interface implementation
func (m *PositionTag) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *PositionTag) UnmarshalBinary(b []byte) error {
	var res PositionTag
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
