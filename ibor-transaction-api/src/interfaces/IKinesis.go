package interfaces

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/kinesis/kinesisiface"
	"stash.ezesoft.net/imsacnt/ibor-transaction-api/src/domain"
)

// IKinesisUtil interface
type IKinesisUtil interface {
	HandleMessage(msg *domain.KinStreamMessage, logger ILogger)
}

// IAwsCreds interface
type IAwsCreds interface {
	NewStaticCredentials(id, secret, token string) *credentials.Credentials
}

// IAwsSession interface
type IAwsSession interface {
	New(cfgs ...*aws.Config) *session.Session
}

// IKinesis interface
type IKinesis interface {
	New(s *session.Session) kinesisiface.KinesisAPI
}
