package domain

// KinStreamMessage message type
type KinStreamMessage struct {
	FirmID           int     `json:"firmId"`
	FirmAuthToken    string  `json:"firmAuthToken"`
	UserID           int     `json:"userId"`
	UserName         string  `json:"userName"`
	ActivityID       string  `json:"activityId"`
	Message          *string `json:"message"`
	UserSessionToken string  `json:"userSessionToken"`
}
