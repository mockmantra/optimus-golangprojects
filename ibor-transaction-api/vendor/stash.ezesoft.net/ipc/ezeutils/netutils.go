package ezeutils

import (
	"net"
	"net/url"
	"time"

	"github.com/pkg/errors"
	"stash.ezesoft.net/ipc/ezelogger"
)

var (
	// DNSResolved is a channel, which will be closed once DNS is successfully resolved
	DNSResolved = make(chan interface{})
)

func QueryDNS(log ezelogger.EzeLogger, urlsToCheck ...string) error {
	if log == nil {
		return errors.New("QueryDNS requires a valid EzeLog instance (nil has been passed)")
	}

	go func() {
		for {
			select {
			case <-time.After(2000 * time.Millisecond):
				resolveResults := make(chan bool, len(urlsToCheck))
				for _, urlString := range urlsToCheck {
					go func(u string) {
						url, err := url.Parse(u)
						if err != nil {
							log.Errorf("failed to parse url '%s': %v", u, err)
							resolveResults <- false
							return
						}
						addrs, err := net.LookupHost(url.Hostname())
						if err != nil {
							log.Warnf("failed to lookup base url's host '%s': %v", url.Hostname(), err)
						}
						resolveResults <- (err == nil && len(addrs) > 0)
					}(urlString)
				}
				dnsResolved := true
				for range urlsToCheck {
					dnsResolved = dnsResolved && <-resolveResults
				}
				if dnsResolved {
					log.Info("Hosts for BaseURL and consul-agent have been resolved")
					close(DNSResolved)
					return
				} else {
					log.Warnf("Will retry DNS resolution in 2000ms...")
				}
			}
		}
	}()
	return nil
}
