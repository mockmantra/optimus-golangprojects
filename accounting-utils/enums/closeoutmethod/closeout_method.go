package closeoutmethod

const (
	// Unknown Closeout Method
	Unknown = 0
	// FIFO Closeout Method
	FIFO = 1
	// LIFO Closeout Method
	LIFO = 2
	// HIFO Closeout Method
	HIFO = 3
	// LOFO Closeout Method
	LOFO = 4
	// AverageCost Closeout Method
	AverageCost = 5
	// SpecificLot Closeout Method
	SpecificLot = 6
	// MinTax Closeout Method
	MinTax = 7
	// MinTaxLongTermFirst Closeout Method
	MinTaxLongTermFirst = 8
	// MinTaxShortTermFirst Closeout Method
	MinTaxShortTermFirst = 9
)
