package side

const (
	// Unknown Side
	Unknown = 0
	// Long Side
	Long = 1
	// Short Side
	Short = 2
)
