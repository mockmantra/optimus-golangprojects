package logicaltime

const (
	// Unknown Logical Time
	Unknown = 0
	// StartOfDay Logical Time
	StartOfDay = 50
	// TradingDay Logical Time
	TradingDay = 100
	// EndOfDay Logical Time
	EndOfDay = 150
)
