package incomeexpensetype

const (
	// Unknown Income Expense Type
	Unknown = 0
	// Admin Income Expense Type
	Admin = 1
	// Legal Income Expense Type
	Legal = 2
	// Management Income Expense Type
	Management = 3
	// Interest Income Expense Type
	Interest = 4
	// Dividend Income Expense Type
	Dividend = 5
	// Cash Income Expense Type
	Cash = 6
)
