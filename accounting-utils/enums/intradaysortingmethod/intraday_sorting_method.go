package intradysortingmethod

const (
	// Unknown Intraday Sorting Method
	Unknown = 0
	// DateAscending Intraday Sorting Method
	DateAscending = 1
	// DateDescending Intraday Sorting Method
	DateDescending = 2
	// TradePriceAscending Intraday Sorting Method
	TradePriceAscending = 3
	// TradePriceDescending Intraday Sorting Method
	TradePriceDescending = 4
)
