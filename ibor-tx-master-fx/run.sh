export Connection_String="root:Optimus123@tcp(127.0.0.1:3306)/testDb?parseTime=true"
export VAULT_CERT="vault_cert_value"
export VAULT_KEY="vault_key_value"
export VAULT_TOKEN="vault_token_value"
export AWS_PROXY_URL="http://localhost:4568"
export AWS_UP_STREAM_NAME="CastleAccountingTxFx01"
export AWS_DOWN_STREAM_NAME="CastleAccountingTxFxIntraday01"
export PORT=6989
go run src/cmd/ibor-tx-master-fx/main.go