package persisttransaction

import (
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/mocks"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/models"
)

type allMocks struct {
	ctx          mocks.Context
	logger       mocks.Logger
	vault        mocks.Vault
	dataAccessor mocks.DataAccessor
}

func TestProcessTransactions_WithInsertTrasactionError(t *testing.T) {
	var allMocks allMocks
	transaction := &models.Transactiontable{
		Message: "Test Message",
	}

	var eventTypeID models.EventTypeID = 12
	sourceSystemName := "Trading"
	var securityID models.SecurityID = 202
	var orderQuantity = float64(100)
	date := "2018-09-08T13:25:43.0000000Z"
	tdate, errF := time.Parse(time.RFC3339, date)
	if errF != nil {
		assert.NotNil(t, errF)
	}
	errMessage := "unable to insert record"
	allMocks.dataAccessor.On("InsertTransaction", transaction, tdate).Return(errors.New(errMessage)).Once()

	_, err := PersistTransactionsMaster(transaction, &allMocks.logger, eventTypeID, sourceSystemName, securityID, orderQuantity,
		&allMocks.dataAccessor, "rawStr", 1, "userName", "userSessionToken", "testActivityId", "2018-09-08T13:25:43.0000000Z", "400", "2018-09-08T13:25:43.0000000Z")

	assert.EqualError(t, err, errMessage)

	allMocks.dataAccessor.AssertExpectations(t)
	allMocks.logger.AssertExpectations(t)
}
func TestProcessTransactions_WithInsertTrasactionSuccess(t *testing.T) {
	var allMocks allMocks
	transaction := &models.Transactiontable{
		Message:  "Test Message",
		RecordID: 1,
	}
	allMocks.logger.On("Infof", "Success saving transaction to db, recordId:%d", 1).Return(nil).Once()

	var eventTypeID models.EventTypeID = 12
	sourceSystemName := "Trading"
	var securityID models.SecurityID = 202
	var orderQuantity = float64(100)
	date := "2018-09-08T13:25:43.0000000Z"
	tdate, errF := time.Parse(time.RFC3339, date)
	if errF != nil {
		assert.NotNil(t, errF)
	}
	allMocks.dataAccessor.On("InsertTransaction", transaction, tdate).Return(nil).Once()

	recordID, err := PersistTransactionsMaster(transaction, &allMocks.logger, eventTypeID, sourceSystemName, securityID, orderQuantity,
		&allMocks.dataAccessor, "rawStr", 1, "userName", "userSessionToken", "testActivityId", "2018-09-08T13:25:43.0000000Z", "400", "2018-09-08T13:25:43.0000000Z")

	assert.NotNil(t, recordID)
	assert.Nil(t, err)
	allMocks.dataAccessor.AssertExpectations(t)
	allMocks.logger.AssertExpectations(t)

}
func TestProcessTransactions_WithBusinessDateTimeParseError(t *testing.T) {
	var allMocks allMocks
	transaction := &models.Transactiontable{
		Message:  "Test Message",
		RecordID: 1,
	}

	var eventTypeID models.EventTypeID = 12
	sourceSystemName := "Trading"
	var securityID models.SecurityID = 202
	var orderQuantity = float64(100)

	recordID, err := PersistTransactionsMaster(transaction, &allMocks.logger, eventTypeID, sourceSystemName, securityID, orderQuantity,
		&allMocks.dataAccessor, "rawStr", 1, "userName", "userSessionToken", "testActivityId", "2018-09-08 13:25:43.00", "400", "2018-09-08T13:25:43.0000000Z")

	assert.Equal(t, recordID, 0)
	assert.NotNil(t, err)
	allMocks.dataAccessor.AssertExpectations(t)
	allMocks.logger.AssertExpectations(t)

}
