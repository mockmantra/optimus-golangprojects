package servicesession

import (
	"bytes"
	// "context"
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/url"
	"strings"
	"time"

	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/logger"
	config "stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/svcloginconfig"
	"stash.ezesoft.net/ipc/ezeutils"
)

// kv/ims/Settings/Blue/Framework/EnvironmentName

const (
	messageFormat         = "%s|%s|%s|%s|%s|%s|%s"
	saltLength            = 16
	dpermsTimestampFormat = "1/2/2006 3:04:05 PM"
	dpermsUriKey          = "DPermsConfigDPermsRestUri"
	sigKey                = "sig"
	mimeTypeKey           = "Content-Type"
	jsonMimeType          = "application/json"
	glxTokenKey           = "glx2"
)

func getMac(message, secret string) (string, error) {
	byteAr, err := base64.StdEncoding.DecodeString(secret)
	if err != nil {
		return "", errors.Wrap(err, "failed to decode64 service sharedKey")
	}
	hmac := hmac.New(sha256.New, byteAr)
	if _, err = hmac.Write([]byte(message)); err != nil {
		return "", errors.Wrap(err, "failed to write hmac")
	}
	return hex.EncodeToString(hmac.Sum(nil)), nil
}

// getSignature calculates the sig to send to dperms, together with the message.
// note that in platform code this function has the somewhat deceiving name "GetSecret"
// see https://stash.ezesoft.net/projects/CLOUD/repos/platformcloud/browse/Cloud/Source/Eze.Ims.Platform.Cloud.Framework/ServiceAuthentication/ServiceAuthenticationSecretProvider.cs
func getSignature(sharedKey, salt, username, firmID, environment, timestamp, dpermsId, dpermsVersion string) (string, error) {
	message := fmt.Sprintf(messageFormat, salt, username, firmID, environment, timestamp, dpermsId, dpermsVersion)
	return getMac(message, sharedKey)
}

// MineSomeSalt returns a random string, crypto safe, of given length
// string is made of "base64" characters only.
func mineSomeSalt(length int) (result string, err error) {
	buff := make([]byte, (length+1)/2) // more than we really need, but kinda safe.
	if _, err = rand.Read(buff); err == nil {
		result = hex.EncodeToString(buff)[:length] // make sure our salt is polite - no funny characters
	}
	return
}

type xtype struct {
	index int
}

func (xvar xtype) dosomething() {
	xvar.index++
}

// GetServiceToken ...
func GetServiceToken(firm string) (string, error) {
	activityID := uuid.NewV4().String()
	xvar := xtype{index: 12}
	xvar.dosomething()

	salt, err := mineSomeSalt(saltLength)
	if err != nil {
		return "", errors.Wrap(err, "failed to mine salt")
	}

	timestamp := time.Now().UTC().Format(dpermsTimestampFormat)
	sig, err := getSignature(config.ServiceSessionVars.SharedKey,
		salt, config.IntiServiceUser, firm, config.ServiceSessionVars.EnvName, timestamp, config.ServiceSessionVars.DpermsIdKey,
		config.ServiceSessionVars.DpermsVersion)
	body := map[string]string{sigKey: sig}
	jsonBody, err := json.Marshal(body)
	if err != nil {
		return "", errors.Wrap(err, "failed to marshal request body")
	}

	values := url.Values{}
	// "%s/perms/svclogin_f?salt=%s&username=%s&firm=%s&envir=%s&timestamputc=%s&usedefaultdomain=False&appid=%s&version=%s"
	values.Add("salt", salt)
	values.Add("username", config.IntiServiceUser)
	values.Add("firm", firm)
	values.Add("envir", config.ServiceSessionVars.EnvName)
	values.Add("timestamputc", timestamp)
	values.Add("usedefaultdomain", "False")
	values.Add("appid", config.ServiceSessionVars.DpermsIdKey)
	values.Add("version", config.ServiceSessionVars.DpermsVersion)
	values.Add("activityid", activityID)
	url := fmt.Sprintf("http://%s/perms/svclogin_f?%s", config.ServiceSessionVars.DpermsUri, values.Encode())

	req, err := ezeutils.NewRequest("POST", url, bytes.NewReader(jsonBody))
	if err != nil {
		return "", errors.Wrap(err, "failed to create new request")
	}
	req.Header.Set(ezeutils.XRequestIdKey, activityID)
	req.Header.Set(mimeTypeKey, jsonMimeType)
	req.Header.Set(ezeutils.UserAgentKey, "Inti-Consumer/1.0 Go-http-client/1.1")
	client := ezeutils.NewRetryableClient()
	client.RetryMax = 2
	response, err := client.Do(req)

	if err != nil {
		return "", errors.Wrapf(err, "post http call to dperms host %v", config.ServiceSessionVars.DpermsUri)
	}
	logger.Log.Infof("create service user returned status %v", response.Status)
	defer response.Body.Close()

	switch response.StatusCode {
	case 200:
		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			return "", errors.Wrap(err, "could not read dperms response body")
		}
		var unMarshaled map[string]string
		err = json.Unmarshal(body, &unMarshaled)
		if err != nil {
			return "", errors.Wrap(err, "failed to unmarshal dperms response body")
		}
		result, ok := unMarshaled[glxTokenKey]
		if !ok {
			return "", errors.New("dperms response body had no glx2 token")
		}
		if strings.TrimSpace(result) == "" {
			return "", errors.New("dperms response had glx2 token empty")
		}
		return result, nil
	default:
		return "", errors.Errorf("dperms returned %d (%s) dperms host %v", response.StatusCode, response.Status, config.ServiceSessionVars.DpermsUri)
	}
}
