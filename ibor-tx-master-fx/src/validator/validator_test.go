package validator

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/httputil"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/mocks"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/models"
)

var validatorValidTestJSON = `{
	"Allocations": [
		{
			"BookTypeId": 2,
			"DealtSettleAmount": 100,
			"PortfolioId": 3
		}
	],
	"EventTypeId": 1013
  }`

var validatorPanicTestJSON = `{
	"SystemCurrencySecurityID": 1234,
	"EventTypeId": 1013,
	"Allocations":null
  }`
var validatorPanicAllocationTestJSON = `{
	"Allocations": [
		{
			"PortfolioId": 3,
			"DealtSettleAmount": 100,
			"BookTypeId": 3,
			"PositionTags": [
				{
				  "IndexAttributeId": 1,
				  "IndexAttributeValueId": 3
				}
			]
		}
	],
	"SystemCurrencySecurityID": 1234
  }`

type allMocks struct {
	logger mocks.Logger
}

func TestValidateValidData(t *testing.T) {
	var validatorTestData models.Transaction
	httputil.UserSessionToken = "123"
	var allmocks allMocks
	log := &allmocks.logger
	unmarshalErr := json.Unmarshal([]byte(validatorValidTestJSON), &validatorTestData)
	assert.Nil(t, unmarshalErr)
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	}))
	appconfig.BaseURI = ts.URL
	defer ts.Close()

	errors := Validate(validatorTestData, log)
	errs := []ErrorMessage{ErrorMessage{Field: "SettleDate", Message: []string{"Invalid SettleDate. Invalid format"}}, ErrorMessage{Field: "TradeDate", Message: []string{"Invalid TradeDate. Invalid format"}}, ErrorMessage{Field: "TransactionDateTimeUTC", Message: []string{"Invalid TransactionDateTimeUTC. Incorrect format"}}, ErrorMessage{Field: "SecurityId", Message: []string{"Invalid SecurityId. SecurityId: 0 does not exist"}}, ErrorMessage{Field: "EventTypeId", Message: []string{"EventTypeId : Service lookup failed"}}, ErrorMessage{Field: "TransactionTimeZoneId", Message: []string{"Invalid TransactionTimeZoneId. TimeZoneId: 0 does not exist"}}, ErrorMessage{Field: "SystemCurrencySecurityId", Message: []string{"Invalid SystemCurrencySecurityId. SystemCurrencySecurityId 0  does not exist"}}, ErrorMessage{Field: "DealtCurrencySecurityId", Message: []string{"Invalid DealtCurrencySecurityId. DealtCurrencySecurityId 0  does not exist"}}, ErrorMessage{Field: "BusinessDateTimeUTC", Message: []string{"BusinessDateTimeUTC : Service lookup failed"}}, ErrorMessage{Field: "PortfolioId", Message: []string{"Invalid PortfolioId: 3 does not exist"}}, ErrorMessage{Field: "CounterCurrencySecurityId", Message: []string{"Invalid CounterCurrencySecurityId. CounterCurrencySecurityId 0  does not exist"}}, ErrorMessage{Field: "SecurityTemplateId", Message: []string{"Invalid SecurityTemplateId. SecurityTemplateId: 0 does not exist"}}}
	assert.ElementsMatch(t, errors, errs)
	httputil.UserSessionToken = ""
}

func TestPanicInAllocationFieldsValidation(t *testing.T) {
	var validatorTestData models.Transaction
	var allmocks allMocks
	log := &allmocks.logger
	httputil.UserSessionToken = "123"
	unmarshalErr := json.Unmarshal([]byte(validatorPanicAllocationTestJSON), &validatorTestData)
	assert.Nil(t, unmarshalErr)
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.Contains(r.URL.Path, "api/referencedata/v1/PositionTag/GetAllWithMembers") {
			_, err := w.Write([]byte("[{\"Members\":[{}]}]"))
			if err != nil {
				t.Error("Unable to write the stream")
			}
		}
		w.WriteHeader(http.StatusNotFound)
	}))
	appconfig.BaseURI = ts.URL
	defer ts.Close()
	log.On("Errorf", mock.Anything, mock.Anything, mock.Anything).Return()
	errors := Validate(validatorTestData, log)
	errs := []ErrorMessage{ErrorMessage{Field: "SettleDate", Message: []string{"Invalid SettleDate. Invalid format"}}, ErrorMessage{Field: "TradeDate", Message: []string{"Invalid TradeDate. Invalid format"}}, ErrorMessage{Field: "TransactionDateTimeUTC", Message: []string{"Invalid TransactionDateTimeUTC. Incorrect format"}}, ErrorMessage{Field: "SecurityTemplateId", Message: []string{"Invalid SecurityTemplateId. SecurityTemplateId: 0 does not exist"}}, ErrorMessage{Field: "DealtCurrencySecurityId", Message: []string{"Invalid DealtCurrencySecurityId. DealtCurrencySecurityId 0  does not exist"}}, ErrorMessage{Field: "SecurityId", Message: []string{"Invalid SecurityId. SecurityId: 0 does not exist"}}, ErrorMessage{Field: "BusinessDateTimeUTC", Message: []string{"BusinessDateTimeUTC : Service lookup failed"}}, ErrorMessage{Field: "EventTypeId", Message: []string{"EventTypeId : Service lookup failed"}}, ErrorMessage{Field: "PortfolioId", Message: []string{"Invalid PortfolioId: 3 does not exist"}}, ErrorMessage{Field: "TransactionTimeZoneId", Message: []string{"Invalid TransactionTimeZoneId. TimeZoneId: 0 does not exist"}}, ErrorMessage{Field: "SystemCurrencySecurityId", Message: []string{"Invalid SystemCurrencySecurityId. SystemCurrencySecurityId 1234  does not exist"}}, ErrorMessage{Field: "PositionTags", Message: []string{"Failed to validate field : PositionTags in allocation with PortfolioID : 3"}}, ErrorMessage{Field: "CounterCurrencySecurityId", Message: []string{"Invalid CounterCurrencySecurityId. CounterCurrencySecurityId 0  does not exist"}}}
	assert.ElementsMatch(t, errors, errs)
	log.AssertExpectations(t)
	httputil.UserSessionToken = ""
}

// func TestPanicInMainFieldsValidation(t *testing.T) {
// 	var validatorTestData models.Transaction
// 	var allmocks allMocks
// 	log := &allmocks.logger
// 	httputil.UserSessionToken = "123"
// 	unmarshalErr := json.Unmarshal([]byte(validatorPanicTestJSON), &validatorTestData)
// 	assert.Nil(t, unmarshalErr)
// 	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
// 		_, err := w.Write(nil)
// 		if err != nil {
// 			t.Error("Error in writing to response")
// 		}
// 	}))
// 	appconfig.BaseURI = ts.URL
// 	defer ts.Close()
// 	monkey.Patch(models.NewSecurityRequest, func(securityID int) ([]models.Security, error) {
// 		return []models.Security{}, nil
// 	})
// 	log.On("Errorf", mock.Anything, mock.Anything, mock.Anything).Return()
// 	errors := Validate(validatorTestData, log)
// 	errs := []ErrorMessage{ErrorMessage{Field: "SourceSystemReference", Message: []string{"SourceSystemReference: Service lookup failed"}}, ErrorMessage{Field: "SystemCurrencySecurityId", Message: []string{"Failed to validate the field: SystemCurrencySecurityId"}}, ErrorMessage{Field: "SecurityId", Message: []string{"Failed to validate the field: SecurityId"}}, ErrorMessage{Field: "SettleDate", Message: []string{"Invalid SettleDate. Invalid format"}}, ErrorMessage{Field: "CounterCurrencySecurityId", Message: []string{"Failed to validate the field: CounterCurrencySecurityId"}}, ErrorMessage{Field: "TransactionDateTimeUTC", Message: []string{"Invalid TransactionDateTimeUTC. Incorrect format"}}, ErrorMessage{Field: "TradeDate", Message: []string{"Invalid TradeDate. Invalid format"}}, ErrorMessage{Field: "DealtCurrencySecurityId", Message: []string{"Failed to validate the field: DealtCurrencySecurityId"}}, ErrorMessage{Field: "SecurityTemplateId", Message: []string{"Invalid SecurityTemplateId. Service lookup failed"}}, ErrorMessage{Field: "BusinessDateTimeUTC", Message: []string{"BusinessDateTimeUTC : Service lookup failed"}}, ErrorMessage{Field: "EventTypeId", Message: []string{"EventTypeId : Service lookup failed"}}, ErrorMessage{Field: "TransactionTimeZoneId", Message: []string{"TransactionTimeZoneId: Service lookup failed"}}}
// 	assert.ElementsMatch(t, errors, errs)
// 	// log.AssertExpectations(t)
// 	httputil.UserSessionToken = ""
// }
