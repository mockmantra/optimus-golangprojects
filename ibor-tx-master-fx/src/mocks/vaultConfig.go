// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import (
	"errors"

	"github.com/hashicorp/vault/api"
	"github.com/stretchr/testify/mock"
)

// IVaultConfig is an autogenerated mock type for the IVaultConfig type
type VaultConfig struct {
	mock.Mock
}

// ConfigureTLS provides a mock function with given fields: _a0
func (_m *VaultConfig) ConfigureTLS(_a0 *api.TLSConfig) error {
	ret := _m.Called(_a0)

	var r0 error
	if rf, ok := ret.Get(0).(func(*api.TLSConfig) error); ok {
		r0 = rf(_a0)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// ReadEnvironment provides a mock function with given fields:
func (_m *VaultConfig) ReadEnvironment() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

type MockConfig struct {
}

func (m *MockConfig) ConfigureTLS(*api.TLSConfig) error {
	return errors.New("Close failed")
}

func (m *MockConfig) ReadEnvironment() error {
	return errors.New("Close failed")
}
