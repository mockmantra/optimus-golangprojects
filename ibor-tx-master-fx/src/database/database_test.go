package database

import (
	"log"
	"testing"
	"time"

	mocket "github.com/Selvatico/go-mocket"
	_ "github.com/go-sql-driver/mysql" //using mysql package in gorm.open()
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/logger"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/mocks"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/models"
)

// Tests

// DataAccessorInstance() test
func TestDataAccessorInstance(t *testing.T) {
	da := DataAccessorInstance()
	assert.NotNil(t, da)
}

func SetupTests() *gorm.DB {
	mocket.Catcher.Register()

	db, err := gorm.Open(mocket.DriverName, "root:Joy@915ETN@tcp(127.0.0.1:3306)/testDb")
	if err != nil {
		log.Fatalf("error mocking gorm: %s", err)
	}
	db.LogMode(true)

	return db
}

// If Create() fails
func TestInsertTransactionCreateError(t *testing.T) {
	mockVault := new(mocks.Vault)
	dbmock := SetupTests()
	da := DataAccessor{dbmock, "TestFirmName", logger.Log, mockVault}
	date := "2018-09-08T13:25:43.0000000Z"
	tdate, errF := time.Parse(time.RFC3339, date)
	if errF != nil {
		assert.NotNil(t, errF)
	}
	fake := []map[string]interface{}{{"count": 1}}
	mocket.Catcher.Reset().NewMock().WithQuery("SELECT count(*) FROM \"Ibor_Fx_Transactions\"").WithReply(fake).OneTime()
	err := da.InsertTransaction(&models.Transactiontable{}, tdate)
	assert.NotNil(t, err)
}

// If everything is successful
func TestInsertTransactionSuccessful(t *testing.T) {
	dbmock := SetupTests()
	mockVault := new(mocks.Vault)
	da := DataAccessor{dbmock, "TestFirmName", logger.Log, mockVault}
	date := "2018-09-08T13:25:43.0000000Z"
	tdate, errF := time.Parse(time.RFC3339, date)
	if errF != nil {
		assert.NotNil(t, errF)
	}
	fakeResponse := []map[string]interface{}{{"RecordID": 1, "MessageBody": "ABCD", "ActivityID": "12",
		"BusinessDateTimeUTC": time.Now(), "IsDeleted": false, "ValidFromUTC": time.Now(), "ValidToUTC": time.Now(), "EventTypeID": 12,
		"SecurityID": 12, "OrderQuantity": 11, "SourceSystemName": "12", "UserID": 11, "UserName": "San", "UserSessionToken": "wew"}}
	fake := []map[string]interface{}{{"count": 1}}
	mocket.Catcher.Reset().NewMock().WithQuery("SELECT count(*) FROM \"Ibor_Fx_Transactions\"  WHERE").WithExecException().OneTime()
	mocket.Catcher.NewMock().WithQuery("SELECT * FROM \"Ibor_Fx_Transactions\" WHERE").WithReply(fakeResponse[0:1]).OneTime()
	mocket.Catcher.NewMock().WithQuery("SELECT count(*) FROM \"Ibor_Fx_Transactions\"  WHERE").WithReply(fake).OneTime()
	mocket.Catcher.NewMock().WithQuery("UPDATE \"transactiontables\" SET ").WithReply(fake)
	err := da.InsertTransaction(&models.Transactiontable{
		SourceSystemName:      "Trading",
		SourceSystemReference: "4803",
		IsDeleted:             false,
	}, tdate)

	assert.Nil(t, err)
}

// getTenantCredentials() tests
// Need Vault Mock to test getTenantCredentials
func TestGetTenantCredentialsTokenExists(t *testing.T) {
	dbmock := SetupTests()
	mockVault := new(mocks.Vault)
	da := DataAccessor{dbmock, "TestFirmName", logger.Log, mockVault}
	mockAuroraConnection := &models.AuroraConnection{Host: "testHost", Password: "testpass", Port: 8888, User: "testUser"}
	mockMap := map[string]*models.AuroraConnection{"testFirmToken": mockAuroraConnection}
	mockVault.On("GetDbCredentials").Return(mockMap)

	con, err := da.getTenantCredentials("testFirmToken")
	assert.Nil(t, err)
	assert.Equal(t, con, mockAuroraConnection)
}

func TestGetTenantCredentialsTokenDoesNotExist(t *testing.T) {
	dbmock := SetupTests()
	mockVault := new(mocks.Vault)
	da := DataAccessor{dbmock, "TestFirmName", logger.Log, mockVault}
	mockMap := map[string]*models.AuroraConnection{}
	mockVault.On("GetDbCredentials").Return(mockMap)

	con, err := da.getTenantCredentials("testFirmToken")
	assert.NotNil(t, err)
	assert.Nil(t, con)
}

// GET SCHEMA NAME test not needed

//Close() Tests
//Refactor Close to return error OR need mock Log to test
// func TestCloseError(t *testing.T) {
// 	dbmock := SetupTests()
// 	mockVault := new(mocks.Vault)
// 	da := DataAccessor{dbmock, "TestFirmName", logger.Log, mockVault}
// 	mocket.Catcher.NewMock()
// 	//dbmock.Close().Error
// 	//mockDB.On("Close").Return(errors.New("Close failed"))
// 	err := da.Close()
// 	assert.NotNil(t, err)
// 	assert.Equal(t, da, DataAccessor{})
// }

func TestCloseSuccessful(t *testing.T) {
	dbmock := SetupTests()
	mockVault := new(mocks.Vault)
	da := DataAccessor{dbmock, "TestFirmName", logger.Log, mockVault}
	err := da.Close()
	assert.Nil(t, err)
	assert.Equal(t, da, DataAccessor{})
}

// If firm token was not found
func TestConnectFirmTokenNotFound(t *testing.T) {
	dbmock := SetupTests()
	mockVault := new(mocks.Vault)
	da := DataAccessor{dbmock, "TestFirmName", logger.Log, mockVault}
	mockVault.On("GetTenantCredentials", mock.Anything).Return(&models.AuroraConnection{}, errors.New("No db credentials found"))
	appconfig.RunEnv = "castle"
	err := da.Connect("firmtoken")
	assert.NotNil(t, err)
}

// If getting tenant credentials fail
func TestConnectGetTenantCredentialsFail(t *testing.T) {
	dbmock := SetupTests()
	mockVault := new(mocks.Vault)
	da := DataAccessor{dbmock, "TestFirmName", logger.Log, mockVault}

	mockVault.On("GetTenantCredentials", mock.Anything).Return(&models.AuroraConnection{}, errors.New("No db credentials found"))

	appconfig.RunEnv = "castle"
	err := da.Connect("firmtoken")
	assert.NotNil(t, err)
}

// If there was an error while opening the connection to database
func TestConnectTokenDBOpenFailure(t *testing.T) {

	// Save current function and restore at the end:
	oldGormOpen := gormOpen
	defer func() { gormOpen = oldGormOpen }()

	gormOpen = func(a string, b ...interface{}) (*gorm.DB, error) {
		return nil, errors.New("Cannot open DB")
	}

	dbmock := SetupTests()
	mockVault := new(mocks.Vault)
	da := DataAccessor{dbmock, "TestFirmName", logger.Log, mockVault}

	mockAuroraConnection := &models.AuroraConnection{Host: "testHost", Password: "testpass", Port: 8888, User: "testUser"}

	mockVault.On("GetTenantCredentials", mock.Anything).Return(mockAuroraConnection, nil)

	appconfig.RunEnv = "castle"
	err := da.Connect("firmtoken")

	assert.NotNil(t, err)
}

// If there is an error connecting to db and credentials cannot be retrieved from the vault
func TestConnectTokenDBOpenFailureCredentialsCouldNotBeRetrieved(t *testing.T) {

	// Save current function and restore at the end:
	oldGormOpen := gormOpen
	defer func() { gormOpen = oldGormOpen }()

	gormOpen = func(a string, b ...interface{}) (*gorm.DB, error) {
		return nil, errors.New("Cannot open DB")
	}

	dbmock := SetupTests()
	mockVault := new(mocks.Vault)
	da := DataAccessor{dbmock, "TestFirmName", logger.Log, mockVault}

	mockAuroraConnection := &models.AuroraConnection{Host: "testHost", Password: "testpass", Port: 8888, User: "testUser"}

	mockVault.On("GetTenantCredentials", mock.Anything).Return(mockAuroraConnection, nil)

	appconfig.RunEnv = "castle"
	err := da.Connect("firmtoken")

	assert.NotNil(t, err)
}

// If connect is successful
func TestConnectSuccess(t *testing.T) {
	// Save current function and restore at the end:
	oldGormOpen := gormOpen
	defer func() { gormOpen = oldGormOpen }()

	gormOpen = func(a string, b ...interface{}) (*gorm.DB, error) {
		return nil, nil
	}

	dbmock := SetupTests()
	mockVault := new(mocks.Vault)
	da := DataAccessor{dbmock, "TestFirmName", logger.Log, mockVault}
	appconfig.IsDevMode = false
	mockAuroraConnection := &models.AuroraConnection{Host: "testHost", Password: "testpass", Port: 8888, User: "testUser"}

	mockVault.On("GetTenantCredentials", mock.Anything).Return(mockAuroraConnection, nil)

	appconfig.RunEnv = "castle"
	err := da.Connect("firmtoken")

	assert.Nil(t, err)
}

// // RETRY
// // Need to test retry??

func TestInsertValidationErrosError(t *testing.T) {
	dbmock := SetupTests()
	mockVault := new(mocks.Vault)
	da := DataAccessor{dbmock, "TestFirmName", logger.Log, mockVault}
	mocket.Catcher.Reset().NewMock().WithQuery("INSERT INTO Ibor_ValidationErrors").WithError(errors.New("INsertion Failed"))
	errorstrings := []string{"US1"}
	err := da.InsertValidationErrors(1, errorstrings)
	assert.NotNil(t, err)
}

func TestInsertValidationErrors(t *testing.T) {
	dbmock := SetupTests()
	mockVault := new(mocks.Vault)
	mocket.Catcher.Reset()
	da := DataAccessor{dbmock, "TestFirmName", logger.Log, mockVault}
	errorstrings := []string{"US1"}
	err := da.InsertValidationErrors(1, errorstrings)
	assert.Nil(t, err)
}
