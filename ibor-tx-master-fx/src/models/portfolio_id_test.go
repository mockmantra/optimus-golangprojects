package models

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/httputil"
)

var portfolioIDTestJSON = `{
	"Allocations": [
	  {
		"Quantity":    200,
		"PortfolioId": 12
	  },
	  {
		"Quantity":    200,
		"PortfolioId": 12
	  }
	]
}`

func TestPortfolioID_WhenPortfolioAPIFails(t *testing.T) {
	var portfolioID PortfolioID = 7
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadGateway)
	}))

	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "PortfolioID"

	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(baseCurrencySecurityIDTestJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)

	err := portfolioID.Validate(testTransaction, key, 0)

	assert.EqualError(t, err, "Error in allocation portfolioID 7. PortfolioID : Service lookup failed")
}
func TestPortfolioID_WhenPortfolioAPIReturnsInvalidData(t *testing.T) {
	var portfolioID PortfolioID = 7
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, writeErr := w.Write([]byte("{\"test\":xer}"))
		assert.Nil(t, writeErr)
	}))
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "PortfolioID"
	transaction := Transaction{
		Allocations: []Allocation{},
	}

	err := portfolioID.Validate(transaction, key, 0)

	assert.EqualError(t, err, "Error in allocation portfolioID 7. PortfolioID : Service lookup failed")

}
func TestPortfolioID_WhenPortfolioAPIReturnsValidDataButInvalidSecurityId(t *testing.T) {
	var portfolioID PortfolioID = 7
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, writeErr := w.Write([]byte("{\"PortfolioId\":10}"))
		assert.Nil(t, writeErr)
	}))
	httputil.UserSessionToken = "123"
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "PortfolioID"
	transaction := Transaction{
		Allocations: []Allocation{},
	}

	err := portfolioID.Validate(transaction, key, 0)

	assert.Equal(t, err, fmt.Errorf("Invalid %s: Expected PortfolioID %d, Provided PortfolioID %d", key, 10, 7))
	httputil.UserSessionToken = ""

}
func TestPortfolioID_WhenPortfolioAPIReturnsValidData(t *testing.T) {
	var portfolioID PortfolioID = 7
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, writeErr := w.Write([]byte("{\"PortfolioId\":7}"))
		assert.Nil(t, writeErr)
	}))
	httputil.UserSessionToken = "123"
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "PortfolioID"
	transaction := Transaction{
		Allocations: []Allocation{},
	}

	err := portfolioID.Validate(transaction, key, 0)

	assert.Nil(t, err)
	httputil.UserSessionToken = ""
}
