package models

import (
	"encoding/json"
	"fmt"
	"time"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/httputil"
)

// BusinessDateTimeUTC field
type BusinessDateTimeUTC DateTime

var timeZoneURL = "referencedata/v1/GetTimeZoneById"

// TimeZoneResponse type
type TimeZoneResponse struct {
	BaseUtcOffset int `json:"BaseUtcOffset"`
	DstUtcOffset  int `json:"DstUtcOffset"`
}

// Validate validates the BusinessDateTimeUTC
func (value BusinessDateTimeUTC) Validate(transaction Transaction, fieldName string) error {
	timeZoneResponse, err := NewTimezoneRequest(int(transaction.TransactionTimeZoneID))
	if err == nil {
		businessDateTimeUTC, err := time.Parse(time.RFC3339Nano, string(value))
		tradeDate, err1 := time.Parse(time.RFC3339Nano, string(transaction.TradeDate))
		if err == nil && err1 == nil {
			tradeDateUTC := tradeDate.Add(-time.Minute * time.Duration(timeZoneResponse.BaseUtcOffset))
			businessYear, businessMonth, businessDay := businessDateTimeUTC.Date()
			tradeYear, tradeMonth, tradeDay := tradeDateUTC.Date()
			if businessYear == tradeYear && businessMonth == tradeMonth && businessDay == tradeDay {
				return nil
			}
			return fmt.Errorf("Invalid %s. Provided %s : %s, TradeDate: %s", fieldName, fieldName, value, transaction.TradeDate)
		}
		return fmt.Errorf("%s: %v, TradeDate: %v", fieldName, err, err1)
	}
	return fmt.Errorf("%s : Service lookup failed", fieldName)
}

// NewTimezoneRequest fetches the TimeZone details by timezoneid
func NewTimezoneRequest(timeZoneID int) (TimeZoneResponse, error) {
	bodyBytes, err := httputil.NewRequest("GET", fmt.Sprintf("%s?timeZoneId=%d", timeZoneURL, timeZoneID), nil)
	if err == nil {
		var timeZoneResponse []TimeZoneResponse
		unmarshalErr := json.Unmarshal(bodyBytes, &timeZoneResponse)
		if unmarshalErr != nil {
			return TimeZoneResponse{}, unmarshalErr
		}
		return timeZoneResponse[0], err
	}
	return TimeZoneResponse{}, err
}
