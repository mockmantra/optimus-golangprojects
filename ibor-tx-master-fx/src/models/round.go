package models

import (
	"math"
)

// Round is used for rounding
func Round(val float64, roundOn float64, places int) float64 {

	pow := math.Pow(10, float64(places))
	digit := pow * val
	_, div := math.Modf(digit)
	var round float64

	if (div >= roundOn && val > 0) || (div < roundOn && val <= 0) {
		round = math.Ceil(digit)
	} else {
		round = math.Floor(digit)
	}

	return round / pow
}
