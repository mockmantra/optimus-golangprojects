package models

// AllocationValidator interface
type AllocationValidator interface {
	Validate(transaction Transaction, fieldName string, allocationIndex int) error
}

// Allocation allocation
//TODO: All the fields with *int64 as type need a new custom type which has a Validator method associated with it.
type Allocation struct {
	BookTypeID                     *BookTypeID                     `json:"BookTypeId"`
	IsFinalized                    bool                            `json:"IsFinalized,omitempty"`
	ExecutingBrokerCounterpartyID  *ExecutingBrokerCounterpartyID  `json:"ExecutingBrokerCounterpartyId,omitempty"`
	PortfolioID                    *PortfolioID                    `json:"PortfolioId"`
	BaseCurrencySecurityID         *BaseCurrencySecurityID         `json:"BaseCurrencySecurityId"`
	CustodianCounterpartyAccountID *CustodianCounterpartyAccountID `json:"CustodianCounterpartyAccountId,omitempty"`
	PositionTags                   *PositionTagsSlice              `json:"PositionTags"`
	FwdPoints                      *float64                        `json:"FwdPoints"`
	DealtSettleAmount              *float64                        `json:"DealtSettleAmount"`
	CounterSettleAmount            *CounterSettleAmount            `json:"CounterSettleAmount"`
	DealRate                       *float64                        `json:"DealRate"`
	DealtToBaseFXRate              *DealtToBaseFXRate              `json:"DealtToBaseFXRate"`
	DealtToSystemFXRate            *DealtToSystemFXRate            `json:"DealtToSystemFXRate"`
	CounterToBaseFXRate            *CounterToBaseFXRate            `json:"CounterToBaseFXRate"`
	CounterToSystemFXRate          *CounterToSystemFXRate          `json:"CounterToSystemFXRate"`
}
