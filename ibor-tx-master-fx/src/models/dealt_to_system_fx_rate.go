package models

import (
	"fmt"
)

//DealtToSystemFXRate field
type DealtToSystemFXRate float64

// Validate validates the fields
func (value DealtToSystemFXRate) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	if transaction.DealtCurrencySecurityID == transaction.SystemCurrencySecurityID {
		if value != 1 {
			return fmt.Errorf("Error in allocation PortfolioID %d. Invalid %s. The SystemCurrencySecurityID and DealtCurrencySecurityID are equal but the value of DealtToSystemFXRate is %g instead of 1", *transaction.Allocations[allocationIndex].PortfolioID, fieldName, float64(value))
		}
	}
	return nil
}
