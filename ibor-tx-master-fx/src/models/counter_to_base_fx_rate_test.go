package models

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

//When CounterCurrency is equal to BaseCurrency; counterToBaseFXRate = 1
func TestCounteroBaseFXRate_CounterCurrencySecurityIdEqualsBaseCurrencySecurityID(t *testing.T) {
	counterToBaseFXRate := CounterToBaseFXRate(1)
	allocations := make([]Allocation, 0)
	baseCurrency := BaseCurrencySecurityID(155)
	allocation := Allocation{
		BaseCurrencySecurityID: &baseCurrency,
	}
	allocations = append(allocations, allocation)
	transaction := Transaction{
		Allocations:               allocations,
		CounterCurrencySecurityID: 155,
	}
	err := counterToBaseFXRate.Validate(transaction, "CounterToBaseFXRate", 0)

	assert.Nil(t, err)
}

//When CounterCurrency is equal to BaseCurrency; counteroBaseFXRate != 1
func TestCounteroBaseFXRate_IsNotEqualTo1CounterCurrencySecurityIdEqualsBaseCurrencySecurityID(t *testing.T) {
	counterToBaseFXRate := CounterToBaseFXRate(2)
	allocations := make([]Allocation, 0)
	baseCurrency := BaseCurrencySecurityID(15)
	portfolioID := PortfolioID(12)
	allocation := Allocation{
		BaseCurrencySecurityID: &baseCurrency,
		PortfolioID:            &portfolioID,
	}
	allocations = append(allocations, allocation)
	transaction := Transaction{
		Allocations:               allocations,
		CounterCurrencySecurityID: 15,
	}
	err := counterToBaseFXRate.Validate(transaction, "CounterToBaseFXRate", 0)

	assert.EqualError(t, err, "Error in allocation PortfolioID 12. Invalid CounterToBaseFXRate. The CounterCurrencySecurityID and BaseCurrencySecurityID are equal but the value of CounterToBaseFXRate is 2 instead of 1")
}
