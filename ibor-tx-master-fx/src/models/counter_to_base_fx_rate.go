package models

import (
	"fmt"
)

//CounterToBaseFXRate field
type CounterToBaseFXRate float64

// Validate validates the fields
func (value CounterToBaseFXRate) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	if int64(transaction.CounterCurrencySecurityID) == int64(*transaction.Allocations[allocationIndex].BaseCurrencySecurityID) {
		if value != 1 {
			return fmt.Errorf("Error in allocation PortfolioID %d. Invalid %s. The CounterCurrencySecurityID and BaseCurrencySecurityID are equal but the value of CounterToBaseFXRate is %g instead of 1", *transaction.Allocations[allocationIndex].PortfolioID, fieldName, float64(value))
		}
	}
	return nil
}
