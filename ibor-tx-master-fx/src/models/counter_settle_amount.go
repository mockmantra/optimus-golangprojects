package models

import (
	"fmt"
	"reflect"
)

// CounterSettleAmount filed
type CounterSettleAmount float64

// Validate validates the fields
func (value CounterSettleAmount) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	if reflect.ValueOf(transaction.Allocations[allocationIndex].DealtSettleAmount).IsNil() {
		return fmt.Errorf("Error in allocation PortfolioID %d. Invalid %s : DealtSettleAmount value is missing", *transaction.Allocations[allocationIndex].PortfolioID, fieldName)
	}
	if reflect.ValueOf(transaction.Allocations[allocationIndex].DealRate).IsNil() {
		return fmt.Errorf("Error in allocation PortfolioID %d. Invalid %s : DealRate value is missing", *transaction.Allocations[allocationIndex].PortfolioID, fieldName)
	}

	product := (float64(*transaction.Allocations[allocationIndex].DealtSettleAmount) * float64(*transaction.Allocations[allocationIndex].DealRate))
	roundedProduct := Round(float64(product), 0.5, 4)
	roundedCounterSettleAmount := Round(float64(value), 0.5, 4)
	if roundedProduct == roundedCounterSettleAmount {
		return nil
	}
	return fmt.Errorf("Error in allocation PortfolioID %d. Invalid %s. Expected value :%g, Provided value : %g", *transaction.Allocations[allocationIndex].PortfolioID, fieldName, roundedProduct, roundedCounterSettleAmount)

}
