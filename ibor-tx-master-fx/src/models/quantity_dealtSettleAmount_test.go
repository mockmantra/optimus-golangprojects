package models

import (
	"encoding/json"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestOrderQuantity_OrderQuantityForPortfolidByBookIdAreNotEqual(t *testing.T) {

	var quantity DealtSettleAmount = 400
	var quantityTestJSON = `{
		"Allocations": [
		  {
			"BookTypeId": 1,
			"PortfolioId": 1,
			"DealtSettleAmount":   200
		  },
		  {
			"BookTypeId": 1,
			"PortfolioId": 2,
			"DealtSettleAmount":   200
		  },
		  {
			"BookTypeId": 2,
			"PortfolioId": 1,
			"DealtSettleAmount":   50
		  },
		  {
			"BookTypeId": 2,
			"PortfolioId": 1,
			"DealtSettleAmount":   50
		  },
		  {
			"BookTypeId": 2,
			"PortfolioId": 2,
			"DealtSettleAmount":   120
		  },
		  {
			"BookTypeId": 2,
			"PortfolioId": 2,
			"DealtSettleAmount":   180
		  }
		],
		"DealtSettleAmount": 400
	}`
	var testTransaction Transaction
	unMarshalErr := json.Unmarshal([]byte(quantityTestJSON), &testTransaction)
	assert.Nil(t, unMarshalErr)
	err := quantity.Validate(testTransaction, "DealtSettleAmount")
	errStr := err.Error()
	errsActual := strings.Split(errStr, "||")
	errsExpected1 := []string{"Invalid DealtSettleAmount. The sum of DealtSettleAmount by Portfolio Id: 1 in Book Type 1: 200 does not match with Book Type 2 : 100",
		"Invalid DealtSettleAmount. The sum of DealtSettleAmount by Portfolio Id: 2 in Book Type 1: 200 does not match with Book Type 2 : 300"}
	errsExpected2 := []string{"Invalid DealtSettleAmount. The sum of DealtSettleAmount by Portfolio Id: 1 in Book Type 2: 100 does not match with Book Type 1 : 200",
		"Invalid DealtSettleAmount. The sum of DealtSettleAmount by Portfolio Id: 2 in Book Type 2: 300 does not match with Book Type 1 : 200"}
	if !sameStringSlice(errsActual, errsExpected1) && !sameStringSlice(errsActual, errsExpected2) {
		t.Error("Expected Errors are not Equal to Actual List")
	}
}
func TestOrderQuantity_OrderQuantityEqualToSumOfQuantityOfIndividualBooksAndPortfoliosMatched(t *testing.T) {

	var quantity DealtSettleAmount = 400
	var quantityTestJSON = `{
		"Allocations": [
		  {
			"BookTypeId": 1,
			"PortfolioId": 1,
			"DealtSettleAmount":   200
		  },
		  {
			"BookTypeId": 1,
			"PortfolioId": 2,
			"DealtSettleAmount":   200
		  },
		  {
			"BookTypeId": 2,
			"PortfolioId": 1,
			"DealtSettleAmount":   100
		  },
		  {
			"BookTypeId": 2,
			"PortfolioId": 1,
			"DealtSettleAmount":   100
		  },
		  {
			"BookTypeId": 2,
			"PortfolioId": 2,
			"DealtSettleAmount":   120
		  },
		  {
			"BookTypeId": 2,
			"PortfolioId": 2,
			"DealtSettleAmount":   80
		  }
		],
		"DealtSettleAmount": 400
	}`
	var testTransaction Transaction
	unMarshalErr := json.Unmarshal([]byte(quantityTestJSON), &testTransaction)
	assert.Nil(t, unMarshalErr)
	err := quantity.Validate(testTransaction, "DealtSettleAmount")
	assert.Nil(t, err)
}

func sameStringSlice(x, y []string) bool {
	if len(x) != len(y) {
		return false
	}
	// create a map of string -> int
	diff := make(map[string]int, len(x))
	for _, _x := range x {
		// 0 value for int is 0, so just increment a counter for the string
		diff[_x]++
	}
	for _, _y := range y {
		// If the string _y is not in diff bail out early
		if _, ok := diff[_y]; !ok {
			return false
		}
		diff[_y]--
		if diff[_y] == 0 {
			delete(diff, _y)
		}
	}
	if len(diff) == 0 {
		return true
	}
	return false
}
