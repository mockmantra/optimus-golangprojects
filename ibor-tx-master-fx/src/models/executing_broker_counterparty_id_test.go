package models

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/httputil"
)

var executingBrokerTestJSON = `{
	"Allocations": [
	  {
		"PortfolioId" :2
	  },
	  {
		"PortfolioId": 12
	  }
	]
  }`

var errExecutingBroker = fmt.Errorf("Error in allocation PortfolioID 2. Invalid ExecutingBrokerCounterpartyId : 30 does not exist")
var errExecutingBroker1 = fmt.Errorf("Error in allocation PortfolioID 2. Invalid ExecutingBrokerCounterpartyId : 12 does not exist")

var testCasesExecutingBroker = []struct {
	in  ExecutingBrokerCounterpartyID
	out error
}{
	{32, nil},
	{3, nil},
	{30, errExecutingBroker},
	{12, errExecutingBroker1},
}

func TestExecutingBrokerCounterpartyIdWithoutUnmarshal(t *testing.T) {

	var brokerJSON = `{
		"Allocations": [
		  {
			"PortfolioId":12,
			"BookTypeId": 1,
			"Quantity":   200
		  },
		  {
			"PortfolioId":13,
			"BookTypeId": 1
		  }
		]
	}`
	var executingBrokerID ExecutingBrokerCounterpartyID = 12
	var testTransaction Transaction
	unMarshalError := json.Unmarshal([]byte(brokerJSON), &testTransaction)
	assert.Nil(t, unMarshalError)
	str := ""
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()
	err := executingBrokerID.Validate(testTransaction, "ExecutingBrokerCounterpartyID", 1)

	assert.Equal(t, err, fmt.Errorf("Error in portfolioID 13. ExecutingBrokerCounterpartyID : Unmarshlling error"))

	httputil.UserSessionToken = ""
}

func TestExecutingBrokerCounterpartyIdWithUnmarshal(t *testing.T) {

	var brokerJSON = `{
		"Allocations": [
		  {
			"PortfolioId":12,
			"BookTypeId": 1,
			"Quantity":   200
		  },
		  {
			"PortfolioId":13,
			"BookTypeId": 1
		  }
		]
	}`
	var executingBrokerID ExecutingBrokerCounterpartyID = 32
	var testTransaction Transaction
	unMarshalError := json.Unmarshal([]byte(brokerJSON), &testTransaction)
	assert.Nil(t, unMarshalError)
	str := "[{\"CounterpartyId\":32},{\"CounterpartyId\":3}]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	err := executingBrokerID.Validate(testTransaction, "ExecutingBrokerCounterpartyID", 1)

	assert.Nil(t, err)

	httputil.UserSessionToken = ""
}

func TestExecutingBrokerCounterpartyIdLookup(t *testing.T) {

	var brokerJSON = `{
		"Allocations": [
		  {
				"PortfolioId":12,
			"BookTypeId": 1,
			"Quantity":   200
		  },
		  {
				"PortfolioId":13,
			"BookTypeId": 1
		  }
		]
	}`
	var executingBrokerID ExecutingBrokerCounterpartyID = 12
	var testTransaction Transaction
	unMarshalError := json.Unmarshal([]byte(brokerJSON), &testTransaction)
	assert.Nil(t, unMarshalError)
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadGateway)
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()
	err := executingBrokerID.Validate(testTransaction, "ExecutingBrokerCounterpartyID", 1)

	assert.Equal(t, err, fmt.Errorf("Error in portfolioID %d. %s : Service lookup failed", int(*testTransaction.Allocations[1].PortfolioID), "ExecutingBrokerCounterpartyID"))

	httputil.UserSessionToken = ""
}
