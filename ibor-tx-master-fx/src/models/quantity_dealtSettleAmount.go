package models

import (
	"fmt"
	"strings"
)

// DealtSettleAmount int type
type DealtSettleAmount float64

type dealtSettleAmountByBook struct {
	dealtSettleAmount float64
	portfolio         map[int]float64
}

// Validate validates Orderquantity field
func (value DealtSettleAmount) Validate(transaction Transaction, fieldName string) error {
	allocations := make(map[int]dealtSettleAmountByBook)
	for _, allocation := range transaction.Allocations {
		bookTypeID := int(*allocation.BookTypeID)
		dealtSettleAmount := float64(*allocation.DealtSettleAmount)
		portfolioID := int(*allocation.PortfolioID)
		if val, ok := allocations[bookTypeID]; ok {
			if portfolio, ok := val.portfolio[portfolioID]; ok {
				val.portfolio[portfolioID] = float64(portfolio) + dealtSettleAmount
			} else {
				val.portfolio[portfolioID] = float64(dealtSettleAmount)
			}
			allocations[bookTypeID] = dealtSettleAmountByBook{
				dealtSettleAmount: val.dealtSettleAmount + dealtSettleAmount,
				portfolio:         val.portfolio,
			}
		} else {
			portfolioMap := make(map[int]float64)
			portfolioMap[portfolioID] = float64(dealtSettleAmount)
			allocations[bookTypeID] = dealtSettleAmountByBook{
				dealtSettleAmount: dealtSettleAmount,
				portfolio:         portfolioMap,
			}
		}
	}

	var errorStrings []string

	for id, allocation := range allocations {
		// if allocation.dealtSettleAmount != float64(transaction.OrderQuantity) {
		// 	errorStrings = append(errorStrings, fmt.Sprintf("Invalid %s. The sum of Quantities for Book Type %d: %g does not match Orderquantity: %g", fieldName, id, allocation.dealtSettleAmount, value))
		// }
		for innerID, innerAllocation := range allocations {
			if innerID != id {
				for portfolioID, secondDealtSettleAmount := range innerAllocation.portfolio {
					firstDealtSettleAmount := allocation.portfolio[portfolioID]
					if firstDealtSettleAmount == 0 || secondDealtSettleAmount != firstDealtSettleAmount {
						errorStrings = append(errorStrings, fmt.Sprintf("Invalid DealtSettleAmount. The sum of DealtSettleAmount by Portfolio Id: %d in Book Type %d: %g does not match with Book Type %d : %g", portfolioID, id, firstDealtSettleAmount, innerID, secondDealtSettleAmount))
					}
				}
			}
		}
		delete(allocations, id)
	}
	if len(errorStrings) > 0 {
		return fmt.Errorf(strings.Join(errorStrings, "||"))
	}
	return nil
}
