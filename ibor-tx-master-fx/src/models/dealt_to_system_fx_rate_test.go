package models

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

//When DealtCurrency is equal to SystemCurrency; DealToSystemFXRate = 1
func TestDealToSystemFXRate_DealtCurrencySecurityIdEqualsSystemCurrencySecurityID(t *testing.T) {
	dealtToSystemFXRate := DealtToSystemFXRate(1)
	transaction := Transaction{
		SystemCurrencySecurityID: 12,
		DealtCurrencySecurityID:  12,
	}

	err := dealtToSystemFXRate.Validate(transaction, "DealtToSystemFXRate", 0)

	assert.Nil(t, err)
}

//When DealtCurrency is equal to SystemCurrency; DealToSystemFXRate != 1
func TestDealToSystemFXRate_IsNotEqualTo1DealtCurrencySecurityIdEqualsSystemCurrencySecurityID(t *testing.T) {
	dealtToSystemFXRate := DealtToSystemFXRate(2)
	allocations := make([]Allocation, 0)

	portfolioID := PortfolioID(12)
	allocation := Allocation{
		PortfolioID: &portfolioID,
	}
	allocations = append(allocations, allocation)
	transaction := Transaction{
		Allocations:              allocations,
		SystemCurrencySecurityID: 12,
		DealtCurrencySecurityID:  12,
	}
	err := dealtToSystemFXRate.Validate(transaction, "DealtToSystemFXRate", 0)

	assert.EqualError(t, err, "Error in allocation PortfolioID 12. Invalid DealtToSystemFXRate. The SystemCurrencySecurityID and DealtCurrencySecurityID are equal but the value of DealtToSystemFXRate is 2 instead of 1")
}
