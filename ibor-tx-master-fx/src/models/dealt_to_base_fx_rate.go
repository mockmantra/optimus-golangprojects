package models

import (
	"fmt"
)

//DealtToBaseFXRate field
type DealtToBaseFXRate float64

// Validate validates the fields
func (value DealtToBaseFXRate) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	if int64(*transaction.Allocations[allocationIndex].BaseCurrencySecurityID) == int64(transaction.DealtCurrencySecurityID) {
		if value != 1 {
			return fmt.Errorf("Error in allocation PortfolioID %d. Invalid %s. The BaseCurrencySecurityID and DealtCurrencySecurityID are equal but the value of DealtToBaseFXRate is %g instead of 1", *transaction.Allocations[allocationIndex].PortfolioID, fieldName, float64(value))
		}
	}
	return nil
}
