package models

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/httputil"
)

func TestBusinessDateTimeUTC_WithValidUserTimeZoneAndIsDifferentToTradeDate(t *testing.T) {

	var businessDateTimeUTC BusinessDateTimeUTC = "2018-09-08T13:25:43.1234567Z"
	str := "[{\"BaseUtcOffset\":-300}]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"

	defer ts.Close()
	transaction := Transaction{
		//UserTimeZoneID: 1,
		TradeDate: "2018-09-08T21:25:43Z",
	}

	fieldName := "BusinessDateTimeUTC"

	err := businessDateTimeUTC.Validate(transaction, fieldName)

	assert.Equal(t, err, fmt.Errorf("Invalid %s. Provided %s : %v, TradeDate: %s", "BusinessDateTimeUTC", "BusinessDateTimeUTC", businessDateTimeUTC, transaction.TradeDate))

	httputil.UserSessionToken = ""
}
func TestBusinessDateTimeUTC_WithValidUserTimeZoneAndIsDateIsSameasTradeDate(t *testing.T) {

	var businessDateTimeUTC BusinessDateTimeUTC = "2018-09-08T13:25:43.1234567Z"
	str := "[{\"BaseUtcOffset\":-300}]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"

	defer ts.Close()
	transaction := Transaction{
		//UserTimeZoneID: 1,
		TradeDate: "2018-09-08T05:25:43Z",
	}

	err := businessDateTimeUTC.Validate(transaction, "BusinessDateTimeUTC")

	assert.Nil(t, err)

	httputil.UserSessionToken = ""

}
func TestBusinessDateTimeUTC_WithValidUserTimeZoneAndBusinessDateTimeUTCIsInvalidFormat(t *testing.T) {

	var businessDateTimeUTC BusinessDateTimeUTC = "2018-09-08 13:25:43.1234567"
	str := "[{\"BaseUtcOffset\":-300}]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL
	defer ts.Close()

	httputil.UserSessionToken = "123"

	transaction := Transaction{
		//UserTimeZoneID: 1,
		TradeDate: "2018-09-08T05:25:43Z",
	}

	fieldName := "BusinessDateTimeUTC"
	err := businessDateTimeUTC.Validate(transaction, fieldName)
	if err == nil || strings.Index(err.Error(), "cannot parse") == -1 {
		t.Error("Expected the parse error to be returned but nothing returned")
	}

	httputil.UserSessionToken = ""
}
func TestBusinessDateTimeUTC_WithValidUserTimeZoneAndTradeDateIsInvalidFormat(t *testing.T) {
	var businessDateTimeUTC BusinessDateTimeUTC = "2018-09-08T13:25:43.1234567Z"
	str := "[{\"BaseUtcOffset\":-300}]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"

	defer ts.Close()
	transaction := Transaction{
		//UserTimeZoneID: 1,
		TradeDate: "2018-09-08 05:25:43",
	}
	fieldName := "BusinessDateTimeUTC"
	err := businessDateTimeUTC.Validate(transaction, fieldName)
	if err == nil || strings.Index(err.Error(), "cannot parse") == -1 {
		t.Error("Expected the parse error to be returned but nothing returned")
	}

	httputil.UserSessionToken = ""

}
func TestBusinessDateTimeUTC_WithValidUserTimeZoneResponse(t *testing.T) {
	var businessDateTimeUTC BusinessDateTimeUTC = "2018-09-08T13:25:43.1234567Z"
	str := "[{\"BaseUtcOffset\":test}]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"

	defer ts.Close()
	transaction := Transaction{
		//UserTimeZoneID: 1,
		TradeDate: "2018-09-08 05:25:43",
	}
	fieldName := "BusinessDateTimeUTC"
	err := businessDateTimeUTC.Validate(transaction, fieldName)
	assert.Equal(t, err, fmt.Errorf("%s : Service lookup failed", fieldName))

	httputil.UserSessionToken = ""

}
func TestBusinessDateTimeUTC_WithInValidUserTimeZone(t *testing.T) {
	var businessDateTimeUTC BusinessDateTimeUTC = "2018-09-08T13:25:43.1234567Z"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"

	defer ts.Close()
	transaction := Transaction{
		//UserTimeZoneID: 1,
		TradeDate: "2018-09-08T05:25:43Z",
	}

	err := businessDateTimeUTC.Validate(transaction, "BusinessDateTimeUTC")

	assert.Equal(t, err, fmt.Errorf("%s : Service lookup failed", "BusinessDateTimeUTC"))

	httputil.UserSessionToken = ""
}
