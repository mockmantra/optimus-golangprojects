package models

import (
	"encoding/json"
	"fmt"
	"strings"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/httputil"
)

const portfolioURL string = "referencedata/v1/Portfolio/%d"

// PortfolioResponse type
type PortfolioResponse struct {
	ID             int `json:"Id"`
	PortfolioID    int `json:"PortfolioId"`
	BaseCurrencyID int `json:"BaseCurrencyId"`
}

// PortfolioID type
type PortfolioID int

// Validate validates the PortfolioID
func (value PortfolioID) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	portfolio, err := NewPortfolioRequest(int(value))
	if err != nil {
		if strings.Contains(err.Error(), "error:404") {
			return fmt.Errorf("Invalid %s: %d does not exist", fieldName, value)
		}
		return fmt.Errorf("Error in allocation portfolioID %d. %s : Service lookup failed", value, fieldName)
	} else if portfolio.PortfolioID != int(value) {
		return fmt.Errorf("Invalid %s: Expected PortfolioID %d, Provided PortfolioID %d", fieldName, portfolio.PortfolioID, int(value))
	}
	return nil
}

// NewPortfolioRequest fetches the Portfolio by PortfolioID
func NewPortfolioRequest(portfolioID int) (PortfolioResponse, error) {
	bodyBytes, err := httputil.NewRequest("GET", fmt.Sprintf(portfolioURL, portfolioID), nil)
	var portfolioResponse PortfolioResponse
	if err == nil {
		unmarshalError := json.Unmarshal(bodyBytes, &portfolioResponse)
		if unmarshalError != nil {
			return portfolioResponse, unmarshalError
		}
	}
	return portfolioResponse, err
}
