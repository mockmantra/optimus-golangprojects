package models

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

//When CounterCurrency is equal to SystemCurrency; DealToSystemFXRate = 1
func TestDealToSystemFXRate_CounterCurrencySecurityIdEqualsSystemCurrencySecurityID(t *testing.T) {
	counterToSystemFXRate := CounterToSystemFXRate(1)
	transaction := Transaction{
		SystemCurrencySecurityID:  12,
		CounterCurrencySecurityID: 12,
	}

	err := counterToSystemFXRate.Validate(transaction, "CounterToSystemFXRate", 0)

	assert.Nil(t, err)
}

//When CounterCurrency is equal to SystemCurrency; DealToSystemFXRate != 1
func TestDealToSystemFXRate_IsNotEqualTo1CounterCurrencySecurityIdEqualsSystemCurrencySecurityID(t *testing.T) {
	counterToSystemFXRate := CounterToSystemFXRate(2)
	allocations := make([]Allocation, 0)

	portfolioID := PortfolioID(12)
	allocation := Allocation{
		PortfolioID: &portfolioID,
	}
	allocations = append(allocations, allocation)
	transaction := Transaction{
		Allocations:               allocations,
		SystemCurrencySecurityID:  12,
		CounterCurrencySecurityID: 12,
	}
	err := counterToSystemFXRate.Validate(transaction, "CounterToSystemFXRate", 0)

	assert.EqualError(t, err, "Error in allocation PortfolioID 12. Invalid CounterToSystemFXRate. The CounterCurrencySecurityID and SystemCurrencySecurityID are equal but the value of CounterToSystemFXRate is 2 instead of 1")
}
