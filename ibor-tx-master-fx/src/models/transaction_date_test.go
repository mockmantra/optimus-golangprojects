package models

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTranactionDate_InvalidDateFormat(t *testing.T) {
	var tranactionDate TranactionDate
	transaction := Transaction{
		TradeDate:  "2018-09-08 13:25:43Z",
		SettleDate: "2018-09-08 13:25:43Z",
	}
	key := "TradeDate"
	err := tranactionDate.Validate(transaction, key)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. Invalid format", key))
}
func TestTranactionDate_ValidDateFormatAndNotBeforeOrEqualSettleDate(t *testing.T) {
	var tranactionDate TranactionDate
	transaction := Transaction{
		SettleDate: "2018-09-07T13:25:43Z",
		TradeDate:  "2018-09-08T13:25:43Z",
	}
	key := "TradeDate"
	err := tranactionDate.Validate(transaction, key)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. TradeDate must be before SettleDate. Provided TradeDate: %s, SettleDate: %s", key, transaction.TradeDate, transaction.SettleDate))
}
func TestTranactionDate_ValidDateFormatAndBeforeSettleDate(t *testing.T) {
	var tranactionDate TranactionDate
	transaction := Transaction{
		SettleDate: "2018-09-07T13:25:43Z",
		TradeDate:  "2018-09-06T13:25:43Z",
	}
	key := "TradeDate"
	err := tranactionDate.Validate(transaction, key)

	assert.Nil(t, err)
}

func TestTranactionDate_ValidDateFormatAndNotBeforeOrEqualSettleDateHour(t *testing.T) {
	var tranactionDate TranactionDate
	transaction := Transaction{
		SettleDate: "2018-09-07T12:25:43Z",
		TradeDate:  "2018-09-07T13:25:43Z",
	}
	key := "TradeDate"
	err := tranactionDate.Validate(transaction, key)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. TradeDate must be before SettleDate. Provided TradeDate: %s, SettleDate: %s", key, transaction.TradeDate, transaction.SettleDate))
}
func TestTranactionDate_ValidDateFormatAndBeforeSettleDateHour(t *testing.T) {
	var tranactionDate TranactionDate
	transaction := Transaction{
		SettleDate: "2018-09-07T14:25:43Z",
		TradeDate:  "2018-09-07T13:25:43Z",
	}
	key := "TradeDate"
	err := tranactionDate.Validate(transaction, key)

	assert.Nil(t, err)
}

func TestTranactionDate_ValidDateFormatAndNotBeforeOrEqualSettleDateMinute(t *testing.T) {
	var tranactionDate TranactionDate
	transaction := Transaction{
		SettleDate: "2018-09-07T13:24:43Z",
		TradeDate:  "2018-09-07T13:25:43Z",
	}
	key := "TradeDate"
	err := tranactionDate.Validate(transaction, key)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. TradeDate must be before SettleDate. Provided TradeDate: %s, SettleDate: %s", key, transaction.TradeDate, transaction.SettleDate))
}
func TestTranactionDate_ValidDateFormatAndBeforeSettleDateMinute(t *testing.T) {
	var tranactionDate TranactionDate
	transaction := Transaction{
		SettleDate: "2018-09-07T13:26:43Z",
		TradeDate:  "2018-09-07T13:25:43Z",
	}
	key := "TradeDate"
	err := tranactionDate.Validate(transaction, key)

	assert.Nil(t, err)
}

func TestTranactionDate_ValidDateFormatAndNotBeforeOrEqualSettleDateSecond(t *testing.T) {
	var tranactionDate TranactionDate
	transaction := Transaction{
		SettleDate: "2018-09-07T13:25:42Z",
		TradeDate:  "2018-09-07T13:25:43Z",
	}
	key := "TradeDate"
	err := tranactionDate.Validate(transaction, key)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. TradeDate must be before SettleDate. Provided TradeDate: %s, SettleDate: %s", key, transaction.TradeDate, transaction.SettleDate))
}
func TestTranactionDate_ValidDateFormatAndBeforeSettleDateSecond(t *testing.T) {
	var tranactionDate TranactionDate
	transaction := Transaction{
		SettleDate: "2018-09-07T13:25:44Z",
		TradeDate:  "2018-09-07T13:25:43Z",
	}
	key := "TradeDate"
	err := tranactionDate.Validate(transaction, key)

	assert.Nil(t, err)
}

func TestTranactionDate_ValidDateFormatAndNotBeforeOrEqualSettleDateNanosecond(t *testing.T) {
	var tranactionDate TranactionDate
	transaction := Transaction{
		SettleDate: "2018-09-07T13:25:43.0000001Z",
		TradeDate:  "2018-09-07T13:25:43.0000002Z",
	}
	key := "TradeDate"
	err := tranactionDate.Validate(transaction, key)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. TradeDate must be before SettleDate. Provided TradeDate: %s, SettleDate: %s", key, transaction.TradeDate, transaction.SettleDate))
}
func TestTranactionDate_ValidDateFormatAndBeforeSettleDateNanosecond(t *testing.T) {
	var tranactionDate TranactionDate
	transaction := Transaction{
		SettleDate: "2018-09-07T13:25:43.0000003Z",
		TradeDate:  "2018-09-07T13:25:43.0000002Z",
	}
	key := "TradeDate"
	err := tranactionDate.Validate(transaction, key)

	assert.Nil(t, err)
}
