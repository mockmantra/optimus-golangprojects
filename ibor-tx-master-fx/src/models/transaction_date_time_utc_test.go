package models

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestTransactionDateTimeUTC_InvalidDateFormat(t *testing.T) {
	var transactionDateTimeUTC TransactionDateTimeUTC = "2018-09-08 13:25:43Z"
	transaction := Transaction{}
	key := "TransactionDateTimeUTC"
	err := transactionDateTimeUTC.Validate(transaction, key)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. Incorrect format", key))
}
func TestTransactionDateTimeUTC_ValidDateFormatAndNotEqualsToCurrentDate(t *testing.T) {
	var transactionDateTimeUTC TransactionDateTimeUTC = "2018-09-08T13:25:43.1234567Z"
	transaction := Transaction{}
	key := "TransactionDateTimeUTC"
	err := transactionDateTimeUTC.Validate(transaction, key)
	currentDateUTC := time.Now().UTC().Format(time.RFC3339)[:10]
	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. TransactionDate must match current date. Current Date: %s, Provided TransactionDate: %s", key, currentDateUTC, transactionDateTimeUTC[:10]))
}
func TestTransactionDateTimeUTC_ValidDateFormatAndEqualsToCurrentDate(t *testing.T) {
	currentDate := time.Now().UTC().Format(time.RFC3339Nano)
	transactionDateTimeUTC := TransactionDateTimeUTC(currentDate)
	transaction := Transaction{}
	key := "TransactionDateTimeUTC"
	err := transactionDateTimeUTC.Validate(transaction, key)

	assert.Nil(t, err)
}
