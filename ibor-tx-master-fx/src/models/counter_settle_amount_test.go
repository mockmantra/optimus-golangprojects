package models

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

var counterSettleAmountTestJSON = `{
	"Allocations": [
	  {
		"PortfolioId":12,
		"CounterSettleAmount" : 12000,
		"DealRate": 1.2
	  }
	],
	"SystemCurrencySecurityId": 13
}`

//Test if DealtSettleAmount is not present (nil).
func TestCounterSettleAmountInvalidIfDealtSettleAmountIsNil(t *testing.T) {
	var counterSettleAmount CounterSettleAmount = 100
	var transaction Transaction
	unmarshalErr := json.Unmarshal([]byte(counterSettleAmountTestJSON), &transaction)
	assert.Nil(t, unmarshalErr)

	err := counterSettleAmount.Validate(transaction, "CounterSettleAmount", 0)

	assert.Equal(t, err, fmt.Errorf("Error in allocation PortfolioID %d. Invalid %s : DealtSettleAmount value is missing", *transaction.Allocations[0].PortfolioID, "CounterSettleAmount"))
}

//Test if DealRate is not present (nil).
func TestCounterSettleAmountInvalidIfDealRateIsNil(t *testing.T) {
	var counterSettleAmountTestJSON = `{
		"Allocations": [
		  {
			"PortfolioId":12,
			"CounterSettleAmount" : 12000,
			"DealtSettleAmount": 12000
		  }
		],
		"SystemCurrencySecurityId": 13
	}`
	var counterSettleAmount CounterSettleAmount = 100
	var transaction Transaction
	unmarshalErr := json.Unmarshal([]byte(counterSettleAmountTestJSON), &transaction)
	assert.Nil(t, unmarshalErr)

	err := counterSettleAmount.Validate(transaction, "CounterSettleAmount", 0)

	assert.Equal(t, err, fmt.Errorf("Error in allocation PortfolioID %d. Invalid %s : DealRate value is missing", *transaction.Allocations[0].PortfolioID, "CounterSettleAmount"))
}

//Test for wrong CounterSettleAmount, when DealtSettleAmount and DealRate are present.
func TestCounterSettleAmountInvalid(t *testing.T) {
	var counterSettleAmountTestJSON = `{
		"Allocations": [
		  {
			"PortfolioId":12,
			"CounterSettleAmount" : 12000,
			"DealtSettleAmount": 12000,
			"DealRate": 1.5
		  }
		],
		"SystemCurrencySecurityId": 13
	}`
	var counterSettleAmount CounterSettleAmount = 12000
	var transaction Transaction
	unmarshalErr := json.Unmarshal([]byte(counterSettleAmountTestJSON), &transaction)
	assert.Nil(t, unmarshalErr)
	var prod = float64(*transaction.Allocations[0].DealtSettleAmount) * float64(*transaction.Allocations[0].DealRate)
	err := counterSettleAmount.Validate(transaction, "CounterSettleAmount", 0)

	assert.Equal(t, err, fmt.Errorf("Error in allocation PortfolioID %d. Invalid %s. Expected value :%g, Provided value : %g", *transaction.Allocations[0].PortfolioID, "CounterSettleAmount", float64(prod), float64(*transaction.Allocations[0].CounterSettleAmount)))
}

//Test for correct CounterSettleAmount, when DealtSettleAmount and DealRate are present.
func TestCounterSettleAmountValid(t *testing.T) {
	var counterSettleAmountTestJSON = `{
		"Allocations": [
		  {
			"PortfolioId":12,
			"CounterSettleAmount" : 12000,
			"DealtSettleAmount": 12000,
			"DealRate": 1.5
		  }
		],
		"SystemCurrencySecurityId": 13
	}`
	var counterSettleAmount CounterSettleAmount = 18000
	var transaction Transaction
	unmarshalErr := json.Unmarshal([]byte(counterSettleAmountTestJSON), &transaction)
	assert.Nil(t, unmarshalErr)
	err := counterSettleAmount.Validate(transaction, "CounterSettleAmount", 0)

	assert.Nil(t, err)
}
