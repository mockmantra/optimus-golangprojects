package models

import (
	"fmt"
	"strings"
)

// BaseCurrencySecurityID field
type BaseCurrencySecurityID int

// Validate validates the fields
func (value BaseCurrencySecurityID) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	currentAllocation := transaction.Allocations[allocationIndex]
	CurrencyPortfolio, err := NewPortfolioRequest(int(*currentAllocation.PortfolioID))
	if err == nil {
		if CurrencyPortfolio.BaseCurrencyID == int(value) {
			return nil
		}
		return fmt.Errorf("Error in allocation PortfolioID %d. Invalid %s : Expected value %d, Provided Value %d", int(*currentAllocation.PortfolioID), fieldName, CurrencyPortfolio.BaseCurrencyID, value)
	} else if strings.Contains(err.Error(), "error:404") {
		return fmt.Errorf("Invalid %s : PortfolioID %d does not exist", fieldName, int(*currentAllocation.PortfolioID))
	}
	return fmt.Errorf("Error in allocation portfolioID %d. %s : Service lookup failed", int(*currentAllocation.PortfolioID), fieldName)
}
