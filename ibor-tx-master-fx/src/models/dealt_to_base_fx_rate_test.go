package models

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

//When DealtCurrency is equal to BaseCurrency; DealToBaseFXRate = 1
func TestDealToBaseFXRate_DealtCurrencySecurityIdEqualsBaseCurrencySecurityID(t *testing.T) {
	dealtToBaseFXRate := DealtToBaseFXRate(1)
	allocations := make([]Allocation, 0)
	baseCurrency := BaseCurrencySecurityID(155)
	allocation := Allocation{
		BaseCurrencySecurityID: &baseCurrency,
	}
	allocations = append(allocations, allocation)
	transaction := Transaction{
		Allocations:             allocations,
		DealtCurrencySecurityID: 155,
	}
	err := dealtToBaseFXRate.Validate(transaction, "DealtToBaseFXRate", 0)

	assert.Nil(t, err)
}

//When DealtCurrency is equal to BaseCurrency; DealToBaseFXRate != 1
func TestDealToBaseFXRate_IsNotEqualTo1DealtCurrencySecurityIdEqualsBaseCurrencySecurityID(t *testing.T) {
	dealtToBaseFXRate := DealtToBaseFXRate(2)
	allocations := make([]Allocation, 0)
	baseCurrency := BaseCurrencySecurityID(15)
	portfolioID := PortfolioID(12)
	allocation := Allocation{
		BaseCurrencySecurityID: &baseCurrency,
		PortfolioID:            &portfolioID,
	}
	allocations = append(allocations, allocation)
	transaction := Transaction{
		Allocations:             allocations,
		DealtCurrencySecurityID: 15,
	}
	err := dealtToBaseFXRate.Validate(transaction, "DealtToBaseFXRate", 0)

	assert.EqualError(t, err, "Error in allocation PortfolioID 12. Invalid DealtToBaseFXRate. The BaseCurrencySecurityID and DealtCurrencySecurityID are equal but the value of DealtToBaseFXRate is 2 instead of 1")
}
