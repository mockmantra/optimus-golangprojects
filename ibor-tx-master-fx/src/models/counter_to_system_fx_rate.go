package models

import (
	"fmt"
)

//CounterToSystemFXRate field
type CounterToSystemFXRate float64

// Validate validates the fields
func (value CounterToSystemFXRate) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	if transaction.CounterCurrencySecurityID == transaction.SystemCurrencySecurityID {
		if value != 1 {
			return fmt.Errorf("Error in allocation PortfolioID %d. Invalid %s. The CounterCurrencySecurityID and SystemCurrencySecurityID are equal but the value of CounterToSystemFXRate is %g instead of 1", *transaction.Allocations[allocationIndex].PortfolioID, fieldName, float64(value))
		}
	}
	return nil
}
