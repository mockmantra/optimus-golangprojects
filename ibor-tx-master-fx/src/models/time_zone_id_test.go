package models

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/httputil"
)

func TestTimeZoneId_WithErrorInHttpCall(t *testing.T) {
	var timeZoneID TimeZoneID = 7
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadGateway)
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"

	defer ts.Close()
	transaction := Transaction{}

	err := timeZoneID.Validate(transaction, "TimeZoneId")

	assert.EqualError(t, err, "TimeZoneId: Service lookup failed")

	httputil.UserSessionToken = ""

}
func TestTimeZoneId_WithSecurityIdNotFound(t *testing.T) {
	var timeZoneID TimeZoneID = 7
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	}))
	appconfig.BaseURI = ts.URL

	defer ts.Close()

	httputil.UserSessionToken = "123"

	transaction := Transaction{}

	err := timeZoneID.Validate(transaction, "TimeZoneId")

	assert.EqualError(t, err, fmt.Sprintf("Invalid TimeZoneId. TimeZoneId: %d does not exist", timeZoneID))

	httputil.UserSessionToken = ""
}
func TestTimeZoneId_WithValidResponse(t *testing.T) {
	var timeZoneID TimeZoneID = 7
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte("[{\"BaseUtcOffset\":-300}]"))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"

	defer ts.Close()
	transaction := Transaction{}

	err := timeZoneID.Validate(transaction, "TimeZoneId")

	assert.Nil(t, err)

	httputil.UserSessionToken = ""
}
