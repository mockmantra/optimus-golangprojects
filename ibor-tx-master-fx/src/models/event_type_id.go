package models

import (
	"encoding/json"
	"fmt"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/httputil"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/logger"
)

const eventTypeURL = "securitymaster/v1/Templates/GetEventTypesForTemplate?templateId=%d"

// EventTypeID field
type EventTypeID int

// EventType field
type EventType struct {
	EventTypeID   int    `json:"EventTypeId"`
	EventTypeName string `json:"EventTypeName"`
}

// Validate validates the fields
func (value EventTypeID) Validate(transaction Transaction, fieldName string) error {
	eventTypes, err := NewGetEventTypeForTemplateRequest(1)
	if err == nil {
		var exists bool
		for _, eventType := range eventTypes {
			if eventType.EventTypeID == int(value) {
				logger.Log.Infof("Validating eventType. EventTypeName: %s", eventType.EventTypeName)
				exists = true
				break
			}
		}
		if !exists {
			return fmt.Errorf("Invalid %s. Must be 3(for Buy), 6(for Cover), 14(for Sell) or 17(for Short)", fieldName)
		}
	} else {
		return fmt.Errorf("%s : Service lookup failed", fieldName)
	}
	return nil
}

// NewGetEventTypeForTemplateRequest gets EventTypes for TemplateId
func NewGetEventTypeForTemplateRequest(templateID int) ([]EventType, error) {
	bodyBytes, err := httputil.NewRequest("GET", fmt.Sprintf(eventTypeURL, templateID), nil)
	eventTypes := make([]EventType, 0)
	if err == nil {
		errMarshal := json.Unmarshal(bodyBytes, &eventTypes)
		if errMarshal != nil {
			return eventTypes, errMarshal
		}
	}
	return eventTypes, err
}
