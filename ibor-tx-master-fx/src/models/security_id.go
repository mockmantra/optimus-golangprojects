package models

import (
	"fmt"
	"strings"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/logger"
)

// Deal ...
type Deal struct {
	ID                     string `json:"id"`
	DealSide               string `json:"dealSide"`
	CurrencyPairSecurityID int    `json:"currencyPairSecurityId"`
}

// DealsReponse ...
type DealsReponse struct {
	Deals []*Deal `json:"deals"`
}

// DealsReponseData ...
type DealsReponseData struct {
	Data DealsReponse `json:"data"`
}

// SecurityID field
type SecurityID int

// Validate validates the fields
func (value SecurityID) Validate(transaction Transaction, fieldName string) error {
	securities, err := NewSecurityRequest(int(value))
	if err == nil {
		logger.Log.Infof("Validating security. Symbol:  %s", securities[0].Symbol)
		if securities[0].AssetClassName != "FX" {
			return fmt.Errorf("Invalid %s. Expected asset class: FX, Provided asset class: %s", fieldName, securities[0].AssetClassName)
		}
	} else {
		if strings.Contains(err.Error(), "error:404") {
			return fmt.Errorf("Invalid %s. SecurityId: %d does not exist", fieldName, value)
		}
		return fmt.Errorf("Invalid %s. Service lookup failed", fieldName)

	}
	return nil
}
