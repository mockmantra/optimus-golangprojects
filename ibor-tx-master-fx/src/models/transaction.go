package models

// Validator interface
type Validator interface {
	Validate(transaction Transaction, fieldName string) error
}

// Transaction transaction
//TODO: All the fields with *int64 as type need a new custom type which has a Validator method associated with it.
type Transaction struct {
	Allocations               []Allocation           `json:"Allocations"`
	BusinessDateTimeUTC       BusinessDateTimeUTC    `json:"BusinessDateTimeUTC"`
	CounterCurrencySecurityID CurrencySecurityID     `json:"CounterCurrencySecurityId"`
	DealtCurrencySecurityID   CurrencySecurityID     `json:"DealtCurrencySecurityId"`
	EventTypeID               EventTypeID            `json:"EventTypeId"`
	OrderCreatedDate          string                 `json:"OrderCreatedDate"`
	SecurityID                SecurityID             `json:"SecurityId"`
	SecurityTemplateID        SecurityTemplateID     `json:"SecurityTemplateId"`
	SequenceNumber            int                    `json:"SequenceNumber"`
	SettleDate                TranactionDate         `json:"SettleDate"`
	SourceSystemName          string                 `json:"SourceSystemName"`
	SourceSystemReference     string                 `json:"SourceSystemReference"`
	SystemCurrencySecurityID  CurrencySecurityID     `json:"SystemCurrencySecurityId"`
	TradeDate                 TranactionDate         `json:"TradeDate"`
	TransactionDateTimeUTC    TransactionDateTimeUTC `json:"TransactionDateTimeUTC"`
	TransactionTimeZoneID     TimeZoneID             `json:"TransactionTimeZoneId"`
	OrderQuantity             DealtSettleAmount      `json:"OrderQuantity"`
}
