package httputil

import (
	"context"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/logger"
	"stash.ezesoft.net/ipc/ezeutils"
)

// UserSessionToken set in main.go
var UserSessionToken = ""

// Ctx is currentContext
var Ctx context.Context

// NewRequest method
func NewRequest(httpMethod string, url string, body io.Reader) ([]byte, error) {
	client := &http.Client{}
	apiURL := fmt.Sprintf("%s/api/%s", appconfig.BaseURI, url)
	req, _ := http.NewRequest(httpMethod, apiURL, body)
	if Ctx != nil {
		req = req.WithContext(Ctx)
		ezeutils.SetRequestHeadersFromContext(Ctx, req, logger.Log)
	}
	if UserSessionToken == "" {
		uErr := fmt.Errorf("Authentication Failed, No UserSessionToken")
		logger.Log.Errorf(uErr.Error())
		return nil, uErr
	}

	req.Header.Add("authorization", getAuthorizationToken(UserSessionToken))
	resp, err := client.Do(req)
	if resp != nil && resp.StatusCode != 200 {
		errorMsg := fmt.Sprintf("%s, error:%s", apiURL, resp.Status)
		logger.Log.Debug(errorMsg)
		return nil, errors.New(errorMsg)
	} else if err != nil {
		return nil, err
	}
	bodyBytes, _ := ioutil.ReadAll(resp.Body)
	return bodyBytes, nil
}

func getAuthorizationToken(UserSessionToken string) string {
	str := UserSessionToken + ":"
	return "Basic " + base64.StdEncoding.EncodeToString([]byte(str))
}
