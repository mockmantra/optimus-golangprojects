package workdistro

import (
	"context"
	"runtime/debug"

	tomb "gopkg.in/tomb.v2"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/interfaces"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/models"
	"stash.ezesoft.net/ipc/ezeutils"

	"github.com/pkg/errors"

	"stash.ezesoft.net/ipc/work-distribution/workdistributionclient"
)

var (
	distributionStatusChan chan workdistributionclient.ClientStatus
	tmb                    tomb.Tomb
)

// ListenWDAssignments ...
func ListenWDAssignments(assignmentChan chan models.Assignment, logger interfaces.ILogger) {
	defer func() {
		if p := recover(); p != nil {
			logger.Errorf("Recover on ListenWDAssignments %s: %s", p, string(debug.Stack()))
		}
	}()
	logger.Info("subscribing to work distribution service...")
	var err error
	var distibutionAssignmentChan chan workdistributionclient.WorkerAssignments
	var quitChan chan bool
	ctx := ezeutils.WithKeyValue(tmb.Context(context.Background()), ezeutils.UserAgentKey, "ibor_tx_master_fx/1.0 Go-http-client/1.1")
	distibutionAssignmentChan, distributionStatusChan, quitChan, err = workdistributionclient.SubscribeToWork(
		ctx,
		appconfig.ServiceName,
		appconfig.WorkType,
		&map[string]string{"Host": appconfig.TxMasterHost, "Container": appconfig.TxMasterContainer, "Instance": appconfig.TxMasterInstance})
	if err != nil {
		err = errors.Wrap(err, "error when start subscribe to work distribution service")
		logger.Error(err.Error())
	}
	logger.Info("subscribed to work...")
	tmb.Go(func() error {
		hasUpdatedSequenceNumber := false
		for {
			select {
			case <-quitChan:
				logger.Info("stop looking for assignments - WD client quit")
				return nil
			case newAssignment := <-distibutionAssignmentChan:
				if len(newAssignment.WorkItems) > 0 && !hasUpdatedSequenceNumber {
					assignment := convertWDAssignments(&newAssignment, logger)
					select {
					case assignmentChan <- assignment:
						hasUpdatedSequenceNumber = true
					default:
						logger.Debug("assignments channel blocked")
					}
				}
			case message := <-distributionStatusChan:
				if message.Error != nil {
					logger.Errorf("WD subscription error, error:%v", message.Error)
				}
				if message.Message != "" {
					logger.Infof("WD subscription message: %s", message.Message)
				}
			}
		}
	})
}

func convertWDAssignments(a *workdistributionclient.WorkerAssignments, logger interfaces.ILogger) models.Assignment {
	assignment := models.Assignment{}
	for _, wi := range a.WorkItems {
		if wi.Metadata == nil {
			logger.Warn("invalid work item nil metadata")
			continue
		}
		md := wi.Metadata
		mdm, ok := md.(map[string]interface{})
		if !ok {
			logger.Warnf("invalid md interface conversion: %T", md)
		}
		// SequenceNumber
		SequenceNumber, ok := mdm["SequenceNumber"]
		if !ok {
			logger.Warnf("invalid interface conversion: %T", mdm["SequenceNumber"])
			continue
		}
		assignment.SequenceNumber = SequenceNumber.(string)
	}
	return assignment
}
