package interfaces

import (
	"context"

	"stash.ezesoft.net/ipc/ezelogger"
)

// ILogger interface
type ILogger interface {
	WithContext(ctx context.Context) *ezelogger.EzeLog
	ezelogger.EzeLogger
}
