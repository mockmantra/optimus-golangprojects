package interfaces

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/kinesis"
	"github.com/aws/aws-sdk-go/service/kinesis/kinesisiface"
)

// IKinesisUtil interface
type IKinesisUtil interface {
	HandleMessage(data []byte, streamName string, logger ILogger)
	GetShardIterator(shardID, streamName, startingSequenceNumber string, logger ILogger) (*kinesis.GetShardIteratorOutput, error)
	GetRecords(shardIterator *string, logger ILogger) (*kinesis.GetRecordsOutput, error)
	InsertRecord(streamName string, key string, value []byte) (*kinesis.PutRecordOutput, error)
}

// IAwsCreds interface
type IAwsCreds interface {
	NewStaticCredentials(id, secret, token string) *credentials.Credentials
}

// IAwsSession interface
type IAwsSession interface {
	New(cfgs ...*aws.Config) *session.Session
}

// IKinesis interface
type IKinesis interface {
	New(s *session.Session) kinesisiface.KinesisAPI
}
