package interfaces

import (
	"time"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/models"
)

// IDataAccessor - Interface for accessing Database
type IDataAccessor interface {
	InsertTransaction(*models.Transactiontable, time.Time) error
	Close() error
	Connect(string) error
	InsertValidationErrors(recordid int, validationErrors []string) error
}
