package interfaces

import (
	"io"

	"github.com/hashicorp/vault/api"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/models"
)

// IVault interface
type IVault interface {
	ReadTenantCreds() (map[string]interface{}, error)
	GetDbCredentials() map[string]*models.AuroraConnection
	GetTenantCredentials(firmToken string) (*models.AuroraConnection, error)
	Login() (*api.Secret, error)
}

// IVaultAPI interface
type IVaultAPI interface {
	ParseSecret(r io.Reader) (*api.Secret, error)
	DefaultConfig() IVaultConfig
	NewClient(config IVaultConfig) (IVaultClient, error)
}

//IVaultClient interface
type IVaultClient interface {
	Logical() *api.Logical
	SetToken(v string)
	NewRequest(method, requestPath string) *api.Request
	RawRequest(r *api.Request) (*api.Response, error)
}

//IVaultConfig ...
type IVaultConfig interface {
	ConfigureTLS(*api.TLSConfig) error
	ReadEnvironment() error
}
