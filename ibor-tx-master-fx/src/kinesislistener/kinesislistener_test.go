package kinesislistener

import (
	// "context"
	// "encoding/json"
	"errors"
	"testing"
	"time"

	// "github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/kinesis"
	"github.com/stretchr/testify/mock"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src"
	// "stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/handler"
	// "stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/interfaces"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/mocks"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/models"
	// "stash.ezesoft.net/ipc/work-distribution/workdistributionclient"
)

type allmocks struct {
	kinesisUtil mocks.KinesisUtil
	logger      mocks.Logger
}

func TestStart_WithShardIteratorError(t *testing.T) {
	// GIVEN
	assignments := make(chan models.Assignment)
	defer close(assignments)

	done := make(chan bool)
	defer close(done)

	go func() {
		assignments <- models.Assignment{SequenceNumber: "123"}
		time.Sleep(time.Duration(time.Second * 1))
		done <- true
	}()

	// WHEN
	var allmocks allmocks

	appconfig.AwsUpStreamName = "teststream"

	allmocks.kinesisUtil.On("GetShardIterator", appconfig.KinesisShardID, appconfig.AwsUpStreamName, "123", &allmocks.logger).Return(nil, errors.New("Failed to get Shard Iterator"))
	Start(assignments, &allmocks.kinesisUtil, &allmocks.logger)
	<-done // blocks until the input write routine is finished
	allmocks.kinesisUtil.AssertExpectations(t)
}

func TestStart_WithGetRecordsError(t *testing.T) {
	// GIVEN
	assignments := make(chan models.Assignment)
	defer close(assignments)

	done := make(chan bool)
	defer close(done)

	go func() {
		assignments <- models.Assignment{SequenceNumber: "123"}
		time.Sleep(time.Duration(time.Second * 1))
		done <- true
	}()

	// WHEN
	var allmocks allmocks

	appconfig.AwsUpStreamName = "teststream"

	shardIteratorOutput := &kinesis.GetShardIteratorOutput{}
	allmocks.logger.On("Info", mock.Anything).Return()
	allmocks.kinesisUtil.On("GetShardIterator", appconfig.KinesisShardID, appconfig.AwsUpStreamName, "123", &allmocks.logger).Return(shardIteratorOutput, nil)
	allmocks.kinesisUtil.On("GetRecords", shardIteratorOutput.ShardIterator, &allmocks.logger).Return(nil, errors.New("failed to get records"))
	Start(assignments, &allmocks.kinesisUtil, &allmocks.logger)
	<-done // blocks until the input write routine is finished
	allmocks.kinesisUtil.AssertExpectations(t)
	allmocks.logger.AssertCalled(t, "Info", "Sleeping for 10 seconds...")
}
func TestStart_WithGetRecordsErrorAsExpiredIteratorExceptionPrefix(t *testing.T) {
	// GIVEN
	assignments := make(chan models.Assignment)
	defer close(assignments)

	done := make(chan bool)
	defer close(done)

	go func() {
		assignments <- models.Assignment{SequenceNumber: "123"}
		time.Sleep(time.Duration(time.Second * 1))
		done <- true
	}()

	// WHEN
	var allmocks allmocks

	appconfig.AwsUpStreamName = "teststream"

	shardIteratorOutput := &kinesis.GetShardIteratorOutput{}
	allmocks.logger.On("Info", mock.Anything).Return()
	allmocks.logger.On("Infof", mock.Anything, mock.Anything).Return()
	allmocks.kinesisUtil.On("GetShardIterator", appconfig.KinesisShardID, appconfig.AwsUpStreamName, "123", &allmocks.logger).Return(shardIteratorOutput, nil)
	allmocks.kinesisUtil.On("GetRecords", shardIteratorOutput.ShardIterator, &allmocks.logger).Return(nil, errors.New("ExpiredIteratorException"))
	Start(assignments, &allmocks.kinesisUtil, &allmocks.logger)
	<-done // blocks until the input write routine is finished
	allmocks.kinesisUtil.AssertExpectations(t)
	allmocks.logger.AssertCalled(t, "Info", "Sleeping for 10 seconds...")
	allmocks.logger.AssertCalled(t, "Infof", "GetRecords returned ExpiredIteratorException hence refreshing the shardIterator with sequenceNumber: %s", "123")
}
func TestStart_WithGetRecordsWithEmptyReponse(t *testing.T) {
	// GIVEN
	assignments := make(chan models.Assignment)
	defer close(assignments)

	done := make(chan bool)
	defer close(done)

	go func() {
		assignments <- models.Assignment{SequenceNumber: "123"}
		time.Sleep(time.Duration(time.Second * 1))
		done <- true
	}()

	// WHEN
	var allmocks allmocks

	appconfig.AwsUpStreamName = "teststream"

	shardIteratorOutput := &kinesis.GetShardIteratorOutput{}
	getRecordsOutput := &kinesis.GetRecordsOutput{}
	allmocks.logger.On("Info", mock.Anything).Return()
	allmocks.kinesisUtil.On("GetShardIterator", appconfig.KinesisShardID, appconfig.AwsUpStreamName, "123", &allmocks.logger).Return(shardIteratorOutput, nil)
	allmocks.kinesisUtil.On("GetRecords", shardIteratorOutput.ShardIterator, &allmocks.logger).Return(getRecordsOutput, nil)
	Start(assignments, &allmocks.kinesisUtil, &allmocks.logger)
	<-done // blocks until the input write routine is finished
	allmocks.kinesisUtil.AssertExpectations(t)
	allmocks.logger.AssertCalled(t, "Info", "No new records found...")
	allmocks.logger.AssertCalled(t, "Info", "Sleeping for 10 seconds...")
}

// func TestStart_WithGetRecordsWithNonEmptyReponseAndFailedToUpdateTheSequenceNumber(t *testing.T) {
// 	// GIVEN
// 	assignments := make(chan models.Assignment)
// 	defer close(assignments)

// 	done := make(chan bool)
// 	defer close(done)

// 	go func() {
// 		assignments <- models.Assignment{SequenceNumber: "123"}
// 		time.Sleep(time.Duration(time.Second * 1))
// 		done <- true
// 	}()

// 	// WHEN
// 	var allmocks allmocks

// 	appconfig.AwsUpStreamName = "teststream"
// 	err := errors.New("Failed to update")
// 	shardIteratorOutput := &kinesis.GetShardIteratorOutput{}
// 	records := []*kinesis.Record{{SequenceNumber: aws.String("1234")}}
// 	getRecordsOutput := &kinesis.GetRecordsOutput{Records: records}
// 	allmocks.logger.On("Info", mock.Anything).Return()
// 	allmocks.logger.On("Infof", mock.Anything, mock.Anything).Return()
// 	allmocks.logger.On("Errorf", mock.Anything, mock.Anything).Return()
// 	allmocks.kinesisUtil.On("GetShardIterator", appconfig.KinesisShardID, appconfig.AwsUpStreamName, "123", &allmocks.logger).Return(shardIteratorOutput, nil)
// 	allmocks.kinesisUtil.On("GetRecords", shardIteratorOutput.ShardIterator, &allmocks.logger).Return(getRecordsOutput, nil)
// 	monkey.Patch(workdistributionclient.UpdateWorkItem, func(ctx context.Context, workType, workId string, metadata interface{}) (*workdistributionclient.WorkWorkItem, error) {
// 		return nil, err
// 	})
// 	monkey.Patch(handler.HandleTransaction, func(record kinesis.Record, k interfaces.IKinesisUtil, log interfaces.ILogger) {
// 	})

// 	Start(assignments, &allmocks.kinesisUtil, &allmocks.logger)
// 	<-done // blocks until the input write routine is finished
// 	allmocks.kinesisUtil.AssertExpectations(t)
// 	allmocks.logger.AssertCalled(t, "Infof", "Kinesis message Sequence number: %s", "1234")
// 	allmocks.logger.AssertCalled(t, "Errorf", "Failed to update sequence number in workItem: %v", err)
// 	allmocks.logger.AssertCalled(t, "Info", "Sleeping for 10 seconds...")
// }
// func TestStart_WithGetRecordsSuccesfullyUpdateWorkItem(t *testing.T) {
// 	// GIVEN
// 	assignments := make(chan models.Assignment)
// 	defer close(assignments)

// 	done := make(chan bool)
// 	defer close(done)

// 	go func() {
// 		assignments <- models.Assignment{SequenceNumber: "123"}
// 		time.Sleep(time.Duration(time.Second * 1))
// 		done <- true
// 	}()

// 	// WHEN
// 	var allmocks allmocks

// 	appconfig.AwsUpStreamName = "teststream"
// 	shardIteratorOutput := &kinesis.GetShardIteratorOutput{}
// 	records := []*kinesis.Record{{SequenceNumber: aws.String("1234")}}
// 	getRecordsOutput := &kinesis.GetRecordsOutput{Records: records}
// 	allmocks.logger.On("Info", mock.Anything).Return()
// 	allmocks.logger.On("Infof", mock.Anything, mock.Anything).Return()
// 	allmocks.logger.On("Errorf", mock.Anything, mock.Anything).Return()
// 	allmocks.kinesisUtil.On("GetShardIterator", appconfig.KinesisShardID, appconfig.AwsUpStreamName, "123", &allmocks.logger).Return(shardIteratorOutput, nil)
// 	allmocks.kinesisUtil.On("GetRecords", shardIteratorOutput.ShardIterator, &allmocks.logger).Return(getRecordsOutput, nil)
// 	monkey.Patch(workdistributionclient.UpdateWorkItem, func(ctx context.Context, workType, workId string, metadata interface{}) (*workdistributionclient.WorkWorkItem, error) {
// 		return &workdistributionclient.WorkWorkItem{}, nil
// 	})
// 	monkey.Patch(handler.HandleTransaction, func(record kinesis.Record, k interfaces.IKinesisUtil, log interfaces.ILogger) {
// 	})

// 	Start(assignments, &allmocks.kinesisUtil, &allmocks.logger)
// 	<-done // blocks until the input write routine is finished
// 	allmocks.kinesisUtil.AssertExpectations(t)
// 	allmocks.logger.AssertCalled(t, "Infof", "Kinesis message Sequence number: %s", "1234")
// 	allmocks.logger.AssertCalled(t, "Infof", "Successfully updated sequence number in workItem: %s", "{\"Enabled\":false,\"Id\":\"\",\"Metadata\":null}")
// 	allmocks.logger.AssertCalled(t, "Info", "Sleeping for 10 seconds...")
// // }
// func TestStart_WithGetRecordsSuccesfullyUpdateWorkItemWithFailedToParseTheResponse(t *testing.T) {
// 	// GIVEN
// 	assignments := make(chan models.Assignment)
// 	defer close(assignments)

// 	done := make(chan bool)
// 	defer close(done)

// 	go func() {
// 		assignments <- models.Assignment{SequenceNumber: "123"}
// 		time.Sleep(time.Duration(time.Second * 1))
// 		done <- true
// 	}()

// 	// WHEN
// 	var allmocks allmocks

// 	appconfig.AwsUpStreamName = "teststream"
// 	shardIteratorOutput := &kinesis.GetShardIteratorOutput{}
// 	records := []*kinesis.Record{{SequenceNumber: aws.String("1234")}}
// 	getRecordsOutput := &kinesis.GetRecordsOutput{Records: records}
// 	allmocks.logger.On("Info", mock.Anything).Return()
// 	allmocks.logger.On("Infof", mock.Anything, mock.Anything).Return()
// 	allmocks.logger.On("Errorf", mock.Anything, mock.Anything).Return()
// 	allmocks.kinesisUtil.On("GetShardIterator", appconfig.KinesisShardID, appconfig.AwsUpStreamName, "123", &allmocks.logger).Return(shardIteratorOutput, nil)
// 	allmocks.kinesisUtil.On("GetRecords", shardIteratorOutput.ShardIterator, &allmocks.logger).Return(getRecordsOutput, nil)
// 	monkey.Patch(workdistributionclient.UpdateWorkItem, func(ctx context.Context, workType, workId string, metadata interface{}) (*workdistributionclient.WorkWorkItem, error) {
// 		return &workdistributionclient.WorkWorkItem{}, nil
// 	})
// 	monkey.Patch(handler.HandleTransaction, func(record kinesis.Record, k interfaces.IKinesisUtil, log interfaces.ILogger) {
// 	})
// 	err := errors.New("Failed to parse")
// 	monkey.Patch(json.Marshal, func(v interface{}) ([]byte, error) {
// 		return nil, err
// 	})

// 	Start(assignments, &allmocks.kinesisUtil, &allmocks.logger)
// 	<-done // blocks until the input write routine is finished
// 	allmocks.kinesisUtil.AssertExpectations(t)
// 	allmocks.logger.AssertCalled(t, "Infof", "Kinesis message Sequence number: %s", "1234")
// 	allmocks.logger.AssertCalled(t, "Errorf", "Error in parsing UpdateWorkItem response: %v", err)
// 	allmocks.logger.AssertCalled(t, "Info", "Sleeping for 10 seconds...")
// }
