package handler

// import (
// 	"context"
// 	"errors"
// 	"reflect"
// 	"testing"

// 	"github.com/stretchr/testify/assert"

// 	"github.com/aws/aws-sdk-go/service/kinesis"
// 	"github.com/stretchr/testify/mock"
// 	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/appcontext"
// 	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/database"
// 	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/interfaces"
// 	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/mocks"
// 	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/models"
// 	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/persisttransaction"
// 	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/servicesession"
// 	validator "stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/validator"
// )

// type allMocks struct {
// 	kinesisUtil mocks.KinesisUtil
// 	logger      mocks.Logger
// }

// func TestHandleTransaction_WithInvalidRecordData(t *testing.T) {

// 	record := kinesis.Record{
// 		Data: []byte("test"),
// 	}

// 	var allmocks allMocks
// 	log := &allmocks.logger
// 	monkey.Patch(appcontext.GetLoggerWithCtx, func(ctx context.Context) interfaces.ILogger {
// 		return log
// 	})
// 	log.On("Errorf", mock.Anything, mock.Anything).Return()
// 	HandleTransaction(record, &allmocks.kinesisUtil, log)

// 	log.AssertExpectations(t)
// }
// func TestHandleTransaction_WithInvalidTransaction(t *testing.T) {

// 	streamMessage := `{
// 		"FirmID" : 123,
// 		"FirmAuthToken" :"T71Q5",
// 		"UserID" : 12,
// 		"Message" : "test",
// 		"ActivityID" :"dsdafdd",
// 		"UserSessionToken" :"hgggdsfgds"
// 	}`

// 	record := kinesis.Record{
// 		Data: []byte(streamMessage),
// 	}

// 	var allmocks allMocks
// 	log := &allmocks.logger
// 	monkey.Patch(appcontext.GetLoggerWithCtx, func(ctx context.Context) interfaces.ILogger {
// 		return log
// 	})
// 	log.On("Errorf", mock.Anything, mock.Anything).Return()
// 	HandleTransaction(record, &allmocks.kinesisUtil, log)

// 	log.AssertExpectations(t)
// }
// func TestHandleTransaction_WithErrorInSetUserSessionToken(t *testing.T) {

// 	streamMessage := `{
// 		"FirmID" : 123,
// 		"FirmAuthToken" :"T71Q5",
// 		"UserID" : 12,
// 		"Message" : "{\"Allocations\":[]}",
// 		"ActivityID" :"dsdafdd",
// 		"UserSessionToken" :"hgggdsfgds"
// 	}`

// 	record := kinesis.Record{
// 		Data: []byte(streamMessage),
// 	}

// 	var allmocks allMocks
// 	tokenError := errors.New("Unable to set the session token")
// 	monkey.Patch(servicesession.SetUserSessionToken, func(firmToken string) error {
// 		return tokenError
// 	})
// 	log := &allmocks.logger
// 	monkey.Patch(appcontext.GetLoggerWithCtx, func(ctx context.Context) interfaces.ILogger {
// 		return log
// 	})
// 	log.On("Errorf", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything)
// 	HandleTransaction(record, &allmocks.kinesisUtil, log)

// 	log.AssertCalled(t, "Errorf", "Failed to get user session token for FirmId: %d, UserName: %s, ActivityId: %s, Error: %v", 123, "", "dsdafdd", tokenError)
// }
// func TestHandleTransaction_WithErrorInDbConnect(t *testing.T) {

// 	streamMessage := `{
// 		"FirmID" : 123,
// 		"FirmAuthToken" :"T71Q5",
// 		"UserID" : 12,
// 		"Message" : "{\"Allocations\":[]}",
// 		"ActivityID" :"dsdafdd",
// 		"UserSessionToken" :"hgggdsfgds"
// 	}`

// 	record := kinesis.Record{
// 		Data: []byte(streamMessage),
// 	}

// 	var allmocks allMocks
// 	dbConnectErr := errors.New("Failed to connect to DB")
// 	monkey.Patch(servicesession.SetUserSessionToken, func(firmToken string) error {
// 		return nil
// 	})
// 	log := &allmocks.logger
// 	monkey.Patch(appcontext.GetLoggerWithCtx, func(ctx context.Context) interfaces.ILogger {
// 		return log
// 	})
// 	da := database.DataAccessorInstance()
// 	monkey.PatchInstanceMethod(reflect.TypeOf(da), "Connect", func(_ *database.DataAccessor, firmAuthToken string) (err error) {
// 		return dbConnectErr
// 	})

// 	log.On("Errorf", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return()
// 	HandleTransaction(record, &allmocks.kinesisUtil, log)

// 	log.AssertCalled(t, "Errorf", "Connection to db failed, error:%v", dbConnectErr)
// }
// func TestHandleTransaction_WithErrorInPersistTransaction(t *testing.T) {

// 	streamMessage := `{
// 		"FirmID" : 123,
// 		"FirmAuthToken" :"T71Q5",
// 		"UserID" : 12,
// 		"Message" : "{\"Allocations\":[]}",
// 		"ActivityID" :"dsdafdd",
// 		"UserSessionToken" :"hgggdsfgds"
// 	}`

// 	record := kinesis.Record{
// 		Data: []byte(streamMessage),
// 	}

// 	var allmocks allMocks
// 	persistTransactionsMasterErr := errors.New("Failed to Insert to DB")
// 	monkey.Patch(servicesession.SetUserSessionToken, func(firmToken string) error {
// 		return nil
// 	})

// 	da := database.DataAccessorInstance()
// 	monkey.PatchInstanceMethod(reflect.TypeOf(da), "Connect", func(_ *database.DataAccessor, firmAuthToken string) (err error) {
// 		return nil
// 	})

// 	monkey.Patch(persisttransaction.PersistTransactionsMaster, func(trs *models.Transactiontable, logger interfaces.ILogger,
// 		eventTypeID models.EventTypeID, sourceSystemName string, securityID models.SecurityID, orderQuantity float64,
// 		da interfaces.IDataAccessor, rawDataStr string, userID int, userName string, userSessionToken string, activityID string, businessDateTime string, sourceSystemReference string, transactionDateTimeUTC string) (int, error) {
// 		return 0, persistTransactionsMasterErr
// 	})

// 	log := &allmocks.logger
// 	monkey.Patch(appcontext.GetLoggerWithCtx, func(ctx context.Context) interfaces.ILogger {
// 		return log
// 	})
// 	log.On("Errorf", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return()
// 	HandleTransaction(record, &allmocks.kinesisUtil, log)

// 	log.AssertCalled(t, "Errorf", "Error in saving transaction to db, error:%v", persistTransactionsMasterErr)
// }
// func TestHandleTransaction_ErrorInInsertingTheValidationMessages(t *testing.T) {

// 	streamMessage := `{
// 		"FirmID" : 123,
// 		"FirmAuthToken" :"T71Q5",
// 		"UserID" : 12,
// 		"Message" : "{\"Allocations\":[]}",
// 		"ActivityID" :"dsdafdd",
// 		"UserSessionToken" :"hgggdsfgds"
// 	}`

// 	record := kinesis.Record{
// 		Data: []byte(streamMessage),
// 	}

// 	var allmocks allMocks
// 	monkey.Patch(servicesession.SetUserSessionToken, func(firmToken string) error {
// 		return nil
// 	})

// 	da := database.DataAccessorInstance()
// 	monkey.PatchInstanceMethod(reflect.TypeOf(da), "Connect", func(_ *database.DataAccessor, firmAuthToken string) (err error) {
// 		return nil
// 	})

// 	monkey.Patch(persisttransaction.PersistTransactionsMaster, func(trs *models.Transactiontable, logger interfaces.ILogger,
// 		eventTypeID models.EventTypeID, sourceSystemName string, securityID models.SecurityID, orderQuantity float64,
// 		da interfaces.IDataAccessor, rawDataStr string, userID int, userName string, userSessionToken string, activityID string, businessDateTime string, sourceSystemReference string, transactionDateTimeUTC string) (int, error) {
// 		return 12, nil
// 	})

// 	var errorMessages []validator.ErrorMessage
// 	errorMessages = append(errorMessages, validator.ErrorMessage{Field: "F1", Message: []string{"E1"}})
// 	monkey.Patch(validator.Validate, func(data models.Transaction, _ interfaces.ILogger) []validator.ErrorMessage {
// 		return errorMessages
// 	})

// 	errInsertValidationErrors := errors.New("Error in inserting Validation Errors")

// 	monkey.PatchInstanceMethod(reflect.TypeOf(da), "InsertValidationErrors", func(_ *database.DataAccessor, recordid int, validationErrors []string) error {
// 		return errInsertValidationErrors
// 	})
// 	log := &allmocks.logger
// 	monkey.Patch(appcontext.GetLoggerWithCtx, func(ctx context.Context) interfaces.ILogger {
// 		return log
// 	})
// 	log.On("Infof", mock.Anything, mock.Anything)

// 	log.On("Errorf", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return()
// 	log.On("Errorf", mock.Anything, mock.Anything, mock.Anything).Return()
// 	HandleTransaction(record, &allmocks.kinesisUtil, log)
// 	log.AssertCalled(t, "Errorf", "Error in saving rejections for recordId:%d to db, error:%v", 12, errInsertValidationErrors)
// }
// func TestHandleTransaction_ErrorInClosingTheDBConnection(t *testing.T) {

// 	streamMessage := `{
// 		"FirmID" : 123,
// 		"FirmAuthToken" :"T71Q5",
// 		"UserID" : 12,
// 		"Message" : "{\"Allocations\":[]}",
// 		"ActivityID" :"dsdafdd",
// 		"UserSessionToken" :"hgggdsfgds"
// 	}`

// 	record := kinesis.Record{
// 		Data: []byte(streamMessage),
// 	}

// 	var allmocks allMocks
// 	monkey.Patch(servicesession.SetUserSessionToken, func(firmToken string) error {
// 		return nil
// 	})

// 	da := database.DataAccessorInstance()
// 	monkey.PatchInstanceMethod(reflect.TypeOf(da), "Connect", func(_ *database.DataAccessor, firmAuthToken string) (err error) {
// 		return nil
// 	})

// 	monkey.Patch(persisttransaction.PersistTransactionsMaster, func(trs *models.Transactiontable, logger interfaces.ILogger,
// 		eventTypeID models.EventTypeID, sourceSystemName string, securityID models.SecurityID, orderQuantity float64,
// 		da interfaces.IDataAccessor, rawDataStr string, userID int, userName string, userSessionToken string, activityID string, businessDateTime string, sourceSystemReference string, transactionDateTimeUTC string) (int, error) {
// 		return 12, nil
// 	})

// 	var errorMessages []validator.ErrorMessage
// 	monkey.Patch(validator.Validate, func(data models.Transaction, _ interfaces.ILogger) []validator.ErrorMessage {
// 		return errorMessages
// 	})

// 	monkey.PatchInstanceMethod(reflect.TypeOf(da), "InsertValidationErrors", func(_ *database.DataAccessor, recordid int, validationErrors []string) error {
// 		return nil
// 	})

// 	errInDbClose := errors.New("Error in closing the Db connection")
// 	monkey.PatchInstanceMethod(reflect.TypeOf(da), "Close", func(_ *database.DataAccessor) error {
// 		return errInDbClose
// 	})
// 	log := &allmocks.logger
// 	monkey.Patch(appcontext.GetLoggerWithCtx, func(ctx context.Context) interfaces.ILogger {
// 		return log
// 	})
// 	log.On("Infof", mock.Anything, mock.Anything)

// 	log.On("Errorf", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return()
// 	log.On("Errorf", mock.Anything, mock.Anything, mock.Anything).Return()
// 	allmocks.kinesisUtil.On("HandleMessage", mock.Anything, mock.Anything, mock.Anything)
// 	HandleTransaction(record, &allmocks.kinesisUtil, log)

// 	allmocks.kinesisUtil.AssertCalled(t, "HandleMessage", record.Data, "", log)
// 	log.AssertCalled(t, "Errorf", "Error in closing the db connection, error:%v", errInDbClose)
// }
// func TestHandleTransaction_SuccessfullyInsertedErrorsInDB(t *testing.T) {

// 	streamMessage := `{
// 		"FirmID" : 123,
// 		"FirmAuthToken" :"T71Q5",
// 		"UserID" : 12,
// 		"Message" : "{\"Allocations\":[]}",
// 		"ActivityID" :"dsdafdd",
// 		"UserSessionToken" :"hgggdsfgds"
// 	}`

// 	record := kinesis.Record{
// 		Data: []byte(streamMessage),
// 	}

// 	var allmocks allMocks
// 	monkey.Patch(servicesession.SetUserSessionToken, func(firmToken string) error {
// 		return nil
// 	})

// 	da := database.DataAccessorInstance()
// 	monkey.PatchInstanceMethod(reflect.TypeOf(da), "Connect", func(_ *database.DataAccessor, firmAuthToken string) (err error) {
// 		return nil
// 	})

// 	monkey.Patch(persisttransaction.PersistTransactionsMaster, func(trs *models.Transactiontable, logger interfaces.ILogger,
// 		eventTypeID models.EventTypeID, sourceSystemName string, securityID models.SecurityID, orderQuantity float64,
// 		da interfaces.IDataAccessor, rawDataStr string, userID int, userName string, userSessionToken string, activityID string, businessDateTime string, sourceSystemReference string, transactionDateTimeUTC string) (int, error) {
// 		return 12, nil
// 	})

// 	var errorMessages []validator.ErrorMessage
// 	errorMessages = append(errorMessages, validator.ErrorMessage{Field: "F1", Message: []string{"E1"}})
// 	monkey.Patch(validator.Validate, func(data models.Transaction, _ interfaces.ILogger) []validator.ErrorMessage {
// 		return errorMessages
// 	})

// 	monkey.PatchInstanceMethod(reflect.TypeOf(da), "InsertValidationErrors", func(_ *database.DataAccessor, recordid int, validationErrors []string) error {
// 		return nil
// 	})

// 	monkey.PatchInstanceMethod(reflect.TypeOf(da), "Close", func(_ *database.DataAccessor) error {
// 		return nil
// 	})
// 	log := &allmocks.logger
// 	monkey.Patch(appcontext.GetLoggerWithCtx, func(ctx context.Context) interfaces.ILogger {
// 		return log
// 	})
// 	log.On("Infof", mock.Anything, mock.Anything)

// 	log.On("Errorf", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return()
// 	log.On("Errorf", mock.Anything, mock.Anything, mock.Anything).Return()
// 	allmocks.kinesisUtil.On("HandleMessage", mock.Anything, mock.Anything, mock.Anything)
// 	HandleTransaction(record, &allmocks.kinesisUtil, log)
// 	log.AssertCalled(t, "Infof", "[{\"Field\":\"F1\",\"Message\":[\"E1\"]},{\"Field\":\"Rejected Error Strings\",\"Message\":[\"\"]}]")
// 	log.AssertCalled(t, "Infof", "Success saving rejections for recordId:%d to db", 12)

// }

// //Function to test GetOrderQuntity when allocations are present
// func TestGetOrderQuntity(t *testing.T) {
// 	allocations := make([]models.Allocation, 0)

// 	portfolioID := models.PortfolioID(12)
// 	bookTypeID := models.BookTypeID(2)
// 	dealtSettleAmount := 1000.0

// 	allocation := models.Allocation{
// 		BookTypeID:        &bookTypeID,
// 		PortfolioID:       &portfolioID,
// 		DealtSettleAmount: &dealtSettleAmount,
// 	}
// 	allocations = append(allocations, allocation)
// 	transaction := models.Transaction{
// 		Allocations: allocations,
// 	}

// 	err := getOrderQuntity(transaction)

// 	assert.Equal(t, err, 1000.0)
// }

// func TestPanicInTransactionHandler(t *testing.T) {
// 	streamMessage := `{
// 		"FirmID" : 123,
// 		"FirmAuthToken" :"T71Q5",
// 		"UserID" : 12,
// 		"Message" : null,
// 		"ActivityID" :"dsdafdd",
// 		"UserSessionToken" :"hgggdsfgds"
// 	}`

// 	record := kinesis.Record{
// 		Data: []byte(streamMessage),
// 	}
// 	var allmocks allMocks
// 	log := &allmocks.logger
// 	monkey.Patch(appcontext.GetLoggerWithCtx, func(ctx context.Context) interfaces.ILogger {
// 		return log
// 	})
// 	log.On("Errorf", mock.Anything, mock.Anything).Return()
// 	log.On("Errorf", mock.Anything, mock.Anything, mock.Anything).Return()
// 	HandleTransaction(record, &allmocks.kinesisUtil, log)
// 	log.AssertExpectations(t)
// }
