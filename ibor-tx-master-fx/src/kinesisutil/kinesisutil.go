package kinesisutil

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/kinesis"
	"github.com/aws/aws-sdk-go/service/kinesis/kinesisiface"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-fx/src/interfaces"
)

// KinesisUtil type
type KinesisUtil struct {
	client      kinesisiface.KinesisAPI
	credentials interfaces.IAwsCreds
	session     interfaces.IAwsSession
	kinesis     interfaces.IKinesis
}

var (
	awsProxyURL = appconfig.AwsProxyURL
)

func (k *KinesisUtil) createClient(logger interfaces.ILogger) {
	var s *session.Session
	var awsAccessKey string
	var awsSecretKey string
	region := appconfig.AwsRegionName
	if !appconfig.IsDevMode {
		awsAccessKey = appconfig.AwsAccessKey
		awsSecretKey = appconfig.AwsSecretKey
		s = k.session.New(&aws.Config{
			// replace the credentials
			Credentials:      k.credentials.NewStaticCredentials(awsAccessKey, awsSecretKey, ""),
			S3ForcePathStyle: aws.Bool(true),
			Region:           &region,
		})
	} else {
		logger.Debug("Running locally with localstack")
		s = k.session.New(&aws.Config{
			// replace the credentials
			Credentials:      k.credentials.NewStaticCredentials("foo", "var", ""),
			S3ForcePathStyle: aws.Bool(true),
			Region:           &region,
			Endpoint:         aws.String(awsProxyURL),
		})
	}
	logger.Info("Kinesis Session has been created successfully.")
	k.client = k.kinesis.New(s)
}

// GetRecords for given shardIterator
func (k *KinesisUtil) GetRecords(shardIterator *string, logger interfaces.ILogger) (*kinesis.GetRecordsOutput, error) {

	// get records use shard iterator for making request
	records, err := k.client.GetRecords(&kinesis.GetRecordsInput{
		ShardIterator: shardIterator,
	})

	if err != nil {
		logger.Errorf("Failed to get records, error:%v", err)
	}
	return records, err
}

// GetShardIterator by shardId
func (k *KinesisUtil) GetShardIterator(shardID string, streamName string, startingSequenceNumber string, logger interfaces.ILogger) (*kinesis.GetShardIteratorOutput, error) {
	if k.client == nil {
		k.createClient(logger)
	}
	// retrieve iterator
	var iteratorOutput *kinesis.GetShardIteratorOutput
	var err error
	if startingSequenceNumber != "" {
		iteratorOutput, err = k.client.GetShardIterator(&kinesis.GetShardIteratorInput{
			ShardId:                aws.String(shardID),
			ShardIteratorType:      aws.String("AFTER_SEQUENCE_NUMBER"),
			StartingSequenceNumber: aws.String(startingSequenceNumber),
			StreamName:             aws.String(streamName),
		})
	} else {
		iteratorOutput, err = k.client.GetShardIterator(&kinesis.GetShardIteratorInput{
			ShardId:           aws.String(shardID),
			ShardIteratorType: aws.String("TRIM_HORIZON"),
			StreamName:        aws.String(streamName),
		})
	}
	if err != nil {
		logger.Errorf("Failed to get the shard iterator, error:%v", err)
	}
	return iteratorOutput, err
}

// InsertRecord puts a record into kenisis stream
func (k *KinesisUtil) InsertRecord(streamName string, key string, value []byte) (*kinesis.PutRecordOutput, error) {
	putRecordOutput, err := k.client.PutRecord(&kinesis.PutRecordInput{
		Data:         value,
		StreamName:   aws.String(streamName),
		PartitionKey: aws.String(key),
	})

	return putRecordOutput, err
}

// HandleMessage forwards the message to kinesis
func (k *KinesisUtil) HandleMessage(data []byte, streamName string, logger interfaces.ILogger) {
	if k.client == nil {
		k.createClient(logger)
	}
	i := 0
	for {
		retryStr := ""
		if i > 0 {
			retryStr = fmt.Sprintf("Retry:%d ", i)
		}
		logger.Infof("%sStarted inserting the record into kinesis.", retryStr)
		_, errInsertRecord := k.InsertRecord(streamName, "key123", data)
		if errInsertRecord != nil {
			i++
			logger.Errorf("%sError in kenisis PutRecord(), error: %v", retryStr, errInsertRecord)
		} else {
			logger.Infof("%sSuccessfully inserted record into kinesis.", retryStr)
			break
		}
	}
}
