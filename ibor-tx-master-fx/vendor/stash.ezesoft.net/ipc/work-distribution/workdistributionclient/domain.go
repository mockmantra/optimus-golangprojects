package workdistributionclient

import "github.com/go-openapi/strfmt"

const (
	Delete           = "DELETE"
	Post             = "POST"
	SuccessfulDelete = 204
)

type WorkerAssignments struct {
	Index       string
	Id          string    `json:"Id"`
	WorkerGroup string    `json:"WorkerGroup"`
	WorkType    string    `json:"WorkType"`
	WorkItems   WorkItems `json:"WorkItems"`
	TTL         string    `json:"TTL"`
}

type WorkMetadata interface{}

type Extra struct {
	AssignedOn strfmt.DateTime `json:"AssignedOn,omitempty"`
}

// used for work assignment API (aka "workers")
type WorkItem struct {
	WorkId   string       `json:"WorkId"`
	Metadata WorkMetadata `json:"Metadata"`
	Locks    []string     `json:"Locks,omitempty"`
	Extra    *Extra       `json:"Extra,omitempty"`
}
type WorkItems []WorkItem

type ClientStatus struct {
	Error    error
	Warning  string
	Message  string
	Failures int
}

// WorkWorkItem is used for work api (aka "work")
type WorkWorkItem struct {
	Enabled  bool         `json:"Enabled"`
	Id       string       `json:"Id"`
	Metadata WorkMetadata `json:"Metadata"`
}

// WorkWorkItems is used for work api (aka "work")
type WorkWorkItems []WorkWorkItem
