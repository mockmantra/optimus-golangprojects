package ezelogger

import (
	"context"
	"fmt"
	"log"
	"os"
	"regexp"
	"sort"
	"strings"
	"time"
)

var reNewlineReplacement = regexp.MustCompile(`\r?\n`)

// Loggable interface can be implemented and registered with logger by anyone who wants to automatically append
// its own message to log based on go context content
type Loggable interface {
	LogMessageFromContext(ctx context.Context) (string, bool)
}

type EzeLogger interface {
	// AddLoggable registers an instance implementing Loggable interface to allow logger to extract and log information from context
	AddLoggable(loggable Loggable)
	// Info logs a message with INFO level
	Info(a ...interface{})
	// Debug logs a message with DEBUG level
	Debug(a ...interface{})
	// Warn logs a message with WARN level
	Warn(a ...interface{})
	// Error logs a message with ERROR level
	Error(a ...interface{})
	// Infof logs a message with INFO level
	Infof(s string, v ...interface{})
	// Debugf logs a message with DEBUG level
	Debugf(s string, v ...interface{})
	// Warnf logs a message with WARN level
	Warnf(s string, v ...interface{})
	// Errorf logs a message with ERROR level
	Errorf(s string, v ...interface{})
}

// EzeLog struct represents logger object which is returned by GetLogger call
type EzeLog struct {
	infoLogger          *log.Logger
	warnLogger          *log.Logger
	errorLogger         *log.Logger
	debugLogger         *log.Logger
	appDomain           string
	ctx                 context.Context
	logTrace            bool
	loggableEntities    []Loggable
	messageLinesLimit   int
	messageSizeLimit    int
	replaceNewlines     bool
	replaceDoubleQuotes bool
	extraFields         map[string]interface{}
	sortExtraFields     bool
}

// GetLogger should be used by consumers of this package to create logger instance
func GetLogger(appdomain string) *EzeLog {
	logger := new(EzeLog)
	logger.infoLogger = log.New(os.Stdout, `Level="INFO", `, 0)
	logger.debugLogger = log.New(os.Stdout, `Level="DEBUG", `, 0)
	logger.warnLogger = log.New(os.Stdout, `Level="WARN", `, 0)
	logger.errorLogger = log.New(os.Stderr, `Level="ERROR", `, 0)
	logger.appDomain = appdomain
	logger.messageLinesLimit = -1
	logger.messageSizeLimit = 10240
	logger.replaceNewlines = false
	logger.replaceDoubleQuotes = false
	logger.sortExtraFields = false

	return logger
}

// CfgMessageLinesLimit (n) can be passed to Configure call to limit the number of line in the message being logged
func CfgMessageLinesLimit(n int) func(log *EzeLog) error {
	return func(log *EzeLog) error {
		log.messageLinesLimit = n
		return nil
	}
}

// CfgMessageSizeLimit (n) can be passed to Configure call to limit the size of the message being logged
func CfgMessageSizeLimit(n int) func(log *EzeLog) error {
	return func(log *EzeLog) error {
		log.messageSizeLimit = n
		return nil
	}
}

// CfgReplaceNewlines can be passed to Configure call to replace newline characters with \n sequence
func CfgReplaceNewlines(log *EzeLog) error {
	log.replaceNewlines = true
	return nil
}

// CfgReplaceDoubleQuotes can be passed to Configure call to replace double quotes with single quotes
func CfgReplaceDoubleQuotes(log *EzeLog) error {
	log.replaceDoubleQuotes = true
	return nil
}

// CfgSortExtraFields can be passed to Configure call to sort additional fields in increasing order
func CfgSortExtraFields(log *EzeLog) error {
	log.sortExtraFields = true
	return nil
}

// Configure can be called on the logger instance to override logging defaults
func (logger *EzeLog) Configure(cfgs ...func(log *EzeLog) error) *EzeLog {
	for _, cfg := range cfgs {
		err := cfg(logger)
		if err != nil {
			// log error
			logger.Warn("Failed to configure option")
		}
	}
	return logger
}

// AddLoggable registers an instance implementing Loggable interface to allow logger to extract and log information from context
func (logger *EzeLog) AddLoggable(loggable Loggable) {
	logger.loggableEntities = append(logger.loggableEntities, loggable)
}

// WithContext attaches context to the log message so that logger can extract and log additional contextual information
func (logger *EzeLog) WithContext(ctx context.Context) *EzeLog {
	if ctx == nil {
		return logger
	}

	newlogger := new(EzeLog)
	*newlogger = *logger
	newlogger.ctx = ctx
	newlogger.logTrace = false
	return newlogger
}

// WithTracer enables logger to log any tracing information, if available in current context
func (logger *EzeLog) WithTracer() *EzeLog {
	newlogger := new(EzeLog)
	*newlogger = *logger
	newlogger.logTrace = true
	return newlogger
}

// WithFields passes logger a list of name/value pairs to be logged
func (logger *EzeLog) WithFields(fields map[string]interface{}) *EzeLog {
	newlogger := new(EzeLog)
	*newlogger = *logger
	if newlogger.extraFields == nil {
		newlogger.extraFields = make(map[string]interface{}, len(fields))
	}
	for k, v := range fields {
		newlogger.extraFields[k] = v
	}
	return newlogger
}

// Info logs a message with INFO level
func (logger *EzeLog) Info(a ...interface{}) {
	logger.infoLogger.Println(logger.createLogMessage(logger.appDomain, logger.logTrace, a...))
}

// Debug logs a message with DEBUG level
func (logger *EzeLog) Debug(a ...interface{}) {
	logger.debugLogger.Println(logger.createLogMessage(logger.appDomain, logger.logTrace, a...))
}

// Warn logs a message with WARN level
func (logger *EzeLog) Warn(a ...interface{}) {
	logger.warnLogger.Println(logger.createLogMessage(logger.appDomain, logger.logTrace, a...))
}

// Error logs a message with ERROR level
func (logger *EzeLog) Error(a ...interface{}) {
	logger.errorLogger.Println(logger.createLogMessage(logger.appDomain, logger.logTrace, a...))
}

// Infof logs a message with INFO level
func (logger *EzeLog) Infof(s string, v ...interface{}) {
	a := fmt.Sprintf(s, v...)
	logger.infoLogger.Println(logger.createLogMessage(logger.appDomain, logger.logTrace, a))
}

// Debugf logs a message with DEBUG level
func (logger *EzeLog) Debugf(s string, v ...interface{}) {
	a := fmt.Sprintf(s, v...)
	logger.debugLogger.Println(logger.createLogMessage(logger.appDomain, logger.logTrace, a))
}

// Warnf logs a message with WARN level
func (logger *EzeLog) Warnf(s string, v ...interface{}) {
	a := fmt.Sprintf(s, v...)
	logger.warnLogger.Println(logger.createLogMessage(logger.appDomain, logger.logTrace, a))
}

// Errorf logs a message with ERROR level
func (logger *EzeLog) Errorf(s string, v ...interface{}) {
	a := fmt.Sprintf(s, v...)
	logger.errorLogger.Println(logger.createLogMessage(logger.appDomain, logger.logTrace, a))
}

func (logger *EzeLog) createLogMessage(appDomain string, logTrace bool, a ...interface{}) string {
	msg := fmt.Sprintf(`Date="%s", AppDomain="%s"`, getDateString(), appDomain)
	s := fmt.Sprint(a...)

	for _, v := range logger.loggableEntities {
		if s, ok := v.LogMessageFromContext(logger.ctx); ok {
			msg = fmt.Sprintf(`%s, %s`, msg, s)

		}
	}

	if logTrace {
		if trc, ok := tracerFromContext(logger.ctx); ok {
			s = fmt.Sprintf("%s: %s", trc.Message, s)
		}
	}

	if s != "" {
		splitChar := "\n"
		if logger.replaceNewlines {
			splitChar = `\n`
		}
		s = reNewlineReplacement.ReplaceAllLiteralString(s, splitChar)
		if logger.messageLinesLimit >= 0 {
			msgLines := strings.SplitN(s, splitChar, logger.messageLinesLimit+1)
			numLines := len(msgLines)
			// In the last element of the slice there are any remaining lines from the original string.
			// Remove them from the join, unless it's already a single line
			if logger.messageLinesLimit >= 0 && numLines > logger.messageLinesLimit {
				numLines = logger.messageLinesLimit
			}
			s = strings.Join(msgLines[:numLines], splitChar)
		}
		// limit length of the message to 1024B
		// and escape double quotes
		if logger.replaceDoubleQuotes {
			s = strings.Replace(s, `"`, `'`, -1)
		}
		msg = fmt.Sprintf(`%s, Message="%.*s"`, msg, logger.messageSizeLimit, s)
	}

	if logger.extraFields != nil {
		var dataListString string

		var fieldNames []string
		for fn := range logger.extraFields {
			fieldNames = append(fieldNames, fn)
		}

		if logger.sortExtraFields {
			sort.Strings(fieldNames)
		}

		for _, k := range fieldNames {
			kvpString := fmt.Sprintf(`, %s="%v"`, k, logger.extraFields[k])
			dataListString += kvpString
		}

		msg = fmt.Sprintf("%s%s", msg, dataListString)
	}
	return msg
}

func getDateString() string {
	t := time.Now()
	date := t.Format("2006-01-02 15:04:05.000")
	date = strings.Replace(date, ".", ",", 1)

	return date
}
