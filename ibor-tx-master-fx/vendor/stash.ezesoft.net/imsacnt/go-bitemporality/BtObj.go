package bitemporality

import "github.com/jinzhu/gorm"

// BtObj contains all the fields required for using the package
type BtObj struct {
	TableName  string                 // Name of the table to run this package on
	Database   *gorm.DB               // Instance of gorm based database object
	TableModel interface{}            // empty instance of table model
	UniqueKey  map[string]interface{} // map of the unique keys for the records
}
