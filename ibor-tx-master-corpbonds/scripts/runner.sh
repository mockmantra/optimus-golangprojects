#!/bin/bash
set -e 

src="/go/src/stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src"
reportDir="reports"

export PATH=/usr/local/go/bin:$PATH
export GOPATH=/go
export GOBIN=/go/bin
export PATH=$GOBIN:$PATH

# Clean up and recreate report directory
rm -rf "$reportDir"
mkdir -p "$reportDir"

# Generate lint report
/usr/local/go/bin/go get github.com/axw/gocov/gocov github.com/AlekSi/gocov-xml gopkg.in/alecthomas/gometalinter.v2 github.com/jstemmer/go-junit-report;
gometalinter.v2 --install ;

gometalinter.v2 --deadline 5m --checkstyle $src/... > $reportDir/lint.xml || true ;

export APP_ENV=local;
export IMS_BASE_URL="http://aecastle01plt01.awsdev.ezesoftcloud.com:8001";
export IMS_CORPBOND_SVC_USER="IborCrpBndTMSvcUser";
export AWS_REGION="us-east-1";
export CONSUL_URL="http://aecastle01ran01.awsdev.ezesoftcloud.com:8500";
export CL_DPERMSRESTURI="aedevauth01dperm.awsdev.ezesoftcloud.com";

CGO_ENABLED=0

echo "~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~"
echo "Starting UNIT TESTS!"
echo "~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~"

/usr/local/go/bin/go test -v -coverprofile=$reportDir/coverage.out stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/... | /go/bin/go-junit-report > $reportDir/jUnitReport.xml;
gocov convert $reportDir/coverage.out | gocov-xml > $reportDir/coverage.xml ;

chmod -R a+rwx $reportDir ;

# Making the file paths in the reports relative
# Workaround for a bug in gometalinter where the "src" path can be duplicated
sed -i -e "s|../../../../../../go/src/stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/||g" "$reportDir/lint.xml"
sed -i -e "s|../../../../../../go/src/stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/||g" "$reportDir/coverage.xml"

# Workaround for a bug in gometalinter where the "src" path can be duplicated
#sed -i -e "s|$src/||g" "$reportDir/lint.xml"

echo "~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~"
echo "Finished executing unit tests"
echo "~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~"