package models

import (
	"time"
)

const (
	// ConfigurationTableName is the name of the table within the database
	ImpactTransactionTableName = "Ibor_ImpactTransaction"
)

// ConfigurationTable represents the table present in the database for storing closeout configurations
type ImpactTransactionTable struct {
	ImpactTransactionId   int       `gorm:"column:id;primary_key;AUTO_INCREMENT"`
	ValidFromUTC          time.Time `gorm:"column:ValidFromUTC"`
	ValidToUTC            time.Time `gorm:"column:ValidToUTC;default:null"`
	IsDeleted             bool      `gorm:"column:IsDeleted"`
	SourceSystemName      string    `gorm:"column:SourceSystemName"`
	SourceSystemReference string    `gorm:"column:SourceSystemReference"`
	ActivityId            string    `gorm:"column:ActivityId"`
	TransactionJson       []byte    `gorm:"column:TransactionJson"`
}

// TableName ...
func (ImpactTransactionTable) TableName() string {
	return ImpactTransactionTableName
}
