package appconfig

import (
	"context"
	"os"
	"strings"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/logger"
)

var (
	// BaseURI is the base URI of the corresponding IMS environment
	BaseURI string
	// IsDevMode should be set to true when running in dev env
	IsDevMode bool
	// RunEnv gives the current environment
	RunEnv string
	// AwsRegionName represents the AwsRegion
	AwsRegionName string
	// DbSchemaTemplate is aws secret key
	DbSchemaTemplate string
	// DbConnectAttempts is db retry attempts
	DbConnectAttempts int
)

func init() {
	var cx context.Context
	loggerWithCtx := logger.Log.WithContext(cx)

	DbSchemaTemplate = "Eclipse_%s"
	DbConnectAttempts = 3

	bu := os.Getenv("IMS_BASE_URL")
	if bu == "" {
		loggerWithCtx.Debugf("Please, set IMS_BASE_URL environment variable")
	}
	BaseURI = bu

	ae := os.Getenv("APP_ENV")
	if strings.EqualFold(ae, "local") {
		loggerWithCtx.Debugf("IN DEV MODE")
		IsDevMode = true
	}
	RunEnv = ae

	AwsRegionName = os.Getenv("AWS_REGION")
	if len(AwsRegionName) == 0 {
		loggerWithCtx.Debugf("AWS_REGION variable is not set")
	}
}
