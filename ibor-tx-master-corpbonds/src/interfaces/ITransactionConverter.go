package interfaces

import (
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/converter/models"
)

// ITransactionConverter ..
type ITransactionConverter interface {
	ConvertToImpactGeneratorTransaction(*models.Transaction) *models.ImpactTransaction
}
