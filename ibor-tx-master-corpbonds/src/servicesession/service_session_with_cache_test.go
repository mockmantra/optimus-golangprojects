package servicesession

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

// When no token set...should return false
func TestServiceSessionWithCache_isTokenValid_whenNoTokenSet_shouldBeFalse(t *testing.T) {
	isValid := isTokenValid()
	assert.Equal(t, false, isValid)
	assert.Equal(t, false, tokenTimeStamp.IsZero()) // time should be set now
}

// When last token is set more than the allowed time ago...should return false
func TestServiceSessionWithCache_isTokenValid_whenTokenSetMoreThanMinimumDurationAgo_shouldBeFalse(t *testing.T) {
	oldTime := time.Now().Add(time.Duration(-5) * time.Minute) // last token 5 min old
	tokenTimeStamp = oldTime
	isValid := isTokenValid()
	assert.Equal(t, false, isValid)
}

// // When EMPTY token is set within the allowed time...should return false
// func TestServiceSessionWithCache_isTokenValid_whenEmptyTokenSetWithinMinimumDurationAgo_shouldBeFalse(t *testing.T) {
// 	tokenTimeStamp = time.Now().Add(time.Duration(-1) * time.Minute) // last token 1 min old
// 	isValid := isTokenValid()
// 	assert.Equal(t, false, isValid)
// }

// When valid token is set within the allowed time...should return true
func TestServiceSessionWithCache_isTokenValid_whenValidTokenSetWithinMinimumDurationAgo_shouldBeTrue(t *testing.T) {
	tokenTimeStamp = time.Now().Add(time.Duration(-1) * time.Minute) // last token 1 min old
	userSessionToken = "blabla"
	isValid := isTokenValid()
	assert.Equal(t, true, isValid)
}

// // When old token is still valid
// func TestServiceSessionWithCache_SetUserSessionToken_whenPreviousTokenIsValid_shouldReturnNoError(t *testing.T) {
// 	tokenTimeStamp = time.Now().Add(time.Duration(-1) * time.Minute) // valid duration
// 	userSessionToken = "blabla"                                      // valid token
// 	err := SetUserSessionToken(123)
// 	assert.Nil(t, err)
// }

// When old token expired...but firmId is bad to create new token
func TestServiceSessionWithCache_SetUserSessionToken_whenPreviousTokenIsNotValidAndFirmIdIsInvalid_shouldReturnError(t *testing.T) {
	tokenTimeStamp = time.Now().Add(time.Duration(-10) * time.Minute) // in-valid duration..more than 10 mins ago
	userSessionToken = "blabla"                                       // valid token
	err := SetUserSessionToken("-41")
	assert.Contains(t, err.Error(), "dperms response body had no glx2 token")
}

// When old token expired...should create a new token
func TestServiceSessionWithCache_SetUserSessionToken_whenPreviousTokenIsNotValidAndCreatesNewTokenSuccessfuly_shouldReturnNoError(t *testing.T) {
	oldTime := time.Now().Add(time.Duration(-10) * time.Minute) // in-valid duration..more than 10 mins ago
	tokenTimeStamp = oldTime
	userSessionToken = "blabla" // valid token

	GetServiceTokenFunc = func(firm string) (string, error) {
		return "tbzqk", nil
	}

	err := SetUserSessionToken("T3335")
	assert.NotEqual(t, userSessionToken, "blabla")                // should have a new token
	assert.NotEqual(t, tokenTimeStamp.String(), oldTime.String()) // should have a new time stamp
	assert.Nil(t, err)                                            // nil error
}
