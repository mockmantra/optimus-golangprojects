package handler

import (
	"context"
	"encoding/json"
	"runtime/debug"
	"time"

	utilsModels "stash.ezesoft.net/imsacnt/accounting-utils/models"
	"stash.ezesoft.net/imsacnt/accounting-utils/rmqhandler"
	appconfig "stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/converter"
	converterModel "stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/converter/models"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/database"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/httputil"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/interfaces"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/models"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/persisttransaction"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/servicesession"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/svcloginconfig"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/validator"
	"stash.ezesoft.net/ipc/ezeutils"
)

const rmqExchange = "Eze.Requests"
const rmqQueueTemplate = "Eze.Ibor.Closeout.ImpactTransactionTopic"
const transactionMasterService = "ibor-tx-master-corpbonds"

// HandleTransaction do the full validations and generates the impact.
func HandleTransaction(ctx context.Context, message []byte, userSession *ezeutils.UserSession, log interfaces.ILogger) {
	defer func() {
		if p := recover(); p != nil {
			log.Errorf("Error occured while processing transaction: %s", string(message))
			log.Errorf("Recover on HandleTransaction %s: %s", p, string(debug.Stack()))
		}
	}()
	activityID, _ := ezeutils.FromContext(ctx, ezeutils.XRequestIdKey)

	// setting the context for the http request
	httputil.Ctx = ctx
	var transaction validator.Transaction
	err := json.Unmarshal(message, &transaction)
	if err == nil {
		var tokenErr error
		// set the user session token variable in httputil.go
		if !appconfig.IsDevMode {
			tokenErr = servicesession.SetUserSessionToken(userSession.FirmAuthToken)
		} else {
			log.Debug("Detected DEV Mode - Using hard coded session token...")
			httputil.UserSessionToken = userSession.UserSessionToken
		}
		if tokenErr == nil {
			// persist validation msg and errors to db
			var dbData models.Transactiontable
			da := database.DataAccessorInstance()
			// code to insert the message to db
			errDbConnect := da.Connect(userSession.FirmAuthToken)
			if errDbConnect == nil {
				recordID, errMsg := persisttransaction.PersistTransactionsMaster(
					&dbData, log, transaction.EventTypeID, transaction.SourceSystemName,
					transaction.SecurityID, transaction.OrderQuantity, da,
					string(message), userSession.UserID,
					userSession.UserName, userSession.UserSessionToken,
					activityID, string(transaction.BusinessDateTimeUTC),
					string(transaction.SourceSystemReference), string(transaction.TransactionDateTimeUTC))

				if errMsg == nil {
					// validate transaction message
					errorStrings := validator.Validate(transaction, log)
					// If there are any validation errors, then log to the database.
					if len(errorStrings) > 0 {
						var errStringArray []string
						for _, errString := range errorStrings {
							errStringArray = append(errStringArray, errString.Message...)
						}
						errInsertion := da.InsertValidationErrors(recordID, errStringArray)
						if errInsertion != nil {
							log.Errorf("Error in saving rejections for recordId:%d to db, error:%v", recordID, errInsertion)
						} else {
							log.Infof("Success saving rejections for recordId:%d to db", recordID)
							errorStrings = append(errorStrings, validator.ErrorMessage{Field: "Rejected Error Strings", Message: []string{""}})
							e, _ := json.Marshal(errorStrings)
							log.Infof(string(e))
						}
					} else {
						var txns converterModel.Transaction
						if errUnmarshal := json.Unmarshal(message, &txns); errUnmarshal == nil {
							tc := converter.TransactionConverterInstance(ctx, log.WithContext(ctx))
							impactTransaction := tc.ConvertToImpactGeneratorTransaction(&txns)
							err := persistImpactTransaction(userSession.FirmAuthToken, da, log, activityID, impactTransaction)
							impactTransactionJSON, err := json.Marshal(impactTransaction)
							rmqMessage := utilsModels.RmqMessage{
								Message:       impactTransactionJSON,
								FirmID:        userSession.FirmID,
								ActivityID:    activityID,
								UserID:        userSession.UserID,
								UserName:      userSession.UserName,
								FirmAuthToken: userSession.FirmAuthToken,
							}
							rmqMessageJSON, err := json.Marshal(rmqMessage)

							if err == nil {
								rc := rmqhandler.RmqConnectorInstance(log.WithContext(ctx), svcloginconfig.RmqVars, transactionMasterService)
								statistics := make(map[string]interface{})
								err = rc.Publish(rmqMessageJSON, rmqQueueTemplate, userSession.FirmAuthToken, rmqExchange, statistics)
								if err != nil {
									log.Errorf("Error publishing Impact Transactions to queue on rmq server- %v", err)
								}
							} else {
								log.Errorf("Error in serializing ImpactTransaction:%s, error:%v", impactTransaction, err)
							}
						} else {
							log.Errorf("Error in deserializing to converter Transaction. error:%v", err)
						}
					}
				} else {
					log.Errorf("Error in saving transaction to db, error:%v", errMsg)
				}
				errDbClose := da.Close()
				if errDbClose != nil {
					log.Errorf("Error in closing the db connection, error:%v", errDbClose)
				}
			} else {
				log.Errorf("Connection to db failed, error:%v", errDbConnect)
			}
		} else {
			log.Errorf("Failed to get user session token for FirmId: %d, UserName: %s, ActivityId: %s, Error: %v", userSession.FirmID, userSession.UserName, activityID, tokenErr)
		}
	} else {
		log.Errorf("Error in Unmarshal Message: %v", err)
	}
}

func persistImpactTransaction(firmAuthToken string, da *database.DataAccessor, log interfaces.ILogger, activityID string, impactTransaction *converterModel.ImpactTransaction) (err error) {

	failPersistMessage := "Unable to persist Impact transaction to the database."

	jsonData, err := json.Marshal(impactTransaction)
	if err != nil {
		log.Warnf("%s There was an error parsing ImpactTransaction object into Json: %v", failPersistMessage, err)
		return
	}

	transactionTimeUTC, err := time.Parse(time.RFC3339, string(impactTransaction.TransactionDateTimeUTC))
	if err != nil {
		log.Warnf("%s There was an error parsing TransactionTimeUTC: %v", failPersistMessage, err)
		return
	}

	impactTransactionTable := models.ImpactTransactionTable{
		ValidFromUTC:          transactionTimeUTC,
		IsDeleted:             false,
		SourceSystemName:      *impactTransaction.SourceSystemName,
		SourceSystemReference: *impactTransaction.SourceSystemReference,
		ActivityId:            activityID,
		TransactionJson:       jsonData,
	}

	err = da.InsertImpactTransaction(&impactTransactionTable, transactionTimeUTC)

	if err != nil {
		log.Warnf("%s Message: %v", failPersistMessage, err)
		return
	}

	return nil
}
