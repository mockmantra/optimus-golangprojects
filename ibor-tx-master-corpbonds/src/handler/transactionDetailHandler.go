package handler

import (
	"context"
	"runtime/debug"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/database"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/interfaces"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/models"
	"stash.ezesoft.net/ipc/ezeutils"
)

func HandleTransactionDetails(ctx context.Context, filterDetails []*models.TransactionDetails, userSession *ezeutils.UserSession, log interfaces.ILogger) ([]*models.TransactionDetailsResponse, error) {
	defer func() {
		if p := recover(); p != nil {
			log.Errorf("Error occured while getting transaction details")
			log.Errorf("Recover on HandleTransactionDetails %s: %s", p, string(debug.Stack()))
		}
	}()

	var transDetails []*models.TransactionDetailsResponse
	var errGet error

	da := database.DataAccessorInstance()

	errDbConnect := da.Connect(userSession.FirmAuthToken)
	defer func() {
		err := da.Close()
		if err != nil {
			log.Errorf("Error in closing the db connection, error:%v", err)
		}
	}()
	if errDbConnect == nil {

		transDetails, errGet = da.GetTransactionDetails(filterDetails, log)

		if errGet != nil {
			log.Errorf("Error in fetching transaction details from db, error:%v", errGet)
			return nil, errGet
		}
		errDbClose := da.Close()
		if errDbClose != nil {
			log.Errorf("Error in closing the db connection, error:%v", errDbClose)
		}
	} else {
		log.Errorf("Connection to db failed, error:%v", errDbConnect)
	}
	return transDetails, nil
}
