package utils

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRounding_Ceil(t *testing.T) {
	expectedOutput := 2.2457
	ouput := Round(PrecisionRound(float64(2.24564736), 0.00005), 0.5, 4)
	assert.Equal(t, ouput, expectedOutput)
}
func TestRounding_Floor(t *testing.T) {
	expectedOutput := 2.2454
	ouput := Round(PrecisionRound(float64(2.24544444), 0.00002), 0.5, 4)
	assert.Equal(t, ouput, expectedOutput)
}

func TestRoundingNegative_Ceil(t *testing.T) {
	expectedOutput := -2.2457
	ouput := Round(PrecisionRound(float64(-2.24564736), 0.00006), -0.6, 4)
	assert.Equal(t, ouput, expectedOutput)
}

func TestRoundingNegative_Floor(t *testing.T) {
	expectedOutput := -2.2458
	ouput := Round(PrecisionRound(float64(-2.24578899), 0.00005), 0.5, 4)
	assert.Equal(t, ouput, expectedOutput)
}
