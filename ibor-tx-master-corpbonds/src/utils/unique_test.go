package utils

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUnique(t *testing.T) {
	input := []int{1, 1, 2, 3, 4, 4}
	expectedOutput := []int{1, 2, 3, 4}
	ouput := Unique(input)
	assert.Equal(t, ouput, expectedOutput)
}
