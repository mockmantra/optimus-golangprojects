package validator

import (
	"fmt"
	"strings"
)

// TimeZoneID type
type TimeZoneID int

// Validate validates the fields
func (value TimeZoneID) Validate(transaction Transaction, fieldName string) error {
	_, err := NewTimezoneRequest(int(value))
	if err != nil {
		if strings.Contains(err.Error(), "error:404") {
			return fmt.Errorf("Invalid %s. TimeZoneId: %d does not exist", fieldName, value)
		}
		return fmt.Errorf("%s: Service lookup failed", fieldName)
	}
	return nil
}
