package validator

import (
	"fmt"
	"time"
)

// TransactionDate field
type TransactionDate string

// Validate validates the SettleDate and TradeDate field
func (value TransactionDate) Validate(transaction Transaction, fieldName string) error {
	settleDate, err := time.Parse(time.RFC3339Nano, string(transaction.SettleDate))
	tradeDate, err1 := time.Parse(time.RFC3339Nano, string(transaction.TradeDate))
	if err == nil && err1 == nil {
		if tradeDate.After(settleDate) {
			return fmt.Errorf("Invalid %s. TradeDate must be before SettleDate. Provided TradeDate: %s, SettleDate: %s", fieldName, transaction.TradeDate, transaction.SettleDate)
		}
	} else {
		return fmt.Errorf("Invalid %s. Invalid format", fieldName)
	}
	return nil
}
