package validator

import (
	"fmt"
	"reflect"
)

// FeeCharge type
type FeeCharge float64

// Validate validates the field
func (value FeeCharge) Validate(transaction Transaction, fieldName string, allocationIndex int, nestedIndex int) error {
	if !reflect.ValueOf(transaction.Allocations[allocationIndex].ExecQuantity).IsNil() && int(*transaction.Allocations[allocationIndex].ExecQuantity) > 0 {
		feeCharges := *transaction.Allocations[allocationIndex].FeeCharges
		if !reflect.ValueOf(feeCharges[nestedIndex].FeeChargeID).IsNil() {
			return nil
		}
		return fmt.Errorf("Error in allocation portfolioID %d. Invalid FeeCharge: Missing FeeChargeID for FeeCharge Value %d", int(*transaction.Allocations[allocationIndex].PortfolioID), int(*feeCharges[nestedIndex].FeeCharge))
	}
	return fmt.Errorf("Invalid FeeCharge for allocation portfolioID %d. Expected : ExecQuantity not valid", int(*transaction.Allocations[allocationIndex].PortfolioID))
}
