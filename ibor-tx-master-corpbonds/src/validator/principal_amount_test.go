package validator

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPrincipalAmount_WhenExecQuantityIsNotGreaterThanZero(t *testing.T) {
	var principalAmount PrincipalAmount = 15.0123
	var principalJSON = `{
		"Allocations": [
		  {
			"BookTypeId": 1,
			"Quantity":   200,
			"PortfolioId": 2,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12,
			"SettlePrice":12.98034,
			"ExecQuantity":0
		  },
		  {
			"BookTypeId": 1,
			"Quantity":   200,
			"PortfolioId": 2,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12,
			"SettlePrice":12,
			"ExecQuantity":0
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(principalJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	fieldName := "PricipalAmount"
	err := principalAmount.Validate(testTransaction, fieldName, 0)

	assert.EqualError(t, err, fmt.Sprintf("Error in allocation PortfolioID %d. Invalid %s : ExecQuantity not valid", *testTransaction.Allocations[0].PortfolioID, fieldName))
}
func TestPrincipalAmount_WhenPricipalAmountIsNotEqualToProductOfSettlePriceAndExecQuantiy(t *testing.T) {
	var principalAmount PrincipalAmount = 15.0123
	var prinicipalJSON = `{
		"Allocations": [
		  {
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity":12,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12,
			"SettlePrice":12,
			"PortfolioID" :11
		  },
		  {
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity":12,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12,
			"SettlePrice":12
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(prinicipalJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	fieldName := "PricipalAmount"
	err := principalAmount.Validate(testTransaction, fieldName, 0)
	assert.EqualError(t, err, "Error in allocation PortfolioID 11. Invalid PricipalAmount - Expected value :144.000000, Provided value : 15.012300")
}
func TestPrincipalAmount_WhenSettlePriceIsMissing(t *testing.T) {
	var principalAmount PrincipalAmount = 144
	var prinicipalJSON = `{
		"Allocations": [
		  {
			"BookTypeId": 1,
			"PortfolioId": 2,
			"Quantity":   200,
			"ExecQuantity":12,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12
		  },
		  {
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity":12,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12,
			"SettlePrice":12
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(prinicipalJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	fieldName := "PricipalAmount"
	err := principalAmount.Validate(testTransaction, fieldName, 0)
	assert.EqualError(t, err, fmt.Sprintf("Error in allocation PortfolioID %d. Invalid %s : Settle Price not provided", *testTransaction.Allocations[0].PortfolioID, fieldName))
}

func TestPrincipalAmount_WhenPricipalAmountIsEqualToProductOfSettlePriceAndExecQuantiy(t *testing.T) {
	var principalAmount PrincipalAmount = 155.7641
	var prinicipalJSON = `{
		"Allocations": [
		  {
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity":12,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12,
			"SettlePrice":12.98034,
			"PortfolioID" : 12
		  },
		  {
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity":12,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12,
			"SettlePrice":12
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(prinicipalJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	fieldName := "PricipalAmount"
	err := principalAmount.Validate(testTransaction, fieldName, 0)
	assert.Nil(t, err)
}

func TestPrincipalAmount_WhenPricipalAmountIsEqualToProductOfSettlePriceAndExecQuantiyWithPrecision(t *testing.T) {
	var principalAmount PrincipalAmount = 202212.00025
	var prinicipalJSON = `{
		"Allocations": [
		  {
			"BookTypeId": 1,
			"Quantity":   600,
			"ExecQuantity":6500,
			"Fees":            12,
			"PrincipalAmount": 202212.00025,
			"Commission":      12,
			"SettlePrice":31.1095385,
			"PortfolioID" : 12
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(prinicipalJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	fieldName := "PricipalAmount"
	err := principalAmount.Validate(testTransaction, fieldName, 0)
	assert.Nil(t, err)
}
