package validator

import (
	"fmt"
	"strconv"
	"strings"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/logger"
)

// SourceSystemReference field
type SourceSystemReference string

// Validate validates the fields
func (value SourceSystemReference) Validate(transaction Transaction, fieldName string) error {
	if strings.ToUpper(transaction.SourceSystemName) == "IMS-TRADING" {
		id, err := strconv.Atoi(string(value))
		if err != nil {
			return fmt.Errorf("Invalid %s. Not a valid integer value: %s", fieldName, value)
		}

		logger.Log.Infof("Validating SourceSystemReference. Id:  %d", id)
		orderResponse, err := NewOrdersRequest(id)
		if err == nil {
			if len(orderResponse.Items) == 0 {
				return fmt.Errorf("Invalid %s. There are no items", fieldName)
			}
		} else {
			if strings.Contains(err.Error(), "error:404") {
				return fmt.Errorf("Invalid %s. OrderId: %d does not exist", fieldName, id)
			}
			return fmt.Errorf("%s: Service lookup failed", fieldName)
		}
	}
	return nil
}
