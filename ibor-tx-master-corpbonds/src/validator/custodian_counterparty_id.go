package validator

import (
	"encoding/json"
	"fmt"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/httputil"
)

// CustodianCounterpartyAccountID field
type CustodianCounterpartyAccountID int

// CustodianAccount type
type CustodianAccount struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// GraphQLCustodianResponse type
type GraphQLCustodianResponse struct {
	Data struct {
		CustodianAccounts []CustodianAccount `json:"custodianAccounts"`
	} `json:"data"`
}

const custodianGraphQL = "coredata/v2/graphql?query={custodianAccounts(ids:[%d]){id,name}}"

// Validate validates the field
func (value CustodianCounterpartyAccountID) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	if transaction.Allocations[allocationIndex].IsFinalized {
		if *transaction.Allocations[allocationIndex].BookTypeID == BookTypeLocation {
			bodyBytes, err := httputil.NewRequest("GET", fmt.Sprintf(custodianGraphQL, value), nil)
			var custodianResponse GraphQLCustodianResponse
			if err == nil {
				unmarshalErr := json.Unmarshal(bodyBytes, &custodianResponse)
				if unmarshalErr == nil {
					if len(custodianResponse.Data.CustodianAccounts) > 0 {
						return nil
					}
					return fmt.Errorf("Error in allocation PortfolioID %d. Invalid %s : %d does not exist", int(*transaction.Allocations[allocationIndex].PortfolioID), fieldName, int(value))
				}
				return fmt.Errorf("Error in portfolioID %d. CustodianCounterpartyAccountID : Unmarshalling error", int(*transaction.Allocations[allocationIndex].PortfolioID))
			}
			return fmt.Errorf("Error in portfolioID %d. %s : Service lookup failed", int(*transaction.Allocations[allocationIndex].PortfolioID), fieldName)
		}
		return fmt.Errorf("Error in allocation portfolioID %d. Invalid %s : Expected BookType %d (for Location), Provided BookType %d", int(*transaction.Allocations[allocationIndex].PortfolioID), fieldName, BookTypeLocation, int(*transaction.Allocations[allocationIndex].BookTypeID))
	}
	return nil
}
