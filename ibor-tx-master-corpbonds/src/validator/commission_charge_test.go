package validator

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

var commissionChargeTestJSON = `{
	"Allocations": [
		{
		  "ExecQuantity": 100,
		  "Commission": 10,
		  "CommissionCharges": [
			{
			  "CommissionCharge": 10,
			  "CommissionChargeId": 22
			}
		  ]
		},
		{
			"PortfolioID": 11,
			"Commission": 10,
			"CommissionCharges": [
			  {
				"CommissionCharge": 10,
				"CommissionChargeId": 22
			  }
			]
		},
		{
			"ExecQuantity": 100,
			"PortfolioID": 12,
			"Commission": 12,
			"CommissionCharges": [
			  {
				"CommissionCharge": 10
			  }
			]
		}
	]
}`

func TestCommissionChargeValid(t *testing.T) {
	var testTransaction Transaction
	unmarhsalErr := json.Unmarshal([]byte(commissionChargeTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalErr)
	commCharges := *testTransaction.Allocations[0].CommissionCharges
	err := commCharges[0].CommissionCharge.Validate(testTransaction, "CommissionCharge", 0, 0)
	assert.Nil(t, err)
}

func TestCommissionChargeInvalidExecQuantity(t *testing.T) {
	var testTransaction Transaction
	unmarhsalErr := json.Unmarshal([]byte(commissionChargeTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalErr)
	commCharges := *testTransaction.Allocations[0].CommissionCharges
	err := commCharges[0].CommissionCharge.Validate(testTransaction, "CommissionCharge", 1, 0)
	assert.Equal(t, err, fmt.Errorf("Error in allocation portfolioID %d. Invalid CommissionCharge: ExecQuantity not valid", int(*testTransaction.Allocations[1].PortfolioID)))
}

func TestCommissionChargeWithoutCommissionChargeId(t *testing.T) {
	var testTransaction Transaction
	unmarhsalErr := json.Unmarshal([]byte(commissionChargeTestJSON), &testTransaction)
	assert.Nil(t, unmarhsalErr)
	commCharges := *testTransaction.Allocations[0].CommissionCharges
	err := commCharges[0].CommissionCharge.Validate(testTransaction, "CommissionCharge", 2, 0)
	assert.Equal(t, err, fmt.Errorf("Error in allocation portfolioID %d. Invalid %s: Missing CommissionChargeID for Commission Value %d", int(*testTransaction.Allocations[2].PortfolioID), "CommissionCharge", int(*commCharges[0].CommissionCharge)))
}
