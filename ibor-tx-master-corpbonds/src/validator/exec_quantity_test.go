package validator

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

var execQuantityTestJSON = `{
	"Allocations": [
	  {
			"PortfolioId" :12,
		"RouteId": 1,
		"ExecQuantity": 100,
		"Quantity": 200
	  },
	  {
			"PortfolioId" :13,
		"ExecQuantity": 100,
		"Quantity": 200
	  },
	  {
			"PortfolioId" :14,
		"RouteId": 1,
		"ExecQuantity": 300,
		"Quantity": 200
	  }
	]
  }`

func TestExecQuantityValid(t *testing.T) {
	var testTransaction Transaction
	unMarshalErr := json.Unmarshal([]byte(execQuantityTestJSON), &testTransaction)
	assert.Nil(t, unMarshalErr)
	err := testTransaction.Allocations[0].ExecQuantity.Validate(testTransaction, "ExecQuantity", 0)
	assert.Nil(t, err)
}

func TestExecQuantityWithoutRouteId(t *testing.T) {
	var testTransaction Transaction
	unMarshalErr := json.Unmarshal([]byte(execQuantityTestJSON), &testTransaction)
	assert.Nil(t, unMarshalErr)
	err := testTransaction.Allocations[1].ExecQuantity.Validate(testTransaction, "ExecQuantity", 1)
	assert.EqualError(t, err, "Error in allocation portfolioID 13. Invalid ExecQuantity :RouteId not present")
}

func TestExecQuantityInvalidQuantity(t *testing.T) {
	var testTransaction Transaction
	unMarshalErr := json.Unmarshal([]byte(execQuantityTestJSON), &testTransaction)
	assert.Nil(t, unMarshalErr)
	err := testTransaction.Allocations[2].ExecQuantity.Validate(testTransaction, "ExecQuantity", 2)
	assert.EqualError(t, err, "Error in allocation portfolioID 14. Invalid ExecQuantity :ExecQuantity should not be greater than Quantity")
}
