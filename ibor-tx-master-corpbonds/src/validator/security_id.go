package validator

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/httputil"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/logger"
)

// Order ...
type Order struct {
	OrderID    int      `json:"OrderId"`
	SecurityID int      `json:"SecurityId"`
	TradeDate  DateTime `json:"TradeDate"`
}

// OrderResponse type
type OrderResponse struct {
	Items []*Order `json:"Items"`
}

const ordersURL = "trading/v1/Orders?orderIds=%d"

// SecurityID field
type SecurityID int

// Validate validates the fields
func (value SecurityID) Validate(transaction Transaction, fieldName string) error {
	securities, err := NewSecurityRequest(int(value))
	if err == nil {
		logger.Log.Infof("Validating security. Symbol:  %s", securities[0].Symbol)
		if securities[0].AssetClass.Name != "FixedIncome" {
			return fmt.Errorf("Invalid %s. Expected asset class: FixedIncome, Provided asset class: %s", fieldName, securities[0].AssetClass.Name)
		}
	} else {
		if strings.Contains(err.Error(), "error:404") {
			return fmt.Errorf("Invalid %s. SecurityId: %d does not exist", fieldName, value)
		}
		return fmt.Errorf("Invalid %s. Service lookup failed", fieldName)
	}
	if strings.ToUpper(transaction.SourceSystemName) == "IMS-TRADING" {
		id, err := strconv.Atoi(string(transaction.SourceSystemReference))
		if err != nil {
			return fmt.Errorf("Invalid SourceSystemReference provided: %s", transaction.SourceSystemReference)
		}
		orderResponse, err := NewOrdersRequest(id)
		if err == nil {
			order := orderResponse.Items[0]
			if order.SecurityID != int(value) {
				return fmt.Errorf("Invalid %s. The security id is not associated to the order", fieldName)
			}
		} else {
			if strings.Contains(err.Error(), "error:404") {
				return fmt.Errorf("Invalid %s. OrderId: %d does not exist", fieldName, id)
			}
			return fmt.Errorf("Invalid %s. Service lookup failed", fieldName)
		}
	}
	return nil
}

// NewOrdersRequest fetches the Orders by OrderId
func NewOrdersRequest(SourceSystemReference int) (OrderResponse, error) {
	bodyBytes, err := httputil.NewRequest("GET", fmt.Sprintf(ordersURL, SourceSystemReference), nil)
	var orderResponse OrderResponse
	if err == nil {
		unmarshalError := json.Unmarshal(bodyBytes, &orderResponse)
		if unmarshalError != nil {
			return orderResponse, unmarshalError
		}
	}
	return orderResponse, err
}
