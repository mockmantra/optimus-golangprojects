package validator

import (
//"fmt"
)

// CommissionCharges commission charge
type CommissionCharges struct {
	CommissionCharge   *CommissionCharge   `json:"CommissionCharge"`
	CommissionChargeID *CommissionChargeID `json:"CommissionChargeId"`
}

// CommissionChargesSlice type
type CommissionChargesSlice []*CommissionCharges

// Validate validates the field
func (value CommissionChargesSlice) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	for chargeIndex, commissionCharge := range value {
		err := commissionCharge.CommissionCharge.Validate(transaction, fieldName, allocationIndex, chargeIndex)
		if err != nil {
			return err
		}
		err = commissionCharge.CommissionChargeID.Validate(transaction, fieldName, allocationIndex, chargeIndex)
		if err != nil {
			return err
		}
	}
	return nil
}
