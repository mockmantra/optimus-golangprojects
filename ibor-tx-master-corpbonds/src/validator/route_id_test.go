package validator

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/httputil"
)

func TestRouteID_WithErrorInRouteAPIHttpCall(t *testing.T) {
	var routeID RouteID = 7
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadGateway)
	}))
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "RouteID"
	type somestruct struct{}
	// transaction := Transaction{}

	err := routeID.Validate(somestruct{}, key)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. Service lookup failed", key))
}

func TestRouteID_WithHttpNotFound(t *testing.T) {
	var routeID RouteID = 7
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	}))
	httputil.UserSessionToken = "123"
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "RouteID"
	type somestruct struct{}
	// transaction := Transaction{}

	err := routeID.Validate(somestruct{}, key)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. RouteId: %d does not exist", key, routeID))
	httputil.UserSessionToken = ""
}

func TestRouteID_WithInValidResponseWithItems(t *testing.T) {
	var routeID RouteID = 7
	str := "{\"Items\": [{\"RouteID\":700}]}"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, writeErr := w.Write([]byte(str))
		assert.Nil(t, writeErr)
	}))
	httputil.UserSessionToken = "123"
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "RouteID"
	type somestruct struct{}
	// transaction := Transaction{}

	err := routeID.Validate(somestruct{}, key)
	assert.EqualError(t, err, fmt.Sprintf("Invalid %s: %d", key, routeID))
	httputil.UserSessionToken = ""
}
func TestRouteID_WithValidResponseWithItems(t *testing.T) {
	var routeID RouteID = 7
	str := "{\"Items\": [{\"RouteID\":7}]}"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, writeErr := w.Write([]byte(str))
		assert.Nil(t, writeErr)
	}))
	httputil.UserSessionToken = "123"
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "RouteID"
	type somestruct struct{}
	// transaction := Transaction{}

	err := routeID.Validate(somestruct{}, key)
	assert.Nil(t, err)
	httputil.UserSessionToken = ""
}
