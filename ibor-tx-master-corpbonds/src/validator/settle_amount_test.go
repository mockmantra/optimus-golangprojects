package validator

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSettleAmountValidforBuyAndCOver(t *testing.T) {
	var settleAmount SettleAmount = 36
	var settlePricestJSON = `{
		"EventTypeID" : 3,
		"Allocations": [
		  {
			"BookTypeId": 1,
			"Quantity":   200,
			"Fees":            12,
			"PrincipalAmount": 12,
			"ExecQuantity":12,
			"Commission":      12
		  },
		  {
			"BookTypeId": 1,
			"Quantity":   200,
			"Fees":            12,
			"ExecQuantity":12,
			"PrincipalAmount": 12,
			"Commission":      12
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(settlePricestJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)

	err := settleAmount.Validate(testTransaction, "SettleAmount", 1)

	assert.Nil(t, err)
}

func TestSettleAmountValidforBuyandCoverErr(t *testing.T) {
	var settleAmount SettleAmount = 37.00
	var settlePriceJSON = `{
		"EventTypeID" : 6,
		"Allocations": [
		  {
			"PortfolioId" :13,
			"BookTypeId": 1,
			"Quantity":   200,
			"Fees":        12,
			"ExecQuantity":12,
			"PrincipalAmount": 12,
			"Commission":      12
		  },
		  {
			"PortfolioId" :13,
			"BookTypeId": 1,
			"PortfolioId": 			2,
			"Quantity":   200,
			"ExecQuantity":12,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(settlePriceJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	fieldName := "SettleAmount"
	err := settleAmount.Validate(testTransaction, "SettleAmount", 1)
	assert.Equal(t, err, fmt.Errorf("Error in allocation portfolioID %d. Invalid %s - Expected value :%f, Provided value : %f", int(*testTransaction.Allocations[0].PortfolioID), fieldName, 36.000000, settleAmount))
}
func TestSettleAmountExecQunatityError(t *testing.T) {
	var settleAmount SettleAmount = 39
	var settlePricestJSON = `{
		"EventTypeID" : 14,
		"Allocations": [
		  {
			"BookTypeId": 1,
			"Quantity":   200,
			"PortfolioId": 			2,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12
		  },
		  {
			"BookTypeId": 			1,
			"PortfolioId": 			2,
			"Quantity":   		200,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(settlePricestJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	err := settleAmount.Validate(testTransaction, "SettleAmount", 1)

	assert.EqualError(t, err, fmt.Sprintf("Invalid SettleAmount for allocation with PortfolioID %d. Invalid Exec Quantity provided", *testTransaction.Allocations[1].PortfolioID))
}

func TestSettleAmountPrincipalAmountMissing(t *testing.T) {
	var settleAmount SettleAmount = 39
	var settlePricestJSON = `{
		"EventTypeID" : 14,
		"Allocations": [
		  {
			"BookTypeId": 1,
			"Quantity":   200,
			"PortfolioId": 			3,
			"ExecQuantity":12,
			"Fees":            12,
			"Commission":      12
		  },
		  {
			"BookTypeId": 			1,
			"PortfolioId": 			2,
			"Quantity":   		200,
			"ExecQuantity":12,
			"Fees":            12,
			"Commission":      12
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(settlePricestJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	err := settleAmount.Validate(testTransaction, "SettleAmount", 1)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. Principal Amount value is missing for PortfolioId: %d", "SettleAmount", *testTransaction.Allocations[1].PortfolioID))
}

func TestSettleAmountCommissionMissing(t *testing.T) {
	var settleAmount SettleAmount = 39
	var settlePricestJSON = `{
		"EventTypeID" : 3,
		"Allocations": [
		  {
			"BookTypeId": 1,
			"PortfolioId": 			2,
			"Quantity":   200,
			"Fees":            12,
			"PrincipalAmount": 12,
			"ExecQuantity":12,
			"Commission":      12
		  },
		  {
			"BookTypeId": 1,
			"PortfolioId": 			2,
			"Quantity":   200,
			"Fees":            12,
			"ExecQuantity":12,
			"PrincipalAmount": 12
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(settlePricestJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	err := settleAmount.Validate(testTransaction, "SettleAmount", 1)

	assert.EqualError(t, err, fmt.Sprintf("Error in allocation portfolioID %d. Invalid %s - Expected value :24.000000, Provided value : 39.000000", *testTransaction.Allocations[1].PortfolioID, "SettleAmount"))
}

func TestSettleAmountFeesMissing(t *testing.T) {
	var settleAmount SettleAmount = 39
	var settlePricestJSON = `{
		"EventTypeID" : 3,
		"Allocations": [
		  {
			"BookTypeId": 1,
			"PortfolioId": 			2,
			"Quantity":   200,
			"Fees":            12,
			"PrincipalAmount": 12,
			"ExecQuantity":12,
			"Commission":      12
		  },
		  {
			"BookTypeId": 1,
			"PortfolioId": 			2,
			"Quantity":   200,
			"ExecQuantity":12,
			"PrincipalAmount": 12,
			"Commission":      12
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(settlePricestJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	err := settleAmount.Validate(testTransaction, "SettleAmount", 1)

	assert.EqualError(t, err, fmt.Sprintf("Error in allocation portfolioID %d. Invalid %s - Expected value :24.000000, Provided value : 39.000000", *testTransaction.Allocations[1].PortfolioID, "SettleAmount"))
}

func TestSettleAmount_WhenPricipalAmountIsNotEqualToDifferenceOfPrincipalAmountCommissionAndFees(t *testing.T) {
	var settleAmount SettleAmount = 32.00
	var settleAmountJSON = `{
		"EventTypeID" : 14,
		"Allocations": [
		  {
				"PortfolioId" :13,
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity": 2,
			"Fees":            12,
			"PrincipalAmount": 30,
			"Commission":      12
		  },
		  {
				"PortfolioId" :14,
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity": 2,
			"Fees":            12,
			"PrincipalAmount": 36,
			"Commission":      12
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(settleAmountJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	fieldName := "SettleAmount"
	err := settleAmount.Validate(testTransaction, fieldName, 0)
	assert.Equal(t, err, fmt.Errorf("Error in allocation portfolioID %d. Invalid %s - Expected value :%f, Provided value : %f", int(*testTransaction.Allocations[0].PortfolioID), fieldName, 6.000000, settleAmount))
}

func TestSettleAmount_WhenPricipalAmountIsEqualToDifferenceOfPrincipalAmountCommissionAndFees(t *testing.T) {
	var settleAmount SettleAmount = 28.00
	var settleAmountJSON = `{
		"EventTypeID" : 14,
		"Allocations": [
		  {
				"PortfolioId" :13,
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity": 2,
			"Fees":            0,
			"PrincipalAmount": 40,
			"Commission":      12
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(settleAmountJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	fieldName := "SettleAmount"
	err := settleAmount.Validate(testTransaction, fieldName, 0)
	assert.Nil(t, err)
}

func TestSettleAmount_WhenPricipalAmountIsEqualToDifferenceOfPrincipalAmountCommissionAndFeesValid(t *testing.T) {
	var settleAmountJSON = `{
		"EventTypeID" : 14,
		"Allocations": [
		  {
			"PortfolioId" :13,
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity": 2,
			"Fees":            1.2,
			"PrincipalAmount": 10.23456,
			"Commission":      1.454455
		  }
		]
	}`
	var settleAmount SettleAmount = 7.580105
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(settleAmountJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	fieldName := "SettleAmount"
	err := settleAmount.Validate(testTransaction, fieldName, 0)
	assert.Nil(t, err)
}

func TestSettleAmount_WhenPricipalAmountIsEqualToSumOfPrincipalAmountCommissionAndFeesValidPrecision(t *testing.T) {
	var settleAmountJSON = `{
		"EventTypeID" : 3,
		"Allocations": [
		  {
				"BaseCurrencySecurityId": 157,
				"BaseToLocalFxRate": 0.76432,
				"BookTypeId": 2,
				"Commission": 0,
				"Fees":0,
				"CommissionCharges": [],
				"CustodianCounterpartyAccountId": 1,
				"ExecQuantity": 99873,
				"ExecutingBrokerCounterpartyId": 5,
				"FeeCharges": [],
				"IsFinalized": false,
				"NetAmount": 364191.88814999996,
				"PortfolioId": 1,
				"PositionTags": null,
				"PrincipalAmount": 364191.88814999996,
				"Quantity": 803900,
				"RouteId": 9406,
				"RouteName": "BTIG-DESK",
				"SettleAmount": 364191.88814999996,
				"SettleCurrencySecurityId": 172,
				"SettlePrice": 3.64655,
				"SettleToBaseFXRate": 1.30837,
				"SettleToLocalFXRate": 1,
				"SettleToSystemFXRate": 1.30837
			}
		]
	}`
	var settleAmount SettleAmount = 364191.88814999996
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(settleAmountJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	fieldName := "SettleAmount"
	err := settleAmount.Validate(testTransaction, fieldName, 0)
	assert.Nil(t, err)
}
