package validator

import (
	"encoding/json"
	"fmt"
	"reflect"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/httputil"
)

// ExecutingBrokerCounterpartyID field
type ExecutingBrokerCounterpartyID int

// Counterparty field
type Counterparty struct {
	CounterpartyTypeID int `json:"CounterpartyTypeId"`
	CounterpartyID     int `json:"CounterpartyId"`
}

// ExecutingBrokerResponse type
type ExecutingBrokerResponse []Counterparty

const executingBrokerURL = "referencedata/v1/ExecutingBroker"

// Validate validates the fields
func (value ExecutingBrokerCounterpartyID) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	if !reflect.ValueOf(transaction.Allocations[allocationIndex].RouteID).IsNil() {
		bodyBytes, err := httputil.NewRequest("GET", executingBrokerURL, nil)
		var executingBrokersResponse ExecutingBrokerResponse
		if err == nil {
			unmarshalErr := json.Unmarshal(bodyBytes, &executingBrokersResponse)
			if unmarshalErr == nil {
				var exists bool
				for _, executingBroker := range executingBrokersResponse {
					if int(value) == executingBroker.CounterpartyID {
						exists = true
						break
					}
				}
				if !exists {
					return fmt.Errorf("Error in allocation PortfolioID %d. Invalid %s : %d does not exist", int(*transaction.Allocations[allocationIndex].PortfolioID), fieldName, int(value))
				}
			} else {
				return fmt.Errorf("Error in portfolioID %d. %s : Unmarshlling error", int(*transaction.Allocations[allocationIndex].PortfolioID), fieldName)
			}
		} else {

			return fmt.Errorf("Error in portfolioID %d. %s : Service lookup failed", int(*transaction.Allocations[allocationIndex].PortfolioID), fieldName)
		}
	} else {
		return fmt.Errorf("Error in allocation PortfolioID %d. Invalid %s : RouteId missing", int(*transaction.Allocations[allocationIndex].PortfolioID), fieldName)
	}
	return nil
}
