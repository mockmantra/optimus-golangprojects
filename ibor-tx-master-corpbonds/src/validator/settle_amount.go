package validator

import (
	"fmt"
	"reflect"

	"stash.ezesoft.net/imsacnt/accounting-utils/enums/eventtype"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/utils"
)

// SettleAmount type
type SettleAmount float64

// Validate validates the field
func (value SettleAmount) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	var fees, commission float64
	currentAllocation := transaction.Allocations[allocationIndex]
	if reflect.ValueOf(currentAllocation.ExecQuantity).IsNil() || int(*currentAllocation.ExecQuantity) <= 0 {
		return fmt.Errorf("Invalid %s for allocation with PortfolioID %d. Invalid Exec Quantity provided", fieldName, int(*transaction.Allocations[0].PortfolioID))
	}
	if reflect.ValueOf(currentAllocation.PrincipalAmount).IsNil() {
		return fmt.Errorf("Invalid %s. Principal Amount value is missing for PortfolioId: %d", fieldName, *transaction.Allocations[allocationIndex].PortfolioID)
	}
	if reflect.ValueOf(currentAllocation.Commission).IsNil() {
		commission = 0
	} else {
		commission = float64(*currentAllocation.Commission)
	}
	if reflect.ValueOf(currentAllocation.Fees).IsNil() {
		fees = 0
	} else {
		fees = float64(*currentAllocation.Fees)
	}
	roundedSettleAmount := utils.Round(utils.PrecisionRound(float64(value), 0.00005), 0.5, 4)

	if transaction.EventTypeID == eventtype.Buy || transaction.EventTypeID == eventtype.Cover {
		sum := float64(*currentAllocation.PrincipalAmount) + commission + fees
		roundedSum := utils.Round((utils.PrecisionRound(sum, 0.00005)), 0.5, 4)
		if roundedSum != roundedSettleAmount {
			return fmt.Errorf("Error in allocation portfolioID %d. Invalid %s - Expected value :%f, Provided value : %f", int(*transaction.Allocations[0].PortfolioID), fieldName, roundedSum, roundedSettleAmount)
		}
	} else if transaction.EventTypeID == eventtype.Sell || transaction.EventTypeID == eventtype.Short {
		diff := float64(*currentAllocation.PrincipalAmount) - commission - fees
		roundedDiff := utils.Round((utils.PrecisionRound(diff, 0.00005)), 0.5, 4)
		if roundedDiff != roundedSettleAmount {
			return fmt.Errorf("Error in allocation portfolioID %d. Invalid %s - Expected value :%f, Provided value : %f", int(*transaction.Allocations[0].PortfolioID), fieldName, roundedDiff, roundedSettleAmount)
		}
	} else {
		return fmt.Errorf("Error in allocation portfolioID %d. Invalid %s due to eventTypeID %d", int(*transaction.Allocations[0].PortfolioID), fieldName, transaction.EventTypeID)
	}
	return nil
}
