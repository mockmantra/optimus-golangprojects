package validator

import (
	"fmt"
	"time"
)

// TransactionDateTimeUTC type
type TransactionDateTimeUTC string

// Validate validates the fields
func (value TransactionDateTimeUTC) Validate(transaction Transaction, fieldName string) error {
	transactionDateTimeUTC, err := time.Parse(time.RFC3339Nano, string(value))
	currentDateUTC := time.Now().UTC()
	if err == nil {
		businessYear, businessMonth, businessDay := transactionDateTimeUTC.Date()
		currentYear, currentMonth, currentDay := currentDateUTC.Date()
		if businessYear == currentYear && businessMonth == currentMonth && businessDay == currentDay {
			return nil
		}
		return fmt.Errorf("Invalid %s. TransactionDate must match current date. Current Date: %s, Provided TransactionDate: %s", fieldName, currentDateUTC.Format(time.RFC3339)[:10], transactionDateTimeUTC.Format(time.RFC3339)[:10])
	}
	return fmt.Errorf("Invalid %s. Incorrect format", fieldName)
}
