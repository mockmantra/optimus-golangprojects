package validator

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/httputil"
)

func TestSecurityTemplateID_WithErrorInHttpCall(t *testing.T) {
	var securityTemplateID SecurityTemplateID = 7
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadGateway)
	}))

	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SecurityTemplateID"
	transaction := Transaction{}
	httputil.UserSessionToken = "123"

	err := securityTemplateID.Validate(transaction, key)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. Service lookup failed", key))
	httputil.UserSessionToken = ""

}
func TestSecurityTemplateID_WithHttpNotFound(t *testing.T) {
	var securityTemplateID SecurityTemplateID = 7
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	}))

	defer ts.Close()
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	key := "SecurityTemplateID"
	transaction := Transaction{}

	err := securityTemplateID.Validate(transaction, key)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. SecurityTemplateId: %d does not exist", key, securityTemplateID))
	httputil.UserSessionToken = ""

}
func TestSecurityTemplateID_WithValidResponseWithEmptyEventTypes(t *testing.T) {
	var securityTemplateID SecurityTemplateID = 7
	str := `{"data": {"eventTypes": []}}`
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	httputil.UserSessionToken = "123"

	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SecurityTemplateID"
	transaction := Transaction{}

	err := securityTemplateID.Validate(transaction, key)
	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. There are no items", key))
	httputil.UserSessionToken = ""
}
func TestSecurityTemplateID_WithValidResponseWithItems(t *testing.T) {
	var securityTemplateID SecurityTemplateID = 7
	str := `{"data": {"eventTypes": [{"eventTypeName":"Buy","eventTypeId":3},{"eventTypeName":"Cover","eventTypeId":6},{"eventTypeName":"Sell","eventTypeId":14}]}}`
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))

	httputil.UserSessionToken = "123"
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SecurityTemplateID"
	transaction := Transaction{}

	err := securityTemplateID.Validate(transaction, key)
	assert.Nil(t, err)
	httputil.UserSessionToken = ""
}
