package validator

import (
	"encoding/json"
	"fmt"
	"strings"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/httputil"
)

// RouteID field
type RouteID int

// Route ..
type Route struct {
	RouteID int `json:"RouteID"`
}

// RouteResponse ..
type RouteResponse struct {
	Items []*Route `json:"Items"`
}

const routeURL = "trading/v1/routes/%d"

// Validate validates the fields
func (value RouteID) Validate(validationStruct interface{}, fieldName string) error {
	routeResponse, err := NewRouteRequest(int(value))
	if err != nil {
		if strings.Contains(err.Error(), "error:404") {
			return fmt.Errorf("Invalid %s. RouteId: %d does not exist", fieldName, value)
		}
		return fmt.Errorf("Invalid %s. Service lookup failed", fieldName)
	}
	if routeResponse.Items != nil && routeResponse.Items[0].RouteID != int(value) {
		return fmt.Errorf("Invalid %s: %d", fieldName, value)
	}
	return nil
}

// NewRouteRequest fetches the Orders by OrderId
func NewRouteRequest(routeID int) (RouteResponse, error) {
	bodyBytes, err := httputil.NewRequest("GET", fmt.Sprintf(routeURL, routeID), nil)
	var routeResponse RouteResponse
	if err == nil {
		unmarshalErr := json.Unmarshal(bodyBytes, &routeResponse)
		if unmarshalErr != nil {
			return RouteResponse{}, unmarshalErr
		}
	}
	return routeResponse, err
}
