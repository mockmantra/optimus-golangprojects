package validator

import (
	"fmt"
	"reflect"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/utils"
)

// Commission type
type Commission float64

// Validate validates the field
func (value Commission) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	if !reflect.ValueOf(transaction.Allocations[allocationIndex].ExecQuantity).IsNil() && int(*transaction.Allocations[allocationIndex].ExecQuantity) > 0 {
		if !reflect.ValueOf(transaction.Allocations[allocationIndex].CommissionCharges).IsNil() {
			var commissionTotal float64
			for _, commissionCharges := range *transaction.Allocations[allocationIndex].CommissionCharges {
				commissionTotal += float64(*commissionCharges.CommissionCharge)
			}

			if *transaction.Allocations[allocationIndex].BookTypeID != BookTypeStrategy {
				roundedCommisionTotal := utils.Round(float64(commissionTotal), 0.5, 4)
				roundedCommission := utils.Round(float64(value), 0.5, 4)
				if roundedCommisionTotal == roundedCommission {
					return nil
				}
				return fmt.Errorf("Error in portfolioID %d. Invalid %s : Expected value %f, Provided value %f", int(*transaction.Allocations[allocationIndex].PortfolioID), fieldName, roundedCommission, roundedCommisionTotal)

			}
			var locationCommissionTotal float64
			var strategyCommissionTotal float64

			for _, commission := range transaction.Allocations {
				if commission.Commission != nil {
					if *commission.BookTypeID == BookTypeLocation {
						locationCommissionTotal += float64(*commission.Commission)
					} else if *commission.BookTypeID == BookTypeStrategy {
						strategyCommissionTotal += float64(*commission.Commission)
					}
				}
			}
			if utils.Round(locationCommissionTotal, 0.5, 4) == utils.Round(strategyCommissionTotal, 0.5, 4) {
				return nil
			}
			return fmt.Errorf("Error in portfolioID %d. Invalid %s for BookTypeId : 3 ", int(*transaction.Allocations[allocationIndex].PortfolioID), fieldName)
		}
		return fmt.Errorf("Error in portfolioID %d. Invalid %s : Value should be present", int(*transaction.Allocations[allocationIndex].PortfolioID), fieldName)
	}
	return fmt.Errorf("Error in portfolioID %d. Invalid %s : ExecQuantity not valid", int(*transaction.Allocations[allocationIndex].PortfolioID), fieldName)
}
