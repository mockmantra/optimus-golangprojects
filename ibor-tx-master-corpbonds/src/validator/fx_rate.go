package validator

import (
	"fmt"
	"reflect"
)

// FxRate type
type FxRate float64

// Validate validates the field
func (value FxRate) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	currentAllocation := transaction.Allocations[allocationIndex]

	if fieldName == "BaseToLocalFXRate" {
		if int(*currentAllocation.BaseCurrencySecurityID) == int(transaction.LocalCurrencySecurityID) {
			if value != 1 {
				return fmt.Errorf("Error in allocation portfolioID %d. Invalid %s - Expected value: %d, Provided value : %f", int(*currentAllocation.PortfolioID), fieldName, 1, value)
			}
		}
		return nil
	}
	if !reflect.ValueOf(currentAllocation.ExecQuantity).IsNil() &&
		*currentAllocation.ExecQuantity > 0 {

		switch fieldName {
		case "SettleToSystemFXRate":
			if !reflect.ValueOf(currentAllocation.SettleCurrencySecurityID).IsNil() {
				if int(*currentAllocation.SettleCurrencySecurityID) == int(transaction.SystemCurrencySecurityID) {
					if value != 1 {
						return fmt.Errorf("Error in allocation portfolioID %d. Invalid %s - Expected value: %d, Provided value : %f", int(*currentAllocation.PortfolioID), fieldName, 1, value)
					}
				}
			} else {
				return fmt.Errorf("Error in allocation portfolioID %d. Invalid %s - SettleCurrencySecurityID is missing", int(*currentAllocation.PortfolioID), fieldName)
			}
		case "SettleToLocalFXRate":
			if !reflect.ValueOf(currentAllocation.SettleCurrencySecurityID).IsNil() {
				if int(transaction.LocalCurrencySecurityID) == int(*currentAllocation.SettleCurrencySecurityID) {
					if value != 1 {
						return fmt.Errorf("Error in allocation portfolioID %d. Invalid %s - Expected value: %d, Provided value : %f", int(*currentAllocation.PortfolioID), fieldName, 1, value)
					}
				}
			} else {
				return fmt.Errorf("Error in allocation portfolioID %d. Invalid %s - SettleCurrencySecurityID is missing", int(*currentAllocation.PortfolioID), fieldName)
			}
		case "SettleToBaseFXRate":
			if !reflect.ValueOf(currentAllocation.SettleCurrencySecurityID).IsNil() {
				if int(*currentAllocation.SettleCurrencySecurityID) == int(*currentAllocation.BaseCurrencySecurityID) {
					if value != 1 {
						return fmt.Errorf("Error in allocation portfolioID %d. Invalid %s - Expected value: %d, Provided value : %f", int(*currentAllocation.PortfolioID), fieldName, 1, value)
					}
				}
			} else {
				return fmt.Errorf("Error in allocation portfolioID %d. Invalid %s - SettleCurrencySecurityID is missing", int(*currentAllocation.PortfolioID), fieldName)
			}
		}
		return nil
	}
	return fmt.Errorf("Invalid %s for allocation portfolioID %d. Expected : ExecQuantity not valid", fieldName, int(*currentAllocation.PortfolioID))
}
