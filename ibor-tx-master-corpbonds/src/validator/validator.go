package validator

import (
	"fmt"
	"reflect"
	"runtime/debug"
	"strings"
	"sync"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/interfaces"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/utils"
)

// ErrorMessage ...
type ErrorMessage struct {
	Field   string
	Message []string
}

// Validate ...
func Validate(data Transaction, log interfaces.ILogger) (errorMessages []ErrorMessage) {
	mux := &sync.Mutex{}
	// load data into envelope
	v := reflect.ValueOf(data)
	t := v.Type()
	var wg sync.WaitGroup

	var appendErrorMessage = func(field string, validationErrors []string) {
		mux.Lock()
		errorMessages = append(errorMessages, ErrorMessage{field, validationErrors})
		mux.Unlock()
	}

	var recoverPanic = func(field string, allocationIndex int) {
		if p := recover(); p != nil {
			var errorMessage string
			if allocationIndex > -1 {
				errorMessage = fmt.Sprintf("Failed to validate field : %s in allocation with PortfolioID : %d",
					field, int(*data.Allocations[allocationIndex].PortfolioID))
			} else {
				errorMessage = fmt.Sprintf("Failed to validate the field: %s", field)
			}
			appendErrorMessage(field, []string{errorMessage})
			log.Errorf("Recover on validation %s: %s", p, string(debug.Stack()))
		}
	}

	getSecurities(data, log)

	for i := 0; i < t.NumField(); i++ {
		if validator, ok := v.Field(i).Interface().(Validator); ok {
			wg.Add(1)
			go func(x int, typ reflect.Type) {
				defer wg.Done()
				field := getFieldName(x, typ)
				defer recoverPanic(field, -1)
				if err := validator.Validate(data, field); err != nil {
					errorsSlice := strings.Split(err.Error(), "||")
					appendErrorMessage(field, errorsSlice)
				}
			}(i, t)
		}
	}
	// Validating Allocations
	// check if allocations are nil???
	allocations := data.Allocations
	for allocationIndex, allocation := range allocations {
		// load data into envelope
		v = reflect.ValueOf(allocation)
		t = v.Type()
		for i := 0; i < t.NumField(); i++ {
			if validator, ok := v.Field(i).Interface().(AllocationValidator); ok && !reflect.ValueOf(validator).IsNil() {
				wg.Add(1)
				go func(x int, typ reflect.Type, alloIndex int) {
					defer wg.Done()
					field := getFieldName(x, typ)
					defer recoverPanic(field, alloIndex)
					if err := validator.Validate(data, field, alloIndex); err != nil {
						appendErrorMessage(field, []string{err.Error()})
					}
				}(i, t, allocationIndex)
			}
		}
	}

	wg.Wait()
	return errorMessages
}

func getFieldName(x int, typ reflect.Type) string {
	return strings.Split(typ.Field(x).Tag.Get("json"), ",")[0]
}

func getSecurities(transaction Transaction, log interfaces.ILogger) {
	securityIDs := []int{
		int(transaction.SecurityID),
		int(transaction.LocalCurrencySecurityID),
		int(transaction.SystemCurrencySecurityID),
	}

	for _, allocation := range transaction.Allocations {
		if allocation.SettleCurrencySecurityID != nil {
			securityIDs = append(securityIDs, int(*allocation.SettleCurrencySecurityID))
		}
		if allocation.BaseCurrencySecurityID != nil {
			securityIDs = append(securityIDs, int(*allocation.BaseCurrencySecurityID))
		}
	}

	securityIDs = utils.Unique(securityIDs)

	err := GetSecurities(securityIDs)

	if err != nil {
		log.Errorf("Unable to fetch the securities %v", securityIDs)
	}
}
