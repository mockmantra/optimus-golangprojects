package validator

import (
	"fmt"
	"reflect"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/utils"
)

// PrincipalAmount type
type PrincipalAmount float64

// Validate validates the field
func (value PrincipalAmount) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	currentAllocation := transaction.Allocations[allocationIndex]

	if reflect.ValueOf(transaction.Allocations[allocationIndex].ExecQuantity).IsNil() || int(*transaction.Allocations[allocationIndex].ExecQuantity) <= 0 {
		return fmt.Errorf("Error in allocation PortfolioID %d. Invalid %s : ExecQuantity not valid", *transaction.Allocations[allocationIndex].PortfolioID, fieldName)
	}
	if reflect.ValueOf(transaction.Allocations[allocationIndex].SettlePrice).IsNil() {
		return fmt.Errorf("Error in allocation PortfolioID %d. Invalid %s : Settle Price not provided", *transaction.Allocations[allocationIndex].PortfolioID, fieldName)
	}
	product := float64(*currentAllocation.SettlePrice) * float64(*currentAllocation.ExecQuantity)
	roundedProduct := utils.Round(utils.PrecisionRound(product, 0.00005), 0.5, 4)
	roundedPrincipalAmount := utils.Round(utils.PrecisionRound(float64(value), 0.00005), 0.5, 4)
	if roundedProduct != roundedPrincipalAmount {
		return fmt.Errorf("Error in allocation PortfolioID %d. Invalid %s - Expected value :%f, Provided value : %f", int(*transaction.Allocations[0].PortfolioID), fieldName, roundedProduct, roundedPrincipalAmount)
	}
	return nil
}
