package validator

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSystemToLocalFXRate_SystemCurrencySecurityIDEqualsLocalCurrencySecurityID(t *testing.T) {
	systemToLocalFXRate := SystemToLocalFXRate(1)
	transaction := Transaction{
		SystemCurrencySecurityID: 12,
		LocalCurrencySecurityID:  12,
	}

	err := systemToLocalFXRate.Validate(transaction, "SystemToLocalFXRate")

	assert.Nil(t, err)
}
func TestSystemToLocalFXRate_IsNotEqualTo1SystemCurrencySecurityIDEqualsLocalCurrencySecurityID(t *testing.T) {
	systemToLocalFXRate := SystemToLocalFXRate(2)
	transaction := Transaction{
		SystemCurrencySecurityID: 12,
		LocalCurrencySecurityID:  12,
	}

	err := systemToLocalFXRate.Validate(transaction, "SystemToLocalFXRate")

	assert.EqualError(t, err, "Invalid SystemToLocalFXRate. The SystemCurrencySecurityID and LocalCurrencySecurityID are equal but the value of SystemToLocalFXRate is 2 instead of 1")
}
