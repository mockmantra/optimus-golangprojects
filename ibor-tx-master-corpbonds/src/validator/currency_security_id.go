package validator

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/httputil"
)

// CurrencySecurityID field
type CurrencySecurityID int

// AssetClass ..
type AssetClass struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// Security type
type Security struct {
	SecurityID int        `json:"id"`
	AssetClass AssetClass `json:"assetClass"`
	Symbol     string     `json:"symbol"`
}

type securityResponse struct {
	Data struct {
		Securites []Security `json:"securities"`
	} `json:"data"`
}

var securitiesInPosition = make(map[int]Security)

const securityURL = "coredata/v2/graphql?query={securities(ids:%v){id,symbol,assetClass{id,name}}}"

// Validate validates the fields
func (value CurrencySecurityID) Validate(transaction Transaction, fieldName string) error {
	securities, err := NewSecurityRequest(int(value))
	if err == nil {
		if securities[0].AssetClass.Name != "Cash" {
			return fmt.Errorf("Invalid %s. Expected AssetClass : Cash, Provided Assetclass : %s", fieldName, securities[0].AssetClass.Name)
		}
	} else {
		if strings.Contains(err.Error(), "error:404") {
			return fmt.Errorf("Invalid %s. %s %d  does not exist", fieldName, fieldName, int(value))
		}
		return fmt.Errorf("%s : Service lookup failed", fieldName)
	}
	return nil
}

// NewSecurityRequest fetches the Orders by OrderId
func NewSecurityRequest(securityID int) ([]Security, error) {
	if security, ok := securitiesInPosition[securityID]; ok {
		return []Security{security}, nil
	}
	bodyBytes, err := httputil.NewRequest("GET", fmt.Sprintf(securityURL, fmt.Sprintf("[%d]", securityID)), nil)
	var securityResponse securityResponse
	var securities []Security
	if err == nil {
		errMarshal := json.Unmarshal(bodyBytes, &securityResponse)
		if errMarshal != nil {
			return securities, errMarshal
		}
		securities = securityResponse.Data.Securites
		if len(securities) > 0 {
			return securities, nil
		}
		return securities, errors.New("error:404")
	}
	return securities, err
}

// GetSecurities gets the securities for supplied Ids
func GetSecurities(securityIDs []int) error {
	bodyBytes, err := httputil.NewRequest("GET", fmt.Sprintf(securityURL, strings.Replace(fmt.Sprint(securityIDs), " ", ",", -1)), nil)
	var securityResponse securityResponse
	if err == nil {
		err = json.Unmarshal(bodyBytes, &securityResponse)
		if err == nil {
			for _, security := range securityResponse.Data.Securites {
				securitiesInPosition[security.SecurityID] = security
			}
		}
	}
	return err
}
