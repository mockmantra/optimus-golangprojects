package validator

import (
	"fmt"
	"reflect"
	"strings"
)

// SettleCurrencySecurityID field
type SettleCurrencySecurityID int

// Validate validates the fields
func (value SettleCurrencySecurityID) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	if !reflect.ValueOf(transaction.Allocations[allocationIndex].ExecQuantity).IsNil() &&
		*transaction.Allocations[allocationIndex].ExecQuantity > 0 {
		securities, err := NewSecurityRequest(int(value))
		if err == nil {
			if securities[0].AssetClass.Name != "Cash" {
				return fmt.Errorf("Invalid %s. The Asset Class name should have been Cash but instead it is %s for PortfolioId: %d", fieldName, securities[0].AssetClass.Name, *transaction.Allocations[allocationIndex].PortfolioID)
			}
		} else {
			if strings.Contains(err.Error(), "error:404") {
				return fmt.Errorf("Invalid %s. Error fetching the SettleCurrencyID for PortfolioId: %d", fieldName, *transaction.Allocations[allocationIndex].PortfolioID)
			}
			return fmt.Errorf("Error in allocation portfolioID %d. %s : Service lookup failed", *transaction.Allocations[allocationIndex].PortfolioID, fieldName)
		}
	}

	return nil
}
