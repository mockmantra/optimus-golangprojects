package validator

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTransactionDate_InvalidDateFormat(t *testing.T) {
	var TransactionDate TransactionDate
	transaction := Transaction{
		TradeDate:  "2018-09-08 13:25:43Z",
		SettleDate: "2018-09-08 13:25:43Z",
	}
	key := "TradeDate"
	err := TransactionDate.Validate(transaction, key)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. Invalid format", key))
}
func TestTransactionDate_ValidDateFormatAndNotBeforeOrEqualSettleDate(t *testing.T) {
	var TransactionDate TransactionDate
	transaction := Transaction{
		SettleDate: "2018-09-07T13:25:43Z",
		TradeDate:  "2018-09-08T13:25:43Z",
	}
	key := "TradeDate"
	err := TransactionDate.Validate(transaction, key)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. TradeDate must be before SettleDate. Provided TradeDate: %s, SettleDate: %s", key, transaction.TradeDate, transaction.SettleDate))
}
func TestTransactionDate_ValidDateFormatAndBeforeSettleDate(t *testing.T) {
	var TransactionDate TransactionDate
	transaction := Transaction{
		SettleDate: "2018-09-07T13:25:43Z",
		TradeDate:  "2018-09-06T13:25:43Z",
	}
	key := "TradeDate"
	err := TransactionDate.Validate(transaction, key)

	assert.Nil(t, err)
}

func TestTransactionDate_ValidDateFormatAndNotBeforeOrEqualSettleDateHour(t *testing.T) {
	var TransactionDate TransactionDate
	transaction := Transaction{
		SettleDate: "2018-09-07T12:25:43Z",
		TradeDate:  "2018-09-07T13:25:43Z",
	}
	key := "TradeDate"
	err := TransactionDate.Validate(transaction, key)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. TradeDate must be before SettleDate. Provided TradeDate: %s, SettleDate: %s", key, transaction.TradeDate, transaction.SettleDate))
}
func TestTransactionDate_ValidDateFormatAndBeforeSettleDateHour(t *testing.T) {
	var TransactionDate TransactionDate
	transaction := Transaction{
		SettleDate: "2018-09-07T14:25:43Z",
		TradeDate:  "2018-09-07T13:25:43Z",
	}
	key := "TradeDate"
	err := TransactionDate.Validate(transaction, key)

	assert.Nil(t, err)
}

func TestTransactionDate_ValidDateFormatAndNotBeforeOrEqualSettleDateMinute(t *testing.T) {
	var TransactionDate TransactionDate
	transaction := Transaction{
		SettleDate: "2018-09-07T13:24:43Z",
		TradeDate:  "2018-09-07T13:25:43Z",
	}
	key := "TradeDate"
	err := TransactionDate.Validate(transaction, key)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. TradeDate must be before SettleDate. Provided TradeDate: %s, SettleDate: %s", key, transaction.TradeDate, transaction.SettleDate))
}
func TestTransactionDate_ValidDateFormatAndBeforeSettleDateMinute(t *testing.T) {
	var TransactionDate TransactionDate
	transaction := Transaction{
		SettleDate: "2018-09-07T13:26:43Z",
		TradeDate:  "2018-09-07T13:25:43Z",
	}
	key := "TradeDate"
	err := TransactionDate.Validate(transaction, key)

	assert.Nil(t, err)
}

func TestTransactionDate_ValidDateFormatAndNotBeforeOrEqualSettleDateSecond(t *testing.T) {
	var TransactionDate TransactionDate
	transaction := Transaction{
		SettleDate: "2018-09-07T13:25:42Z",
		TradeDate:  "2018-09-07T13:25:43Z",
	}
	key := "TradeDate"
	err := TransactionDate.Validate(transaction, key)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. TradeDate must be before SettleDate. Provided TradeDate: %s, SettleDate: %s", key, transaction.TradeDate, transaction.SettleDate))
}
func TestTransactionDate_ValidDateFormatAndBeforeSettleDateSecond(t *testing.T) {
	var TransactionDate TransactionDate
	transaction := Transaction{
		SettleDate: "2018-09-07T13:25:44Z",
		TradeDate:  "2018-09-07T13:25:43Z",
	}
	key := "TradeDate"
	err := TransactionDate.Validate(transaction, key)

	assert.Nil(t, err)
}

func TestTransactionDate_ValidDateFormatAndNotBeforeOrEqualSettleDateNanosecond(t *testing.T) {
	var TransactionDate TransactionDate
	transaction := Transaction{
		SettleDate: "2018-09-07T13:25:43.0000001Z",
		TradeDate:  "2018-09-07T13:25:43.0000002Z",
	}
	key := "TradeDate"
	err := TransactionDate.Validate(transaction, key)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. TradeDate must be before SettleDate. Provided TradeDate: %s, SettleDate: %s", key, transaction.TradeDate, transaction.SettleDate))
}
func TestTransactionDate_ValidDateFormatAndBeforeSettleDateNanosecond(t *testing.T) {
	var TransactionDate TransactionDate
	transaction := Transaction{
		SettleDate: "2018-09-07T13:25:43.0000003Z",
		TradeDate:  "2018-09-07T13:25:43.0000002Z",
	}
	key := "TradeDate"
	err := TransactionDate.Validate(transaction, key)

	assert.Nil(t, err)
}
