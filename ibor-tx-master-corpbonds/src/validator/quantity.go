package validator

import (
	"fmt"
	"strings"
)

// Quantity int type
type Quantity int

type quantityByBook struct {
	quantity  int
	portfolio map[int]int
}

// Validate validates OrderQuantity field
func (value Quantity) Validate(transaction Transaction, fieldName string) error {
	allocations := make(map[int]quantityByBook)
	for _, allocation := range transaction.Allocations {
		bookTypeID := int(*allocation.BookTypeID)
		quantity := int(*allocation.Quantity)
		portfolioID := int(*allocation.PortfolioID)
		if val, ok := allocations[bookTypeID]; ok {
			if portfolio, ok := val.portfolio[portfolioID]; ok {
				val.portfolio[portfolioID] = portfolio + quantity
			} else {
				val.portfolio[portfolioID] = quantity
			}
			allocations[bookTypeID] = quantityByBook{
				quantity:  val.quantity + quantity,
				portfolio: val.portfolio,
			}
		} else {
			portfolioMap := make(map[int]int)
			portfolioMap[portfolioID] = quantity
			allocations[bookTypeID] = quantityByBook{
				quantity:  quantity,
				portfolio: portfolioMap,
			}
		}
	}

	var errorStrings []string

	for id, allocation := range allocations {
		if allocation.quantity != int(transaction.OrderQuantity) {
			errorStrings = append(errorStrings, fmt.Sprintf("Invalid %s. The sum of Quantities for Book Type %d: %d does not match OrderQuantity: %d", fieldName, id, allocation.quantity, value))
		}
		for innerID, innerAllocation := range allocations {
			if innerID != id {
				for portfolioID, secondQuantity := range innerAllocation.portfolio {
					firstQuantity := allocation.portfolio[portfolioID]
					if firstQuantity == 0 || secondQuantity != firstQuantity {
						errorStrings = append(errorStrings, fmt.Sprintf("Invalid %s. The sum of Quantities by Portfolio Id: %d in Book Type %d: %d does not match with Book Type %d : %d", fieldName, portfolioID, id, firstQuantity, innerID, secondQuantity))
					}
				}
			}
		}
		delete(allocations, id)
	}
	if len(errorStrings) > 0 {
		return fmt.Errorf(strings.Join(errorStrings, "||"))
	}
	return nil
}
