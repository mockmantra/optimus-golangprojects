package validator

import (
	"encoding/json"
	"fmt"
	"strings"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/httputil"
)

// PositionTag position tag
type PositionTag struct {
	IndexAttributeID      *int64 `json:"indexAttributeId"`
	IndexAttributeValueID *int64 `json:"indexAttributeValueId"`
}

// PositionTagsSlice type
type PositionTagsSlice []*PositionTag

// PositionTagWithMembers type
type PositionTagWithMembers struct {
	ID      int           `json:"id"`
	Members []PositionTag `json:"members"`
}

// ResponseMember type
type ResponseMember struct {
	IndexAttributeID      *int64 `json:"IndexAttributeId"`
	IndexAttributeValueID *int64 `json:"indexAttributeValueId"`
}

type positionTagResponse struct {
	Data positionTagData `json:"data"`
}

type positionTagData struct {
	PositionTagWithMembers []PositionTagWithMembers `json:"positionTags"`
}

const positionTagURL = "coredata/v2/graphql?query={positionTags{id,members{indexAttributeId,indexAttributeValueId}}}"

// Validate validates the field
func (value PositionTagsSlice) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	if *transaction.Allocations[allocationIndex].BookTypeID != BookTypeStrategy {
		return fmt.Errorf("Invalid %s as the provided book type is %d. Expected %d (for Strategy)", fieldName, *transaction.Allocations[allocationIndex].BookTypeID, BookTypeStrategy)
	}
	bodyBytes, err := httputil.NewRequest("GET", positionTagURL, nil)
	var positionTagWithMembersResponse positionTagResponse
	if err == nil {
		unmarshalErr := json.Unmarshal(bodyBytes, &positionTagWithMembersResponse)
		if unmarshalErr != nil {
			return fmt.Errorf("Invalid %s. Service response error", fieldName)
		}
	} else {
		if strings.Contains(err.Error(), "error:404") {
			return fmt.Errorf("Invalid %s. Records not found", fieldName)
		}
		return fmt.Errorf("Invalid %s. Service lookup failed", fieldName)
	}
	var returnErr = fmt.Errorf("Error")
	for _, positionTag := range value {
		for _, positionTagWithMember := range positionTagWithMembersResponse.Data.PositionTagWithMembers {
			for _, member := range positionTagWithMember.Members {
				if *positionTag.IndexAttributeID == *member.IndexAttributeID && *positionTag.IndexAttributeValueID == *member.IndexAttributeValueID {
					returnErr = nil
				}
			}
		}
		if returnErr != nil {
			return fmt.Errorf("Invalid %s for PortfolioId: %d. IndexAttributeID %d and indexAttributeValueID %d does not refernce a valid position tag", fieldName, *transaction.Allocations[allocationIndex].PortfolioID, int(*positionTag.IndexAttributeID), (*positionTag.IndexAttributeValueID))
		}
		returnErr = fmt.Errorf("Error")

	}
	return nil
}
