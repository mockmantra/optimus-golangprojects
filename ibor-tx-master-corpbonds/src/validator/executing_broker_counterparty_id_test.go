package validator

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/httputil"
)

var executingBrokerTestJSON = `{
	"Allocations": [
	  {
		"RouteId": 1,
		"PortfolioId" :2
	  },
	  {
		"PortfolioId": 12
	  }
	]
  }`

var errExecutingBroker = fmt.Errorf("Error in allocation PortfolioID 2. Invalid ExecutingBrokerCounterpartyId : 30 does not exist")
var errExecutingBroker1 = fmt.Errorf("Error in allocation PortfolioID 2. Invalid ExecutingBrokerCounterpartyId : 12 does not exist")
var errRoute = fmt.Errorf("Error in allocation PortfolioID 12. Invalid ExecutingBrokerCounterpartyId : RouteId missing")
var testCasesExecutingBroker = []struct {
	in  ExecutingBrokerCounterpartyID
	out error
}{
	{32, nil},
	{3, nil},
	{30, errExecutingBroker},
	{12, errExecutingBroker1},
}
var testCasesExecutingBrokerRoute = []struct {
	in  ExecutingBrokerCounterpartyID
	out error
}{
	{32, errRoute},
	{3, errRoute},
	{30, errRoute},
	{12, errRoute},
}

func TestExecutingBrokerCounterpartyIdValidWithRouteId(t *testing.T) {
	var testTransaction Transaction
	unMarshalError := json.Unmarshal([]byte(executingBrokerTestJSON), &testTransaction)
	assert.Nil(t, unMarshalError)
	str := "[{\"CounterpartyId\":32},{\"CounterpartyId\":3}]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()

	for _, testCase := range testCasesExecutingBroker {
		err := testCase.in.Validate(testTransaction, "ExecutingBrokerCounterpartyId", 0)
		assert.Equal(t, testCase.out, err)
	}
	httputil.UserSessionToken = ""
}

func TestExecutingBrokerCounterpartyIdWithoutRouteId(t *testing.T) {
	var testTransaction Transaction
	unMarshalError := json.Unmarshal([]byte(executingBrokerTestJSON), &testTransaction)
	assert.Nil(t, unMarshalError)
	str := "[{\"CounterpartyId\":32},{\"CounterpartyId\":3}]"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	httputil.UserSessionToken = "123"
	appconfig.BaseURI = ts.URL
	defer ts.Close()
	for _, testCase := range testCasesExecutingBrokerRoute {
		err := testCase.in.Validate(testTransaction, "ExecutingBrokerCounterpartyId", 1)
		assert.Equal(t, testCase.out, err)
	}
	httputil.UserSessionToken = ""
}

func TestExecutingBrokerCounterpartyIdWithoutUnmarshal(t *testing.T) {

	var brokerJSON = `{
		"Allocations": [
		  {
			"PortfolioId":12,
			"RouteId":1,
			"BookTypeId": 1,
			"Quantity":   200
		  },
		  {
			"PortfolioId":13,
			"BookTypeId": 1,
			"RouteId":1
			
		  }
		]
	}`
	var executingBrokerID ExecutingBrokerCounterpartyID = 12
	var testTransaction Transaction
	unMarshalError := json.Unmarshal([]byte(brokerJSON), &testTransaction)
	assert.Nil(t, unMarshalError)
	str := ""
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()
	err := executingBrokerID.Validate(testTransaction, "ExecutingBrokerCounterpartyID", 1)

	assert.Equal(t, err, fmt.Errorf("Error in portfolioID 13. ExecutingBrokerCounterpartyID : Unmarshlling error"))

	httputil.UserSessionToken = ""
}

func TestExecutingBrokerCounterpartyIdLookup(t *testing.T) {

	var brokerJSON = `{
		"Allocations": [
		  {
				"PortfolioId":12,
			"RouteId":1,
			"BookTypeId": 1,
			"Quantity":   200
		  },
		  {
				"PortfolioId":13,
			"BookTypeId": 1,
			"RouteId":1
			
		  }
		]
	}`
	var executingBrokerID ExecutingBrokerCounterpartyID = 12
	var testTransaction Transaction
	unMarshalError := json.Unmarshal([]byte(brokerJSON), &testTransaction)
	assert.Nil(t, unMarshalError)
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadGateway)
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()
	err := executingBrokerID.Validate(testTransaction, "ExecutingBrokerCounterpartyID", 1)

	assert.Equal(t, err, fmt.Errorf("Error in portfolioID %d. %s : Service lookup failed", int(*testTransaction.Allocations[1].PortfolioID), "ExecutingBrokerCounterpartyID"))

	httputil.UserSessionToken = ""
}
