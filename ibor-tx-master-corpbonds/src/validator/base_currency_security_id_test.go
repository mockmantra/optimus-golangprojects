package validator

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/httputil"
)

var baseCurrencySecurityIDTestJSON = `{
	"Allocations": [
	  {
		"Quantity":    200,
		"PortfolioId": 12
	  },
	  {
		"Quantity":    200,
		"PortfolioId": 12
	  }
	]
}`

func TestBaseCurrencySecurityIdWithInvalidCurrency(t *testing.T) {
	var baseCurrencySecurityID BaseCurrencySecurityID = 155
	str := `{ "data": {"portfolios": [{"baseCurrencyId":155, "id":4}]}}`
	currencyStr := "{\"data\":{\"currencies\":[{\"id\":155,\"cashSecurityId\":134}]}}"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.Index(r.URL.RawQuery, "currencies") > 0 {
			_, err := w.Write([]byte(currencyStr))
			if err != nil {
				t.Error("Unable to write the stream")
			}
		} else if strings.Index(r.URL.RawQuery, "portfolios") > 0 {
			_, err := w.Write([]byte(str))
			if err != nil {
				t.Error("Unable to write the stream")
			}
		}
	}))
	appconfig.BaseURI = ts.URL

	defer ts.Close()

	httputil.UserSessionToken = "123"

	fieldName := "BaseCurrencySecurityID"
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(baseCurrencySecurityIDTestJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)

	err := baseCurrencySecurityID.Validate(testTransaction, fieldName, 1)

	assert.Error(t, err, errors.New("Error in allocation PortfolioID 12. Invalid BaseCurrencySecurityID : Expected value 134, Provided Value 155"))

	httputil.UserSessionToken = ""
}

func TestBaseCurrencySecurityIDWithValidData(t *testing.T) {
	var baseCurrencySecurityID BaseCurrencySecurityID = 155
	str := `{ "data": {"portfolios": [{"baseCurrencyId":155, "id":4}]}}`
	currencyStr := "{\"data\":{\"currencies\":[{\"id\":155,\"cashSecurityId\":155}]}}"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.Index(r.URL.RawQuery, "currencies") > 0 {
			_, err := w.Write([]byte(currencyStr))
			if err != nil {
				t.Error("Unable to write the stream")
			}
		} else if strings.Index(r.URL.RawQuery, "portfolios") > 0 {
			_, err := w.Write([]byte(str))
			if err != nil {
				t.Error("Unable to write the stream")
			}
		}
	}))
	appconfig.BaseURI = ts.URL

	defer ts.Close()

	httputil.UserSessionToken = "123"

	fieldName := "BaseCurrencySecurityID"
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(baseCurrencySecurityIDTestJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)

	err := baseCurrencySecurityID.Validate(testTransaction, fieldName, 1)

	assert.Nil(t, err)

	httputil.UserSessionToken = ""
}

func TestBaseCurrencySecurityIDWithCurrenciesUnmarshalError(t *testing.T) {
	var baseCurrencySecurityID BaseCurrencySecurityID = 155
	str := `{"data": {"portfolios": [{"baseCurrencyId":155, "id":4}]}}`
	currencyStr := "{\"data\":currencies"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.Index(r.URL.RawQuery, "currencies") > 0 {
			_, err := w.Write([]byte(currencyStr))
			if err != nil {
				t.Error("Unable to write the stream")
			}
		} else if strings.Index(r.URL.RawQuery, "portfolios") > 0 {
			_, err := w.Write([]byte(str))
			if err != nil {
				t.Error("Unable to write the stream")
			}
		}
	}))
	appconfig.BaseURI = ts.URL

	defer ts.Close()

	httputil.UserSessionToken = "123"

	fieldName := "BaseCurrencySecurityID"
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(baseCurrencySecurityIDTestJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)

	err := baseCurrencySecurityID.Validate(testTransaction, fieldName, 1)

	assert.Error(t, err, errors.New("Error in portfolioID 12. BaseCurrencySecurityID : Unmarshalling error"))

	httputil.UserSessionToken = ""
}

func TestBaseCurrencySecurityIDWithCurrenciesServiceLookFailed(t *testing.T) {
	var baseCurrencySecurityID BaseCurrencySecurityID = 155
	str := `{ "data": {"portfolios": [{"baseCurrencyId":155, "id":4}]}}`
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.Index(r.URL.RawQuery, "currencies") > 0 {
			w.WriteHeader(http.StatusBadGateway)
		} else if strings.Index(r.URL.RawQuery, "portfolios") > 0 {
			_, err := w.Write([]byte(str))
			if err != nil {
				t.Error("Unable to write the stream")
			}
		}
	}))
	appconfig.BaseURI = ts.URL

	defer ts.Close()

	httputil.UserSessionToken = "123"

	fieldName := "BaseCurrencySecurityID"
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(baseCurrencySecurityIDTestJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)

	err := baseCurrencySecurityID.Validate(testTransaction, fieldName, 1)

	assert.Error(t, err, errors.New("Error in portfolioID 12. BaseCurrencySecurityID : Unmarshalling error"))

	httputil.UserSessionToken = ""
}

func TestBaseCurrencySecurityIDWIthInValidData(t *testing.T) {
	var baseCurrencySecurityID BaseCurrencySecurityID = 155
	str := `{ "data": {"portfolios": [{"baseCurrencyId":134, "id":4}]}}`
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"

	defer ts.Close()

	fieldName := "BaseCurrencySecurityID"
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(baseCurrencySecurityIDTestJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)

	err := baseCurrencySecurityID.Validate(testTransaction, fieldName, 1)

	assert.Equal(t, err, fmt.Errorf("Error in allocation PortfolioID %d. Portfolio has invalid %s", int(*testTransaction.Allocations[0].PortfolioID), fieldName))
	httputil.UserSessionToken = ""
}

func TestBaseCurrencySecurityIDAPICallError(t *testing.T) {
	var baseCurrencySecurityID BaseCurrencySecurityID = 155
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"

	defer ts.Close()

	fieldName := "BaseCurrencySecurityID"
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(baseCurrencySecurityIDTestJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	err := baseCurrencySecurityID.Validate(testTransaction, fieldName, 1)
	assert.Equal(t, err, fmt.Errorf("Invalid %s : PortfolioID %d does not exist", fieldName, int(*testTransaction.Allocations[0].PortfolioID)))

	httputil.UserSessionToken = ""
}

func TestBaseCurrencySecurityIDAPICallErrorLookup(t *testing.T) {
	var baseCurrencySecurityID BaseCurrencySecurityID = 155
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadGateway)
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"

	defer ts.Close()

	fieldName := "BaseCurrencySecurityID"
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(baseCurrencySecurityIDTestJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	err := baseCurrencySecurityID.Validate(testTransaction, fieldName, 1)
	assert.EqualError(t, err, "Error in allocation portfolioID 12. BaseCurrencySecurityID : Service lookup failed")

	httputil.UserSessionToken = ""
}
