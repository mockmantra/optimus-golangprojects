package validator

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

var feeJSON = `{
	"Allocations": [
		{
			"BookTypeId" : 2,
			"PortfolioId" :12,
		  "ExecQuantity": 100,
		  "Fees": 10,
		  "FeeCharges": [
			{
			  "FeeCharge": 10,
			  "FeeChargeId": 22
			},
			{
			  "FeeCharge": 0,
			  "FeeChargeId": 24
			}
		  ]
		},
		{
			"BookTypeId" : 2,
			"PortfolioId" :13,
			"ExecQuantity": 0,
			"Fees": 10,
			"FeeCharges": [
			  {
				"FeeCharge": 10,
				"FeeChargeId": 22
			  },
			  {
				"FeeCharge": 0,
				"FeeChargeId": 24
			  }
			]
		},
		{
			"BookTypeId" : 3,
			"PortfolioId" :14,
			"ExecQuantity": 100,
			"Fees": 10,
			"FeeCharges": []
		},
		{
			"BookTypeId" : 3,
			"PortfolioId" :15,
			"ExecQuantity": 100,
			"Fees": 10,
			"FeeCharges": []
		},
		{
			"BookTypeId" : 2,
			"PortfolioId" :15,
			"ExecQuantity": 100,
			"Fees": 0
		}
	]
}`

func TestFeeValid(t *testing.T) {
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(feeJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	err := testTransaction.Allocations[0].Fees.Validate(testTransaction, "Fees", 0)
	assert.Nil(t, err)
}

func TestFeeInvalidExecQuantity(t *testing.T) {
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(feeJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	err := testTransaction.Allocations[1].Fees.Validate(testTransaction, "Fees", 1)
	assert.NotNil(t, err)
}

func TestFeesValidFeesForStrategy(t *testing.T) {
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(feeJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	err := testTransaction.Allocations[2].Fees.Validate(testTransaction, "Fees", 2)
	assert.Nil(t, err)
}

func TestFeeInvalidFeeCharges(t *testing.T) {
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(feeJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	err := testTransaction.Allocations[4].Fees.Validate(testTransaction, "Fees", 4)
	assert.Equal(t, err, fmt.Errorf("Error in portfolioID 15. Invalid Fees : Value should be present"))
}
func TestFeeValidSumForStrategyBook(t *testing.T) {
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(feeJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	err := testTransaction.Allocations[2].Fees.Validate(testTransaction, "Fees", 2)
	assert.Nil(t, err)
}

var feesJSON = `{
	"Allocations": [
		{
			"BookTypeId" : 2,
			"PortfolioId" :12,
		  "ExecQuantity": 100,
		  "FeeCharges": [
			{
			  "FeeCharge": 0
			  "FeeChargeId": 22
			}
		  ]
		},
		{
			"BookTypeId" : 2,
			"PortfolioId" :13,
			"ExecQuantity": 0,
			"Fees": 10,
			"FeeCharges": [
			  {
				"FeeCharge": 10,
				"FeeChargeId": 22
			  },
			  {
				"FeeCharge": 0,
				"FeeChargeId": 24
			  }
			]
		},
		{
			"BookTypeId" : 3,
			"PortfolioId" :14,
			"ExecQuantity": 100,
			"Fees": 10,
			"FeeCharges": []
		},
		{
			"BookTypeId" : 3,
			"PortfolioId" :15,
			"ExecQuantity": 100,
			"Fees": 10,
			"FeeCharges": []
		},
		{
			"BookTypeId" : 2,
			"PortfolioId" :15,
			"ExecQuantity": 100,
			"Fees": 0
		}
	]
}`

func TestFeeValidWhenNotPresentInAllAllocations(t *testing.T) {
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(feeJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	err := testTransaction.Allocations[2].Fees.Validate(testTransaction, "Fees", 2)
	assert.Nil(t, err)
}
