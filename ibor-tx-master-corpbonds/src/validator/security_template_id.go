package validator

import (
	"fmt"
	"strings"
)

// SecurityTemplateID field
type SecurityTemplateID int

// Validate validates the field
func (value SecurityTemplateID) Validate(transaction Transaction, fieldName string) error {
	eventTypes, err := NewGetEventTypeForTemplateRequest(int(value))
	if err == nil {
		if len(eventTypes) == 0 {
			return fmt.Errorf("Invalid %s. There are no items", fieldName)
		}
	} else {
		if strings.Contains(err.Error(), "error:404") {
			return fmt.Errorf("Invalid %s. SecurityTemplateId: %d does not exist", fieldName, value)
		}
		return fmt.Errorf("Invalid %s. Service lookup failed", fieldName)
	}
	return nil
}
