package validator

import (
	"fmt"
	"reflect"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/utils"
)

// Fee type
type Fee float64

// Validate validates the field
func (value Fee) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	if !reflect.ValueOf(transaction.Allocations[allocationIndex].ExecQuantity).IsNil() && int(*transaction.Allocations[allocationIndex].ExecQuantity) > 0 {
		if !reflect.ValueOf(transaction.Allocations[allocationIndex].FeeCharges).IsNil() {
			var feeTotal float64
			for _, feeCharges := range *transaction.Allocations[allocationIndex].FeeCharges {
				feeTotal += float64(*feeCharges.FeeCharge)
			}

			if *transaction.Allocations[allocationIndex].BookTypeID != BookTypeStrategy {
				roundedFeeTotal := utils.Round(float64(feeTotal), 0.5, 4)
				roundedFee := utils.Round(float64(value), 0.5, 4)
				if roundedFeeTotal == roundedFee {
					return nil
				}
				return fmt.Errorf("Error in portfolioID %d. Invalid %s : Expected value %f, Provided value %f", int(*transaction.Allocations[allocationIndex].PortfolioID), fieldName, roundedFee, roundedFeeTotal)

			}
			var locationFeeTotal float64
			var strategyFeeTotal float64

			for _, allocation := range transaction.Allocations {
				if allocation.Fees != nil {
					if *allocation.BookTypeID == BookTypeLocation {
						locationFeeTotal += float64(*allocation.Fees)
					} else if *allocation.BookTypeID == BookTypeStrategy {
						strategyFeeTotal += float64(*allocation.Fees)
					}
				}
			}
			if utils.Round(locationFeeTotal, 0.5, 4) == utils.Round(strategyFeeTotal, 0.5, 4) {
				return nil
			}
			return fmt.Errorf("Error in portfolioID %d. Invalid %s for BookTypeId : 3 ", int(*transaction.Allocations[allocationIndex].PortfolioID), fieldName)
		}
		return fmt.Errorf("Error in portfolioID %d. Invalid %s : Value should be present", int(*transaction.Allocations[allocationIndex].PortfolioID), fieldName)
	}
	return fmt.Errorf("Invalid %s for allocation portfolioID %d. Expected : ExecQuantity not valid", fieldName, int(*transaction.Allocations[allocationIndex].PortfolioID))
}
