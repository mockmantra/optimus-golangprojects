package validator

// Validator interface
type Validator interface {
	Validate(transaction Transaction, fieldName string) error
}

// Transaction transaction
type Transaction struct {
	Allocations              []Allocation           `json:"Allocations"`
	BusinessDateTimeUTC      BusinessDateTimeUTC    `json:"BusinessDateTimeUTC"`
	EventTypeID              EventTypeID            `json:"EventTypeId"`
	LocalCurrencySecurityID  CurrencySecurityID     `json:"LocalCurrencySecurityId"`
	OrderCreatedDate         string                 `json:"OrderCreatedDate"`
	OrderQuantity            Quantity               `json:"OrderQuantity"`
	SecurityID               SecurityID             `json:"SecurityId"`
	SecurityTemplateID       SecurityTemplateID     `json:"SecurityTemplateId"`
	SequenceNumber           int                    `json:"SequenceNumber"`
	SettleDate               TransactionDate        `json:"SettleDate"`
	SourceSystemName         string                 `json:"SourceSystemName"`
	SourceSystemReference    SourceSystemReference  `json:"SourceSystemReference"`
	SystemCurrencySecurityID CurrencySecurityID     `json:"SystemCurrencySecurityId"`
	SystemToLocalFXRate      SystemToLocalFXRate    `json:"SystemToLocalFXRate"`
	TradeDate                TransactionDate        `json:"TradeDate"`
	TransactionDateTimeUTC   TransactionDateTimeUTC `json:"TransactionDateTimeUTC"`
	TransactionTimeZoneID    TimeZoneID             `json:"TransactionTimeZoneId"`
	UserTimeZoneID           TimeZoneID             `json:"UserTimeZoneId"`
	Yield                    float64                `json:"Yield"`
}
