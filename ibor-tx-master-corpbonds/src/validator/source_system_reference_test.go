package validator

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/httputil"
)

func TestSourceSystemReference_WhenNotaValidInteger(t *testing.T) {
	var sourceSystemReference SourceSystemReference = "7x"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadGateway)
	}))

	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SourceSystemReference"
	transaction := Transaction{
		SourceSystemName: "Ims-Trading",
	}
	httputil.UserSessionToken = "123"

	err := sourceSystemReference.Validate(transaction, key)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. Not a valid integer value: %s", key, sourceSystemReference))
	httputil.UserSessionToken = ""
}
func TestSourceSystemReference_WithErrorInHttpCall(t *testing.T) {
	var sourceSystemReference SourceSystemReference = "7"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadGateway)
	}))
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SourceSystemReference"
	transaction := Transaction{
		SourceSystemName: "ims-trading",
	}
	httputil.UserSessionToken = "123"

	err := sourceSystemReference.Validate(transaction, key)

	assert.EqualError(t, err, fmt.Sprintf("%s: Service lookup failed", key))
	httputil.UserSessionToken = ""
}
func TestSourceSystemReference_WithoutValidResponse(t *testing.T) {
	var sourceSystemReference SourceSystemReference = "7"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	}))
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SourceSystemReference"
	transaction := Transaction{
		SourceSystemName: "ims-tRAding",
	}
	httputil.UserSessionToken = "123"

	err := sourceSystemReference.Validate(transaction, key)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. OrderId: %s does not exist", key, sourceSystemReference))
	httputil.UserSessionToken = ""
}
func TestSourceSystemReference_WithValidResponseWithOutItems(t *testing.T) {
	var sourceSystemReference SourceSystemReference = "7"
	str := "{\"Items\":[]}"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	httputil.UserSessionToken = "123"

	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SourceSystemReference"
	transaction := Transaction{
		SourceSystemName: "IMS-TRADing",
	}

	err := sourceSystemReference.Validate(transaction, key)
	assert.EqualError(t, err, "Invalid SourceSystemReference. There are no items")

	httputil.UserSessionToken = ""

}
func TestSourceSystemReference_WithValidResponseWithItems(t *testing.T) {
	var sourceSystemReference SourceSystemReference = "7"
	str := "{\"Items\":[{\"OrderId\":7}]}"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))

	httputil.UserSessionToken = "123"

	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SourceSystemReference"
	transaction := Transaction{
		SourceSystemName: "Ims-TRadiNG",
	}

	err := sourceSystemReference.Validate(transaction, key)
	assert.Nil(t, err)

	httputil.UserSessionToken = ""
}

func TestSourceSystemReference_WithNonTradingSourceSystemName(t *testing.T) {
	var sourceSystemReference SourceSystemReference = "7"
	httputil.UserSessionToken = "123"
	key := "SourceSystemReference"
	transaction := Transaction{
		SourceSystemName: "OMS",
	}

	err := sourceSystemReference.Validate(transaction, key)
	assert.Nil(t, err)
}
