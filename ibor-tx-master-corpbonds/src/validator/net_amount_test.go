package validator

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNetAmount_WhenExecQuantityIsNotGreaterThanZero(t *testing.T) {
	var netAmount NetAmount = 15.0123
	var netAmounttJSON = `{
		"Allocations": [
		  {
				"PortfolioId" :12,
			"BookTypeId": 1,
			"Quantity":   200,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12,
			"ExecQuantity": 0
		  },
		  {
				"PortfolioId" :13,
			"BookTypeId": 1,
			"Quantity":   200,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12,
			"ExecQuantity": 0
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(netAmounttJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	fieldName := "NetAmount"
	err := netAmount.Validate(testTransaction, fieldName, 0)

	assert.Equal(t, err, fmt.Errorf("Invalid %s for allocation with PortfolioID %d. Invalid Exec Quantity provided", fieldName, int(*testTransaction.Allocations[0].PortfolioID)))
}
func TestNetAmount_WhenPricipalAmountIsNotEqualToSumOfPrincipalAmountCommissionAndFees(t *testing.T) {
	var netAmount NetAmount = 37.00
	var netAmounttJSON = `{
		"EventTypeID" : 6,
		"Allocations": [
		  {
				"PortfolioId" :13,
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity": 2,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12
		  },
		  {
				"PortfolioId" :14,
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity": 2,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(netAmounttJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	fieldName := "NetAmount"
	err := netAmount.Validate(testTransaction, fieldName, 0)
	assert.Equal(t, err, fmt.Errorf("Error in allocation portfolioID %d. Invalid %s - Expected value :%f, Provided value : %f", int(*testTransaction.Allocations[0].PortfolioID), fieldName, 36.000000, netAmount))
}
func TestNetAmount_WhenPricipalAmountIsNotEqualToDifferenceOfPrincipalAmountCommissionAndFees(t *testing.T) {
	var netAmount NetAmount = 32.00
	var netAmounttJSON = `{
		"EventTypeID" : 14,
		"Allocations": [
		  {
				"PortfolioId" :13,
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity": 2,
			"Fees":            12,
			"PrincipalAmount": 30,
			"Commission":      12
		  },
		  {
				"PortfolioId" :14,
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity": 2,
			"Fees":            12,
			"PrincipalAmount": 36,
			"Commission":      12
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(netAmounttJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	fieldName := "NetAmount"
	err := netAmount.Validate(testTransaction, fieldName, 0)
	assert.Equal(t, err, fmt.Errorf("Error in allocation portfolioID %d. Invalid %s - Expected value :%f, Provided value : %f", int(*testTransaction.Allocations[0].PortfolioID), fieldName, 6.000000, netAmount))
}

func TestNetAmount_WhenPricipalAmountIsEqualToSumOfPrincipalAmountCommissionAndFees(t *testing.T) {
	var netAmount NetAmount = 36.00
	var netAmounttJSON = `{
		"EventTypeID" : 3,
		"Allocations": [
		  {
				"PortfolioId" :13,
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity": 2,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12
		  },
		  {
				"PortfolioId" :14,
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity": 2,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(netAmounttJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	fieldName := "NetAmount"
	err := netAmount.Validate(testTransaction, fieldName, 0)
	assert.Nil(t, err)
}
func TestNetAmount_WhenPricipalAmountIsEqualToDifferenceOfPrincipalAmountCommissionAndFees(t *testing.T) {
	var netAmount NetAmount = 28.00
	var netAmountJSON = `{
		"EventTypeID" : 14,
		"Allocations": [
		  {
				"PortfolioId" :13,
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity": 2,
			"Fees":            0,
			"PrincipalAmount": 40,
			"Commission":      12
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(netAmountJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	fieldName := "NetAmount"
	err := netAmount.Validate(testTransaction, fieldName, 0)
	assert.Nil(t, err)
}

func TestNetAmount_WhenPricipalAmountIsMissing(t *testing.T) {
	var netAmount NetAmount = 36.00
	var netAmounttJSON = `{
		"EventTypeID" : 14,
		"Allocations": [
		  {
				"PortfolioId" :13,
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity": 2,
			"Fees":            12,
			"Commission":      12
		  },
		  {
				"PortfolioId" :14,
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity": 2,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(netAmounttJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	fieldName := "NetAmount"
	err := netAmount.Validate(testTransaction, fieldName, 0)
	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. Principal Amount value is missing for PortfolioId: %d", fieldName, *testTransaction.Allocations[0].PortfolioID))
}

func TestNetAmount_WhenCommissionIsMissing(t *testing.T) {
	var netAmount NetAmount = 36.00
	var netAmounttJSON = `{
		"EventTypeID" : 3,
		"Allocations": [
		  {
				"PortfolioId" :13,
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity": 2,
			"Fees":            12,
			"PrincipalAmount": 12
		  },
		  {
				"PortfolioId" :14,
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity": 2,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(netAmounttJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	fieldName := "NetAmount"
	err := netAmount.Validate(testTransaction, fieldName, 0)
	assert.EqualError(t, err, fmt.Sprintf("Error in allocation portfolioID %d. Invalid %s - Expected value :24.000000, Provided value : 36.000000", *testTransaction.Allocations[0].PortfolioID, fieldName))
}

func TestNetAmount_WhenFeesIsMissing(t *testing.T) {
	var netAmount NetAmount = 36.00
	var netAmounttJSON = `{
		"EventTypeID" : 3,
		"Allocations": [
		  {
				"PortfolioId" :13,
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity": 2,
			"PrincipalAmount": 12,
			"Commission":      12
		  },
		  {
				"PortfolioId" :14,
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity": 2,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(netAmounttJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	fieldName := "NetAmount"
	err := netAmount.Validate(testTransaction, fieldName, 0)
	assert.EqualError(t, err, fmt.Sprintf("Error in allocation portfolioID %d. Invalid %s - Expected value :24.000000, Provided value : 36.000000", *testTransaction.Allocations[0].PortfolioID, fieldName))
}

func TestNetAmount_WhenPricipalAmountIsEqualToSumOfPrincipalAmountCommissionAndFeesValid(t *testing.T) {
	var netAmounttJSON = `{
		"EventTypeID" : 3,
		"Allocations": [
		  {
			"PortfolioId" :13,
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity": 2,
			"Fees":            1.2,
			"PrincipalAmount": 1.23456,
			"Commission":      1.454455
		  },
		  {
			"PortfolioId" :14,
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity": 2,
			"Fees":            1.2,
			"PrincipalAmount": 1.23456,
			"Commission":      1.454455
		  }
		]
	}`
	var netAmount NetAmount = 3.889015
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(netAmounttJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	fieldName := "NetAmount"
	err := netAmount.Validate(testTransaction, fieldName, 0)
	assert.Nil(t, err)
}
func TestNetAmount_WhenPricipalAmountIsEqualToDifferenceOfPrincipalAmountCommissionAndFeesValid(t *testing.T) {
	var netAmounttJSON = `{
		"EventTypeID" : 14,
		"Allocations": [
		  {
			"PortfolioId" :13,
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity": 2,
			"Fees":            1.2,
			"PrincipalAmount": 10.23456,
			"Commission":      1.454455
		  }
		]
	}`
	var netAmount NetAmount = 7.580105
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(netAmounttJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	fieldName := "NetAmount"
	err := netAmount.Validate(testTransaction, fieldName, 0)
	assert.Nil(t, err)
}

func TestNetAmount_WhenPricipalAmountIsEqualToSumOfPrincipalAmountCommissionAndFeesValidPrecision(t *testing.T) {
	var netAmounttJSON = `{
		"EventTypeID" : 3,
		"Allocations": [
		  {
				"BaseCurrencySecurityId": 157,
				"BaseToLocalFxRate": 0.76432,
				"BookTypeId": 2,
				"Commission": 0,
				"Fees":0,
				"CommissionCharges": [],
				"CustodianCounterpartyAccountId": 1,
				"ExecQuantity": 99873,
				"ExecutingBrokerCounterpartyId": 5,
				"FeeCharges": [],
				"IsFinalized": false,
				"NetAmount": 364191.88814999996,
				"PortfolioId": 1,
				"PositionTags": null,
				"PrincipalAmount": 364191.88814999996,
				"Quantity": 803900,
				"RouteId": 9406,
				"RouteName": "BTIG-DESK",
				"SettleAmount": 364191.88814999996,
				"SettleCurrencySecurityId": 172,
				"SettlePrice": 3.64655,
				"SettleToBaseFXRate": 1.30837,
				"SettleToLocalFXRate": 1,
				"SettleToSystemFXRate": 1.30837
			}
		]
	}`
	var netAmount NetAmount = 364191.88814999996
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(netAmounttJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	fieldName := "NetAmount"
	err := netAmount.Validate(testTransaction, fieldName, 0)
	assert.Nil(t, err)
}
