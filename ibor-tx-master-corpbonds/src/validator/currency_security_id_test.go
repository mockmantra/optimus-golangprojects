package validator

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/httputil"
)

func TestCurrencySecurityId_WithValidSecurityIdAndAssetClassIsNotCash(t *testing.T) {
	var currencySecurityID CurrencySecurityID = 5
	str := `{"data": {"securities": [{"id": 5,"symbol": "USN","assetClass": {"id": 3,"name": "NonCash"}}]}}`
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))

	httputil.UserSessionToken = "123"

	appconfig.BaseURI = ts.URL
	defer ts.Close()
	transaction := Transaction{}

	err := currencySecurityID.Validate(transaction, "CurrencySecurityId")

	assert.Equal(t, err, fmt.Errorf("Invalid %s. Expected AssetClass : Cash, Provided Assetclass : %s", "CurrencySecurityId", "NonCash"))

	httputil.UserSessionToken = ""
}
func TestCurrencySecurityId_WithValidSecurityIdAndAssetClassIsCash(t *testing.T) {
	var currencySecurityID CurrencySecurityID = 4
	str := `{"data": {"securities": [{"id": 5,"symbol": "USN","assetClass": {"id": 4,"name": "Cash"}}]}}`
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"

	defer ts.Close()
	transaction := Transaction{}

	err := currencySecurityID.Validate(transaction, "CurrencySecurityId")

	assert.Nil(t, err)

	httputil.UserSessionToken = ""
}
func TestCurrencySecurityId_WithInvalidSecurityId(t *testing.T) {
	var currencySecurityID CurrencySecurityID = 1
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	}))
	appconfig.BaseURI = ts.URL

	defer ts.Close()

	httputil.UserSessionToken = "123"

	transaction := Transaction{}

	err := currencySecurityID.Validate(transaction, "CurrencySecurityId")

	assert.EqualError(t, err, "Invalid CurrencySecurityId. CurrencySecurityId 1  does not exist")

	httputil.UserSessionToken = ""
}

func TestCurrencySecurityId_WithInvalidSecurityIdLookup(t *testing.T) {
	var currencySecurityID CurrencySecurityID = 1
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadGateway)
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"

	defer ts.Close()
	transaction := Transaction{}

	err := currencySecurityID.Validate(transaction, "CurrencySecurityId")

	assert.Equal(t, err, fmt.Errorf("%s : Service lookup failed", "CurrencySecurityId"))

	httputil.UserSessionToken = ""
}

func TestCurrencySecurityId_WithUnmarhsalError(t *testing.T) {
	var currencySecurityID CurrencySecurityID = 4
	str := ""
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"

	defer ts.Close()
	transaction := Transaction{}

	err := currencySecurityID.Validate(transaction, "CurrencySecurityId")

	assert.Equal(t, err, fmt.Errorf("%s : Service lookup failed", "CurrencySecurityId"))

	httputil.UserSessionToken = ""
}
func TestCurrencySecurityId_WithGetSecuritiesMethodSuccess(t *testing.T) {
	str := `
	{
		"data": {
		  "securities": [
			{
			  "id": 1234,
			  "symbol": "USN",
			  "assetClass": {
				"id": 3,
				"name": "Cash"
			  }
			},
			{
				"id": 1235,
				"symbol": "USD",
				"assetClass": {
				  "id": 3,
				  "name": "Cash"
				}
			  },
			  {
				"id": 1245,
				"symbol": "GBP",
				"assetClass": {
				  "id": 3,
				  "name": "Cash"
				}
			  },
			  {
				"id": 1456,
				"symbol": "JPY",
				"assetClass": {
				  "id": 3,
				  "name": "Cash"
				}
			  }
		  ]
		}
	  }
	`
	var currencySecurityID CurrencySecurityID = 1245
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL

	httputil.UserSessionToken = "123"
	err := GetSecurities([]int{1234, 1235, 1245, 1456})
	if err != nil {
		t.Error("Error in GetSecurities")
	}
	defer ts.Close()
	transaction := Transaction{}

	err = currencySecurityID.Validate(transaction, "CurrencySecurityId")

	assert.Equal(t, err, nil)

	httputil.UserSessionToken = ""
}
