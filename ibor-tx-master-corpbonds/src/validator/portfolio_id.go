package validator

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/httputil"
)

const portfolioURL string = "coredata/v2/graphql?query={portfolios(ids:%v){id,baseCurrencyId}}"

// Portfolio type
type Portfolio struct {
	PortfolioID    int `json:"id"`
	BaseCurrencyID int `json:"baseCurrencyId"`
}

// portfolioResponse response
type portfolioResponse struct {
	Data struct {
		Portfolios []Portfolio `json:"portfolios"`
	} `json:"data"`
}

// PortfolioID type
type PortfolioID int

// Validate validates the PortfolioID
func (value PortfolioID) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	portfolio, err := NewPortfolioRequest(int(value))
	if err != nil {
		if strings.Contains(err.Error(), "error:404") {
			return fmt.Errorf("Invalid %s: %d does not exist", fieldName, value)
		}
		return fmt.Errorf("Error in allocation portfolioID %d. %s : Service lookup failed", value, fieldName)
	} else if portfolio.PortfolioID != int(value) {
		return fmt.Errorf("Invalid %s: Expected PortfolioID %d, Provided PortfolioID %d", fieldName, portfolio.PortfolioID, int(value))
	}
	return nil
}

// NewPortfolioRequest fetches the Portfolio by PortfolioID
func NewPortfolioRequest(portfolioID int) (Portfolio, error) {
	bodyBytes, err := httputil.NewRequest("GET", fmt.Sprintf(portfolioURL, fmt.Sprintf("[%d]", portfolioID)), nil)
	var portfolioResponse portfolioResponse
	var portfolio Portfolio
	if err == nil {
		err = json.Unmarshal(bodyBytes, &portfolioResponse)
		if err == nil {
			if len(portfolioResponse.Data.Portfolios) > 0 {
				portfolio = portfolioResponse.Data.Portfolios[0]
				return portfolio, nil
			}
			return portfolio, errors.New("error:404")
		}
	}
	return portfolio, err
}
