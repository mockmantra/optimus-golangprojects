package validator

import (
	"encoding/json"
	"fmt"
	"strings"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/httputil"
)

// Currency ..
type Currency struct {
	ID             int `json:"id"`
	CashSecurityID int `json:"cashSecurityId"`
}

//GraphQLCurrencyResponse ..
type GraphQLCurrencyResponse struct {
	Data struct {
		Currencies []Currency `json:"currencies"`
	} `json:"data"`
}

const currencyGraphQL = "coredata/v2/graphql?query={currencies(id:%d){id,cashSecurityId}}"

// BaseCurrencySecurityID field
type BaseCurrencySecurityID int

// Validate validates the fields
func (value BaseCurrencySecurityID) Validate(transaction Transaction, fieldName string, allocationIndex int) error {
	currentAllocation := transaction.Allocations[allocationIndex]
	currencyPortfolio, err := NewPortfolioRequest(int(*currentAllocation.PortfolioID))
	if err == nil {
		bodyBytes, err := httputil.NewRequest("GET", fmt.Sprintf(currencyGraphQL, currencyPortfolio.BaseCurrencyID), nil)
		var currencyResponse GraphQLCurrencyResponse
		if err == nil {
			unmarshalErr := json.Unmarshal(bodyBytes, &currencyResponse)
			if unmarshalErr == nil {
				if len(currencyResponse.Data.Currencies) > 0 {
					cashSecurityID := currencyResponse.Data.Currencies[0].CashSecurityID
					if cashSecurityID == int(value) {
						return nil
					}
					return fmt.Errorf("Error in allocation PortfolioID %d. Invalid %s : Expected value %d, Provided Value %d", int(*currentAllocation.PortfolioID), fieldName, cashSecurityID, value)
				}
				return fmt.Errorf("Error in allocation PortfolioID %d. Portfolio has invalid %s", int(*currentAllocation.PortfolioID), fieldName)
			}
			return fmt.Errorf("Error in portfolioID %d. BaseCurrencySecurityID : Unmarshalling error", int(*currentAllocation.PortfolioID))
		}
		return fmt.Errorf("Error in allocation portfolioID %d. %s : Currency GraphQL Service lookup failed", int(*currentAllocation.PortfolioID), fieldName)
	} else if strings.Contains(err.Error(), "error:404") {
		return fmt.Errorf("Invalid %s : PortfolioID %d does not exist", fieldName, int(*currentAllocation.PortfolioID))
	}
	return fmt.Errorf("Error in allocation portfolioID %d. %s : Service lookup failed", int(*currentAllocation.PortfolioID), fieldName)
}
