package validator

import (
	"encoding/json"
	"fmt"
	"reflect"
	"strings"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/httputil"
)

// FeeChargeID type
type FeeChargeID int

const feeURL = "referencedata/v1/GetFeeById?chargeId=%d"

type feeResponse []struct {
	ID int `json:"Id"`
}

// Validate validates the field
func (value FeeChargeID) Validate(transaction Transaction, fieldName string, allocationIndex int, nestedIndex int) error {
	if !reflect.ValueOf(transaction.Allocations[allocationIndex].ExecQuantity).IsNil() && int(*transaction.Allocations[allocationIndex].ExecQuantity) > 0 {
		feeCharges := *transaction.Allocations[allocationIndex].FeeCharges
		if !reflect.ValueOf(feeCharges[nestedIndex].FeeCharge).IsNil() {
			bodyBytes, err := httputil.NewRequest("GET", fmt.Sprintf(feeURL, int(value)), nil)
			var feeResponse feeResponse
			if err == nil {
				error := json.Unmarshal(bodyBytes, &feeResponse)
				if error == nil && len(feeResponse) > 0 {
					return nil
				}
				return fmt.Errorf("Error in portfolioID %d. FeeChargeID : Unmarshlling error", int(*transaction.Allocations[allocationIndex].PortfolioID))
			} else if strings.Contains(err.Error(), "error:404") {
				return fmt.Errorf("Error in portfolioID %d. Invalid FeeChargeID : %d does not exist", int(*transaction.Allocations[allocationIndex].PortfolioID), int(*feeCharges[nestedIndex].FeeChargeID))
			}
			return fmt.Errorf("Error in allocation portfolioID %d. FeeChargeID : Service lookup failed", int(*transaction.Allocations[allocationIndex].PortfolioID))
		}
		return fmt.Errorf("Error in allocation portfolioID %d. Invalid FeeChargeID, Missing FeeCharge for FeeChargeID %d", int(*transaction.Allocations[allocationIndex].PortfolioID), int(*feeCharges[nestedIndex].FeeChargeID))
	}
	return fmt.Errorf("Error in allocation portfolioID %d. Invalid FeeChargeID : ExecQuantity not valid", int(*transaction.Allocations[allocationIndex].PortfolioID))

}
