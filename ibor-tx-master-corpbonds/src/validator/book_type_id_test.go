package validator

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

var bookTypeIDTestJSON = `{
	"Allocations": [
	  {
		"BookTypeId": 3,
		"Quantity":    200,
		"PortfolioId": 12
	  },
	  {
		"BookTypeId": 1,
		"Quantity":    200,
		"PortfolioId": 12
	  }
	]
}`

func TestBookTypeIDValid(t *testing.T) {
	var testTransaction Transaction
	unMarshalErr := json.Unmarshal([]byte(bookTypeIDTestJSON), &testTransaction)
	assert.Nil(t, unMarshalErr)
	err := testTransaction.Allocations[0].BookTypeID.Validate(testTransaction, "BookTypeID", 0)
	assert.Nil(t, err)
}

func TestBookTypeIDInValid(t *testing.T) {
	var testTransaction Transaction
	unMarshalErr := json.Unmarshal([]byte(bookTypeIDTestJSON), &testTransaction)
	assert.Nil(t, unMarshalErr)
	err := testTransaction.Allocations[1].BookTypeID.Validate(testTransaction, "BookTypeID", 0)
	assert.EqualError(t, err, "Error in allocation PortfolioID 12. Invalid BookTypeID : Expected 2 (for Location) or 3 (for Strategy), Provided value 1")
}
