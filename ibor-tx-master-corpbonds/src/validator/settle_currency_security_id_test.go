package validator

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/httputil"
)

func TestSettleCurrencySecurityIDValidErr(t *testing.T) {
	var currencySecurityID SettleCurrencySecurityID = 5
	str := `{"data": {"securities": [{"id": 5,"symbol": "USN","assetClass": {"id": 3,"name": "NonCash"}}]}}`
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))

	httputil.UserSessionToken = "123"

	var currencyJSON = `{
		"Allocations": [
		  {
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity":12,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12
		  },
		  {
			"BookTypeId": 1,
			"PortfolioId": 2,
			"Quantity":   200,
			"ExecQuantity":12,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(currencyJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)
	appconfig.BaseURI = ts.URL
	defer ts.Close()
	err := currencySecurityID.Validate(testTransaction, "SettleCurrencySecurityID", 1)

	assert.EqualError(t, err, fmt.Sprintf("Invalid SettleCurrencySecurityID. The Asset Class name should have been Cash but instead it is NonCash for PortfolioId: %d", *testTransaction.Allocations[1].PortfolioID))
	httputil.UserSessionToken = ""
}

func TestSettleCurrencySecurityId_WithValidSecurityIdAndAssetClassIsCash(t *testing.T) {
	var settleCurrencySecurityID SettleCurrencySecurityID = 4
	str := `{"data": {"securities": [{"id": 4,"symbol": "USN","assetClass": {"id": 3,"name": "Cash"}}]}}`
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(str))
		if err != nil {
			t.Error("Unable to write the stream")
		}
	}))
	appconfig.BaseURI = ts.URL
	defer ts.Close()

	httputil.UserSessionToken = "123"

	var currencyJSON = `{
		"Allocations": [
		  {
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity":12,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12
		  },
		  {
			"BookTypeId": 1,
			"PortfolioId": 2,
			"Quantity":   200,
			"ExecQuantity":12,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(currencyJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)

	err := settleCurrencySecurityID.Validate(testTransaction, "SettleCurrencySecurityID", 1)

	assert.Nil(t, err)

	httputil.UserSessionToken = ""
}

func TestSettleCurrencySecurityId_WithInvalidSecurityId(t *testing.T) {
	var settleCurrencySecurityID SettleCurrencySecurityID = 1
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	}))
	appconfig.BaseURI = ts.URL
	httputil.UserSessionToken = "123"
	defer ts.Close()
	var currencyJSON = `{
		"Allocations": [
		  {
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity":12,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12
		  },
		  {
			"BookTypeId": 1,
			"PortfolioId": 2,
			"Quantity":   200,
			"ExecQuantity":12,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(currencyJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)

	err := settleCurrencySecurityID.Validate(testTransaction, "SettleCurrencySecurityID", 1)

	assert.EqualError(t, err, fmt.Sprintf("Invalid SettleCurrencySecurityID. Error fetching the SettleCurrencyID for PortfolioId: %d", *testTransaction.Allocations[1].PortfolioID))
	httputil.UserSessionToken = ""
}

func TestSettleCurrencySecurityId_WithHttpError(t *testing.T) {
	var settleCurrencySecurityID SettleCurrencySecurityID = 1
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadGateway)
	}))
	appconfig.BaseURI = ts.URL

	defer ts.Close()
	var currencyJSON = `{
		"Allocations": [
		  {
			"BookTypeId": 1,
			"Quantity":   200,
			"ExecQuantity":12,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12
		  },
		  {
			"BookTypeId": 1,
			"PortfolioId": 2,
			"Quantity":   200,
			"ExecQuantity":12,
			"Fees":            12,
			"PrincipalAmount": 12,
			"Commission":      12
		  }
		]
	}`
	var testTransaction Transaction
	unmarshalErr := json.Unmarshal([]byte(currencyJSON), &testTransaction)
	assert.Nil(t, unmarshalErr)

	err := settleCurrencySecurityID.Validate(testTransaction, "SettleCurrencySecurityID", 1)

	assert.EqualError(t, err, fmt.Sprintf("Error in allocation portfolioID %d. SettleCurrencySecurityID : Service lookup failed", *testTransaction.Allocations[1].PortfolioID))
}
