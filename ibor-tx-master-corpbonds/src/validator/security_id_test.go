package validator

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/httputil"
)

func TestSecurityID_WithErrorInSecurityAPIHttpCall(t *testing.T) {
	var securityID SecurityID = 7
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadGateway)
	}))

	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SecurityID"
	transaction := Transaction{}

	err := securityID.Validate(transaction, key)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. Service lookup failed", key))

}
func TestSecurityID_WithErrorSecurityNotFound(t *testing.T) {
	var securityID SecurityID = 7
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	}))

	httputil.UserSessionToken = "123"
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SecurityID"
	transaction := Transaction{}

	err := securityID.Validate(transaction, key)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. SecurityId: %d does not exist", key, securityID))
	httputil.UserSessionToken = "123"

}
func TestSecurityID_WithAssetClassNameIsNotFixedIncomeInSecurityAPIResponse(t *testing.T) {
	var securityID SecurityID = 7
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.Index(r.URL.Path, "coredata/v2/graphql") > -1 {
			_, err := w.Write([]byte(`{"data": {"securities": [{"id": 7,"symbol": "USN","assetClass": {"id": 3,"name": "NonCash"}}]}}`))
			if err != nil {
				t.Error("Unable to write the stream")
			}
		} else {
			w.WriteHeader(http.StatusNotFound)
		}
	}))
	httputil.UserSessionToken = "123"
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SecurityID"
	transaction := Transaction{
		SourceSystemName: "Ims-Trading",
	}

	err := securityID.Validate(transaction, key)

	assert.EqualError(t, err, "Invalid SecurityID. Expected asset class: FixedIncome, Provided asset class: NonCash")
	httputil.UserSessionToken = ""
}
func TestSecurityID_WithAssetClassNameIsFixedIncomeInSecurityAPIResponseAndOrderApiReturnError(t *testing.T) {
	var securityID SecurityID = 7
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.Index(r.URL.Path, "coredata/v2/graphql") > -1 {
			_, err := w.Write([]byte(`{"data": {"securities": [{"id": 7,"symbol": "GOOG","assetClass": {"id": 3,"name": "FixedIncome"}}]}}`))
			if err != nil {
				t.Error("Unable to write the stream")
			}
		} else {
			w.WriteHeader(http.StatusNotFound)
		}
	}))
	httputil.UserSessionToken = "123"
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SecurityID"
	transaction := Transaction{
		SourceSystemReference: "8",
		SourceSystemName:      "Ims-Trading",
	}

	err := securityID.Validate(transaction, key)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. OrderId: %s does not exist", key, transaction.SourceSystemReference))
	httputil.UserSessionToken = ""
}
func TestSecurityID_WithAssetClassNameIsFixedIncomeInSecurityAPIResponseAndErrorInOrderApiCall(t *testing.T) {
	var securityID SecurityID = 7
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.Index(r.URL.Path, "coredata/v2/graphql") > -1 {
			_, err := w.Write([]byte(`{"data": {"securities": [{"id":7,"symbol": "GOOG","assetClass": {"id": 3,"name": "FixedIncome"}}]}}`))
			if err != nil {
				t.Error("Unable to write the stream")
			}
		} else {
			w.WriteHeader(http.StatusBadGateway)
		}
	}))
	httputil.UserSessionToken = "123"
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SecurityID"
	transaction := Transaction{
		SourceSystemReference: "8",
		SourceSystemName:      "Ims-Trading",
	}

	err := securityID.Validate(transaction, key)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. Service lookup failed", key))
	httputil.UserSessionToken = ""
}
func TestSecurityID_WithAssetClassNameIsFixedIncomeInSecurityAPIResponseAndOrderApiReturnValidSecurityId(t *testing.T) {
	var securityID SecurityID = 7
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.Index(r.URL.Path, "coredata/v2/graphql") > -1 {
			_, err := w.Write([]byte(`{"data": {"securities": [{"id": 7,"symbol": "GOOG","assetClass": {"id": 3,"name": "FixedIncome"}}]}}`))
			if err != nil {
				t.Error("Unable to write the stream")
			}
		} else {
			_, err := w.Write([]byte("{\"Items\":[{\"SecurityID\":7}]}"))
			if err != nil {
				t.Error("Unable to write the stream")
			}
		}
	}))
	httputil.UserSessionToken = "123"
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SecurityID"
	transaction := Transaction{
		SourceSystemReference: "8",
		SourceSystemName:      "Ims-Trading",
	}

	err := securityID.Validate(transaction, key)

	assert.Nil(t, err)
	httputil.UserSessionToken = ""

}
func TestSecurityID_WithAssetClassNameIsFixedIncomeInSecurityAPIResponseAndOrderApiReturnInValidSecurityId(t *testing.T) {
	var securityID SecurityID = 7
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.Index(r.URL.Path, "coredata/v2/graphql") > -1 {
			_, err := w.Write([]byte(`{"data": {"securities": [{"id": 7,"symbol": "GOOG","assetClass": {"id": 3,"name": "FixedIncome"}}]}}`))
			if err != nil {
				t.Error("Unable to write the stream")
			}
		} else {
			_, err := w.Write([]byte("{\"Items\":[{\"SecurityID\":8}]}"))
			if err != nil {
				t.Error("Unable to write the stream")
			}
		}
	}))
	httputil.UserSessionToken = "123"
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SecurityID"
	transaction := Transaction{
		SourceSystemReference: "8",
		SourceSystemName:      "Ims-Trading",
	}

	err := securityID.Validate(transaction, key)

	assert.EqualError(t, err, "Invalid SecurityID. The security id is not associated to the order")
	httputil.UserSessionToken = ""

}
func TestSecurityID_WithAssetClassNameIsFixedIncomeInSecurityAPIResponseAndInvalidSourceSystemReference(t *testing.T) {
	var securityID SecurityID = 7
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.Index(r.URL.Path, "coredata/v2/graphql") > -1 {
			_, err := w.Write([]byte(`{"data": {"securities": [{"id": 7,"symbol": "GOOG","assetClass": {"id": 3,"name": "FixedIncome"}}]}}`))
			if err != nil {
				t.Error("Unable to write the stream")
			}
		} else {
			_, err := w.Write([]byte("{\"Items\":[{\"SecurityID\":8}]}"))
			if err != nil {
				t.Error("Unable to write the stream")
			}
		}
	}))
	httputil.UserSessionToken = "123"
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SecurityID"
	transaction := Transaction{
		SourceSystemReference: "8x",
		SourceSystemName:      "Ims-Trading",
	}

	err := securityID.Validate(transaction, key)

	assert.EqualError(t, err, "Invalid SourceSystemReference provided: 8x")
	httputil.UserSessionToken = ""

}
func TestSecurityID_WithInvalidResponseFromOrdersAPI(t *testing.T) {
	var securityID SecurityID = 7
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.Index(r.URL.Path, "coredata/v2/graphql") > -1 {
			_, err := w.Write([]byte(`{"data": {"securities": [{"id": 7,"symbol": "GOOG","assetClass": {"id": 3,"name": "FixedIncome"}}]}}`))
			if err != nil {
				t.Error("Unable to write the stream")
			}
		} else {
			_, err := w.Write([]byte("{\"Items\":test"))
			if err != nil {
				t.Error("Unable to write the stream")
			}
		}
	}))
	httputil.UserSessionToken = "123"
	defer ts.Close()
	appconfig.BaseURI = ts.URL
	key := "SecurityID"
	transaction := Transaction{
		SourceSystemReference: "8",
		SourceSystemName:      "Ims-Trading",
	}

	err := securityID.Validate(transaction, key)

	assert.EqualError(t, err, fmt.Sprintf("Invalid %s. Service lookup failed", key))
	httputil.UserSessionToken = ""

}
