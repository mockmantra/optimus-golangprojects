package models

// ImpactTransaction impactTransaction
// swagger:model impactTransaction
type ImpactTransaction struct {

	// allocations
	Allocations []*ImpactAllocation `json:"Allocations"`

	// business date time u t c
	// Required: true
	BusinessDateTimeUTC DateTimeUTC `json:"BusinessDateTimeUTC"`

	// event type Id
	// Required: true
	EventTypeID *int64 `json:"EventTypeId"`

	// is deleted
	IsDeleted bool `json:"IsDeleted,omitempty"`

	// local currency security Id
	// Required: true
	LocalCurrencySecurityID *int64 `json:"LocalCurrencySecurityId"`

	// order created date
	// Required: true
	OrderCreatedDate DateTimeUTC `json:"OrderCreatedDate"`

	// order quantity
	// Required: true
	OrderQuantity *int64 `json:"OrderQuantity"`

	// security Id
	// Required: true
	SecurityID *int64 `json:"SecurityId"`

	// security template Id
	// Required: true
	SecurityTemplateID *int64 `json:"SecurityTemplateId"`

	// sequence number
	// Required: true
	SequenceNumber *int64 `json:"SequenceNumber"`

	// settle date
	// Required: true
	SettleDate DateTime `json:"SettleDate"`

	// source system name
	// Required: true
	SourceSystemName *string `json:"SourceSystemName"`

	// source system reference
	// Required: true
	SourceSystemReference *string `json:"SourceSystemReference"`

	// system currency security Id
	// Required: true
	SystemCurrencySecurityID *int64 `json:"SystemCurrencySecurityId"`

	// system to local f x rate
	// Required: true
	SystemToLocalFXRate *float64 `json:"SystemToLocalFXRate"`

	// trade date
	// Required: true
	TradeDate DateTime `json:"TradeDate"`

	// transaction date time u t c
	// Required: true
	TransactionDateTimeUTC DateTimeUTC `json:"TransactionDateTimeUTC"`

	// transaction time zone Id
	// Required: true
	TransactionTimeZoneID *int64 `json:"TransactionTimeZoneId"`
}
