// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"net/http"
	"os"

	errors "github.com/go-openapi/errors"
	"github.com/go-openapi/loads"
	runtime "github.com/go-openapi/runtime"
	middleware "github.com/go-openapi/runtime/middleware"

	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/handler"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/logger"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/middlewares"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/models"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/restapi/operations"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/restapi/operations/health"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/restapi/operations/transaction"
	"stash.ezesoft.net/ipc/ezelogger"
	"stash.ezesoft.net/ipc/ezeutils"
)

var basePath = "/api/ibor/tx-master/corporatebonds/v1"
var swaggerJSONPath = basePath + "/ui/swagger.json"

//go:generate swagger generate server --target .. --name  --spec ../swagger.json

func configureFlags(api *operations.IborTxMasterCorpbondsAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

func configureAPI(api *operations.IborTxMasterCorpbondsAPI) http.Handler {
	// configure the api here
	api.ServeError = errors.ServeError

	// Set your custom logger if needed. Default one is log.Printf
	// Expected interface func(string, ...interface{})
	//
	// Example:
	// api.Logger = log.Printf

	api.JSONConsumer = runtime.JSONConsumer()

	api.JSONProducer = runtime.JSONProducer()

	api.TxtProducer = runtime.TextProducer()

	api.TransactionGetTransactionDetailsHandler = transaction.GetTransactionDetailsHandlerFunc(func(params transaction.GetTransactionDetailsParams) middleware.Responder {
		ctx, cancelFunc := ezeutils.WithRequest(params.HTTPRequest.Context(), params.HTTPRequest)
		defer cancelFunc()
		loggerWithCtx := logger.Log.WithContext(ctx)

		var transDetails []*models.TransactionDetailsResponse
		var errGet error

		usersessionJSON, _ := ezeutils.FromContext(ctx, "userSession")
		userSession := &ezeutils.UserSession{}
		err := json.Unmarshal([]byte(usersessionJSON), userSession)

		if err != nil {
			loggerWithCtx.Error(err)
		}

		transDetails, errGet = handler.HandleTransactionDetails(ctx, params.SourceSystemRefAndName, userSession, loggerWithCtx)

		if errGet != nil {
			loggerWithCtx.Error(err)
		}

		return transaction.NewGetTransactionDetailsOK().WithPayload(transDetails)
	})

	api.TransactionTransactionHandler = transaction.TransactionHandlerFunc(func(params transaction.TransactionParams) middleware.Responder {
		ctx, cancelFunc := ezeutils.WithRequest(params.HTTPRequest.Context(), params.HTTPRequest)
		defer cancelFunc()
		loggerWithCtx := logger.Log.WithContext(ctx)

		usersessionJSON, _ := ezeutils.FromContext(ctx, "userSession")
		userSession := &ezeutils.UserSession{}
		err := json.Unmarshal([]byte(usersessionJSON), userSession)
		if err != nil {
			loggerWithCtx.Error(err)
		}

		// Created new context
		newCtx := context.Background()
		activityID, _ := ezeutils.FromContext(ctx, ezeutils.XRequestIdKey)
		newCtx = ezeutils.WithKeyValue(newCtx, ezeutils.XRequestIdKey, activityID)                   // Set ActivityId
		newCtx = ezeutils.WithKeyValue(newCtx, ezeutils.UserNameKey, userSession.UserName)           // Set UserName
		newCtx = ezeutils.WithKeyValue(newCtx, ezeutils.FirmAuthTokenKey, userSession.FirmAuthToken) // Set FirmAuthToken
		loggerWithCtx = logger.Log.WithContext(newCtx)
		for _, transaction := range params.Transaction {
			dataJSON, errToJSON := json.Marshal(*transaction)
			if errToJSON != nil {
				loggerWithCtx.Error(errToJSON)
			} else {
				go func(data []byte) {
					loggerWithCtx.Debugf("Started full validation for: %s", string(data))
					handler.HandleTransaction(newCtx, data, userSession, loggerWithCtx)
				}(dataJSON)
			}

		}
		return transaction.NewTransactionOK()
	})

	api.HealthHealthCheckHandler = health.HealthCheckHandlerFunc(func(params health.HealthCheckParams) middleware.Responder {
		healthCheckResponse := &health.HealthCheckOK{}
		return healthCheckResponse.WithPayload("OK")
	})

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix"
func configureServer(s *http.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation
func setupMiddlewares(handler http.Handler) http.Handler {
	//return handler
	return middlewares.Authorize(&authorizeHandler{}, handler)
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	handler = installSwaggerJSONHandler(middlewares.InstallUImiddleware(handler))
	if os.Getenv("PPROF") == "1" {
		handler = ezeutils.PprofHandler(handler)
	}
	return handler
}

// InstallSwaggerJSONHandler serves the swagger.json at eos api location rather than server's root path
func installSwaggerJSONHandler(handler http.Handler) http.Handler {
	handlerFunction := func(w http.ResponseWriter, r *http.Request) {

		if r.URL.Path == swaggerJSONPath {
			swaggerSpec, err := loads.Analyzed(SwaggerJSON, "")
			if err != nil {
				//logger.Log.Error("Failed to get swagger spec json.")
			}
			rawSpec := swaggerSpec.Raw()
			rootHandler := middleware.Spec(basePath+"/ui/", rawSpec, http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
				rw.Header().Set("Content-Type", "application/json")
				rw.WriteHeader(http.StatusFound)
				return
			}))
			rootHandler.ServeHTTP(w, r)
			return
		}
		handler.ServeHTTP(w, r)
		return
	}
	return http.HandlerFunc(handlerFunction)
}

type authorizeHandler struct{}

func (authorizeHandler *authorizeHandler) AuthorizeRequestUsingContext(ctx context.Context, baseURI string, logger *ezelogger.EzeLog) (*ezeutils.UserSession, context.Context, error) {
	return ezeutils.AuthorizeRequestUsingContext(ctx, baseURI, logger)
}
