package middlewares

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	appconfig "stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/logger"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/models"
	"stash.ezesoft.net/ipc/ezelogger"
	"stash.ezesoft.net/ipc/ezeutils"
)

// IAuthorizeHandler exposes method to authorize the request
type IAuthorizeHandler interface {
	AuthorizeRequestUsingContext(context.Context, string, *ezelogger.EzeLog) (*ezeutils.UserSession, context.Context, error)
}

// Authorize a request and make userSession available if the request authorized successfuly
func Authorize(authorizeHandler IAuthorizeHandler, handler http.Handler) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		basePath := "/api/ibor/tx-master/corporatebonds/v1"
		skipRoutes := "health"
		if strings.Index(r.URL.Path, basePath+"/"+skipRoutes) == 0 {
			handler.ServeHTTP(w, r)
			return
		}

		ctx, cancelFunc := ezeutils.WithRequest(r.Context(), r)
		loggerWithCtx := logger.Log.WithContext(ctx)
		userSession, ctx, err := authorizeHandler.AuthorizeRequestUsingContext(ctx, appconfig.BaseURI, logger.Log)
		if err != nil {
			defer cancelFunc()
			msg := err.Error()
			loggerWithCtx.Error(fmt.Sprintf("[PutTransactions] Error occurred while authorizing request. Error: %v", msg), nil)
			var code int
			switch e := err.(type) {
			case *ezeutils.StatusError:
				code = e.Status()
			default:
				code = 500
			}

			w.WriteHeader(code)
			dataJSON, _ := json.Marshal(&models.Error{Message: &msg})
			rawData := string(dataJSON)
			_, err := w.Write([]byte(rawData))
			if err != nil {
				loggerWithCtx.Error(fmt.Sprintf("Unable to write the error message. Error: %v", err))
			}

			return
		}

		dataJSON, _ := json.Marshal(userSession)
		ezeutils.WithKeyValue(ctx, "userSession", string(dataJSON))

		request := r.WithContext(ctx)
		// Read the content for logging
		var bodyBytes []byte
		if request.Body != nil {
			bodyBytes, _ = ioutil.ReadAll(request.Body)
			// Restore the io.ReadCloser to its original state
			request.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

			buffer := new(bytes.Buffer)
			if compactErr := json.Compact(buffer, bodyBytes); compactErr != nil {
				loggerWithCtx.Error(compactErr)
			}
			loggerWithCtx.Debugf("Started light validation for: %v", buffer)
		}
		handler.ServeHTTP(w, request)
	})

}
