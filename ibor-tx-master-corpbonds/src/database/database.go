package database

import (
	"encoding/json"
	"fmt"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql" //using mysql package in gorm.open()
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	bitemporalLayer "stash.ezesoft.net/imsacnt/accounting-utils/go-bitemporality"
	appconfig "stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/interfaces"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/logger"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/models"
	"stash.ezesoft.net/imsacnt/ibor-tx-master-corpbonds/src/vault"
)

// var lock sync.Mutex
var gormOpen = gorm.Open

const tableName = "Ibor_ValidationErrors"
const transactionTableName = "Ibor_CorpBonds_Transactions"
const validFromUTC = "ValidFromUTC"
const validToUTC = "ValidToUTC"
const impactTransactionTable = "Ibor_ImpactTransaction"

// TransactionType contains the type of transaction.
const TransactionType = "CorpBondsTransaction"

func init() {
	if !appconfig.IsDevMode {
		da := DataAccessorInstance()
		mapOfFirmDbConnections := da.vault.GetDbCredentials()
		if len(mapOfFirmDbConnections) < 1 {
			logger.Log.Error("Error reading tenant db credentials from vault")
			panic("Error loading db credentials from vault")
		}
	}
}

// DataAccessor is used to connect to db and execute db operation
type DataAccessor struct {
	db    *gorm.DB
	firm  string
	Log   interfaces.ILogger
	vault interfaces.IVault
}

// DataAccessorInstance - Returns an instance of DataAccessor struct
func DataAccessorInstance() *DataAccessor {
	vaultInstance := vault.Instance()
	return &DataAccessor{
		Log:   logger.Log,
		vault: vaultInstance,
	}
}

// InsertTransaction will insert the records into the transaction table in the database.
func (da *DataAccessor) InsertTransaction(payload *models.Transactiontable, transactionDateTimeUTC time.Time) error {
	var bitemporalStruct bitemporalLayer.BtObj
	bitemporalStruct.TableName = transactionTableName
	uniqueKey := make(map[string]interface{})
	uniqueKey["SourceSystemName"] = payload.SourceSystemName
	uniqueKey["SourceSystemReference"] = payload.SourceSystemReference
	bitemporalStruct.UniqueKey = uniqueKey
	bitemporalStruct.Database = da.db
	var table models.Transactiontable
	bitemporalStruct.TableModel = &table

	validDateTimeFields := bitemporalLayer.DateTimeUpdate{
		NewDateTime:  transactionDateTimeUTC,
		FromTableKey: validFromUTC,
		ToTableKey:   validToUTC,
	}

	if err := bitemporalStruct.Insert(false, payload, validDateTimeFields); err != nil {
		return err
	}
	return nil

}

// GetTransactionDetails gets the transaction details
func (da *DataAccessor) GetTransactionDetails(filterDetails []*models.TransactionDetails, log interfaces.ILogger) ([]*models.TransactionDetailsResponse, error) {
	log.Info("Retreiving Transaction details.")

	var filterCriteria string
	for index, element := range filterDetails {
		if index == 0 {
			filterCriteria += "(SourceSystemName = '" + *element.SourceSystemName + "' AND SourceSystemReference = '" + *element.SourceSystemReference + "')"
		} else {
			filterCriteria += " OR (SourceSystemName = '" + *element.SourceSystemName + "' AND SourceSystemReference = '" + *element.SourceSystemReference + "')"
		}
	}

	rows, err := da.db.Table("Ibor_CorpBonds_Transactions").
		Select("Ibor_CorpBonds_Transactions.MessageBody").
		Where(filterCriteria).
		Rows()

	if err != nil {
		log.Error(err)
		return nil, err
	}

	defer func() {
		err := rows.Close()
		if err != nil {
			log.Error(err)
		}
	}()
	return createTransactionList(log, rows)
}

func createTransactionList(log interfaces.ILogger, rows interfaces.ISqlRow) ([]*models.TransactionDetailsResponse, error) {
	dbResults := make([]*models.TransactionDetailsResponse, 0)
	index := 0
	for rows.Next() {
		var result string
		err := rows.Scan(&result)
		if err != nil {
			log.Error(err)
			return nil, err
		}
		index++
		var transaction models.TransactionDetailsResponse
		err = json.Unmarshal([]byte(result), &transaction)
		if err != nil {
			log.Error(err)
			return nil, err
		}
		dbResults = append(dbResults, &transaction)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}
	return dbResults, nil
}

// InsertValidationErrors inserts the validation errors
func (da *DataAccessor) InsertValidationErrors(recordid int, validationErrors []string) error {
	da.Log.Infof("Validating Transaction type: %s", TransactionType)
	sqlStr := "INSERT INTO " + tableName + "(TransactionRecordID,ErrorMessage, TransactionType) VALUES "
	for _, elem := range validationErrors {
		sqlStr += fmt.Sprintf("(%v, %q, %#v),", recordid, elem, TransactionType)
	}
	sqlStr = sqlStr[:len(sqlStr)-1]
	if err := da.db.Exec(sqlStr).Error; err != nil {
		return err
	}
	return nil
}

// InsertImpactTransaction will insert the records into the Ibor_ImpactTransaction table in the database using the Bi-Temporality library.
func (da *DataAccessor) InsertImpactTransaction(payload *models.ImpactTransactionTable, transactionTime time.Time) error {

	var tableModel models.ImpactTransactionTable
	uniqueKey := make(map[string]interface{})
	uniqueKey["SourceSystemName"] = payload.SourceSystemName
	uniqueKey["SourceSystemReference"] = payload.SourceSystemReference

	bitemporalStruct := bitemporalLayer.BtObj{
		TableName:  impactTransactionTable,
		Database:   da.db,
		TableModel: &tableModel,
		UniqueKey:  uniqueKey,
	}

	validDateTimeFields := bitemporalLayer.DateTimeUpdate{
		NewDateTime:  transactionTime,
		FromTableKey: validFromUTC,
		ToTableKey:   validToUTC,
	}

	if err := bitemporalStruct.Insert(false, payload, validDateTimeFields); err != nil {
		err = errors.Wrapf(err, "Error attempting to insert Impact Transaction")
		da.Log.Error(err)
		return err
	}

	da.Log.Infof("Successfully inserted to Impact Transaction with SourceSystemName %s,  SourceSystemReference %s", payload.SourceSystemName, payload.SourceSystemReference)
	return nil
}

func (da *DataAccessor) getTenantCredentials(firmToken string) (*models.AuroraConnection, error) {
	con, ok := da.vault.GetDbCredentials()[firmToken]
	if !ok {
		return nil, fmt.Errorf("No db credentials found for firm: -%s-", firmToken)
	}
	da.Log.Debugf("Vault credentials loaded for tenant %s", firmToken)
	return con, nil
}

func (da *DataAccessor) getSchemaName() string {
	return fmt.Sprintf(appconfig.DbSchemaTemplate, da.firm)
}

// Close closes the database connection of a data accessor
func (da *DataAccessor) Close() (err error) {

	if da.db != nil {
		if err = da.db.Close(); err != nil {
			err = errors.Wrapf(err, "Error while closing connection with database %v", da.getSchemaName())
			da.Log.Error(err)
		} else {
			err = nil
		}
	}
	*da = DataAccessor{}
	return err
}

// Connect establishes connection to firm's database
// TODO: check if lock required on this!
func (da *DataAccessor) Connect(firmAuthToken string) (err error) {

	err = retry(appconfig.DbConnectAttempts, time.Second, func() error {

		connectionString, err := da.getConnectionString(firmAuthToken)
		if err != nil {
			return err
		}

		db, err := gormOpen("mysql", connectionString)
		if err != nil {
			return err
		}

		da.db = db
		return nil
	})

	return

}

func (da *DataAccessor) getConnectionString(firmToken string) (connectionString string, err error) {
	if appconfig.IsDevMode {
		connectionString = os.Getenv("Connection_String")
		if len(connectionString) == 0 {
			return "", errors.New("Connection string is not set")
		}
		return
	}

	connectionInfo, er := da.vault.GetTenantCredentials(firmToken)

	if er != nil {
		return "", er
	}
	da.firm = firmToken
	connectionString = buildConnectionString(connectionInfo, da.getSchemaName())
	return
}

func buildConnectionString(connectionInfo *models.AuroraConnection, dbName string) string {
	return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?parseTime=True&loc=UTC",
		connectionInfo.User,
		connectionInfo.Password,
		connectionInfo.Host,
		connectionInfo.Port,
		dbName)
}
func retry(attempts int, sleep time.Duration, f func() error) (err error) {
	for {
		if err = f(); err == nil {
			return nil
		}
		attempts--
		if attempts <= 0 {
			break
		}
		time.Sleep(sleep)
	}
	return errors.Wrap(err, "Failed to open database connection. Exiting...") // do not log connectionstring
}
