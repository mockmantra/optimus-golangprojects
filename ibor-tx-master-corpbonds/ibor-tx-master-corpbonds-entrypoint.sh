#!/bin/bash

source ./scripts/rancher-set-env.sh
set -e
echo "Starting ibor-tx-master-corpbonds wrapper shell script"

# Get lowercase file path
envFile=`echo ./rancher-environments/$ENVIRONMENT.$REGION.env | tr '[:upper:]' '[:lower:]'`
if [ -f $envFile ]
then
    set -o allexport
    source $envFile
    set +o allexport
    echo "Environment variables loaded from $envFile."
else
    echo "Environment file $envFile does not exist. Environment variables not loaded from file."
fi
echo "Environment variable list:"
env | sed 's/^/  /'

echo "Starting service..."


if [[ “$APP_ENV” != “dev” && -n “$VAULT_ADDR” ]] ; then
	# sh  $ROUTING_SCRIPTS_DIR/certs.sh
	# sh  $ROUTING_SCRIPTS_DIR/install-vault-deps.sh
	# sh  $ROUTING_SCRIPTS_DIR/vault-auth.sh

	echo "Determining vault settings"
	if [ -z "$VAULT_SKIP_VERIFY" ]; then
		echo "VAULT_SKIP_VERIFY is not defined. Defaulting to false."
		VAULT_SKIP_VERIFY=false
	fi
	if [ -z "$VAULT_TOKEN" ]; then
		echo "VAULT_TOKEN is not defined. Defaulting to value from ~/.vault-token."
		export VAULT_TOKEN=$(cat ~/.vault-token)
	fi
	echo "Using $VAULT_ADDR as vault address"

	if [ -z "$VAULT_CERT" ]; then
		echo "VAULT_CERT is not defined. Defaulting to value from /cert/vault.pem."
		export VAULT_CERT="/cert/vault.pem"
	fi
	if [ -z "$VAULT_KEY" ]; then
		echo "VAULT_KEY is not defined. Defaulting to value from /cert/key.pem."
		export VAULT_KEY="/cert/key.pem"
	fi

	export  VAULT_ADDR VAULT_SKIP_VERIFY VAULT_CERT VAULT_KEY
fi

export RANCHER_HOST_NAME=`curl http://rancher-metadata/latest/self/host/name`
export RANCHER_CONTAINER_NAME=`curl http://rancher-metadata/latest/self/container/name`

chmod +x $GOPATH/bin/ibor-tx-master-corpbonds-server
$GOPATH/bin/ibor-tx-master-corpbonds-server --host "0.0.0.0" --port 6989