# ibor-tx-master-corpbonds

Ibor-tx-master-corpbonds is a rest api built using [Open API](https://swagger.io/docs/specification/2-0/basic-structure/) specification.

## Installation

* Go Programming Language
  should be installed in the machine, if not it can be downloaded from https://golang.org/dl/
* Install [dep](https://golang.github.io/dep/docs/installation.html), a dependency management tool for Go.
* Install and run mysql. Update your credentials in launch.json before spinnig this service up.
  Link for installation

  https://dev.mysql.com/downloads/installer/

  Get the equityswap generation script from [here](https://stash.ezesoft.net/projects/CLOUD/repos/db-toolbox/browse/src/database/mysql/accounting/ibor-tx-master-corpbonds/tables/000001_Ibor_Corpbonds_Transactions.sql) and validation errors script from [here](https://stash.ezesoft.net/projects/CLOUD/repos/db-toolbox/browse/src/database/mysql/accounting/ibor-tx-master-corpbonds/tables/000002_Ibor_ValidationError.sql)

## Steps to run in local
* Run below command to run app in local.
    ```bash
    make run
    ```
* To debug application in vscode, goto debug tab and click on play icon.(Make sure that all the necessary tools installed for debugging go).

    *Note*: App listens at port 6989 when we launch it from shell script or debugger but it does not work as expected unless we integrate the authentication api.

* Install nginx in local if not done already and add below configuration in `nginx.conf` file under `usr/local/etc/nginx` and restart the nginx service.

    Add upstreams in `http` section

    ```
    upstream Platform_Api_api_platform_v1 {
        server localhost:3000;
    }

    upstream Ibor_tx_corpbonds_api_v1 {
        server localhost:6989;
    }
    ```

    Add/Update following in `server` section

    ```
    listen 8001

    location /api/platform/v1 {
        proxy_pass http://Platform_Api_api_platform_v1;
    }
    location /api/ibor/tx-master/corporatebonds/v1/ {
        proxy_pass http://Ibor_tx_corpbonds_api_v1;
    }
    ```
    Note: Now the api can be accessed using http://localhost:8001/api/ibor/tx-master/corporatebonds/v1/"

## Steps to update the swagger api.
* Make necessary changes to the `swagger.json` file. 
* Update the golang files of api using following command from src folder.
    ```bash
    swagger generate server
    ```

### Tests
* Run all tests in the project.
    ```bash
    make test
    ```
*  Run all tests with coverage in terminal.
    ```bash
    make test_cover
    ```
*  Run all tests with coverage in browser.
    ```bash
    make test_cover_html
    ```
* If you want to run/debug a test with go extension(test At Cursor) in vscode, keep below setting at `.vscode/settings.json`
    ```
    {
        "go.testEnvVars":{ "APP_ENV": "local"}
    }

    ```

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## History

TODO: Write history

## Credits

TODO: Write credits

## License

TODO: Copyright (c) 2015 Eze Software Group.   All rights reserved.