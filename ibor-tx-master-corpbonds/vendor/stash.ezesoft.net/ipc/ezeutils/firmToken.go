package ezeutils

import (
	"fmt"
	"strings"
)

var (
	strCrockfordBase32      = "0123456789ABCDEFGHJKMNPQRSTVWXYZ"
	crockfordBaseLength     = 32 // CrockfordBase32.Length;
	numberDigitsToUse       = 4  // valid for upto 5
	obfuscatedSlope         = uint64(611951)
	obfuscationInverseSlope = uint64(31377551)
	obfuscatedOffset        = uint64(0x001735215)
)

func getBitMask() uint64 {
	switch numberDigitsToUse {
	case 4:
		return 0x000FFFFF
	case 5:
		return 0x01FFFFFF
	case 3:
		return 0x00007FFF
	case 2:
		return 0x000003FF
	case 1:
		return 0x0000001F
	}
	panic("Code only supports 1-5 digits")
}

func obfuscate(clearFirmID int) int {
	bitMask := getBitMask()
	return int((uint64(clearFirmID)*obfuscatedSlope + obfuscatedOffset) & bitMask)
}

func deobfuscate(obFirmID int) int {
	bitMask := getBitMask()
	return int((obfuscationInverseSlope * (uint64(obFirmID) - obfuscatedOffset)) & bitMask)
}

func FirmTokenFromTenantID(firmID int) (string, error) {
	if firmID <= 0 {
		return "", fmt.Errorf("invalid firm ID: %d", firmID)
	}
	if firmID == 1 {
		return "EZE", nil
	}
	obFirmID := obfuscate(firmID)
	return obTenantIDToHostName(obFirmID)
}

func TenantIDFromFirmToken(firmToken string) (int, error) {
	if firmToken == "EZE" {
		return 1, nil
	}
	obFirmID, err := hostNameToObTenantID(firmToken)
	if err != nil {
		return 0, err
	}
	return deobfuscate(obFirmID), nil
}

func obTenantIDToHostName(obFirmId int) (string, error) {
	if obFirmId < 0 {
		return "", fmt.Errorf("obfuscated firmID is less than 0")
	}
	arrHost := make([]byte, numberDigitsToUse)
	for digit := numberDigitsToUse - 1; digit >= 0; digit-- {
		quot := obFirmId / crockfordBaseLength
		rem := obFirmId % crockfordBaseLength
		arrHost[digit] = strCrockfordBase32[rem]
		obFirmId = quot
	}
	return "T" + string(arrHost), nil
}

func hostNameToObTenantID(obFirmToken string) (int, error) {
	if obFirmToken == "" {
		return 0, fmt.Errorf("empty obfuscated firmToken")
	}
	obFirmToken = strings.TrimPrefix(obFirmToken, "T")
	obFirmToken = strings.ToUpper(obFirmToken)
	if len(obFirmToken) != numberDigitsToUse {
		return 0, fmt.Errorf("obfuscated firmToken is of unexpected length %d", len(obFirmToken))
	}

	obfuscated := 0
	for digit := 0; digit < numberDigitsToUse; digit++ {
		index := strings.IndexByte(strCrockfordBase32, obFirmToken[digit])
		obfuscated = crockfordBaseLength*obfuscated + index
	}
	return obfuscated, nil
}
