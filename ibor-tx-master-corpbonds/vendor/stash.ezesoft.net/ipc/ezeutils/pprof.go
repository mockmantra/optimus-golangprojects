package ezeutils

import (
	"net/http"
	"net/http/httputil"
	"net/http/pprof"
	"strings"
)

// PprofHandler handles routing to Pprof endpoints
func PprofHandler(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.HasPrefix(r.URL.Path, "/debug/pprof/cmdline") {
			pprof.Cmdline(w, r)
		} else if strings.HasPrefix(r.URL.Path, "/debug/pprof/profile") {
			pprof.Profile(w, r)
		} else if strings.HasPrefix(r.URL.Path, "/debug/pprof/symbol") {
			pprof.Symbol(w, r)
		} else if strings.HasPrefix(r.URL.Path, "/debug/pprof/trace") {
			pprof.Trace(w, r)
		} else if strings.HasPrefix(r.URL.Path, "/debug/pprof") {
			pprof.Index(w, r)
		} else {
			handler.ServeHTTP(w, r)
		}
	})
}

// PprofProxy handles proxying to a custom host:port via X-Redirect-To-Host HTTP header
func PprofProxy(handler http.Handler, base string) http.Handler {
	director := func(req *http.Request) {
		req.URL.Scheme = "http"
		req.URL.Host = req.Header.Get("X-Redirect-To-Host")
		if _, ok := req.Header["User-Agent"]; !ok {
			req.Header.Set("User-Agent", "") // explicitly disable User-Agent so it's not set to default value
		}
		req.Header.Set("X-Redirect-To-Host", "") // remove X-Redirect-To-Host heasder to avoid infinite redirection loop
	}
	proxy := &httputil.ReverseProxy{Director: director}
	return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		if strings.HasPrefix(req.URL.Path, base+"/debug/pprof") {
			req.URL.Path = req.URL.Path[len(base):]
			proxy.ServeHTTP(rw, req)
		} else {
			handler.ServeHTTP(rw, req)
		}
	})
}
