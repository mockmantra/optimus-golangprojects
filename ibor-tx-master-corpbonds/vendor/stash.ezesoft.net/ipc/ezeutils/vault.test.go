package ezeutils

import (
	"testing"
)

func TestCreatePassword(t *testing.T) {

	password := CreatePassword(16)
	if len(password) < 20 {
		t.Errorf("CreatePassword result is too short: %v", password)
	}
}
