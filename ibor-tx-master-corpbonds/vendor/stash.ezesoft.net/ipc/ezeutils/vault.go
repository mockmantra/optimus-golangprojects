package ezeutils

import (
	"context"
	"crypto/rand"
	"crypto/tls"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/hashicorp/vault/api"
	"github.com/pkg/errors"
	"stash.ezesoft.net/ipc/ezelogger"
)

// TODO: add secret renewal using vault client's Renewer
// This may require stop using vault token from env var and login via API, or maybe we could find a way to convert token to a renewable *Secret type

const (
	rancherContainerID = "RANCHER_CONTAINER_ID"
)

var (
	vtoken, vaddr, vcert, vkey string
	tokenDeadline              time.Time
	containerID                string
	logger                     ezelogger.EzeLogger
	vClient                    *api.Client
	vLogical                   *api.Logical
	tokenLock                  sync.Mutex
)

func init() {
	vtoken = os.Getenv("VAULT_TOKEN")
	vaddr = os.Getenv("VAULT_ADDR")
	vcert = os.Getenv("VAULT_CERT")
	vkey = os.Getenv("VAULT_KEY")
	tokenDeadline = time.Now().Add(time.Minute * 59) // as the token has been already created by startup shell
	containerID = os.Getenv(rancherContainerID)
	logger = ezelogger.GetLogger("VaultClient").Configure(ezelogger.CfgReplaceNewlines, ezelogger.CfgReplaceDoubleQuotes, ezelogger.CfgMessageLinesLimit(100), ezelogger.CfgMessageSizeLimit(3072))
}

// EnableRenewLoginToken starts token renew process. ctx paramenter can be used to cancel the process by cancelling the context
func EnableRenewLoginToken(ctx context.Context) {
	go func(quit <-chan struct{}) {
		for {
			select {
			case <-quit:
				logger.Info("quit vault token renew timer")
				return
			case <-time.After(time.Duration(time.Minute * 29)):
				logger.Infof("attempting to renew vault token")
				err := renew(3600)
				if err != nil {
					logger.Errorf("failed to renew vault token %v", err)
				}
			}
		}
	}(ctx.Done())
}

// TryReadStringSecret reads a string secret from vault's path/field, logging in to Vault if necessary
// If errorIfNotFound then if there is nothing stored at that key or with that field then returns an error
func TryReadStringSecret(path, field string, errorIfNotFound bool) (string, error) {
	vault, errVault := getVaultClient()
	if errVault != nil {
		return "", errVault
	}

	s2, err := vault.Read(path)
	if err != nil {
		tokenLock.Lock()
		vtoken = ""
		tokenLock.Unlock()
		return "", err
	}
	if s2 == nil {
		if errorIfNotFound {
			return "", fmt.Errorf("No secret read back")
		}
		return "", nil
	}
	val, ok := s2.Data[field]
	if !ok {
		if errorIfNotFound {
			return "", fmt.Errorf("No field '%s' in the secret", field)
		}
		return "", nil
	}

	strval, ok := val.(string)
	if !ok {
		return "", fmt.Errorf("Field '%s' in the secret is not a string but a '%T'", field, val)
	}
	return strval, nil
}

// TryReadSecrets reads secret data from vault's path, logging in to Vault if necessary
// If errorIfNotFound then if there is nothing stored at that key then returns an error
func TryReadSecrets(path string, errorIfNotFound bool) (map[string]interface{}, error) {
	vault, errVault := getVaultClient()
	if errVault != nil {
		return nil, errVault
	}

	s2, err := vault.Read(path)
	if err != nil {
		tokenLock.Lock()
		vtoken = ""
		tokenLock.Unlock()
		return nil, err
	}
	if s2 == nil {
		if errorIfNotFound {
			return nil, fmt.Errorf("No secret read back")
		}
		return map[string]interface{}{}, nil
	}
	return s2.Data, nil
}

// ReadStringSecret reads a string secret from vault's path/field, logging in to Vault if necessary
func ReadStringSecret(path, field string) (string, error) {
	return TryReadStringSecret(path, field, true)
}

// WriteStringSecret writes a string secret value to vault's path/field, logging in to Vault if necessary
func WriteStringSecret(path, field, value string) error {
	vault, err := getVaultClient()
	if err != nil {
		return err
	}

	content := map[string]interface{}{field: value}

	_, err = vault.Write(path, content)
	if err != nil {
		tokenLock.Lock()
		vtoken = ""
		tokenLock.Unlock()
		return err
	}
	return nil
}

// create an alpha-numeric password - for now this doesn't use vault but the intention is that it will
func CreatePassword(byteSize int) string {
	bytes := make([]byte, byteSize)
	if _, err := rand.Read(bytes); err != nil {
		panic(err)
	}
	encoded := base64.RawURLEncoding.EncodeToString(bytes) // alpha-numeric but also _ and -
	encoded = strings.Replace(encoded, "_", "z", -1)
	encoded = strings.Replace(encoded, "-", "z", -1)
	password := strings.ToLower(encoded)
	return password
}

func registerContainer() error {
	if containerID != "" {
		httpc := http.Client{
			Transport: &http.Transport{
				DialContext: func(_ context.Context, _, _ string) (net.Conn, error) {
					return net.Dial("unix", "/var/lib/poll-vault/agent.sock")
				},
			},
		}
		var response *http.Response
		var err error
		response, err = httpc.Get("http://unix/setupauth/" + containerID)
		if err != nil {
			return errors.Wrapf(err, "failed to GET  poll-vault socket for container '%s'", containerID)
		}
		_, _ = ioutil.ReadAll(response.Body)
		logger.Debugf("registered container '%s' with poll-vault", containerID)
		// sleep for a second in case vault needs time to process the cert
		time.Sleep(time.Second)
	}
	return nil
}

func login() (string, time.Duration, error) {
	logger.Debug("Logging into Vault...")
	// re-register cert with poll-vault agent
	err := registerContainer()
	if err != nil {
		logger.Error(err)
	}

	t := &api.TLSConfig{
		CACert:        "",
		CAPath:        "",
		ClientCert:    vcert,
		ClientKey:     vkey,
		TLSServerName: "localhost",
		Insecure:      true,
	}
	c, err := newHTTPClient(t)
	if err != nil {
		return "", 0, errors.Wrap(err, "failed to create HTTP client")
	}

	r, err := http.NewRequest("POST", vaddr+"/v1/auth/cert/login", nil)
	if err != nil {
		return "", 0, errors.Wrap(err, "failed to create auth request to Vault")
	}
	resp, err := c.Do(r)
	if err != nil {
		return "", 0, errors.Wrap(err, "failed to execute Vault auth request")
	}
	defer resp.Body.Close()
	statusError := &StatusError{Code: resp.StatusCode, Err: errors.New(resp.Status)}
	if statusError.Failed() {
		statusError.Err = fmt.Errorf("Code: %v, Status: %v", statusError.Code, statusError.Err)
		return "", 0, errors.Wrap(statusError, "failed to authenticate to Vault")
	}

	s, err := api.ParseSecret(resp.Body)
	if err != nil {
		statusError.Err = fmt.Errorf("Code: %v, Status: %v, Error: %v", statusError.Code, statusError.Err, err)
		if !statusError.Failed() {
			statusError.Code = 500
		}
		return "", 0, errors.Wrap(statusError, "failed to read response from Vault auth request")
	}
	if s.Auth == nil || s.Auth.ClientToken == "" {
		return "", 0, fmt.Errorf("vault response doesn't contain client token: %v", s)
	}
	tokenTTL := time.Hour
	if s.Auth.LeaseDuration > 0 {
		logger.Infof("Token lease duration is %d seconds", s.Auth.LeaseDuration)
		tokenTTL = time.Duration(s.Auth.LeaseDuration) * time.Second
	}

	logger.Info("Successfully logged in to Vault")
	return s.Auth.ClientToken, tokenTTL, nil
}

func newHTTPClient(t *api.TLSConfig) (*http.Client, error) {
	client := CreateHTTPClient()
	clientTLSConfig := &tls.Config{
		MinVersion: tls.VersionTLS12,
	}
	client.Transport.(*http.Transport).TLSClientConfig = clientTLSConfig

	var clientCert tls.Certificate
	foundClientCert := false

	switch {
	case t.ClientCert != "" && t.ClientKey != "":
		var err error
		clientCert, err = tls.LoadX509KeyPair(t.ClientCert, t.ClientKey)
		if err != nil {
			return nil, err
		}
		foundClientCert = true
	case t.ClientCert != "" || t.ClientKey != "":
		return nil, fmt.Errorf("both client cert and client key must be provided")
	}

	if t.Insecure {
		clientTLSConfig.InsecureSkipVerify = true
	}

	if foundClientCert {
		// We use this function to ignore the server's preferential list of
		// CAs, otherwise any CA used for the cert auth backend must be in the
		// server's CA pool
		clientTLSConfig.GetClientCertificate = func(*tls.CertificateRequestInfo) (*tls.Certificate, error) {
			return &clientCert, nil
		}
	}

	if t.TLSServerName != "" {
		clientTLSConfig.ServerName = t.TLSServerName
	}

	return client, nil
}

// get a Vault client, logging in to Vault if necessary
func getVaultClient() (*api.Logical, error) {
	var err error
	if vClient == nil {
		vaultCFG := api.DefaultConfig()
		vaultCFG.Address = vaddr
		vClient, err = api.NewClient(vaultCFG)
		if err != nil {
			return nil, err
		}
	}
	tokenLock.Lock()
	defer tokenLock.Unlock()

	if time.Now().After(tokenDeadline) {
		vtoken = ""
	}
	//logger.Log.Debugf("Size of env vars: vtoken=%d vtoken1=%d vaddr=%d",len(vtoken),len(vaddr))

	if vtoken == "" {
		var tokenTTL time.Duration
		vtoken, tokenTTL, err = login()
		if err != nil {
			return nil, errors.Wrapf(err, "failed to login to vault at %v", vaddr)
		}
		tokenDeadline = time.Now().Add(tokenTTL)
		logger.Info("logged into vault while reading a secret")
	}
	vClient.SetToken(vtoken)
	if vLogical == nil {
		vLogical = vClient.Logical()
	}
	return vLogical, nil
}

//Renew attempts to prolong the TTL of the token for future use
func renew(increment int) error {
	logger.Debug("start renewing vault token ")
	vaultCFG := api.DefaultConfig()
	vaultCFG.Address = vaddr
	vaultCFG.HttpClient = CreateHTTPClient()
	clientTLSConfig := &tls.Config{
		MinVersion: tls.VersionTLS12,
	}
	vaultCFG.HttpClient.Transport.(*http.Transport).TLSClientConfig = clientTLSConfig

	t := &api.TLSConfig{
		CACert:        "",
		CAPath:        "",
		ClientCert:    vcert,
		ClientKey:     vkey,
		TLSServerName: "localhost",
		Insecure:      true,
	}
	err := vaultCFG.ConfigureTLS(t)
	if err != nil {
		logger.Errorf("failed to configire TLS %v", err)
	}
	vClient, err := api.NewClient(vaultCFG)
	if err != nil {
		return err
	}
	tokenLock.Lock()
	defer tokenLock.Unlock()
	if vtoken == "" {
		var tokenTTL time.Duration
		vtoken, tokenTTL, err = login()
		if err != nil {
			return errors.Wrapf(err, "failed to login to vault at %v while renewing vault token", vaddr)
		}
		tokenDeadline = time.Now().Add(tokenTTL)

		logger.Info("logged into vault while renewing vault token")
		return nil
	}
	vClient.SetToken(vtoken)
	_, er := vClient.Auth().Token().RenewSelf(increment)
	if er != nil {
		if strings.Contains(er.Error(), "lease expired") || strings.Contains(er.Error(), "permission denied") {
			vtoken = ""
		} else if strings.Contains(er.Error(), "invalid certificate") {
			err := registerContainer()
			if err != nil {
				logger.Error(err)
			}
		}
		return er
	}
	logger.Info("success renewing vault token")

	return nil
}
