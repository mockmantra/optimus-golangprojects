package models

// RmqMessage stores data that will be sent in an RMQ message
type RmqMessage struct {
	Message       []byte `json:"Message"`
	FirmID        int    `json:"FirmID"`
	ActivityID    string `json:"ActivityID"`
	UserID        int    `json:"UserID"`
	UserName      string `json:"UserName"`
	FirmAuthToken string `json:"FirmAuthToken"`
}
