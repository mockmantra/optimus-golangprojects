package models

// RmqConfig stores data to communicate with rmq server
type RmqConfig struct {
	HostPort         string
	Silo             string
	User             string
	Password         string
	RancherHost      string
	RancherContainer string
}
