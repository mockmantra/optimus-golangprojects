package constants

//StatisticsKey how we call the key of the statistics map on the context
const StatisticsKey = "Statistics"

// NoStatisticsError message if stats has not been added to the context
const NoStatisticsError = "no statistics map found in context"
