package bitemporality

// IBiTemporal - Interface for accessing BiTemporal sql operations
type IBiTemporal interface {
	SelectFromTo(whereRange WhereRange) (data interface{}, countData int, err error)
	SelectActive(validToTableKey string) (data interface{}, countData int, err error)
	SelectDeleted() (data interface{}, countData int, err error)
	Insert(isDeleted bool, payload interface{}, validDateTimeFields DateTimeUpdate) error
	InsertWithBusinessDateTime(isDeleted bool, payload interface{}, validDateTimeFields DateTimeUpdate, businessDateTimeFields DateTimeUpdate) error
	InsertBulk(isDeleted bool, payload interface{}, validDateTimeFields DateTimeUpdate) (insertCount int, err error)
}
