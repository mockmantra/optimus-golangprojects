package booktype

const (
	// Net Book Type
	Net = 1
	// Location Book Type
	Location = 2
	// Strategy Book Type
	Strategy = 3
)
