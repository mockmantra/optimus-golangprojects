def folder='accounting/ibor-tx-master-corpbonds'
def testFolder="$folder/tests"

//This is the main container build

job("$folder/build_ibor_tx_master_corpbonds") {
  label('autoscale')
  multiscm {
    git {
      remote {
        name('origin')
        url('ssh://git@stash.ezesoft.net:7999/imsacnt/ibor-tx-master-corpbonds.git')
        credentials('stash_global')
      }
      branch('master')
      extensions {
        submoduleOptions {
          //get latest submodules
          tracking(true)
        }
       }
      }
  }
	triggers {
		scm('H/5* * * * *')
	}
  wrappers {
    buildInDocker {
      dockerfile('./rancher/slave')
      volume('/var/run/docker.sock', '/var/run/docker.sock')
      userGroup('2000')
      verbose()
    }
    environmentVariables {
    	propertiesFile('properties')
    }
    credentialsBinding {
      usernamePassword('dtr_user', 'dtr_password', 'dtr-builder')
      usernamePassword('vaultguard_username', 'vaultguard_password', 'vaultguard-user')
    }
    preBuildCleanup()
    maskPasswords()
    ansiColorBuildWrapper {
      colorMapName('XTerm')
      defaultFg(15)
      defaultBg(1)
    }
    buildName('${ENV,var="container_name"} ${ENV,var="version"}')
    sonarBuildWrapper {
      installationName 'SonarQube'
    }
  }
  steps {

    // Pull the latest jenkins-scripts
    shell('if [ -d $scripts ]; then rm -rf $scripts; fi && git clone $scripts_repo')

    // build the application on slave
    shell('bash scripts/build-base.sh')
    
    // Run lint and unittests
    shell('bash scripts/runner.sh')

    // run end to end tests
    //shell('bash scripts/run-e2e.sh')
    
    // Build the container
    shell('bash jenkins-scripts/build-container-step.sh')

    // Upload the container
    shell('bash jenkins-scripts/upload-container-step.sh')

    // Register container with Vault
    shell('bash jenkins-scripts/register-container.sh Dev')
    shell('bash jenkins-scripts/register-container.sh AWSProd')

    // Assemble some variables to pass on the stack build
    shell('bash jenkins-scripts/vars.template')

    // Run sonarqube scanner
    sonarRunnerBuilder({
     project 'sonar-project.properties'
     properties 'sonar.projectVersion=$version_prefix.$BUILD_NUMBER'
     jdk '(Inherit From Job)'
     task 'scan'
    })

    copyArtifacts("$folder/build_ibor_tx_master_corpbonds") {
        includePatterns('*.vars')
        targetDirectory('vars')
        flatten()
        optional(false)
        buildSelector {
            latestSuccessful(true)
        }
    }

    // Update Rancher Stack Information
    shell('cd rancher && bash ../jenkins-scripts/rancher-run-template.sh && bash ../jenkins-scripts/rancher-update-catalog.sh')
  }

  publishers {

    archiveArtifacts{
      pattern("*.vars,reports/*.xml")
      pattern("docker-compose.yml")
      pattern("rancher-compose.yml")
      onlyIfSuccessful()
    }

	whiteSourcePublisher {
		jobCheckPolicies('global')
		jobForceUpdate('global')
		jobApiToken('bc083042-88fb-4776-9288-afdf9044ae01')
		jobUserKey(null)
		product('b8072ee844704d9ca5cdecce033784522464af0749b04c26953779d48e55acad')
		productVersion(null)
		projectToken(null)
		libIncludes('**/*.c **/*.cc **/*.cp **/*.cpp **/*.cxx **/*.c++ **/*.h **/*.hpp **/*.hxx **/*.dll **/*.cs **/*.nupkg **/*.m **/*.mm **/*.js **/*.php **/*.gem **/*.rb **/*.tgz **/*.deb **/*.gzip **/*.rpm **/*.tar.bz2 **/*.zip **/*.tar.gz **/*.egg **/*.whl **/*.py')
		libExcludes('**/*.jar')
		requesterEmail(null)
		ignorePomModules(false)
		mavenProjectToken(null)
		modulesToInclude(null)
		modulesToExclude(null)
		moduleTokens(null)
	}
    stashNotifier()

    //Yes we need all these settings :(
    slackNotifier {
      teamDomain(null)
      authToken(null)
      room('team-optimus')
      startNotification(true)
      notifyNotBuilt(true)
      notifyAborted(true)
      notifyFailure(true)
      notifySuccess(true)
      notifyUnstable(true)
      notifyBackToNormal(true)
      notifyRepeatedFailure(true)
      includeTestSummary(true)
      includeCustomMessage(false)
      customMessage(null)
      buildServerUrl(null)
      sendAs(null)
      commitInfoChoice('NONE')
    }


    triggers {
      scm('H/5 * * * *')
    }
  }
}
