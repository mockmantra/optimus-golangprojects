Project Name
TODO: Write a project description

Installation
TODO: Describe the installation process

Usage
Steps to configure authentication api through nginx.
Clone and run the repo mock-eclipse. And run npm start at root directory. This api runs on the port 3000.

Install nginx in local if not done already and add below configuration in nginx.conf file under /usr/local/etc/nginx/nginx.conf and restart the nginx service.

Add upstreams in http section

upstream Platform_Api_api_platform_v1 {
    server localhost:3000;
}

upstream Ibor_transaction_read_api_v1 {
    server localhost:33380;
}
Add/Update following in server section

listen 8001

location /api/platform/v1 {
    proxy_pass http://Platform_Api_api_platform_v1;
}
location /api/ibor/tpnl/equity/v1 {
    proxy_pass http://ibor_pnl_equity_api_v1;
}
Note: Now the api can be accessed using http://localhost:8001/api/ibor/pnl/equity/v1/doc/swagger

Contributing
Fork it!
Create your feature branch: git checkout -b my-new-feature
Commit your changes: git commit -am 'Add some feature'
Push to the branch: git push origin my-new-feature
Submit a pull request 😃
History
TODO: Write history

Credits
TODO: Write credits

License
TODO: Copyright (c) 2015 Eze Software Group. All rights reserved.