package controllers

import (
	"encoding/json"
	"errors"
	"regexp"

	"github.com/gin-gonic/gin"
	gcontext "github.com/gorilla/context"
	calculator "stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/calculator"
	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/mocks"
	"stash.ezesoft.net/ipc/ezelogger"

	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/database"
	domain "stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/domainEntities"
)

// PnlRequest JSON
type PnlRequest struct {
	LogicalTimeID int    `json:"logicalTimeId"`
	BusinessDate  string `json:"businessDate"`
}

func registerCalculateProfitLoss(router *gin.RouterGroup) {
	router.POST("/calculateProfitLoss", calculateProfitLoss)
}

func calculateProfitLoss(c *gin.Context) {
	workflowContext := gcontext.Get(c.Request, domain.WorkflowContextKey).(*domain.WorkflowContext)
	workflowContext.Logger.Info("Starting to calculate profit and loss")

	pnlRequest, err := validateAndCreatePnlRequest(c)
	if err != nil {
		workflowContext.Logger.Error(err)
		c.JSON(400, gin.H{"message": "Bad request"})
		return
	}

	pnlValuations := getPnlValuations(pnlRequest.LogicalTimeID, pnlRequest.BusinessDate, workflowContext.Logger)
	if len(pnlValuations) == 0 {
		c.JSON(200, gin.H{"message": "No valuations returned for input"})
		return
	}

	pnl := calculator.CalculateProfitLoss(pnlValuations, workflowContext.Logger)
	err = writeInDB(workflowContext, pnl)
	if err != nil {
		workflowContext.Logger.Error(err)
		c.JSON(500, gin.H{"message": "internal error occured"})
		return
	}

	workflowContext.Logger.Info("Finished calculating profit and loss")
	c.JSON(200, gin.H{"message": "Database updated with calculated Pnl"})
}

func validateAndCreatePnlRequest(c *gin.Context) (PnlRequest, error) {
	var pnlRequest PnlRequest
	err := c.ShouldBindJSON(&pnlRequest)
	if err != nil {
		return pnlRequest, err
	}
	dateRegEx := "^(0[1-9]|1[0-2])\\/(0[1-9]|1\\d|2\\d|3[01])\\/\\d{4}$"
	match, err := regexp.MatchString(dateRegEx, pnlRequest.BusinessDate)

	if !match {
		return pnlRequest, errors.New("incorrect date field")
	}
	return pnlRequest, nil
}

func getPnlValuations(logicalTimeID int, businessDate string, logger *ezelogger.EzeLog) []domain.Valuation {
	//valuations := []domain.Valuation{}
	valuations := make([]domain.Valuation, 0)
	err := json.Unmarshal([]byte(mocks.GetValuationResponse()), &valuations)
	if err != nil {
		logger.Error(err)
	}

	// valuation1 := domain.Valuation{
	// 	ID:                         1,
	// 	LotID:                      100,
	// 	LogicalTimeID:              logicaltime.EndOfDay,
	// 	ValueLocal:                 1000.00,
	// 	SecurityPriceLocal:         10.000,
	// 	Quantity:                   100,
	// 	ValidFromDateTimeUTC:       time.Now(),
	// 	IsDeleted:                  false,
	// 	BusinessDate:               time.Date(2019, 01, 10, 00, 00, 00, 00, time.UTC),
	// 	BaseToLocalFXRate:          2.203,
	// 	SystemToLocalFXRate:        3.4567,
	// 	UnrealizedProfitLossLocal:  10000,
	// 	UnrealizedProfitLossBase:   10000,
	// 	UnrealizedProfitLossSystem: 10000,
	// 	FXGainLossBase:             10000,
	// 	FXGainLossSystem:           10000,
	// 	LocalCurrencyID:            12,
	// 	BaseCurrencyID:             13,
	// 	CostLocal:                  12.23,
	// 	CostSystem:                 14.56,
	// 	CostBase:                   17.89,
	// }

	// valuation2 := domain.Valuation{
	// 	ID:                         2,
	// 	LotID:                      100,
	// 	LogicalTimeID:              logicaltime.StartOfDay,
	// 	ValueLocal:                 1000.00,
	// 	SecurityPriceLocal:         10.00,
	// 	Quantity:                   100,
	// 	ValidFromDateTimeUTC:       time.Now(),
	// 	IsDeleted:                  false,
	// 	BusinessDate:               time.Date(2019, 01, 11, 00, 00, 00, 00, time.UTC),
	// 	BaseToLocalFXRate:          2.203,
	// 	SystemToLocalFXRate:        3.4567,
	// 	UnrealizedProfitLossLocal:  10400,
	// 	UnrealizedProfitLossBase:   10400,
	// 	UnrealizedProfitLossSystem: 10400,
	// 	FXGainLossBase:             10400,
	// 	FXGainLossSystem:           10400,
	// 	LocalCurrencyID:            12,
	// 	BaseCurrencyID:             13,
	// 	CostLocal:                  12.23,
	// 	CostSystem:                 14.56,
	// 	CostBase:                   17.89,
	// }

	//valuations = append(valuations, valuation1, valuation2, valuation3)

	return valuations
}

func writeInDB(workflowContext *domain.WorkflowContext, pnl []domain.ProfitLoss) error {
	da := database.DataAccessorInstance()
	workflowContext.Logger.Info("Establishing connection to the database")
	err := da.Connect(workflowContext.UserSession.FirmAuthToken)

	if err != nil {
		return err
	}

	defer func() {
		err = da.Close()
		if err != nil {
			workflowContext.Logger.Error(err)
		}
	}()

	workflowContext.Logger.Info("Successfully established connection to the database")
	workflowContext.Logger.Info("Beginning db transactional write of calculated PnL")
	err = da.Write(workflowContext, pnl)
	if err != nil {
		return err
	}
	workflowContext.Logger.Info("Successfully persisted calculated PnL to DB")
	return nil
}
