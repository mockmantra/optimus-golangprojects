package controllers

import (
	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/config"

	"github.com/gin-gonic/gin"
)

// RegisterRoutes register all the routes with gin
func RegisterRoutes(router *gin.Engine) {
	pnlEquityRouter := router.Group(config.Config.APIBase)
	registerHealthCheck(pnlEquityRouter)
	registerCalculateProfitLoss(pnlEquityRouter)
}
