package interfaces

import (
	"database/sql"
	"github.com/jinzhu/gorm"
	domain "stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/domainEntities"
)

// IDB - Interface for DB struct in gorm
type IDB interface {
	HasTable(interface{}) bool
	CreateTable(...interface{}) *gorm.DB
	Create(interface{}) *gorm.DB
	Close() error
	Exec(sql string, values ...interface{}) *gorm.DB
	Table(name string) *gorm.DB
	ScanRows(rows *sql.Rows, result interface{}) error
	Where(query interface{}, args ...interface{}) *gorm.DB
	Select(query interface{}, args ...interface{}) *gorm.DB
	Joins(query string, args ...interface{}) *gorm.DB
	Begin() *gorm.DB
	Commit() *gorm.DB
	Rollback() *gorm.DB
	LogMode(enable bool) *gorm.DB
}

// IDataAccessor - Interface for accessing Database
type IDataAccessor interface {
	Close() error
	Connect(string) error
	Write(context *domain.WorkflowContext, profitLossList []domain.ProfitLoss) (e error)
}
