package app

import (
	"net/http"
	"time"

	"stash.ezesoft.net/ipc/ezelogger"

	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/config"
	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/controllers"
	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/logger"
	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/middlewares"

	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
	"github.com/go-openapi/loads"
	"github.com/shopspring/decimal"
)

// Application stores http server, Gin Engine and a default logger
type Application struct {
	*http.Server

	*gin.Engine

	Logger *ezelogger.EzeLog
}

var (
	engine = gin.Default()

	// App - global Applcation instance
	App = &Application{
		Server: &http.Server{
			Addr:         ":33380",
			Handler:      engine,
			ReadTimeout:  30 * time.Second,
			WriteTimeout: 50 * time.Second,
		},
		Logger: logger.GetLogger(nil, "", ""),
		Engine: engine,
	}
)

// Run configures middlewares and routs and run the server on 33380
func Run() {
	// require the this setting to marshal decimal without quotes
	decimal.MarshalJSONWithoutQuotes = true
	App.configureSwagger()
	App.Logger.Info("Registering middlewares")
	middlewares.RegisterMiddlewares(App.Engine)
	App.Logger.Info("Registering routes")
	controllers.RegisterRoutes(App.Engine)
	App.Logger.Info("Starting service on port 33380")

	//this is needed because the service start up very fast before the eze-route is made available
	//time.Sleep(time.Second * 10)

	err := App.Server.ListenAndServe()
	if err != nil {
		App.Logger.Error(err)
	}
}

func (app *Application) configureSwagger() {
	app.Engine.GET(config.Config.APIBase+"/swagger/swagger.json", serveSwaggerJSON)
	app.Logger.Info("Initializing static swagger files")
	app.Engine.StaticFile(config.Config.APIBase+"/doc/swagger", "./swagger-ui/index.html")
	app.Engine.Use(static.Serve(config.Config.APIBase+"/doc", static.LocalFile("./swagger-ui", false)))
}

func serveSwaggerJSON(c *gin.Context) {
	doc, err := loads.Spec("./swagger.yaml")
	if err != nil {
		c.JSON(500, gin.H{"message": "Error reading swagger doc"})
	}
	c.JSON(200, doc.Raw())
}
