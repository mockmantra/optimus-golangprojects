package middlewares

import (
	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/config"
	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/domainEntities"
	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/logger"

	"github.com/gin-gonic/gin"
	gcontext "github.com/gorilla/context"
)

func assignWorkflowContext(c *gin.Context) {
	if !config.Config.SkipAuth(c.Request.URL.Path) {
		activityID := c.Request.Header.Get("X-Request-Id")
		session := gcontext.Get(c.Request, domain.UserSessionContextKey).(*domain.UserSession)
		path := c.Request.URL.Path
		workflowContext := &domain.WorkflowContext{
			UserSession: session,
			ActivityID:  activityID,
			Logger:      logger.GetLogger(session, activityID, path),
		}
		gcontext.Set(c.Request, domain.WorkflowContextKey, workflowContext)
	}
	c.Next()
}
