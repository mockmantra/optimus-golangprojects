package middlewares

import (
	"net/url"
	"time"

	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/config"
	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/domainEntities"
	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/logger"

	"github.com/gin-gonic/gin"
	gcontext "github.com/gorilla/context"
)

func logRequest(c *gin.Context) {
	if !config.Config.SkipAuth(c.Request.URL.Path) {
		workflowContext := gcontext.Get(c.Request, domain.WorkflowContextKey).(*domain.WorkflowContext)
		query, err := url.QueryUnescape(c.Request.URL.RawQuery)
		if err != nil {
			query = c.Request.URL.RawQuery
		}
		log := workflowContext.Logger.WithFields(logger.Fields{
			"Url":     c.Request.URL.Path,
			"Request": query})

		log.Info("Start HTTP Request")
		start := time.Now()

		c.Next()

		elapsed := time.Since(start)

		log.WithFields(logger.Fields{"ExecutionDuration": elapsed.Nanoseconds() / 1000000}).Info("End Http Request")
	} else {
		c.Next()
	}
}
