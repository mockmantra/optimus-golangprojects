package logger

import (
	domain "stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/domainEntities"
	"stash.ezesoft.net/ipc/ezelogger"
	"stash.ezesoft.net/ipc/ezeutils"
)

var (
	// Log instance
	Log *ezelogger.EzeLog
)

// Fields ...
type Fields map[string]interface{}

func init() {
	Log = ezelogger.GetLogger("ibor-pnl-equity").Configure(ezelogger.CfgReplaceNewlines, ezelogger.CfgReplaceDoubleQuotes, ezelogger.CfgMessageLinesLimit(2048))
	ezeutils.SetLogger(Log)
}

// GetLogger return logger with user sessions fileds bound to the entry
func GetLogger(userSession *domain.UserSession, activityID string, path string) *ezelogger.EzeLog {
	logger := Log.WithFields(Fields{
		"AppDomain":  "ibor-pnl-equity",
		"Product":    "Accounting",
		"ActivityId": activityID,
		"Path":       path,
	})
	if userSession == nil {
		return logger
	}

	return logger.WithFields(Fields{
		"AppDomain":     "ibor-pnl-equity",
		"Product":       "Accounting",
		"ActivityId":    activityID,
		"FirmID":        userSession.FirmID,
		"UserID":        userSession.UserID,
		"FirmName":      userSession.FirmName,
		"UserName":      userSession.UserName,
		"FirmAuthToken": userSession.FirmAuthToken,
		"Path":          path,
	})
}
