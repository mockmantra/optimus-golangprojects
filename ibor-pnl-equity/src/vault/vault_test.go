package vault

import (
	"errors"
	"fmt"
	"net/http"
	"os"
	"reflect"
	"testing"
	"time"

	"github.com/bouk/monkey"
	"github.com/hashicorp/vault/api"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/logger"
	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/mocks"
	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/models"
)

func init() {
	/* load test data */
	err := os.Setenv("APP_ENV", "test")
	err = os.Setenv("VAULT_TOKEN", "test")
	err = os.Setenv("VAULT_CERT", "test")
	err = os.Setenv("VAULT_KEY", "test")
	if err != nil {
		fmt.Println("Error in setting the Environment fields")
	}
}

func TestGetDbCredentials(t *testing.T) {
	err := os.Setenv("APP_ENV", "test")
	err = os.Setenv("VAULT_TOKEN", "test")
	err = os.Setenv("VAULT_CERT", "test")
	err = os.Setenv("VAULT_KEY", "test")
	if err != nil {
		t.Error("Error in setting the Environment fields")
	}
	vaultAPI := new(mocks.VaultAPI)
	vaultClient := new(mocks.VaultClient)
	vd := Data{vaultClient, vaultAPI, logger.Log}
	vaultAPI.On("DefaultConfig", mock.Anything).Return(&mocks.MockConfig{})
	vaultAPI.On("NewClient", mock.Anything).Return(&mocks.MockClient{}, nil)
	mockSecret := &api.Secret{Auth: &api.SecretAuth{
		ClientToken: "hello"}}
	vaultAPI.On("ParseSecret", mock.Anything).Return(mockSecret, nil)
	var d *api.Logical
	monkey.PatchInstanceMethod(reflect.TypeOf(d), "Read", func(_ *api.Logical, path string) (*api.Secret, error) {
		return nil, fmt.Errorf("no dialing allowed")
	})
	defer monkey.UnpatchInstanceMethod(reflect.TypeOf(d), "Read")
	monkey.PatchInstanceMethod(reflect.TypeOf(d), "Read", func(_ *api.Logical, path string) (*api.Secret, error) {
		return &api.Secret{Data: map[string]interface{}{"firm": "mockAuroraConnection"}}, nil
	})
	defer monkey.UnpatchInstanceMethod(reflect.TypeOf(d), "Read")

	con := vd.GetDbCredentials()
	assert.NotNil(t, con)
}

func TestRetrieveCredsFromVault(t *testing.T) {
	vaultAPI := new(mocks.VaultAPI)
	vaultClient := new(mocks.VaultClient)

	vd := Data{vaultClient, vaultAPI, logger.Log}
	vaultAPI.On("DefaultConfig", mock.Anything).Return(&mocks.MockConfig{})
	vaultAPI.On("NewClient", mock.Anything).Return(&mocks.MockClient{}, nil)
	mockSecret := &api.Secret{Auth: &api.SecretAuth{
		ClientToken: "hello"}}
	vaultAPI.On("ParseSecret", mock.Anything).Return(mockSecret, nil)
	var d *api.Logical
	monkey.PatchInstanceMethod(reflect.TypeOf(d), "Read", func(_ *api.Logical, path string) (*api.Secret, error) {
		return nil, fmt.Errorf("no dialing allowed")
	})
	defer monkey.UnpatchInstanceMethod(reflect.TypeOf(d), "Read")
	monkey.PatchInstanceMethod(reflect.TypeOf(d), "Read", func(_ *api.Logical, path string) (*api.Secret, error) {
		return &api.Secret{Data: map[string]interface{}{"firm": "mockAuroraConnection"}}, nil
	})
	defer monkey.UnpatchInstanceMethod(reflect.TypeOf(d), "Read")
	m := vd.retrieveCredsFromVault()
	assert.Nil(t, m)
}

func TestRetrieveCredsFromVaultError(t *testing.T) {
	vaultAPI := new(mocks.VaultAPI)
	vaultClient := new(mocks.VaultClient)

	vd := Data{vaultClient, vaultAPI, logger.Log}
	vaultAPI.On("DefaultConfig", mock.Anything).Return(&mocks.MockConfig{})
	vaultAPI.On("NewClient", mock.Anything).Return(&mocks.MockClient{}, nil)
	vaultAPI.On("ParseSecret", mock.Anything).Return(nil, errors.New("Error Occured"))
	var d *api.Logical
	monkey.PatchInstanceMethod(reflect.TypeOf(d), "Read", func(_ *api.Logical, path string) (*api.Secret, error) {
		return nil, fmt.Errorf("no dialing allowed")
	})
	defer monkey.UnpatchInstanceMethod(reflect.TypeOf(d), "Read")
	monkey.PatchInstanceMethod(reflect.TypeOf(d), "Read", func(_ *api.Logical, path string) (*api.Secret, error) {
		return &api.Secret{Data: map[string]interface{}{"firm": "mockAuroraConnection"}}, nil
	})
	defer monkey.UnpatchInstanceMethod(reflect.TypeOf(d), "Read")
	m := vd.retrieveCredsFromVault()
	assert.NotNil(t, m)
}

func TestBuildConnectionMap(t *testing.T) {
	conMap := make(map[string]interface{})
	conMap["firm"] = map[string]interface{}{"DB_HOST": "testHost", "DB_UID": "testUser", "DB_PWD": "testpass"}
	mockAuroraConnection := &models.AuroraConnection{Host: "testHost", Password: "testpass", Port: 3306, User: "testUser"}
	m, err := BuildConnectionMap(conMap)
	assert.Equal(t, m, map[string]*models.AuroraConnection{"firm": mockAuroraConnection})
	assert.Nil(t, err)
}

func TestLogin(t *testing.T) {
	vaultAPI := new(mocks.VaultAPI)
	vaultClient := new(mocks.VaultClient)
	mockReadCloser := new(mocks.ReadCloser)
	mockReadCloser.On("Close").Return(nil)
	mockResp := &api.Response{&http.Response{Body: mockReadCloser}}
	fmt.Printf("%T", mockResp.Body.Close())
	mockSecret := &api.Secret{}
	vaultClient.On("NewRequest", mock.Anything, mock.Anything).Return(&api.Request{})
	vaultClient.On("RawRequest", mock.Anything).Return(mockResp, nil)
	vaultAPI.On("ParseSecret", mock.Anything).Return(mockSecret, nil)
	vd := Data{vaultClient, vaultAPI, logger.Log}
	secret, err := vd.Login()
	assert.Equal(t, secret, &api.Secret{})
	assert.Nil(t, err)

}
func TestLoginError(t *testing.T) {
	vaultAPI := new(mocks.VaultAPI)
	vaultClient := new(mocks.VaultClient)
	mockReadCloser := new(mocks.ReadCloser)
	mockReadCloser.On("Close").Return(nil)
	mockResp := &api.Response{&http.Response{Body: mockReadCloser}}
	fmt.Printf("%T", mockResp.Body.Close())
	mockSecret := &api.Secret{}
	vaultClient.On("NewRequest", mock.Anything, mock.Anything).Return(&api.Request{})
	vaultClient.On("RawRequest", mock.Anything).Return(mockResp, errors.New(""))
	vaultAPI.On("ParseSecret", mock.Anything).Return(mockSecret, nil)
	vd := Data{vaultClient, vaultAPI, logger.Log}
	secret, err := vd.Login()
	assert.Nil(t, secret)
	assert.NotNil(t, err)

}

func TestReadTenantCreds(t *testing.T) {
	vaultAPI := new(mocks.VaultAPI)
	vaultClient := new(mocks.VaultClient)
	vd := Data{vaultClient, vaultAPI, logger.Log}
	vaultAPI.On("DefaultConfig", mock.Anything).Return(&mocks.MockConfig{})
	vaultAPI.On("NewClient", mock.Anything).Return(&mocks.MockClient{}, nil)
	mockSecret := &api.Secret{Auth: &api.SecretAuth{
		ClientToken: "hello"}}
	vaultAPI.On("ParseSecret", mock.Anything).Return(mockSecret, nil)
	var d *api.Logical
	monkey.PatchInstanceMethod(reflect.TypeOf(d), "Read", func(_ *api.Logical, path string) (*api.Secret, error) {
		return &api.Secret{Data: map[string]interface{}{"firm": "mockAuroraConnection"}}, nil
	})
	defer monkey.UnpatchInstanceMethod(reflect.TypeOf(d), "Read")

	m, errm := vd.ReadTenantCreds()
	assert.Equal(t, m, map[string]interface{}{"firm": "mockAuroraConnection"})
	assert.Nil(t, errm)

}
func TestReadTenantCredsNilError(t *testing.T) {
	vaultAPI := new(mocks.VaultAPI)
	vaultClient := new(mocks.VaultClient)
	vd := Data{vaultClient, vaultAPI, logger.Log}
	vaultAPI.On("DefaultConfig", mock.Anything).Return(&mocks.MockConfig{})
	vaultAPI.On("NewClient", mock.Anything).Return(&mocks.MockClient{}, nil)
	mockSecret := &api.Secret{Auth: &api.SecretAuth{
		ClientToken: "hello"}}
	vaultAPI.On("ParseSecret", mock.Anything).Return(mockSecret, nil)
	var d *api.Logical
	monkey.PatchInstanceMethod(reflect.TypeOf(d), "Read", func(_ *api.Logical, path string) (*api.Secret, error) {
		return &api.Secret{Data: map[string]interface{}{"firm": "mockAuroraConnection"}}, nil
	})
	defer monkey.UnpatchInstanceMethod(reflect.TypeOf(d), "Read")
	vaultTokenTimeout = time.Now().Sub(vaultTokenInitTime)

	m, errm := vd.ReadTenantCreds()
	assert.Equal(t, m, map[string]interface{}{"firm": "mockAuroraConnection"})
	assert.Nil(t, errm)

}

func TestReadTenantCredsNilMap(t *testing.T) {
	vaultAPI := new(mocks.VaultAPI)
	vaultClient := new(mocks.VaultClient)
	vd := Data{vaultClient, vaultAPI, logger.Log}
	vaultAPI.On("DefaultConfig", mock.Anything).Return(&mocks.MockConfig{})
	vaultAPI.On("NewClient", mock.Anything).Return(&mocks.MockClient{}, nil)
	mockSecret := &api.Secret{Auth: &api.SecretAuth{
		ClientToken: "hello"}}
	vaultAPI.On("ParseSecret", mock.Anything).Return(mockSecret, nil)
	var d *api.Logical
	monkey.PatchInstanceMethod(reflect.TypeOf(d), "Read", func(_ *api.Logical, path string) (*api.Secret, error) {
		return nil, nil
	})
	defer monkey.UnpatchInstanceMethod(reflect.TypeOf(d), "Read")

	m, errm := vd.ReadTenantCreds()
	assert.Nil(t, m)
	assert.NotNil(t, errm)

}
func TestReadTenantCredsErrorInternal(t *testing.T) {
	vaultAPI := new(mocks.VaultAPI)
	vaultClient := new(mocks.VaultClient)
	vd := Data{vaultClient, vaultAPI, logger.Log}
	vaultAPI.On("DefaultConfig", mock.Anything).Return(&mocks.MockConfig{})
	vaultAPI.On("NewClient", mock.Anything).Return(&mocks.MockClient{}, errors.New(""))
	mockSecret := &api.Secret{Auth: &api.SecretAuth{
		ClientToken: "hello"}}
	vaultAPI.On("ParseSecret", mock.Anything).Return(mockSecret, nil)
	var d *api.Logical
	monkey.PatchInstanceMethod(reflect.TypeOf(d), "Read", func(_ *api.Logical, path string) (*api.Secret, error) {
		return nil, nil
	})
	defer monkey.UnpatchInstanceMethod(reflect.TypeOf(d), "Read")

	m, errm := vd.ReadTenantCreds()
	assert.Nil(t, m)
	assert.NotNil(t, errm)

}

func TestReadTenantCredsError2(t *testing.T) {
	vaultAPI := new(mocks.VaultAPI)
	vaultClient := new(mocks.VaultClient)
	vd := Data{vaultClient, vaultAPI, logger.Log}
	vaultAPI.On("DefaultConfig", mock.Anything).Return(&mocks.MockConfig{})
	vaultAPI.On("NewClient", mock.Anything).Return(&mocks.MockClient{}, nil)
	mockSecret := &api.Secret{Auth: &api.SecretAuth{
		ClientToken: "hello"}}
	vaultAPI.On("ParseSecret", mock.Anything).Return(mockSecret, nil)
	var d *api.Logical
	monkey.PatchInstanceMethod(reflect.TypeOf(d), "Read", func(_ *api.Logical, path string) (*api.Secret, error) {
		return nil, errors.New("")
	})
	defer monkey.UnpatchInstanceMethod(reflect.TypeOf(d), "Read")

	m, errm := vd.ReadTenantCreds()
	assert.Nil(t, m)
	assert.NotNil(t, errm)

}

func TestReadTenantCredsError3(t *testing.T) {
	vaultAPI := new(mocks.VaultAPI)
	vaultClient := new(mocks.VaultClient)
	vd := Data{vaultClient, vaultAPI, logger.Log}
	vaultAPI.On("DefaultConfig", mock.Anything).Return(&mocks.MockConfig{})
	vaultAPI.On("NewClient", mock.Anything).Return(&mocks.MockClient{}, nil)
	mockSecret := &api.Secret{Auth: &api.SecretAuth{
		ClientToken: "hello"}}
	vaultAPI.On("ParseSecret", mock.Anything).Return(mockSecret, errors.New(""))
	var d *api.Logical
	monkey.PatchInstanceMethod(reflect.TypeOf(d), "Read", func(_ *api.Logical, path string) (*api.Secret, error) {
		return nil, errors.New("")
	})
	defer monkey.UnpatchInstanceMethod(reflect.TypeOf(d), "Read")

	m, errm := vd.ReadTenantCreds()
	assert.Nil(t, m)
	assert.NotNil(t, errm)

}

func TestReadTenantCredsError5(t *testing.T) {
	vaultAPI := new(mocks.VaultAPI)
	vaultClient := new(mocks.VaultClient)
	vd := Data{vaultClient, vaultAPI, logger.Log}
	vaultAPI.On("DefaultConfig", mock.Anything).Return(&mocks.MockConfig{})
	vaultAPI.On("NewClient", mock.Anything).Return(&mocks.MockClient{}, nil)
	vaultAPI.On("ParseSecret", mock.Anything).Return(nil, nil)
	var d *api.Logical
	monkey.PatchInstanceMethod(reflect.TypeOf(d), "Read", func(_ *api.Logical, path string) (*api.Secret, error) {
		return nil, nil
	})
	defer monkey.UnpatchInstanceMethod(reflect.TypeOf(d), "Read")
	m, errm := vd.ReadTenantCreds()
	assert.Nil(t, m)
	assert.NotNil(t, errm)

}
