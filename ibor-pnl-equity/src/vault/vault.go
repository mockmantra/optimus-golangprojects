package vault

import (
	"fmt"
	"io"
	"os"
	"sync"
	"time"

	"github.com/hashicorp/vault/api"
	appconfig "stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/config"
	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/interfaces"
	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/logger"
	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/models"
)

// Data ...
type Data struct {
	vct      interfaces.IVaultClient
	VHandler interfaces.IVaultAPI
	Log      interfaces.ILogger
}

const iborTransPath = "/secret/ims/ibor-pnl-equity/aurora/tenant/%s"

var (
	vaultToken         string
	vaultCert          string
	vaultKey           string
	vaultTokenTTL      int
	vaultTokenTimeout  time.Duration
	vaultTokenInitTime time.Time
	mutex              *sync.Mutex
	dbCredentials      map[string]*models.AuroraConnection
	lock               sync.Mutex
)

func init() {

	logger.Log.Debug("[ETI][vault.go]: Init() called for Env: ", appconfig.RUN_ENV)

	if !appconfig.IsDevMode {
		vaultToken = os.Getenv("VAULT_TOKEN")
		if len(vaultToken) == 0 {
			panic("[ETI][vault.go]: VAULT_TOKEN variable is not set")
		}

		vaultCert = os.Getenv("VAULT_CERT")
		if len(vaultCert) == 0 {
			panic("[ETI][vault.go] :VAULT_CERT variable is not set")
		}

		vaultKey = os.Getenv("VAULT_KEY")
		if len(vaultKey) == 0 {
			panic("[ETI][vault.go] :VAULT_KEY variable is not set")
		}

		vaultTokenTTL = 3600
		vaultTokenInitTime = time.Now()
		vaultTokenTimeout = 55 * time.Minute
		mutex = &sync.Mutex{}
	}
}

// GetDbCredentials ...
func (vd *Data) GetDbCredentials() map[string]*models.AuroraConnection {
	if len(dbCredentials) != 0 {
		return dbCredentials
	}
	err := vd.retrieveCredsFromVault()
	if err != nil {
		vd.Log.Debugf("[ETI][vault.go]: Error In Reading Credetentials, %v", err)
	}

	return dbCredentials
}

// GetTenantCredentials ...
func (vd *Data) GetTenantCredentials(firmToken string) (*models.AuroraConnection, error) {
	con, ok := vd.GetDbCredentials()[firmToken]
	if !ok {
		return nil, fmt.Errorf("No db credentials found for firm: -%s-", firmToken)
	}
	return con, nil
}

// RetrieveCredsFromVault ...
func (vd *Data) retrieveCredsFromVault() error {
	m, err := vd.ReadTenantCreds()
	if err != nil {
		return err
	}
	vd.Log.Debugf("[ETI][vault.go] :Loaded secrets for %d tenants", len(m))
	dbCredentials, _ = BuildConnectionMap(m)
	return nil
}

// BuildConnectionMap ...
func BuildConnectionMap(m map[string]interface{}) (map[string]*models.AuroraConnection, error) {
	ret := make(map[string]*models.AuroraConnection, len(m))
	for k, v := range m {
		mi, ok := v.(map[string]interface{})
		if !ok {
			logger.Log.Errorf("[ETI][vault.go] :Invalid interface conversion in vault secret %T for key: %s", v, k)
			continue
		}
		ret[k] = &models.AuroraConnection{
			Port:     3306,
			Host:     mi["DB_HOST"].(string),
			User:     mi["DB_UID"].(string),
			Password: mi["DB_PWD"].(string),
		}
	}
	return ret, nil
}

// ReadTenantCreds stores all tenant aurora creds in vault and returns what it read back from vault
func (vd *Data) ReadTenantCreds() (map[string]interface{}, error) {
	if err := vd.doRenewVaultToken(); err != nil {
		logger.Log.Warn("[ETI][vault.go] :Unable to renew Vault token in order to update credentails!!")
		return nil, err
	}

	vaultCFG := vd.VHandler.DefaultConfig()

	var err error
	vClient, err := vd.VHandler.NewClient(vaultCFG)
	if err != nil {
		logger.Log.Debugf("[ETI][vault.go][ReadTenantCreds]: failed creating new client %v", err)
		return nil, err
	}

	//logger.Log.Debugf("Size of env vars: vtoken=%d vtoken1=%d vaddr=%d",len(vtoken),len(vaddr))
	vClient.SetToken(vaultToken)
	vault := vClient.Logical()

	spath := fmt.Sprintf(iborTransPath, appconfig.AWS_REGION_NAME)
	secret, err := vault.Read(spath)
	if err != nil {
		logger.Log.Debugf("[ETI][vault.go][ReadTenantCreds]: failed reading from path %v", err)
		return nil, err
	}
	if secret == nil {
		logger.Log.Debugf("[ETI][vault.go][ReadTenantCreds] :secret is nil")
		return nil, fmt.Errorf("[ETI][vault.go] :no secret read back at path %s", spath)
	}
	logger.Log.Infof("[ETI][vault.go]: vault tenant map size %d", len(secret.Data))

	return secret.Data, nil
}

func (vd *Data) doRenewVaultToken() error {
	tokenDuration := time.Now().Sub(vaultTokenInitTime)
	if tokenDuration > vaultTokenTimeout {
		token, err := vd.renewVaultToken()
		if err == nil {
			vaultTokenInitTime = time.Now()
			vaultToken = token
		}
		return err
	}
	return nil
}

func (vd *Data) renewVaultToken() (string, error) {
	logger.Log.Debug("[ETI][vault.go] :Start renewing vault token")
	vaultCFG := vd.VHandler.DefaultConfig()
	t := &api.TLSConfig{
		CACert:        "",
		CAPath:        "",
		ClientCert:    vaultCert,
		ClientKey:     vaultKey,
		TLSServerName: "localhost",
		Insecure:      true,
	}
	err := vaultCFG.ConfigureTLS(t)
	if err != nil {
		logger.Log.Errorf("[ETI][vault.go] :failed to configire TLS %v", err)
	}
	vClient, err := vd.VHandler.NewClient(vaultCFG)
	if err != nil {
		return "", err
	}
	vd.vct = vClient

	secret, err := vd.Login()
	if err != nil {
		logger.Log.Error("[ETI][vault.go] :Vault login using certificate failed.")
		return "", err
	}
	if secret == nil {
		return "", fmt.Errorf("[ETI][vault.go] :Failed to create token, received nil secret")
	}
	logger.Log.Info("[ETI][vault.go] :success renewing vault token")
	return secret.Auth.ClientToken, nil
}

// Login ...
func (vd *Data) Login() (*api.Secret, error) {
	req := vd.vct.NewRequest("POST", "/v1/auth/cert/login")

	resp, err := vd.vct.RawRequest(req)
	if err != nil {
		return nil, err
	}

	defer func(f io.Closer) {
		if err := f.Close(); err != nil {
			logger.Log.Error("[ETI][vault.go] :Error in closing the resp.body")
		}
	}(resp.Body)

	return vd.VHandler.ParseSecret(resp.Body)
}
