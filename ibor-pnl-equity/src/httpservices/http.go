package httpservices

import (
	"net/http"
	"time"
)

var (
	client = &http.Client{
		Transport: &http.Transport{
			MaxIdleConns:       10,
			IdleConnTimeout:    30 * time.Second,
			DisableCompression: true,
		},
		Timeout: time.Duration(60) * time.Second}
)

// GetHTTPClient returns instance of http client shared by the service instance
func GetHTTPClient() *http.Client {
	return client
}
