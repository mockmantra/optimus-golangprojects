package calculator

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"stash.ezesoft.net/imsacnt/accounting-utils/enums/logicaltime"
	domain "stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/domainEntities"
	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/logger"
)

func TestCalculation_ForOnlyOneValuation(t *testing.T) {
	valuations := []domain.Valuation{}
	valuation1 := domain.Valuation{
		ID:                         1,
		LotID:                      100,
		LogicalTimeID:              logicaltime.EndOfDay,
		ValueLocal:                 1000.00,
		SecurityPriceLocal:         10.000,
		Quantity:                   100,
		IsDeleted:                  false,
		BusinessDate:               time.Date(2019, 01, 10, 00, 00, 00, 00, time.UTC),
		BaseToLocalFXRate:          2.203,
		SystemToLocalFXRate:        3.4567,
		UnrealizedProfitLossLocal:  10000,
		UnrealizedProfitLossBase:   10000,
		UnrealizedProfitLossSystem: 10000,
		FXGainLossBase:             10000,
		FXGainLossSystem:           10000,
		LocalCurrencyID:            12,
		BaseCurrencyID:             13,
		CostLocal:                  12.23,
		CostSystem:                 14.56,
		CostBase:                   17.89,
		PositionStateID:            60,
		CloseoutGroupID:            1,
	}
	valuations = append(valuations, valuation1)

	pnlValuations := CalculateProfitLoss(valuations, logger.Log)

	assert.Equal(t, 1, len(pnlValuations))
	assert.Equal(t, 100, pnlValuations[0].LotID)
	assert.Equal(t, 10000.00, pnlValuations[0].ChangeInUnrealizedProfitLossLocal)
	assert.Equal(t, 10000.00, pnlValuations[0].ChangeInUnrealizedProfitLossBase)
	assert.Equal(t, 10000.00, pnlValuations[0].ChangeInUnrealizedProfitLossSystem)
	assert.Equal(t, 10000.00, pnlValuations[0].ChangeInUnrealizedFXGainLossBase)
	assert.Equal(t, 10000.00, pnlValuations[0].ChangeInUnrealizedFXGainLossSystem)
	assert.Equal(t, 2.203, pnlValuations[0].BaseToLocalFXRate)
	assert.Equal(t, 3.4567, pnlValuations[0].SystemToLocalFXRate)
	assert.Equal(t, time.Date(2019, 01, 10, 00, 00, 00, 00, time.UTC), pnlValuations[0].StartPeriodBusinessDate)
	assert.Equal(t, time.Date(2019, 01, 10, 00, 00, 00, 00, time.UTC), pnlValuations[0].EndPeriodBusinessDate)
	assert.Equal(t, logicaltime.EndOfDay, pnlValuations[0].StartPeriodLogicalTimeID)
	assert.Equal(t, logicaltime.EndOfDay, pnlValuations[0].EndPeriodLogicalTimeID)
	assert.Equal(t, 60, pnlValuations[0].PositionStateID)
	assert.Equal(t, 1, pnlValuations[0].CloseoutGroupID)
}

func TestEndOfDay_ForThreeValuations_GeneratesTwoRecords(t *testing.T) {
	valuations := getPnlValuations()

	pnlValuations := CalculateProfitLoss(valuations, logger.Log)

	assert.Equal(t, 2, len(pnlValuations))

	assert.Equal(t, 100, pnlValuations[0].LotID)
	assert.Equal(t, 400.00, pnlValuations[0].ChangeInUnrealizedProfitLossLocal)
	assert.Equal(t, 400.00, pnlValuations[0].ChangeInUnrealizedProfitLossBase)
	assert.Equal(t, 400.00, pnlValuations[0].ChangeInUnrealizedProfitLossSystem)
	assert.Equal(t, 400.00, pnlValuations[0].ChangeInUnrealizedFXGainLossBase)
	assert.Equal(t, 400.00, pnlValuations[0].ChangeInUnrealizedFXGainLossSystem)
	assert.Equal(t, 2.303, pnlValuations[0].BaseToLocalFXRate)
	assert.Equal(t, 3.5567, pnlValuations[0].SystemToLocalFXRate)
	assert.Equal(t, time.Date(2019, 01, 10, 00, 00, 00, 00, time.UTC), pnlValuations[0].StartPeriodBusinessDate)
	assert.Equal(t, time.Date(2019, 01, 11, 00, 00, 00, 00, time.UTC), pnlValuations[0].EndPeriodBusinessDate)
	assert.Equal(t, logicaltime.EndOfDay, pnlValuations[0].StartPeriodLogicalTimeID)
	assert.Equal(t, logicaltime.StartOfDay, pnlValuations[0].EndPeriodLogicalTimeID)
	assert.Equal(t, 60, pnlValuations[0].PositionStateID)
	assert.Equal(t, 1, pnlValuations[0].CloseoutGroupID)

	assert.Equal(t, 100, pnlValuations[1].LotID)
	assert.Equal(t, 600.00, pnlValuations[1].ChangeInUnrealizedProfitLossLocal)
	assert.Equal(t, 600.00, pnlValuations[1].ChangeInUnrealizedProfitLossBase)
	assert.Equal(t, 600.00, pnlValuations[1].ChangeInUnrealizedProfitLossSystem)
	assert.Equal(t, 600.00, pnlValuations[1].ChangeInUnrealizedFXGainLossBase)
	assert.Equal(t, 600.00, pnlValuations[1].ChangeInUnrealizedFXGainLossSystem)
	assert.Equal(t, 2.403, pnlValuations[1].BaseToLocalFXRate)
	assert.Equal(t, 3.6567, pnlValuations[1].SystemToLocalFXRate)
	assert.Equal(t, time.Date(2019, 01, 11, 00, 00, 00, 00, time.UTC), pnlValuations[1].StartPeriodBusinessDate)
	assert.Equal(t, time.Date(2019, 01, 11, 01, 01, 00, 00, time.UTC), pnlValuations[1].EndPeriodBusinessDate)
	assert.Equal(t, logicaltime.StartOfDay, pnlValuations[1].StartPeriodLogicalTimeID)
	assert.Equal(t, logicaltime.EndOfDay, pnlValuations[1].EndPeriodLogicalTimeID)
	assert.Equal(t, 60, pnlValuations[1].PositionStateID)
	assert.Equal(t, 1, pnlValuations[1].CloseoutGroupID)
}

func TestEmptyValuations(t *testing.T) {
	valuations := []domain.Valuation{}

	pnlValuations := CalculateProfitLoss(valuations, logger.Log)

	assert.Equal(t, 0, len(pnlValuations))
}

func getPnlValuations() []domain.Valuation {
	valuation1 := domain.Valuation{
		ID:                         1,
		LotID:                      100,
		LogicalTimeID:              logicaltime.EndOfDay,
		ValueLocal:                 1000.00,
		SecurityPriceLocal:         10.000,
		Quantity:                   100,
		IsDeleted:                  false,
		BusinessDate:               time.Date(2019, 01, 10, 00, 00, 00, 00, time.UTC),
		BaseToLocalFXRate:          2.203,
		SystemToLocalFXRate:        3.4567,
		UnrealizedProfitLossLocal:  10000,
		UnrealizedProfitLossBase:   10000,
		UnrealizedProfitLossSystem: 10000,
		FXGainLossBase:             10000,
		FXGainLossSystem:           10000,
		LocalCurrencyID:            12,
		BaseCurrencyID:             13,
		CostLocal:                  12.23,
		CostSystem:                 14.56,
		CostBase:                   17.89,
		PositionStateID:            60,
		CloseoutGroupID:            1,
	}

	valuation2 := domain.Valuation{
		ID:                         2,
		LotID:                      100,
		LogicalTimeID:              logicaltime.StartOfDay,
		ValueLocal:                 1000.00,
		SecurityPriceLocal:         10.00,
		Quantity:                   100,
		IsDeleted:                  false,
		BusinessDate:               time.Date(2019, 01, 11, 00, 00, 00, 00, time.UTC),
		BaseToLocalFXRate:          2.303,
		SystemToLocalFXRate:        3.5567,
		UnrealizedProfitLossLocal:  10400,
		UnrealizedProfitLossBase:   10400,
		UnrealizedProfitLossSystem: 10400,
		FXGainLossBase:             10400,
		FXGainLossSystem:           10400,
		LocalCurrencyID:            12,
		BaseCurrencyID:             13,
		CostLocal:                  12.23,
		CostSystem:                 14.56,
		CostBase:                   17.89,
		PositionStateID:            60,
		CloseoutGroupID:            1,
	}

	valuation3 := domain.Valuation{
		ID:                         3,
		LotID:                      100,
		LogicalTimeID:              logicaltime.EndOfDay,
		ValueLocal:                 1100.00,
		SecurityPriceLocal:         11.00,
		Quantity:                   100,
		IsDeleted:                  false,
		BusinessDate:               time.Date(2019, 01, 11, 01, 01, 00, 00, time.UTC),
		BaseToLocalFXRate:          2.403,
		SystemToLocalFXRate:        3.6567,
		UnrealizedProfitLossLocal:  11000,
		UnrealizedProfitLossBase:   11000,
		UnrealizedProfitLossSystem: 11000,
		FXGainLossBase:             11000,
		FXGainLossSystem:           11000,
		LocalCurrencyID:            12,
		BaseCurrencyID:             13,
		CostLocal:                  12.23,
		CostSystem:                 14.56,
		CostBase:                   17.89,
		PositionStateID:            60,
		CloseoutGroupID:            1,
	}

	return []domain.Valuation{valuation1, valuation2, valuation3}
}
