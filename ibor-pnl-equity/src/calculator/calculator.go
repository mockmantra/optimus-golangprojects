package calculator

import (
	"time"

	domain "stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/domainEntities"
	"stash.ezesoft.net/ipc/ezelogger"
)

// CalculateProfitLoss ...
func CalculateProfitLoss(pnlValuation []domain.Valuation, logger *ezelogger.EzeLog) []domain.ProfitLoss {
	lotValuation := transform(pnlValuation, logger)

	result := []domain.ProfitLoss{}
	for _, valuation := range lotValuation {
		if len(valuation) == 1 {
			pnl := domain.ProfitLoss{}
			pnl.ChangeInUnrealizedProfitLossLocal = valuation[0].UnrealizedProfitLossLocal
			pnl.ChangeInUnrealizedProfitLossBase = valuation[0].UnrealizedProfitLossBase
			pnl.ChangeInUnrealizedProfitLossSystem = valuation[0].UnrealizedProfitLossSystem
			pnl.ChangeInUnrealizedFXGainLossBase = valuation[0].FXGainLossBase
			pnl.ChangeInUnrealizedFXGainLossSystem = valuation[0].FXGainLossSystem

			pnl.BaseToLocalFXRate = valuation[0].BaseToLocalFXRate
			pnl.EndPeriodBusinessDate = valuation[0].BusinessDate
			pnl.StartPeriodBusinessDate = valuation[0].BusinessDate
			pnl.EndPeriodLogicalTimeID = valuation[0].LogicalTimeID
			pnl.StartPeriodLogicalTimeID = valuation[0].LogicalTimeID
			pnl.LotID = valuation[0].LotID
			pnl.SystemToLocalFXRate = valuation[0].SystemToLocalFXRate
			pnl.UtcSystemDateTime = time.Now()
			pnl.PositionStateID = valuation[0].PositionStateID
			pnl.CloseoutGroupID = valuation[0].CloseoutGroupID
			result = append(result, pnl)
		} else {
			for current := 1; current < len(valuation); current++ {
				pnl := domain.ProfitLoss{}
				previous := current - 1
				pnl.LotID = valuation[current].LotID
				pnl.ChangeInUnrealizedProfitLossLocal = valuation[current].UnrealizedProfitLossLocal - valuation[previous].UnrealizedProfitLossLocal
				pnl.ChangeInUnrealizedProfitLossBase = valuation[current].UnrealizedProfitLossBase - valuation[previous].UnrealizedProfitLossBase
				pnl.ChangeInUnrealizedProfitLossSystem = valuation[current].UnrealizedProfitLossSystem - valuation[previous].UnrealizedProfitLossSystem
				pnl.ChangeInUnrealizedFXGainLossBase = valuation[current].FXGainLossBase - valuation[previous].FXGainLossBase
				pnl.ChangeInUnrealizedFXGainLossSystem = valuation[current].FXGainLossSystem - valuation[previous].FXGainLossSystem

				pnl.ChangeInUnrealizedCapitalGainLocal = pnl.ChangeInUnrealizedProfitLossLocal
				pnl.ChangeInUnrealizedCapitalGainBase = pnl.ChangeInUnrealizedProfitLossBase - pnl.ChangeInUnrealizedFXGainLossBase
				pnl.ChangeInUnrealizedCapitalGainSystem = pnl.ChangeInUnrealizedProfitLossSystem - pnl.ChangeInUnrealizedFXGainLossSystem

				pnl.BaseToLocalFXRate = valuation[current].BaseToLocalFXRate

				pnl.EndPeriodBusinessDate = valuation[current].BusinessDate
				pnl.StartPeriodBusinessDate = valuation[previous].BusinessDate

				pnl.PositionStateID = valuation[current].PositionStateID
				pnl.CloseoutGroupID = valuation[current].CloseoutGroupID

				if valuation[current].LogicalTimeID == valuation[previous].LogicalTimeID {
					logger.Errorf("Consecutive Valuation snapshots for a LotId %d has the same LogicalTimeId: %d", pnl.LotID, valuation[current].LogicalTimeID)
				}

				pnl.EndPeriodLogicalTimeID = valuation[current].LogicalTimeID
				pnl.StartPeriodLogicalTimeID = valuation[previous].LogicalTimeID

				pnl.SystemToLocalFXRate = valuation[current].SystemToLocalFXRate
				pnl.UtcSystemDateTime = time.Now()
				// pnl.RealizedCapitalGainLocal
				// pnl.RealizedFXGainLossBase
				// pnl.RealizedFXGainLossSystem
				// pnl.RealizedFXGainLossBase

				result = append(result, pnl)
			}
		}

	}

	return result
}

func transform(pnlValuation []domain.Valuation, logger *ezelogger.EzeLog) map[int][]*domain.Valuation {
	var m map[int][]*domain.Valuation
	m = make(map[int][]*domain.Valuation)

	tempID := -1
	for i := 0; i < len(pnlValuation); i++ {
		if tempID > pnlValuation[i].ID {
			logger.Error("Valuation Ids are not sorted")
		}
		tempID = pnlValuation[i].ID

		valuation, ok := m[pnlValuation[i].LotID]
		if ok {
			valuation = append(valuation, &pnlValuation[i])
			m[pnlValuation[i].LotID] = valuation
		} else {
			m[pnlValuation[i].LotID] = []*domain.Valuation{&pnlValuation[i]}
		}

	}

	return m
}
