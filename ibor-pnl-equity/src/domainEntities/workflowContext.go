package domain

import (
	"stash.ezesoft.net/ipc/ezelogger"
)

type contextKey string

//https://medium.com/@matryer/context-keys-in-go-5312346a868d
const (
	// WorkflowContextKey to retrieve user workflow context from request context
	WorkflowContextKey    contextKey = contextKey("WorkflowContext")
	UserSessionContextKey contextKey = contextKey("UserSession")
)

// WorkflowContext contains all the information about the workflow and UserSession
type WorkflowContext struct {
	// ActivityID of the request
	ActivityID string

	Logger *ezelogger.EzeLog

	UserSession *UserSession
}
