package domain

// UserSession defines the current context's user info fetch from the session
type UserSession struct {
	FirmID           int
	UserID           int
	FirmName         string
	UserName         string
	FirmAuthToken    string
	EnvironmentName  string
	UserSessionToken string
}
