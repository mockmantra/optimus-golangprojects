package domain

import (
	"time"
)

//ProfitLoss ...
type ProfitLoss struct {
	ID                                  int
	UtcSystemDateTime                   time.Time
	LotID                               int
	StartPeriodBusinessDate             time.Time
	StartPeriodLogicalTimeID            int
	EndPeriodBusinessDate               time.Time
	EndPeriodLogicalTimeID              int
	BaseToLocalFXRate                   float64
	SystemToLocalFXRate                 float64
	ChangeInUnrealizedProfitLossLocal   float64
	ChangeInUnrealizedProfitLossBase    float64
	ChangeInUnrealizedCapitalGainLocal  float64
	ChangeInUnrealizedFXGainLossBase    float64
	ChangeInUnrealizedProfitLossSystem  float64
	ChangeInUnrealizedFXGainLossSystem  float64
	RealizedCapitalGainLocal            float64
	RealizedProfitLossLocal             float64
	RealizedFXGainLossBase              float64
	RealizedFXGainLossSystem            float64
	ChangeInUnrealizedCapitalGainBase   float64
	ChangeInUnrealizedCapitalGainSystem float64
	PositionStateID                     int
	CloseoutGroupID                     int
}
