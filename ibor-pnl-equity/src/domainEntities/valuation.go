package domain

import (
	"time"
)

// Valuation struct
type Valuation struct {
	ID                         int
	LotID                      int
	LogicalTimeID              int
	ValueLocal                 float64
	SecurityPriceLocal         float64
	Quantity                   int
	IsDeleted                  bool
	BusinessDate               time.Time
	BaseToLocalFXRate          float64
	SystemToLocalFXRate        float64
	UnrealizedProfitLossLocal  float64
	UnrealizedProfitLossBase   float64
	UnrealizedProfitLossSystem float64
	FXGainLossBase             float64
	FXGainLossSystem           float64
	LocalCurrencyID            int
	BaseCurrencyID             int
	CostLocal                  float64
	CostSystem                 float64
	CostBase                   float64
	PositionStateID            int
	CloseoutGroupID            int
}

// PnlValuation Read-API returned valuations
type PnlValuation struct {
	Valuations []Valuation
}
