package utils

import (
	"io"

	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/logger"
)

//CallClose ... Since defer can't handle the return value from a function called
//Created a wrapper which'll handle the error in case Close is called
func CallClose(c io.Closer) {
	errors := c.Close()
	if errors != nil {
		logger.GetLogger(nil, "", "").Error(errors)
	}
}
