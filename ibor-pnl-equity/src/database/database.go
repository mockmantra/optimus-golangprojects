package database

import (
	"bytes"
	"fmt"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql" //using mysql package in gorm.open()
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	appconfig "stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/config"
	domain "stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/domainEntities"
	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/interfaces"
	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/logger"
	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/models"
	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/vault"
)

const (
	tableName = "Ibor_ProfitLoss_Equity"
)

// var lock sync.Mutex
var gormOpen = gorm.Open

// DataAccessor is used to connect to db and execute db operation
type DataAccessor struct {
	db    interfaces.IDB
	firm  string
	Log   interfaces.ILogger
	vault interfaces.IVault
	//dbUtils interfaces.IDBUtils
}

// DataAccessorInstance - Returns an instance of DataAccessor struct
func DataAccessorInstance() *DataAccessor {
	vaultInstance := vault.Instance()
	return &DataAccessor{
		Log:   logger.Log,
		vault: vaultInstance,
	}
}

// Close closes the database connection of a data accessor
func (da *DataAccessor) Close() (err error) {

	if da.db != nil {
		if err = da.db.Close(); err != nil {
			err = errors.Wrapf(err, "Error while closing connection with database %v", da.getSchemaName())
			da.Log.Error(err)
		} else {
			err = nil
		}
	}
	*da = DataAccessor{}
	return err
}

// Connect establishes connection to firm's database
func (da *DataAccessor) Connect(firmAuthToken string) (err error) {
	return retry(appconfig.DB_CONNECT_ATTEMPTS, time.Second, func() error {

		connectionString, err := da.getConnectionString(firmAuthToken)
		if err != nil {
			return err
		}

		db, err := gormOpen("mysql", connectionString)
		if err != nil {
			return err
		}

		da.db = db
		return nil
	})
}

// Write will write profitloss in the database.
func (da *DataAccessor) Write(context *domain.WorkflowContext, profitLossList []domain.ProfitLoss) (e error) {
	//Uncomment to log underlying sql query
	//da.db.LogMode(true)

	tx := da.db.Begin()

	updateQuery := da.buildUpdateQuery(profitLossList)
	if err := tx.Exec(updateQuery).Error; err != nil {
		tx.Rollback()
		return err
	}

	insertQuery := da.buildInsertQuery(profitLossList)
	if err := tx.Exec(insertQuery).Error; err != nil {
		tx.Rollback()
		return err
	}
	tx.Commit()
	return nil
}

func (da *DataAccessor) buildUpdateQuery(profitLossList []domain.ProfitLoss) string {

	var buffer bytes.Buffer

	buffer.WriteString(`
	DROP TEMPORARY TABLE IF EXISTS Temp;
	CREATE TEMPORARY TABLE Temp(
		LotId INT UNSIGNED NOT NULL,
		UtcSystemDateTime DATETIME(6) NOT NULL, 
		INDEX index_temp (LotId)
	 );
	 INSERT INTO Temp(LotId, UtcSystemDateTime) VALUES `)

	pnlLength := len(profitLossList)
	for index, item := range profitLossList {
		valueFormat := "(%v,%q),"
		if index == pnlLength-1 {
			valueFormat = valueFormat[:len(valueFormat)-1]
		}
		buffer.WriteString(
			fmt.Sprintf(valueFormat,
				item.LotID,
				item.UtcSystemDateTime.Format("2006-01-02 15:04:05"),
			),
		)
	}
	buffer.WriteString(`;
	UPDATE ` + tableName + `, Temp
		INNER JOIN ` + tableName + ` PE ON PE.LotId = Temp.LotId
	SET PE.ValidToDateTimeUTC = Temp.UtcSystemDateTime
	WHERE PE.ValidToDateTimeUTC is NULL;`)

	return buffer.String()
}

func (da *DataAccessor) buildInsertQuery(profitLossList []domain.ProfitLoss) string {
	var buffer bytes.Buffer

	buffer.WriteString(`INSERT INTO ` + tableName + `(
		ValidFromDateTimeUTC, IsDeleted, LotId,PositionStateId,
		StartPeriodBusinessDate, StartPeriodLogicalTimeId, EndPeriodBusinessDate, EndPeriodLogicalTimeId,
		BaseToLocalFXRate,SystemToLocalFXRate,ChangeInUnrealizedProfitLossLocal,ChangeInUnrealizedCapitalGainLocal,
		ChangeInUnrealizedProfitLossBase,ChangeInUnrealizedFXGainLossBase,ChangeInUnrealizedProfitLossSystem,ChangeInUnrealizedFXGainLossSystem,
		RealizedProfitLossLocal, RealizedCapitalGainLocal,RealizedFXGainLossBase,RealizedFXGainLossSystem,CloseoutGroupId
		) VALUES `)

	valueFormat := "(%q, %v, %v, %v, %q, %v,%q, %v, %v,%v, %v, %v,%v, %v, %v,%v, %v, %v, %v, %v, %v),"
	for _, item := range profitLossList {
		buffer.WriteString(
			fmt.Sprintf(valueFormat,
				item.UtcSystemDateTime.Format("2006-01-02 15:04:05"),
				false,
				item.LotID,
				item.PositionStateID,
				item.StartPeriodBusinessDate.Format("2006-01-02"),
				item.StartPeriodLogicalTimeID,
				item.EndPeriodBusinessDate.Format("2006-01-02"),
				item.EndPeriodLogicalTimeID,
				item.BaseToLocalFXRate,
				item.SystemToLocalFXRate,
				item.ChangeInUnrealizedProfitLossLocal,
				item.ChangeInUnrealizedCapitalGainLocal,
				item.ChangeInUnrealizedProfitLossBase,
				item.ChangeInUnrealizedFXGainLossBase,
				item.ChangeInUnrealizedProfitLossSystem,
				item.ChangeInUnrealizedFXGainLossSystem,
				item.RealizedProfitLossLocal,
				item.RealizedCapitalGainLocal,
				item.RealizedFXGainLossBase,
				item.RealizedFXGainLossSystem,
				item.CloseoutGroupID,
			),
		)
	}
	query := buffer.String()
	return query[:len(query)-1] // remove last , (comma) from query string
}

func (da *DataAccessor) getTenantCredentials(firmToken string) (*models.AuroraConnection, error) {
	con, ok := da.vault.GetDbCredentials()[firmToken]
	if !ok {
		return nil, fmt.Errorf("No db credentials found for firm: -%s-", firmToken)
	}
	da.Log.Debugf("Vault credentials loaded for tenant %s", firmToken)
	return con, nil
}

func (da *DataAccessor) getSchemaName() string {
	return fmt.Sprintf(appconfig.DB_SCHEMA_TEMPLATE, da.firm)
}

func (da *DataAccessor) getConnectionString(firmToken string) (connectionString string, err error) {
	if appconfig.IsDevMode {
		connectionString = os.Getenv("Connection_String")
		if len(connectionString) == 0 {
			return "", errors.New("Connection string is not set")
		}
		return
	}

	connectionInfo, er := da.vault.GetTenantCredentials(firmToken)

	if er != nil {
		return "", er
	}
	da.firm = firmToken
	connectionString = buildConnectionString(connectionInfo, da.getSchemaName())
	return
}

func buildConnectionString(connectionInfo *models.AuroraConnection, dbName string) string {
	return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?parseTime=True&loc=UTC&multiStatements=true",
		connectionInfo.User,
		connectionInfo.Password,
		connectionInfo.Host,
		connectionInfo.Port,
		dbName)
}

func retry(attempts int, sleep time.Duration, f func() error) (err error) {
	for {
		if err = f(); err == nil {
			return nil
		}
		attempts--
		if attempts <= 0 {
			break
		}
		time.Sleep(sleep)
	}
	return errors.Wrap(err, "Failed to open database connection. Exiting...") // do not log connectionstring
}
