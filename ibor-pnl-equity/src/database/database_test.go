package database

import (
	"testing"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/config"
	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/logger"
	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/mocks"
	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/models"
)

// DataAccessorInstance() test
func TestDataAccessorInstance(t *testing.T) {
	da := DataAccessorInstance()
	assert.NotNil(t, da)
}

// Need Vault Mock to test getTenantCredentials
func TestGetTenantCredentialsTokenExists(t *testing.T) {
	mockDB := new(mocks.IDB)
	mockVault := new(mocks.Vault)
	da := DataAccessor{mockDB, "TestFirmName", logger.Log, mockVault}
	mockAuroraConnection := &models.AuroraConnection{Host: "testHost", Password: "testpass", Port: 8888, User: "testUser"}
	mockMap := map[string]*models.AuroraConnection{"testFirmToken": mockAuroraConnection}
	mockVault.On("GetDbCredentials").Return(mockMap)

	con, err := da.getTenantCredentials("testFirmToken")
	assert.Nil(t, err)
	assert.Equal(t, con, mockAuroraConnection)
}

func TestGetTenantCredentialsTokenDoesNotExist(t *testing.T) {
	mockDB := new(mocks.IDB)
	mockVault := new(mocks.Vault)
	da := DataAccessor{mockDB, "TestFirmName", logger.Log, mockVault}
	mockMap := map[string]*models.AuroraConnection{}
	mockVault.On("GetDbCredentials").Return(mockMap)

	con, err := da.getTenantCredentials("testFirmToken")
	assert.NotNil(t, err)
	assert.Nil(t, con)
}

func TestCloseError(t *testing.T) {
	mockDB := new(mocks.IDB)
	mockVault := new(mocks.Vault)
	da := DataAccessor{mockDB, "TestFirmName", logger.Log, mockVault}
	mockDB.On("Close").Return(errors.New("Close failed"))
	err := da.Close()
	assert.NotNil(t, err)
	assert.Equal(t, da, DataAccessor{})
}

func TestCloseSuccessful(t *testing.T) {
	mockDB := new(mocks.IDB)
	mockVault := new(mocks.Vault)
	da := DataAccessor{mockDB, "TestFirmName", logger.Log, mockVault}
	mockDB.On("Close").Return(nil)
	err := da.Close()
	assert.Nil(t, err)
	assert.Equal(t, da, DataAccessor{})
}

// If firm token was not found
func TestConnectFirmTokenNotFound(t *testing.T) {
	mockDB := new(mocks.IDB)
	mockVault := new(mocks.Vault)
	da := DataAccessor{mockDB, "TestFirmName", logger.Log, mockVault}
	mockVault.On("GetTenantCredentials", mock.Anything).Return(&models.AuroraConnection{}, errors.New("No db credentials found"))
	err := da.Connect("firmtoken")

	assert.NotNil(t, err)
}

// If getting tenant credentials fail
func TestConnectGetTenantCredentialsFail(t *testing.T) {
	mockDB := new(mocks.IDB)
	mockVault := new(mocks.Vault)
	da := DataAccessor{mockDB, "TestFirmName", logger.Log, mockVault}
	mockVault.On("GetTenantCredentials", mock.Anything).Return(&models.AuroraConnection{}, errors.New("No db credentials found"))
	err := da.Connect("firmtoken")
	assert.NotNil(t, err)
}

func TestConnectTokenDBOpenFailure(t *testing.T) {
	// Save current function and restore at the end:
	oldGormOpen := gormOpen
	defer func() { gormOpen = oldGormOpen }()

	gormOpen = func(a string, b ...interface{}) (*gorm.DB, error) {
		return nil, errors.New("Cannot open DB")
	}

	mockDB := new(mocks.IDB)
	mockVault := new(mocks.Vault)
	da := DataAccessor{mockDB, "TestFirmName", logger.Log, mockVault}
	mockAuroraConnection := &models.AuroraConnection{Host: "testHost", Password: "testpass", Port: 8888, User: "testUser"}

	mockVault.On("GetTenantCredentials", mock.Anything).Return(mockAuroraConnection, nil)
	err := da.Connect("firmtoken")

	assert.NotNil(t, err)
}

// If there is an error connecting to db and credentials cannot be retrieved from the vault
func TestConnectTokenDBOpenFailureCredentialsCouldNotBeRetrieved(t *testing.T) {
	// Save current function and restore at the end:
	oldGormOpen := gormOpen
	defer func() { gormOpen = oldGormOpen }()

	gormOpen = func(a string, b ...interface{}) (*gorm.DB, error) {
		return nil, errors.New("Cannot open DB")
	}

	mockDB := new(mocks.IDB)
	mockVault := new(mocks.Vault)
	da := DataAccessor{mockDB, "TestFirmName", logger.Log, mockVault}

	mockAuroraConnection := &models.AuroraConnection{Host: "testHost", Password: "testpass", Port: 8888, User: "testUser"}

	mockVault.On("GetTenantCredentials", mock.Anything).Return(mockAuroraConnection, nil)
	err := da.Connect("firmtoken")

	assert.NotNil(t, err)
}

// If connect is successful
func TestConnectSuccess(t *testing.T) {
	// Save current function and restore at the end:
	oldGormOpen := gormOpen
	defer func() { gormOpen = oldGormOpen }()

	gormOpen = func(a string, b ...interface{}) (*gorm.DB, error) {
		return nil, nil
	}

	mockDB := new(mocks.IDB)
	mockVault := new(mocks.Vault)
	da := DataAccessor{mockDB, "TestFirmName", logger.Log, mockVault}
	mockAuroraConnection := &models.AuroraConnection{Host: "testHost", Password: "testpass", Port: 8888, User: "testUser"}

	mockVault.On("GetTenantCredentials", mock.Anything).Return(mockAuroraConnection, nil)
	config.IsDevMode = false
	err := da.Connect("firmtoken")

	assert.Nil(t, err)
}
