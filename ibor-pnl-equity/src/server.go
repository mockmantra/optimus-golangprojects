package main

import (
	"os"

	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/app"
	"stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/config"
)

// func healthHandler() {
// 	http.HandleFunc("/api/ibor/pnl/equity/v1/health", func(w http.ResponseWriter, r *http.Request) {
// 		fmt.Fprintf(w, "OK")
// 	})
// 	addr := fmt.Sprintf(":%s", appconfig.Port)
// 	logger.Log.Infof("listen on%s", addr)
// 	logger.Log.Error(http.ListenAndServe(addr, nil))
// }

func main() {
	// healthHandler()
	initConfig()
	app.Run()
}

func initConfig() {
	err := config.InitializeFromVault()
	if err != nil {
		os.Exit(1)
	}
}
