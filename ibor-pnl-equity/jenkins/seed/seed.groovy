def folder='accounting/ibor-pnl-equity'
def testFolder="$folder/tests"

//This is the main container build
job("$folder/build_ibor_pnl_equity") {
  label('autoscale')
  multiscm {
    git {
      remote {
        name('origin')
        url('ssh://git@stash.ezesoft.net:7999/imsacnt/ibor-pnl-equity.git')
        credentials('stash_global')
      }
      branch('master')
      extensions {
        submoduleOptions {
          //get latest submodules
          tracking(true)
        }
       }
      }
  }
	triggers {
		scm('H/5* * * * *')
	}
  wrappers {
    buildInDocker {
      dockerfile('./jenkins/slave')
      volume('/var/run/docker.sock', '/var/run/docker.sock')
      userGroup('2000')
      verbose()
    }
    environmentVariables {
    	propertiesFile('./jenkins/properties')
    }
    credentialsBinding {
      usernamePassword('dtr_user', 'dtr_password', 'dtr-builder')
      usernamePassword('vaultguard_username', 'vaultguard_password', 'vaultguard-user')
    }
    preBuildCleanup()
    maskPasswords()
    ansiColorBuildWrapper {
      colorMapName('XTerm')
      defaultFg(15)
      defaultBg(1)
    }
    sonarBuildWrapper {
      installationName 'SonarQube'
    }
    buildName('${ENV,var="container_name"} ${ENV,var="version"}')
  }
  steps {

    // Pull the latest jenkins-scripts
    shell('if [ -d $scripts ]; then rm -rf $scripts; fi && git clone $scripts_repo')

    // Copy build files into local GOPATH in slave container
    shell('mkdir -p /go/src/stash.ezesoft.net/imsacnt/ibor-pnl-equity && cp -r $WORKSPACE/* /go/src/stash.ezesoft.net/imsacnt/ibor-pnl-equity')

    // Fetch dep package manager
    shell('/usr/local/go/bin/go get -u github.com/golang/dep/cmd/dep')

    // Install dep
    shell('cd $GOPATH/src/github.com/golang/dep && /usr/local/go/bin/go install ./...')

    // Restore dependencies.
    shell( 'cd /go/src/stash.ezesoft.net/imsacnt/ibor-pnl-equity && dep ensure -vendor-only')

    // Build and generate the application binary
    shell('CGO_ENABLED=0 GOOS=linux GOARCH=amd64 /usr/local/go/bin/go build -a -installsuffix cgo -ldflags="-w -s" -o $WORKSPACE/ibor-pnl-equity stash.ezesoft.net/imsacnt/ibor-pnl-equity/src/')

    // Run lint and Unit Tests
    shell('bash jenkins/scripts/unitTest.sh')

    // Run sonarqube scanner
    sonarRunnerBuilder({
     project './jenkins/sonar-project.properties'
     properties 'sonar.projectVersion=$version_prefix.$BUILD_NUMBER'
     jdk '(Inherit From Job)'
     task 'scan'
    })

    // Build the container
    shell('bash jenkins-scripts/build-container-step.sh')

    // Run component test BDDs on the built container
    shell('bash componentTest.sh')

    // Upload the container
    shell('bash jenkins-scripts/upload-container-step.sh')

    // Register container with Vault
    shell('bash jenkins-scripts/register-container.sh Dev')
    shell('bash jenkins-scripts/register-container.sh AWSProd')

    // Update Rancher Stack Information
    shell('cd rancher && bash ../jenkins-scripts/rancher-run-template.sh && bash ../jenkins-scripts/rancher-update-catalog.sh')
  }

  // Publish Cucumber Report.
  configure { project ->
  project / 'publishers' << 'net.masterthought.jenkins.CucumberReportPublisher' {
    fileIncludePattern '**/*.json'
    jsonReportDirectory 'reports'
    buildStatus 'FAILURE'  //other option is 'UNSTABLE' - if you'd like it left unchanged, don't provide a value
    sortingMethod 'ALPHABETICAL'
  }
}

  publishers {
    // Publish JUnit Report.
    archiveJunit('reports/jUnitReport.xml')

    // Publish Cobertura Code Coverage Report.
    cobertura('reports/coverage.xml')


    archiveArtifacts{
      pattern("reports/*.xml, reports/*.json")
      pattern("rancher/docker-compose.yml")
      pattern("rancher/rancher-compose.yml")
      onlyIfSuccessful()
    }

	whiteSourcePublisher {
		jobCheckPolicies('global')
		jobForceUpdate('global')
		jobApiToken('bc083042-88fb-4776-9288-afdf9044ae01')
		jobUserKey(null)
		product('b8072ee844704d9ca5cdecce033784522464af0749b04c26953779d48e55acad')
		productVersion(null)
		projectToken(null)
		libIncludes('**/*.c **/*.cc **/*.cp **/*.cpp **/*.cxx **/*.c++ **/*.h **/*.hpp **/*.hxx **/*.dll **/*.cs **/*.nupkg **/*.m **/*.mm **/*.js **/*.php **/*.gem **/*.rb **/*.tgz **/*.deb **/*.gzip **/*.rpm **/*.tar.bz2 **/*.zip **/*.tar.gz **/*.egg **/*.whl **/*.py')
		libExcludes('**/*.jar')
		requesterEmail(null)
		ignorePomModules(false)
		mavenProjectToken(null)
		modulesToInclude(null)
		modulesToExclude(null)
		moduleTokens(null)
	}
    stashNotifier()

    //Yes we need all these settings :(
    slackNotifier {
      teamDomain(null)
      authToken(null)
      room('team-optimus')
      startNotification(true)
      notifyNotBuilt(true)
      notifyAborted(true)
      notifyFailure(true)
      notifySuccess(true)
      notifyUnstable(true)
      notifyBackToNormal(true)
      notifyRepeatedFailure(true)
      includeTestSummary(true)
      includeCustomMessage(false)
      customMessage(null)
      sendAs(null)
      commitInfoChoice('NONE')
    }


    triggers {
      scm('H/5 * * * *')
    }
  }
}
