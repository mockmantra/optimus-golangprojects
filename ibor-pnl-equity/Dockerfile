FROM alpine:3.8

LABEL com.ezesoft.secrets.want=true

ENV ROUTING_SCRIPTS_DIR=/routing-scripts

# Copy the service executable to gobin
COPY ./ibor-pnl-equity /go/bin/
COPY ./*.sh /
COPY ./etc/routing-scripts/*.sh /routing-scripts/
COPY ./src/swagger-ui/ /swagger-ui/
COPY ./src/swagger.yaml /

# added specific version of curl 7.48 as required by vault-certs.sh
RUN apk update && apk add --no-cache ca-certificates bash openssl curl unzip && mkdir -p /tmp && \
  bash $ROUTING_SCRIPTS_DIR/certs.sh && \
  curl https://releases.hashicorp.com/vault/0.8.3/vault_0.8.3_linux_amd64.zip -o /tmp/vault.zip && \
  unzip /tmp/vault.zip -d /bin && \
  rm /tmp/vault.zip && \
  curl https://s3.amazonaws.com/linux-tarballs/curl/curl-7.48-trusty-2.tar.gz -o /tmp/curl-7.48-trusty-2.tar.gz && \
  tar -xvzf /tmp/curl-7.48-trusty-2.tar.gz -C /tmp && \
  mv /tmp/curl /bin/curl-7.48 && \
  rm /tmp/curl-7.48-trusty-2.tar.gz && \
  curl https://releases.hashicorp.com/consul-template/0.14.0/consul-template_0.14.0_linux_amd64.zip -o /tmp/consul-template.zip && \
  unzip -o /tmp/consul-template.zip -d /bin && \
  rm /tmp/consul-template.zip && \
  chmod +x /ibor-pnl-equity-entrypoint.sh

ENV GIN_MODE=release

EXPOSE 33380

ENTRYPOINT ["/ibor-pnl-equity-entrypoint.sh"]