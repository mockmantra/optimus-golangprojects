set -e

BDD_CLIENT="bdd_client"
SERVICE="service"
MOCK_IMS="mock_ims"
DB="sqldb"
NETWORK="bdd_network"
serviceTag="service"
bddReportPath="./reports/"
skipServiceBuild=false

if [ "$tag" != "" ]; then
serviceTag=$tag
skipServiceBuild=true
fi

set -e

cleanup_network() {
    echo "Cleaning up network: $NETWORK"
    if docker network ls | grep $NETWORK
    then
        echo "found network: $NETWORK"
        docker network rm $NETWORK
   else
        echo "Network not found."
    fi
}

stop_containers(){
    echo "Step1 Stopping any zombie containers..."
    declare -a arr=($BDD_CLIENT $SERVICE $MOCK_IMS $DB)

    for i in "${arr[@]}"
    do
        echo "Removing $i ......"
        container_id=$(docker ps -aqf "name=$i")
        if [[ ! -z "$container_id" ]]
        then
            echo "Container found: " $container_id
            docker rm -f $container_id
        else
            echo "No container found for: $i"
        fi
    done
}

function cleanup {
    stop_containers
    cleanup_network
}

function run_tests {
    echo "called with param: $1"
    cleanup

    echo "Step 1 :Create network: $NETWORK"
    docker network create --driver bridge $NETWORK 
    
    echo "Step 2 :Build mock-ims....."
    docker build -t $MOCK_IMS -f tests/bdd/mock-eclipse/Dockerfile  tests/bdd/mock-eclipse

    echo "Step 3 :Run mock-ims....."
    docker run -d --name $MOCK_IMS --network $NETWORK $MOCK_IMS

    echo "Step 4 :Build DataBase"
    docker build -t $DB -f tests/bdd/data/Dockerfile tests/bdd/data

    echo "Step 5 :Run DataBase"
    docker run -d --name $DB --network $NETWORK $DB 

    if $skipServiceBuild ; then
        echo "Step 6 :Skiping service build and will use the existing build tag $serviceTag ....."
    else
        echo "Step 6 :Build service in bdd mode....."
        docker build -t $SERVICE -f tests/bdd/service/Dockerfile .
    fi
    
                                                           
    echo "Step 7 :Run service....."                                                 
    docker run -d --name $SERVICE -e APP_ENV="local" -e IMS_BASE_URL=http://$MOCK_IMS:3000 -e Connection_String="root:hello!1234@tcp($DB:3306)/pnlDb?parseTime=true&multiStatements=true" --network $NETWORK $serviceTag
    
    sleep 10
    echo "Step 8 : Build component-tests-bdd....."
    docker build -t $BDD_CLIENT --build-arg service=$SERVICE -f tests/bdd/component-tests/Dockerfile tests/bdd/component-tests/
    echo "Step 9 : Run component-tests-bdd....."
    docker run  --name $BDD_CLIENT -e Auth_API_ROOT=http://$MOCK_IMS:3000 -e API_ROOT=http://$SERVICE:33380 --network $NETWORK $BDD_CLIENT $1
    
    mkdir -p $bddReportPath
    docker cp $BDD_CLIENT:usr/src/app/report/cucumber_report.json $bddReportPath

    
}

time run_tests $1
cleanup