const common = require("./common");
const session = require("./session");


let mergedApiDefs = {};
Object.assign(mergedApiDefs, common, session);

module.exports = mergedApiDefs;