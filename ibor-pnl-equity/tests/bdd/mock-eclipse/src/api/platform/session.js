const sessionRoute = "/api/platform/v1/session";
const srpTransactionRoute = "/api/platform/v1/srptransaction";
const userStore = require("../../common/sessions");
const basicAuth = require("basic-auth");    

const srpApi = {
    route: srpTransactionRoute,
    post: function (req, res, next) {
        // for SRP always return JSON with ResourceId null which will force fallback to SessionPost
        res.status(200).json({
            ResourceId: null
        });
    }
}

const sessionApi = {
    route: sessionRoute,
    post: function(req, res, next) {
        if(req.headers.authorization) {
            var credentials = basicAuth(req);
            if(credentials) {
                let fqUserName =  credentials.name.toUpperCase();
                let fqUserNameObj = fqUserName.split('\\');
                let user = userStore.getUserData(`${fqUserNameObj[0]}-${fqUserNameObj[1]}`);

                if(user && user.credentials.password === credentials.pass) {
                    return res.status(200).json(user.session);
                }
            }

            res.status(401).json("not authorized");
        }else {
            res.status(400).json("bad request");
        }
    },
    get: function(req, res, next) {
        if(req.headers.authorization) {
            var credentials = basicAuth(req);
            if(credentials) {
                let session = userStore.getSessionByGlxToken(credentials.name);
                if(session) {
                    return res.status(200).json(session);
                }
            }

            res.status(401).json("not authorized");
        }else {
            res.status(401).json("not authrozied");
        }
    },

    head: function(req, res, next) {
        if(req.headers.authorization) {
            var credentials = basicAuth(req);
            if(credentials) {
                let session = userStore.getSessionByGlxToken(credentials.name);
                if(session) {
                    return res.status(200).json({});
                }
            }

            res.status(401).json("not authorized");
        }else {
            res.status(401).json("not authrozied");
        }
    }
}

module.exports = {
    srpApi,
    sessionApi
}