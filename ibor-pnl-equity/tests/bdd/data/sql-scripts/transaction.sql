DROP DATABASE IF EXISTS pnlDb;
create database pnlDb;
use pnlDb;
show databases;

DROP TABLE IF EXISTS Ibor_ProfitLoss_Equity;


CREATE TABLE IF NOT EXISTS Ibor_ProfitLoss_Equity(
 Id 		INT UNSIGNED 		NOT NULL 	AUTO_INCREMENT, 
 ValidFromDateTimeUTC 		DATETIME(6) 	NOT NULL, 
 ValidToDateTimeUTC		DATETIME(6), 							
 IsDeleted 	BOOL 			NOT NULL 	DEFAULT FALSE,
 LotId			INT UNSIGNED 	  	NOT NULL,
 PositionStateId		INT UNSIGNED		NOT NULL,
 StartPeriodBusinessDate 		DATE 		NOT NULL, 
 StartPeriodLogicalTimeId			INT UNSIGNED  	NOT NULL,
 EndPeriodBusinessDate			DATE 			NOT NULL, 
 EndPeriodLogicalTimeId			INT UNSIGNED 		NOT NULL,
 BaseToLocalFXRate			DECIMAL (28,10)		NOT NULL,
 SystemToLocalFXRate			DECIMAL (28,10) 	NOT NULL,
 ChangeInUnrealizedProfitLossLocal 	 DECIMAL (28,10) NOT NULL,  
 ChangeInUnrealizedProfitLossBase 	DECIMAL (28,10)  NOT NULL,  
 ChangeInUnrealizedProfitLossSystem 	 DECIMAL (28,10)    NOT NULL,  
 ChangeInUnrealizedFXGainLossBase		 DECIMAL (28,10)  NOT NULL,
 ChangeInUnrealizedFXGainLossSystem		 DECIMAL (28,10)  NOT NULL,
 ChangeInUnrealizedCapitalGainLocal		 DECIMAL (28,10)  NOT NULL,
 RealizedProfitLossLocal 		DECIMAL (28,10)   NOT NULL,
 RealizedFXGainLossBase		 DECIMAL (28,10) NOT NULL,
 RealizedFXGainLossSystem		 DECIMAL (28,10) NOT NULL,
 RealizedCapitalGainLocal DECIMAL (28,10)   NOT NULL,
 CloseoutGroupId		INT UNSIGNED      NOT NULL,
 
 PRIMARY KEY(Id));

  show tables;

