'use strict';
const request = require('request-promise');



class HttpService {
    /**
     * Basic HTTP service
     * @param {string} headerToken Auth token to be used in all HTTP calls
     * @param {string} apiRoot Root for all http calls
     */
    constructor(headerToken, apiRoot) {
        this.headerToken = headerToken;
        this.apiRoot = apiRoot;
        this.setDefaults(apiRoot);
    }

    setDefaults(apiPath) {

        this.baseRequest = request.defaults({
            timeout: 60000,
            baseUrl: `${this.apiRoot}`,
            resolveWithFullResponse: true,
            simple: false,
            headers: {
                'Authorization': this.headerToken,
                'Content-Type': 'application/json'
            }

        });
        // console.log("base url",baseUrl);
    }

    /**
     * Method will accept the api endpoint and an object then fire a GET request 
     * @typedef {Object} params Object to be formatted into query string
     * @property {string|Number} queryParam the name of the query parameter and its value
     * @param {string} path The path to the method
     * @param {params} params
     * @example
     */
    get(path, params) {
        // if (params) {
        //     console.log("before");
        //     params = JSON.parse(params);
        //     console.log("after");
        // }
        return this.baseRequest(path, {
            method: 'GET',
            qs: params
        }).then(res => res.toJSON());

        console.log("!!!!!#############RESPONSE#############!!!! " + JSON.parse(this.response.body));
    }


    /**
     * Make a DELETE request
     * @param {string} path path to the API (after the base url)
     * @param {*} params object to be passed as parameters
     * @example
     * `delete('positionEvents', {'sourceSystemName': ____, 'sourceSystemReference': _____, 'utcAsOfDateTime': _____})`
     */
    delete(path, params) {
        return this.baseRequest(path, {
            method: 'DELETE',
            qs: params
        }).then(res => res.toJSON());
    }

    /**
     * Make a POST request
     * @param {string} path the path to the method
     * @param {{}} body the JSON object being passed to the POST call
     */
    postj(path, body) {
        return this.baseRequest(path, {
            method: 'POST',
            body: body
        }).then(res => res.toJSON());
    }

    postp(path, params) {
        if (params) {
            params = JSON.parse(params);

        }
        return this.baseRequest(path, {
            method: 'POST',
            qs: params
        }).then(res => res.toJSON());
    }

    put(path, body) {
        return this.baseRequest(path, {
            method: 'PUT',
            body: body
        }).then(res => res.toJSON());
    }
    undelete(path, params) {
        return this.baseRequest(path, {
            method: 'PUT',
            qs: params
        }).then(res => res.toJSON());
    }


}

module.exports = HttpService;