
Feature: Pnl Service Tests

    Scenario: Validate pnl 200 status

        When I make a POST request to /api/ibor/pnl/equity/v1/calculateProfitLoss with JSON:
            """
            {
            "name":"PNL_Positive_200.json"
            }
            """
        Then I should get a 200 response

    Scenario: Validate pnl 400 status

        When I make a POST request to /api/ibor/pnl/equity/v1/calculateProfitLoss with JSON:
            """
            {
            "name":"PNL_Negative_400.json"
            }
            """
        Then I should get a 400 response

    Scenario: Validate pnl 400 status with no data in request

        When I make a POST request to /api/ibor/pnl/equity/v1/calculateProfitLoss with JSON:
            """
            {
            "name":"PNL_Negative_400_NoFields.json"
            }
            """
        Then I should get a 400 response
