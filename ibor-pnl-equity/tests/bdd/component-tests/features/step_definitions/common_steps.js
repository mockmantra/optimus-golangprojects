/*
Any common steps will be placed here for use and
chai will be used for assertions
*/
'use strict';

const { defineSupportCode } = require('cucumber')
const chai = require('chai');
const chaiExclude = require('chai-exclude');
chai.use(chaiExclude);
var { setDefaultTimeout } = require('cucumber');
const _ = require('lodash');
setDefaultTimeout(120 * 1000);


defineSupportCode(function (self) {
    let Given = self.Given;
    let When = self.When;
    let Then = self.Then;

    Given(/^a request to (.*)$/, function (endpoint) {
        this.httpService.setDefaults(endpoint);
    });

    When(/^I send a GET request to (.*)$/, async function (path) {
        this.serviceResponse = await this.httpService.get(path);

    });
    When(/^I send a POST request to (.*)$/, async function (path) {
        this.serviceResponse = await this.httpService.post(path);

    });

    When(/^I make a GET request to (.*) with the JSON:$/, async function (path, body) {

        var exfilename = ((JSON.parse(body)).name);
        var fs = require("fs");
        var relativeFileName = __dirname + '/../JSONs/' + exfilename;
        var config = fs.readFileSync(relativeFileName, "utf8");

        console.log("COMING FROM TEXT FILE" + config);
        this.response = await this.httpService.get(path, config);
        var sleep = require('system-sleep');
        sleep(10 * 1000); // sleep for 30 seconds

    });

    When(/^I make a POST request to (.*) with JSON:$/, async function (path, body) {

        var exfilename = ((JSON.parse(body)).name);
        var fs = require("fs");
        var relativeFileName = __dirname + '/../JSONs/' + exfilename;
        var config = fs.readFileSync(relativeFileName, "utf8");
        this.serviceResponse = await this.httpService.postj(path, config);

    });

    Then(/^I should get a (\d{3}) response$/, function (responseCode) {
        console.log("service response", this.serviceResponse);
        console.log("status code", this.serviceResponse.statusCode);
        chai.expect(this.serviceResponse.statusCode).to.equal(parseInt(responseCode));
    });

    Then(/the status code should be (\d+)/, function (statusCode) {
        chai.expect(this.serviceResponse.statusCode).to.equal(statusCode);
    });

    Then(/the status code should not be (\d+)/, function (statusCode) {
        chai.expect(this.serviceResponse.statusCode).to.not.equal(statusCode);
    });

});
