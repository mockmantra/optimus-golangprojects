const fs = require('fs-extra');

function loadFile(context, filePath) {
    var data = fs.readJsonSync(filePath);
    context.request = data.request;
    context.expectedResponse = data.response;
    var activityId = null;
    if ("scenario_api_response" in data) {
        activityId = data["scenario_api_response"];
        activityId["Request"] = data.request;
        activityId = JSON.stringify(activityId);
    }
    context.activityId = activityId;
}

module.exports = {
    loadFile
}